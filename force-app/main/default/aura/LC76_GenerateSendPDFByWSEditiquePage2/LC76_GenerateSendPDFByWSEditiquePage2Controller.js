({
    doInit : function(component, event, helper) {
        helper.initOptions(component);
        helper.initData(component);
    },
    changeCheckBox : function(component, event, helper) {
        if(event.target.checked)
            document.getElementById('generer_email').checked = true;
    },
    changeRadio : function(component, event, helper) {
        var acc = component.get("v.accountInfo");
        if (acc.Type === 'BusinessAccount' && document.getElementById('generer_email').checked === false) {
            acc.Contacts.forEach(con => {
                con.value = false;
            });
        }
        component.set("v.accountInfo", acc);
    },
})