({
    initOptions : function(component) {
        var sendOptions = [
            {
                label: 'Générer et envoyer par mail à :',
                value: 'generer_email'
            },
            {
                label: 'Générer et envoyer par courrier à :',
                value: 'generer_courrier'
            },
            {
                label: 'Générer le document sans l\'envoyer',
                value: 'generer'
            }
        ];

        component.set("v.sendOptions", sendOptions);
    },

    initData : function(component) {
        //0Q00Q000000TvTWSA0 quote
        var action = component.get("c.pickListvalue");
        action.setParams({
            //"recordId": component.get("v.recordId")
            "recordId": "0Q00Q000000TvTWSA0"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('1');
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                var list = response.getReturnValue();
                var accountInfo = {};
                for(var i = 0; i < list.length ; i++) {
                    if (list[i].label == 'document' && list[i].data != null) {
                        component.set("v.isQuote", true);
                        var documentOptions = [];
                        list[i].data.forEach(elt => {
                            documentOptions.push({
                                label: elt.label,
                                value: elt.value
                            });
                        });
                        component.set("v.documentOptions", documentOptions);
                    }
                    else if (list[i].label == 'sendMethod') {
                        list[i].data.forEach(elt => {
                            switch (elt.label) {
                                case "Type":
                                    accountInfo.Type = elt.value;
                                    break;
                                case "Email":
                                    accountInfo.Email = elt.value;
                                    break;
                                case "Name":
                                    accountInfo.Name = elt.value;
                                    break;
                                case "LastName":
                                    accountInfo.LastName = elt.value;
                                    break;
                                case "FirstName":
                                    accountInfo.FirstName = elt.value;
                                    break;
                                case "Salutation":
                                    accountInfo.Salutation = elt.value;
                                    break;
                                case "BillingStreet":
                                    accountInfo.BillingStreet = elt.value;
                                    break;
                                case "BillingPostalCode":
                                    accountInfo.BillingPostalCode = elt.value;
                                    break;
                                case "BillingCity":
                                    accountInfo.BillingCity = elt.value;
                                    break;
                                case "Contacts":
                                    accountInfo.Contacts = [];
                                    elt.value.forEach(cons => {
                                        var valueCon = {};
                                        cons.forEach(con => {
                                            switch (con.label) {
                                                case "LastName":
                                                    valueCon.LastName = con.value;
                                                    break;
                                                case "FirstName":
                                                    valueCon.FirstName = con.value;
                                                    break;
                                                case "Email":
                                                    valueCon.Email = con.value;
                                                    break;
                                            }
                                            valueCon.value = false;                                 //valeur du checkbox true ou false
                                        });
                                        accountInfo.Contacts.push(valueCon);
                                    });
                                    break;
                                default:
                            }
                            
                        });
                    }
                }
                component.set("v.accountInfo", accountInfo);
                console.log(accountInfo);
                this.checkValidation(component);
            }
            else
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()));
                         
        });
        $A.enqueueAction(action);
    },

    checkValidation : function(component) {
        var acc = component.get("v.accountInfo");
        if (acc.Type === 'PersonAccount' && acc.Email === '') {
            document.getElementById('generer_email').disabled = true;
        }
        if (acc.Type === 'BusinessAccount') {
            var b = false;
            acc.Contacts.forEach(con => {
                if (con.Email !== '')
                    b = true;
            });
            if (!b)
                document.getElementById('generer_email').disabled = true;
        }
        
        if (acc.BillingStreet === '' || acc.BillingPostalCode === '' || acc.BillingCity === '') {
            document.getElementById('generer_courrier').disabled = true;
        }
    }

})