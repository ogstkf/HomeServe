/**
 * @File Name          : LC02_EligibilityWizardHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 11/07/2019, 15:54:51
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    09/07/2019, 10:06:32   RRJ     Initial Version
 **/
({
    setPage: function(component, page) {
        if (page == 1) {
            component.set("v.currentPage", 1);
        }
        if (page == 2) {
            component.set("v.currentPage", 2);
        }
        if (page == 3) {
            component.set("v.currentPage", 3);
        }
        if (page == 4) {
            component.set("v.currentPage", 4);
        }
    }
});