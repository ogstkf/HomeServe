/**
 * @File Name          : LC02_EligibilityWizardController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 19/09/2019, 16:26:57
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    08/07/2019, 16:56:32   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        // component.set("v.currentPage", 3);
        var pageReference = component.get("v.pageReference");
        if (pageReference.state) {
            helper.setPage(component, pageReference.state.c__pgNum);
        }
    },

    handlePageEvt: function(component, event, helper) {
        var clientData = event.getParam("pageData");
        var pgNum = event.getParam("pageNo");
        clientData.prevPage = clientData.currPage;
        clientData.currPage = pgNum;
        component.set("v.clientDetails", clientData);
        helper.setPage(component, pgNum);
    }
});