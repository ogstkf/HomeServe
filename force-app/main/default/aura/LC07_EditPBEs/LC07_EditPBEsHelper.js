/**
 * @File Name          : LC07_EditPBEsHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : LGO
 * @Last Modified On   : 17-11-2021
 * @Modification Log   :
 * Ver       Date     Author      		    Modification
 * 1.0    06/01/2020   RRJ  Initial Version
 * 2.0    23/07/2020   RRA                 TEC-66- Permettre la saisie négative du champ Prix Catalogue de l'info AIDE                                             
 **/
 ({
    tvaValues: function(component, event, helper){
        var tvaValues = $A.get("$Label.c.ValeursTVA").split(';');
        var options = [];
        options.push({
            label:'',
            value: null
        });
        
        for (var j = 0; j < tvaValues.length; j++) {
            options.push({
                label:tvaValues[j]*100,
                value: tvaValues[j]*100
            });
        }
        component.set('v.lstTVAValues', options);

        // component.find("a_opt").set("v.value", options[0]);
        // console.log('hihi ',component.get('v.lstTVAValues'));
    },
    calculatePrixHt: function (lineItem) {
        if (
            !$A.util.isEmpty(lineItem.qli.Remise_en100__c) &&
            !$A.util.isEmpty(lineItem.qli.UnitPrice) &&
            !$A.util.isEmpty(lineItem.qli.Quantity)
        ) {
            return Math.round(((100 - lineItem.qli.Remise_en100__c) / 100) * lineItem.qli.UnitPrice * 100) * lineItem.qli.Quantity / 100;
        } else if (
            !$A.util.isEmpty(lineItem.qli.Remise_en_euros__c) &&
            !$A.util.isEmpty(lineItem.qli.UnitPrice) &&
            !$A.util.isEmpty(lineItem.qli.Quantity)
        ) {
            return Math.round((lineItem.qli.UnitPrice * lineItem.qli.Quantity - lineItem.qli.Remise_en_euros__c) *100) / 100;
        } else if (!$A.util.isEmpty(lineItem.qli.UnitPrice) && !$A.util.isEmpty(lineItem.qli.Quantity)) {
            return lineItem.qli.UnitPrice * lineItem.qli.Quantity;
        } else {
            return 0;
        }
    },
    calculatePrixTTC: function (line) {
        if (!$A.util.isEmpty(line.prixHt) && !$A.util.isEmpty(line.qli.Taux_de_TVA__c)) {
            console.log('TVA = ', line.qli.Taux_de_TVA__c);
              // RRA - 20200724 - CT-66 - le taux ne s'applique pas pour l'info AIDE - reference sur la page confluence -->
            var prixLineHt;
            if (line.prodCode.substr(0,4) === "AIDE"){
                prixLineHt = line.prixHt;
                line.qli.Taux_de_TVA__c = 0;
            }
            else{
                prixLineHt = line.prixHt + line.prixHt * (line.qli.Taux_de_TVA__c / 100);
            }
            return prixLineHt
        }
        return line.prixHt;
    },

    checkValidity: function (line, quote) {
        var isValid = true;
        var minPrice = quote.RecordType.DeveloperName == 'Devis_RTC' ? line.listPrice : 0;
            console.log('line', line.prodCode);
         // RRA - 20200723 - CT-66 - permettre la saisie négative du champ Prix Catalogue de l'info AIDE - reference sur la page confluence -->
            if (line.prodCode != undefined && line.prodCode.substr(0,4) === "AIDE"){
                console.log('line substr ', line.prodCode.substr(0,4) );
                isValid = true;
            }
            //MNA -16/11/2021 TEC-696 Remise exceptionnel
            else if (line.isRemiseExc ) {
                if(!$A.util.isEmpty(line.qli.UnitPrice) && line.qli.UnitPrice <= 0) 
                    isValid = true;
                else
                    isValid = false;
            } 
            else {
                if (!$A.util.isEmpty(line.qli.UnitPrice) && line.qli.UnitPrice < minPrice) {
                    isValid = false;
                } else if (!$A.util.isEmpty(line.qli.Quantity) && line.qli.Quantity < 0) {
                    isValid = false;
                } else if (
                    !$A.util.isEmpty(line.qli.Remise_en100__c) &&
                    (line.qli.Remise_en100__c < 0 || line.qli.Remise_en100__c > 100)
                ) {
                    isValid = false;
                } else if (
                    !$A.util.isEmpty(line.qli.Remise_en_euros__c) &&
                    (line.qli.Remise_en_euros__c < 0 || line.qli.Remise_en_euros__c > line.qli.Quantity * line.qli.UnitPrice)
                ) {
                    isValid = false;
                } else if (
                    !$A.util.isEmpty(line.qli.Taux_de_TVA__c) &&
                    (line.qli.Taux_de_TVA__c < 0 || line.qli.Taux_de_TVA__c > 100)
                ) {
                    isValid = false;
                }
            }
        return isValid;
    },

    reloadAttributes: function (component, lstQLI) {
        console.log('reloadAttributes  ',lstQLI);
        if (!$A.util.isEmpty(lstQLI)) {
            var quote = component.get('v.quo');
            var isValid = true;
            var sums = { 
                sumHT: 0, 
                sumTTC: 0 , 
                sumDebourse: 0, 
                sumMarge: 0, 
                sumPercent: 0 
            };

            lstQLI.forEach((line) => {
                line.prixHt = this.calculatePrixHt(line);
                line.prixTTC = this.calculatePrixTTC(line);
			
                line.isDis = true;

                if($A.util.isEmpty(line.prixMoyen)) {
                   line.isDis = false;
            	}

                
                if (line.Sequence__c == 0){line.Sequence__c = null};
                isValid = this.checkValidity(line, quote) ? isValid : false;
                sums.sumHT += (line.prixHt < 0 && !line.isRemiseExc) ? 0 : line.prixHt; 
                sums.sumTTC += line.prixTTC ;  


            });

            
            component.set('v.sums', sums);
            this.handleChngeDeb(component,0,lstQLI);
            component.set('v.lstQLI', lstQLI);
            component.set('v.validInputs', isValid);
            var cmpEvent = component.getEvent('cmpEvent');
            cmpEvent.setParams({
                action: 'finishBtn',
                data: { valid: isValid, lstQli : lstQLI},
            });
            cmpEvent.fire();
        }
    },
    handleChngeDeb: function (component,debValue,lstQLIs) {
        console.log('# handleChngeDeb helper lstQLIs ',lstQLIs);

        var sumDebAll = 0;

        lstQLIs.forEach((line) => {
            if(isNaN(line.Debourse) || $A.util.isEmpty(line.Debourse)) {
                line.Debourse = 0; 
                if(!isNaN(line.qli.Debourse__c) || !$A.util.isEmpty(line.qli.Debourse__c)) {
                    line.Debourse = line.qli.Debourse__c;
                } 
            }
            if(!$A.util.isEmpty(line.prixMoyen)){
            	line.Debourse  = line.prixMoyen * line.qli.Quantity;
        	}
           
            sumDebAll += parseFloat(line.Debourse);
            
        });   

        var sumDeb = component.get('v.sums');

        sumDeb.sumDebourse= sumDebAll;
        sumDeb.sumMarge = sumDeb.sumHT - sumDeb.sumDebourse; 
        if(sumDeb.sumHT != 0){
            sumDeb.sumPercent = sumDeb.sumMarge / sumDeb.sumHT; 
        }      

        component.set('v.sums', sumDeb);

    },
});