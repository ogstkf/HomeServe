/**
 * @File Name          : LC07_EditPBEsController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : LGO
 * @Last Modified On   : 06-25-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    04/12/2019   RRJ     Initial Version
 * 2.0   9/10/2020    RRA                          TEC-66- Disable Remise %, Remise en €, Taux de TVA % - reference sur la page confluence 
 **/
({
    // <!-- LG0 TEC-399 07.01.21 -->
    doInit: function (component, event, helper) {
        helper.tvaValues(component);

        // {!lineItem.prodCode}lineItem.prodCode.startsWith('AIDE-')

        //set AIde products to 0
        component.set('v.TVAAide',0);
        console.log('# prix ht change');
      

    },
    handleChngeDeb: function (component, event, helper) {
        console.log('# handleChngeDeb controller');
        // var lineIndex = event.target.dataset.lineid;
        var lstQLIs = component.get('v.lstQLI');
        var value = event.getSource().get("v.value");
        if(isNaN(value))        value=0;
        console.log('# handleChngeDeb controller value ',value);
        console.log('# handleChngeDeb controller lstQLIs ',lstQLIs);
        // console.log('# handleChngeDeb controller lineIndex ',lineIndex);
        // if (lineIndex != -1) {
// 
            helper.handleChngeDeb(component,value,lstQLIs);
        // }

        
    },
        
    handleChangeTVA: function(component, event, helper){
        var lineIndex = event.target.dataset.lineid;
        var lstQLIs = component.get('v.lstQLI');
        var value = event.getSource().get("v.value");

        if (lineIndex != -1) {
            lstQLIs[lineIndex].qli.Taux_de_TVA__c = value;
            helper.reloadAttributes(component, lstQLIs);
        }
    },

    handleChangeHT: function (component, event, helper) {
        console.log('# prix ht change');
        var lineIndex = event.target.dataset.lineid;
        var lstQLIs = component.get('v.lstQLI');
        if (lineIndex != -1) {
            helper.reloadAttributes(component, lstQLIs);
        }

    },

    handleChangePrix: function (component, event, helper) {
        console.log('# prix cat change');
        var quo = component.get('v.quo');
        console.log('# quo' , quo);
        if (quo.RecordType.DeveloperName === 'Devis_standard') {
            var cmpEvent = component.getEvent('cmpEvent');
            cmpEvent.setParams({
                action: 'reloadQLI',
            });
            console.log('# cmpEvent' , cmpEvent);
            cmpEvent.fire();
        } else {
            var lineIndex = event.getSource().get('v.accesskey');
            var lstQLIs = component.get('v.lstQLI');
            if (lineIndex != -1) {
                helper.reloadAttributes(component, lstQLIs);
            }
        }

    },

   /* handleChange : function (component, event, helper) {
        var inputBox = component.find("prixCatalogue").get("v.value");
        console.log('# inputBox', inputBox);
        var x = inputBox.replace("-", '');
        component.find("prixCatalogue").set("v.value", x);
        console.log('# inputBox', inputBox);
    },*/

    handleChangePrixx: function (component, event, helper) {
        console.log('# prix cat change');
        var quo = component.get('v.quo');
        console.log('# quo' , quo);
        if (quo.RecordType.DeveloperName === 'Devis_standard') {
            var cmpEvent = component.getEvent('cmpEvent');
            cmpEvent.setParams({
                action: 'reloadQLI',
            });
            console.log('# cmpEvent' , cmpEvent);
            cmpEvent.fire();
        } else {
            var lineIndex = event.getSource().get('v.accesskey');
            var lstQLIs = component.get('v.lstQLI');
            if (lineIndex != -1) {
                helper.reloadAttributes(component, lstQLIs);
            }
        }
    },


    handleChangeQty: function (component, event, helper) {
        console.log('# qty change');
        var quo = component.get('v.quo');
        var lineIndex = event.getSource().get('v.accesskey');
        var lstQLIs = component.get('v.lstQLI');
        var lstRemiseRtc = component.get('v.lstRemiseRTC');
        if (lineIndex != -1) {
            if (quo.RecordType.DeveloperName === 'Devis_RTC') {
                console.log('### lstRemiseRtc: ', lstRemiseRtc);
                var remiseProd = lstRemiseRtc.find((remise) => {
                    return (
                        lstQLIs[lineIndex].qli.Quantity >= remise.Qmin__c &&
                        lstQLIs[lineIndex].prodCode === remise.Product_Code__c
                    );
                });
                if ($A.util.isEmpty(remiseProd)) {
                    lstQLIs[lineIndex].qli.Remise_en_euros__c = null;
                    lstQLIs[lineIndex].qli.Remise_en100__c = null;
                } else {
                    if (!$A.util.isEmpty(remiseProd.Remisevaleur__c)) {
                        lstQLIs[lineIndex].qli.Remise_en_euros__c = remiseProd.Remisevaleur__c;
                    } else if (!$A.util.isEmpty(remiseProd.Remisepourcent__c)) {
                        lstQLIs[lineIndex].qli.Remise_en100__c = remiseProd.Remisepourcent__c;
                    }
                }
            }
            helper.reloadAttributes(component, lstQLIs);
        }
    },

    handleLineChange: function (component, event, helper) {
        var lstQLIs = component.get('v.lstQLI');
        helper.reloadAttributes(component, lstQLIs);
    },

    handleReload: function (component, event, helper) {
        console.log('#### reloading qlis');
        var params = event.getParam('arguments');
        console.log('#### params', params);
        if (params) {
            var lstQLIs = params.lstQli;
            console.log('#### lstQLIs', lstQLIs);
            // RRA - 20200722 - CT-66 - disable Remise %, Remise en €, Taux de TVA % - reference sur la page confluence 
            for (var i = 0; i < lstQLIs.length; i++){
                var lstAide = lstQLIs[i].prodCode;
            // if (lstAide.substr(0,4) === "AIDE"){
                if (lstAide != undefined && lstAide.includes ('AIDE-')){
                    component.set('v.statutAide', lstQLIs[i].prodCode);
                // component.set('v.statutAide', lstAide.split("-")[1] + '-' +lstAide.split("-")[2]);
                    //console.log('#### lstAide = ', lstAide.split("-")[1] + '-' +lstAide.split("-")[2]);
                    lstQLIs[i].prodCodeVal = lstAide.replace('AIDE-','');
                }

            }
          /*  for (var i = 0; i < lstQLIs.length; i++){
                var lstAide = lstQLIs[i].prodCode;
                console.log('#### lstQLIs', lstQLIs.length);
                console.log('#### lstQLIs', lstQLIs);
                console.log('#### lstAide', lstAide);
                console.log('#### verifAideOrigine', component.get('v.verifAide'));
                if (lstAide === 'AIDE-CEE') {
                    component.set('v.verifAide', true);
                    console.log('#### verifAideOK', component.get('v.verifAide'));
                    //helper.reloadAttributes(component, lstQLIs);
                }
               else /*(lstAide.substring(0,4) !== 'AIDE')*/ { 
                 //   component.set('v.verifAide', false);
                  //  console.log('#### verifAideKO', component.get('v.verifAide'));
                    //helper.reloadAttributes(component, lstQLIs);
              //  }
            }
            helper.reloadAttributes(component, lstQLIs);           
        }
        //component.set('v.verifAide', false);
    },

    handleAdd: function (component, event, helper) {
        component.set('v.loading', true);
        var lineIndex = event.target.dataset.lineid;
        var lstQLIs = component.get('v.lstQLI');
        if (lineIndex != -1) {
            var lineToCopy = Object.assign({}, lstQLIs[lineIndex]);
            var linesToCopy = [lineToCopy];
            lineToCopy.qli.Id = null;
            if (lineToCopy.isBundle && lineToCopy.prodId != null) {
                console.log('#### to copy is bundle', lineToCopy);
                var lstOptions = lstQLIs.filter((line) => {
                    return line.isOption == true && line.parentBundleId === lineToCopy.prodId;
                });
                var lstUniqOpts = [];
                lstOptions.forEach((opt) => {
                    if (
                        !lstUniqOpts.some((uniqOpt) => {
                            return opt.prodId === uniqOpt.prodId;
                        })
                    ) {
                        lstUniqOpts.push(opt);
                    }
                });
                linesToCopy = linesToCopy.concat(lstUniqOpts);
            }
            linesToCopy.forEach((line) => {
                line.qli.Id = null;
            });
            lstQLIs.splice(lineIndex, 0, linesToCopy);
            lstQLIs = lstQLIs.flat();
            var lstQliStr = JSON.stringify(lstQLIs);
            component.set('v.lstQLI', JSON.parse(lstQliStr));
        }
        component.set('v.loading', false);
    },
});