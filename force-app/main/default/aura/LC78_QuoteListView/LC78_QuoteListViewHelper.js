({
    initData: function(component) {
        component.set("v.isLoading", true);
        var quotes = [];
        console.log(component.get("v.subject"));

        var action = component.get('c.getData');
        action.setParams({
            accId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                //Account
                var account = response.getReturnValue().acc;

                //Quote
                response.getReturnValue().lstwrpQuoType.forEach(elt => {
                    let quote = {};
                    quote.Type = elt.type;
                    for  (var j in elt.quo) {
                        quote[j] = elt.quo[j];
                    }
                    quote.Devis_etabli_le__c = this.dateFormat(quote.Devis_etabli_le__c);
                    quote.Prix_TTC__c = this.deviseFormat(quote.Prix_TTC__c);
                    quote.linkName = '/lightning/r/Quote/' + quote.Id + '/view';
                    quote.linkNumber = '/lightning/r/Quote/' + quote.Id + '/view';
                    
                    quotes.push(quote);
                });
                
                component.set("v.account", account);
                component.set("v.quotes", quotes);
                this.initDataTable(component);
                this.filter(component);
                component.set("v.isLoading", false);
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(state + ' Error message: ' + errors[0].message);
                    }
                } else {
                    console.log(state + ' Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
        
    },

    initDataTable : function (component) {
        var actions = [
            {label: 'Edit', name: 'edit'},
            {label: 'Delete', name: 'delete'}
        ];

        component.set('v.columns', [
                        {label: 'Nom du devis', fieldName: 'linkName', type: 'url', sortable: true,
                            typeAttributes: {label: { fieldName: 'Name' }, target: '_self', tooltip: { fieldName: 'Name' } }
                        },
                        {label: 'Devis établi le', fieldName: 'Devis_etabli_le__c', type: 'text', sortable: true },
                        {label: 'Prix TTC', fieldName: 'Prix_TTC__c', type: 'text', sortable: true },
                        {label: 'Etat', fieldName: 'Status', type: 'text', sortable: true },
                        {label: 'Numéro du devis', fieldName: 'linkNumber', type: 'url', sortable: true,
                            typeAttributes: {label: { fieldName: 'QuoteNumber' }, target: '_self', tooltip: { fieldName: 'QuoteNumber' } }
                        },
                        {label: 'Type', fieldName: 'Type', type: 'text', sortable: true },
                        {type: 'action', typeAttributes: { rowActions: actions }}
        ]);
    },

    initEtatPicklist: function (component) {
        var action = component.get("c.statusPicklistValue");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var etatOptions = [];
                response.getReturnValue().forEach(elt => {
                    etatOptions.push({
                        label: elt,
                        value: elt
                    });
                });
                component.set("v.etatOptions", etatOptions);
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(state + ' Error message: ' + errors[0].message);
                    }
                } else {
                    console.log(state + ' Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },

    dateFormat : function(value) {
        var dateArray = value.split('-');
        return dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0];
    },

    dateInitialValue : function(value) {
        var dateArray = value.split('/');
        return dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0];
    },

    deviseFormat : function(value) {
        return 'EUR ' + new Intl.NumberFormat('fr-FR',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(value);
    },

    editAction : function(event) {
        var quoteId = event.getParam('row').Id;
        
        var editQuoteEvt = $A.get("e.force:editRecord");
        editQuoteEvt.setParams({
            recordId: quoteId
        });
        editQuoteEvt.fire();
    },
    
    close : function(component) {
        component.set("v.quoteDel", {
            id: "",
            modalShow: false
        });
    },

    handleSort : function(component, event) {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        
        var orderBy = sortedBy;                                                                             

        if (orderBy.startsWith('link')) {
            component.get("v.columns").forEach(column => {
                if (orderBy === column.fieldName) {
                    orderBy = column.typeAttributes.label.fieldName;
                }
            });
        }
        
        var cloneData = component.get("v.quotesToDisplay").slice(0);
        cloneData.sort((this.sortBy(orderBy, sortDirection === 'asc' ? 1 : -1)));
        component.set('v.quotesToDisplay', cloneData);

        var cloneData = component.get("v.quotes").slice(0);
        cloneData.sort((this.sortBy(orderBy, sortDirection === 'asc' ? 1 : -1)));
        component.set('v.quotes', cloneData);

        component.set('v.sortDirection', sortDirection);
        component.set('v.sortedBy', sortedBy);
    },
    
    sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };
              

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },
    
    updateSize : function(component) {
        var elt = document.getElementById("length");
        console.log(elt.clientWidth + ' ' + elt.clientHeight);
        component.set("v.width", elt.clientWidth - 318);
        component.set("v.height", elt.clientHeight - 100);
    },
    
    filter : function(component) {
        
        var quotesToDisplay = [];
        var quotes = component.get("v.quotes");
        var filterValue = component.get("v.filterValue");
        quotes.forEach(elt => {
            
            if (elt.Name.includes(filterValue.Name)
                && (filterValue.StartDate === '' || new Date(this.dateInitialValue(elt.Devis_etabli_le__c)) >= new Date(filterValue.StartDate))
                && (filterValue.EndDate === '' || new Date(this.dateInitialValue(elt.Devis_etabli_le__c)) <= new Date(filterValue.EndDate)) 
                && (filterValue.Etat.length === 0 || filterValue.Etat.toString().includes(elt.Status)))
            {
                console.log('her');
                let b = false;
                if (filterValue.Type.length === 0) {
                    b = true;
                }
                else {
                    elt.Type.split(',').forEach(Type => {
                        console.log(Type);
                        console.log(filterValue.Type);
                        console.log(filterValue.Type.toString());
                        console.log(filterValue.Type.toString().includes(Type));
                        if (filterValue.Type.toString().includes(Type)) {
                            b = true;
                        }
                    });
                }
                if (b)
                    quotesToDisplay.push(elt);

            }
        });

        component.set("v.quotesToDisplay", quotesToDisplay);
        console.log(quotesToDisplay);

    }
})