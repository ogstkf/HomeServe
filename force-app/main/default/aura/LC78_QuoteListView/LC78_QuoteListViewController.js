({
    doInit : function(component, event, helper) {
        helper.initData(component);
        helper.initEtatPicklist(component);
        window.addEventListener("resize", function() {
            helper.updateSize(component)
        });
        
       component.set("v.filterValue", {
            Name: "",
            StartDate: "",
            EndDate: "",
            Etat: [],
            Type: []
        });
    },

    refresh : function(component, event, helper) {
        helper.initData(component);
        helper.initEtatPicklist(component);
        window.addEventListener("resize", function() {
            helper.updateSize(component)
        });
    },

    accountListView : function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": null,
            "listViewName": null,
            "scope": "Account"
        });
        navEvent.fire();
    },

    accountInfo : function(component, event, helper) {
        console.log(component.get("v.account").Id);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.account").Id,
            "slideDevName": "related"
        });
        navEvt.fire();
    },

    handleRowAction : function(component, event, helper) {
        var action = event.getParam('action');

        switch (action.name) {
            case 'edit':
                helper.editAction(event);
                break;
            case 'delete':
                component.set("v.quoteDel", {
                    id: event.getParam('row').Id,
                    modalShow: true
                });
                break;
        }
    },

    handleSort : function(component, event, helper) {
        helper.handleSort(component, event);
    },

    cancel : function(component, event, helper) {
        helper.close(component);
    },
    
    deleteQuote : function(component, event, helper) {
        var action = component.get("c.delQuote");
        console.log(JSON.stringify(component.get("v.quoteDel")));
        action.setParams({
            quoId: component.get("v.quoteDel").id
        });
        helper.close(component);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                var toastEvent = $A.get('e.force:showToast');
                if (toastEvent) {
                    toastEvent.setParams({
                        title: 'test',
                        type: 'success',
                        message: 'Quote "'+ response.getReturnValue() +'" was deleted'
                    });
                    toastEvent.fire();
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(state + ' Error message: ' + errors[0].message);
                    }
                } else {
                    console.log(state + ' Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },

    openFilter : function(component, event, helper) {
        component.set("v.isFilter", !component.get("v.isFilter"));
        var elt = document.getElementById("length");
        console.log(elt.clientWidth);
        console.log(elt.clientHeight);  

        component.set("v.width", elt.clientWidth - 318);
        component.set("v.height", elt.clientHeight - 100);
    },

    updateSize : function(component, event, helper) {
        var elt = document.getElementById("length");
        console.log(elt.clientWidth + ' ' + elt.clientHeight);
        component.set("v.width", elt.clientWidth - 300);
        if (elt.clientHeight > 670)
            component.set("v.height", 670);
        else
            component.set("v.height", elt.clientHeight);
        
    },

    filter : function(component, event, helper) {
        helper.filter(component);
    }
})