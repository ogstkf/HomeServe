/**
 * @File Name          : LC02_EligibilityIdClientController.js
 * @Description        :
 * @Author             : ZJO
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 05-08-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    09/07/2019, 18:16:24   RRJ     Initial Version
 **/
({
    changeHandler: function (component, event, helper) {
        // helper.validateFields(component);
        if (helper.validateFields(component)) {
            var details = component.get("v.clientDet");
            var action = component.get("c.search");
            action.setParams({
                jsonInput: JSON.stringify(details)
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    // console.log("#### lines: ", response.getReturnValue());          
                    component.set("v.lstAccLog", JSON.parse(response.getReturnValue()));
                } else if (state === "INCOMPLETE") {
                    alert("Incomplete");
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },

    forward: function (component) {
        var evt = component.getEvent("pageEvent");
        evt.setParam("pageNo", 3);
        var clientDetails = component.get("v.clientDet");
        evt.setParam("pageData", clientDetails);
        evt.fire();
    },

    // searchCampagne: function (component, event, helper) {
    //     var currentText = event.getSource().get("v.value");
    //     var resultBox = component.find('resultBox');
    //     component.set("v.Loading", true);

    //     if (currentText.length > 0) {
    //         $A.util.addClass(resultBox, 'slds-is-open');
    //     }
    //     else {
    //         $A.util.removeClass(resultBox, 'slds-is-open');
    //     }

    //     var action = component.get("c.getResults");
    //     action.setParams({
    //         "searchVal": currentText
    //     });

    //     action.setCallback(this, function (response) {
    //         var STATE = response.getState();
    //         if (STATE === "SUCCESS") {
    //             component.set("v.searchRecords", response.getReturnValue());

    //             if (component.get("v.searchRecords").length == 0) {
    //                 console.log('000000');
    //             }
    //         }
    //         else if (state === "ERROR") {
    //             var errors = response.getError();
    //             if (errors) {
    //                 if (errors[0] && errors[0].message) {
    //                     console.log("Error message: " +
    //                         errors[0].message);
    //                 }
    //             } else {
    //                 console.log("Unknown error");
    //             }
    //         }
    //         component.set("v.Loading", false);
    //     });

    //     $A.enqueueAction(action);
    // },

    // setSelectedRecord: function (component, event, helper) {
    //     var currentText = event.currentTarget.id;
    //     var resultBox = component.find('resultBox');
    //     $A.util.removeClass(resultBox, 'slds-is-open');

    //     component.set("v.selectRecordName", event.currentTarget.dataset.name);
    //     component.set("v.selectRecordId", currentText);
    //     component.find('userinput').set("v.readonly", true);
    // },

    // resetData: function (component, event, helper) {
    //     component.set("v.selectRecordName", "");
    //     component.set("v.selectRecordId", "");
    //     component.find('userinput').set("v.readonly", false);
    // }
});