/**
 * @File Name          : LC02_EligibilityIdClientHelper.js
 * @Description        :
 * @Author             : ZJO
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 03-08-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/07/2019, 16:39:48   RRJ     Initial Version
 **/
({
    validateFields: function (component) {
        var clDetails = component.get("v.clientDet");
        console.log(JSON.stringify(clDetails));
        var fldToValidate = [
            clDetails.clAdresseLogStreet,
            clDetails.clAdresseLogPostalCode,
            clDetails.clAdresseLogCity,
            clDetails.clNum,
            clDetails.clPhone,
            clDetails.clNumCompte,
            clDetails.clImm_Res,
            clDetails.clStair,
            clDetails.clFloor,
            clDetails.clDoor,
            clDetails.clNumeroESI
        ];
        if (clDetails.clSoc) {
            fldToValidate.push(clDetails.clSocName);
        } else {
            fldToValidate.push(clDetails.clName);
            fldToValidate.push(clDetails.clFirstName);
            fldToValidate.push(clDetails.clEmail);
        }
        // console.log(fldToValidate);
        var isValid = false;
        isValid = fldToValidate.some(fldValue => !$A.util.isEmpty(fldValue));
        isValid = isValid && component.find("email").checkValidity();
        console.log("### isValid: ", isValid);
        return isValid;
    }
});