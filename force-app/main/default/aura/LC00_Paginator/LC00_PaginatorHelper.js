({
    computePages: function(cmp) {
        var currentPage = cmp.get('v.currentPage');
        var totalRecords = cmp.get('v.totalRecords');
        var pageSize = cmp.get('v.pageSize');
        
        if (pageSize === 0)
            pageSize = 10;
        
        // Calculate total pages
        var totalPages = Math.ceil(totalRecords / pageSize);

        cmp.set('v.totalPages', totalPages);

        // Check whether current page is still valid
        if (currentPage <= 0) {
            cmp.set('v.currentPage', 1);
            this.emitUpdateEvent(cmp);
        }
        else if (currentPage > totalPages) {
            cmp.set('v.currentPage', totalPages)
            this.emitUpdateEvent(cmp);
        }
    },
    
    emitUpdateEvent: function(cmp) {
        var event = cmp.getEvent('onPageChange');
        
        event.setParams({
            'currentPage': cmp.get('v.currentPage'),
            'totalPages': cmp.get('v.totalPages'),
            'pageSize': cmp.get('v.pageSize')
        });
        
        event.fire();

    }
})