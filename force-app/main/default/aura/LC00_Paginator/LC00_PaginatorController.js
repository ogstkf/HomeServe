({
    recomputePages: function(cmp, event, helper) {
        helper.computePages(cmp);
    },
    
    nextPage: function(cmp, event, helper) {
        cmp.set('v.currentPage', cmp.get('v.currentPage') + 1);
        helper.emitUpdateEvent(cmp);
    },
    
    prevPage: function(cmp, event, helper) {
        cmp.set('v.currentPage', cmp.get('v.currentPage') - 1);
        helper.emitUpdateEvent(cmp);
    }
})