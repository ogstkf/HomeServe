/**
 * @File Name          : LC07_SelectPBEController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/17/2020, 2:11:29 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    03/12/2019   RRJ     Initial Version
 **/
({
    handleSearchChange: function (component, event, helper) {
        console.log('event search: ', event);
        var searchVal = event.getParam('value');
        var cmpEvent = component.getEvent('cmpEvent');
        cmpEvent.setParams({
            action: 'searchPBE',
            data: { searchText: searchVal },
        });
        cmpEvent.fire();
    },

    handleSearch: function (component, event, helper) {
        console.log('#### keypress: ', event);
        console.log('#### keypress: ', event.which);
        if (event.which == 13) {
            var searchTxt = component.get('v.searchVal');
            var cmpEvent = component.getEvent('cmpEvent');
            cmpEvent.setParams({
                action: 'searchPBE',
                data: { searchText: searchTxt },
            });
            cmpEvent.fire();
        }
    },

    clickPrev: function (component, event, helper) {
        var currPage = component.get('v.pageNum');
        helper.changePage(component, currPage, currPage - 1);
    },
    clickNext: function (component, event, helper) {
        var currPage = component.get('v.pageNum');
        helper.changePage(component, currPage, currPage + 1);
    },

    handleLstPbeCHange: function (component, event, helper) {
        // console.log('### pbe changed:');
        var lstPbe = component.get('v.lstPbe');
        helper.displayList(component, lstPbe);
    },

    handleSelected: function (component, event, helper) {
        helper.populateSelected(component);
    },

    showSelected: function (component, event, helper) {
        var lstSelected = component.get('v.lstSelected');
        component.set('v.lstToDisplay', lstSelected);
        helper.displayList(component, lstSelected);
        component.set('v.isShowSelected', true);
    },

    showResult: function (component, event, helper) {
        var lstPbe = component.get('v.lstPbe');
        helper.displayList(component, lstPbe);
        component.set('v.isShowSelected', false);
    },

    handleNav: function (component, event, helper) {
        console.log(event);
        // helper.navigateToRecord(component, event, prodId);
    },

    clearDevType: function (component, event, helper) {
        var lookupComp = component.find('customLookUp');
        lookupComp.clearRecord();
    },
});