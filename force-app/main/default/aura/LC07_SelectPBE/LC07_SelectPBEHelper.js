/**
 * @File Name          : LC07_SelectPBEHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 21/01/2020, 18:57:20
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    03/12/2019   RRJ     Initial Version
 **/
({
    changePage: function(component, originPage, targetPage) {
        var lstPbe = component.get('v.lstPbe');
        if (targetPage > originPage) {
            var lstDisplay = lstPbe.slice(originPage * 50, targetPage * 50);
        } else {
            var lstDisplay = lstPbe.slice((targetPage - 1) * 50, (originPage - 1) * 50);
        }
        component.set('v.lstToDisplay', lstDisplay);
        component.set('v.pageNum', targetPage);
    },

    displayList: function(component, list) {
        var numPages = Math.ceil(list.length / 50);
        numPages = numPages == 0 ? 1 : numPages;
        component.set('v.numPages', numPages);
        component.set('v.pageNum', 1);
        if (list.length <= 50) {
            component.set('v.lstToDisplay', list);
        } else {
            component.set('v.lstToDisplay', list.slice(0, 50));
        }
    },

    populateSelected: function(component) {
        var lstPBE = component.get('v.lstToDisplay');
        var lstSelected = component.get('v.lstSelected');
        lstPBE.forEach(pbe => {
            var index = lstSelected.findIndex(selPBE => {
                return selPBE.Id === pbe.Id;
            });
            if (pbe.isLineSelected === true && index === -1) {
                lstSelected.push(pbe);
            }
            if (pbe.isLineSelected === false && index !== -1) {
                lstSelected.splice(index, 1);
            }
        });
        component.set('v.lstSelected', lstSelected);
    },

    navigateToRecord: function(component, event, objId) {
        var navService = component.find('navService');
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                recordId: objId,
                actionName: 'view'
            }
        };
        event.preventDefault();
        navService.navigate(pageReference);
    }
});