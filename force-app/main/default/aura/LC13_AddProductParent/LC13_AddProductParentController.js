/**
 * @File Name          : LC13_AddProductParentController.js
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 29/11/2019, 15:56:19
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    29/11/2019   RRJ     Initial Version
**/
({
    init: function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");

        console.log(component.get("v.recordId"));
        evt.setParams({
            componentDef: "c:LC13_AddProduct",
            componentAttributes: {
                recordId: component.get("v.recordId")
            }
        });
        evt.fire();
    }
});