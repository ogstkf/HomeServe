/**
 * @File Name          : LC53_GenerateAvisDePassageController.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 20/04/2020, 11:26:23
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/04/2020   ZJO     Initial Version
**/
({
    doInit: function (component, event, helper) {
        component.set("v.showSpinner", true);
        helper.createFiles(component);
    }
})