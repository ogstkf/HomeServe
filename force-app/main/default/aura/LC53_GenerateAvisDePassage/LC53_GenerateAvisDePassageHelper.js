/**
 * @File Name          : LC53_GenerateAvisDePassageHelper.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 17/04/2020, 10:16:00
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/04/2020   ZJO     Initial Version
**/
({
    createFiles: function (component) {

        component.set('v.showSpinner', true);

        var servAppId = component.get('v.recordId');
        var action = component.get('c.saveAvisDePassage');
        action.setParams({ servAppId: servAppId });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('## result :', result);
                if (result.error) {

                    console.log('## result Error:', result.message);
                    var toastEvent = $A.get('e.force:showToast');
                    toastEvent.setParams({
                        title: 'ERROR',
                        type: 'error',
                        message: result.message
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get('e.force:closeQuickAction');
                    dismissActionPanel.fire();

                } else {

                    console.log('## Success');
                    var dismissActionPanel = $A.get('e.force:closeQuickAction');
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                    window.open('/' + result.ADP);

                }
            } else {
                console.log('## error obj : ', action.getError()[0]);
            }
        });
        $A.enqueueAction(action);
    }
})