/**
 * @File Name          : LC02_CreationCompteSocieteHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 02-10-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    23/07/2019, 21:18:00   RRJ     Initial Version
 **/
({
    changePage: function(component, pageNum) {
        var evt = component.getEvent("pageEvent");
        evt.setParam("pageNo", pageNum);
        var clientDetails = component.get("v.clientDet");
        evt.setParam("pageData", clientDetails);
        evt.fire();
    },

    getPickVal: function(component) {
        var action = component.get("c.getPicklists");

        var picklistFlds = JSON.stringify([
            { label: "Logement__c", value: "Professional_Use__c" },
            { label: "Logement__c", value: "Age__c" },
            { label: "Asset", value: "Equipment_type__c" },
            { label: "Account", value: "Type" },
            { label: "Contact", value: "Salutation"}
        ]);

        action.setParams({
            pickFlds: picklistFlds
        });
        component.set("v.showSpinner", true);

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log("##### pickVal: ", JSON.stringify(response.getReturnValue()));
                var pickVals = response.getReturnValue();
                var valuePairs = {
                    UsagePro: pickVals["Logement__c-Professional_Use__c"],
                    AgeLogement: pickVals["Logement__c-Age__c"],
                    TypeEquipment: pickVals["Asset-Equipment_type__c"],
                    TypeAccount: pickVals["Account-Type"],
                    Civilite: pickVals["Contact-Salutation"]
                };
                // console.log("##### valuePairs: ", valuePairs);

                component.set("v.lstPicklistValues", valuePairs);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    fetchDepPicklistValues: function(component, objDetails, controllerField, dependentField) {
        var action = component.get("c.getDepPickVals");
        action.setParams({
            sObjName: "Case",
            controllingFld: "Type",
            dependentFld: "Reason__c"
        });
        component.set("v.showSpinner", true);

        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = JSON.parse(response.getReturnValue());
                //console.log("##### StoreResponse: ", StoreResponse);

                component.set("v.depnedentFieldMap", StoreResponse);
                var clDetails = component.get("v.clientDet");

                var listOfkeys = [];
                var ControllerField = [];

                for (var i = 0; i < StoreResponse.length; i++) {
                    listOfkeys.push({
                        value: StoreResponse[i].value,
                        label: StoreResponse[i].label
                    });
                }

                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push({ label: "-- Aucun --", value: "" });
                }

                for (var i = 0; i < listOfkeys.length; i++) {
                    if (clDetails.clPresta == listOfkeys[i].value) {
                        listOfkeys[i].selected = true;
                    }
                    ControllerField.push(listOfkeys[i]);
                }
                component.set("v.typeVals", ControllerField);
                this.fetchDepValue(component);
            } else if (response.getState() === "INCOMPLETE") {
                alert("Incomplete");
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: ", errors);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    fetchDepValue: function(component) {
        var clDetails = component.get("v.clientDet");
        console.log("#### clDetails: ", JSON.stringify(clDetails));

        var controllerValueKey = clDetails.clPresta; // get selected controller field value
        var depnedentFieldMap = component.get("v.depnedentFieldMap");

        if (controllerValueKey != "") {
            var ListOfDependentFields = depnedentFieldMap.find(
                element => element.value == controllerValueKey
            ).children;
            // console.log("#### ListOfDependentFields: ", ListOfDependentFields);

            if (ListOfDependentFields.length > 0) {
                var selected = ListOfDependentFields.findIndex(
                    element => element.value == clDetails.clMotif
                );
                // console.log(selected);

                if (selected != -1) {
                    ListOfDependentFields[selected].selected = true;
                } else {
                    clDetails.clMotif = "";
                    component.set("v.clientDet", clDetails);
                }
                component.set("v.reasonVals", ListOfDependentFields);
            } else {
                component.set("v.reasonVals", []);
                clDetails.clMotif = "";
                component.set("v.clientDet", clDetails);
            }
        } else {
            component.set("v.reasonVals", []);
            clDetails.clMotif = "";
            component.set("v.clientDet", clDetails);
        }
    },

    showToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    },

    redirectDemande: function(component, caseId) {
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: caseId,
                actionName: "view"
            }
        };
        navService.navigate(pageReference, true);
    },

    populateEqpType: function(component) {
        // console.log("##### details presta", JSON.stringify(component.get("v.clientDet")));
        var clientDetails = component.get("v.clientDet");
        // console.log("##### populate: eqptype");

        if (clientDetails.clEqpType) {
            // console.log("##### populate: eqptype 2");
            var eqpTypeMapping = [
                { key: "Chaudière gaz", value: "Chaudière" },
                { key: "Radiateur gaz", value: "Radiateur" },
                { key: "Aerotherme, Radiant", value: "Chaudière" },
                { key: "Chaudière fioul", value: "Chaudière" },
                { key: "Poêle à granulés", value: "Chaudière" },
                { key: "Pompe à chaleur hybride Fioul", value: "Appareil hybride" },
                { key: "Pompe à chaleur hybride Gaz", value: "Appareil hybride" },
                { key: "Pompe à chaleur air/eau", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur eau/eau", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 1 unités", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 2 unités", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 3 unités", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 4 unités", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur piscine", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur géothermie eau/eau", value: "Pompe à chaleur" },
                { key: "Climatisation gainable faible puissance", value: "Climatisation" },
                { key: "Climatisation grosse puissance", value: "Climatisation" },
                { key: "Rafraichissement d’air réversible 1 unité", value: "Climatisation" },
                { key: "Rafraichissement d’air réversible 2 unités", value: "Climatisation" },
                { key: "Rafraichissement d’air réversible 3 unités", value: "Climatisation" },
                { key: "Rafraichissement d’air réversible 4 unités", value: "Climatisation" },
                { key: "Rafraichissement d’air réversible 5 + X unités", value: "Climatisation" },
                { key: "Rafraichissement d’air mobile", value: "Climatisation" },
                { key: "Climatisation cave à vin", value: "Climatisation" },
                { key: "Chambre froide", value: "Matériel froid" },
                { key: "Climatisation laboratoire", value: "Climatisation" },
                { key: "Vitrine", value: "Matériel froid" },
                { key: "Machine à glaçon", value: "Matériel froid" },
                { key: "Fourneau", value: "Autres" },
                { key: "Four", value: "Autres" },
                { key: "Lave vaisselle", value: "Autres" },
                { key: "Autres", value: "Autres" },
                { key: "Plancher chauffant", value: "Plancher chauffant" },
                { key: "Pompe à chaleur air/air et clim 5 + X unités", value: "Pompe à chaleur" },
                { key: "Chauffe-eau thermodynamique hermétique", value: "Chauffe eau" },
                { key: "Chauffe-eau thermodynamique avec unité extérieure", value: "Chauffe eau" },               
                { key: "Chauffe-eau solaire", value: "Chauffe eau" },
                { key: "Chauffe-eau gaz", value: "Chauffe eau" },
                { key: "Chauffe-eau électrique", value: "Chauffe eau" },
                { key: "Caisson VMC", value: "Produit VMC" },
                { key: "Caisson VMC gaz", value: "Produit VMC" },
                { key: "Bouche VMC", value: "Produit VMC" },
                { key: "Cheminée", value: "Conduit d'évacuation" },
                { key: "Conduit 3CEP", value: "Conduit d'évacuation" },
                { key: "Cuisine", value: "Autres" },
                { key: "Détecteur de fumée", value: "Autres" }
            ];

            var mappedValue = eqpTypeMapping.find(
                element => element.key == clientDetails.clEqpType
            );
            // console.log("##### populate: eqptype 3", mappedValue);
            if (!$A.util.isUndefinedOrNull(mappedValue)) {
                // console.log("##### populate: eqptype 4");
                clientDetails.clAstEqpType = mappedValue.value;
            }

            component.set("v.clientDet", clientDetails);
            // console.log("##### details presta", JSON.stringify(component.get("v.clientDet")));
        }
    },

    checkCPValidity: function(component) {
        var action = component.get("c.analyseGeo");
        var details = component.get("v.clientDet");
        action.setParams({
            jsonInput: JSON.stringify(details)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log(JSON.stringify("geo Eligible: " + response.getReturnValue()));
                component.set("v.geoData", response.getReturnValue());
                var cpElement = component.find("clCodePostal");
                if (response.getReturnValue().isEligible == "true") {
                    component.set("v.isEliGeo", true);
                    cpElement.setCustomValidity("");
                    cpElement.reportValidity();
                } else {
                    component.set("v.isEliGeo", false);
                    cpElement.setCustomValidity("le code postale n'est pas eligible");
                    cpElement.reportValidity();
                }
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    checkPrestaValidity: function(component) {
        var action = component.get("c.analysePresta");
        var details = component.get("v.clientDet");
        action.setParams({
            jsonInput: JSON.stringify(details)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("presta Eli: " + JSON.stringify(response.getReturnValue()));
                if ($a.isEmpty(response.getReturnValue())) {
                    component.set("v.isEliPresta", false);
                } else {
                    component.set("v.isEliPresta", true);
                }
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
});