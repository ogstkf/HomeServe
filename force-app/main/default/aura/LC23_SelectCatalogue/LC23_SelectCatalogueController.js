/**
 * @File Name          : LC23_SelectCatalogueController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 18/12/2019, 13:16:42
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    18/12/2019   RRJ     Initial Version
 **/
({
    handleChange: function(component, event, helper) {
        helper.setParentPbId(component);
    }
});