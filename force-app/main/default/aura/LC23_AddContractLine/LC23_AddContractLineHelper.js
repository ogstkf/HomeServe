/**
 * @File Name          : LC23_AddContractLineHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 13-12-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    18/12/2019   RRJ     Initial Version
 **/
({
    setNavOptions: function(component, canNext, canBack, canFinish) {
        var options = {
            canBack: canBack,
            canNext: canNext,
            canFinish: canFinish
        };
        component.set('v.navOptions', options);
    },

    initialize: function(component) {
        component.set('v.isLoading', true);
        var conId = component.get('v.recordId');
        var action = component.get('c.fetchContractDetails');
        action.setParams({
            srvConId: conId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('#### responce: ', response.getReturnValue());
                component.set('v.con', response.getReturnValue());
                var prcBk = null;
                console.log('*** 1 agency con:',  response.getReturnValue());
                // this.getPrcBkId(response.getReturnValue());
                // console.log('#### prcBkId: ', prcBk);
                //DMU 20201029 - Added condition fon agency__c tec-202
                var con = response.getReturnValue().con;                
                if (con.Agency__c != null){ // if ($A.util.isEmpty(prcBk)) {
                    console.log('*** in agency con');
                    var self = this;
                    this.getPb(component).then(
                        $A.getCallback(function(result) {
                            console.log('######## lstPB: ', result);
                            component.set('v.lstPB', result);
                            if (result.length == 1) {
                                component.set('v.pbk', result[0]);
                                self.loadSelectPBE(component);
                            } else {
                                self.changePage(component, 'selectCatalog');
                            }
                        })
                    );
                } else {
                    component.set('v.pbk', prcBk);
                    this.loadSelectPBE(component);
                }

                component.set("v.isContrat1ere", con.Contrat_1ere_annee__c);
            } else if (state === 'INCOMPLETE') {
                alert('Incomplete');
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },

    getPrcBkId: function(con) {
        if (!$A.util.isEmpty(con.Pricebook2Id)) {
            return { Id: con.Pricebook2Id, Name: con.Pricebook2.Name };
        } else {
            return null;
        }
    },

    getPb: function(component) {
        return new Promise(
            $A.getCallback(function(resolve, reject) {
                var action = component.get('c.fetchPb');   //DMU 20201029 - Added parametre for condition on agency__c tec-202
                action.setParams({
                    srvCon: component.get('v.con').con
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        console.log('#### response get pb: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.log('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    loadSelectPBE: function(component) {
        var lstPBE = [];
        var lstQLI = [];
        var self = this;

        component.set('v.isLoading', true);
        this.getPriceBookEntries(component)
            .then(
                $A.getCallback(function(result) {
                    lstPBE = result;
                    return self.getExistingCLI(component);
                })
            )
            .then(
                $A.getCallback(function(result) {
                    lstQLI = result;
                    component.set('v.lstExistingCLI', lstQLI);
                    var lists = self.processQLI(lstPBE, lstQLI);
                    component.set('v.lstAllPBE', lists.lstPBE);
                    component.set('v.lstSelectedPBE', lists.lstSelectedPBE);
                    self.changePage(component, 'selectProduct');
                    self.setNavOptions(component, true, false, false);
                    component.set('v.isLoading', false);
                })
            );
    },

    changePage: function(component, page) {
        var currPage = component.get('v.currentPage');
        component.set('v.currentPage', page);
        component.set('v.previousPage', currPage);
    },
    getPriceBookEntries: function(component, searchTxt) {
        return new Promise(
            $A.getCallback(function(resolve, reject) {
                var pbkId = component.get('v.pbk').Id;
                var action = component.get('c.fetchPriceBookEntries');
                var con = component.get('v.con').con;
                action.setParams({
                    prcBkId: pbkId,
                    searchText: searchTxt,
                    servCon: con
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        console.log('#### responce get Pbe: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.log('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    getExistingCLI: function(component) {
        return new Promise(
            $A.getCallback(function(resolve, reject) {
                var pbkId = component.get('v.pbk').Id;
                var conId = component.get('v.con').con.Id;
                var action = component.get('c.fetchCLI');
                action.setParams({
                    conId: conId,
                    pbkId: pbkId
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        console.log('#### responce get cli: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.log('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    processQLI: function(lstPBE, lstQLI) {
        var lstSelectedPBE = [];
        lstPBE.forEach(pbe => {
            var hasExisting = lstQLI.some(qli => {
                return qli.PricebookEntryId === pbe.Id;
            });
            if (hasExisting) {
                pbe.isExistingCLI = true;
                pbe.isLineSelected = true;
                lstSelectedPBE.push(pbe);
            }
        });
        return { lstPBE: lstPBE, lstSelectedPBE: lstSelectedPBE };
    },

    preselectPBE: function(lstPBE, lstSelected, component) {
        lstPBE.forEach(pbe => {
            var isSelected = lstSelected.some(selPbe => {
                return selPbe.Id === pbe.Id;
            });
            if (isSelected) {
                pbe.isLineSelected = true;
            }
        });
        component.set('v.lstAllPBE', lstPBE);
    },

    checkPBId: function(component) {
        var prcBk = component.get('v.pbk');
        if ($A.util.isEmpty(prcBk)) {
            this.showToast('', 'error', 'Veuillez selectionner une catalogue', 'dismissible');
        } else {
            this.loadSelectPBE(component);
        }
    },

    getListPbeId: function(lstPBE) {
        var lstId = [];
        lstPBE.forEach(pbe => {
            lstId.push(pbe.Id);
        });
        return lstId;
    },

    closeTab: function(component) {
        var workspaceAPI = component.find('workspace');
        workspaceAPI
            .getFocusedTabInfo()
            .then(function(response) {
                var focusedTabId = response.tabId;
                workspaceAPI.closeTab({ tabId: focusedTabId });
            })
            .catch(function(error) {
                console.log(error);
            });
    },

    showToast: function(title, type, message, mode) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            mode: mode,
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    },

    afterFinish: function(component, recId) {
        var workspaceAPI = component.find('workspace');
        var self = this;

        workspaceAPI.isConsoleNavigation().then((isConsole) =>{
            if(isConsole){
                workspaceAPI
                .getFocusedTabInfo()
                .then(function(response) {
                    console.log('###### tab1 ', response);
                    console.log('###### tab1 ', response.tabId);
                    var focusedTabId = response.tabId;
                    /*workspaceAPI.closeTab({ tabId: focusedTabId }).then(function() {
                        var navService = component.find('navService');
                        var pageRef = {
                            type: 'standard__recordPage',
                            attributes: {
                                recordId: recId,
                                actionName: 'view'
                            }
                        };
                        navService.generateUrl(pageRef).then(function(url) {
                            window.location.href = url;
                        });
                    });*/
                    workspaceAPI.closeTab({ tabId: focusedTabId });
                    var navService = component.find('navService');
                    var pageRef = {
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: recId,
                            actionName: 'view'
                        }
                    };
                    navService.generateUrl(pageRef).then(function(url) {
                        window.location.href = url;
                    });
                })
                .catch(function(error) {
                    console.log(error);
                });
            }else{
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/"+recId
                });
                urlEvent.fire();
            }
        }).catch(
            (e) => console.log(e)
        );
        
    },

    fetchCLI: function(component, lstId) {
        return new Promise(
            $A.getCallback(function(resolve, reject) {
                console.log('here');
                var conId = component.get('v.con').con.Id;
                console.log('here');
                var action = component.get('c.populateCLI');
                var pbId = component.get('v.pbk').Id;
                action.setParams({
                    lstPbeId: lstId,
                    conId: conId,
                    pbId: pbId
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        console.log('#### responce get cli New: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.log('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    navigateToRecord: function(component, recId) {
        var navService = component.find('navService');
        var pageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: recId,
                actionName: 'view'
            }
        };
        navService.navigate(pageRef);
    },

    buildClis: function(lstExisting, lstNew) {
        lstNew.forEach(lineItm => {
            if ($A.util.isEmpty(lineItm.cli.Discount)) {
                lineItm.cli.Discount = null;
            }
            lineItm.action = 'upsert';
        });

        var lstCLI = [];
        lstExisting.forEach(cli => {
            var toDelete = !lstNew.some(lineItm => {
                return cli.Id === lineItm.cli.Id;
            });
            if (toDelete) {
                var line = {
                    cli: cli,
                    action: 'delete'
                };
                lstCLI.push(line);
            }
        });

        return lstNew.concat(lstCLI);
    },

    //MNA TEC-890 06/12/2021 Gestion du quotidien des contrats
    buildClisCollectif: function (lstNewAssetWrapper, con) {
        var lstNewCliCol = new Array();
        lstNewAssetWrapper.forEach(function (newCliAssetWrapper) {
            newCliAssetWrapper.lstCliWrap.forEach(function (cliWrap) {
                cliWrap.cli.TVA__c = newCliAssetWrapper.tva;
                cliWrap.cli.ServiceContractId = con.con.Id;
                cliWrap.cli.ServiceContractId = con.con.Id;
                cliWrap.cli.assetId = cliWrap.assetId;
                if (cliWrap.isSelected) {
                    cliWrap.action = "upsert";
                    cliWrap.cli.Quantity = 1;
                    lstNewCliCol.push(cliWrap);
                }
                if (!cliWrap.isSelected && cliWrap.cli.Id != null) {
                    cliWrap.action = "delete";
                    lstNewCliCol.push(cliWrap);
                }
            });
        });

        return lstNewCliCol;
    },

    finish: function(component, lstNew, con, pbk, isContrat1ere) {
        return new Promise(
            $A.getCallback(function(resolve, reject) {
                var action = component.get('c.saveCLI');
                action.setParams({
                    JSONlstWrap: lstNew,
                    conWrapper: JSON.stringify(con),
                    pb: pbk,
                    isContrat1ere: isContrat1ere
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        console.log('#### responce save clis: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.log('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    //MNA TEC-890 06/12/2021 Gestion du quotidien des contrats
    fetchCliCollectif: function (component, conId, lstCli) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var action = component.get("c.buildCliCollectif");
                action.setParams({
                    conId: conId,
                    JSONlstWrap: JSON.stringify(lstCli)
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log("#### responce get collectif clis: ", response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === "INCOMPLETE") {
                        alert("Incomplete");
                    } else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    sortCLI: function(lstCli) {
        return lstCli.sort((a, b) => {
            if (a.isBundle) {
                return -1;
            } else if (a.isGuaranty) {
                return 1;
            } else {
                return 0;
            }
        });
    },
    
    //MNA 14/07/2021 TEC 710
    checkBundleAdvantage: function(component, lstCli) {
        var hasBundleAdvantage = false;
        var lstAllPBE = component.get("v.lstAllPBE");
        var mapPbeById = new Map();
        lstAllPBE.forEach(pbe =>{
            mapPbeById.set(pbe.Id, pbe);
        });

        lstCli.forEach(line => {
            line.checkBundleAdvantage = false;

            if (line.isBundle)
                if (line.cli && line.cli.PricebookEntryId && mapPbeById.has(line.cli.PricebookEntryId) && mapPbeById.get(line.cli.PricebookEntryId).Product2
                    && mapPbeById.get(line.cli.PricebookEntryId).Product2.ProductCode && mapPbeById.get(line.cli.PricebookEntryId).Product2.ProductCode.startsWith('AVANTAGE-')) {
                        hasBundleAdvantage = true;
                        line.isBundleAdvantage = true;
                }
            
        });
        console.log('******************'+ hasBundleAdvantage);
        component.set("v.hasBundleAdvantage", hasBundleAdvantage);
    },

});