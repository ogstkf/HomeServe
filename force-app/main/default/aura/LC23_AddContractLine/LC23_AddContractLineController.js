/**
 * @File Name          : LC23_AddContractLineController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 14-12-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    18/12/2019   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        helper.setNavOptions(component, true, false, false);
        helper.initialize(component);
    },

    handleComponentEvent: function(component, event, helper) {
        var action = event.getParam('action');
        if (action === 'setPB') {
            var pb = event.getParam('data');
            component.set('v.pbk', pb);
        } else if (action === 'searchPBE') {
            component.set('v.isLoading', true);
            var searchTxt = event.getParam('data').searchText;
            var lstPBE = [];
            helper.getPriceBookEntries(component, searchTxt).then(
                $A.getCallback(function(result) {
                    lstPBE = result;
                    var lstCli = component.get('v.lstExistingCLI');
                    var lists = helper.processQLI(lstPBE, lstCli);
                    var lstSelected = component.get('v.lstSelectedPBE');
                    component.set('v.lstAllPBE', lists.lstPBE);
                    helper.preselectPBE(lists.lstPBE, lstSelected, component);
                    component.set('v.isLoading', false);
                })
            );
        } else if (action == 'finishBtn') {
            var isValidForFinish = event.getParam('data').valid;
            var disableFinish = !isValidForFinish;
            component.set('v.disableFinish', disableFinish);
        }
        event.stopPropagation();
    },

    handleNext: function(component, event, helper) {
        var page = component.get('v.currentPage');
        var con = component.get("v.con");
        
        var lstPBE = component.get('v.lstSelectedPBE');
        var lstId = helper.getListPbeId(lstPBE);

        console.log('###page'+ page);
        console.log(con.type);

        if (page == 'selectCatalog') {
            component.set('v.isLoading', true);
            helper.checkPBId(component);
            helper.changePage(component, 'selectProduct');
            helper.setNavOptions(component, true, true, false);
            component.set('v.isLoading', false);
        } else if (page === 'selectProduct' && con.type != "collectif") {
            component.set('v.isLoading', true);
            helper.fetchCLI(component, lstId).then(
                $A.getCallback(function(result) {
                    var lstCli = helper.sortCLI(result);
                    console.log(JSON.parse(JSON.stringify(lstCli)));
                    component.set('v.lstNewCLI', lstCli);

                    //MNA 14/07/2021 TEC 710
                    helper.checkBundleAdvantage(component, lstCli);

                    helper.changePage(component, 'editProduct');
                    helper.setNavOptions(component, true, false, true);
                    component.set('v.isLoading', false);
                })
            );
        } else if (page === 'selectProduct' && con.type == "collectif") {
            component.set('v.isLoading', true);
            helper.fetchCLI(component, lstId).then(
                $A.getCallback(function(result) {
                    var lstCli = helper.sortCLI(result);
                    console.log(JSON.parse(JSON.stringify(lstCli)));
                    component.set('v.lstNewCLI', lstCli);
                    
                    helper.changePage(component, 'editProduct');
                    helper.setNavOptions(component, true, true, false);
                    component.set('v.isLoading', false);
                })
            );
        } else if (page === "editProduct" && con.type == "collectif") {
            console.time("answer time");
            component.set("v.isLoading", true);
            var lstCli = component.get("v.lstNewCLI");
            var selectedCli = [];
            lstCli.forEach((cli) => {
                if (cli.isOption || cli.isBundle) {
					if ($A.util.isEmpty(cli.cli.UnitPrice)) {
						// Prix de vente vide sur l'edit des produit ne doit pas etre inclut dans la colonne sur la 3 em ecran
						cli.cli.UnitPrice = cli.prixHt = -1;
					}
				} 
                selectedCli.push(cli);
            });
            component.set("v.lstNewCLICollectif", selectedCli);
            helper.fetchCliCollectif(component, con.con.Id, selectedCli).then(
                $A.getCallback(function (result) {
                    component.set("v.lstAstCollectif", result);
                    helper.changePage(component, "AffectationBdlOptCtrCollectif", "editProduct");
                    helper.setNavOptions(component, false, true, true);
                    component.set("v.isLoading", false);
                    console.timeEnd("answer time");
                })
            );
        } else {
        }
    },

    handleBack: function(component, event, helper) {
        var prevPage = component.get('v.previousPage');
        var currPage = component.get('v.currentPage');
        //helper.changePage(component, prevPage);
        /*if (currPage === 'editProduct' && prevPage === 'selectProduct' && helper.getPrcBkId === null) {
            helper.setNavOptions(component, true, true, false);
        } else {
            helper.setNavOptions(component, true, false, false);
        }*/
        if (currPage === "selectProduct" && helper.getPrcBkId === null) {
            helper.changePage(component, "selectCatalog");
            helper.setNavOptions(component, true, false, false);
        }
        else if (currPage === 'editProduct' && helper.getPrcBkId === null) {
            helper.changePage(component, "selectProduct");
            helper.setNavOptions(component, true, true, false);
        }
        else if (currPage === 'editProduct') {
            helper.changePage(component, "selectProduct");
            helper.setNavOptions(component, true, false, false);
        }
        else if (currPage == "AffectationBdlOptCtrCollectif") {
            helper.changePage(component, "editProduct");
            helper.setNavOptions(component, true, true, false);
        }
    },

    handleFinish: function(component, event, helper) {
        component.set('v.isLoading', true);
        var lstExisting = component.get('v.lstExistingCLI');
        var lstNewCli = component.get('v.lstNewCLI');
        var con = component.get('v.con');
        var pbk = component.get('v.pbk');
        var isContrat1ere = component.get("v.isContrat1ere");
        if (con.type === "collectif") {
            var lstNewAssetWrapper = component.get("v.lstAstCollectif");
            console.log("handle finish collectif");
            console.log(lstNewAssetWrapper);
            var lstNewCliCollectif = helper.buildClisCollectif(lstNewAssetWrapper, con);
            console.log("lstNewCli collectif ", lstNewCliCollectif);
            // throw new Error("Something went badly wrong!");
            helper.finish(component, JSON.stringify(lstNewCliCollectif), con, pbk).then(
                $A.getCallback(function (result) {
                    console.log("######## saved: ", result);
                    helper.afterFinish(component, result);
                    component.set("v.isLoading", false);
                }),
                $A.getCallback(function (error) {
                    helper.showToast("ERROR", "error", error[0].message, "dismissible");
                    component.set("v.isLoading", false);
                })
            );
        }
        else {
            console.log(lstNewCli);
            lstNewCli = helper.buildClis(lstExisting, lstNewCli);
            helper.finish(component, JSON.stringify(lstNewCli), con, pbk, isContrat1ere).then(
                $A.getCallback(function(result) {
                    console.log('######## saved: ', result);
                    helper.afterFinish(component, result);
                    component.set('v.isLoading', false);
                }),
                $A.getCallback(function(error) {
                    helper.showToast('ERROR', 'error', error[0].message, 'dismissible');
                    component.set('v.isLoading', false);
                })
            );
        }
    },

    handleCancel: function(component, event, helper) {
        console.log('#### isocnsole check');
        var workspaceAPI = component.find("workspace");
        console.log("Workspace: ", workspaceAPI);
        workspaceAPI.isConsoleNavigation().then(function(isConsole) {
            console.log("## isocnsole check: ");
            if(isConsole){
                console.log('isConsole');
                helper.closeTab(component);
            }else{
                console.log("notConsole");
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/"+component.get("v.recordId")
                });
                urlEvent.fire();
            }
        }).catch(function(e)
            {console.log(e)
        
        });
        
    },

    contrat1ereAction: function(component, event, helper) {
        
        var isContrat1ere = component.get("v.isContrat1ere");
        component.set("v.isContrat1ere", !isContrat1ere);
        isContrat1ere = component.get("v.isContrat1ere");

        if (isContrat1ere) {
            event.getSource().set("v.variant", "destructive");
            event.getSource().set("v.label", "Annuler contrat 1ère année");
            event.getSource().set("v.title", "Annuler contrat 1ère année");
        }
        else {
            event.getSource().set("v.variant", "Success");
            event.getSource().set("v.label", "Contrat 1ère année");
            event.getSource().set("v.title", "Contrat 1ère année");
        }
    },
    
});