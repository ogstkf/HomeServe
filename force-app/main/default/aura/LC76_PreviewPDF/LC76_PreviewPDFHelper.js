({
    createJSON : function(component, docId) {

        var action = component.get("c.createJSON");
        
        action.setParams({
            "Id": component.get("v.recordId"),
            "isPreview": true,
            "isAnonymise": true,
            "isGenerate" : false,
            "template": null,
            "documentId": docId,
            "isWatermark": false
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log(state);

            if (state === 'SUCCESS') {
                var body = {};
                

            try  {
                var toastEvent = $A.get('e.force:showToast');
                body = JSON.parse(response.getReturnValue());
                toastEvent.setParams({
                    title: 'ERROR',
                    type: 'error',
                    message: body.message
                });
                
                toastEvent.fire();
            }
            catch (error) {
                var string64 = response.getReturnValue();
                
                var blob = this.base64ToBlob(string64);

                var objectUrl = URL.createObjectURL(blob);

                var urlEvent = $A.get("e.force:navigateToURL");
                
                
                if (urlEvent) {
                    urlEvent.setParams({
                        "url": objectUrl
                    });
                    urlEvent.fire();
                }
            }
                
            }
            else {
                console.log(response.getError());
            }
            $A.get('e.force:closeQuickAction').fire();
        });
        $A.enqueueAction(action);
    },
    base64ToBlob : function(string64) {
        var byteCharacters = atob(string64);
        var byteArrays = [];
        
        for (var offset = 0; offset < byteCharacters.length; offset += 512) {
            var slice = byteCharacters.slice(offset, offset + 512);
            
            
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            
            var byteArray = new Uint8Array(byteNumbers);
            
            byteArrays.push(byteArray);
        }
        
        return new Blob(byteArrays, {type: 'application/pdf'});

    }
})