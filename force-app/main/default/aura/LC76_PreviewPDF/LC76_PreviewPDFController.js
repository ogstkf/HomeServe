({
    doInit: function(component, event, helper) {
        var timeout = Date.now();
        var updateDocIdAction = component.get("c.updateDocumentId");                   //update the documentId before calling WebService
        
        updateDocIdAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('DocumentId :' + response.getReturnValue());
                helper.createJSON(component, response.getReturnValue());                     //Call WebService
            }
            else {
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(updateDocIdAction);
        
    }
})