/**
 * @File Name          : LC07_AddProductHelper.js
 * @Description        :
 * @Author             : SBH
 * @Group              :
 * @Last Modified By   : SBH
 * @Last Modified On   : 05/11/2019, 16:00:39
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/11/2019   SBH     Initial Version
 **/
({
    fetchOrderLineItem: function(component, pricebookEntries){
		console.log('### in fetch all order line items')

		var action = component.get("c.fetchOrderLineItem");
        var orderId = component.get("v.recordId");

        console.log('##orderId: ' , orderId);
        action.setParams({
            orderId: orderId
        });

        console.log('## OrderId: ' , orderId);

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var details = response.getReturnValue();
                console.log('##details order line item@ ', details);
                console.log('## Price book entrie: ' , pricebookEntries);

                for(var i = 0; i < pricebookEntries.length; i++){
                    if(details.hasOwnProperty(pricebookEntries[i].pbe.Product2Id)){
                        console.log('Pricebook product' , pricebookEntries[i].pbe.Product2Id);
                        pricebookEntries[i].pbe.Remise100 = details[pricebookEntries[i].pbe.Product2Id].Remise100__c;
                        pricebookEntries[i].pbe.Remise = details[pricebookEntries[i].pbe.Product2Id].Remise__c;
                    }
                }
                component.set("v.oli", details);
                component.set("v.lstPricebkEntryItems", pricebookEntries);

                

            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchPicklistValues: function(component) {
        console.log("### in fetchPicklistValues 123");
        console.log("### check IseEcourComm in helper 0: ", component.get("v.isEnCourComm"));

        var lstFamily = [];
        var lstPicklistVals = component.get("v.lstPicklistVals");
        var action = component.get("c.getPicklistValues");

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                lstFamily = response.getReturnValue();

                for (var i = 0; i < lstFamily.length; i++) {
                    var item = { label: lstFamily[i], value: lstFamily[i] };
                    lstPicklistVals.push(item);
                }
                component.set("v.lstPicklistVals", lstPicklistVals);
            }
        });

        $A.enqueueAction(action);
    },

    fetchAllPriceBxEntryItems: function(component) {
        console.log("### in fetchAllPriceBxEntryItems: " + component.get("v.recordId"));
        console.log("### check IseEcourComm in helper 1: ", component.get("v.isEnCourComm"));
        console.log('## component selected pricebookentries: ', component.get("v.selPriceBkEntry") );
        component.set("v.isLoading", true);
        var lstPricebkEntryItems = [];
        var action = component.get("c.getAllPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        // var lstUpdated = [];

        // var lstOLI = [];
        // lstOLI = component.get("c.fetchOrderLineItem");
        // var selPbes = component.get("v.selPriceBkEntry"); 

        action.setParams({
            ofst: offset,
            orderId: component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                component.set("v.isLoading", false);
                lstPricebkEntryItems = response.getReturnValue();
                console.log("List of pricebook entry items: ", lstPricebkEntryItems);
                // console.log("List of order line items: ", lstOLI);

                // Ally
                // for(var i = 0; i < lstPricebkEntryItems.size(); i++){
                //     if(lstPricebkEntryItems[i].Product2Id = lstOLI[i].Product2Id){
                //         // lstPricebkEntryItems[i].Remise100 = lstOLI[i].Remise100__c;
                //         // lstPricebkEntryItems[i].Remise = lstOLI[i].Remise__c;
                //         lstUpdated.push(lstPricebkEntryItems[i]);
                //     }
                //     else{
                //         lstUpdated.push(lstPricebkEntryItems[i]);
                //     }
                // }

                console.log("##### lstUpdtPBE: ", lstPricebkEntryItems);
                this.refreshSelectedPbe(component, lstPricebkEntryItems);
                // component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);
                this.fetchOrderLineItem(component, lstPricebkEntryItems);


                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    searchPriceBkEntry: function(component) {
        console.log("### check IseEcourComm in helper 3: ", component.get("v.isEnCourComm"));

        component.set("v.isLoading", true);
        var searchText = component.find("srchbxId").get("v.value");
        var lstPricebkEntryItems = [];
        var action = component.get("c.searchPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            searchText: searchText,
            ofst: offset,
            orderId: component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                component.set("v.isLoading", false);
                lstPricebkEntryItems = response.getReturnValue();
                console.log('## lstPbeItems: ',  lstPricebkEntryItems);
                this.refreshSelectedPbe(component, lstPricebkEntryItems);

                

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    filterPriceBkEntry: function(component) {
        console.log("#### in filterPriceBkEntry");
        console.log("### check IseEcourComm in helper 4: ", component.get("v.isEnCourComm"));

        component.set("v.isLoading", true);
        var cmbxProdFam = component.find("cmbxProdFam").get("v.value");
        var lstPricebkEntryItems = [];
        var action = component.get("c.filterPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            prodFam: cmbxProdFam,
            ofst: offset,
            quoteId: component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                component.set("v.isLoading", false);
                lstPricebkEntryItems = response.getReturnValue();
                component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    searchFilterPriceBkEntry: function(component) {
        console.log("#### in searchFilterPriceBkEntry");

        component.set("v.isLoading", true);
        var searchText = component.find("srchbxId").get("v.value");
        var cmbxProdFam = component.find("cmbxProdFam").get("v.value");
        var lstPricebkEntryItems = [];
        var action = component.get("c.searchFilterPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            searchText: searchText,
            prodFam: cmbxProdFam,
            ofst: offset,
            quoteId: component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                component.set("v.isLoading", false);
                lstPricebkEntryItems = response.getReturnValue();
                component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    searchPriceBkEntryImmobilier: function(component) {
        console.log("#### in searchPriceBkEntryImmobilier");
        console.log("### check IseEcourComm in helper 6: ", component.get("v.isEnCourComm"));
        component.set("v.isLoading", true);

        var searchText = component.find("srchbxId").get("v.value");
        var cmbxProdFam = component.find("cmbxProdFam").get("v.value");

        console.log("#### in searchText: " + searchText);
        console.log("#### in cmbxProdFam: " + cmbxProdFam);

        var lstPricebkEntryItems = [];
        var action = component.get("c.searchImmobilierPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            searchText: searchText,
            prodFam: cmbxProdFam,
            ofst: offset,
            quoteId: component.get("v.recordId"),
            enCourComm: component.get("v.isEnCourComm")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                console.log("#### SUCCESS 123: ");
                component.set("v.isLoading", false);

                lstPricebkEntryItems = response.getReturnValue();
                component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    goBackOnePage: function(component) {
        var curPage = component.get("v.currentPage");
        var searchText = component.find("srchbxId").get("v.value");
        var cmbxVal = component.find("cmbxProdFam").get("v.value");

        if (curPage > 1) {
            component.set("v.currentPage", curPage - 1);

            if (searchText.length == 0 && (typeof cmbxVal == "undefined" || cmbxVal == "None")) {
                this.fetchAllPriceBxEntryItems(component);
            } else if (
                searchText.length > 0 &&
                (typeof cmbxVal == "undefined" || cmbxVal == "None")
            ) {
                this.searchPriceBkEntry(component);
            } else if (
                searchText.length == 0 &&
                (typeof cmbxVal != "undefined" && cmbxVal != "None")
            ) {
                this.filterPriceBkEntry(component);
            } else if (
                searchText.length > 0 &&
                (typeof cmbxVal != "undefined" && cmbxVal != "None")
            ) {
                this.searchFilterPriceBkEntry(component);
            }
        }
    },

    fetchQuoteDetails: function(component) {
        var action = component.get("c.fetchDetails");
        var orderId = component.get("v.recordId");

        console.log('##orderId: ' , orderId);
        action.setParams({
            orderId: orderId
        });

        console.log('## OrderId: ' , orderId);

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var details = response.getReturnValue();
                console.log('##details@ ', details);
                component.set("v.ord", details);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    checkIsFlow: function(component) {
        console.log("##### check isFlow");
        var availableActions = component.get("v.availableActions");
        console.log("#### availableActions: ", availableActions);
        if (!$A.util.isEmpty(availableActions)) {
            component.set("v.isFlow", true);
        }
    },

    prePopulateRemises: function(component, event, helper){
        console.log('## Prepopulate remises start');

        // Todo: Retrieve Remises from salesforce 
        // additional input params: Account related to the order
        // use Map (Id pbe -> Results)

        var order = component.get("v.ord");
        console.log('order:: ', order);

        var orderId = component.get("v.recordId");
        console.log('order:: ', order);

        var json = JSON.stringify(component.get('v.selPriceBkEntry'));
        console.log('JSON is  : ' , json);

        var action = component.get("c.getRemises");
        
        action.setParams({
            accountId: order.AccountId,
            json: json,
            OrderId: orderId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                console.log('## prepopulated details:  ', resp);
                if(resp.result != undefined){
                    helper.calculateUnitPrices(component, event, helper, resp.result);
                    

                }
                // component.set("v.ord", details);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);





        component.set("v.showAddProd", false);
        component.set("v.showCalcProdAmt", true);

        
        console.log('## Prepopulate remises end');
    }, 

    calculateUnitPrices: function(component, event, helper, result){
        console.log('##calculating unit prices: ', result);
       
        for(var i = 0 ; i < result.length; i++){
            
            if(result[i].UnitPrice != undefined && result[i].Remise100 != undefined){
                result[i].UnitPriceOrderItem = result[i].UnitPrice * ( (100 - result[i].Remise100 ) / 100 );
            }

            
            
        }

        console.log('## processed result: ' , result);
        component.set("v.selPriceBkEntry", result);
    },

    refreshSelectedPbe: function(component, lstPbes){
        console.log('## refreshSelectedPbes : ', lstPbes);
        console.log('## selectedPbes: ' , component.get("v.selPriceBkEntry"));
        var selPbes = component.get("v.selPriceBkEntry"); 
        
        for(var i = 0 ; i < lstPbes.length ; i++){
            console.log('lstPbe [i] ' , lstPbes[i]);
            var currentPbe = lstPbes[i];
            for(var j = 0 ; j < selPbes.length; j++){
                console.log('## selPbes vs currentPbe: ' , selPbes[j], currentPbe);
                if(currentPbe.pbe.Id == selPbes[j].Id){
                    console.log('##matching pbe Id found' );
                    lstPbes[i].selected = true;
                }
            }
        }

        console.log('## lstPbes: ' , lstPbes);

        component.set("v.lstPricebkEntryItems", lstPbes);
    },

    showAllRes: function(component, event, helper){
        console.log('showAllRes start');
        // var lstPricebookEntries = component.get("v.lstPricebkEntryItems");
        // var selPriceBkEntry = component.get("v.selPriceBkEntry");
        // component.set('v.selPriceBkEntryTemp' ,  lstPricebookEntries);

        // // loop through list of pbes to remove unselected pbes
        // for(var i = 0 ; i < lstPricebookEntries.length; i++){
            
        // }
        // // populate temp list

        component.set("v.showOnlySelected", false);
        
        console.log('showAllRes end');
    },

    showSelectedPbes: function(component, event, helper){
        console.log('showSelectedPbes start');
        component.set("v.showOnlySelected", true);
        console.log('showSelectedPbes end');
    },

});