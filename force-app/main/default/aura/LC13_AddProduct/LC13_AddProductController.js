/**
 * @File Name          : LC07_AddProductController.js
 * @Description        :
 * @Author             : SBH
 * @Group              :
 * @Last Modified By   : SBH
 * @Last Modified On   : 04/11/2019, 22:11:03
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/11/2019   SBH     Initial Version
 **/
({
    init: function(component, event, helper) {
        component.set("v.currentPage", 1);
        helper.fetchQuoteDetails(component);
        // helper.fetchPicklistValues(component);
        helper.fetchAllPriceBxEntryItems(component);
        helper.fetchOrderLineItem(component);
        
        

        console.log("### in comm");
        var x = component.find("headerId");
        console.log("### x", x);
        $A.util.removeClass(x, "slds-global-header_container");
        $A.util.addClass(x, "slds-section slds-is-open slds-scrollable_y slds-scrollable_x");
        $A.util.removeClass(component.find("h_01"), "slds-global-header");
        $A.util.removeClass(component.find("footer_Cn1"), "slds-docked-form-footer");
        helper.checkIsFlow(component);
    },

    handleCancel: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");

        navEvt.setParams({
            recordId: component.get("v.recordId")
        });

        navEvt.fire();
    },

    handleSearch: function(component, event, helper) {
        console.log("** in handleSearch");
        component.set("v.currentPage", 1);
        console.log("** in handleSearch 1");
        var searchText = component.find("srchbxId").get("v.value");
        console.log("** in handleSearch 2");
        // var cmbxVal = component.find("cmbxProdFam").get("v.value");
        console.log("** event.which: ", event.which);

        // if (event.which == 13) {
            if (searchText.length == 0) {
                helper.fetchAllPriceBxEntryItems(component);
            } else if (searchText.length > 0) {
                helper.searchPriceBkEntry(component);
            } else if (searchText.length == 0) {
                helper.filterPriceBkEntry(component);
            } else if (searchText.length > 0) {
                helper.searchFilterPriceBkEntry(component);
            }
        // }
    },

    clickPrev: function(component, event, helper) {
        var curPage = component.get("v.currentPage");
        var searchText = component.find("srchbxId").get("v.value");
        // var cmbxVal = component.find("cmbxProdFam").get("v.value");

        if (curPage > 1) {
            component.set("v.currentPage", curPage - 1);

            if (searchText.length == 0) {
                helper.fetchAllPriceBxEntryItems(component);
            } else if (searchText.length > 0) {
                helper.searchPriceBkEntry(component);
            } else if (searchText.length == 0) {
                helper.filterPriceBkEntry(component);
            } else if (searchText.length > 0) {
                helper.searchFilterPriceBkEntry(component);
            }
        }
    },

    clickNext: function(component, event, helper) {
        var curPage = component.get("v.currentPage");
        var searchText = component.find("srchbxId").get("v.value");
        // var cmbxVal = component.find("cmbxProdFam").get("v.value");

        component.set("v.currentPage", curPage + 1);

        if (searchText.length == 0) {
            helper.fetchAllPriceBxEntryItems(component);
        } else if (searchText.length > 0) {
            helper.searchPriceBkEntry(component);
        } else if (searchText.length == 0) {
            helper.filterPriceBkEntry(component);
        } else if (searchText.length > 0) {
            helper.searchFilterPriceBkEntry(component);
        }
    },

    goNext: function(component, event, helper) {
        var curPage = component.get("v.currentPage");
        console.log("*** goNext ", component.get("v.selPriceBkEntry"));
        var pricebookEntries = component.get("v.selPriceBkEntry");

        helper.prePopulateRemises(component, event, helper);

        
    },

    handleSelectedChange: function(component, event, helper) {
        var selectedPB = component.get("v.selPriceBkEntry");
        component.set("v.selPrcBkEntriesJSON", JSON.stringify(selectedPB));
    },

    toggleResultView: function(component, event, helper){
        var currentLabel = component.get('v.searchResLabel');

        if(currentLabel == 'Show selected'){
            helper.showSelectedPbes(component, event, helper);
            component.set('v.searchResLabel', "Show result");
        }else{
            helper.showAllRes(component, event, helper);
            component.set('v.searchResLabel', "Show selected");
        }
    }
});