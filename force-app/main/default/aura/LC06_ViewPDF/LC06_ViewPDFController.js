({
	Init : function(component, event, helper) {       
       console.log('## Starting  Init: ');
        var woId = component.get("v.recordId");
        var action = component.get("c.getSA");
        action.setParams({
            "woId" : woId

        });
        action.setCallback(this, function(a) { 
            var state = a.getState();

            if(component.isValid() && state === 'SUCCESS'){
                var result = a.getReturnValue();
                console.log('## results :',result);
                if(!result.error){
                    component.set("v.saID",result.serviceAppointementID);
                    console.log('## serviceAppointementID :',result.serviceAppointementID);
                 
                }else{
       				console.log('## Error');
                }
            }
            else {
                console.log('## error : ',action.getError()[0]);  

            }           
        });
        $A.enqueueAction(action);
    },
    PAC : function(component, event, helper) {
        	var url ='/apex/VFP06_AAE_PAC?id=' + component.get("v.saID");
		 window.open(url);
	},
    GAZ : function(component, event, helper) {
        	var url ='/apex/VFP06_AAE_Gaz?id=' + component.get("v.saID");
		 window.open(url);
	},
    FIOUL : function(component, event, helper) {
        	var url ='/apex/VFP06_AAE_Fioul?id=' + component.get("v.saID");
		 window.open(url);
	},
    MES : function(component, event, helper) {
        	var url ='/apex/VFP06_CR_MES?id=' + component.get("v.saID");
		 window.open(url);
	},
    INT : function(component, event, helper) {
        	var url ='/apex/VFP06_CR_Intervention?id=' + component.get("v.saID");
		 window.open(url);
	},    
})