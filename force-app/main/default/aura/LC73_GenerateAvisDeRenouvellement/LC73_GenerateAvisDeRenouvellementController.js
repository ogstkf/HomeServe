/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 08-25-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-25-2020   ZJO   Initial Version
**/
({
    doInit: function (component, event, helper) {
        component.set("v.showSpinner", true);
        helper.createAvisDeRenFiles(component);
    }
})