({
    doInit : function(component, event, helper) {
        component.set("v.typeOptions", [
            {'label': 'Payeur', 'value': 'Payeur'},
            {'label': 'Propriétaire', 'value': 'Propriétaire'},
            {'label': 'Occupant', 'value': 'Occupant'}
        ]);
        
        console.log(JSON.parse(JSON.stringify(component.get("v.filterValue"))));
    },
    
    hideFilter : function(component, event, helper) {
        component.set("v.isFilter", false);
    },

    handleChange : function(component, event, helper) {
        var value = event.getParam('value');
        var name = event.getSource().get("v.name");
        var filterValue = component.get("v.filterValue");
        
        if (name === 'Etat') {
            filterValue.Etat = value;
        }
        else if (name === 'Type') {
            filterValue.Type = value;
        }
        component.set("v.filterValue", filterValue);
        console.log(JSON.parse(JSON.stringify(component.get("v.filterValue"))));
    },

    handleClick : function(component, event, helper) {
        $A.get("e.c:LE78_QuoteCustomCard").fire();
        component.set("v.isFilter", false);
    },
    
    clearFilter : function(component, event, helper) {
        component.set("v.filterValue", {
             Name: "",
             StartDate: "",
             EndDate: "",
             Etat: [],
             Type: []
         });
    },
})