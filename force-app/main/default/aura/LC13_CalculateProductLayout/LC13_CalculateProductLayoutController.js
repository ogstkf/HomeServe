/**
 * @File Name          : LC07_CalculateProductLayoutController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 05/11/2019, 15:42:28
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    04/11/2019   RRJ     Initial Version
 **/
({
    handleLinkClick: function(component, event, helper) {
        window.open("/" + component.get("v.pbe").Id);
        window.open("/" + component.get("v.ord").Id);
    },

    init: function (component, event, helper){
      helper.calculateInitialUnitPrice(component, event, helper);
    },

    onRemiseChanged: function(component, event, helper){
      
      var newRemise = component.get('v.pbe.Remise');
      
      console.log('remise 100: ' , newRemise);

      if(newRemise != undefined || newRemise != null){
        console.log('remise not null');
        component.set('v.pbe.Remise100', null);
        helper.reCalculateUnitPrice(component, event, helper);
      }else{
        console.log('remise null');
      }

    },

    onRemiseChanged100: function(component, event, helper){
      console.log('## on remise changed 100 start');
      var newRemise100 = component.get('v.pbe.Remise100');
      console.log('remise 100: ' , newRemise100);
      if(newRemise100 != undefined || newRemise100 != null){
        console.log('remise 100 not null');
        component.set('v.pbe.Remise', null);
        helper.reCalculateUnitPrice(component, event, helper);
      }else{
        console.log('remise 100 null');
      }
      console.log('## on remise changed 100 end');
    },

    onUnitPriceChanged: function(component, event, helper){
      console.log('## on unit price changed start');
      component.set('v.pbe.Remise100', null);
      component.set('v.pbe.Remise', null);
      console.log('## on unit price changed End');
    },

  });