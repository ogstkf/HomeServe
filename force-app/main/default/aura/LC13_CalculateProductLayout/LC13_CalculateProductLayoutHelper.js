({
	reCalculateUnitPrice: function(component, event, helper){
		console.log('## recalculating unitprice start');
  
		var remise = component.get('v.pbe.Remise') != undefined ? parseFloat(component.get('v.pbe.Remise')) : undefined;
		var remise100 = component.get('v.pbe.Remise100') != undefined ? parseFloat(component.get('v.pbe.Remise100')) : undefined;
		var ecoContrib = component.get("v.pbe.Eco_contribution") != undefined ? parseFloat(component.get('v.pbe.Eco_contribution')) : undefined;
		var listPrice = component.get("v.pbe.UnitPrice") != undefined ? parseFloat(component.get('v.pbe.UnitPrice')) : undefined;
		

		console.log(remise, remise100, ecoContrib, listPrice);

		var unitPrice;

		if(remise != undefined){
		  if(ecoContrib != undefined){
			unitPrice = (listPrice - remise) + ecoContrib;
		  }else{
			unitPrice = (listPrice - remise);
		  }
		}else if(remise100 != undefined){
		  if(ecoContrib != undefined){
			unitPrice = (listPrice  * ((100 - remise100) / 100) ) + ecoContrib;
		  }else{
			unitPrice = (listPrice  * ((100 - remise100) / 100) );
		  }
		}else{

		}
  
		component.set("v.pbe.UnitPriceOrderItem", unitPrice);
		console.log('## recalculating unitprice end ');
	},

	calculateInitialUnitPrice: function(component, event, helper){
		console.log('## initial unitprice start');
  
		var remise = component.get('v.pbe.Remise') != undefined ? parseFloat(component.get('v.pbe.Remise')) : undefined;
		var remise100 = component.get('v.pbe.Remise100') != undefined ? parseFloat(component.get('v.pbe.Remise100')) : undefined;
		var ecoContrib = component.get("v.pbe.Eco_contribution") != undefined ? parseFloat(component.get('v.pbe.Eco_contribution')) : undefined;
		var listPrice = component.get("v.pbe.UnitPrice") != undefined ? parseFloat(component.get('v.pbe.UnitPrice')) : undefined;

		console.log(remise, remise100, ecoContrib, listPrice);

		var unitPrice;

		if(remise != undefined){
		  if(ecoContrib != undefined){
			unitPrice = (listPrice - remise) + ecoContrib;
		  }else{
			unitPrice = (listPrice - remise);
		  }
		}else if(remise100 != undefined){
		  if(ecoContrib != undefined){
			unitPrice = (listPrice  * ((100 - remise100) / 100) ) + ecoContrib;
		  }else{
			unitPrice = (listPrice  * ((100 - remise100) / 100) );
		  }
		}else{
			unitPrice = listPrice;
		}
  
		component.set("v.pbe.UnitPriceOrderItem", unitPrice);
		console.log('## initial unitprice end ');
	}
})