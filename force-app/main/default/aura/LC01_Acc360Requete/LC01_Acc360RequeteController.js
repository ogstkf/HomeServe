/**
 * @File Name          : LC01_Acc360RequeteController.js
 * @Description        :
 * @Author             : DMU
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 23/07/2019, 21:16:13
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    25/06/2019, 11:00:00   DMU     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        helper.populateData(component, component.get("v.recordId"));
        helper.getAcc(component, component.get("v.recordId"));
    },

    newRequete: function(component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        var accDetails = component.get("v.accDet");
        // console.log("##### : accDetails: ", accDetails);
        createRecordEvent.setParams({
            entityApiName: "Case",
            defaultFieldValues: {
                AccountId: accDetails.Id,
                ContactId: accDetails.PersonContactId
            }
        });

        createRecordEvent.fire();
    }
});