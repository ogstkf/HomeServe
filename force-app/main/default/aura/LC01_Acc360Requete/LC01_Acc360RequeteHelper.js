/**
 * @File Name          : LC01_Acc360RequeteHelper.js
 * @Description        :
 * @Author             : DMU
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 11/12/2019, 11:07:42
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    25/06/2019, 11:00:00   DMU     Initial Version
 **/
({
    populateData: function(component, accId) {
        var action = component.get("c.goToCase");

        action.setParams({
            accId: accId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                var lstCa = this.processCases(resp);
                component.set("v.lstCase", lstCa);
                component.set("v.loading", false);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        this.showToast("ERROR", "error", errors[0].message);
                        component.set("v.loading", false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getAcc: function(component, accId) {
        var action = component.get("c.getAcountDetails");
        action.setParams({
            accId: accId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.accDet", response.getReturnValue());
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    processCases: function(lstCases) {
        lstCases.forEach(caseWrap => {
            if (caseWrap.numWO == 1 && !$A.util.isEmpty(caseWrap.cse.WorkOrders)) {
                caseWrap.woId = caseWrap.cse.WorkOrders[0].Id;
            }
        });
        return lstCases;
    }
});