({
    createFiles: function (component) {

        component.set('v.showSpinner', true);

        var servConId = component.get('v.recordId');
        console.log('## sc :', servConId);
        var action = component.get('c.saveContratPDF');
        action.setParams({ servConId: servConId });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('## result :', result);
                if (result.error) {

                    console.log('## result Error:', result.message);
                    var toastEvent = $A.get('e.force:showToast');
                    toastEvent.setParams({
                        title: 'ERROR',
                        type: 'error',
                        message: result.message
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get('e.force:closeQuickAction');
                    dismissActionPanel.fire();

                } else {

                    console.log('## Success');
                    var dismissActionPanel = $A.get('e.force:closeQuickAction');
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                    window.open('/' + result.ADP);

                }
            } else {
                console.log('## error obj : ', action.getError()[0]);
            }
        });
        $A.enqueueAction(action);
    }
})