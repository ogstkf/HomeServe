({
    doInit : function(component, event, helper) {
        var action = component.get("c.revokeMandat");

        action.setParams({"CBId" : component.get("v.recordId")});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var toastEvent = $A.get("e.force:showToast");
                var returnValue = response.getReturnValue();
                toastEvent.setParams({
                    "title": returnValue.title,
                    "message": returnValue.message,
                    "type": returnValue.type
                });
                toastEvent.fire();
            }
            else {
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()));
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": title,
                    "message": message,
                    "type": type
                });
                toastEvent.fire();
            }
            $A.get('e.force:closeQuickAction').fire();
        });

        $A.enqueueAction(action);
    }
})
