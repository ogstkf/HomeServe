({
    doInit : function(component, event, helper) {
        component.set("v.isLoading", true);
        var myPageRef = component.get("v.pageReference");
        component.set("v.listView", myPageRef.state.c__listView);
        helper.initColumns(component, JSON.parse(myPageRef.state.c__columns));
        helper.initData(component, myPageRef.state.c__query);                
    },

    updateSelected : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set('v.selectedRowsCount', selectedRows.length);
    },

    generateAP : function(component, event, helper) {
        component.set("v.isLoading", true);
        var evt = $A.get("e.force:navigateToComponent");
        var SAIds = [];
        var lines = component.find('SATable').getSelectedRows();

        if (lines.length === 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error",
                type: "error",
                message: "Vous devez selectionner au moins une ligne"
            });
            toastEvent.fire();
        }
        else {
            lines.forEach(line => {
                SAIds.push(line.Id);
            });
            evt.setParams({
                componentDef: "c:LC76GenerateMassAvisDePassage",
                componentAttributes: {
                    listofSA: SAIds
                }
            });
            evt.fire();
        }
        component.set("v.isLoading", false);
    },

    handleSort : function(component, event, helper) {
        helper.handleSort(component, event);
    }
})