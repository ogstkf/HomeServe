({
    initColumns : function(component, pgRefColumns) {
        var columns = [];
        pgRefColumns.forEach(col => {
            let column = {
                "label": this.value(col.label),
                "type": col.type,
                "fieldName": col.fieldNameOrPath,
                "sortable": true
            };
            if (col.type === 'date') {
                column.typeAttributes = {
                    year: "numeric",
                    month: "long",
                    day: "2-digit",
                    hour: "2-digit",
                    minute: "2-digit"
                };
            }
            columns.push(column);
        });
        component.set("v.columns", columns);
    },

    initData : function(component, pgRefQuery) {
        var action = component.get("c.getData");
        action.setParams({
            "query": pgRefQuery
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if (state === 'SUCCESS') {
                let data = response.getReturnValue();
                data.forEach(line => {                          //Used for related object ex: Account.Name
                    for (let i in line) {
                        if (line[i] !== null && typeof(line[i]) === 'object' && !Array.isArray(line[i])) 
                            for (var j in line[i]) {
                                line[i+'.'+j] = line[i][j];
                            }
                    }
                });
                component.set("v.data", data);
                component.set("v.dataToDisplay", data);
            }
            else {
                console.log(response.getError());
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },
    
    handleSort : function(component, event) {
        var sortedBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        
        var orderBy = sortedBy;

        var cloneData = component.get("v.data").slice(0);
        cloneData.sort((this.sortBy(orderBy, sortDirection === 'asc' ? 1 : -1)));

        component.set('v.data', cloneData);
        component.set('v.sortDirection', sortDirection);
        component.set('v.sortedBy', sortedBy);
    },
    
    sortBy: function(field, reverse, primer) {
        var key = primer
            ? function(x) {
                  return primer(x[field]);
              }
            : function(x) {
                  return x[field];
              };
              

        return function(a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },

    value: function(label) {
        return label.replace("789", "'");
    }
})