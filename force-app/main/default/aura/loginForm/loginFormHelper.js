({
    handleLogin: function (component, event, helpler) {
        const username = component.find("username").get("v.value");
        const password = component.find("password").get("v.value");
        let action = component.get("c.login");
        let startUrl = component.get("v.startUrl");
        
        let elUsername = component.find('username');
        let elPassword = component.find('password');
        const assetType = component.get('v.assetType');
        if(username && password){
            this.displayMessageElement(elUsername, "");
            this.displayMessageElement(elPassword, "");
            
            startUrl = decodeURIComponent(startUrl);
            startUrl = startUrl+'?acctype='+component.get("v.acctype") + '&type=' + assetType;
            
            action.setParams({
                username:username,
                password:password,
                startUrl:startUrl,
                cryptedPassword: false
            });
            action.setCallback(this, function(a){
                var rtnValue = a.getReturnValue();
                if (rtnValue !== null) {
                    component.set("v.errorMessage",rtnValue);
                    component.set("v.showError",true);
                }
            });
            
            $A.enqueueAction(action);
        }else{
            // Si username vide
            this.checkIfEmpty(username, elUsername, $A.get("$Label.c.FillThisField"));
            // Si mot de passe vide
            this.checkIfEmpty(password, elPassword, $A.get("$Label.c.FillThisField"));
        }
    },
    
    getIsUsernamePasswordEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsUsernamePasswordEnabled");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isUsernamePasswordEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    checkIfEmpty : function (value, element, messageToDisplay) {
        if(!value){
            this.displayMessageElement(element, $A.get(messageToDisplay));
        }else{
            this.displayMessageElement(element, "");
        }
    },
    
    displayMessageElement : function (element, messageToDisplay) {
        element.setCustomValidity(messageToDisplay);
        element.reportValidity();
    }
})