({
    initialize: function(component, event, helper) {
        component.set("v.startUrl", $A.get("$Label.c.CallBackRegister"));        
    },
    
    handleLogin: function (component, event, helpler) {
        var urlParams = window.location.search;
        if(urlParams.includes('particulier')){
            component.set("v.acctype","particulier");
        }else{
            component.set("v.acctype","professionnel");
        }            
        helpler.handleLogin(component, event, helpler);
    }
})