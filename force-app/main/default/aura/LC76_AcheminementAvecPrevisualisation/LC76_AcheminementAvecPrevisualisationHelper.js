({
    acheminer : function(component) {
        var servConId = component.get('v.recordId');
        var action = component.get('c.acheminer');
        action.setParams({ 
            "targetId": servConId,
            "transactId": null
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            var body = JSON.parse(response.getReturnValue());
            var titleToast = 'ERROR';
            var typeToast = 'error';
            if (body.statusCode >= 200 && body.statusCode < 300) {
                titleToast = 'SUCCESS';
                typeToast = 'success';
            }
            
            if (state === 'SUCCESS') {
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    title: titleToast,
                    type: typeToast,
                    message: body.message
                });
                toastEvent.fire();
            }
            else {
                console.log(response.getError()[0].message);
            }
            $A.get('e.force:closeQuickAction').fire();
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    }
})