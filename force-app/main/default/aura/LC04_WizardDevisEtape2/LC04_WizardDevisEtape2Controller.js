/**
 * @File Name          : LC04_WizardDevisEtape2Controller.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 24/10/2019, 18:00:42
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    24/10/2019   RRJ     Initial Version
 **/
({
    handleLoad: function(cmp, event, helper) {
        cmp.set("v.showSpinner", false);
    },

    handleSubmit: function(cmp, event, helper) {
        cmp.set("v.disabled", true);
        cmp.set("v.showSpinner", true);
    },

    handleError: function(cmp, event, helper) {
        // errors are handled by lightning:inputField and lightning:messages
        // so this just hides the spinner
        cmp.set("v.showSpinner", false);
    },

    handleSuccess: function(cmp, event, helper) {
        var params = event.getParams();
        cmp.set("v.recordId", params.response.id);
        cmp.set("v.showSpinner", false);
        cmp.set("v.saved", true);
    },

    init: function(cmp, event, helper) {
        // Figure out which buttons to display
        var availableActions = cmp.get("v.availableActions");
        for (var i = 0; i < availableActions.length; i++) {
            if (availableActions[i] == "PAUSE") {
                cmp.set("v.canPause", true);
            } else if (availableActions[i] == "BACK") {
                cmp.set("v.canBack", true);
            } else if (availableActions[i] == "NEXT") {
                cmp.set("v.canNext", true);
            } else if (availableActions[i] == "FINISH") {
                cmp.set("v.canFinish", true);
            }
        }
    },

    onButtonPressed: function(cmp, event, helper) {
        // Figure out which action was called
        var actionClicked = event.getSource().getLocalId();
        // Fire that action
        var navigate = cmp.get("v.navigateFlow");
        navigate(actionClicked);
    }
});