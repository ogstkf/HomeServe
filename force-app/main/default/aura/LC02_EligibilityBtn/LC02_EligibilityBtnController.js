/**
 * @File Name          : LC02_EligibilityBtnController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 09/07/2019, 14:30:46
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    08/07/2019, 15:41:16   RRJ     Initial Version
 **/
({
    openEligibilityWiz: function(cmp, event, helper) {
        var navService = cmp.find("navService");
        var pageReference = {
            type: "standard__component",
            attributes: {
                componentName: "c__LC02_EligibilityWizard"
            }
        };
        event.preventDefault();
        navService.navigate(pageReference);
    }
});