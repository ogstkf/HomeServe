/**
 * @File Name          : LC23_AddContractLineActionController.js
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 02-19-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/12/2019   RRJ     Initial Version
**/
({
    init: function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        
        console.log(component.get("v.recordId"));
        evt.setParams({
            componentDef: "c:LC23_AddContractLine",
            componentAttributes: {
                recordId: component.get("v.recordId")
            }
        });
        evt.fire();
    }
});