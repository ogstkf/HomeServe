({
    doInit : function(component, event, helper) {
        console.log("#### starting doInit ");

        var quoId = component.get("v.recordId");
        var action = component.get("c.fetchQuoDetails");
        action.setParams({
            quoId: quoId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("#### responce: ", response.getReturnValue());

                var qu = response.getReturnValue();
                console.log("##### qu: ", qu.Status);

                if ((qu.Status == "Validé, signé - en attente d'intervention") || (qu.Status == "Validé,signé mais abandonné") || (qu.Status == "Validé, signé et terminé") || (qu.Status == "Expired")) {
                    component.set("v.quoteVS", true);
                    component.set("v.isModalOpen", true);
                    console.log("Status: ", component.get("v.quoteVS"));
                    console.log("Model: ", component.get("v.isModalOpen"));
                    helper.openModel(component);
                    component.set("v.showSpinner",false);
                }
                else{
                    helper.initialize(component);
                }

                

            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            // component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
        // console.log('Starting LC53_GenerateDevis doInit');
        // component.set("v.showSpinner",true);

        // var quoteId = component.get("v.recordId");
        // console.log("quoteid", component.get("v.recordId"));
        // var action = component.get("c.insertImageAttachment"); 
        // action.setParams({"quoteId" : quoteId});

        // action.setCallback(this, function(response) {
        //     var state = response.getState();
        //     if (state === "SUCCESS") {  
        //         var result = response.getReturnValue();            
        //         console.log("## result :" ,result);
        //         helper.createDevis(component);
        //     }
        //     else {
        //         console.log('## error obj : ',action.getError()[0]);
        //     }          
        // });
        // $A.enqueueAction(action);   
    },



    closeModel: function(component, event, helper) {
        console.log("In 2");
        // Set isModalOpen attribute to false
        component.set("v.isModalOpen", false);
    },

})