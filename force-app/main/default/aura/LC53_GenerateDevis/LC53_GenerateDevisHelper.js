({
    initialize: function(component) {
        console.log('Starting LC53_GenerateDevis doInit');
        component.set("v.showSpinner",true);

        var quoteId = component.get("v.recordId");
        console.log("quoteid", component.get("v.recordId"));
        var action = component.get("c.insertImageAttachment"); 
        action.setParams({"quoteId" : quoteId});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                var result = response.getReturnValue();            
                console.log("## result :" ,result);
                this.createDevis(component);
            }
            else {
                console.log('## error obj : ',action.getError()[0]);
            }          
        });
        $A.enqueueAction(action);
    },
    openModel: function(component) {
        console.log("In");
        // Set isModalOpen attribute to true
        component.set("v.isModalOpen", true);
    },

    createDevis : function(component) {
        console.log('Starting LC53_GenerateDevis doInit');
        component.set("v.showSpinner",true);

        var quoteId = component.get("v.recordId");
        var action = component.get("c.saveDevis"); 
        action.setParams({"quoteId" : quoteId, "anonymise" : component.get("v.anonymise")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                var result = response.getReturnValue();            
                console.log("## result :" ,result);

                if(result.error){
                    console.log("## result Error:" ,result.message);

                }else{
                    console.log("## Success");
                    this.closeModal();
                    var url ='/'+result.quoteNumber;
                    window.open(url);
                }
            }
            else {
                console.log('## error obj : ',action.getError()[0]);
            }          
        });
        $A.enqueueAction(action);   
    },
    
    
    closeModal : function() {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
    }
})