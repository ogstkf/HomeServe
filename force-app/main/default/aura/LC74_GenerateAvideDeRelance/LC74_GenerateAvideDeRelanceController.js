/*
 * @File Name          : LC74_GenerateAvideDeRelanceController.js
 * @Description        : 
 * @Author             : RRA
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 26/08/2020, 10:00:00
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    26/08/2019, 10:00:00   RRA     Initial Version */

({
    doInit: function (component, event, helper) {
        component.set("v.showSpinner", true);
        helper.createPDFAvisRelance(component);
    }
})