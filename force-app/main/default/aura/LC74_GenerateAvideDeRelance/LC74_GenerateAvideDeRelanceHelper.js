/*
 * @File Name          : LC74_GenerateAvideDeRelanceHelper.js
 * @Description        : 
 * @Author             : RRA
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 26/08/2020, 10:00:00
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    26/08/2019, 10:00:00   RRA     Initial Version --> */
({
    createPDFAvisRelance: function (component) {
        component.set('v.showSpinner', true);

        var servConId = component.get('v.recordId');
        console.log('## servConId :', servConId);
        var action = component.get('c.saveAvisRelance');
        action.setParams({ servConId: servConId });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('## state :', state);
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('## result :', result);
                if (result.error) {

                    console.log('## result Error:', result.message);
                    var toastEvent = $A.get('e.force:showToast');
                    toastEvent.setParams({
                        title: 'ERROR',
                        type: 'error',
                        
                        message: result.message
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get('e.force:closeQuickAction');
                    dismissActionPanel.fire();

                } else {

                    console.log('## Success');
                    var dismissActionPanel = $A.get('e.force:closeQuickAction');
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                    window.open('/' + result.AvisDeRelance);

                }
            } else {
                console.log('## error obj : ', action.getError()[0]);
            }
        });
        $A.enqueueAction(action);

    }
})