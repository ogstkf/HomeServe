({
    initialize: function(component, event, helper) {

        var urlParams = window.location.search;
        if(urlParams.includes('particulier')){
            component.set("v.accType","particulier");
        }else{
            component.set("v.accType","professionnel");
        }              

        const type = helper.getParameterByName('type');
        
        component.set('v.assetType', type);
        
        var action = component.get("c.isUserConnected")
        action.setCallback(this, function(response){
            var rtnValue = response.getReturnValue();
            if(!!rtnValue) {
                //User is connected
                // window.location.href = '/s/' + $A.get("$Label.c.URLHousingInformations");
                window.location.href = '/s/' + $A.get("$Label.c.URLHousingInformations")+'?acctype='+component.get("v.accType") + '&type=' + type;   
            }
        });
        $A.enqueueAction(action);
        
    },  
    handleSelfRegister: function (component, event, helpler) {
        helpler.handleSelfRegister(component, event, helpler);
    },
    captchaOK : function(component, event, helper) {
        component.set("v.captchaError", false);
    },
    handlePaste: function(component, event, helper) {
    	event.preventDefault(); 
	}
})