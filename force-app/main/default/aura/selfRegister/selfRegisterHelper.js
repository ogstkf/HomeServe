({
    handleSelfRegister: function (component, event, helper) {

        var captchaToken = component.get("v.captchaToken");
        var accountId = component.get("v.accountId");
        var regConfirmUrl = component.get("v.regConfirmUrl");
        var salutation = component.get("v.salutationResult");
        var firstname = component.find("firstname").get("v.value");
        var lastname = component.find("lastname").get("v.value");
        var email = component.find("email").get("v.value");
        var confirmEmail = component.find("confirmEmail").get("v.value");
        var password = component.find("password").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value");        
        var accType = component.get("v.accType");
        
        if(helper.validateForm(helper, component)) {
            var action = component.get("c.selfRegister");    
            var type = component.get('v.assetType');
            var startUrl = $A.get("$Label.c.CallBackRegister")+'?acctype='+component.get("v.accType") + '&type=' + type;
            action.setParams({
                salutation: salutation,
                firstname: firstname,
                lastname: lastname,
                email: email,
                confirmEmail: confirmEmail,
                password: password,
                confirmPassword: confirmPassword,
                startUrl:startUrl,
                token: captchaToken,
                accType: accType
            });
              
            action.setCallback(this, function(response){
                var rtnValue = response.getReturnValue();
                if (rtnValue !== null) {
                    if(rtnValue.includes("robot")) {
                        component.set("v.captchaError", true);
                    }
                    else if(rtnValue.includes("captcha")) {
                        var captcha = component.find("captcha");
        				captcha.reload();
                        component.set("v.captchaError", true);
                    }
                    else if(rtnValue.includes("password")) {
                        component.set("v.passwordError", true);
                    }
                    else if(rtnValue.includes("utilisateur existe")) {
                        this.displayErrorOnComponent(component.find("email"), "L'utilisateur existe déjà");
                        this.displayErrorOnComponent(component.find("confirmEmail"), "L'utilisateur existe déjà");
                        const openConnectionBox = component.get("v.openConnectionBox")
                        if (openConnectionBox) {
                            console.log("openConnectionBox");
                            $A.enqueueAction(openConnectionBox);
                        }
                    }
					else {
                        component.set("v.errorMessage",rtnValue);
                        component.set("v.showError",true);
					}
                }   
            });
            
            $A.enqueueAction(action);
        }
    },
    validateForm: function(helper, component) {
        const captchaToken = component.get("v.captchaToken");
        const accountId = component.get("v.accountId");
        const regConfirmUrl = component.get("v.regConfirmUrl");
        const salutation = component.get("v.salutationResult");
        const firstname = component.find("firstname").get("v.value");
        const lastname = component.find("lastname").get("v.value");
        const email = component.find("email").get("v.value");
        const confirmEmail = component.find("confirmEmail").get("v.value");
        const password = component.find("password").get("v.value");
        const confirmPassword = component.find("confirmPassword").get("v.value");        

        console.log('** salutation validateForm: ', salutation);
		
        var everythingOK = true;        
        everythingOK &= helper.verifyCaptcha(helper, captchaToken, component);        
        if(component.get("v.accType") == 'particulier'){
            everythingOK &= helper.verifyValue(helper, salutation, component.find("salutation"), $A.get("$Label.c.SalutationRequired"));
            everythingOK &= helper.verifyValue(helper, firstname, component.find("firstname"), $A.get("$Label.c.FirstNameRequired"));
            everythingOK &= helper.verifyValue(helper, lastname, component.find("lastname"), $A.get("$Label.c.LastNameRequired"));
            everythingOK &= helper.verifyValue(helper, email, component.find("email"), $A.get("$Label.c.EmailRequired"));
            everythingOK &= helper.verifyValue(helper, confirmEmail, component.find("confirmEmail"), $A.get("$Label.c.ConfirmationEmailRequired"));
            everythingOK &= helper.verifySameValue(helper, email, confirmEmail, component.find("email"), component.find("confirmEmail"), $A.get("$Label.c.MailsNotEquals"));
            everythingOK &= helper.validateEmail(helper, email, component);
            everythingOK &= helper.verifyPassword(helper, component);  
        }   
        else{
            everythingOK &= helper.validateSIRET(helper, component, component.find("firstname").get("v.value").length);            
            everythingOK &= helper.verifyValueProf(helper, lastname, component.find("lastname"), $A.get("$Label.c.LastNameRequired"));
            everythingOK &= helper.verifyValueProf(helper, email, component.find("email"), $A.get("$Label.c.EmailRequired"));
            everythingOK &= helper.verifyValueProf(helper, confirmEmail, component.find("confirmEmail"), $A.get("$Label.c.ConfirmationEmailRequired"));
            everythingOK &= helper.verifySameValue(helper, email, confirmEmail, component.find("email"), component.find("confirmEmail"), $A.get("$Label.c.MailsNotEquals"));
            everythingOK &= helper.validateEmail(helper, email, component);
            everythingOK &= helper.verifyPassword(helper, component);
        }
        
        console.log('** salutation 4 ', everythingOK);      

        return everythingOK;
    },
    verifyCaptcha: function(helper, captchaToken, component) {
        if (!captchaToken) {
                component.set("v.captchaError", true);
            	return false;
        } 
        return true;
    },
    verifyValue: function(helper, value, element, errorMessage) {        
        if (!value) {
            helper.displayErrorOnComponent(element, errorMessage);
            return false;
        } else {
            helper.displayErrorOnComponent(element, "");
            return true;
        }        
    },
    verifyValueProf: function(helper, value, element, errorMessage) {        
        if (!value) {
            helper.displayErrorOnComponent(element, errorMessage);
            return false;
        } else {
            helper.displayErrorOnComponent(element, "");
            return true;
        }       
	},
    
    verifySameValue: function(helper, value1, value2, element1, element2, errorMessage) {
		if (value1 && value2 && value1 != value2) {
            helper.displayErrorOnComponent(element1, errorMessage);
            helper.displayErrorOnComponent(element2, errorMessage);
            return false;
        }
        return true;
	},
    verifyPassword: function(helper, component) {
        const password = component.find("password").get("v.value");
        const confirmPassword = component.find("confirmPassword").get("v.value");    
        const email = component.find("email").get("v.value");
        
        if(!helper.verifyValue(helper, password, component.find("password"), $A.get("$Label.c.PasswordConfirmRequired"))) {
            helper.verifyValue(helper, confirmPassword, component.find("confirmPassword"), $A.get("$Label.c.PasswordRequired"));
            return false;
        }
        if(!helper.verifyValue(helper, confirmPassword, component.find("confirmPassword"), $A.get("$Label.c.PasswordRequired"))) {
            return false;
        }
        if(!helper.verifySameValue(helper, password, confirmPassword, component.find("password"), component.find("confirmPassword"), $A.get("$Label.c.PasswordNotEquals"))) {
            return false;
        }
        
        if (password.includes(email)) {
            helper.displayErrorOnComponent(component.find("password"), "Le mot de passe ne doit pas inclure votre adresse email");
            helper.displayErrorOnComponent(component.find("confirmPassword"), "Le mot de passe ne doit pas inclure votre adresse email");
            return false;
        }
        
  
        //1 uppercase, 1 lowercase, 1 number
        const anUpperCase = /[A-Z]/;
    	const aLowerCase = /[a-z]/; 
   	 	const aNumber = /[0-9]/;
        var numUpper = 0;
        var numLower = 0;
        var numNums = 0;
        for(var i=0; i<password.length; i++){
            if(anUpperCase.test(password[i]))
                numUpper++;
            else if(aLowerCase.test(password[i]))
                numLower++;
            else if(aNumber.test(password[i]))
                numNums++;
        }
        if(password.length < 8 || numUpper==0 || numLower==0 || numNums ==0)
        {
            helper.displayErrorOnComponent(component.find("password"), "Le mot de passe doit comporter une minuscule, une majuscule et un chiffre et faire au moins 8 caractères");
            helper.displayErrorOnComponent(component.find("confirmPassword"), "Le mot de passe doit comporter une minuscule, une majuscule et un chiffre et faire au moins 8 caractères");
            return false;
        }
        return true;
    },
    
    validateEmail : function(helper, email, component) {
        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const isValid = mailformat.test(email);
        if(!isValid) {
            helper.displayErrorOnComponent(component.find("email"), "Veuillez saisir un email valide comprenant un @ et un .");
        	helper.displayErrorOnComponent(component.find("confirmEmail"), "Veuillez saisir un email valide comprenant un @ et un .");
        }

        return isValid;
    },
    displayErrorOnComponent: function(element, errorMessage) {
		element.setCustomValidity(errorMessage);
        element.reportValidity();
    },
    
    validateSIRET :function(helper, component, value) {        
        if(value != 14){
            helper.displayErrorOnComponent(component.find("firstname"), "Le numéro de SIRET doit comporter 14 chiffres exactement, merci de le corriger."); 
            return false;
        }
        else{
            return true;
        }        
    },   
    getParameterByName : function (name, url) {
        
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
            
    }
})