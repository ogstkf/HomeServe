({
    doInit : function(component, event, helper) {
        component.set('v.isLoading', true);
        helper.getLogementAddress(component, event, helper);
    },

    handleDownloadDoc : function(component, event, helper) {
        component.set('v.isLoading', true);
        helper.downloadDoc(component, event, helper);
    },

    showMore : function(component, event, helper) {
        var index = event.getSource().get('v.value');
        var lstAllDocuments = component.get('v.lstAllDocuments');
        var lstDocumentsToDisplay = component.get('v.lstDocumentsToDisplay'); 

        lstDocumentsToDisplay[index].isExpanded = true;
        lstDocumentsToDisplay[index].lstDoc = lstAllDocuments[index].lstDoc;
        component.set('v.lstDocumentsToDisplay', lstDocumentsToDisplay);
    },

    showLess : function(component, event, helper) {
        var index = event.getSource().get('v.value');
        var lstAllDocuments = component.get('v.lstAllDocuments');
        var lstDocumentsToDisplay = component.get('v.lstDocumentsToDisplay');

        lstDocumentsToDisplay[index].isExpanded = false;
        lstDocumentsToDisplay[index].lstDoc = lstAllDocuments[index].lstDoc.slice(0, 5);
        component.set('v.lstDocumentsToDisplay', lstDocumentsToDisplay);
    },

    handleContactAgence : function(component, event, helper) {
        const urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": '/service-client',
          "target": "_blank"
        });
        urlEvent.fire();
    }, 

    handleOrder : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var index1 = selectedItem.dataset.index1;
        var index2 = selectedItem.dataset.index2;

        var lstAllDocuments = JSON.parse(JSON.stringify(component.get('v.lstAllDocuments')));
        var lstDocumentsToDisplay = component.get('v.lstDocumentsToDisplay');

        var header = lstAllDocuments[index1].header;
        for(var i=0; i<header.length; i++){
            if(header[i].enableOrder){
                if(i == index2){
                    header[i].isOrdered = true;

                    if(header[i].isAsc){
                        header[i].isAsc = false;
                        header[i].isDesc = true;
                    }
                    else{
                        header[i].isAsc = true;
                        header[i].isDesc = false;
                    }
                }
                else{
                    header[i].isOrdered = false;
                    delete header[i].isAsc;
                    delete header[i].isDesc;
                }
            }
        }

        // add updated header, to global and display list
        lstAllDocuments[index1].header = header;
        lstDocumentsToDisplay[index1].header = header;

        // order the list by field desired by user
        var lstDocSortedToDisplay = JSON.parse(JSON.stringify(helper.orderList(lstAllDocuments[index1].lstDoc, header[index2].api, header[index2].isAsc ? 'asc' : 'desc')));
        lstAllDocuments[index1].lstDoc = lstDocSortedToDisplay;
        
        if(lstDocumentsToDisplay[index1].isExpanded){
            lstDocumentsToDisplay[index1].lstDoc = lstDocSortedToDisplay;
        }
        else{
            lstDocumentsToDisplay[index1].lstDoc = lstAllDocuments[index1].lstDoc.slice(0, 5);
        }

        component.set('v.lstDocumentsToDisplay', lstDocumentsToDisplay);
        component.set('v.lstAllDocuments', lstAllDocuments);        
    }, 

    handleRetourAccueil : function(component, event, helper) {
        var urlEvent = $A.get('e.force:navigateToURL');
        urlEvent.setParams({
            url: '/',
        });
        urlEvent.fire();
    }
})