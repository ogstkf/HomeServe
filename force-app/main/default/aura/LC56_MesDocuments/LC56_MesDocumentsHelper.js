({
    getLogementAddress: function(component, event, helper) {
        var action = component.get('c.getLogements');

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();

                var docGroupedByAddr = {};
                result.forEach(addr => {
                    docGroupedByAddr[addr] = [];
                });

                component.set('v.docGroupedByAddr', docGroupedByAddr);

                this.getDocuments(component, event, helper);
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + JSON.stringify(errors));
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },

    getDocuments : function(component, event, helper) {
        var action = component.get('c.fetchMesDocuments');

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var obj = component.get('v.docGroupedByAddr');

                var result = JSON.parse(response.getReturnValue());
                if(result.msg == 'success'){
                    var docList = result.docList;
                    for(var i=0;i<docList.length; i++){
                        var addr = docList[i].addressLogement;
                        obj[addr].push(docList[i]);
                    }

                    var lstAllDocuments = [];
                    for(var addr in obj){
                        var sortedDocs = obj[addr].slice().sort((a, b) => new Date(b.docDate) - new Date(a.docDate))
                        lstAllDocuments.push({
                            address: addr,
                            lstDoc: sortedDocs,
                            header: [{ title: 'Document', api: 'type', enableOrder: true, isOrdered: false, styleClass: 'header position slds-text-title_bold', hasBorder: true },
                                        { title: 'Date', api: 'docDate', enableOrder: true, isOrdered: true, isAsc: false, isDesc: true, styleClass: 'header position slds-text-title_bold width15', hasBorder: true  },
                                        { title: 'Equipement', api: 'equipment', enableOrder: true, isOrdered: false, styleClass: 'header position slds-text-title_bold width15', hasBorder: true  },
                                        { title: 'Actions', styleClass: 'header position slds-text-title_bold width9', hasBorder: false  }]
                        });
                    }

                    
                    // sorting - display logement with most recent document first
                    lstAllDocuments.sort((a,b) => {
                        var date1 = a.lstDoc[0] ? new Date(a.lstDoc[0].docDate) : new Date('1990-01-01');
                        var date2 = b.lstDoc[0] ? new Date(b.lstDoc[0].docDate) : new Date('1990-01-01');
                        
                        if ( date1 > date2 ){
                            return -1;
                          }
                          if ( date2 < date1 ){
                            return 1;
                          }
                          return 0;
                    });

                    var lstDocumentsToDisplay = this.generateSubList(lstAllDocuments);

                    console.log('lstAllDocuments: ', JSON.parse(JSON.stringify(lstAllDocuments)));

                    component.set('v.lstAllDocuments', lstAllDocuments);
                    component.set('v.lstDocumentsToDisplay', lstDocumentsToDisplay);
                    component.set('v.isLoading', false);
                }
                else{
                    console.log('getDocuments result: ', result);
                    
                    // when there is no documents
                    var lstDocumentsToDisplay = [];
                    for(var addr in obj){
                        lstDocumentsToDisplay.push({address: addr,
                                                    lstDoc: []});
                    }
                    component.set('v.lstDocumentsToDisplay', lstDocumentsToDisplay);
                    component.set('v.isLoading', false);
                }
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + JSON.stringify(errors));
                    }
                } else {
                    console.log('Unknown error');
                }
                component.set('v.isLoading', false);
            }
        });
        $A.enqueueAction(action);
    },

    downloadDoc : function(component, event, helper) {
        var docId = event.getSource().get('v.value');

        var action = component.get('c.getDoc');
        action.setParams({
            "docId" : docId
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var result = JSON.parse(response.getReturnValue());

                if(result.msg == 'success'){
                    var dataType = 'application/';
                    if(result.doc.type != 'pdf'){
                        dataType = 'image/';
                    }

                    this.downloadFile('data:'+dataType+result.doc.type+';base64,'+result.doc.base64, result.doc.title);
                    component.set('v.isLoading', false);
                }
                else{
                    component.set('v.isLoading', false);
                }
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + JSON.stringify(errors));
                    }
                } else {
                    console.log('Unknown error');
                }
                component.set('v.isLoading', false);
            }
        });
        $A.enqueueAction(action);
    },

    downloadFile : function(dataurl, filename) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
    
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
    
        var file = new File([u8arr], filename, {type:mime});
    
        var objecturl =  window.URL.createObjectURL(file);
        const downloadLink = document.createElement("a");
        downloadLink.href = objecturl;
        downloadLink.download = filename;
        downloadLink.click();
    },

    generateSubList : function(lstAllDocuments) {
        var lstDocumentsToDisplay = [];

        for(var i=0; i<lstAllDocuments.length; i++){
            var obj = lstAllDocuments[i];
            var docObj = { address: obj.address, 
                            header: obj.header };
            if(obj.lstDoc.length > 5){
                docObj.lstDoc = obj.lstDoc.slice(0, 5);
                docObj.isExpanded = false;
                docObj.displayLoadMore = true;
            }
            else{
                docObj.lstDoc = obj.lstDoc;
                docObj.isExpanded = false;
                docObj.displayLoadMore = false;
            }
            lstDocumentsToDisplay[i] = docObj;
        }
        return lstDocumentsToDisplay;
    },

    orderList : function(lstDoc, fieldToOrder, ascOrDesc){
        lstDoc.sort((a,b) => {
            var val1, val2;
            if(fieldToOrder == 'docDate'){
                val1 = new Date(a.docDate);
                val2 = new Date(b.docDate);
            }
            else{
                val1 = a[fieldToOrder];
                val2 = b[fieldToOrder];
            }

            if(ascOrDesc == 'desc'){
                if (val1 > val2){
                    return -1;
                }
                if (val1 < val2){
                    return 1;
                }
            }
            else{
                if (val1 < val2){
                    return -1;
                }
                if (val1 > val2){
                    return 1;
                }
            }
            return 0;
        });
        return lstDoc;
    }
})