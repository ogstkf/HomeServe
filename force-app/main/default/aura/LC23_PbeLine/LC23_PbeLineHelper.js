/**
 * @File Name          : LC23_PbeLineHelper.js
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 20/12/2019, 11:48:19
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/12/2019   RRJ     Initial Version
**/
({
    navigateToRecord: function(component, recId) {
        var navService = component.find("navService");
        var pageRef = {
            type: "standard__recordPage",
            attributes: {
                recordId: recId,
                actionName: "view"
            }
        };
        navService.navigate(pageRef);
    }
});