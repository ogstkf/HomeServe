/**
 * @File Name          : LC23_PbeLineController.js
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 20/12/2019, 11:49:04
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/12/2019   RRJ     Initial Version
**/
({
    handleNav: function(component, event, helper) {
        var recId = component.get("v.pbe").Product2Id;
        helper.navigateToRecord(component, recId);
    }
});