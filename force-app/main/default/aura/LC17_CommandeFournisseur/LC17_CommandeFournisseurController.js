({
    doInit : function(component, event, helper) {       
        console.log('## Permettre la génération du pdf quand la commande est active ');
        var action = component.get("c.checkIfActive");
        action.setParams({
            "oId" : component.get("v.recordId")
        });
        action.setCallback(this, function(a) { 
            var state = a.getState();
            if(component.isValid() && state === 'SUCCESS'){
                var result = a.getReturnValue();
                if(!result.error){
                    // var url ='/apex/VFP39_OrderItemPDF?id=' + component.get("v.recordId");
                    // window.open(url);
                    
                    // $A.get("e.force:closeQuickAction").fire(); 
                    // //$A.get('e.force:refreshView').fire();

                    // var navEvt = $A.get("e.force:navigateToSObject");
                    // navEvt.setParams({
                    //   "recordId": component.get("v.recordId"),
                    //   "slideDevName": "related"
                    // });
                    // navEvt.fire();
                    // console.log('## REFRESH ');

                    console.log('## redirection: ' , result);

                    helper.closeModal();
                    var url ='/'+result.content_doc_id;
                    window.open(url);

                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "ERROR",
                        type: "error",
                        message: "La commande doit être active pour la génération du pdf."
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire(); 
                }
            }
            else {
                console.log('## error : ',action.getError()[0]);  
            }           
        });
        $A.enqueueAction(action);
        
    },
    goToVF : function(component, event, helper) {       
        console.log('## Permettre la génération du pdf quand la commande est active ');
        // var oId = component.get("v.recordId");
        // var action = component.get("c.checkIfActive");
        // action.setParams({
        //     "oId" : oId
        // });
        // action.setCallback(this, function(a) { 
        //     var state = a.getState();

        //     if(component.isValid() && state === 'SUCCESS'){
        //         var result = a.getReturnValue();
        //         console.log('## results :',result);
        //         if(!result.error){
        //             var url ='/apex/VFP39_OrderItemPDF?id=' + component.get("v.recordId");
        //             window.open(url);
                
        //         }else{
        //             console.log('## Error');
        //         }
        //     }
        //     else {
        //         console.log('## error : ',action.getError()[0]);  

        //     }           
        // });
        // $A.enqueueAction(action);
        
        var url ='/apex/VFP39_OrderItemPDF?id=' + component.get("v.recordId");
        window.open(url);
    },

    

})