/**
 * @File Name          : LC04_WizardDevisEcranSyntheseController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 06/11/2019, 21:04:20
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    06/11/2019   RRJ     Initial Version
 **/
({
    init: function(component, event, helper) {
        helper.handleFlowInit(component);
        helper.populateColHeaders(component);
        helper.populateTable(component);
    },

    handleFlowNav: function(component, event, helper) {
        helper.navigateFlow(component, event);
    }
});