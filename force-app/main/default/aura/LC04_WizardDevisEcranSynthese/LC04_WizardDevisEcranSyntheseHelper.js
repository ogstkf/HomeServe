/**
 * @File Name          : LC04_WizardDevisEcranSyntheseHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 06/11/2019, 21:49:46
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    06/11/2019   RRJ     Initial Version
 **/
({
    handleFlowInit: function(component) {
        var availableActions = component.get("v.availableActions");
        console.log("##### availableActions: ", availableActions);
        for (var i = 0; i < availableActions.length; i++) {
            if (availableActions[i] == "BACK") {
                component.set("v.canBack", true);
            } else if (availableActions[i] == "NEXT") {
                component.set("v.canNext", true);
            } else if (availableActions[i] == "FINISH") {
                component.set("v.canFinish", true);
            }
        }
    },

    navigateFlow: function(component, event) {
        var actionClicked = event.getSource().getLocalId();
        var navigate = component.get("v.navigateFlow");
        navigate(actionClicked);
    },

    populateColHeaders: function(component) {
        var cols = [
            // { label: "Numéro d'élément de ligne", fieldName: "LineNumber", type: "text" },
            { label: "Produit", fieldName: "Produit", type: "text" },
            { label: "Prix HT avant remise", fieldName: "Prix_HT_avant_remise__c", type: "number" },
            { label: "Remise en %", fieldName: "Remise_en100__c", type: "number" },
            { label: "Remise en euros", fieldName: "Remise_en_euros__c", type: "number" },
            { label: "Taux de TVA", fieldName: "Taux_de_TVA__c", type: "number" },
            { label: "Prix TTC", fieldName: "Prix_TTC__c", type: "number" }
        ];

        component.set("v.lstCol", cols);
    },

    populateTable: function(component) {
        var action = component.get("c.fetchQLIs");
        var quoteId = component.get("v.quoteId");
        action.setParams({
            quoteId: quoteId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.lstQLI", this.processData(response.getReturnValue()));
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    processData: function(lstQLI) {
        lstQLI.forEach(qli => {
            qli.Produit = qli.Product2.Name;
        });
        return lstQLI;
    }
});