/**
 * @File Name          : LC_ListViewCmpHelper.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 06/05/2020, 10:08:06
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/05/2020   ZJO     Initial Version
**/
({
    fetchData: function (component) {

        var myPageRef = component.get("v.pageReference");
        var conId = myPageRef.state.c__contractId;
        component.set("v.conId", conId);

        console.log("### recordId:", conId);

        var action = component.get('c.getCliRecords');
        action.setParams({
            strObjectName: 'ContractLineItem',
            strFieldSetName: 'Facturation',
            servConId: conId
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                console.log("### columns:", response.getReturnValue().lstDataTableColumns);
                console.log("### data:", response.getReturnValue().lstDataTableData);
                component.set("v.mycolumns", response.getReturnValue().lstDataTableColumns);

                var rows = response.getReturnValue().lstDataTableData;
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Asset) {
                        row.AssetName = row.Asset.Name;
                    }

                    if (row.Product2) {
                        row.ProductName = row.Product2.Name;
                    }

                    if (row.Collective_Account__r) {
                        row.ClientCollectif = row.Collective_Account__r.Name;
                    }
                }
                component.set("v.mydata", rows);

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: ", errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        }));
        $A.enqueueAction(action);
    },

    sortBy: function (field, reverse, primer) {
        var key = primer
            ? function (x) {
                return primer(x[field]);
            }
            : function (x) {
                return x[field];
            };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    },

    handleSort: function (component, event) {
        var sortBy = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        console.log(component.get("v.mydata"));

        var data = component.get("v.mydata");
        data.sort((this.sortBy(sortBy, sortDirection === 'asc' ? 1 : -1)));

        component.set('v.mydata', data);
        component.set('v.sortDirection', sortDirection);
        component.set('v.sortBy', sortBy);
    },

    handleSave: function (component, event) {

        var draftValues = event.getParam('draftValues');
        console.log(draftValues);

        var action = component.get('c.updateCliRecords');
        action.setParams({
            updatedCliList: draftValues
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("### updated values:", response.getReturnValue());
                component.set('v.errors', []);
                component.set('v.draftValues', []);
                $A.get('e.force:refreshView').fire();

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: ", errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        }));
        $A.enqueueAction(action);
    }


})