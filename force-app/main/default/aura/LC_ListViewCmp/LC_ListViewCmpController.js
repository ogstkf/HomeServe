/**
 * @File Name          : LC_ListViewCmpController.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 04/05/2020, 11:35:44
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/05/2020   ZJO     Initial Version
**/
({
    init: function (component, event, helper) {
        helper.fetchData(component);
    },

    handleSort: function (component, event, helper) {
        helper.handleSort(component, event);
    },

    handleSave: function (component, event, helper) {
        helper.handleSave(component, event);
    }
})