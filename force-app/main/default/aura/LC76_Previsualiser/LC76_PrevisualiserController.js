({
    doInit : function(component, event, helper) {
        var action = component.get("c.pickListvalue");
        action.setParams({
            "recordId": "8101X000000CcAUQA0"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
            }
            else
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()));
                         
        });
        $A.enqueueAction(action);
    }
})