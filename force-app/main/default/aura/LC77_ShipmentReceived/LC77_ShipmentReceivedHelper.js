({
    recu : function(component) {
        var shipId = component.get('v.recordId');
        var action = component.get('c.recu');
        action.setParams({ 
            "shipId": shipId
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if (state === 'SUCCESS') {
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    title: 'SUCCESS',
                    type: 'success',
                    message: 'Tous les produits ont été reçus'
                });
                toastEvent.fire();
            }
            else {
                var toastEvent = $A.get('e.force:showToast');
                toastEvent.setParams({
                    title: 'ERROR',
                    type: 'error',
                    message: 'error'//response.getError()[0].message
                });
                toastEvent.fire();
                console.log(response.getError()[0]);
            }
            $A.get('e.force:closeQuickAction').fire();
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    }
})