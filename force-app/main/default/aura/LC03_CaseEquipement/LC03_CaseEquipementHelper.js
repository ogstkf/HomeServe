/**
 * @File Name          : LC03_CaseEquipementHelper.js
 * @Description        :
 * @Author             : DMU
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 30/07/2019, 00:48:48
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    09/07/2019, 10:30:00   DMU     Initial Version
 **/
({
    populateData: function(component, caId) {
        var action = component.get("c.fetchEqp");
        action.setParams({
            caseId: caId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.lstEqp", response.getReturnValue());
                component.set("v.loading", false);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        component.set("v.loading", false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
});