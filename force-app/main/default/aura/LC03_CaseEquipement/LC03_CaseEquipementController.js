/**
 * @File Name          : Controller.js
 * @Description        :
 * @Author             : DMU
 * @Group              :
 * @Last Modified By   : DMU
 * @Last Modified On   : 09/07/2019, 10:30:00
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    09/07/2019, 10:30:00   DMU     Initial Version
 **/
({
    doInit: function(component, event, helper) {
    	// console.log('*** id record: ',component.get("v.recordId"));
        helper.populateData(component, component.get("v.recordId"));
    }
});