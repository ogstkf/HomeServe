/**
 * @File Name          : LC_NavigateToCustomTableCmpController.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08/05/2020, 11:13:27
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/05/2020   ZJO     Initial Version
**/
({
    doInit: function (component, event, helper) {
        component.set("v.showSpinner", true);

        var servConId = component.get('v.recordId');

        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__LC_CustomTableCmp',
            },
            state: {
                "c__contractId": servConId
            }
        };
        component.set("v.pageReference", pageReference);
    },

    handleClick: function (component, event, helper) {
        var navService = component.find("navService");
        var pageReference = component.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);
    }
})