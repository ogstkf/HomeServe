/**
 * @File Name          : LC10_ProdReqLineItemOrderController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 07/12/2019, 00:52:55
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    07/12/2019   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        console.log("doInit start");
        helper.initComponent(component, event, helper);

        console.log("doInit end");
    },

    onChangeRadio: function(component, event, helper) {
        console.log("## onChange start: ");
        console.log("##Event: ", event);
        console.log("## checkbox Id: ", event.target.id);
        console.log("## checkbox val: ", event.target.checked);

        if (event.target.checked == true) {
            var res = component.get("v.table_data");
            var all = component.get("v.allSelected");
            var pricebook = component.get("v.allPricebookSelected");
            var prliId = component.get("v.prliId");
            console.log("## Radio Button True ");
            helper.radioTrue(component, event, res, all, pricebook, prliId);
        } else {
            helper.radioFalse(component, event, helper);
        }

        console.log("## onChecked end: ");
    },

    onCheck: function(component, event, helper) {
        var isCheck = event.target.checked == true;
        var pbId = event.target.dataset.pbid;

        var lstData = component.get("v.table_data");
        lstData.forEach(row => {
            if (row.pb.Id === pbId) {
                row.isChecked = true;
            } else {
                row.isChecked = false;
            }
        });

        component.set("v.table_data", lstData);
    },

    commander: function(component, event, helper) {
        var lstData = component.get("v.table_data");
        console.log("##### lstData: ", lstData);
        var row = lstData.find(dataRow => {
            return dataRow.isChecked === true;
        });
        console.log(">>>> row: ", row);
        helper.createCommande(component, row);
    },

    back: function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        console.log("Workspace: ", workspaceAPI);
        
        workspaceAPI.isConsoleNavigation().then(function(isConsole) {
            console.log(isConsole);

            if(isConsole){
                workspaceAPI
                    .getFocusedTabInfo()
                    .then(function(response) {
                        console.log("###### tab1 ", response);
                        console.log("###### tab1 ", response.tabId);
                        var focusedTabId = response.tabId;
                        workspaceAPI
                            .openTab({
                                pageReference: {
                                    type: "standard__objectPage",
                                    attributes: {
                                        objectApiName: "ProductRequestLineItem",
                                        actionName: "list"
                                    }
                                }
                            })
                            .then(function(response) {
                                workspaceAPI.closeTab({ tabId: focusedTabId });
                                workspaceAPI.focusTab({ tabId: response });
                                console.log(response);
                            });
                    })
                    .catch(function(error) {
                        console.log(error);
                    }); 
            }else{
                var hostname = window.location.hostname;
                var arr = hostname.split(".");
                var instance = arr[0];  

                console.log('instance: ' , instance);
                
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/lightning/o/ProductRequestLineItem/list?filterName=" + component.get('v.listviewId')
                });
                urlEvent.fire();


                console.log('### listviewid: ' , component.get('v.listviewId'));
                // helper.navToListview(component.get('v.listviewId'));
                console.log('## is Not console.. navigate to Listview');
            }
        })
        .catch(function(error) {
            console.log(error);
        });


        // workspaceAPI
        //     .getFocusedTabInfo()
        //     .then(function(response) {
        //         console.log("###### tab1 ", response);
        //         console.log("###### tab1 ", response.tabId);
        //         var focusedTabId = response.tabId;
        //         workspaceAPI
        //             .openTab({
        //                 pageReference: {
        //                     type: "standard__objectPage",
        //                     attributes: {
        //                         objectApiName: "ProductRequestLineItem",
        //                         actionName: "list"
        //                     }
        //                 }
        //             })
        //             .then(function(response) {
        //                 workspaceAPI.closeTab({ tabId: focusedTabId });
        //                 workspaceAPI.focusTab({ tabId: response });
        //                 console.log(response);
        //             });
        //     })
        //     .catch(function(error) {
        //         console.log(error);
        //     });
    }
});