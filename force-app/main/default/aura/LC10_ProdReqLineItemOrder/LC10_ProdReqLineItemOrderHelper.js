/**
 * @File Name          : LC10_ProdReqLineItemOrderHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 03-11-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    07/12/2019          RRJ                  Initial Version
 **/
({
    initComponent: function(component, event, helper) {
        var recordIds = component.get("v.pageReference").state.c__ids;
        var listviewId = component.get("v.pageReference").state.c__listviewid;
        component.set("v.listviewId", listviewId);

        var action = component.get("c.getData");
        action.setParams({
            recordIds: recordIds
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("From server: ", response.getReturnValue());
                var dataReturned = response.getReturnValue();

                console.log(dataReturned);

                var lstAll = dataReturned;
                lstAll.forEach(function(row) {
                    var totalUnit = 0;
                    var multtotal = 0;
                    var children = row.lstPrliWrap;
                    children.forEach(function(child) {
                        totalUnit += $A.util.isEmpty(child.unitPrice) ? 0 : child.unitPrice;

                        var qr = $A.util.isEmpty(child.QuantityRequested) ? 0 : child.QuantityRequested;
                        var up = $A.util.isEmpty(child.unitPrice) ? 0 : child.unitPrice;
                        var mult = $A.util.isEmpty(child.QuantityRequested) || $A.util.isEmpty(child.unitPrice) ? 0 : qr * up;
                        console.log("Mult", mult);
                        multtotal += mult;
                    });

                    row.totalUnit = totalUnit;
                    row.totalMult = multtotal;
                });

                component.set("v.table_data", lstAll);
                component.set("v.showPage", true);
                component.set("v.show_spinner", false);
            } else if (state === "INCOMPLETE") {
                console.log("incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        this.showToast("Erreur!", "error", errors[0].message);
                        this.closeFocusedTab(component);
                        this.navigateToList(component);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
        console.log("Init component end");
    },

    validateCommande: function(component, event, helper) {
        return true;
        console.log("## validate commande start");
        var listSelectedIds = component.get("v.checked_ids");

        console.log("checked_ids", component.get("v.checked_ids"));
        console.log("Prli Id in validate command: ", component.get("v.prliId"));

        if (listSelectedIds.length == 0 && listPRLIId == 0) {
            //todo: throw error
            return false;
        }

        var setPricebookIds = new Set();
        for (var i = 0; i < listSelectedIds.length; i++) {
            console.log("## validate current id: ", listSelectedIds[i]);
            var currId = listSelectedIds[i].split("_");

            if (currId[0] != undefined) {
                setPricebookIds.add(currId[0]);
            }
            console.log("## curr Splitted ids: ", currId);
        }

        console.log("## after for: ");
        console.log("setPricebookIds.size()", setPricebookIds.size);

        if (setPricebookIds.size == 1) {
            return true;
        } else {
            this.showToast("Erreur!", "error", "Vous ne pouvez sélectionner qu'un seul fournisseur pour une commande");
            return false;
        }

        console.log("## setPricebookIds: ", setPricebookIds);
        console.log("## validate commande end");
    },

    showToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    },

    createCommande: function(component, data) {
        var isValid = this.validateCommande(component);
        if (isValid) {
            var action = component.get("c.saveOrders");
            action.setParams({
                JSONwrapper: JSON.stringify(data)
            });

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    this.afterFinish(component, response.getReturnValue());
                    component.set("v.show_spinner", false);
                } else if (state === "INCOMPLETE") {
                    console.log("incomplete");
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            this.showToast("Erreur!", "error", errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });

            $A.enqueueAction(action);
        } else {
            this.showToast("Erreur!", "error", "Vous ne pouvez sélectionner qu'un seul fournisseur pour une commande");
        }
    },

    afterFinish: function(component, recId) {
        var workspaceAPI = component.find("workspace");
        console.log("Workspace: ", workspaceAPI);
        var self = this;

        workspaceAPI.isConsoleNavigation().then(function(isConsole) {
            console.log(isConsole);

            if(isConsole){
                workspaceAPI
                    .getFocusedTabInfo()
                    .then(function(response) {
                        console.log("###### tab1 ", response);
                        console.log("###### tab1 ", response.tabId);
                        var focusedTabId = response.tabId;
                        // self.navigateToRecord(component, recId);
                        var pageRef = {
                            type: "standard__recordPage",
                            attributes: {
                                recordId: recId,
                                actionName: "view"
                            }
                        };
                        workspaceAPI
                        .openTab({ pageReference: pageRef, focus: true })
                        .then(function(response) {
                            workspaceAPI.closeTab({ tabId: focusedTabId });
                            workspaceAPI.focusTab({tabId: response});
                            console.log(response);
                        });
                        // .then(function() {
                        //     workspaceAPI = component.find("workspace");
                        //     console.log('###### workspace: ', workspaceAPI);
                        //     workspaceAPI
                        //         .getFocusedTabInfo()
                        //         .then(function(response) {
                        //             var newTab = response.tabId;
                        //             console.log("###### tab2 ", response);
                        //             console.log("###### tab2 ", response.tabId);
                        //             // workspaceAPI.refreshTab({
                        //             //     tabId: newTab,
                        //             //     includeAllSubtabs: true
                        //             // });
                        //         })
                        //         .catch(function(error) {
                        //             console.log(error);
                        //         });
                        // });
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }else{
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/"+recId
                });
                urlEvent.fire();
            }

        }).catch(function(error) {
                console.log(error);
        });

        
    },

    navigateToRecord: function(component, recId) {
        var navService = component.find("navService");
        var pageRef = {
            type: "standard__recordPage",
            attributes: {
                recordId: recId,
                actionName: "view"
            }
        };
        navService.navigate(pageRef);
    },

    closeFocusedTab: function(component) {
        console.log("Enter closeFocusedTab");
        var workspaceAPI = component.find("workspace");
        workspaceAPI
            .getFocusedTabInfo()
            .then(function(response) {
                var focusedTabId = response.tabId;
                workspaceAPI.closeTab({ tabId: focusedTabId });
            })
            .catch(function(error) {
                console.log(error);
            });
        console.log("Exit closeFocusedTab");
    },

    navigateToList: function(component) {
        console.log("## ListviewId; ", component.get("v.listviewId"));
        var lvId = component.get("v.listviewId");
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            listViewId: lvId,
            scope: "ProductRequestLineItem"
        });
        navEvent.fire();
    },

    closeTab: function(component) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI
            .getFocusedTabInfo()
            .then(function(response) {
                var focusedTabId = response.tabId;
                workspaceAPI.closeTab({ tabId: focusedTabId });
            })
            .catch(function(error) {
                console.log(error);
            });
    }
});