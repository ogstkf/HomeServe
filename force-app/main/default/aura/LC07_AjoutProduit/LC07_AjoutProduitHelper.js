/**
 * @File Name          : LC07_AjoutProduitHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 17-09-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    29/11/2019   RRJ     Initial Version
 **/
({
    initialize: function (component) {
        component.set('v.isLoading', true);
        var quoId = component.get('v.recordId');
        var action = component.get('c.fetchQuoDetails');
        action.setParams({
            quoId: quoId,
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                // console.log('#### responce: ', response.getReturnValue());

                var returnValue = response.getReturnValue();
                var isAdmin = returnValue.isAdmin;
                var qu = returnValue.quote;
                // console.log('##### qu: ', qu.Status);

                if ( !isAdmin && (
                    qu.Status == "Validé, signé - en attente d'intervention" ||
                    qu.Status == 'Validé,signé mais abandonné' ||
                    qu.Status == 'Validé, signé et terminé' ||
                    qu.Status == 'Expired')
                ) {
                    component.set('v.quoteVS', true);
                    component.set('v.isModalOpen', true);
                    // console.log('Status: ', component.get('v.quoteVS'));
                    // console.log('Model: ', component.get('v.isModalOpen'));
                    this.openModel(component);
                }

                component.set('v.quo', returnValue.quote);

                var prcBk = null;
                if ($A.util.isEmpty(qu.Agency__c)) {
                    prcBk = this.getPrcBkId(returnValue.quote);
                }

                // console.log('#### prcBkId: ', prcBk);
                //DMU 20201028 - TEC-201 - Added condition to verify qu.agency
                //since prcBk should be taken from qu.agency
                if (qu.Agency__c != undefined) {        //if ($A.util.isEmpty(prcBk)) { // <-------this was previous condition
                    console.log('in if has Agency on this quote', qu.Agency__c);
                    var self = this;
                    this.getPb(component).then(
                        $A.getCallback(function (result) {
                            console.log('######## lstPB: ', result);
                            component.set('v.lstPB', result);
                            if (result.length == 1 && qu.RecordType.DeveloperName != 'Devis_RTC') {
                                component.set('v.pbk', result[0]);
                                self.loadSelectPBE(component);
                            } else {
                                self.changePage(component, 'selectCatalog');
                            }
                        })
                    );
                } 
                else {
                    // component.set('v.pbk', prcBk);
                    // this.loadSelectPBE(component);
                    //DMU 20201028 - TEC-201 - Added condition to verify qu.agency
                    console.log('No Agency on this quote');
                    var noAgencyOnQuote = document.getElementsByClassName("AgencenotFound");
                    noAgencyOnQuote[0].style.display = 'block';
                }
            } else if (state === 'INCOMPLETE') {
                alert('Incomplete');
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.error('Error message: ' + errors[0].message);
                    }
                } else {
                    console.error('Unknown error');
                }
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },

    getPb: function (component) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var quoId = component.get('v.recordId');
                var action = component.get('c.fetchPb');
                action.setParams({
                    quoId: quoId,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        // console.log('#### response get pb: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    getPriceBookEntries: function (component, searchTxt) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var pbkId = component.get('v.pbk').Id;
                var quoId = component.get('v.recordId');
                var action = component.get('c.fetchPriceBookEntries');
                action.setParams({
                    prcBkId: pbkId,
                    searchText: searchTxt,
                    quoId: quoId,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        // console.log('#### responce get Pbe: ', response.getReturnValue());
                        // component.set("v.lstAllPBE", response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    getExistingQLI: function (component) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var pbkId = component.get('v.pbk').Id;
                var quoId = component.get('v.quo').Id;
                var action = component.get('c.fetchQLI');
                action.setParams({
                    quoId: quoId,
                    pbkId: pbkId,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        // console.log('#### responce get qli: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    getPrcBkId: function (quo) {
        if (!$A.util.isEmpty(quo.Pricebook2Id)) {
            return { Id: quo.Pricebook2Id, Name: quo.Pricebook2.Name };
        } else if (!$A.util.isEmpty(quo.Opportunity.Pricebook2Id)) {
            return { Id: quo.Opportunity.Pricebook2Id, Name: quo.Opportunity.Pricebook2.Name };
        } else {
            return null;
        }
    },

    checkPBId: function (component) {
        var prcBk = component.get('v.pbk');
        if ($A.util.isEmpty(prcBk)) {
            this.showToast('', 'error', 'Veuillez selectionner une catalogue', 'dismissible');
        } else {
            this.savePbk(component);
            this.loadSelectPBE(component);
            this.changePage(component, 'selectProduct');
            this.setNavOptions(component, true, true, false);
        }
    },

    showToast: function (title, type, message, mode) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            mode: mode,
            title: title,
            type: type,
            message: message,
        });
        toastEvent.fire();
    },

    changePage: function (component, page) {
        var currPage = component.get('v.currentPage');
        component.set('v.currentPage', page);
        component.set('v.previousPage', currPage);
    },

    loadSelectPBE: function (component) {
        // console.log('entering promise');
        var lstPBE = [];
        var lstQLI = [];
        var self = this;

        component.set('v.isLoading', true);
        this.getPriceBookEntries(component, '')
            .then(
                $A.getCallback(function (result) {
                    lstPBE = result;
                    return self.getExistingQLI(component);
                })
            )
            .then(
                $A.getCallback(function (result) {
                    lstQLI = result;
                    component.set('v.lstExistingQLI', lstQLI);
                    var lists = self.processQLI(lstPBE, lstQLI);
                    component.set('v.lstAllPBE', lists.lstPBE);
                    component.set('v.lstSelectedPBE', lists.lstSelectedPBE);
                    self.changePage(component, 'selectProduct');
                    self.setNavOptions(component, true, false, false);
                    component.set('v.isLoading', false);
                })
            );
    },

    processQLI: function (lstPBE, lstQLI) {
        var lstSelectedPBE = [];
        var lstNewSel = [];
        lstQLI.forEach((qli) => {
            var line = {
                Id: qli.PricebookEntryId,
                Name: qli.Product2.Name,
                Product2Id: qli.Product2Id,
                UnitPrice: qli.UnitPrice,
                Product2: qli.Product2,
	            Description__c: qli.Description__c, 
                isLineSelected: true,
                isExistingQLI: true,
            };
            // console.log('>>>>> rrj', qli);
            lstSelectedPBE.push(line);
        });
        // console.log(lstNewSel);
        lstPBE.forEach((pbe) => {
            var hasExisting = lstQLI.some((qli) => {
                return qli.PricebookEntryId === pbe.Id;
            });
            if (hasExisting) {
                pbe.isExistingQLI = true;
                pbe.isLineSelected = true;
                // lstSelectedPBE.push(pbe);
            }
        });
        return { lstPBE: lstPBE, lstSelectedPBE: lstSelectedPBE };
    },

    preselectPBE: function (lstPBE, lstSelected, component) {
        lstPBE.forEach((pbe) => {
            var isSelected = lstSelected.some((selPbe) => {
                return selPbe.Id === pbe.Id;
            });
            if (isSelected) {
                pbe.isLineSelected = true;
            }
        });
        component.set('v.lstAllPBE', lstPBE);
    },

    setNavOptions: function (component, canNext, canBack, canFinish) {
        var options = {
            canBack: canBack,
            canNext: canNext,
            canFinish: canFinish,
        };
        component.set('v.navOptions', options);
    },

    fetchQLI: function (component, lstId) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var quoId = component.get('v.quo').Id;
                var devisType = component.get('v.devistype');
                var devTypeId = $A.util.isEmpty(devisType) ? null : devisType.Id;

                var action = component.get('c.populateQLI');
                action.setParams({
                    mapPBE: lstId,
                    quoteId: quoId,
                    devtype: devTypeId,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        // console.log('#### responce get qli New: ', response.getReturnValue());
                        // component.set("v.lstAllPBE", response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    fetchRemiseRTC: function (component, lstId) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var action = component.get('c.getRemiseRTC');
                action.setParams({
                    mapPBE: lstId,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        // console.log('#### responce get remises: ', response.getReturnValue());
                        // component.set("v.lstAllPBE", response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    finish: function (component, lstNew, quo, pbk) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var action = component.get('c.saveQLIs');
                action.setParams({
                    JSONlstWrap: lstNew,
                    quo: quo,
                    pb: pbk,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        // console.log('#### responce save qlis: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    getListPbeId: function (lstPBE) {
        var lstId = [];
        lstPBE.forEach((pbe) => {
            lstId.push(pbe.Id);
        });
        return lstId;
    },

    buildQlis: function (lstExisting, lstNew) {
        lstNew.forEach((lineItm) => {
            if ($A.util.isEmpty(lineItm.qli.Remise_en100__c)) {
                lineItm.qli.Remise_en100__c = null;
            }
            if ($A.util.isEmpty(lineItm.qli.Remise_en_euros__c)) {
                lineItm.qli.Remise_en_euros__c = null;
            }        	
            lineItm.action = 'upsert';
        });

        var lstQLI = [];
        lstExisting.forEach((qli) => {
            var toDelete = !lstNew.some((lineItm) => {
                return qli.Id === lineItm.qli.Id;
            });
            if (toDelete) {
                var line = {
                    qli: qli,
                    action: 'delete',
                };
                lstQLI.push(line);
            }
        });

        return lstNew.concat(lstQLI);
    },

    navigateToRecord: function (component, recId) {
        var navService = component.find('navService');
        var pageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: recId,
                actionName: 'view',
            },
        };
        navService.navigate(pageRef);
    },

    closeTab: function (component) {
        var workspaceAPI = component.find('workspace');
        workspaceAPI.isConsoleNavigation().then(function (isConsole) {
            if (isConsole) {
                workspaceAPI
                    .getFocusedTabInfo()
                    .then(function (response) {
                        var focusedTabId = response.tabId;
                        workspaceAPI.closeTab({ tabId: focusedTabId });
                    })
                    .catch(function (error) {
                        console.error(error);
                    });
            } else {
                // console.log('notConsole');
                var urlEvent = $A.get('e.force:navigateToURL');
                urlEvent.setParams({
                    url: '/' + component.get('v.recordId'),
                });
                urlEvent.fire();
            }
        });
    },

    afterFinish: function (component, recId) {
        component.set('v.isLoading', true);
        var workspaceAPI = component.find('workspace');
        var self = this;

        workspaceAPI.isConsoleNavigation().then(function (isConsole) {
            if (isConsole) {
                workspaceAPI
                    .getFocusedTabInfo()
                    .then(function (response) {
                        var focusedTabId = response.tabId;
                        /*workspaceAPI.closeTab({ tabId: focusedTabId }).then(function () {
                            var navService = component.find('navService');
                            var pageRef = {
                                type: 'standard__recordPage',
                                attributes: {
                                    recordId: recId,
                                    actionName: 'view',
                                },
                            };
                            navService.generateUrl(pageRef).then(function (url) {
                                window.location.href = url;
                                component.set('v.isLoading', false);
                            });
                        });*/
                        var navService = component.find('navService');
                        var pageRef = {
                            type: 'standard__recordPage',
                            attributes: {
                                recordId: recId,
                                actionName: 'view'
                            }
                        };
                        navService.generateUrl(pageRef).then(function(url) {
                            window.location.href = url;
                        });
                        workspaceAPI.closeTab({ tabId: focusedTabId });
                    })
                    .catch(function (error) {
                        console.error(error);
                    });
            } else {
                console.log('notConsole');
                var urlEvent = $A.get('e.force:navigateToURL');
                urlEvent.setParams({
                    url: '/' + component.get('v.recordId'),
                });
                urlEvent.fire();
            }
        });
    },
    openModel: function (component) {
        console.log('In');
        // Set isModalOpen attribute to true
        component.set('v.isModalOpen', true);
    },

    handleDescriptionChange: function (component) {
        var devisType = component.get('v.devistype');
        var quo = component.get('v.quo');

        if (devisType != null) {
            quo.Description = devisType.Description__c;
            component.set('v.quo', quo);
        }
    },

    handleDevisTypeChange: function (component, devisType) {
        if ($A.util.isEmpty(devisType)) {
            this.unselectDevisTypeLines(component);
            this.getQuoDet(component).then(
                $A.getCallback(function (result) {
                    component.set('v.quo', result);
                })
            );
            component.set('v.devistype', null);
        } else {
            devisType = devisType.selectedAccountGetFromEvent;
            component.set('v.devistype', devisType);
            var action = component.get('c.getAllProductsQLIT');
            action.setParams({
                devistype: devisType,
                pbk: component.get('v.pbk'),
            });

            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var result = response.getReturnValue();

                    var lstAllPbe = component.get('v.lstAllPBE');
                    var lstSelectedPbe = component.get('v.lstSelectedPBE');

                    if (result.isValid == true) {
                        var lstLDD = result.lstPbe;
                        console.log('##### lstLDD: ', lstLDD);
                        for (var i = 0; i < lstLDD.length; i++) {
                            var lddPbe = lstLDD[i];
                            console.log('##### lddPbe: ', lddPbe);
                            var indexInLstAll = lstAllPbe.findIndex((pbe) => {
                                return pbe.Id == lddPbe.Id;
                            });
                            if (indexInLstAll != -1) {
                                lstAllPbe[indexInLstAll].isLineSelected = true;
                                lstAllPbe[indexInLstAll].isDevisType = true;
                            }

                            var indexInLstSelected = lstSelectedPbe.findIndex((selPbe) => {
                                return selPbe.Id === lddPbe.Id;
                            });
                            if (indexInLstSelected === -1) {                                
                                lddPbe.isLineSelected = true;
                                lddPbe.isDevisType = true;
                                lstSelectedPbe.push(lddPbe);
                            } else {
                                lstSelectedPbe[indexInLstSelected].isLineSelected = true;
                                lstSelectedPbe[indexInLstSelected].isDevisType = true;
                            }
                        }
                        component.set('v.lstAllPBE', lstAllPbe);
                        component.set('v.lstSelectedPBE', lstSelectedPbe);
                    } else if (result.isValid == false) {
                        this.showToast(
                            'ERROR',
                            'error',
                            'Le produit ' +
                            result.invalidProdName +
                            ' n’est plus commercialisé, ce devis type n’est plus utilisable',
                            'dismissible'
                        );
                        // unselect record
                        var selectPbeComp = component.find('selectPbe');
                        selectPbeComp.clearDevisType();
                    }
                } else if (state === 'INCOMPLETE') {
                    alert('Incomplete');
                } else if (state === 'ERROR') {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error('Error message: ' + errors[0].message);
                        }
                    } else {
                        console.error('Unknown error');
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },

    unselectDevisTypeLines: function (component) {
        var lstAllPbe = component.get('v.lstAllPBE');
        var lstSelPbe = component.get('v.lstSelectedPBE');

        var lstNewSel = [];
        lstAllPbe.forEach((pbe) => {
            if (pbe.isLineSelected && pbe.isDevisType) {
                pbe.isLineSelected = false;
                pbe.isDevisType = false;
            }
        });
        lstSelPbe.forEach((pbe) => {
            if (pbe.isLineSelected && !pbe.isDevisType) {
                lstNewSel.push(pbe);
            }
        });

        component.set('v.lstAllPBE', lstAllPbe);
        component.set('v.lstSelectedPBE', lstNewSel);
    },

    getQuoDet: function (component) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var quoId = component.get('v.recordId');
                var action = component.get('c.fetchQuoDetails');
                action.setParams({
                    quoId: quoId,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        // console.log('#### responce get quo: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    reloadQLI: function (component) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var lstQli = component.get('v.lstNewQLI');
                var quoId = component.get('v.recordId');
                // console.log('### remise qli standard ', lstQli);
                var action = component.get('c.calulateRemiseQLIs');
                action.setParams({
                    JSONlstWrap: JSON.stringify(lstQli),
                    quoteId: quoId,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        var lstQli = response.getReturnValue();
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },

    savePbk: function (component) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                var pbk = component.get('v.pbk');
                var quo = component.get('v.quo');
                var action = component.get('c.saveQuoPriceBook');
                action.setParams({
                    quo: quo,
                    pbk: pbk,
                });
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === 'SUCCESS') {
                        console.log('#### responce save: ', response.getReturnValue());
                        resolve(response.getReturnValue());
                    } else if (state === 'INCOMPLETE') {
                        alert('Incomplete');
                    } else if (state === 'ERROR') {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.error('Error message: ' + errors[0].message);
                            }
                        } else {
                            console.error('Unknown error');
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },
});