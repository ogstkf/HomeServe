/**
 * @File Name          : LC07_AjoutProduitController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : LGO
 * @Last Modified On   : 17-11-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    29/11/2019   RRJ     Initial Version
 **/
({
    doInit: function (component, event, helper) {
        console.log('*** init');
        helper.setNavOptions(component, true, false, false);
        helper.initialize(component);
    },

    handleComponentEvent: function (component, event, helper) {
        var action = event.getParam('action');
        if (action === 'setPB') {
            var pb = event.getParam('data');
            component.set('v.pbk', pb);
        } else if (action === 'searchPBE') {
            component.set('v.isLoading', true);
            var searchTxt = event.getParam('data').searchText;
            var lstPBE = [];
            helper.getPriceBookEntries(component, searchTxt).then(
                $A.getCallback(function (result) {
                    lstPBE = result;
                    var lstQli = component.get('v.lstExistingQLI');
                    var lists = helper.processQLI(lstPBE, lstQli);
                    var lstSelected = component.get('v.lstSelectedPBE');
                    component.set('v.lstAllPBE', lists.lstPBE);
                    helper.preselectPBE(lists.lstPBE, lstSelected, component);
                    component.set('v.isLoading', false);
                })
            );
        } else if (action === 'devistypePBE') {
            var devisType = event.getParam('data');
            helper.handleDevisTypeChange(component, devisType);
            // console.log('##### devisType: ', devisType);



        } 
        else if (action == 'finishBtn') {
            var params = event.getParam('data');
            var isValidForFinish = params.valid;
            console.log('isValidForFinish', isValidForFinish);
            var disableFinish = !isValidForFinish;
            component.set('v.disableFinish', disableFinish);
        } 
        else if (action == 'reloadQLI') {
            helper.reloadQLI(component).then(
                $A.getCallback(function (result) {
                    var editCmp = component.find('editPbe');
                    editCmp.reloadQLIs(result);
                })
            );
        } else if (action == 'toggleLoading') {
            var loading = component.get('v.isLoading');
            var loading = !loading;
            component.set('v.isLoading', loading);
        }
        event.stopPropagation();
    },

    selectRecord: function (component, event, helper) {
        // get the selected record from list
        var getSelectRecord = component.get('v.oRecord');
        // call the event
        var compEvent = component.getEvent('oSelectedRecordEvent');
        // set the Selected sObject Record to the event attribute.
        compEvent.setParams({ recordByEvent: getSelectRecord });
        // fire the event
        compEvent.fire();
    },

    handleNext: function (component, event, helper) {
        var page = component.get('v.currentPage');
        var quo = component.get('v.quo');
        if (page == 'selectCatalog') {
            component.set('v.isLoading', true);
            helper.checkPBId(component);
            component.set('v.isLoading', false);
        } else if (page === 'selectProduct') {
            component.set('v.isLoading', true);
            var lstPBE = component.get('v.lstSelectedPBE');
            console.log('lstPBE', lstPBE);
            var lstId = helper.getListPbeId(lstPBE);
            console.log('lstId', lstId);
            helper.fetchQLI(component, lstId).then(
                $A.getCallback(function (result) {
                    var qlis = Object.assign([], result);
                    var existingQlis = component.get('v.lstNewQLI');

                    qlis.forEach((qli, index, arrQli) => {
                        var extIndex = existingQlis.findIndex(
                            (extQli) => qli.qli.Id === extQli.qli.Id && qli.isExisting === true
                        );
                        if (extIndex !== -1) {
                            arrQli[index] = existingQlis[extIndex];
                            console.log('### eisting: ', existingQlis[extIndex]);
                        }
                    });

                    if (!$A.util.isEmpty(quo.RecordType) && quo.RecordType.DeveloperName == 'Devis_RTC') {
                        helper.fetchRemiseRTC(component, lstId).then(
                            $A.getCallback(function (result) {
                                component.set('v.lstRemiseRTC', result);
                            })
                        );
                    }
                    console.log('OK');
                    helper.handleDescriptionChange(component);
                    helper.changePage(component, 'editProduct');
                    var editCmp = component.find('editPbe');
                    editCmp.reloadQLIs(qlis);
                    helper.setNavOptions(component, false, true, true);
                    component.set('v.isLoading', false);
                })
            );
        } else if (page === 'editProduct') {
        } else {
        }
    },

    handleBack: function (component, event, helper) {
        var prevPage = component.get('v.previousPage');
        var currPage = component.get('v.currentPage');
        helper.changePage(component, prevPage);
        if (currPage === 'editProduct' && prevPage === 'selectProduct' && helper.getPrcBkId === null) {
            helper.setNavOptions(component, true, true, false);
        } else {
            helper.setNavOptions(component, true, false, false);
        }
    },

    handleFinish: function (component, event, helper) {
        console.log('handleFinish ');

        component.set('v.isLoading', true);
        window.setTimeout(function() {
            var lstExisting = component.get('v.lstExistingQLI');
            var lstNewQli = component.get('v.lstNewQLI');
            var quo = component.get('v.quo');
            var pbk = component.get('v.pbk');
            let mapTva = new Map();
            let mapTvaMore = new Map();
            var tvaRemise;
            var isValidRemise;
            console.log('lstNewQli', lstNewQli);
            console.log('**** in finish dmu', lstExisting);
            var countNull = 0;
    
            lstNewQli = helper.buildQlis(lstExisting, lstNewQli);
    
            lstNewQli.forEach((line) => {
                console.log('****  line', line);         
        
                if(line.qli.Taux_de_TVA__c == undefined){
                    line.qli.Taux_de_TVA__c = 0;
                }
    
                if (!mapTva.has(parseFloat(line.qli.Taux_de_TVA__c))) {
                    mapTva.set(parseFloat(line.qli.Taux_de_TVA__c), parseFloat(line.prixTTC));
                }
                else {
                    mapTva.set(parseFloat(line.qli.Taux_de_TVA__c), mapTva.get(parseFloat(line.qli.Taux_de_TVA__c)) + parseFloat(line.prixTTC));
                    mapTvaMore.set(parseFloat(line.qli.Taux_de_TVA__c), true);
                }
    
                if (line.isRemiseExc) {
                    tvaRemise = parseFloat(line.qli.Taux_de_TVA__c);
                    isValidRemise = parseFloat(line.prixTTC) <= 0;
                }
    
                if($A.util.isEmpty(line.qli.Taux_de_TVA__c)){
                    countNull++;
                }
    
            });
    
            if(countNull != 0){
                component.set('v.isLoading', false);
                helper.showToast( 'ERREUR', 'error', 'Vous ne pouvez pas laisser des valeurs de TVA vides, merci de sélectionner un taux de TVA','dismissible');
            }
            // component.set('v.isLoading', false);
            else{
                console.log(tvaRemise);
                console.log(mapTva.get(tvaRemise));
                if (tvaRemise != undefined && !isValidRemise) {
                        component.set('v.isLoading', false);
                        helper.showToast( 'ERREUR',
                                            'error',
                                            'Le montant d\'un produit de remise doit être négatif',
                                            'dismissible');
                }
                else if (tvaRemise != undefined && mapTvaMore.get(tvaRemise) == undefined) {
                    component.set('v.isLoading', false);
                    helper.showToast( 'ERREUR',
                                        'error',
                                        'Le taux de TVA du produit de remise exceptionnelle doit correspondre au taux de TVA d’au moins un des produits du devis, merci de le modifier',
                                        'dismissible');
                }
                else if (tvaRemise != undefined && mapTvaMore.get(tvaRemise) == true && mapTva.get(tvaRemise) < 0) {
                    component.set('v.isLoading', false);
                    helper.showToast( 'ERREUR',
                                        'error',
                                        'Le montant de la remise exceptionnelle dépasse le montant des produits du même taux de TVA, merci de baisser le prix de la remise exceptionnelle',
                                        'dismissible');
                }
                else {
                    helper.finish(component, JSON.stringify(lstNewQli), quo, pbk).then(
                        $A.getCallback(function (result) {
                            // console.log('######## saved: ', result);
                            // helper.closeTab(component);
                            helper.afterFinish(component, result);
                            component.set('v.isLoading', false);
                        }),
                        $A.getCallback(function (error) {
                            helper.showToast('ERREUR', 'error', error[0].message, 'dismissible');
                        })
                    );  
                }
                
                         
            }
        }, 2000);
        

       
                

    },

    handleCancel: function (component, event, helper) {
        helper.closeTab(component);
    },

    closeModel: function (component, event, helper) {
        // console.log('In 2');
        // Set isModalOpen attribute to false
        helper.closeTab(component);
        component.set('v.isModalOpen', false);
    },
});