({
	doInit : function(component, event, helper) {
		var action = component.get("c.instantiateMethod");
		action.setParams({
            "ordId" : component.get("v.recordId")
		});	
		
		action.setCallback(this, function(a){
			var state = a.getState();
			if(state == 'SUCCESS'){
				window.setTimeout(
					$A.getCallback(function() {
						$A.get("e.force:closeQuickAction").fire();
						var ret = a.getReturnValue();
                        var msg = ret.msg;
                        var st = ret.state;
						var toastEvent = $A.get("e.force:showToast");
						if (st === 'success') { //CRA20201701
							toastEvent.setParams({
								title: "SUCCESS",
								type: "success",
								message: msg
							});
						}
						else {
							toastEvent.setParams({
								title: "ERROR",
								type: "error",
								message: msg
							});
						}
						
						toastEvent.fire();	
					}),2000
				);	
			}
			else{
				window.setTimeout(
					$A.getCallback(function() {
						$A.get("e.force:closeQuickAction").fire();

						var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
							title: "ERROR",
							type: "error",
							message: a.getReturnValue()
						});
						toastEvent.fire();
					}),1500
				);	
			}
		});
		$A.enqueueAction(action);

	},

})