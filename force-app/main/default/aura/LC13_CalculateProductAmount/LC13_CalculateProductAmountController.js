/**
 * @File Name          : LC07_CalculateProductAmountController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 05/11/2019, 15:45:33
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    04/11/2019   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        console.log("### doInit Calculate");
        var environment2 = component.get("v.environmentType2");
        console.log("### environment2 zz: ", environment2);
        if (environment2 == "Community") {
            console.log("### in comm 2");
            $A.util.removeClass(component.find("footer_Cn2"), "slds-docked-form-footer");
        }
        helper.handleFlowInit(component);
    },

    handleBack: function(component, event, helper) {
        component.set("v.showAddProd", true);
        console.log('## selected pebes: ' , component.get('v.priceBkEntry'));
        component.set("v.showCalcProdAmt", false);
    },

    handleCancel: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");

        navEvt.setParams({
            recordId: component.get("v.recId")
        });

        navEvt.fire();
    },

    handleSave: function(component, event, helper) {
        console.log('## handleSave@ : ');
        helper.saveOrderlineItem(component, event);
    },

    handleFlowAction: function(component, event, helper) {
        helper.navigateFlow(component, event);
    },

    saveQLIforFlow: function(component, event, helper) {
        component.set("v.showSpinner", true);
        var action = component.get("c.saveQuoteLineItem");
        var priceBkEntrylist = component.get("v.priceBkEntry");

        action.setParams({
            json: JSON.stringify(priceBkEntrylist),
            quoteId: component.get("v.recId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                console.log("SUCCESS");
                var result = response.getReturnValue();
                console.log("## result : ", result);
                helper.navigateFlow(component, event);
            } else if (state === "ERROR") {
                console.log("### ERROR");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "",
                    message: result,
                    type: "error"
                });
                toastEvent.fire();
            }
            component.set("v.showSpinner", false);
        });

        $A.enqueueAction(action);
    }
});