/**
 * @File Name          : LC07_CalculateProductAmountHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 06/11/2019, 21:16:53
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    04/11/2019   RRJ     Initial Version
 **/
({
    saveOrderlineItem: function(component, event) {
        var action = component.get("c.saveOrderlineItem");

        console.log("### 11");

        var priceBkEntrylist = component.get("v.priceBkEntry");
        console.log("### priceBkEntrylist : ", priceBkEntrylist);
        console.log("### priceBkEntrylist josn: ", JSON.stringify(priceBkEntrylist));

        // DMU: 20190326 - query mode de gestion from object Product & make mode de gestion editable for saving
        action.setParams({
            json: JSON.stringify(priceBkEntrylist),
            orderId: component.get("v.recId")
        });

        console.log("### 111");

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                console.log("SUCCESS");

                var result = response.getReturnValue();
                console.log("## result : ", result);

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "",
                    message: result,
                    type: "success"
                });
                toastEvent.fire();

                setTimeout(function() {
                    window.parent.location = "/" + component.get("v.recId");
                }, 2000);
            } else if (state === "ERROR") {
                console.log("### ERROR");
                console.log('result:' , response);


                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "",
                    message: response.getError()[0].message,
                    type: "error"
                });
                toastEvent.fire();
            }
        });

        $A.enqueueAction(action);
    },

    fetchPickListVal: function(component, fieldName, elementId) {
        // DMU: 20190326 - query mode de gestion from object Product & make mode de gestion editable for saving

        var action = component.get("c.getPickListValueModeGestion");
        action.setParams({
            prodId: component.get("v.priceBkEntry").Product2Id
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("### allValues", allValues);

                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                console.log('###  component.find("mgestion")', component.find("mgestion"));
                component.find("mgestion").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },

    iter: function(value) {
        return value;
    },

    handleFlowInit: function(component) {
        var availableActions = component.get("v.availableActions");
        console.log("#### availableActions: ", availableActions);
        if (!$A.util.isEmpty(availableActions)) {
            component.set("v.isFlow", true);
            for (var i = 0; i < availableActions.length; i++) {
                if (availableActions[i] == "PAUSE") {
                    component.set("v.canPause", true);
                } else if (availableActions[i] == "BACK") {
                    component.set("v.canBack", true);
                } else if (availableActions[i] == "NEXT") {
                    component.set("v.canNext", true);
                } else if (availableActions[i] == "FINISH") {
                    component.set("v.canFinish", true);
                }
            }
            var selectedPBEs = component.get("v.selectedPrcBkEntriesJSON");
            component.set("v.priceBkEntry", JSON.parse(selectedPBEs));
            console.log('##handleFlow Initi: pbes: '  , component.get("v.priceBkEntry"));
        }
    },

    navigateFlow: function(component, event) {
        var actionClicked = event.getSource().getLocalId();
        var navigate = component.get("v.navigateFlow");
        navigate(actionClicked);
    }
});