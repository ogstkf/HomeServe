({
    doInit : function(component, event, helper) {
        var createAcountContactEvent = $A.get("e.force:createRecord");
                
        var today = new Date().getFullYear() + '-' + parseInt(new Date().getMonth() + 1) + '-' + new Date().getDate();   
        console.log('#### today: ', today);        
        
        var action = component.get('c.getSTforUserconnected');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('#### response get ag|Id: ', response.getReturnValue());
                var a_Id = (response.getReturnValue()==null || response.getReturnValue()=='')?null:response.getReturnValue();
                createAcountContactEvent.setParams({
                    "entityApiName": "Order",
                    "defaultFieldValues": {
                        'Status' : 'Demande d\'achats',
                        'Agence__c' : a_Id,           
                        'EffectiveDate' : today
                    }
                });
                createAcountContactEvent.fire();
            } else if (state === 'INCOMPLETE') {
                alert('Incomplete');
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    } 
})