/**
 * @File Name          : LC01_Acc360RendezVousHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 24/06/2019, 18:39:53
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    21/06/2019, 16:35:20   RRJ     Initial Version
 **/
({
    populateData: function(component, accId) {
        var action = component.get("c.fetchRDV");
        action.setParams({
            accId: accId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.lstRDV", response.getReturnValue());
                component.set("v.loading", false);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        this.showToast("ERROR", "error", errors[0].message);
                        component.set("v.loading", false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    annulerRendezVouz: function(component, rdvId) {
        var action = component.get("c.annulerRdv");

        action.setParams({
            rdvId: rdvId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.populateData(component, component.get("v.recordId"));
                this.showToast("SUCCESS", "success", "record saved successfully!");
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        this.showToast("ERROR", "error", errors[0].message);
                        component.set("v.loading", false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    showToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    }
});