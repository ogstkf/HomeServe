/**
 * @File Name          : LC01_Acc360RendezVousController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 24/06/2019, 17:45:51
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    21/06/2019, 16:09:09   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        helper.populateData(component, component.get("v.recordId"));
    },

    handleDMLChanges: function(component, event, helper) {
        var rdvId = event.getParam("evtData");
        helper.annulerRendezVouz(component, rdvId);
        component.set("v.loading", true);
    }
});