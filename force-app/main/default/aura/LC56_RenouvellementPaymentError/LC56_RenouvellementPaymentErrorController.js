/**
 * @File Name          : LC56_RenouvellementPaymentErrorController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 4/22/2020, 10:44:42 AM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/2/2020   RRJ     Initial Version
 **/
({
    handleRetour: function (component, event, helper) {
        if (window.location !== window.parent.location) {
            console.log(' in iframe');
            var lexOrigin = window.location.origin;
            console.log('### orig: ', lexOrigin);
            console.log('### loc: ', window.location);
            var data = {
                success: false,
                action: 'goToContrat',
            };
            parent.postMessage(JSON.stringify(data), lexOrigin);
        } else {
            console.log('#### is NOT in iframe');
            window.location.href = '/s/';
        }
    },

    handleRetourPaiement: function (component, event, helper) {
        if (window.location !== window.parent.location) {
            console.log(' in iframe');
            var lexOrigin = window.location.origin;
            console.log('### orig: ', lexOrigin);
            console.log('### loc: ', window.location);
            var data = {
                success: false,
                action: 'goToPaiement',
            };
            parent.postMessage(JSON.stringify(data), lexOrigin);
        } else {
            console.log('#### is NOT in iframe');
            window.location.href = '/s/';
        }
    },
});