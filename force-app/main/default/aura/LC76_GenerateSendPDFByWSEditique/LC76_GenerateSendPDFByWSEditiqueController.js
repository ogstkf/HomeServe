({
/*    doInit : function(component, event, helper) {
        var action = component.get("c.att");
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === 'SUCCESS') {
                var string64 = response.getReturnValue();
                console.log(string64);
                var blob = helper.convertToBlob(string64);

                var objectUrl = URL.createObjectURL(blob);

                var urlEvent = $A.get("e.force:navigateToURL");
                
                
                if (urlEvent) {
                    urlEvent.setParams({
                        "url": objectUrl
                    });
                    urlEvent.fire();
                }
                    
            }
            else {
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    */
    doInit: function(component, event, helper) {
        var action = component.get("c.createJSON");
        
        action.setParams({
            "Id": component.get("v.recordId"),
            "isPreview": true,
            "isAnonymise": true,
            "isGenerate" : false,
            "template": null,
            "documentId": null
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log(state);

            var toastEvent = $A.get('e.force:showToast');
            if (state === 'SUCCESS') {
                toastEvent.setParams({
                    title: 'SUCCESS',
                    type: 'success',
                    message: 'success'
                });
                toastEvent.fire();
                console.log(response.getReturnValue());
                var string64 = response.getReturnValue();
                
                var byteCharacters = atob(string64);
                var byteArrays = [];
                
                for (var offset = 0; offset < byteCharacters.length; offset += 512) {
                    var slice = byteCharacters.slice(offset, offset + 512);
                    
                    console.log(slice);
                    
                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }
                    
                    var byteArray = new Uint8Array(byteNumbers);
                    
                    byteArrays.push(byteArray);
                }
                
                var blob = new Blob(byteArrays, {type: 'application/pdf'});

                var objectUrl = URL.createObjectURL(blob);

                console.log('1');
                var urlEvent = $A.get("e.force:navigateToURL");
                
                
                if (urlEvent) {
                    urlEvent.setParams({
                        "url": objectUrl
                    });
                    urlEvent.fire();
                }
                
            }
            else {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})