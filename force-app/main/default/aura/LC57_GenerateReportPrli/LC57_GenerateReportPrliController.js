/**
 * @File Name          : LC57_GenerateReportPrliController.js
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 3/20/2020, 2:04:50 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/20/2020   RRJ     Initial Version
**/
({
    doInit: function(component, event, helper) {
        console.log('doInit start');
        helper.initComponent(component, event, helper);
    }
});