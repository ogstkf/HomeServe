/**
 * @File Name          : LC57_GenerateReportPrliHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 3/20/2020, 2:04:57 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/13/2020   RRJ     Initial Version
 **/
({
    initComponent: function(component, event, helper) {
        var recordIds = component.get('v.pageReference').state.c__ids;
        var listviewId = component.get('v.pageReference').state.c__listviewid;
        component.set('v.listviewId', listviewId);

        var action = component.get('c.getDataReport');
        action.setParams({
            recordIds: recordIds
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('From server: ', response.getReturnValue());
                console.log('Product request line items id: ' + recordIds);
                var dataReturned = response.getReturnValue();

                this.navigationtoReport(component, event, helper, dataReturned);
                component.set('v.show_spinner', false);
            } else if (state === 'INCOMPLETE') {
                console.log('incomplete');
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                        this.showToast('Erreur!', 'error', errors[0].message);
                        this.closeFocusedTab(component);
                        this.navigateToList(component);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });

        $A.enqueueAction(action);
        console.log('Init component end');
    },

    // openReportNewTab: function(component, event, helper, productNames) {
    //     var reportid = $A.get("$Label.c.Report_Id_Eau_Du_Stock");
    //     console.log("reportId" + reportid);
    //     var action = component.get("c.baseUrl");
    //     action.setCallback(this, function(response) {
    //         var state = response.getState();
    //         if (state === "SUCCESS") {
    //             console.log("URL: ", response.getReturnValue());
    //             var url = response.getReturnValue();
    //             window.open("" + url + "/lightning/r/Report/" + reportid + "/view?queryScope=userFolders&fv0=" + productNames + "");
    //             component.set("v.showPage", true);
    //             component.set("v.show_spinner", false);
    //         } else if (state === "INCOMPLETE") {
    //             console.log("incomplete");
    //         }
    //     });

    //     $A.enqueueAction(action);
    // },

    // navigate: function(component, event, helper, productNames) {
    //     console.log("Navigate to url");
    //     var reportid = $A.get("$Label.c.Report_Id_Eau_Du_Stock");

    //     var action = component.get("c.baseUrl");
    //     action.setCallback(this, function(response) {
    //         var state = response.getState();
    //         if (state === "SUCCESS") {
    //             console.log("URL: ", response.getReturnValue());
    //             var url = response.getReturnValue();

    //             var urlEvent = $A.get("e.force:navigateToURL");
    //             urlEvent.setParams({
    //                 url: "" + url + "/lightning/r/Report/" + reportid + "/view?queryScope=userFolders&fv0=" + productNames + ""
    //             });
    //             urlEvent.fire();
    //             component.set("v.showPage", true);
    //             component.set("v.show_spinner", false);
    //         } else if (state === "INCOMPLETE") {
    //             console.log("incomplete");
    //         }
    //     });

    //     $A.enqueueAction(action);
    // },

    navigationtoReport: function(component, event, helper, productNames) {
        var workspaceAPI = component.find('workspace');
        var self = this;

        var reportid = $A.get('$Label.c.Report_Id_Eau_Du_Stock');

        var action = component.get('c.baseUrl');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('URL: ', response.getReturnValue());
                var baseUrl = response.getReturnValue();
                var reportUrl =
                    baseUrl + '/lightning/r/Report/' + reportid + '/view?queryScope=userFolders&fv0=' + productNames;

                debugger;
                // window.open(reportUrl);

                workspaceAPI.isConsoleNavigation().then(function(isConsole) {
                    console.log(isConsole);

                    if (isConsole) {
                        // window.location.href = reportUrl;
                        workspaceAPI.getFocusedTabInfo().then(function(response) {
                            var focusedTabId = response.tabId;
                            var navService = component.find('navService');
                            var pageRef = {
                                type: 'standard__webPage',
                                attributes: {
                                    url: reportUrl
                                }
                            };
                            navService.navigate(pageRef);
                            workspaceAPI.closeTab({ tabId: focusedTabId });
                        });
                    } else {
                        var navService = component.find('navService');
                        var pageRef = {
                            type: 'standard__objectPage',
                            attributes: {
                                objectApiName: 'ProductRequestLineItem',
                                actionName: 'list'
                            },
                            state: {
                                filterName: component.get('v.listviewId')
                            }
                        };
                        navService.navigate(pageRef);
                        // window.location.href = reportUrl;
                        window.open(reportUrl);
                    }
                });

                component.set('v.showPage', true);
                component.set('v.show_spinner', false);
            } else if (state === 'INCOMPLETE') {
                console.log('incomplete');
            }
        });

        $A.enqueueAction(action);
    },

    showToast: function(title, type, message) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    },

    closeTab: function(component) {
        var workspaceAPI = component.find('workspace');
        workspaceAPI
            .getFocusedTabInfo()
            .then(function(response) {
                var focusedTabId = response.tabId;
                workspaceAPI.closeTab({ tabId: focusedTabId });
            })
            .catch(function(error) {
                console.log(error);
            });
    },

    navigateToList: function(component) {
        console.log('## ListviewId; ', component.get('v.listviewId'));
        var lvId = component.get('v.listviewId');
        var navEvent = $A.get('e.force:navigateToList');
        navEvent.setParams({
            listViewId: lvId,
            scope: 'ProductRequestLineItem'
        });
        navEvent.fire();
    }
});