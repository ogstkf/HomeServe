({
    helperMethod : function() {

    },
    getLastDayOfMonth : function getLastDayOfMonth(month) {
        var lastDay;
        if(month==1|| month==3 || month==5 || month==7 || month==8 || month==10 || month==12){
            lastDay = 31;
        }
        else if(month==4 || month==6 || month==9 || month==11){
            lastDay = 30;
        }else{
            lastDay = 28; //In case of February
        }
        return lastDay;
    },
    
    checkRecord: function(component, event, helper, recordId){
        console.log('## checkrecord start');

        //check record in global list
        // component.get();
        //add to list of records
        var listClis = component.get("v.selectedCli");

        var currentSelectedId = event.getSource().get("v.name");
        console.log('currentSelectedId', currentSelectedId);

        listClis.push(currentSelectedId);
        
        component.set("v.selectedCli", listClis);

    },

    uncheckRecord: function(component, event, helper, recordId){
        console.log('## uncheckrecord start')

        //uncheck record in global list

        //remove from list
        var currentSelectedId = event.getSource().get("v.name");
        var listClis = component.get("v.selectedCli");

        for(var i = 0 ; i < listClis.length; i++){
            console.log('listClis', listClis[i]);

            if(listClis[i] == currentSelectedId){
                listClis.splice(i, 1);
            }
        }

        component.set("v.selectedCli", listClis);

        
    },

    showToast: function(title, type, message, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: mode,
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    },

    loadCLICollectiviteAggr:  function(cmp, helper, index, currYear) {
        cmp.set("v.isLoading", true); 
        console.log('*** index loadCLICollectiviteAggr:',index);     
        console.log('*** currYear loadCLICollectiviteAggr:',currYear);  

        var action = cmp.get("c.getContractLineItem");
        action.setParams({
            "index":index,
            "currYear":currYear,
        });        
        action.setCallback(this, function(response){
            if (response.getState() === 'SUCCESS') {
                console.log('*** data lstCLI:',response.getReturnValue());
                cmp.set('v.lstCLI', response.getReturnValue());
            }
         });
         $A.enqueueAction(action); 
        cmp.set("v.isLoading", false); 
    },

    loadResource:  function(cmp, helper, index, currYear) {
        console.log('*** index loadResource:',index);     
        console.log('*** currYear loadResource:',currYear);  

        var actionResource = cmp.get("c.retrieve");
        actionResource.setParams({
            "year":currYear,
            "month":index       
        });

        actionResource.setCallback(this, function(response){
            var data = response.getReturnValue();
            console.log('*** data resource:',data.result);
            
            cmp.set('v.resources', data.result);

        });
        $A.enqueueAction(actionResource);
    }
})