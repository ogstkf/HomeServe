({
    HandleArrows : function(component, event, helper) {
        console.log('*** in HandleArrows');

        var index = component.get("v.IndexCalendar");        
        var currYear = component.get("v.CalendarYear");

        helper.loadCLICollectiviteAggr(component, helper, component.get("v.IndexCalendar")+1, component.get("v.CalendarYear") );

        //retrieve serviceResource      
        //Had to use getMonth() + 1 to get month (zero-index based) because javascript implementation followed JAVA & that is how java.util.Date did it.
        helper.loadResource(component, helper, component.get("v.IndexCalendar")+1 , component.get("v.CalendarYear"));        
    },

    loadData: function (component, event, helper) {
        console.log('*** in loadData');
        var TodayDate = new Date();   
           
        helper.loadCLICollectiviteAggr(component, helper, TodayDate.getMonth()+1, TodayDate.getFullYear() );        
        
        helper.loadResource(component, helper, TodayDate.getMonth()+1, TodayDate.getFullYear());              
    },
    handleChangeCheckbx : function(component, event, helper) {
        // console.log('### ct selected start');
        // console.log('event current target : ' , event);       

        var checked = event.getSource().get('v.checked');
        var recordId = event.getSource().get("v.name");

        if(checked == false){
            helper.uncheckRecord(component, event, helper, recordId);
        }else{
            helper.checkRecord(component, event, helper, recordId);
        }        
    }, 
    handleMainChangeCheckbx: function(component, event, helper) {

        var checkvalue = component.find("selectAll").get("v.checked");        
        console.log('checkvalue : ',checkvalue);
        var lstCLI =[];

        var lstCDL = component.get("v.lstCLI");
        (lstCDL).forEach(function(record){

            record.selected=checkvalue;
            lstCLI.push(record);
        });

        console.log('lstCLI3 : '+lstCLI);

        component.set("v.lstCLI", lstCLI); 

    },
    generateVE: function(component, event, helper){        

        var list = component.get('v.lstCLI');
        console.log('*** list: ' , list);
        //debugger;
        console.log('*** json: ',JSON.parse(JSON.stringify(list)));
        
        var listSelected = component.get("v.selectedCli");
        console.log('*** listSelected: ' , listSelected);

        listSelected.forEach(line =>{
            if(!$A.util.isEmpty(line.resource1)){
                line.res1Id = line.resource1.val;
                delete line.resource1;
            }
            if(!$A.util.isEmpty(line.resource2)){
                line.res2Id = line.resource2.val;
                delete line.resource2;
            }
            if(!$A.util.isEmpty(line.resource3)){
                line.res3Id = line.resource3.val;
                delete line.resource3;
            }
        })
        console.log('listSelected: ' , listSelected);    
        
        var action = component.get("c.saveVE");
                
        var isEmpty = (listSelected.toString() == '' );
        
        if(!isEmpty){
        var isEmpty = (listSelected.toString() == '' );
            var msgConfirm = 'Merci de confirmer la génération des VE Collectives ';// + listSelected.length + ' VE Collectives';
            
            var missingResource = '';
            listSelected.forEach(line =>{          
                // console.log('line.res1Id: ' , line.res1Id);   
                console.log('line.startDate: ' , line.startDate);  
                console.log('line.endDate: ' , line.endDate);  

                if(typeof line.res1Id === 'undefined' && typeof line.res2Id === 'undefined' && typeof line.res3Id === 'undefined'){
                    missingResource = 'YES';
                }else{                    
                    missingResource = 'NO';
                }

                if((typeof line.startDate === 'undefined') || (typeof line.endDate === 'undefined')){
                    missingResource = 'YES';
                }else{
                    missingResource = 'NO';
                }
            })
            if(missingResource == 'YES'){
                helper.showToast("Erreur" ,"error", "Veuillez renseigner un des Ressources et la date de début et la date de fin pour les lignes que vous avez choisies", "dismissible");
            }else{                
                if (confirm(msgConfirm)) {                                    
                    action.setParams({
                        JSONlstWrap : JSON.stringify(listSelected)
                    });   
            
                    action.setCallback(this, function(response){                         
                        var data = response.getReturnValue();
                        console.log('generE VE',data);
                        if(data.error){
                            helper.showToast("Erreur" ,"error", data.msg, "dismissible");
                        }else{
                            helper.showToast("Succès", "success", data.msg, "dismissible");
                        }
                    });
            
                    console.log("123 generE VE")
                    $A.enqueueAction(action);
                    
                } else {
                    // Do nothing!
                }
            }         
        }
        else{
            helper.showToast("Erreur" ,"error", "Veuillez séléctionner au moins une ligne.", "dismissible");
        }        
    },
    handleBack: function(component, event, helper){
        console.log('*** handleBack');
        // var evt = $A.get("e.force:navigateToComponent");
        // evt.setParams({
        //     componentDef: "c:LC21_NavigateToLC20",
        //     // componentAttributes: {
        //     //     CalendarValue : "",
        //     //     CalendarYear : "2020",
        //     //     IndexCalendar : ""
        //     // }
        // });
        // evt.fire(); 

        // var urlEvent = $A.get("e.force:navigateToURL");
        // urlEvent.setParams({
        //     'url': 'https://cham-sf-prod--hquat.lightning.force.com/one/one.app?source=alohaHeader#eyJjb21wb25lbnREZWYiOiJjOkxDMjBfTW9kdWxlRGVQbGFuaWZpY2F0aW9uIiwiYXR0cmlidXRlcyI6e30sInN0YXRlIjp7fX0%3D'
        // });
        // urlEvent.fire();    
        
        $A.get('e.force:refreshView').fire();
    },

    
})