/**
 * @File Name          : LC07_NewAssetHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 10/12/2019, 15:05:41
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/12/2019   RRJ     Initial Version
 **/
({
    // helperMethod : function() {

    // }

    /** handles create new record and sets default values to screen (account id)
     * @param  {Object} component - Lightning component object
     * @param  {Object} event - lightning event object
     * @param  {Object} helper - lightning helper
     * @return none
     */
    createNewRecord: function(component) {
        var url = new URL(document.location.href);
        var recTypeId = url.searchParams.get("recordTypeId");
        var additionalParams = url.searchParams.get("additionalParams");

        console.log("#### additionalParams: ", additionalParams);
        if (!$A.util.isEmpty(additionalParams)) {
            var recIdIndex = additionalParams.indexOf("a0j");
            if (recIdIndex !== -1) {
                console.log("#### recIdIndex: ", recIdIndex);
                var recordId = additionalParams.substring(recIdIndex).slice(0, 15);
                console.log("#### recordId: ", recordId);
                var action = component.get("c.fetchDefaultValues");
                action.setParams({
                    logementId: recordId
                });

                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log("##### resp success: ", response.getReturnValue());
                        //after successful callback
                        //build defaultVals
                        var defaultVals = response.getReturnValue();
                        this.createRecord("Asset", recTypeId, defaultVals);
                    } else if (state == "INCOMPLETE") {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "Oops!",
                            message: "No Internet Connection"
                        });
                        toastEvent.fire();
                    } else if (state == "ERROR") {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: "Error!",
                            message: "Please contact your administrator"
                        });
                        toastEvent.fire();
                    }
                });

                $A.enqueueAction(action);
            } else {
                this.createRecord("Asset", recTypeId, null);
            }
        } else {
            this.createRecord("Asset", recTypeId, null);
        }
    },

    createRecord: function(objName, recTypeId, defaultValues) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            entityApiName: objName,
            recordTypeId: recTypeId,
            defaultFieldValues: defaultValues
        });
        createRecordEvent.fire();
    }
});