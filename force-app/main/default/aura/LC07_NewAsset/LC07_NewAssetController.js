({
    // myAction : function(component, event, helper) {

    // }

    /**
     * Method used to launch the create record screen
     * @param  Object component - the lightning component
     * @param  Object event - the lightning event object
     * @param  Object helper - the lightning helper object
     * @return none
     */
    createRecord: function(component, event, helper) {
        helper.createNewRecord(component);
    }
});