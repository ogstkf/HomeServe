/**
 * @File Name          : LC_NavigateToListviewController.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 04/05/2020, 17:04:58
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/05/2020   ZJO     Initial Version
**/
({
    doInit: function (component, event, helper) {
        component.set("v.showSpinner", true);

        var servConId = component.get('v.recordId');

        var pageReference = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__LC_ListViewCmp',
            },
            state: {
                "c__contractId": servConId
            }
        };
        component.set("v.pageReference", pageReference);
    },

    handleClick: function (component, event, helper) {
        var navService = component.find("navService");
        var pageReference = component.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);
    }
})