({
    initOptions : function(component) {
        var sendOptions = [
            {
                label: 'Générer et envoyer par mail à :',
                value: 'generer_email'
            },
            {
                label: 'Générer et envoyer par courrier à :',
                value: 'generer_courrier'
            },
            {
                label: 'Générer le document sans l\'envoyer',
                value: 'generer'
            }
        ];

        component.set("v.sendOptions", sendOptions);
    },

    initData : function(component) {
        var action = component.get("c.pickListvalue");
        action.setParams({
            "recordId": component.get("v.recordId")
            //"recordId": "0Q00Q000000TvTWSA0"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('1');
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                var list = response.getReturnValue();
                var accountInfo = {};
                for(var i = 0; i < list.length ; i++) {
                    if (list[i].label == 'document' && list[i].data != null) {
                        component.set("v.chooseDoc", true);
                        var documentOptions = [];
                        list[i].data.forEach(elt => {
                            documentOptions.push({
                                label: elt.label,
                                value: elt.value
                            });
                        });
                        component.set("v.documentOptions", documentOptions);
                    }
                    else if (list[i].label == 'sendMethod') {
                        list[i].data.forEach(elt => {
                            switch (elt.label) {
                                case "Type":
                                    accountInfo.Type = elt.value;
                                    break;
                                case "Email":
                                    accountInfo.Email = elt.value;
                                    break;
                                case "Name":
                                    accountInfo.Name = elt.value;
                                    break;
                                case "LastName":
                                    accountInfo.LastName = elt.value;
                                    break;
                                case "FirstName":
                                    accountInfo.FirstName = elt.value;
                                    break;
                                case "Salutation":
                                    accountInfo.Salutation = elt.value;
                                    break;
                                case "BillingStreet":
                                    accountInfo.BillingStreet = elt.value;
                                    break;
                                case "BillingPostalCode":
                                    accountInfo.BillingPostalCode = elt.value;
                                    break;
                                case "BillingCity":
                                    accountInfo.BillingCity = elt.value;
                                    break;
                                case "Contacts":
                                    accountInfo.Contacts = [];
                                    elt.value.forEach(cons => {
                                        var valueCon = {};
                                        cons.forEach(con => {
                                            switch (con.label) {
                                                case "Id":
                                                    valueCon.Id = con.value;
                                                    break;
                                                case "LastName":
                                                    valueCon.LastName = con.value;
                                                    break;
                                                case "FirstName":
                                                    valueCon.FirstName = con.value;
                                                    break;
                                                case "Email":
                                                    valueCon.Email = con.value;
                                                    break;
                                            }
                                            valueCon.value = false;                                 //valeur du checkbox true ou false
                                        });
                                        accountInfo.Contacts.push(valueCon);
                                    });
                                    break;
                                default:
                            }
                            
                        });
                    }
                }
                component.set("v.accountInfo", accountInfo);
                console.log(accountInfo);
                this.checkValidation(component);
                component.set("v.isLoading", false);
            }
            else
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()));
                         
        });
        $A.enqueueAction(action);
    },

    checkValidation : function(component) {
        var acc = component.get("v.accountInfo");
        if (acc.Type === 'PersonAccount' && acc.Email === '') {
            document.getElementById('generer_email').disabled = true;
        }
        if (acc.Type === 'BusinessAccount') {
            var b = false;
            console.log(acc.Contacts);
            if (acc.Contacts == null)
                b = true;
            else {
                acc.Contacts.forEach(con => {
                    if (con.Email !== '')
                        b = true;
                });
            }
            if (!b)
                document.getElementById('generer_email').disabled = true;
        }
        
        if (acc.BillingStreet === '' || acc.BillingPostalCode === '' || acc.BillingCity === '') {
            document.getElementById('generer_courrier').disabled = true;
        }
    },

    showMessage : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        if (toastEvent) {
            toastEvent.setParams({
                "title": title,
                "type": type,
                "message": message
            });
            toastEvent.fire();
        }
    },

    genererEmail : function(component) {
        var emails = [];
        var acc = component.get("v.accountInfo");
        var toastMessage = 'L’envoi par mail à';
        if (acc.Type === 'PersonAccount') {
            emails.push(acc.Email);
            toastMessage += ' ' + acc.Name;
        }
        else if (acc.Type === 'BusinessAccount') {
            var isFirst = true;
            acc.Contacts.forEach(con => {
                if (con.value === true) {
                    emails.push(con.Email);
                    if (!isFirst)
                        toastMessage += ',';
                    if (con.FirstName !== '')
                        toastMessage += ' ' + con.FirstName;
                    if (con.LastName !== '')
                        toastMessage += ' ' + con.LastName;
                }
            });
        }
        console.log(emails);
        toastMessage += ' est terminé';

        var action = component.get("c.sendMail");
        action.setParams({
            "Id": component.get("v.recordId"),
            "Emails": emails
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                this.showMessage("Success", "success", toastMessage);
            }
            else {
                console.log(response.getError()[0].message);
                this.showMessage("Error", "error", response.getError()[0].message);
            }
            $A.get('e.force:closeQuickAction').fire();
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    },

    generer : function(component, type) {
        this.callWebService(component, type, true);
    },

    callWebService : function(component, type, isGenerate) {
        var action = component.get("c.updateDocumentId");                   //update the documentId before calling WebService
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log('DocumentId :' + response.getReturnValue());
                this.createJSON(component, type, isGenerate, response.getReturnValue());                     //Call WebService
            }
            else {
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },

    createJSON : function(component, type, isGenerate, docId) {

        var action = component.get("c.createJSON");
        var template = null;
        var isAnonymise = true;
        if (component.get("v.chooseDoc")) {
            if (component.get("v.documentValue") === 'anonymise')
                isAnonymise = true;
            else if (component.get("v.documentValue") === 'non_anonymise')
                isAnonymise = false;
            else if (component.get("v.documentValue") === 'sans_prix')
                template = 'DEVIS_TECHNIC_TCH';

        }
        action.setParams({
            "Id": component.get("v.recordId"),                                                  //version acheminement avec stockage
            "isPreview": isGenerate,
            "isAnonymise": isAnonymise,
            "isGenerate" : isGenerate,
            "template": template,
            "documentId": docId,
            "isWatermark": true
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var body = {};
            if (JSON.parse(response.getReturnValue()))
                body = JSON.parse(response.getReturnValue());
            else
                body.message = '';
            var titleToast = 'ERROR';
            var typeToast = 'error';
            if (body.statusCode >= 200 && body.statusCode < 300) {
                titleToast = 'SUCCESS';
                typeToast = 'success';
            }

            if (state === 'SUCCESS') {
                this.showMessage(titleToast, typeToast, body.message);
                if (isGenerate) {
                    if (type === 'generer_email') {
                        this.genererEmail(component);
                    }
                    else if (type === 'generer_courrier') {                                         //version acheminement avec stockage
                        console.log(type);
                        this.callWebService(component, type, false);
                    }
                }
            }
            else {
                console.log(response.getError());
            }
            if (type === 'generer' || (type === 'generer_courrier' && !isGenerate)) {                                                            //version acheminement avec stockage
                $A.get('e.force:closeQuickAction').fire();
                $A.get('e.force:refreshView').fire();
            }
            
            
        });
        $A.enqueueAction(action);

    }
})