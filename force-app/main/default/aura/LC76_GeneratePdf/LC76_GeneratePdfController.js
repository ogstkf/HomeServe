({
    doInit : function(component, event, helper) {
        helper.initOptions(component);
        helper.initData(component);
    },

    changeCheckBox : function(component, event, helper) {
        var target = event.target;
        var contacts = component.get("v.accountInfo.Contacts");
        if(target.checked) {
            document.getElementById('generer_email').checked = true;
            contacts.forEach(con => {
                if (target.id === con.Id) {
                    con.value = true;
                }
            });
        }
        else {
            contacts.forEach(con => {
                if (target.id === con.Id) {
                    con.value = false;
                }
            });
        }
    },

    changeRadio : function(component, event, helper) {
        var acc = component.get("v.accountInfo");
        if (acc.Type === 'BusinessAccount' && document.getElementById('generer_email').checked === false) {
            acc.Contacts.forEach(con => {
                con.value = false;
            });
        }
        component.set("v.accountInfo", acc);
    },

    cancel : function(component, event, helper) {
        var close = $A.get("e.force:closeQuickAction");
        close.fire();
    },

    valider : function(component, event, helper) {
        var acc = component.get("v.accountInfo");
        var action = '';
        if (document.getElementById('generer_email').checked === true)
            action = 'generer_email';
        else if (document.getElementById('generer_courrier').checked === true)
            action = 'generer_courrier';
        else if (document.getElementById('generer').checked === true)
            action = 'generer';


        if (acc.Type === 'BusinessAccount' && document.getElementById('generer_email').checked === true) {
            var b = false;
            acc.Contacts.forEach(con => {
                if (con.value === true)
                    b = true;
            });
            if(!b)
                helper.showMessage("Erreur", "error", "Vous devez au moins selectionner un email");
            else {
                component.set("v.isLoading", true);
                helper.generer(component, action);
            }
        }
        else {
            component.set("v.isLoading", true);
            
            helper.generer(component, action);
        }
    },
})