({
    doInit: function (component, event, helper){
        let vfOrigin = $A.get("$Label.c.CommunityCaptchaURL");
        window.addEventListener("message", function(event) {
            if (event.origin !== vfOrigin) {
                // Not the expected origin: Reject the message!
                return;
            } 
            if (event.data){  
                component.set("v.captchaToken", event.data);
                const captchaOK = component.get("v.captchaOK")
                if (captchaOK) {
                    $A.enqueueAction(captchaOK);
                };
                
     
                
                
              	//let myButton = cmp.find("myButton")
                //if (myButton) {
                //	myButton.set('v.disabled', false)
                //}
                
                //resize iframe
                console.log("resizing iframe");
                var captcha = component.find('captcha');
        		//$A.util.removeClass(captcha, 'big');
                //$A.util.addClass(captcha, 'small');
 
            }            
        }, false);                
    },
    reload: function(component, event, helper) {
        document.getElementById("captcha").contentWindow.postMessage("reset");
    }
})