({
    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }, 

    closeAction : function() {
        var closeQuickActionEvt = $A.get("e.force:closeQuickAction");
        if (closeQuickActionEvt)
            closeQuickActionEvt.fire();
    }
})