({
    cloneQuote : function(component, event, helper) {
        component.set("v.isLoading", true);

        //var quoteName = component.find('quoteName').get('v.value');
        var quoteName = component.get("v.name");
        console.log(quoteName);
        if (quoteName === '' || quoteName == null) {
            component.set("v.isLoading", false);
            helper.showToast('Erreur', 'Le nom du devis doit au moins contenir 1 charactère', 'error');
        }
        else {
            var action = component.get("c.clone");
            action.setParams({
                quoId : component.get("v.recordId"),
                quoName : quoteName
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(state);
                if (state === 'SUCCESS') {
                    helper.showToast("Succès", "Le devis a été cloné avec succès", "success");
                    let navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                      "recordId": response.getReturnValue(),
                      "slideDevName": "details"
                    });
                    navEvt.fire();
                }
                else {
                    helper.showToast("Erreur", "Le devis n'a pas pu être cloné", 'error');
                    console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()));
                    helper.closeAction();
                }
            });
            $A.enqueueAction(action);
        }
    },

    closeAction : function(component, event, helper) {
        helper.closeAction();
    }
})