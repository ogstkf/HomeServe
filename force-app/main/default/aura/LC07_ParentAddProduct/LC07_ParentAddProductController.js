/**
 * @File Name          : LC07_ParentAddProductController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 05/12/2019, 14:41:31
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    29/11/2019   RRJ     Initial Version
 **/
({
    init: function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");

        console.log(component.get("v.recordId"));
        evt.setParams({
            componentDef: "c:LC07_AjoutProduit",
            // componentDef: "c:LC07_AddProduct",
            componentAttributes: {
                recordId: component.get("v.recordId")
            }
        });
        evt.fire();
    }
});