({
    initData : function(lstSA, lstSAEmail, lstSACourrier, lstSAError) {
        lstSA.forEach(elt => {
            let SA = [];
            if (elt.acc == null) {
                SA.Id = elt.sa.Id;
                SA.Number = elt.sa.AppointmentNumber;
                lstSAError.push(SA);
            }
            else {
                if (elt.acc.RecordType.DeveloperName === 'PersonAccount') {
                    if (elt.acc.PersonEmail == null) {
                        if (elt.acc.BillingStreet != null && elt.acc.BillingPostalCode != null && elt.acc.BillingCity != null) {
                            SA.Id = elt.sa.Id;
                            SA.Number = elt.sa.AppointmentNumber;
                            SA.AccId = elt.acc.Id;
                            SA.FullName = elt.acc.Name;
                            SA.Address = elt.acc.BillingStreet + ' ' + elt.acc.BillingPostalCode + ' ' + elt.acc.BillingCity;
                            lstSACourrier.push(SA);
                        }
                        else {
                            SA.Id = elt.sa.Id;
                            SA.Number = elt.sa.AppointmentNumber;
                            SA.AccId = elt.acc.Id;
                            SA.FullName = elt.acc.Name;
                            lstSAError.push(SA);
                        }
                    }
                    else {
                        SA.Id = elt.sa.Id;
                        SA.Number = elt.sa.AppointmentNumber;
                        SA.AccId = elt.acc.Id;
                        SA.FullName = elt.acc.Name;
                        SA.Email = elt.acc.PersonEmail;
                        lstSAEmail.push(SA);
                    }
                }
                else if (elt.acc.RecordType.DeveloperName === 'BusinessAccount') {
                    if (elt.acc.Contacts == null || elt.acc.Contacts.length === 0 || elt.acc.Contacts[0].Email == null) {
                        if (elt.acc.BillingStreet != null && elt.acc.BillingPostalCode != null && elt.acc.BillingCity != null) {
                            SA.Id = elt.sa.Id;
                            SA.Number = elt.sa.AppointmentNumber;
                            SA.AccId = elt.acc.Id;
                            SA.FullName = elt.acc.Name;
                            SA.Address = elt.acc.BillingStreet + ' ' + elt.acc.BillingPostalCode + ' ' + elt.acc.BillingCity;
                            lstSACourrier.push(SA);
                        }
                        else {
                            SA.Id = elt.sa.Id;
                            SA.Number = elt.sa.AppointmentNumber;
                            SA.AccId = elt.acc.Id;
                            SA.FullName = elt.acc.Name;
                            lstSAError.push(SA);
                        }
                    }
                    else {
                        SA.Id = elt.sa.Id;
                        SA.Number = elt.sa.AppointmentNumber;
                        SA.AccId = elt.acc.Id;
                        SA.FullName = elt.acc.Name;
                        SA.Email = elt.acc.Contacts[0].Email;
                        lstSAEmail.push(SA);
                    }
                }
            }
        });
        
    },

    initSAToDisplay : function(lstSA) {
        var lstSAToDisplay = [];
        console.log(lstSA);
        console.log('taille : ' + lstSA.length % 3);
        if (lstSA.length > 0) {
            for(var i = 0; i < Math.floor(lstSA.length / 3); i++) {
                lstSAToDisplay.push({
                    "first": lstSA[3 * i],
                    "second": lstSA[3 * i + 1],
                    "third": lstSA[3 * i + 2]
                });
            }

            if (lstSA.length % 3 === 1) {
                lstSAToDisplay.push({
                    "first": lstSA[3 * i]
                });
            }
            else if (lstSA.length % 3 === 2) {
                lstSAToDisplay.push({
                    "first": lstSA[3 * i],
                    "second": lstSA[3 * i + 1]
                });
            }
        }

        console.log(lstSAToDisplay);
        return lstSAToDisplay;
    },
    
    close : function(component) {
        var workspaceAPI = component.find("workspace");

        workspaceAPI.isConsoleNavigation().then(function(isConsole) {
            if(isConsole) {
                workspaceAPI.getFocusedTabInfo()
                    .then(function(response) {
                        var focusedTabId = response.tabId;
                        workspaceAPI
                                .openTab({
                                    pageReference: {
                                        type: "standard__objectPage",
                                        attributes: {
                                            objectApiName: "ServiceAppointment",
                                            actionName: "list"
                                        }
                                    }
                                })
                                .then(function(response) {
                                    workspaceAPI.closeTab({ tabId: focusedTabId });
                                    workspaceAPI.focusTab({ tabId: response });
                                    console.log(response);
                                });
                        })
                    .catch(function(error) {
                        console.log(error);
                    });
            }
            else {
                var homeEvent = $A.get("e.force:navigateToObjectHome");
                homeEvent.setParams({
                    "scope": "ServiceAppointment"
                });
                homeEvent.fire();
            }
        });
    },
})