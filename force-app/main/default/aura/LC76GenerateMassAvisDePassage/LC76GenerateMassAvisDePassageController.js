({
    doInit : function(component, event, helper) {
        component.set("v.isLoading", true);
        var lstSAEmail = [];
        var lstSACourrier = [];
        var lstSAError = [];

        var SAEmailToDisplay = [];
        var SACourrierToDisplay = [];
        var SAErrorToDisplay = [];
/*
        var myPageRef = component.get("v.pageReference");
        console.log(myPageRef);
        var value = myPageRef.state.c__listofSA;
        var SAIds = [];
        value.split(',').forEach(element => {
            if (element !== '')
                SAIds.push(element);
        });*/
        console.log(component.get("v.listofSA"));
        var action = component.get('c.getSA');
        action.setParams({
            //"lstSAId": SAIds
            "lstSAId": component.get("v.listofSA")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                
                helper.initData(response.getReturnValue(), lstSAEmail, lstSACourrier, lstSAError);
                
                SAEmailToDisplay = helper.initSAToDisplay(lstSAEmail);
                SACourrierToDisplay = helper.initSAToDisplay(lstSACourrier);
                SAErrorToDisplay = helper.initSAToDisplay(lstSAError);

                component.set("v.lstSAEmail", lstSAEmail);
                component.set("v.lstSACourrier", lstSACourrier);
                component.set("v.lstSAError", lstSAError);

                component.set("v.SAEmailTodisplay", SAEmailToDisplay);
                component.set("v.SACourrierTodisplay", SACourrierToDisplay);
                component.set("v.SAErrorTodisplay", SAErrorToDisplay);
            
            }
            else
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()[0]));
           
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    valider : function(component, event, helper) {
        component.set("v.isLoading", true);
        var action = component.get("c.generateAvisPassage");
        var lstSAIdEmail = [];
        var lstSAIdCourrier = [];
        var lstSAIdError = [];

        component.get("v.lstSAEmail").forEach(elt => {
            lstSAIdEmail.push(elt.Id);
        });

        component.get("v.lstSACourrier").forEach(elt => {
            lstSAIdCourrier.push(elt.Id);
        });
        
        component.get("v.lstSAError").forEach(elt => {
            lstSAIdError.push(elt.Id);
        });

        action.setParams({
            "lstSAIdEmail": lstSAIdEmail,
            "lstSAIdCourrier": lstSAIdCourrier,
            "lstSAIdError": lstSAIdError
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            var toastEvent = $A.get('e.force:showToast');
            if (state === 'SUCCESS') {
                
                toastEvent.setParams({
                    title: 'SUCCESS',
                    type: 'success',
                    message: "La génération des avis de passage a été planifiée"
                });
                helper.close(component);
            }
            else {
                toastEvent.setParams({
                    title: 'Error',
                    type: 'error',
                    message: "Une erreur est survenue lors de la génération des avis de passage"
                });
                console.log('failed with state: ' + state + ' error :' + JSON.stringify(response.getError()[0]));
            }
            
            toastEvent.fire();
            component.set("v.isLoading", false);
        });

        $A.enqueueAction(action);
    },

    cancel : function(component, event, helper) {
        helper.close(component);
    },

    objInfo : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");

        workspaceAPI.isConsoleNavigation().then(function(isConsole) {
            if(isConsole) {
                workspaceAPI.getFocusedTabInfo()
                    .then(function(response) {    
                        var redirectEvt = $A.get("e.force:navigateToSObject");
                        if (redirectEvt) {
                            redirectEvt.setParams({
                                "recordId": event.target.getAttribute("data-id")
                            });

                            redirectEvt.fire();
                        }
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }
            else {
                var link = '';
                var Id = event.target.getAttribute("data-id");
                if (Id.startsWith('001')) {
                    link = '/lightning/r/Account/' + Id + '/view';
                }
                else if (Id.startsWith('08p')) {
                    link = '/lightning/r/ServiceAppointment/' + Id + '/view'
                }
                component.set("v.isFocus", false);
                window.open(link);
            }
        });
        /*
        var redirectEvt = $A.get("e.force:navigateToSObject");
        if (redirectEvt) {
            redirectEvt.setParams({
                "recordId": event.target.getAttribute("data-id")
            });

            redirectEvt.fire();
        }*/
    },

    locationChange : function(component,event, helper){
        console.log('location');
        $A.get('e.force:refreshView').fire();
    },

    doneRendering : function(component,event, helper){
        console.log(Date.now());
        $A.get('e.force:refreshView').fire();
    },

    mouseEnter : function(component,event, helper){
        var workspaceAPI = component.find("workspace");
        var isFocus = component.get("v.isFocus");

        workspaceAPI.isConsoleNavigation().then(function(isConsole) {
            if(!isConsole && !isFocus) {
                console.log('here');
                $A.get('e.force:refreshView').fire();
            }
        });
    },
})