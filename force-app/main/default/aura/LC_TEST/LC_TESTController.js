/**
 * @File Name          : LC_TESTController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 07/01/2020, 13:34:35
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    07/01/2020   RRJ     Initial Version
 **/
({
    handleChangeHT: function(component, event, helper) {
        var line = component.get("v.lineItem");
        console.log(line);
        console.log(typeof line);
        line.qli.Taux_de_TVA__c = 9;
        component.set("v.lineItem", line);
    },

    doInit: function(component, event, helper) {
        // component.set("v.lineItem", { val: 4 });
    }
});