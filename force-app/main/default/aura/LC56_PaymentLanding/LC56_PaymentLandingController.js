/**
 * @File Name          : LC56_PaymentLandingController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 4/28/2020, 5:43:19 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/1/2020   RRJ     Initial Version
 **/
({
    init: function (component, event, helper) {
        component.set('v.isLoading', true);
        var params = {};
        var parser = document.createElement('a');
        parser.href = window.location.search;
        var query = parser.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            params[pair[0]] = decodeURIComponent(pair[1]);
        }
        var isSuccess = false;
        var stat;
        console.log('### params: ', params);
        if (!$A.util.isEmpty(params.vads_trans_status)) {
            if (params.vads_trans_status == 'AUTHORISED') {
                isSuccess = true;
                stat = {
                    isSuccess: true,
                };
                if (window.location !== window.parent.location) {
                    console.log(' in iframe');
                    var lexOrigin = window.location.origin;
                    console.log('### orig: ', lexOrigin);
                    console.log('### loc: ', window.location);
                    var data = {
                        location: window.location,
                        success: isSuccess,
                    };
                    parent.postMessage(JSON.stringify(data), lexOrigin);
                } else {
                    console.log('#### is NOT in iframe');
                    component.set('');
                }
            } else {
                stat = {
                    isSuccess: false,
                    errorMsg: 'test Error',
                };
                component.set('v.status', stat);
                component.set('v.isLoading', false);
            }
        }
    },
});