/**
 * @description       : 
 * @author            : ARM
 * @group             : 
 * @last modified on  : 14-12-2021
 * @last modified by  : ARM
**/
({
    getBundleChild: function (component,bundleProdId,name,lstAssetWrapper) {
        return new Promise(
            $A.getCallback(function (resolve, reject) {
                let lstCmpEventResult = [];
                var action = component.get("c.getBundleChild");
                action.setParams({
                    bundleProdId: bundleProdId,
                });
                action.setCallback(this, function (response) { 
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log("#### responce get cli New: ", response.getReturnValue());
                        lstCmpEventResult.push(response.getReturnValue());
                        lstCmpEventResult.push(name);
                        lstCmpEventResult.push(lstAssetWrapper);
                        
                        resolve(lstCmpEventResult);
                        
                    } else if (state === "INCOMPLETE") {
                        alert("Incomplete");
                    } else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                        reject(response.getError());
                    }
                });
                $A.enqueueAction(action);
            })
        );
    },
    actionOnChange : function (result){
        return new Promise(
            $A.getCallback(function(resolve,reject){
                var name =result[1];
                var lstName=name.split(';');
                var idAst = lstName[0];
                var namePdt = lstName[1];
                var typePdt = lstName[2];
                var lstAssetWrapper=result[2];
                var lstProdBundleChild=[];
                
                for (let assetWrapper of lstAssetWrapper) { 
                    // validation Bundle : 1 seul bundle actif 
                    if(assetWrapper.ast.Id==idAst){
                        assetWrapper.lstCliWrap.forEach(function(cliWrap){
                            if(cliWrap.name==namePdt && cliWrap.isBundle && typePdt=='Bundle'){
                                lstProdBundleChild=result[0];
                                if(cliWrap.isSelected && lstProdBundleChild.length!==0){
                                    assetWrapper.lstCliWrap.forEach(function(cliWrap1){
                                        if(cliWrap1.isOption || cliWrap1.isGuaranty){
                                            var mustDisabled=true;
                                            lstProdBundleChild.forEach(function(prodBundleChild){
                                                if(cliWrap1.prodId==prodBundleChild.Product__c){
                                                    mustDisabled=false;
                                                }
                                                if(mustDisabled){
                                                    cliWrap1.isDisabled=true;
                                                    cliWrap1.isSelected=false;
                                                }else{
                                                    if(cliWrap1.isGuaranty){
                                                        cliWrap1.isSelected=true;
                                                    }
                                                    cliWrap1.isDisabled=false;
                                                } 
                                            });
                                        }
                                    });
                                }else{
                                    assetWrapper.lstCliWrap.forEach(function(cliWrap1){
                                        if(cliWrap1.isOption || cliWrap1.isGuaranty){
                                            cliWrap1.isDisabled=true;
                                            cliWrap1.isSelected=false;
                                        }
                                    });
                                }
                                console.log('cliWrap ',cliWrap);
                            }else if(cliWrap.isBundle && typePdt=='Bundle'){
                                cliWrap.isSelected=false;
                            }
                        });
                    }
                    
                }
                resolve(lstAssetWrapper);
            })
        );
        
    },
    totalLiCol : function (lstAssetWrapper){
        console.log("helper total ligne et colonne");
        console.log("lstAssetWrapper ",lstAssetWrapper);
        var lstTotalLigne=new Array();
        for (let assetWrapper of lstAssetWrapper) { 
            // Total column dynamique ,Total ligne dynamique
            var prixTotalColonne=0;
            let i=0;
            assetWrapper.lstCliWrap.forEach(function(cliWrap){
                console.log('2');
                lstTotalLigne[i]=lstTotalLigne[i]==undefined ? 0 : lstTotalLigne[i];
                if(cliWrap.isSelected){
                    prixTotalColonne+=cliWrap.prixHt;
                    lstTotalLigne[i]+=cliWrap.prixHt;
                    lstTotalLigne[i]=parseFloat(Number.parseFloat(lstTotalLigne[i]).toFixed(2));
                }else{
                    prixTotalColonne+=0;
                    lstTotalLigne[i]+=0;
                }
                i++;
            });
            assetWrapper.totalColone=Number.parseFloat(prixTotalColonne).toFixed(2);
            assetWrapper.lstTotalLigne=lstTotalLigne;
        }
        return lstAssetWrapper;
    },
    // changePage: function (component, originPage, targetPage) {
    //     var lstAssetWrapper = component.get("v.lstAssetWrapper"); 
    //     if (targetPage > originPage) {
    //         var lstDisplay = lstAssetWrapper.slice(originPage * 20, targetPage * 20);
    //     } else {
    //         var lstDisplay = lstAssetWrapper.slice((targetPage - 1) * 20, (originPage - 1) * 20);
    //     }
    //     component.set("v.lstToDisplay", lstDisplay);
    //     component.set("v.pageNum", targetPage);
    // },
    // displayList: function (component, list) {
    //     var numPages = Math.ceil(list.length / 50);
    //     component.set("v.numPages", numPages);
    //     component.set("v.pageNum", 1);
    //     if (list.length <= 50) {
    //         component.set("v.lstToDisplay", list);
    //     } else {
    //         component.set("v.lstToDisplay", list.slice(0, 50));
    //     }
    // },
    
    updateRecordVisibility: function (cmp) {
        var records = cmp.get('v.lstAssetWrapper');
        var pageSize = cmp.get('v.pageSize');
        var currentPage = cmp.get('v.currentPage');
        var offset = (currentPage - 1) * pageSize;
        var visibleRecords = [];
        
        if (offset > records.length)
            offset = 0;
        
        if (currentPage <= 0) {
            cmp.set('v.visibleLstAssetWrapper', visibleRecords);
            return;
        }
        
        for (var i = offset; i < (offset + pageSize) && i < records.length; i++) {
            visibleRecords.push(records[i]);
        }
        
        cmp.set('v.visibleLstAssetWrapper', visibleRecords);
    },
    freezeHeight: function (cmp) {
        setTimeout($A.getCallback(function () {
            var computedStyle = getComputedStyle(cmp.find('recordsTable').getElement());
            var height = parseInt(computedStyle.height, 10);
            
            console.log('height froze at:', height)
            
            cmp.set('v.minHeight', height);
        }))
    },
    
    unfreezeHeight: function (cmp) {
        cmp.set('v.minHeight', 0);
    }
    ,navigateToRecord: function (component, event, objId) {
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: objId,
                actionName: "view"
            }
        };
        event.preventDefault();
        navService.navigate(pageReference);
    },
})