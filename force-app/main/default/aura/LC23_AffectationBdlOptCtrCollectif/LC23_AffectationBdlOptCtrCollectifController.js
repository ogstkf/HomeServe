/**
 * @description       :
 * @author            : RRJ
 * @group             :
 * @last modified on  : 14-12-2021
 * @last modified by  : ARM
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   02-22-2021   RRJ   Initial Version
 **/
({
    doInit: function (component, event, helper) {
        var valeursTVA = $A.get("$Label.c.ValeursTVA");
        var lstValeursTVA = valeursTVA.split(";");
        component.set("v.lstValeursTVA", lstValeursTVA);

        var records = component.get('v.lstAssetWrapper');
        var pageSize = component.get('v.pageSize');

        if (pageSize > 0) {
            helper.updateRecordVisibility(component);

            // Freeze height of component if pagination has already started
            if (records.length > pageSize)
                helper.freezeHeight(component);
            else
                helper.unfreezeHeight(component);
        }
        else {
            component.set('v.visibleLstAssetWrapper', component.get('v.lstAssetWrapper'));
        }
        console.log(records);
    },
    handleChange : function(component, event, helper) {
        component.set("v.isLoading", true);
        var name =event.getSource().get("v.name");
        var lstName=name.split(';');
        var prodId = lstName[3];
        var lstAssetWrapper=component.get("v.lstAssetWrapper");
        helper.getBundleChild(component,prodId,name,lstAssetWrapper).then(
        $A.getCallback(function(result){
            return helper.actionOnChange(result);
        })).then(
            $A.getCallback(function(result2){
            console.log('result2',result2);
            lstAssetWrapper=helper.totalLiCol(result2);

            var visibleLstAssetWrapper=component.get("v.visibleLstAssetWrapper");
            visibleLstAssetWrapper.forEach(function(visibleAssetWrapper){
                lstAssetWrapper.forEach(function(AssetWrapper){
                    if(visibleAssetWrapper.IdLigne===AssetWrapper.IdLigne){
                        visibleAssetWrapper=AssetWrapper;
                    }
                });
            });
              
            component.set("v.lstAssetWrapper",lstAssetWrapper);
            component.set("v.visibleLstAssetWrapper",visibleLstAssetWrapper);
            component.set("v.isLoading", false);
        }));
        
    },
    pageChanged: function (component, event, helper) {
        component.set('v.currentPage', event.getParam('currentPage'));
        helper.updateRecordVisibility(component);
    },
    handleNav: function (component, event, helper) {
        console.log(event);
        var prodId = event.target.dataset.prodid;
        console.log(prodId);
        helper.navigateToRecord(component, event, prodId);
    },
});