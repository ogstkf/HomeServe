({
    doInit : function(component, event, helper){
      helper.checkEligibility(component, component.get('v.recordId'))  
    },

    handleClick : function(component, event, helper) {
        component.set("v.disableButton", !component.get("v.disableButton"));
        var caseId = component.get('v.recordId');
        helper.getWO(component, event, caseId);
    },
    handleModalCreateQuo : function(component, event, helper){
        var woId = component.get("v.woId");
        var caseId = component.get('v.recordId');
        helper.createQuote(component, event, caseId, woId);
    },
    handleCloseModal : function(component, event, helper){
        component.set('v.showModal', false);
        component.set('v.disableButton', false);
    }
})