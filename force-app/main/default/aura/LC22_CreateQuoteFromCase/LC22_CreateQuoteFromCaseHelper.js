({
    getWO : function(component, event, caseId) {
        var action = component.get("c.fetchWo");
        action.setParams({
            caseId: caseId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var lstWo = this.processMapWo(response.getReturnValue());
                component.set("v.lstWo", lstWo);
                if(lstWo == undefined || lstWo.length == 0){
                    this.createQuote(component, event, caseId, null)
                }else if(lstWo.length == 1){
                    this.createQuote(component, event, caseId, lstWo[0].value)
                }else{
                    component.set('v.showModal', true);
                }
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    processMapWo : function(mapWo){
        var lstWo = [];
        for (var key in mapWo) {
            lstWo.push({ label: mapWo[key], value: key });
        }
        return lstWo;
    },

    createQuote : function(component, event, caseId, WoId){
        var action = component.get("c.createQuoteFromCase");
        action.setParams({ 
            caseId: caseId,
            woId: WoId
        });
        console.log(action)
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if (result.success) {
                    var pageReference = {
                        type: "standard__recordPage",
                        attributes: {
                            recordId: result.qteId,
                            actionName: "view"
                        }
                    };
                    component.set("v.disableButton", !component.get("v.disableButton"));
                    this.navigateToRecord(component, event, pageReference);
                } else {
                    component.set("v.disableButton", !component.get("v.disableButton"));
                    this.showToastMessage("ERROR", "error", result.message);
                }
            }
        });
        $A.enqueueAction(action);
    },

    navigateToRecord: function(component, event, pageReference) {
        var navCase = component.find("navService");
        event.preventDefault();
        navCase.navigate(pageReference);
    },

    
    showToastMessage: function(messageTitle, messageType, messageText) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            title: messageTitle,
            type: messageType,
            message: messageText,
            duration: 5000
        });
        resultsToast.fire();
    },

    checkEligibility: function (component, caseId){
        var action = component.get("c.checkEligibilityTocreateQuote");
        action.setParams({ 
            caseId: caseId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if(!result){
                    component.set('v.dissableButton', true);
                }
            }
        });
        $A.enqueueAction(action);
    }
})