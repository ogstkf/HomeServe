/**
 * @File Name          : LC_CustomTableCellController.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 20/05/2020, 15:37:01
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    09/05/2020   ZJO     Initial Version
**/
({
    doInit: function (component, event, helper) {
        helper.fetchCellValue(component);
        helper.bindPicklistVals(component);
    },

    inlineEdit: function (component, event, helper) {

        component.set("v.editMode", true);
        var field = component.get("v.field");

        if (field.type == 'PICKLIST') {
            setTimeout(function () {
                component.find("picklistOpts").focus();
            }, 100);

            var previousVal = component.find("picklistOpts").get("v.value");
            console.log("## selected val:", previousVal);
            component.set("v.previousValue", previousVal);

        }
        else if (field.type == 'DATE') {
            setTimeout(function () {
                component.find("dateInput").focus();
            }, 100);
        }

        console.log("selected items:", component.get("v.selectedItems"));
        var selectedItems = component.get("v.selectedItems");
        component.set("v.chkboxLabel", "Update " + selectedItems.toString() + " selected items");

        //Get row position of edit button
        var editIndex = event.target.dataset.index;
        console.log("edit index:", editIndex);

        //Single or Multiple edit based on Checkbox selection
        console.log("selected rows:", component.get("v.selectedRows"));
        var selectedRowsIndex = component.get("v.selectedRows");

        if (selectedRowsIndex.indexOf(editIndex.toString()) == -1) {
            component.set("v.showMultipleOption", false);

        } else {
            if (selectedItems > 1) {
                component.set("v.showMultipleOption", true);
            } else {
                component.set("v.showMultipleOption", false);
            }
        }
    },

    onPicklistChange: function (component, event, helper) {
        var selectedRowsIndex = component.get("v.selectedRows");
        var selectedIndex = component.get("v.rowIndex");
        component.set("v.changeMode", true);

        if (selectedRowsIndex.indexOf(selectedIndex.toString()) == -1 || selectedRowsIndex.length == 1) {
            console.log("unchecked or one only checked");
            component.set("v.draftMode", true);
            component.set("v.showSaveCancelBtn", true);

            //send params to parent cmp
            var field = component.get("v.field");
            var selectedVal = event.getSource().get("v.value");
            var cmpEvent = component.getEvent('cmpEvent');

            cmpEvent.setParams({
                rowIndex: parseInt(selectedIndex),
                fieldName: field.name,
                updatedValue: selectedVal,
                updateAllMode: false
            });
            cmpEvent.fire();
        }
    },

    onDateChange: function (component, event, helper) {
        component.set("v.showSaveCancelBtn", true);
        component.set("v.draftMode", true);
    },

    closeEditMode: function (component, event, helper) {
        component.set("v.editMode", false);
    },

    updateAll: function (component, event, helper) {
        var isChecked = event.getSource().get("v.checked");
        if (isChecked == true) {
            component.set("v.updateAllMode", true);
        } else {
            component.set("v.updateAllMode", false);
        }
    },

    applyChanges: function (component, event, helper) {
        var updateAllMode = component.get("v.updateAllMode");
        var changeMode = component.get("v.changeMode");

        if (changeMode == true) {
            component.set("v.draftMode", true);
            component.set("v.showSaveCancelBtn", true);

            var selectedVal = component.find("picklistOpts").get("v.value");
            var field = component.get("v.field");
            var selectedIndex = parseInt(component.get("v.rowIndex"));
            var cmpEvent = component.getEvent('cmpEvent');

            cmpEvent.setParams({
                rowIndex: selectedIndex,
                fieldName: field.name,
                updatedValue: selectedVal,
                updateAllMode: updateAllMode
            });
            cmpEvent.fire();
        }
        component.set("v.editMode", false);
    },

    cancelChanges: function (component, event, helper) {
        helper.fetchCellValue(component);
        component.set("v.editMode", false);
    },

    handleUpdate: function (component, event, helper) {
        console.log("test");
        helper.fetchCellValue(component);
    }
})