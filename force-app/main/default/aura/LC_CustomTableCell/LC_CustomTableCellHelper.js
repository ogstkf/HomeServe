/**
 * @File Name          : LC_CustomTableCellHelper.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 19/05/2020, 17:18:38
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    09/05/2020   ZJO     Initial Version
**/
({
    fetchCellValue: function (component) {
        var record = component.get("v.record");
        var field = component.get("v.field");

        if (!$A.util.isEmpty(record[field.name])) {
            if (field.type == 'REFERENCE') {
                var relationShipName = '';
                if (field.name.indexOf('__c') == -1) {
                    relationShipName = field.name.substring(0, field.name.indexOf('Id'));
                }
                else {
                    relationShipName = field.name.substring(0, field.name.indexOf('__c')) + '__r';
                }
                if (!$A.util.isEmpty(record[relationShipName].Name)) {
                    component.set("v.cellValue", record[relationShipName].Name);
                }
            }
            else {
                component.set("v.cellValue", record[field.name]);
            }
        }

    },

    bindPicklistVals: function (component) {
        var field = component.get("v.field");
        var picklistMap = component.get("v.picklist");

        for (var plField in picklistMap) {
            if (plField == field.name) {
                var opts = picklistMap[plField];
                component.set("v.options", opts);
            }
        }
    },

})