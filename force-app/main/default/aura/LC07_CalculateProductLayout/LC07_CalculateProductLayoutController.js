/**
 * @File Name          : LC07_CalculateProductLayoutController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 05/11/2019, 15:42:28
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    04/11/2019   RRJ     Initial Version
 **/
({
    handleLinkClick: function(component, event, helper) {
        window.open("/" + component.get("v.pbe").Id);
        /*var navEvt = $A.get("e.force:navigateToSObject");
        
        navEvt.setParams({
          "recordId": component.get("v.priceBkEntry").Id
        });

        navEvt.fire();*/
    }
});