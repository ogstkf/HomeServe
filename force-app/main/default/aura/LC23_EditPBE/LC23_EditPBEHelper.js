/**
 * @File Name          : LC23_EditPBEHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 08-07-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    10/02/2020   RRJ     Initial Version
 **/
({
    checkValidity: function(line) {
        if ((line.isOption == true && line.cli.Quantity > 12) || line.cli.UnitPrice < 0 || line.cli.Discount < 0) {
            return false;
        } else {
            return true;
        }
    },

    calculatePrixHt: function(line) {
        var prHt = 0;
        if (!$A.util.isEmpty(line.cli.Quantity) && !$A.util.isEmpty(line.cli.UnitPrice)) {
            prHt = line.cli.Quantity * line.cli.UnitPrice;
        }
        if (!$A.util.isEmpty(line.cli.Discount)) {
            prHt = prHt * (1 - line.cli.Discount / 100);
        }
        
        return prHt;
    },

    calculatePrixTtc: function(line) {
        var prTtc = 0;
        if (!$A.util.isEmpty(line.prixHt) && !$A.util.isEmpty(line.tva)) {
            prTtc = line.prixHt * (1 + line.tva / 100);
        } else if (!$A.util.isEmpty(line.prixHt)) {
            prTtc = line.prixHt;
        }
        return prTtc;
    },
    
});