/**
 * @File Name          : LC23_EditPBEController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : LGO
 * @Last Modified On   : 15-07-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    13/01/2020   RRJ     Initial Version
 **/
({
    handleLineChange: function(component, event, helper) {
        // console.log('#####');
        var isContrat1ere = component.get("v.isContrat1ere");
        var lstQLI = component.get('v.lstCLI');
        lstQLI.forEach(line => {
            //Update 05-07-2021 TEC 710
            if (isContrat1ere) {
                if (line.isBundleAdvantage)
                    if (line.cli.Quantity > 0 && line.cli.UnitPrice > 0)
                        line.cli.Discount = (12 / ( (1+ (line.tva / 100) ) * line.cli.Quantity * line.cli.UnitPrice ) -1) * (-1) * 100;
                    else    
                        line.cli.Discount = undefined;

                if (line.isOption && line.cli.Quantity > 0)
                    line.cli.Discount = 100;

            }
            //Update 05-07-2021 TEC 710 End

            var prixHt = helper.calculatePrixHt(line);
            line.prixHt = prixHt;
            var prixTtc = helper.calculatePrixTtc(line);
            line.prixTTC = prixTtc;
        });
        component.set('v.lstCLI', lstQLI);
    },

    handleGlobalChange: function(component, event, helper) {
        var lstQLI = component.get('v.lstCLI');
        var sums = {
            sumVente: 0,
            sumHt: 0,
            sumTtc: 0
        };
        var isValid = true;
        lstQLI.forEach(line => {
            var uPrc = $A.util.isEmpty(line.cli.UnitPrice) ? 0 : line.cli.UnitPrice;
            sums.sumVente += parseFloat(uPrc);
            var htPrc = $A.util.isEmpty(line.prixHt) ? 0 : line.prixHt;
            sums.sumHt += parseFloat(htPrc);
            var ttcPrc = $A.util.isEmpty(line.prixTTC) ? 0 : line.prixTTC;
            sums.sumTtc += parseFloat(ttcPrc);
            var isLineValid = helper.checkValidity(line);
            if (isLineValid == false) {
                isValid = false;
            }
        });

        var cmpEvent = component.getEvent('cmpEvent');
        cmpEvent.setParams({
            action: 'finishBtn',
            data: { valid: isValid }
        });
        cmpEvent.fire();
        component.set('v.sums', sums);
    }, 
    
    handleContrat1ereChange: function(component, event, helper) {
        var isContrat1ere = component.get("v.isContrat1ere");
        var lstQLI = component.get('v.lstCLI');
        lstQLI.forEach(line => {
            //Update 05-07-2021 TEC 710
            if (isContrat1ere) {
                if (line.isBundle)
                    if (line.cli.Quantity > 0 && line.cli.UnitPrice > 0) {
                        line.cli.oldDiscount = line.cli.Discount;
                        line.cli.Discount = (12 / ( (1+ (line.tva / 100) ) * line.cli.Quantity * line.cli.UnitPrice ) -1) * (-1) * 100;
                    }
                    else    
                        line.cli.Discount = undefined;

                if (line.isOption) 
                    if (line.cli.Quantity > 0)
                        line.cli.Discount = 100;
                    else
                        line.cli.Discount = undefined;
            }
            else {
                if (line.isBundle || line.isOption) {
                    line.cli.Discount = line.cli.oldDiscount;
                }
            }
            
            line.prixHt = helper.calculatePrixHt(line);
            line.prixTTC = helper.calculatePrixTtc(line);
        });
        component.set('v.lstCLI', lstQLI);
    }
});