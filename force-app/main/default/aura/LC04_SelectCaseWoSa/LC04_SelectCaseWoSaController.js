/**
 * @File Name          : LC04_SelectCaseWoSaController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 12/11/2019, 17:43:50
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    11/11/2019   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        var objType = component.get("v.objectType");
        if (objType === "Account") {
            helper.fetchDetailsAcc(component);
        } else if (objType === "Case") {
            helper.fetchDetailsCas(component);
        }
    },

    caseChange: function(component, event, helper) {
        helper.handleCasChange(component);
    },

    woChange: function(component, event, helper) {
        helper.handleWoChange(component);
    }
});