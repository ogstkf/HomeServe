/**
 * @File Name          : LC04_SelectCaseWoSaHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 12/11/2019, 19:11:33
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    11/11/2019   RRJ     Initial Version
 **/
({
    fetchDetailsAcc: function(component) {
        var accId = component.get("v.recordId");
        var action = component.get("c.fetchDataAcc");
        action.setParams({
            accountId: accId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var lstSA = response.getReturnValue();
                var lstCas = this.processAccData(lstSA);
                component.set("v.lstCases", lstCas);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    fetchDetailsCas: function(component) {
        var recId = component.get("v.caseId");
        var action = component.get("c.fetchDataCas");
        action.setParams({
            caseId: recId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var lstSA = response.getReturnValue();
                var lstCas = this.processCaseData(lstSA);
                component.set("v.lstCases", lstCas);
                this.handleCasChange(component);
                this.handleWoChange(component);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    processAccData: function(lstSA) {
        var lstOpts = [];
        lstSA.forEach(function(srvApp) {
            var saOpt, woOpt, casOpt, hasCase, hasWo;
            if (srvApp.Work_Order__r.CaseId) {
                casOpt = {
                    label: srvApp.Work_Order__r.Case.CaseNumber,
                    value: srvApp.Work_Order__r.CaseId,
                    selected: false,
                    children: []
                };
                hasCase = lstOpts.some(option => {
                    return option.value === casOpt.value;
                });
                if (srvApp.Work_Order__c) {
                    woOpt = {
                        label: srvApp.Work_Order__r.Subject,
                        value: srvApp.Work_Order__c,
                        selected: false,
                        children: []
                    };
                    saOpt = { label: srvApp.AppointmentNumber, value: srvApp.Id, selected: false };
                }

                if (hasCase) {
                    var casIndex = lstOpts.findIndex(option => {
                        return option.value === casOpt.value;
                    });
                    if (casIndex != -1) {
                        var woOptions = lstOpts[casIndex].children;
                        hasWo = woOptions.some(woOption => {
                            return woOption.value === woOpt.value;
                        });
                        if (hasWo) {
                            var woIndex = woOptions.findIndex(woOption => {
                                return woOption.value === woOpt.value;
                            });
                            if (woIndex != -1) {
                                lstOpts[casIndex].children[woIndex].children.push(saOpt);
                            }
                        } else {
                            woOpt.children.push(saOpt);
                            lstOpts[casIndex].children.push(woOpt);
                        }
                    }
                } else {
                    casOpt.children.push(woOpt);
                    woOpt.children.push(saOpt);
                    lstOpts.push(casOpt);
                }
            }
        });
        return lstOpts;
    },

    processCaseData: function(lstSA) {
        var lstOptions = this.processAccData(lstSA);
        lstOptions[0].selected = true;
        return lstOptions;
    },

    handleCasChange: function(component) {
        var caseId = component.get("v.caseId");
        var lstCas = component.get("v.lstCases");

        var selected = lstCas.find(option => {
            return option.value === caseId;
        });

        var woOptions = [];
        if (selected) {
            woOptions = selected.children;
            if (woOptions.length === 1) {
                woOptions[0].selected = true;
                component.set("v.workOrderId", woOptions[0].value);
            }
            component.set("v.lstWo", woOptions);
            this.handleWoChange(component);
        } else {
            component.set("v.lstWo", woOptions);
            var saOptions = [];
            component.set("v.lstSA", saOptions);
        }
    },

    handleWoChange: function(component) {
        var woId = component.get("v.workOrderId");
        var lstWo = component.get("v.lstWo");

        var selected = lstWo.find(option => {
            return option.value === woId;
        });

        var saOptions = [];
        if (selected) {
            saOptions = selected.children;
            if (saOptions.length === 1) {
                saOptions[0].selected = true;
                component.set("v.servAppId", saOptions[0].value);
            }
            component.set("v.lstSA", saOptions);
        } else {
            component.set("v.lstSA", saOptions);
        }
    }
});