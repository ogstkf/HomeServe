/**
 * @description       :
 * @author            : RRJ
 * @group             :
 * @last modified on  : 02-12-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log
 * Ver   Date         Author   Modification
 * 1.0   02-23-2021   RRJ   Initial Version
 **/
({
    doInit: function (component, event, helper) {
        var lstQLI = component.get("v.lstCLI");
        lstQLI.forEach((line) => {
            if(line.cli.UnitPrice!=null){
                line.cli.UnitPrice=Math.round(line.cli.UnitPrice*100)/100;
            } 
            if(line.cli.UnitPrice==-1){
                // prix de vente vite donc on remplace -1 par ''
                line.cli.UnitPrice='';
            }
            let prixHt = helper.calculatePrixHt(line);
            line.prixHt = prixHt;
        });
        component.set("v.lstCLI", lstQLI);

        var sums = {
            prixRemise: 0,
            quantite: 0
        };
        lstQLI.forEach((line) => {
            var htPrc = $A.util.isEmpty(line.prixHt) ? 0 : line.prixHt;
            sums.prixRemise += parseFloat(htPrc);
            var qty = !$A.util.isEmpty(line.QteContratCollectif) && (line.isBundle || line.isOption) ? line.QteContratCollectif : 0;
            sums.quantite += parseFloat(qty);
        });
        component.set("v.sums", sums);
    },
    handleLineChange: function (component, event, helper) {
        var lstQLI = component.get("v.lstCLI");
        lstQLI.forEach((line) => {
            let prixHt = helper.calculatePrixHt(line);
            line.prixHt = prixHt;
        });
        component.set("v.rechargeLstCLI",true);
        component.set("v.lstCLI", lstQLI);
    },
    handleGlobalChange: function (component, event, helper) {
        var lstQLI = component.get("v.lstCLI");
        var sums = {
            prixRemise: 0,
            quantite: 0
        };
        lstQLI.forEach((line) => {
            var htPrc = $A.util.isEmpty(line.prixHt) ? 0 : line.prixHt;
            sums.prixRemise += parseFloat(htPrc);
            var qty = !$A.util.isEmpty(line.QteContratCollectif) && (line.isBundle || line.isOption) ? line.QteContratCollectif : 0;
            sums.quantite += parseFloat(qty);
        });
        component.set("v.sums", sums);
    }
});