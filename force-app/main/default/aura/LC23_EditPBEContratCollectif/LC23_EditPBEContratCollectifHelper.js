/**
 * @description       : 
 * @author            : RRJ
 * @group             : 
 * @last modified on  : 02-12-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   02-23-2021   RRJ   Initial Version
**/
({
    calculatePrixHt: function (line) {
        line.cli.UnitPrice=line.cli.UnitPrice!=null ? line.cli.UnitPrice :0;
        var prHt = line.cli.UnitPrice;
        if (!$A.util.isEmpty(line.cli.UnitPrice) && !$A.util.isEmpty(line.cli.Discount)) {
            prHt = line.cli.UnitPrice - line.cli.Discount;
        } 
        return prHt;
    }
});