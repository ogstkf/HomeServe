/**
 * @File Name          : LC_CustomTableCmpController.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 20/05/2020, 11:55:17
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    07/05/2020   ZJO     Initial Version
**/
({
    doInit: function (component, event, helper) {
        helper.getTableFieldSet(component, event, helper);
    },

    handleComponentEvent: function (component, event, helper) {
        console.log(event);

        var rowIndex = event.getParam('rowIndex');
        var fieldName = event.getParam('fieldName');
        var updatedVal = event.getParam('updatedValue');
        var updateAllMode = event.getParam('updateAllMode');

        console.log("## row Index:", rowIndex);
        console.log("## fieldName:", fieldName);
        console.log("## updated value:", updatedVal);
        console.log("## updated all:", updateAllMode);

        var recordsList = component.get("v.tableRecords");
        if (updateAllMode == true) {
            var selectedRows = component.get("v.selectedRows");

            let indexArr = selectedRows.map(function (item) {
                return parseInt(item, 10);
            });
            console.log("## integer arr:", indexArr);

            for (var i = 0; i < indexArr.length; i++) {
                var pos = indexArr[i];
                recordsList[pos][fieldName] = updatedVal;
            }

        } else {
            recordsList[rowIndex][fieldName] = updatedVal;
        }
        console.log("after update:", recordsList);
        component.set("v.tableRecords", recordsList);


        // var data = component.get("v.tableRecords");
        // data[rowPos][fieldName] = updatedVal;
        // component.set("v.tableRecords", data);


        // console.log("updated list:", data);
        // var updatedRecords = [];
        // var data = component.get("v.tableRecords");

        // for (var i = 0; i < data.length; i++) {
        //     if (!$A.util.isEmpty(data[i])) {
        //         if (rowPos == i) {
        //             data[i][fieldName] = updatedVal;
        //         }
        //     }
        //     updatedRecords.push(data[i]);
        // }

        // component.set("v.tableRecords", updatedRecords);
    },

    selectAllRows: function (component, event, helper) {
        var headerCheck = event.getSource().get("v.checked");
        var getAllId = component.find("rowCheck");

        if (!Array.isArray(getAllId)) {
            if (headerCheck == true) {
                component.find("rowCheck").set("v.checked", true);
                component.set("v.selectedCount", 1);
            } else {
                component.find("rowCheck").set("v.checked", false);
                component.set("v.selectedCount", 0);
            }
        } else {
            if (headerCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("rowCheck")[i].set("v.checked", true);
                    component.set("v.selectedCount", getAllId.length);
                }
            } else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("rowCheck")[i].set("v.checked", false);
                    component.set("v.selectedCount", 0);
                }
            }
        }
    },

    handleRowSelection: function (component, event, helper) {
        var isSelected = event.getSource().get("v.checked");
        var selectedRowsCount = component.get("v.selectedCount");

        var rowIndex = event.target.dataset.index;
        var selectedArr = component.get("v.selectedRows");

        if (isSelected == true) {
            selectedArr.push(rowIndex);
            selectedRowsCount++;
        }
        else {
            var arrIndex = selectedArr.indexOf(rowIndex);
            if (arrIndex > -1) {
                selectedArr.splice(arrIndex, 1);
            }
            selectedRowsCount--;
        }
        component.set("v.selectedRows", selectedArr);
        component.set("v.selectedCount", selectedRowsCount);
    },

    handleSort: function (component, event, helper) {

        var fieldName = event.currentTarget.dataset.name;
        var fieldType = event.currentTarget.dataset.type;

        console.log("field name:", fieldName);
        console.log("field type:", fieldType);

    },

    save: function (component, event, helper) {
        var lstClis = component.get("v.tableRecords");
        var action = component.get('c.updateCliRecords');

        action.setParams({
            lstUpdatedClis: lstClis
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                console.log("response:", response.getReturnValue());
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Records updated successfully"
                });
                toastEvent.fire();

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: ", errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        }));
        $A.enqueueAction(action);
    },

    cancel: function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    }

})