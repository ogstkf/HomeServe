/**
 * @File Name          : LC_CustomTableCmpHelper.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 17/05/2020, 23:35:07
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/05/2020   ZJO     Initial Version
**/
({
    getTableFieldSet: function (component, event, helper) {

        var action = component.get("c.getFieldSet");
        action.setParams({
            sObjectName: 'ContractLineItem',
            fieldSetName: 'Facturation'
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                console.log("## fieldset:", response.getReturnValue());
                component.set("v.fieldSetValues", response.getReturnValue().headerList);

                var picklistVals = response.getReturnValue().picklistMap;
                console.log("### picklist vals:", picklistVals);
                component.set("v.picklistVals", picklistVals);

                helper.getTableRows(component);

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: ", errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        }));
        $A.enqueueAction(action);
    },

    getTableRows: function (component) {

        var action = component.get("c.getTableRecords");
        var arrfieldNames = [];

        arrfieldNames = this.getUpdatedFieldNames(component);

        //Get contractId from url
        var conId = component.get('v.pageReference').state.c__contractId;
        console.log("## contract Id:", conId);

        action.setParams({
            parentRecordId: conId,
            lstFieldNames: arrfieldNames
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var records = response.getReturnValue();
                console.log("### records:", records);
                component.set("v.tableRecords", records);

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: ", errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        }));
        $A.enqueueAction(action);
    },

    getUpdatedFieldNames: function (component) {

        var fieldSetValues = component.get("v.fieldSetValues");
        console.log("## fieldset values:", fieldSetValues);

        var setUpdatedFields = new Set();
        var arrUpdatedFields = [];

        for (var i = 0; i < fieldSetValues.length; i++) {
            if (!setUpdatedFields.has(fieldSetValues[i].name)) {

                //For lookup fields get name field of parent object
                if (fieldSetValues[i].type == 'REFERENCE') {
                    if (fieldSetValues[i].name.indexOf('__c') == -1) {
                        setUpdatedFields.add(fieldSetValues[i].name.substring(0, fieldSetValues[i].name.indexOf('Id')) + '.Name');
                    } else {
                        setUpdatedFields.add(fieldSetValues[i].name.substring(0, fieldSetValues[i].name.indexOf('__c')) + '__r.Name');

                    }
                } else {
                    setUpdatedFields.add(fieldSetValues[i].name);
                }
            }
        }

        setUpdatedFields.forEach(v => arrUpdatedFields.push(v));
        console.log("### updated field names", arrUpdatedFields);

        return arrUpdatedFields;
    }

})