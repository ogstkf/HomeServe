({
    cloneContract : function(component, event, helper) {
        var contractId = component.get("v.recordId");

        var action = component.get('c.createNewChildContrat');
        action.setParams({ conId: contractId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('## result :', result);

                if(result.msg != 'success'){
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        type: 'error',
                        title: "Créer Nouveau Contrat",
                        message: result.msg
                    });
                    toastEvent.fire();
                }
                else{
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        type: 'success',
                        title: "Créer Nouveau Contrat",
                        message: 'Le contrat a été créé.'
                    });
                    toastEvent.fire();

                    var workspaceAPI = component.find("workspace");
                    workspaceAPI.openTab({
                        url: '/lightning/r/ServiceContract/'+result.conId+'/view',
                        focus: true
                    })
                    .catch(function(error) {
                        console.log('LC59_CloneContractCollectif: ' ,error);
                    });
                }
                

            } else {
                console.log('## LC59_CloneContractCollectif error obj : ', action.getError()[0]);
            }
        });
        $A.enqueueAction(action);
    }
})