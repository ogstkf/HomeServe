({
    doInit : function(component, event, helper) {
        component.set("v.showSpinner",true);
        helper.cloneContract(component, event, helper);
    }
})