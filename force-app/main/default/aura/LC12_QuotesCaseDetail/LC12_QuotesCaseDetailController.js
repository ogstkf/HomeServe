({
    doInit: function(component, event, helper) {},

    openEqp: function(component, event, helper) {
        var eqpId = component.get("v.eqp").Id;
        helper.navigateToRecord(component, event, eqpId);
    },

    // openLog: function(component, event, helper) {
    //     var logId = component.get("v.eqp").Logement__c;
    //     helper.navigateToRecord(component, event, logId);
    // },

    // openContrat: function(component, event, helper) {
    //     var contractId = component.get("v.eqp").ContractLineItems[0].ServiceContractId;
    //     helper.navigateToRecord(component, event, contractId);
    // },

    // openAgence: function(component, event, helper) {
    //     var agenceId = component.get("v.eqp").Logement__r.Agency__c;
    //     helper.navigateToRecord(component, event, agenceId);
    // }
})