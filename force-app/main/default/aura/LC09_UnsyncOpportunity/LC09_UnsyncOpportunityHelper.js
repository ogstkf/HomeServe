({
    clearDevis : function(component) {
        component.set('v.loading',true);
        var action = component.get("c.clearDevisOpp");
        console.log('## result oppId : ',component.get("v.recordId")); 
        console.log('## result delete : ',component.get("v.toDelete")); 

        action.setParams({
            oppId : component.get("v.recordId"),
            toDelete : component.get("v.toDelete")

        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                var result =  response.getReturnValue();
                console.log('## result : ',result); 

                if(!$A.util.isEmpty(result)){ 
                    $A.get("e.force:closeQuickAction").fire();                   
                    // component.set("v.message",result);
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams( { 'title': '',
                                           'type': 'success',
                                           'message': result});
                    showToast.fire();
                }
                else{
                    console.log('## error : ',action.getError()); 
                    // component.set("v.message",action.getError()); 
                    var showToast = $A.get('e.force:showToast');
                    showToast.setParams( { 'title': '',
                                           'type': 'error',
                                           'message': result});
                    showToast.fire();              
                }             
                
            }
            else {
                console.log('## error : ',action.getError()); 
                // component.set("v.message",action.getError()); 
                var showToast = $A.get('e.force:showToast');
                showToast.setParams( { 'title': '',
                                       'type': 'error',
                                       'message': result});
                showToast.fire();              
            }        
                   
        });
        $A.enqueueAction(action);
        component.set('v.loading',false);

    }
})