({
    buildAccLines: function(accLogLine) {
        var rue = accLogLine.Street__c;
        if (rue == null) { rue = ""; }

        var cp = accLogLine.Postal_Code__c;
        if (cp == null) { cp = ""; }
        
        var ville = accLogLine.City__c;
        if (ville == null) { ville = ""; }
        
        accLogLine.Adresse = rue + "\n" + cp + " " + ville;
        accLogLine.Name = accLogLine.Name + "\n" + " "; // Alt 255

        if ((accLogLine.Door__c != null) || (accLogLine.Floor__c != null)) {
                 if (accLogLine.Door__c  == null) { accLogLine.Adresse = accLogLine.Adresse + "\n" + "(Etage : " + accLogLine.Floor__c + ")"; }
            else if (accLogLine.Floor__c == null) { accLogLine.Adresse = accLogLine.Adresse + "\n" + "(Porte : " + accLogLine.Door__c + ")"; }
            else { accLogLine.Adresse = accLogLine.Adresse + "\n" + "(Etage : " + accLogLine.Floor__c + " - Porte : " + accLogLine.Door__c + ")"; }

            accLogLine.Name = accLogLine.Name + "\n" + " "; // Alt 255
        }

        if (accLogLine.Agency__c == null) { accLogLine.Agency__r = 'whatever'; }
        
        // console.log ('accLogLine.Name : ' + accLogLine.Name);
        // console.log ('accLogLine.Inhabitant__c : ' + accLogLine.Inhabitant__c);
        // console.log ('accLogLine.Agency__c : ' + accLogLine.Agency__c);
        // console.log ('accLogLine.Agency__r : ' + accLogLine.Agency__r);
        // console.log ('accLogLine.Agency__r.Compte_pour_VACANT__c : ' + accLogLine.Agency__r.Compte_pour_VACANT__c);
        // console.log ('customLabel : ' + $A.get("$Label.c.Id_Compte_Vacant"));

        if ((accLogLine.Inhabitant__c == null) || 
            (accLogLine.Inhabitant__c == $A.get("$Label.c.Id_Compte_Vacant")) || 
            ((accLogLine.Inhabitant__c != null) && (accLogLine.Inhabitant__c == accLogLine.Agency__r.Compte_pour_VACANT__c))) 
            {
            accLogLine.btnLabel = "Sélectionner";
            accLogLine.btnAction = "selectLog";
        } else {
            accLogLine.btnLabel = "Déménager Occupant";
            accLogLine.btnAction = "demenagerLoc";
            
            if ((accLogLine.Door__c == null) && (accLogLine.Floor__c == null)) {
                accLogLine.Name = accLogLine.Name + "\n" + " "; // Alt 255
            }
        }

        return accLogLine;
    }
})