({
    init: function(component) {
        var columns = [
            {
                type: "text",
                fieldName: "Name",
                label: "Réf. Logement",
                initialWidth: 130
            },
            {
                type: "text",
                fieldName: "Adresse",
                label: "Adresse"
            },
            {
                type: "button",
                typeAttributes: {
                    name: { fieldName: "btnAction" },
                    iconName: { fieldName: "btnIcon" },
                    label: { fieldName: "btnLabel" },
                    title: { fieldName: "btnLabel" },
                    variant: { fieldName: "btnVariant" },
                    disabled: { fieldName: "btnDisabled" }
                },
                initialWidth: 130
            }
        ];
        component.set("v.gridColumns", columns);

        var data = [];
        component.set("v.gridData", data);
    },

    dataChanged: function(component, event, helper) {
        var lines = component.get("v.tableData");
        var formattedLines = lines.map(helper.buildAccLines);
        component.set("v.gridData", formattedLines);
    },

    handleRowAction: function(component, event, helper) {
        var row = event.getParam("row");
        if (row.btnAction == "demenagerLoc") {
            component.set("v.idActualInhabitantChild", row.Inhabitant__c);
            component.set("v.currentPageChild", 2);
        } 
        else if (row.btnAction == "selectLog") {

            component.set("v.idActualInhabitantChild", row.Inhabitant__c);
            component.set("v.idActualLogementChild", row.Id);

            component.set("v.currentPageChild", 4);
        }
    },

});