/**
 * @File Name          : LC02_EligibilityWizardHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 11/07/2019, 15:54:51
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    09/07/2019, 10:06:32   RRJ     Initial Version
 **/
({
    getPickVal: function(component) {
        var action = component.get("c.getPicklists");

        var picklistFlds = JSON.stringify([
            { label: "Logement__c", value: "Professional_Use__c" },
            { label: "Logement__c", value: "Age__c" },
            { label: "Asset", value: "Equipment_type__c" }
        ]);

        action.setParams({
            pickFlds: picklistFlds
        });
        component.set("v.showSpinner", true);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log("##### pickVal: ", JSON.stringify(response.getReturnValue()));
                var pickVals = response.getReturnValue();
                var valuePairs = {
                    UsagePro: pickVals["Logement__c-Professional_Use__c"],
                    AgeLogement: pickVals["Logement__c-Age__c"],
                    TypeEquipment: pickVals["Asset-Equipment_type__c"]
                };
                // console.log("##### valuePairs: ", valuePairs);

                component.set("v.lstPicklistValues", valuePairs);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    showToast: function(title, type, message, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: mode,
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    },

    redirectDemande: function(component, logementId) {
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: logementId,
                actionName: "view"
            }
        };
        navService.navigate(pageReference, true);
    },

    populateEqpType: function(component) {
        // console.log("##### details presta", JSON.stringify(component.get("v.clientDet")));
        var clientDetails = component.get("v.clientDet");
        // console.log("##### populate: eqptype");
        if (clientDetails.clEqpType) {
            // console.log("##### populate: eqptype 2");
            var eqpTypeMapping = [
                { key: "Chaudière gaz", value: "Chaudière" },
                { key: "Radiateur gaz", value: "Chaudière" },
                { key: "Aerotherme, Radiant", value: "Chaudière" },
                { key: "Chaudière fioul", value: "Chaudière" },
                { key: "Poêle à granulés", value: "Chaudière" },
                { key: "Pompe à chaleur hybride Fioul", value: "Appareil hybride" },
                { key: "Pompe à chaleur hybride Gaz", value: "Appareil hybride" },
                { key: "Pompe à chaleur air/eau", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur eau/eau", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 1 unités", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 2 unités", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 3 unités", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 4 unités", value: "Pompe à chaleur" },
                { key: "Pompe à chaleur air/air et clim 5 + X unités", value: "Pompe à chaleur" },
                { key: "Chauffe-eau thermodynamique non rechargeable", value: "Pompe à chaleur" },
                { key: "Chauffe-eau thermodynamique rechargeable sans unité extérieure", value: "Pompe à chaleur" },
                { key: "Chauffe-eau thermodynamique rechargeable avec unité extérieure", value: "Pompe à chaleur" },
                { key: "Chauffe-eau solaire", value: "Chauffe eau" },
                { key: "Chauffe-eau gaz", value: "Chauffe eau" },
                { key: "Chauffe-eau électrique", value: "Chauffe eau" },
                { key: "Caisson VMC", value: "Produit VMC" },
                { key: "Bouche VMC", value: "Produit VMC" },
                { key: "Cheminée", value: "Conduit d'évacuation" },
                { key: "Conduit 3CEP", value: "Conduit d'évacuation" },
                { key: "Cuisine", value: "Autres" },
                { key: "Détecteur de fumée", value: "Autres" }                
            ];

            var mappedValue = eqpTypeMapping.find(
                element => element.key == clientDetails.clEqpType
            );
            // console.log("##### populate: eqptype 3", mappedValue);
            if (!$A.util.isUndefinedOrNull(mappedValue)) {
                // console.log("##### populate: eqptype 4");
                clientDetails.clAstEqpType = mappedValue.value;
            }

            component.set("v.clientDet", clientDetails);
            // console.log("##### details presta", JSON.stringify(component.get("v.clientDet")));
        }
    },
});