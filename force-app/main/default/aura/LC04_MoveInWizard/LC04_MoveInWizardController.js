({
    doInit: function(component, event, helper) {
        var idActualInhabitant = component.get("v.recordId");
        component.set("v.idActualInhabitant", idActualInhabitant);
        component.set("v.clientDet.idActualInhabitant", idActualInhabitant);

        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set("v.clientDet.dateEmmenagement", today);

        helper.getPickVal(component);
    },

    goToPage1: function(component, event, helper) {
        component.set("v.currentPage", 1);
    },

    goToPage3: function(component, event, helper) {
        component.set("v.currentPage", 3);
    },

    NavigateToActualInhabitant: function(component, event, helper) {
        var idActualInhabitant = component.get("v.idActualInhabitant");

        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: idActualInhabitant,
                actionName: "view"
            }
        };
        event.preventDefault();
        navService.navigate(pageReference);
    },

    validateCreation: function(component, event, helper) {
        helper.populateEqpType(component);
        var action = component.get("c.creerLogement");
        var details = component.get("v.clientDet");

        var cleanedDetails = {};
        Object.keys(details).forEach(function(prop) {
            if (details[prop]) {
                cleanedDetails[prop] = details[prop];
            }
        });
        component.set("v.showSpinner", true);
        action.setParams({
            jsonInput: JSON.stringify(cleanedDetails)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast("Réussite", "success", "Le logement a été créé.", "dismissible");

                var accountId = response.getReturnValue();
                helper.redirectDemande(component, accountId);
            } else if (state === "INCOMPLETE") {
                // alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast("ERROR", "error", errors[0].message, "dismissible");
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    changeHandler: function(component, event, helper) {
        var details = component.get("v.clientAddress");
        var action = component.get("c.search");
        action.setParams({ clientAddress : details });
        // action.setParams({
        //      jsonInput: JSON.stringify(details)
        // });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log("#### lines: ", response.getReturnValue());
                component.set("v.lstLogement", response.getReturnValue());
            } else if (state === "INCOMPLETE") {
                // alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    copyAddress: function(component, event, helper) {
        var clDetail = component.get("v.clientDet");
        var copyAdd = component.get("v.memeAddLog");
        if (copyAdd) {
            clDetail.clCP = clDetail.clCPLog;
            clDetail.clRue = clDetail.clRueLog;
            clDetail.clVille = clDetail.clVilleLog;
            clDetail.clPays = clDetail.clPaysLog;
            component.set("v.clientDet", clDetail);
        }
    },

    validateMoveIn: function(component, event, helper) {
        var idActualLogement = component.get("v.idActualLogement");
        component.set("v.clientDet.idActualLogement", idActualLogement);

        var action = component.get("c.moveInToExistingLogement");
        var details = component.get("v.clientDet");
        
        var cleanedDetails = {};
        Object.keys(details).forEach(function(prop) {
            if (details[prop]) {
                cleanedDetails[prop] = details[prop];
            }
        });
        component.set("v.showSpinner", true);
        action.setParams({
            jsonInput: JSON.stringify(cleanedDetails)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.showToast("Réussite", "success", "L’emménagement a été effectué.", "dismissible");
                var accountId = response.getReturnValue();
                helper.redirectDemande(component, accountId);
            } else if (state === "INCOMPLETE") {
                // alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast("ERROR", "error", errors[0].message, "dismissible");
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },



})