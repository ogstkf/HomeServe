({
    init : function(cmp, event, helper) {
        var lblMessage = $A.get("$Label.c.LC16_Message");
        var lblAgency = $A.get("$Label.c.LC16_Agency");
        var lblAllResources = $A.get("$Label.c.LC16_All_Resources");
        var lblStatus = $A.get("$Label.c.LC16_Status");
        var lblSendNotification = $A.get("$Label.c.LC16_Send_Notification");
        var lblEditNotification = $A.get("$Label.c.LC16_Edit_Notification");
        var lblDeleteNotification = $A.get("$Label.c.LC16_Delete_Notification");
        cmp.set('v.columns', [
            { label: lblMessage, fieldName: 'message__c', type: 'text'},
            { label: lblAgency, fieldName: 'Agency__c', type: 'text',"fixedWidth":200},
            { label: lblAllResources, fieldName: 'all_ressources__c', type: 'boolean',"fixedWidth":150},
            { label:lblStatus, fieldName: 'Status__Trans', type: 'text',"fixedWidth":150},
            { type:"button-icon","fixedWidth":40, "typeAttributes":{ iconName: 'standard:outcome', name:'send_message', 'title':lblSendNotification,variant:'border-filled' }},
            { type:"button-icon","fixedWidth":40, "typeAttributes":{ iconName: 'utility:edit', name:'edit_record', 'title':lblEditNotification,variant:'border-filled' }},
            { type:"button-icon","fixedWidth":40, "typeAttributes":{ iconName: 'utility:delete', name:'delete_record', 'title':lblDeleteNotification,variant:'border-filled' }}
        ]);
        helper.refreshList(cmp);
        
    },
    
    createNotification:function (cmp,event,helper){
        cmp.set('v.mode', 'create');     
    },
    closeModal : function(cmp,event,helper){
        cmp.set('v.mode', 'list');     
    },
    
    handleSubmit: function(cmp,event,helper){
        // event.preventDefault();       // stop the form from submitting
        var fields = event.getParam('fields');
        fields.Status__c = 'Waiting';
        
        
        // fields.LastName = 'My Custom Last Name'; // modify a field
        //cmp.find('myRecordForm').submit(fields);
    }, 
    
    handleSuccess: function(cmp,event,helper){
        helper.refreshList(cmp);
        cmp.set('v.mode', 'list');     
    },
    
    cancelSend: function(cmp,event,helper){
        cmp.set('v.mode', 'list');     
    },
    
    doSend: function(cmp,event,helper){
        // alert('sending');
        var message = cmp.get('v.message');
        if(message){
            var id = message.Id;
            
            helper.sendNotification(cmp,id);
        }
    },
    doDelete: function(cmp,event,helper){
        // alert('sending');
        var message = cmp.get('v.message');
        if(message){
            var id = message.Id;
            
            helper.deleteNotification(cmp,id);
        }
    },
    handleRowAction: function(cmp,event,helper){
        var action = event.getParam('action');
        var row = event.getParam('row');
        cmp.set('v.message', row);
        switch (action.name) {
            case 'send_message':
                
                cmp.set('v.mode', 'send');
                //helper.showRowDetails(row);
                break;
            case 'edit_status':
                helper.editRowStatus(cmp, row, action);
                break;
            case 'delete_record':
               // helper.deleteRecord(cmp,row);
               cmp.set('v.mode', 'delete');
                break;
            default:
                //helper.showRowDetails(row);
                cmp.set('v.mode', 'edit');
                break;
        }
    },
    onSelectChange : function(component, event, helper) {
        
        var selected = component.find("records").get("v.value");
        
        var paginationList = [];
        
        var oppList = component.get("v.messages");
        
        for(var i=0; i< selected; i++){
            paginationList.push(oppList[i]);
        }
        
        component.set("v.paginationList", paginationList);
        
    },
    first : function(component, event, helper){
        
        var oppList = component.get("v.messages");
        
        var pageSize = component.get("v.pageSize");
        
        var paginationList = [];
        
        for(var i=0; i< pageSize; i++){
            if(oppList[i])
            	paginationList.push(oppList[i]);
        }
        component.set("v.paginationList", paginationList);
        
    },
    
    last : function(component, event, helper){
        
        var oppList = component.get("v.messages");
        
        var pageSize = component.get("v.pageSize");
        
        var totalSize = component.get("v.totalSize");
        
        var paginationList = [];
        
        for(var i=totalSize-pageSize+1; i< totalSize; i++){
            if(oppList[i])
            	paginationList.push(oppList[i]);
            
        }
        
        component.set("v.paginationList", paginationList);
        
    },
    
    next : function(component, event, helper){
        
        var oppList = component.get("v.messages");
        
        var end = component.get("v.end");
        
        var start = component.get("v.start");
        
        var pageSize = component.get("v.pageSize");
        
        var paginationList = [];
        
        var counter = 0;
        
        for(var i=end+1; i<end+pageSize+1; i++){
            
            if(oppList.length > end){
                if(oppList[i])
                	paginationList.push(oppList[i]);
                
                counter ++ ;
                
            }
            
        }
        
        start = start + counter;
        
        end = end + counter;
        
        component.set("v.start",start);
        
        component.set("v.end",end);
        
        component.set("v.paginationList", paginationList);
        
    },
    
    previous : function(component, event, helper){
        
        var oppList = component.get("v.messages");
        
        var end = component.get("v.end");
        
        var start = component.get("v.start");
        
        var pageSize = component.get("v.pageSize");
        
        var paginationList = [];
        
        var counter = 0;
        
        for(var i= start-pageSize; i < start ; i++){
            
            if(i > -1){
                if(oppList[i])
                	paginationList.push(oppList[i]);
                
                counter ++;
                
            }
            
            else {
                
                start++;
                
            }
            
        }
        
        start = start - counter;
        
        end = end - counter;
        
        component.set("v.start",start);
        
        component.set("v.end",end);
        
        component.set("v.paginationList", paginationList);
        
    }
})