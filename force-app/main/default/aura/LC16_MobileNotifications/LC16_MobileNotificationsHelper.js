({
    refreshList : function(cmp) {
        var pageSize = cmp.get("v.pageSize");
        var action = cmp.get("c.getMessages");
        //  action.setParams({ firstName : cmp.get("v.firstName") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var lblWaiting = $A.get("$Label.c.LC16_Waiting");
                var lblSent = $A.get("$Label.c.LC16_Sent");
                var list = response.getReturnValue();
                for(var i in list){
                    if(list[i].Status__c === 'Waiting'){
                        list[i].Status__Trans = lblWaiting;
                    }else{
                        list[i].Status__Trans = lblSent;
                    }
                }
                
                cmp.set('v.messages', list);
                cmp.set("v.totalSize",list.length);
                cmp.set("v.start",0);
                cmp.set("v.end",pageSize-1);
                var paginationList = [];

                for(var i=0; i< pageSize; i++){
                    paginationList.push(list[i]);
                }
                cmp.set("v.paginationList", paginationList);
                
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    
    sendNotification: function(cmp, messageId){
        var action = cmp.get("c.sendMessage");
        action.setParams({ messageId : messageId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                this.refreshList(cmp);
                cmp.set('v.mode', 'list');
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    deleteNotification: function(cmp, messageId){
        var action = cmp.get("c.deleteMessage");
        action.setParams({ messageId : messageId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                this.refreshList(cmp);
                cmp.set('v.mode', 'list');
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    }
})