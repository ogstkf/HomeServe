({
    generateJSON : function(component) {
        var servConId = component.get('v.recordId');
        var action = component.get('c.createJSON');
        action.setParams({ 
            "Id": servConId,
            "isPreview": component.get('v.isPreview')
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            var body = {};
            if (JSON.parse(response.getReturnValue()))
                body = JSON.parse(response.getReturnValue());
            else
                body.message = '';
            var titleToast = 'ERROR';
            var typeToast = 'error';
            if (body.statusCode >= 200 && body.statusCode < 300) {
                titleToast = 'SUCCESS';
                typeToast = 'success';
            }

            var toastEvent = $A.get('e.force:showToast');
            if (state === 'SUCCESS') {
                toastEvent.setParams({
                    title: titleToast,
                    type: typeToast,
                    message: body.message
                });
                toastEvent.fire();
            }
            else {
                console.log(response.getError());
            }
            $A.get('e.force:closeQuickAction').fire();
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    }
})