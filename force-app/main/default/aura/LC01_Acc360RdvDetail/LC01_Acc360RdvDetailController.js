/**
 * @File Name          : LC01_Acc360RdvDetailController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 29/07/2019, 16:07:42
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    24/06/2019, 11:23:43   RRJ     Initial Version
 **/
({
    navToRdv: function(component, event, helper) {
        var rdvId = component.get("v.rdv").srvApp.Id;
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: rdvId,
                actionName: "view"
            }
        };

        helper.navigateToRecord(component, event, pageReference);
    },

    openAsset: function(component, event, helper) {
        var astId = component.get("v.rdv").wrkOrder.Case.AssetId;
        console.log("### astId: ", astId);

        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: astId,
                actionName: "view"
            }
        };

        helper.navigateToRecord(component, event, pageReference);
    },

    replanifier: function(component, event, helper) {
        var workOrderId = component.get("v.rdv").srvApp.ParentRecordId;
        var rdvId = component.get("v.rdv").srvApp.Id;
        console.log("###### rdvId: ", rdvId);
        console.log("###### workOrderId: ", workOrderId);

        var pageReference = {};
        if (workOrderId.slice(0, 3) == "0WO") {
            pageReference = {
                type: "standard__recordPage",
                attributes: {
                    recordId: workOrderId,
                    actionName: "view"
                }
            };
        } else {
            pageReference = {
                type: "standard__recordPage",
                attributes: {
                    recordId: rdvId,
                    actionName: "edit"
                }
            };
        }

        helper.navigateToRecord(component, event, pageReference);
    },

    annuler: function(component, event, helper) {
        helper.fireEvt(component, component.get("v.rdv").srvApp.Id);
    }
});