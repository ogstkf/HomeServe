/**
 * @File Name          : LC01_Acc360RdvDetailHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 29/07/2019, 14:01:53
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    24/06/2019, 12:02:17   RRJ     Initial Version
 **/
({
    navigateToRecord: function(component, event, pageReference) {
        var navService = component.find("navService");
        event.preventDefault();
        navService.navigate(pageReference);
        console.log("navv");
    },

    fireEvt: function(component, message) {
        var evt = component.getEvent("annulerRdv");
        evt.setParam("evtData", message);
        evt.fire();
    }
});