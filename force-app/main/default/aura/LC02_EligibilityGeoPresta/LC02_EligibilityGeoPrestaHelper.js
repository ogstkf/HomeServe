/**
 * @File Name          : LC02_EligibilityGeoPrestaHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 18/09/2019, 15:17:37
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/07/2019, 16:11:02   RRJ     Initial Version
 **/
({
    changePage: function(component, pageNum) {
        var evt = component.getEvent("pageEvent");
        evt.setParam("pageNo", pageNum);
        var clientDetails = component.get("v.clientDet");
        evt.setParam("pageData", clientDetails);
        evt.fire();
    },

    getPickListVals: function(component) {
        var action = component.get("c.getPicklists");

        var picklistFlds = JSON.stringify([{ label: "WorkType", value: "Equipment_type__c" }]);

        action.setParams({
            pickFlds: picklistFlds
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var pickVals = response.getReturnValue();
                component.set("v.eqpTypVals", pickVals["WorkType-Equipment_type__c"]);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    processEliPresta: function(component, arrAccr) {
        // console.log("##### arrAccr: ", JSON.stringify(arrAccr));
        var clientDetails = component.get("v.clientDet");
        var prestaEli = false;
        var prestaLoc = false;
        var prestaDel = false;
        if (
            arrAccr.length == 0 ||
            arrAccr.some(accrLine => accrLine.Eligibility__c == "Oui propre")
        ) {
            prestaEli = true;
            prestaLoc = true;
        }
        if (arrAccr.some(accrLine => accrLine.Eligibility__c == "Oui déléguée")) {
            prestaEli = true;
            prestaDel = true;
        }
        if (prestaEli && prestaLoc && arrAccr.length > 0) {
            clientDetails.clReqAgenceLocalId = arrAccr[0].LocalAgency__c;
            clientDetails.clLogAgenceLocalId = arrAccr[0].LocalAgency__c;
        }
        if (prestaEli && prestaDel) {
            clientDetails.clReqAgenceDelId = arrAccr[0].DelegatedAgency__c;
        }
        component.set("v.isPrestaEligible", prestaEli);
        component.set("v.isPrestaLocal", prestaLoc);
        component.set("v.isPrestaDel", prestaDel);
        component.set("v.clientDet", clientDetails);

        var formatedLines = [];
        arrAccr.forEach(function(accrLine) {
            if (accrLine.Eligibility__c != "Non" || accrLine.Valid_until__c <= new Date()) {
                var formattedLine = {
                    agLocalName: accrLine.LocalAgency__r ? accrLine.LocalAgency__r.Name : "",
                    agDelname: accrLine.DelegatedAgency__r ? accrLine.DelegatedAgency__r.Name : "",
                    eliPropre: accrLine.Eligibility__c == "Oui propre" ? true : false,
                    eliDel: accrLine.Eligibility__c == "Oui déléguée" ? true : false
                };
                formatedLines.push(formattedLine);
            }
        });
        if (arrAccr.length == 1 && arrAccr[0].Eligibility__c == "Non") {
            component.set("v.isPrestaEligible", false);
        }
        component.set("v.lstAgAccr", formatedLines);
    },

    fetchPicklistValues: function(component) {
        var action = component.get("c.getDepPickVals");
        action.setParams({
            sObjName: "WorkType",
            controllingFld: "Type__c",
            dependentFld: "Reason__c"
        });

        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = JSON.parse(response.getReturnValue());
                // console.log("##### StoreResponse: ", StoreResponse);

                component.set("v.depnedentFieldMap", StoreResponse);

                var listOfkeys = [];
                var ControllerField = [];
                var DependentField = [];

                for (var i = 0; i < StoreResponse.length; i++) {
                    listOfkeys.push({
                        value: StoreResponse[i].value,
                        label: StoreResponse[i].label
                    });
                }

                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push({ label: "-- Aucun --", value: "" });
                }

                for (var i = 0; i < listOfkeys.length; i++) {
                    ControllerField.push(listOfkeys[i]);
                }
                // console.log("### ControllerField: ", ControllerField);
                component.set("v.typeVals", ControllerField);
            } else if (response.getState() === "INCOMPLETE") {
                alert("Incomplete");
            } else if (response.getState() === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: ", errors);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    onLoad: function(component) {
        var details = component.get("v.clientDet");
        // console.log("#### details eligeo: ", JSON.stringify(details.doRefresh));
        var geoAnalysed = component.get("v.isGeoAnalysed");
        if (details.doRefresh == true) {
            details.doRefresh = false;
            component.set("v.clientDet", details);
            if (details.clLogSelected) {
                var geoData = {
                    isEligible: true,
                    agencyName: details.clLogAgency,
                    agencyId: details.clLogAgencyId
                };
                component.set("v.geoData", geoData);
                component.set("v.isGeoEligible", true);
            } else {
                component.set("v.geoData", {});
                component.set("v.isGeoEligible", false);
                component.set("v.isGeoAnalysed", false);
            }
        }
    },

    setDepField: function(component, controllerField) {
        var depnedentFieldMap = component.get("v.depnedentFieldMap");
        var values = [];
        if (controllerField != "") {
            var ListOfDependentFields = depnedentFieldMap.find(
                element => element.value == controllerField
            ).children;

            if (ListOfDependentFields.length > 0) {
                values = ListOfDependentFields;
            }
        }
        component.set("v.reasonVals", values);
    }
});