/**
 * @File Name          : LC02_EligibilityGeoPrestaController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 13/01/2020, 18:27:34
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    11/07/2019, 16:10:41   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        helper.getPickListVals(component);
        helper.fetchPicklistValues(component);
        // helper.onLoad(component);
        component.set("v.accrCols", [
            { label: "Nom de ‘agence locale", fieldName: "agLocalName", type: "text" },
            { label: "Nom de l’agence spécialisée", fieldName: "agDelname", type: "text" },
            { label: "Eligibilité propre", fieldName: "eliPropre", type: "boolean" },
            { label: "Eligibilité déléguée", fieldName: "eliDel", type: "boolean" }
        ]);
    },

    getEliGeo: function(component, event, helper) {
        var action = component.get("c.analyseGeo");
        var details = component.get("v.clientDet");
        action.setParams({
            jsonInput: JSON.stringify(details)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log(JSON.stringify("geo Eligible: " + response.getReturnValue()));
                component.set("v.geoData", response.getReturnValue());
                if (response.getReturnValue().isEligible == "true") {
                    component.set("v.isGeoEligible", true);
                } else {
                    component.set("v.isGeoEligible", false);
                }
                component.set("v.isGeoAnalysed", true);
                details.clReqAgenceLocalId = response.getReturnValue().agencyId;
                details.clReqSubAgenceId = response.getReturnValue().subAgenceId;
                details.clLogAgenceLocalId = response.getReturnValue().agencyId;
                details.clLogSubAgenceId = response.getReturnValue().subAgenceId;
                component.set("v.clientDet", details);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getEliPresta: function(component, event, helper) {
        var action = component.get("c.analysePresta");
        var details = component.get("v.clientDet");
        action.setParams({
            jsonInput: JSON.stringify(details)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log("presta Eli: " + JSON.stringify(response.getReturnValue()));
                helper.processEliPresta(component, response.getReturnValue());
                component.set("v.isPrestaAnalysed", true);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    back: function(component, event, helper) {
        helper.changePage(component, 1);
    },

    forward: function(component, event, helper) {
        helper.changePage(component, 3);
    },

    creerPiste: function(component) {
        var createRecordEvent = $A.get("e.force:createRecord");
        var clDet = component.get("v.clientDet");
        createRecordEvent.setParams({
            entityApiName: "Lead",
            defaultFieldValues: {
                Email: clDet.clEmail,
                Phone: clDet.clPhone
            }
        });
        createRecordEvent.fire();
    },

    onControllerFieldChange: function(component, event, helper) {
        var controllerValueKey = component.get("v.clientDet").clPresta; // get selected controller field value
        helper.setDepField(component, controllerValueKey);
    },

    detChanged: function(component, event, helper) {
        // console.log("### page changed");
        helper.onLoad(component);
    }
});