({
    handleLinkClick : function(component, event, helper) {
        window.open('/' + component.get("v.pbe").Id); 
    },

    handleOnChange : function(component, event, helper) {
        var selPBE = component.get("v.selPBE");
        var lstPricebkEntryItems = component.get("v.lstPBE");

        for(var i=0; i<lstPricebkEntryItems.length; i++) {

            var toPush;
            var toRemove; 

            if(lstPricebkEntryItems[i].selected) {
                toPush = true;
                toRemove = false; 
                
                for(var j = 0 ; j < selPBE.length; j++){
                    if(selPBE[j].Id == lstPricebkEntryItems[i].pbe.Id){
                        toPush = false; 
                    }
                    
                }

                if(toPush == true )selPBE.push(lstPricebkEntryItems[i].pbe);
                
            }else{
                toRemove = false; 
                var removeIndex; 
                for(var j = 0 ; j < selPBE.length; j++){

                    if(selPBE[j].Id == lstPricebkEntryItems[i].pbe.Id){
                        toRemove = true; 
                        removeIndex = j;
                    }
                    
                }

                if(toRemove ==true){
                    selPBE.splice(removeIndex, 1)
                }

            }
        }

        if(selPBE != null) {
            component.set("v.selPBE", selPBE);
        }
    },
})