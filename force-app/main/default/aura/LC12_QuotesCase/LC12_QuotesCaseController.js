({
    doInit: function(component, event, helper) {
    	console.log('*** id record: ',component.get("v.recordId"));
        helper.populateData(component, component.get("v.recordId"));
    }
})