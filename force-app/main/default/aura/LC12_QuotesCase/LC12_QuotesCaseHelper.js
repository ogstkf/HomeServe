({
    populateData: function(component, caId) {
        var action = component.get("c.fetchQuote");
        action.setParams({
            caseId: caId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.lstQuotes", response.getReturnValue());
                component.set("v.loading", false);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        component.set("v.loading", false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})