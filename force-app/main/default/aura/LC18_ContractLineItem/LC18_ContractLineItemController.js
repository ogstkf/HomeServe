({
    HandleContract : function(component, event, helper) {
        console.log('*** in handle contract');     
        
        component.find("cmbxProdType").set("v.value", "Tout")      
        helper.loadCLI(component, helper, 'Tout', component.get("v.IndexCalendar")+1, component.get("v.CalendarYear") );      
    },

    loadOptions: function (component, event, helper) {
        console.log('*** in loadOptions');
        var options = [
            { value: "Tout", label: "Tout" },
            { value: "Individuel", label: "Individuel" },
            { value: "Collectif", label: "Collectif" }
        ];
        component.set("v.lstPicklistVals", options);

        var TodayDate = new Date();
        helper.loadCLI(component, helper, 'Tout', TodayDate.getMonth()+1, TodayDate.getFullYear() );     
    },
    handleChange : function(component, event, helper) {
        console.log('*** in handleChange');

        var cmbxVal = component.find("cmbxProdType").get("v.value");
        var isDisabled;
        var btnSa = document.getElementsByClassName("btnSave");
        var btnNext = document.getElementsByClassName("btnNx");

        console.log('*** in cmbxVal: ',cmbxVal);
        console.log('*** in btnSa: ', btnSa);
        console.log('*** in btnNext: ', btnNext);

        if(cmbxVal == 'Tout'){
            isDisabled=true;
            btnSa[0].style.display = 'block';
            btnNext[0].style.display = 'none';
        }else if(cmbxVal=='Individuel'){
            isDisabled=false;
            btnSa[0].style.display = 'block';
            btnNext[0].style.display = 'none';
        }else if(cmbxVal=='Collectif'){            
            btnSa[0].style.display = 'none';           
            btnNext[0].style.display = 'block';
        }
        component.set("v.isDisabled",isDisabled); //TO CHECK IF NEEDED

        //component.set("v.isLoading", true);

        helper.loadCLI(component, helper, cmbxVal, component.get("v.IndexCalendar")+1, component.get("v.CalendarYear") );      
        
    },
    handleChangeCheckbx : function(component, event, helper) {     
        var checked = event.getSource().get('v.checked');
        var recordId = event.getSource().get("v.name");

        if(checked == false){
            helper.uncheckRecord(component, event, helper, recordId);
        }else{
            helper.checkRecord(component, event, helper, recordId);
        }        
    },  

    handleMainChangeCheckbx: function(component, event, helper) {
        var checkvalue = component.find("selectAll").get("v.checked");        
        console.log('checkvalue : ',checkvalue);
        var lstCLIToDisplay =[];

        var lstCDL = component.get("v.lstCLIToDisplay");
        (lstCDL).forEach(function(record){

            record.selected=checkvalue;
            lstCLIToDisplay.push(record);
        });     
        component.set("v.lstCLIToDisplay", lstCLIToDisplay); 
    },
    handleSave: function(component, event, helper){
        component.set("v.isLoading", true);

        var listSelected = component.get("v.selectedCli");
        
        var action = component.get("c.save");      
        var isEmpty = (listSelected.toString() == '' );
        console.log('### calendar year : ', component.get("v.CalendarYear"));
        
        if(!isEmpty){
            action.setParams({
                ids : listSelected.toString(),
                index: component.get("v.IndexCalendar")+1, 
                year: component.get("v.CalendarYear")
            });  
            action.setCallback(this, function(response){                
                var data = response.getReturnValue();
                console.log(data);
                if(data.error){
                    helper.showToast("Erreur" ,"error", data.msg, "dismissible");
                }else{
                    helper.showToast("Succès", "success", data.msg, "dismissible");
                }
            });    
            $A.enqueueAction(action);
        }
        else{
            helper.showToast("Erreur" ,"error", "Veuillez séléctionner au moin une ligne.", "dismissible");
        }
        component.set("v.isLoading", false);
    },
    handleNext: function(component, event, helper){
        console.log("in handleNext");
        component.set("v.isLoading", true);

        component.set("v.booleanLoadCollectivite", true); 
 
        var hidetb1 = document.getElementsByClassName("tb1");
        hidetb1[0].style.display = 'none';

        var lc18Scr = document.getElementsByClassName("lc18Screen");
        lc18Scr[0].style.display = 'block';

        var btnPanel1 = document.getElementsByClassName("btnPanel1");
        btnPanel1[0].style.display = 'none';

        var comboType = document.getElementsByClassName("comboboxSCType");
        comboType[0].style.display = 'none';     
        
        component.set("v.isLoading", false);
    },

    onPrev: function(component, event, helper){
        console.log('## prev');
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber-1);
        var cmbxVal = component.find("cmbxProdType").get("v.value");
        helper.loadCLI(component, helper, cmbxVal, component.get("v.IndexCalendar")+1, component.get("v.CalendarYear") );

    },

    onNext: function(component, event, helper){
        console.log('## onNext');
        var pageNumber = component.get("v.pageNumber");
        console.log('pageNymver ', pageNumber);
        component.set("v.pageNumber", pageNumber+1);
        var cmbxVal = component.find("cmbxProdType").get("v.value");
        helper.loadCLI(component, helper, cmbxVal, component.get("v.IndexCalendar")+1, component.get("v.CalendarYear") );
        
        
    }
    
})