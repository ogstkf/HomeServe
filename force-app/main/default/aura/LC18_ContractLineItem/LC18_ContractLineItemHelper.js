({
    helperMethod : function() {

    },
    getLastDayOfMonth : function getLastDayOfMonth(month) {
        var lastDay;
        if(month==1|| month==3 || month==5 || month==7 || month==8 || month==10 || month==12){
            lastDay = 31;
        }
        else if(month==4 || month==6 || month==9 || month==11){
            lastDay = 30;
        }else{
            lastDay = 28; //In case of February
        }
        return lastDay;
    },
    
    checkRecord: function(component, event, helper, recordId){
        console.log('## checkrecord start');

        //check record in global list
        // component.get();
        //add to list of records
        var listClis = component.get("v.selectedCli");

        var currentSelectedId = event.getSource().get("v.name");
        console.log('currentSelectedId', currentSelectedId);

        listClis.push(currentSelectedId);
        
        component.set("v.selectedCli", listClis);

    },

    uncheckRecord: function(component, event, helper, recordId){
        console.log('## uncheckrecord start')

        //uncheck record in global list

        //remove from list
        var currentSelectedId = event.getSource().get("v.name");
        var listClis = component.get("v.selectedCli");

        for(var i = 0 ; i < listClis.length; i++){
            console.log('listClis', listClis[i]);

            if(listClis[i] == currentSelectedId){
                listClis.splice(i, 1);
            }
        }

        component.set("v.selectedCli", listClis);

        
    },

    showToast: function(title, type, message, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: mode,
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    },

    loadCLI: function(cmp, helper, serviceContractType, index, currYear){

        var pageNumber = cmp.get("v.pageNumber");
        var pageSize = cmp.get("v.pageSize");

        console.log('*** loadCLI:');    

        console.log('*** index:',index);     
        console.log('*** currYear:',currYear);   
        cmp.set("v.isLoading", true);

        var action = cmp.get("c.getContractLinetem");
        action.setParams({
            "index":index,
            "currYear":currYear,
            "scType":serviceContractType,
            "pageNumber": pageNumber,
            "pageSize": pageSize
        });
       
        var minDateMplus1 = new Date(currYear+'-'+index+'-'+'01');
        var maxDateMplus1 = new Date(currYear+'-'+index+'-'+helper.getLastDayOfMonth(index));

        var indexMinus1 = index-1;        
        var currYearMinus1;
        if(indexMinus1<1){
            indexMinus1 = 12;
            currYearMinus1 = currYear - 1;
        }else{
            currYearMinus1 = currYear;
        }
        var minDateM = new Date(currYearMinus1+'-'+indexMinus1+'-'+indexMinus1);
        var maxDateM = new Date(currYearMinus1+'-'+indexMinus1+'-'+helper.getLastDayOfMonth(indexMinus1));

        var indexPlus1 = index+1;        
        var currYearPlus1;
        if(indexPlus1>12){            
            indexPlus1 = 1;
            currYearPlus1 = currYear + 1;
        }else{
            currYearPlus1 = currYear;
        }
        var minDateMplus2 = new Date(currYearPlus1+'-'+indexPlus1+'-'+indexPlus1);
        var maxDateMplus2 = new Date(currYearPlus1+'-'+indexPlus1+'-'+helper.getLastDayOfMonth(indexPlus1));
        
        action.setCallback(this, function(response){
            console.log('##ret val : ' , response.getReturnValue());
            var data = response.getReturnValue().data;     
            console.log('*** data in helper: ',data);  
            cmp.set("v.noLignesCLI", data.length); 
            cmp.set("v.resultSize", data.length);
            console.log('##total lines: ' , response.getReturnValue().count[0].expr0);
            cmp.set("v.totalLines" , response.getReturnValue().count[0].expr0);

            helper.setCanNext(cmp);
            
            // cmp.set("v.lstCLIToDisplay", data);      
            var lstCLIToDisplay = [];

            for (var i = 0; i < data.length; i++) {                    
                let record = data[i];                
                let doc= {};
                doc.Id = record.Id;                
                doc.Type = record.ServiceContract.Type__c;                
                doc.Logement = typeof record.ServiceContract.Logement__c != 'undefined'? record.ServiceContract.Logement__r.Name: '' ;
                doc.LogementId = typeof record.ServiceContract.Logement__c != 'undefined'? record.ServiceContract.Logement__c : '';
                doc.Gestionnaire = record.ServiceContract.Type__c == 'Individual' ? '' : ((record.ServiceContract.RootServiceContract != 'undefined' && record.ServiceContract.RootServiceContract.Account != 'undefined') ? record.ServiceContract.RootServiceContract.Account.Name : '') ;
                doc.AccountNumber = record.ServiceContract.Type__c == 'Individual' ? record.ServiceContract.Account.ClientNumber__c : (typeof record.Collective_Account__c != 'undefined' ? record.Collective_Account__r.ClientNumber__c : '');
                doc.AccountName = record.ServiceContract.Type__c == 'Individual' ? record.ServiceContract.Account.Name : (typeof record.Collective_Account__c != 'undefined'? record.Collective_Account__r.Name: '');
                doc.AccountId = record.ServiceContract.Type__c == 'Individual' ? record.ServiceContract.AccountId : (typeof record.Collective_Account__c != 'undefined'? record.Collective_Account__c : '');
                               
                // doc.CollectiveAccountName = typeof record.Collective_Account__c != 'undefined'? record.Collective_Account__r.Name: '' ;
                // doc.CollectiveAccountNumber = typeof record.Collective_Account__c != 'undefined'? record.Collective_Account__r.AccountNumber : '' ;  

                doc.Equipement = typeof record.Asset != 'undefined' ? record.Asset.IDEquipmenta__c : '';
                doc.AssetId =  typeof record.Asset != 'undefined' ? record.AssetId : '';
                doc.typeEquipement = typeof record.Asset != 'undefined' ? record.Asset.Equipment_type_auto__c :'';
                doc.produit = record.Product2.Name;
                doc.ProductId = record.Product2.Id;
                doc.montant = record.UnitPrice;
                doc.MaxDate = record.VE_Max_Date__c;
                doc.MinDate = record.VE_Min_Date__c;
                doc.DesiredDate = record.Desired_VE_Date__c;
                doc.selected = false;
                // console.log('*** data in helper loadcli doc.MaxDate: ',doc.MaxDate);  
                // console.log('*** data in helper loadcli maxDateM : ',maxDateM);  
                if(new Date(record.VE_Max_Date__c) < maxDateM || new Date(record.VE_Max_Date__c).getMonth() == maxDateM.getMonth() ){
                    //console.log('*** data in helper 1: ');  
                    doc.Priority = 'Dépassé';
                    doc.sortNo = '1';
                }              
                if(new Date(record.VE_Max_Date__c).getMonth() == minDateMplus1.getMonth() && new Date(record.VE_Max_Date__c).getFullYear() == maxDateMplus1.getFullYear()){
                    //console.log('*** data in helper 2: ');  
                    doc.Priority='Echéance mois M+1';
                    doc.sortNo = '2';
                }
                if(new Date(record.VE_Max_Date__c).getMonth() == maxDateMplus2.getMonth() && new Date(record.VE_Max_Date__c).getFullYear() == maxDateMplus2.getFullYear()){
                    //console.log('*** data in helper 3: ');  
                    doc.Priority='Echéance mois M+2';
                    doc.sortNo = '3';
                }
                // if( new Date(record.VE_Min_Date__c) <= maxDateMplus1 && (new Date(record.VE_Max_Date__c).getMonth() > maxDateMplus2.getMonth() && new Date(record.VE_Max_Date__c).getFullYear() > maxDateMplus2.getFullYear())){
                if( new Date(record.VE_Min_Date__c) <= maxDateMplus1 && (new Date(record.VE_Max_Date__c) > maxDateMplus2 )){    
                    //console.log('*** data in helper 4: ');  
                    doc.Priority ='Date au + tôt atteinte';
                    doc.sortNo = '4';
                }                
                lstCLIToDisplay.push(doc);            
            }
            lstCLIToDisplay.sort(function(a, b){
                return a.sortNo - b.sortNo;
            });
            cmp.set("v.lstCLIToDisplay", lstCLIToDisplay); 
            cmp.set("v.isLoading", false);

            var checkvalue = cmp.find("selectAll").get("v.checked");        
            console.log('checkvalue : ',checkvalue);
            if(checkvalue){
                var lstCLIToDisplay =[];

                var lstCDL = cmp.get("v.lstCLIToDisplay");
                (lstCDL).forEach(function(record){

                    record.selected=checkvalue;
                    lstCLIToDisplay.push(record);
                });     
                cmp.set("v.lstCLIToDisplay", lstCLIToDisplay); 
            }
        });
        $A.enqueueAction(action);
    },

    setCanNext : function(cmp){
        var cannotNext = cmp.get('v.isLastPage');
        
        var totalLines = cmp.get('v.totalLines');

        var lastLine = (cmp.get("v.pageNumber") - 1 ) * cmp.get("v.pageSize") + cmp.get('v.resultSize');
        console.log('totalLines == lastLine', totalLines == lastLine, totalLines , lastLine);
        if(totalLines == lastLine){
            cannotNext = true; 
            cmp.set('v.isLastPage', cannotNext);
        }else{
            cannotNext = false;
            cmp.set('v.isLastPage', cannotNext);
        }
    }
})