/**
 * @File Name          : testcmpController.js
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 04/05/2020, 16:57:10
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/05/2020   ZJO     Initial Version
**/
({
    doInit: function (component, event, helper) {
        var myPageRef = component.get("v.pageReference");
        var conId = myPageRef.state.c__contractId;
        component.set("v.contractId", conId);
    }
})