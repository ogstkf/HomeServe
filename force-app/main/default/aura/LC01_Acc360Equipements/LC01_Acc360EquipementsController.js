/**
 * @File Name          : LC01_Acc360EquipementsController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 25/06/2019, 20:00:26
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    24/06/2019, 18:52:09   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        helper.populateData(component, component.get("v.recordId"));
    }
});