/**
 * @File Name          : LC01_Acc360EquipementsHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 25/06/2019, 19:59:45
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    25/06/2019, 14:36:55   RRJ     Initial Version
 **/
({
    populateData: function(component, accId) {
        var action = component.get("c.fetchEqp");
        action.setParams({
            accId: accId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.lstEqp", response.getReturnValue());
                console.log('All service contracts from fetchEqp', response.getReturnValue());
                component.set("v.loading", false);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        component.set("v.loading", false);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
});