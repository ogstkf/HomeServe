({
    HandleCap : function(component, event, helper) {

        var index = component.get("v.IndexCalendar");
        var action = component.get("c.getCaps");
        var currYear = component.get("v.CalendarYear");
        action.setParams({
            "index":index,
            "currYear":currYear
        });
        action.setCallback(this, function(response){
            var data = response.getReturnValue();
            component.set("v.CapList", data);
        });
        $A.enqueueAction(action);
    }
})