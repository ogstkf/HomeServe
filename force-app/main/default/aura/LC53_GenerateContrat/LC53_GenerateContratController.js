({

    doInit: function(component, event, helper) {
        console.log('Starting LC53_GenerateContrat doInit');
        component.set("v.showSpinner",true);

        var serConId = component.get("v.recordId");
        console.log("serConId", component.get("v.recordId"));
        var action = component.get("c.insertImageAttachment"); 
        action.setParams({"serConId" : serConId});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                var result = response.getReturnValue();            
                console.log("## result :" ,result);
                helper.createFiles(component);
            }
            else {
                console.log('## error obj : ',action.getError()[0]);
            }          
        });
        $A.enqueueAction(action);
    },
})