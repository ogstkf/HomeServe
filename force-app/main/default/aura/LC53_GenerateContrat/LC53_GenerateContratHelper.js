/**
 * @File Name          : LC53_GenerateContratHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 07/02/2020, 12:05:31
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    07/02/2020   RRJ     Initial Version
 **/
({
    createFiles: function(component) {
        console.log('Starting LC53_GenerateContrat createFiles');
        component.set('v.showSpinner', true);

        var serConId = component.get('v.recordId');
        var action = component.get('c.saveContrat');
        action.setParams({ serConId: serConId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('## result :', result);
                if (result.error) {
                    console.log('## result Error:', result.message);
                    var toastEvent = $A.get('e.force:showToast');
                    toastEvent.setParams({
                        title: 'ERROR',
                        type: 'error',
                        message: result.message
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get('e.force:closeQuickAction');
                    dismissActionPanel.fire();
                } else {
                    console.log('## Success');
                    var dismissActionPanel = $A.get('e.force:closeQuickAction');
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                    if (result.createContract) {
                        var url = '/' + result.cp;
                        window.open(url);
                        var url2 = '/' + result.cgv;
                        window.open(url2);
                        var url3 = '/' + result.Bulletin;
                        window.open(url3);
                        var url4 = '/' + result.FRetract;
                        window.open(url4);
                    }
                }
            } else {
                console.log('## error obj : ', action.getError()[0]);
            }
        });
        $A.enqueueAction(action);
    }
});