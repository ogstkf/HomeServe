/**
 * @File Name          : LC07_AddProductHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 05/11/2019, 16:00:39
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    31/10/2019   RRJ     Initial Version
 **/
({
    fetchPicklistValues: function(component) {
        console.log("### in fetchPicklistValues 123");
        console.log("### check IseEcourComm in helper 0: ", component.get("v.isEnCourComm"));

        var lstFamily = [];
        var lstPicklistVals = component.get("v.lstPicklistVals");
        var action = component.get("c.getPicklistValues");

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                lstFamily = response.getReturnValue();

                for (var i = 0; i < lstFamily.length; i++) {
                    var item = { label: lstFamily[i], value: lstFamily[i] };
                    lstPicklistVals.push(item);
                }
                component.set("v.lstPicklistVals", lstPicklistVals);
            }
        });

        $A.enqueueAction(action);
    },

    fetchAllPriceBxEntryItems: function(component) {
        console.log("### in fetchAllPriceBxEntryItems: " + component.get("v.recordId"));
        console.log("### check IseEcourComm in helper 1: ", component.get("v.isEnCourComm"));

        component.set("v.isLoading", true);
        var lstPricebkEntryItems = [];
        var action = component.get("c.getAllPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            ofst: offset,
            quoteId: component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                component.set("v.isLoading", false);
                lstPricebkEntryItems = response.getReturnValue();
                console.log("##### lstPricebkEntryItems: ", lstPricebkEntryItems);

                component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    searchPriceBkEntry: function(component) {
        console.log("### check IseEcourComm in helper 3: ", component.get("v.isEnCourComm"));

        component.set("v.isLoading", true);
        var searchText = component.find("srchbxId").get("v.value");
        var lstPricebkEntryItems = [];
        var action = component.get("c.searchPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            searchText: searchText,
            ofst: offset,
            quoteId: component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                component.set("v.isLoading", false);
                lstPricebkEntryItems = response.getReturnValue();
                component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    filterPriceBkEntry: function(component) {
        console.log("#### in filterPriceBkEntry");
        console.log("### check IseEcourComm in helper 4: ", component.get("v.isEnCourComm"));

        component.set("v.isLoading", true);
        var cmbxProdFam = component.find("cmbxProdFam").get("v.value");
        var lstPricebkEntryItems = [];
        var action = component.get("c.filterPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            prodFam: cmbxProdFam,
            ofst: offset,
            quoteId: component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                component.set("v.isLoading", false);
                lstPricebkEntryItems = response.getReturnValue();
                component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    searchFilterPriceBkEntry: function(component) {
        console.log("#### in searchFilterPriceBkEntry");

        component.set("v.isLoading", true);
        var searchText = component.find("srchbxId").get("v.value");
        var cmbxProdFam = component.find("cmbxProdFam").get("v.value");
        var lstPricebkEntryItems = [];
        var action = component.get("c.searchFilterPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            searchText: searchText,
            prodFam: cmbxProdFam,
            ofst: offset,
            quoteId: component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                component.set("v.isLoading", false);
                lstPricebkEntryItems = response.getReturnValue();
                component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    searchPriceBkEntryImmobilier: function(component) {
        console.log("#### in searchPriceBkEntryImmobilier");
        console.log("### check IseEcourComm in helper 6: ", component.get("v.isEnCourComm"));
        component.set("v.isLoading", true);

        var searchText = component.find("srchbxId").get("v.value");
        var cmbxProdFam = component.find("cmbxProdFam").get("v.value");

        console.log("#### in searchText: " + searchText);
        console.log("#### in cmbxProdFam: " + cmbxProdFam);

        var lstPricebkEntryItems = [];
        var action = component.get("c.searchImmobilierPricebkEntry");
        var offset = component.get("v.currentPage");
        offset = (offset - 1) * 50;

        action.setParams({
            searchText: searchText,
            prodFam: cmbxProdFam,
            ofst: offset,
            quoteId: component.get("v.recordId"),
            enCourComm: component.get("v.isEnCourComm")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                console.log("#### SUCCESS 123: ");
                component.set("v.isLoading", false);

                lstPricebkEntryItems = response.getReturnValue();
                component.set("v.lstPricebkEntryItems", lstPricebkEntryItems);

                if (
                    typeof lstPricebkEntryItems == "undefined" ||
                    lstPricebkEntryItems.length == 0
                ) {
                    this.goBackOnePage(component);
                } else {
                    window.scrollTo(0, 0);
                }
            }
        });

        $A.enqueueAction(action);
    },

    goBackOnePage: function(component) {
        var curPage = component.get("v.currentPage");
        var searchText = component.find("srchbxId").get("v.value");
        var cmbxVal = component.find("cmbxProdFam").get("v.value");

        if (curPage > 1) {
            component.set("v.currentPage", curPage - 1);

            if (searchText.length == 0 && (typeof cmbxVal == "undefined" || cmbxVal == "None")) {
                this.fetchAllPriceBxEntryItems(component);
            } else if (
                searchText.length > 0 &&
                (typeof cmbxVal == "undefined" || cmbxVal == "None")
            ) {
                this.searchPriceBkEntry(component);
            } else if (
                searchText.length == 0 &&
                (typeof cmbxVal != "undefined" && cmbxVal != "None")
            ) {
                this.filterPriceBkEntry(component);
            } else if (
                searchText.length > 0 &&
                (typeof cmbxVal != "undefined" && cmbxVal != "None")
            ) {
                this.searchFilterPriceBkEntry(component);
            }
        }
    },

    fetchQuoteDetails: function(component) {
        var action = component.get("c.fetchDetails");
        var quoteId = component.get("v.recordId");

        action.setParams({
            quoteId: quoteId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var details = response.getReturnValue();
                component.set("v.quo", details);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    checkIsFlow: function(component) {
        console.log("##### check isFlow");
        var availableActions = component.get("v.availableActions");
        console.log("#### availableActions: ", availableActions);
        if (!$A.util.isEmpty(availableActions)) {
            component.set("v.isFlow", true);
        }
    }
});