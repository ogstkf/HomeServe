/**
 * @File Name          : LC07_AddProductController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 04/11/2019, 22:11:03
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    31/10/2019   RRJ     Initial Version
 **/
({
    init: function(component, event, helper) {
        component.set("v.currentPage", 1);
        helper.fetchQuoteDetails(component);
        helper.fetchPicklistValues(component);
        helper.fetchAllPriceBxEntryItems(component);

        console.log("### in comm");
        var x = component.find("headerId");
        console.log("### x", x);
        // x.classList.remove("slds-global-header_container");
        $A.util.removeClass(x, "slds-global-header_container");
        $A.util.addClass(x, "slds-section slds-is-open slds-scrollable_y slds-scrollable_x");
        $A.util.removeClass(component.find("h_01"), "slds-global-header");
        // $A.util.removeClass(component.find("h_02"), "slds-global-header__item");
        // $A.util.removeClass(component.find("h_05"), "slds-global-header__item");
        $A.util.removeClass(component.find("footer_Cn1"), "slds-docked-form-footer");
        helper.checkIsFlow(component);
    },



    handleCancel: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");

        navEvt.setParams({
            recordId: component.get("v.recordId")
        });

        navEvt.fire();
    },

    handleSearch: function(component, event, helper) {
        console.log("** in handleSearch");
        component.set("v.currentPage", 1);
        console.log("** in handleSearch 1");
        var searchText = component.find("srchbxId").get("v.value");
        console.log("** in handleSearch 2");
        // var cmbxVal = component.find("cmbxProdFam").get("v.value");
        console.log("** event.which: ", event.which);

        if (event.which == 13) {
            if (searchText.length == 0) {
                helper.fetchAllPriceBxEntryItems(component);
            } else if (searchText.length > 0) {
                helper.searchPriceBkEntry(component);
            } else if (searchText.length == 0) {
                helper.filterPriceBkEntry(component);
            } else if (searchText.length > 0) {
                helper.searchFilterPriceBkEntry(component);
            }
        }
    },
    /*
    handleChange : function(component, event, helper) {
        component.set("v.currentPage", 1);
        var searchText = component.find("srchbxId").get("v.value");
        var cmbxVal = component.find("cmbxProdFam").get("v.value");

        console.log('##### searchText:',searchText);
        console.log('##### cmbxVal:',cmbxVal);

        if(cmbxVal == 'Immobilier'){
             console.log('##### in Immobilier:');
             helper.searchPriceBkEntryImmobilier(component);
        }
        // helper.searchPriceBkEntryImmobilier(component);
        else{
            if(searchText.length == 0 && (typeof cmbxVal == 'undefined' || cmbxVal == 'None')) {
                console.log('##### 1');
                helper.fetchAllPriceBxEntryItems(component);
            }
            else if(searchText.length > 0 && (typeof cmbxVal == 'undefined' || cmbxVal == 'None')) {
                console.log('##### 2');
                helper.searchPriceBkEntry(component);
            }
            else if(searchText.length == 0 && (typeof cmbxVal != 'undefined' && cmbxVal != 'None')) {
                console.log('##### 3');
                helper.filterPriceBkEntry(component);
            }
            else if(searchText.length > 0 && (typeof cmbxVal != 'undefined' && cmbxVal != 'None')) {
                console.log('##### 4');
                helper.searchFilterPriceBkEntry(component);
            }
        }

    },
    */

    clickPrev: function(component, event, helper) {
        var curPage = component.get("v.currentPage");
        var searchText = component.find("srchbxId").get("v.value");
        // var cmbxVal = component.find("cmbxProdFam").get("v.value");

        if (curPage > 1) {
            component.set("v.currentPage", curPage - 1);

            if (searchText.length == 0) {
                helper.fetchAllPriceBxEntryItems(component);
            } else if (searchText.length > 0) {
                helper.searchPriceBkEntry(component);
            } else if (searchText.length == 0) {
                helper.filterPriceBkEntry(component);
            } else if (searchText.length > 0) {
                helper.searchFilterPriceBkEntry(component);
            }
        }
    },

    clickNext: function(component, event, helper) {
        var curPage = component.get("v.currentPage");
        var searchText = component.find("srchbxId").get("v.value");
        // var cmbxVal = component.find("cmbxProdFam").get("v.value");

        component.set("v.currentPage", curPage + 1);

        if (searchText.length == 0) {
            helper.fetchAllPriceBxEntryItems(component);
        } else if (searchText.length > 0) {
            helper.searchPriceBkEntry(component);
        } else if (searchText.length == 0) {
            helper.filterPriceBkEntry(component);
        } else if (searchText.length > 0) {
            helper.searchFilterPriceBkEntry(component);
        }
    },

    goNext: function(component, event, helper) {
        var curPage = component.get("v.currentPage");
        console.log("*** goNext ", component.get("v.selPriceBkEntry"));

        component.set("v.showAddProd", false);
        component.set("v.showCalcProdAmt", true);
    },

    handleSelectedChange: function(component, event, helper) {
        var selectedPB = component.get("v.selPriceBkEntry");
        component.set("v.selPrcBkEntriesJSON", JSON.stringify(selectedPB));
    }

    /*handleNext : function(component, event, helper) {
        var selPBE = null;
        var lstPricebkEntryItems = component.get("v.lstPricebkEntryItems");

        for(var i=0; i<lstPricebkEntryItems.length; i++) {
            if(lstPricebkEntryItems[i].selected) {
                selPBE = lstPricebkEntryItems[i].pbe;
                break;
            }
        }

        if(selPBE != null) {
            component.set("v.selPriceBkEntry", selPBE);
            component.set("v.showAddProd", false);
            component.set("v.showCalcProdAmt", true);
        }
    },*/
    /*
    changeEncourComm : function(component, event, helper) {
        // console.log('### check encourComm: ',   event.getSource().get('v.value'));
        
        console.log('### check encourComm 2: ',   component.find("enCourCommId").get("v.value"));

        component.set("v.isEnCourComm", component.find("enCourCommId").get("v.value"));

        component.set("v.currentPage", 1);
        var searchText = component.find("srchbxId").get("v.value");
        var cmbxVal = component.find("cmbxProdFam").get("v.value");

        if(searchText.length == 0 && (typeof cmbxVal == 'undefined' || cmbxVal == 'None')) {
            helper.fetchAllPriceBxEntryItems(component);
        }
        else if(searchText.length > 0 && (typeof cmbxVal == 'undefined' || cmbxVal == 'None')) {
            helper.searchPriceBkEntry(component);
        }
        else if(searchText.length == 0 && (typeof cmbxVal != 'undefined' && cmbxVal != 'None')) {
            helper.filterPriceBkEntry(component);
        }
        else if(searchText.length > 0 && (typeof cmbxVal != 'undefined' && cmbxVal != 'None')) {
            helper.searchFilterPriceBkEntry(component);
        }

    },
    */
});