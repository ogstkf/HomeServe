({
    handleLoad: function(component,event,helper){
        var recordId = component.get('v.recordId');
        var action = component.get("c.getNewDevisType");
        action.setParams({ quoteId : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.devisType', response.getReturnValue());
                component.set('v.showSpinner', false);
            }
            
        });
        
        $A.enqueueAction(action);
    },
    doInit: function(cmp, event, helper) {
       cmp.set('v.disabled', false);
       cmp.set('v.showSpinner', false);
        cmp.set('v.hasError', false);
    },
    
    handleSubmit: function(cmp, event, helper) {
        debugger;
        if(event.getParams().fields.Agence__c){
        	cmp.set('v.disabled', true);
        	cmp.set('v.showSpinner', true);
            cmp.set('v.hasError', false);
        }else{
            cmp.set('v.hasError',true);
            cmp.set('v.error', 'Veuillez choisir une agence');
            event.preventDefault();
        }
       
    },
    
    handleError: function(cmp, event, helper) {
        debugger;
       cmp.set('v.disabled', false);
       cmp.set('v.showSpinner', false);
    },
    
    handleSuccess: function(cmp, event, helper) {
        var params = event.getParams();
        var quoteId = cmp.get('v.recordId');
        var devisTypeId = params.response.id;
        
        
        var action = cmp.get("c.createLigneDevisType");
        action.setParams({devisTypeId:devisTypeId, quoteId: quoteId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set('v.showSpinner', false);
                cmp.set('v.saved', true);
                
                var navEvt = $A.get("e.force:navigateToSObject");
                
                navEvt.setParams({
                    "recordId": devisTypeId
                });
                
                navEvt.fire();
            }
            
        });
        
        $A.enqueueAction(action);

    },
    handleCancel : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        
        navEvt.setParams({
            "recordId": component.get("v.recordId")
        });
        
        navEvt.fire();
    }
})