/**
 * @File Name          : LC02_CreationComptePersonnelController.js
 * @Description        :
 * @Author             : ZJO
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 02-08-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    16/07/2019, 18:01:42   RRJ     Initial Version
 **/
({
    doInit: function (component, event, helper) {
        // console.log("## init client det:", JSON.stringify(component.get("v.clientDet")));
        helper.getPickVal(component);
        helper.fetchPicklistValues(component);
    },

    back: function (component, event, helper) {
        helper.changePage(component, 1);
    },

    copyAddress: function (component, event, helper) {
        var clDetail = component.get("v.clientDet");

        var copyAdd = component.get("v.memeAddLog");
        if (copyAdd) {
            clDetail.clCP = clDetail.clCPLog;
            clDetail.clRue = clDetail.clRueLog;
            clDetail.clVille = clDetail.clVilleLog;
            clDetail.clPays = clDetail.clPaysLog;
            clDetail.clImm_Res = clDetail.clImm_ResLog;
            clDetail.clStair = clDetail.clStairLog;
            clDetail.clFloor = clDetail.clFloorLog;
            clDetail.clDoor = clDetail.clDoorLog;
            component.set("v.clientDet", clDetail);
            // console.log("##### after check: ", JSON.stringify(clDetail));
        }
    },

    creerDemande: function (component, event, helper) {
        console.log("### RUN ");
        helper.populateEqpType(component);
        var action = component.get("c.creerDemandePerso");
        var details = component.get("v.clientDet");
        // console.log("### details ", details);
        //ZJO save field campagne value - CT-64
        var campagneId = component.get("v.selectRecordId");
        details.clCampagne = campagneId;
        //  console.log("##### before creating demande: ", JSON.stringify(details));
        var cleanedDetails = {};
        Object.keys(details).forEach(function (prop) {
            if (details[prop]) {
                cleanedDetails[prop] = details[prop];
            }
        });
        // console.log("### Det Client: ", cleanedDetails);
        // console.log("### JSON Client: ", JSON.stringify(cleanedDetails));
        component.set("v.showSpinner", true);
        action.setParams({
            jsonInput: JSON.stringify(cleanedDetails)
        });
        // console.log("jsonInput: ", jsonInput);
        action.setCallback(this, function (response) {
            // alert ('ok11');
            var state = response.getState();
            console.log("state: ", state);          
            if (state === "SUCCESS") {
                // console.log("success");
                helper.showToast("Réussite", "success", "la demande a été créée", "dismissible");
                var cseId = response.getReturnValue();
                 console.log("##########case id: ", cseId);

                helper.redirectDemande(component, cseId);
            } else if (state === "INCOMPLETE") {
                // alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast("ERROR", "error", errors[0].message, "dismissible");
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    onControllerFieldChange: function (component, event, helper) {
        helper.fetchDepValue(component);
    },

    fetchLogAsset: function (component, event, helper) {
        console.log("##### event: ", event);
        var details = component.get("v.clientDet");
        if (details.clEqpExistant) {
            component.set("v.showSpinner", true);
            var action = component.get("c.getLogmentAssets");
            action.setParams({
                jsonInput: JSON.stringify(details)
            });

            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    // console.log("success");
                    console.log("##### resp log: ", response.getReturnValue());
                    var assets = response.getReturnValue();
                    if ($A.util.isEmpty(assets)) {
                        component.set("v.assetExists", false);
                        details.doNotCreateAsset = false;
                        component.set("v.clientDet", details);
                        helper.showToast(
                            "",
                            "warning",
                            "Aucun équipement corréspondant retrouvé",
                            "dismissible"
                        );
                    } else {
                        component.set("v.assetExists", true);
                        details.doNotCreateAsset = true;
                        component.set("v.clientDet", details);
                    }
                } else if (state === "INCOMPLETE") {
                    // alert("Incomplete");
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            helper.showToast("ERROR", "error", errors[0].message, "dismissible");
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
                component.set("v.showSpinner", false);
            });
            $A.enqueueAction(action);
        } else {
            component.set("v.assetExists", false);
            details.doNotCreateAsset = false;
            component.set("v.clCodePostal", details);
        }
    },

    cpLogChange: function (component, event, helper) {
        var cp = component.get("v.clientDet").clCPLog;
        if (cp.length === 5) {
            //helper.checkCPValidity(component);
        }
    },

    handleDetailChange: function (component, event, helper) {
        var details = component.get("v.clientDet");
        if (
            !(
                $A.util.isEmpty(details.clEqpType) &&
                $A.util.isEmpty(details.clMotif) &&
                $A.util.isEmpty(details.clPresta) &&
                $A.util.isEmpty(details.clCPLog)
            )
        ) {
            //helper.checkPrestaValidity(component);
        }
    },
    //ZJO CT-64 - l'ajout du code campagne (get list of campaigns)
    searchCampagne: function (component, event, helper) {
        var currentText = event.getSource().get("v.value");
        var resultBox = component.find('resultBox');
        component.set("v.Loading", true);

        if (currentText.length > 0) {
            $A.util.addClass(resultBox, 'slds-is-open');
        }
        else {
            $A.util.removeClass(resultBox, 'slds-is-open');
        }

        var action = component.get("c.getResults");
        action.setParams({
            "searchVal": currentText
        });

        action.setCallback(this, function (response) {
            var STATE = response.getState();
            if (STATE === "SUCCESS") {
                component.set("v.searchRecords", response.getReturnValue());

                if (component.get("v.searchRecords").length == 0) {
                    console.log('000000');
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.Loading", false);
        });

        $A.enqueueAction(action);
    },
    //ZJO CT-64 - l'ajout du code campagne (set selected campaign)
    setSelectedRecord: function (component, event, helper) {
        var currentText = event.currentTarget.id;
        var resultBox = component.find('resultBox');
        $A.util.removeClass(resultBox, 'slds-is-open');

        component.set("v.selectRecordName", event.currentTarget.dataset.name);
        component.set("v.selectRecordId", currentText);
        component.find('userinput').set("v.readonly", true);
    },
    //ZJO CT-64 - l'ajout du code campagne (clear selected campaign)
    resetData: function (component, event, helper) {
        component.set("v.selectRecordName", "");
        component.set("v.selectRecordId", "");
        component.find('userinput').set("v.readonly", false);
    }
});