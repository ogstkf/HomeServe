/**
 * @File Name          : LC07_SelectCatalogueHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 06/12/2019, 13:10:17
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    02/12/2019   RRJ     Initial Version
 **/
({
    setParentPbId: function(component) {
        var pbId = component.get("v.pbId");
        var lstPB = component.get("v.lstPB");
        var pb = lstPB.find(pb => pb.Id === pbId);
        console.log("##### pb: ", pb);
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            action: "setPB",
            data: pb
        });
        console.log("#### evt: ", cmpEvent);
        cmpEvent.fire();
        console.log("fired");
    }
});