/**
 * @File Name          : LC07_SelectCatalogueController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 06/12/2019, 13:10:20
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    02/12/2019   RRJ     Initial Version
 **/
({
    handleChange: function(component, event, helper) {
        helper.setParentPbId(component);
    }
});