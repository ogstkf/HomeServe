/**
 * @File Name          : LC23_SelectPBEController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 20/12/2019, 12:10:36
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    18/12/2019   RRJ     Initial Version
 **/
({
    handleSearchChange: function(component, event, helper) {
        console.log("event search: ", event);
        var searchVal = event.getParam("value");
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            action: "searchPBE",
            data: { searchText: searchVal }
        });
        cmpEvent.fire();
    },

    clickPrev: function(component, event, helper) {
        var currPage = component.get("v.pageNum");
        helper.changePage(component, currPage, currPage - 1);
    },
    clickNext: function(component, event, helper) {
        var currPage = component.get("v.pageNum");
        helper.changePage(component, currPage, currPage + 1);
    },

    handleLstPbeCHange: function(component, event, helper) {
        var lstPbe = component.get("v.lstPbe");
        helper.displayList(component, lstPbe);
    },

    handleSelected: function(component, event, helper) {
        console.log(event);
        var pbeId = event.target.dataset.pbeid;
        helper.populateSelected(component, pbeId);
    },

    showSelected: function(component, event, helper) {
        var lstSelected = component.get("v.lstSelected");
        component.set("v.lstToDisplay", lstSelected);
        helper.displayList(component, lstSelected);
        component.set("v.isShowSelected", true);
    },

    showResult: function(component, event, helper) {
        var lstPbe = component.get("v.lstPbe");
        helper.displayList(component, lstPbe);
        component.set("v.isShowSelected", false);
    },

    handleNav: function(component, event, helper) {
        // console.log(event);
        var prodId = event.target.dataset.prodid;
        // console.log(prodId);
        helper.navigateToRecord(component, event, prodId);
    }
});