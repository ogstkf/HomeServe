/**
 * @File Name          : LC23_SelectPBEHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 20/12/2019, 12:15:44
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    18/12/2019   RRJ     Initial Version
 **/
({
    displayList: function(component, list) {
        var numPages = Math.ceil(list.length / 50);
        component.set("v.numPages", numPages);
        component.set("v.pageNum", 1);
        if (list.length <= 50) {
            component.set("v.lstToDisplay", list);
        } else {
            component.set("v.lstToDisplay", list.slice(0, 50));
        }
    },

    populateSelected: function(component, pbeId) {
        var lstPBE = component.get("v.lstToDisplay");
        var lstSelected = [];
        // var pbe = lstPBE.find(pbe => {
        //     return pbe.Id === pbeId;
        // });
        // if (pbe.isLineSelected) {
        // }
        lstPBE.forEach(pbe => {
            if (pbe.Id === pbeId && pbe.isLineSelected) {
                pbe.isLineSelected = true;
                lstSelected.push(pbe);
            } else {
                pbe.isLineSelected = false;
            }
        });
        component.set("v.lstToDisplay", lstPBE);
        component.set("v.lstSelected", lstSelected);
    },

    navigateToRecord: function(component, event, objId) {
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: objId,
                actionName: "view"
            }
        };
        event.preventDefault();
        navService.navigate(pageReference);
    },

    changePage: function(component, originPage, targetPage) {
        var lstPbe = component.get("v.lstPbe");
        if (targetPage > originPage) {
            var lstDisplay = lstPbe.slice(originPage * 50, targetPage * 50);
        } else {
            var lstDisplay = lstPbe.slice((targetPage - 1) * 50, (originPage - 1) * 50);
        }
        component.set("v.lstToDisplay", lstDisplay);
        component.set("v.pageNum", targetPage);
    }
});