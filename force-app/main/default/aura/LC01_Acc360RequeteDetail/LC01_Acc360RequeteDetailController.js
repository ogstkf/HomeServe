/**
 * @File Name          : LC01_Acc360RequeteDetailController.js
 * @Description        :
 * @Author             : DMU
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 28/11/2019, 13:41:51
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    25/06/2019, 11:00:00   DMU     Initial Version
 **/
({
    navToCase: function(component, event, helper) {
        var caseId = component.get("v.ca").cse.Id;
        console.log("##### case: ", caseId);
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: caseId,
                actionName: "view"
            }
        };

        helper.navigateToRecord(component, event, pageReference);
    },
    handleCreateQuote: function(component, event, helper) {
        var caseWrap = component.get("v.ca");
        var cse = caseWrap.cse;
        var acc = component.get("v.accDet");
        console.log("##### cse: " , cse);
        console.log("##### acc: " , acc.Campagne__c);
        console.log("##### woId: " , caseWrap.woId);
        if (caseWrap.numWO > 1) {
            component.set("v.showModalWo", true);
        } else {
            helper.createQte(component, event, cse, acc, caseWrap.woId);
        }
    },

    handleLoadModal: function(component, event, helper) {
        var modalVisibility = component.get("v.showModalWo");
        var cas = component.get("v.ca").cse;
        if (modalVisibility === true) {
            helper.retrieveWO(component, cas.Id);
        }
    },

    handleCloseModal: function(component, event, helper) {
        helper.closeModal(component);
    },

    handleModalCreateQuo: function(component, event, helper) {
        var woId = component.get("v.woId");
        var cse = component.get("v.ca").cse;
        var acc = component.get("v.accDet");
        helper.closeModal(component);
        helper.createQte(component, event, cse, acc, woId);
    }
});