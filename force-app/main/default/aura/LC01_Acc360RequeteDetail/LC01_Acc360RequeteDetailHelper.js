/**
 * @File Name          : LC01_Acc360RequeteDetailHelper.js
 * @Description        :
 * @Author             : DMU
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 28/11/2019, 13:35:06
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    25/06/2019, 11:00:00   DMU     Initial Version
 **/
({
    navigateToRecord: function(component, event, pageReference) {
        var navCase = component.find("navService");
        event.preventDefault();
        navCase.navigate(pageReference);
    },

    createQte: function(component, event, cse, acc, woId) {
        component.set("v.showSpinner", true);
        console.log('cse1 = ', cse);
        console.log('acc1 = ', acc);
        console.log('woId1 = ', woId);
        var creationQte = component.get("c.createQuote");
        creationQte.setParams({ cse: cse, acc: acc, woId: woId });
        creationQte.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if (result.success) {
                    var pageReference = {
                        type: "standard__recordPage",
                        attributes: {
                            recordId: result.qteId,
                            actionName: "view"
                        }
                    };
                    this.navigateToRecord(component, event, pageReference);
                } else {
                    this.showToastMessage("ERROR", "error", result.message);
                }
            }
        });
        $A.enqueueAction(creationQte);
    },

    showToastMessage: function(messageTitle, messageType, messageText) {
        var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            title: messageTitle,
            type: messageType,
            message: messageText,
            duration: 5000
        });
        resultsToast.fire();
    },

    retrieveWO: function(component, caseId) {
        component.set("v.showModalSpinner", true);
        var action = component.get("c.fetchWo");
        action.setParams({
            caseId: caseId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var lstWo = this.processMapWo(response.getReturnValue());
                component.set("v.lstWo", lstWo);
            } else if (state === "INCOMPLETE") {
                alert("Incomplete");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showModalSpinner", false);
        });

        $A.enqueueAction(action);
    },

    closeModal: function(component) {
        component.set("v.showModalWo", false);
    },

    processMapWo: function(mapWo) {
        console.log(mapWo);
        var lstWo = [];
        for (var key in mapWo) {
            lstWo.push({ label: mapWo[key], value: key });
        }
        return lstWo;
    }
});