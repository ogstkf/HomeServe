({
    doInit : function(component, event, helper) {
        helper.doInit(component);
    },

    quoteInfo : function(component, event, helper) {
        var redirectionCaseEvt = $A.get("e.force:navigateToSObject");
        if (redirectionCaseEvt) {
            redirectionCaseEvt.setParams({
                "recordId": event.target.getAttribute("data-id")
            });
            redirectionCaseEvt.fire();
        }
        else {
            var surl = window.location.href.split('/')[0];
            surl += '/lightning/r/Quote/';
            surl += event.target.getAttribute("data-id");
            surl += '/view';
            window.location = surl;
        }
    },

    actionSelected : function(component, event, helper) {
        var action = event.getParam("value");
        var recordId = event.getSource().get("v.name");
        if (action === 'edit')
            helper.editQuote(recordId);
        else if (action === 'delete') {
            component.set("v.quoteDel", {
                id: recordId,
                modalShow: true
            });
        }
    },

    cancel : function(component, event, helper) {
        helper.close(component);
    },

    deleteQuote : function(component, event, helper) {
        var action = component.get("c.delQuote");
        console.log(JSON.stringify(component.get("v.quoteDel")));
        action.setParams({
            quoId: component.get("v.quoteDel").id
        });
        helper.close(component);
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());
                var toastEvent = $A.get('e.force:showToast');
                if (toastEvent) {
                    toastEvent.setParams({
                        title: 'test',
                        type: 'success',
                        message: 'Quote "'+ response.getReturnValue() +'" was deleted'
                    });
                    toastEvent.fire();
                }
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(state + ' Error message: ' + errors[0].message);
                    }
                } else {
                    console.log(state + ' Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },

    showToast: function(component, event, helper) {
        if (!component.get("v.isLoading")) 
            helper.doInit(component);
    },

    viewAll: function(component, event, helper) {
        var navigateToCmpEvt = $A.get("e.force:navigateToComponent");
        console.log(component.get("v.quotes"));
        navigateToCmpEvt.setParams({
            componentDef : "c:LC78_QuoteListView",
            componentAttributes: {
                recordId: component.get("v.recordId"),
                subject: component.get("v.subject")
            }
        });
        navigateToCmpEvt.fire();
    },

    
})