({
    doInit: function(component) {
        component.set("v.isLoading", true);
        var quotes = [];
        var quotesToDisplay = [];
        console.log(component.get("v.subject"));

        var action = component.get('c.getData');
        action.setParams({
            accId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                console.log(response.getReturnValue());

                //Quote
                var quoteReturnValue = response.getReturnValue().lstwrpQuoType;
                var nbQuotes = quoteReturnValue.length;
                if (nbQuotes > 3) {
                    component.set("v.nbQuotes", "3+");
                }
                else
                    component.set("v.nbQuotes", nbQuotes.toString());

                for (var i = 0; i < nbQuotes; i++) {
                    let quote = {};
                    quote.Type = quoteReturnValue[i].type;
                    for  (var j in quoteReturnValue[i].quo) {
                        quote[j] = quoteReturnValue[i].quo[j];
                    }
                    
                    quote.Devis_etabli_le__c = this.dateFormat(quote.Devis_etabli_le__c);
                    quote.Prix_TTC__c = this.deviseFormat(quote.Prix_TTC__c);
                    if (i < 3)
                        quotesToDisplay.push(quote);
                    quotes.push(quote);
                }
                console.log(quotes);

                component.set("v.quotes", quotes);
                component.set("v.quotesToDisplay", quotesToDisplay);
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(state + ' Error message: ' + errors[0].message);
                    }
                } else {
                    console.log(state + ' Unknown error');
                }
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    },

    dateFormat : function(value) {
        if (!value)
            return '';
        var dateArray = value.split('-');
        return dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0];
    },

    deviseFormat : function(value) {
        return 'EUR ' + new Intl.NumberFormat('fr-FR',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(value);
    },

    editQuote : function(recordId) {
        var editQuoteEvt = $A.get("e.force:editRecord");
        editQuoteEvt.setParams({
            recordId: recordId
        });
        editQuoteEvt.fire();
    },

    close : function(component) {
        component.set("v.quoteDel", {
            id: "",
            modalShow: false
        });
    },

})