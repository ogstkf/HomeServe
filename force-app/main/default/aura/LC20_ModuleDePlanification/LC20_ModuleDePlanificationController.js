({
    doInit : function(component, event, helper) {  

        //var calendarMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        var calendarMonths = ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"];
        
        var currYear = new Date().getFullYear();

        var index =  new Date().getMonth();
        component.set("v.CalendarYear",currYear);
        component.set("v.IndexCalendar",index);
        component.set("v.CalendarValue",calendarMonths[index]+' '+currYear);
    },

    handleMonth :function(component, event, helper) {  

        //var calendarMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        var calendarMonths = ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"];

        var index = component.get("v.IndexCalendar");
        var currYear = component.get("v.CalendarYear");

        if(event.getSource().getLocalId() == "Previous"){
            if(index!=0){
                index-=1;
            }else{
                currYear-=1;
                index=11;
            }
        }else{
            if(index!=11){
                index+=1;
            }else{
                index=0;
                currYear+=1;
            }

        }

        component.set("v.CalendarYear",currYear);
        component.set("v.IndexCalendar",index);
        component.set("v.CalendarValue",calendarMonths[index] + ' '+currYear);      
    }
})