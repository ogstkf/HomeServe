/**
 * @File Name          : LC02_EligibilityResultClientHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 02-04-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    09/07/2019, 21:51:41   RRJ     Initial Version
 * 1.1    07/01/2020             SH      Change function buildAccLines
 * 1.2    04/02/2021             MNA     Adding fields Imm_Res__c, Stair__c, Floor__c, Door__c xhen logement is check or uncheck
 **/
({
    navigateToRecord: function(component, event, objId) {
        console.log("navigate", objId);
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: objId,
                actionName: "view"
            }
        };

        event.preventDefault();
        navService.navigate(pageReference);
    },

    checkLogement: function(component, logId) {
        var lstAcc = component.get("v.gridData");
        lstAcc.forEach(acc => {
            if (acc._children) {
                acc._children.forEach(log => {
                    if (log.Id == logId) {
                        log.btnIcon = "utility:check";
                        log.btnLabel = "Logement Sélectionné";
                        log.btnVariant = "success";
                        // log.btnDisabled = true;
                    } else {
                        log.btnIcon = "utility:add";
                        log.btnLabel = "Sélectionner Logement";
                        log.btnVariant = "neutral";
                        // log.btnDisabled = false;
                    }
                });
            }
        });
        component.set("v.gridData", lstAcc);
    },

    fetchLogementDetail: function(component, logId) {
        var clientDet = component.get("v.clientDetails");
        var lstAcc = component.get("v.gridData");
        var logement = {};
        lstAcc.forEach(acc => {
            if (acc._children) {
                acc._children.forEach(log => {
                    if (log.Id == logId) {
                        logement = log;
                    }
                });
            }
        });
        clientDet.clRueLog = logement.Street__c;
        clientDet.clCPLog = logement.Postal_Code__c;
        clientDet.clUsagePro = logement.Professional_Use__c;
        clientDet.clVilleLog = logement.City__c;
        clientDet.clAgeLogement = logement.Age__c;
        clientDet.clLogId = logement.Id;
        clientDet.clLogAgency = logement.Agency;
        clientDet.clLogAgencyId = logement.Agence__c;
        clientDet.clLogSelected = true;
        clientDet.doRefresh = true;
        clientDet.clImm_Res = logement.Imm_Res__c;
        clientDet.clStair = logement.Stair__c;
        clientDet.clFloor = logement.Floor__c;
        clientDet.clDoor = logement.Door__c;

        // // console.log("### dets: " + JSON.stringify(clientDet));
        component.set("v.clientDetails", clientDet);
    },

    isLogSelected: function(component, rowId) {
        var clDet = component.get("v.clientDetails");
        return clDet.clLogId == rowId;
    },

    uncheckLogement: function(component) {
        var clientDet = component.get("v.clientDetails");
        var lstAcc = component.get("v.gridData");
        lstAcc.forEach(acc => {
            if (acc._children) {
                acc._children.forEach(log => {
                    log.btnIcon = "utility:add";
                    log.btnLabel = "Sélectionner Logement";
                    log.btnVariant = "neutral";
                    // log.btnDisabled = false;
                });
            }
        });
        delete clientDet.clRueLog;
        delete clientDet.clCPLog;
        delete clientDet.clUsagePro;
        delete clientDet.clVilleLog;
        delete clientDet.clAgeLogement;
        delete clientDet.clLogAgency;
        delete clientDet.clLogAgencyId;
        delete clientDet.clLogId;
        delete clientDet.clLogSelected;
        delete clientDet.clEqpType;
        delete clientDet.clPresta;
        delete clientDet.clMotif;
        delete clientDet.clImm_Res;
        delete clientDet.clStair;
        delete clientDet.clFloor;
        delete clientDet.clDoor;
        clientDet.doRefresh = true;

        component.set("v.gridData", lstAcc);
        component.set("v.clientDetails", clientDet);
    },

    buildAccLines: function(accLogLine) {
        var accLine = accLogLine.acc;

        accLine.Name = accLine.Name + "\n" + " "; // Added SH - Alt 255

        accLine.btnVariant = "neutral";
        accLine.btnLabel = "Sélectionner Client";
        accLine.btnAction = "selectAcc";
        console.log($A.util.isEmpty(accLogLine.logements));
        if (!$A.util.isEmpty(accLogLine.logements)) {
            accLine._children = accLogLine.logements;
            accLine._children.forEach(function(log) {
                log.Name = log.Name + "\n" + " "; // Added SH - Alt 255
                log.Adresse = log.Street__c + "\n" + log.Postal_Code__c + " " + log.City__c; // Modified SH
                
                if ((log.Door__c != null) || (log.Floor__c != null)) { // Added SH
                         if (log.Door__c  == null) { log.Adresse = log.Adresse + "\n" + "(Etage : " + log.Floor__c + ")"; }
                    else if (log.Floor__c == null) { log.Adresse = log.Adresse + "\n" + "(Porte : " + log.Door__c + ")"; }
                    else { 
                        console.log('etage porte');
                        log.Adresse = log.Adresse + "\n(Etage : " + log.Floor__c + " - Porte : " + log.Door__c + ")"; 
                    }
                    log.Name = log.Name + "\n" + " "; // Added - SH : Alt 255
                }

                //log.Agency = $A.util.isEmpty(log.Agency__c) ? "" : log.Agency__r.Name;
                log.btnLabel = "Sélectionner Logement";
                log.btnAction = "selectLog";
                log.btnIcon = "utility:add";
            });
        }

        return accLine;
    },

    getIdAccounts: function(accLogLine) {
        var accLine = accLogLine.acc.Id;
        return accLine;
    }

});