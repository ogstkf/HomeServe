/**
 * @File Name          : LC02_EligibilityResultClientController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 05-08-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    08/07/2019, 14:20:59   RRJ     Initial Version
 * 1.1    13/01/2020             SH      Added Expand all rows when load
 * 1.2    04/02/2021             MNA     Adding columns Immeuble/résidence, Escalier, Etage, Porte
 **/
({
    init: function(component) {
        var columns = [
            {
                type: "text",
                fieldName: "Name",
                label: "Nom"
            },
            {
                type: "text",
                fieldName: "ClientNumber__c",
                label: "Numéro Client"
            },
            {
                type: "text",
                fieldName: "Adresse",
                label: "Adresse"
            },
            {
                type: "text",
                fieldName: "Imm_Res__c",
                label: "Immeuble/résidence"
            },
            {
                type: "text",
                fieldName: "Stair__c",
                label: "Escalier"
            },
            {
                type: "text",
                fieldName: "Numero_ESI__c",
                label: "Numéro ESI"
            },
            {
                type: "text",
                fieldName: "Floor__c",
                label: "Etage"
            },
            {
                type: "text",
                fieldName: "Door__c",
                label: "Porte"
            },
            {
                type: "phone",
                fieldName: "Phone",
                label: "Téléphone"
            },
            {
                type: "phone",
                fieldName: "PersonHomePhone",
                label: "Téléphone Mobile"
            },
            {
                type: "phone",
                fieldName: "PersonMobilePhone",
                label: "Téléphone Fixe"
            },
            {
                type: "email",
                fieldName: "PersonEmail",
                label: "Email"
            },
            {
                type: "button",
                typeAttributes: {
                    name: { fieldName: "btnAction" },
                    iconName: { fieldName: "btnIcon" },
                    label: { fieldName: "btnLabel" },
                    title: { fieldName: "btnLabel" },
                    variant: { fieldName: "btnVariant" },
                    disabled: { fieldName: "btnDisabled" }
                }
            }
        ];
        component.set("v.gridColumns", columns);

        var data = [];
        component.set("v.gridData", data);
    },

    handleRowAction: function(component, event, helper) {
        var row = event.getParam("row");
        if (row.btnAction == "selectAcc") {
            helper.navigateToRecord(component, event, row.Id);
        } else if (row.btnAction == "selectLog") {
            // console.log("#### log row: ", row);
            if (helper.isLogSelected(component, row.Id)) {
                helper.uncheckLogement(component);
            } else {
                helper.checkLogement(component, row.Id);
                helper.fetchLogementDetail(component, row.Id);
            }
        }
    },

    dataChanged: function(component, event, helper) {
        console.log('changge');
        var lines = component.get("v.tableData");

        // The first line is a fake one with nbAccounts and nbLogements
        // Its email is : 'FakeAccountUsedForCalculation@xxx.xxx'
        var nbAccs = lines[0].acc.Name;
        var nbAccsDisplayed = lines[0].acc.AccountNumber;
        console.log(nbAccs+ '/' + nbAccsDisplayed);
        component.set("v.nbAccounts", nbAccs);
        component.set("v.nbAccountsDisplayed", nbAccsDisplayed);

        // We remove this first line 
        var xxx = lines.shift();
        console.log(JSON.parse(JSON.stringify(lines)));
        var formattedLines = lines.map(helper.buildAccLines);
        console.log(JSON.parse(JSON.stringify(formattedLines)));
        component.set("v.gridData", formattedLines);

        var expandedRows = lines.map(helper.getIdAccounts);
        component.set('v.gridExpandedRows', expandedRows);
        console.log(JSON.parse(JSON.stringify(expandedRows)));
    },
});