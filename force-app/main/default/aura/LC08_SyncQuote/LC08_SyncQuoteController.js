({
	handleCancel : function(component, event, helper) {
		var navEvt = $A.get("e.force:navigateToSObject");
        
        navEvt.setParams({
          "recordId": component.get("v.recordId")
        });

        navEvt.fire();
	},
    handleSave : function(component, event, helper) {
        console.log('** in handleSave');
        console.log('** quoteId' + component.get("v.recordId"));
        
        component.set("v.isLoading", true);
        
		var action = component.get("c.SyncQuote");        
        action.setParams({
            "quoteId" : component.get("v.recordId")
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {                
                component.set("v.isLoading", false);

                var result =  response.getReturnValue();
                console.log('## result : ',result);                              
				
				component.set("v.message", result);
            }else{
                component.set("v.message", "Une erreur s'est produite lors de la synchronisation.  Merci d'essayer ulterierement.");
            }           

            $A.get("e.force:closeQuickAction").fire(); 
            var showToast = $A.get('e.force:showToast');
            showToast.setParams( { 'title': '',
                                    'type': 'success',
                                    'message': component.get("v.message")});
            showToast.fire();
            
        });
        $A.enqueueAction(action);   
    }
    
})