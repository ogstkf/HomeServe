/**
 * @File Name          : LC01_Acc360EqpDetailHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 09/01/2020, 17:52:23
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    25/06/2019, 20:01:38   RRJ     Initial Version
 * 1.1    02/01/2020, 19:57:59   SH      Rajout du déménagement
 **/
({
    newServiceContract: function(component, event, helper) {
        component.set("v.isLoading", true);

        var AccId = component.get("v.eqp").eqpAccId;
        var equipId = component.get("v.eqp").eqpId;

        var action = component.get("c.CreateContract");
        action.setParams({
            equipId: equipId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                console.log("Id SVC : ", response.getReturnValue());
                component.set("v.isLoading", false);
                this.navigateToRecord(component, event, response.getReturnValue());
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast("ERROR", "error", errors[0].message, "dismissible");
                    }
                } else {
                    console.log("Unknown error");
                    component.set("v.isLoading", false);
                    helper.showToast("ERROR", "error", "Unknown error", "dismissible");
                }
            }
            component.set("v.isLoading", false);
        });

        $A.enqueueAction(action);
    },

    generateURL: function(component, eqpId) {
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: eqpId,
                actionName: "view"
            }
        };
        var url = "";
        var defaultUrl = "#";
        navService.generateUrl(pageReference).then(
            $A.getCallback(function(url) {
                url = url ? url : defaultUrl;
                component.set("v.url", url);
                // return url;
            }),
            $A.getCallback(function(error) {
                url = defaultUrl;
                component.set("v.url", defaultUrl);
                // return url;
            })
        );
    },

    navigateToRecord: function(component, event, objId) {
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: objId,
                actionName: "view"
            }
        };
        // event.preventDefault();
        navService.navigate(pageReference);
    },

    removeLinkEquipementAccount: function(component) {
        var dateDemenagement = component.get("v.dateDemenagement");
        var eqpId = component.get("v.eqp").eqpId;

        var action = component.get("c.removeLinkEquipementAndAccount");

        component.set("v.showSpinner", true);
        action.setParams({
            eqpId: eqpId,
            dateDemenagement: dateDemenagement
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                location.reload(); // Doesn't work : $A.get('e.force:refreshView').fire();
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showToast("ERROR", "error", errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });

        $A.enqueueAction(action);
    },

    showToast: function(title, type, message, mode) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: mode,
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    }
});