/**
 * @File Name          : LC01_Acc360EqpDetailController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 09/01/2020, 18:09:14
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    25/06/2019, 20:01:15   RRJ     Initial Version
 * 1.1    02/01/2020, 19:57:59   SH      Rajout du déménagement
 **/
({
    doInit: function(component, event, helper) {
        var eqpId = component.get("v.eqp").eqpId;
        // console.log("###### eqp: ", JSON.stringify(component.get("v.eqp")));
        helper.generateURL(component, eqpId);
        // var url = helper.generateURL(component, eqpId);
        // component.set("v.url", url);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD"); // SH
        component.set("v.dateDemenagement", today);
    },

    openEqp: function(component, event, helper) {
        var navService = component.find("navService");
        var eqpId = component.get("v.eqp").eqpId;
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: eqpId,
                actionName: "view"
            }
        };
        event.preventDefault();
        navService.navigate(pageReference);
    },

    openAgence: function(component, event, helper) {
        var agenceId = component.get("v.eqp").eqpAgenceId;
        helper.navigateToRecord(component, event, agenceId);
    },

    openContrat: function(component, event, helper) {
        console.log("Open contrat");
        // var contratId = component.get('v.eqp').eqpContServIdCreate;
        var contratId =
            component.get("v.eqp").eqpContServIdCreate != null
                ? component.get("v.eqp").eqpContServIdCreate
                : component.get("v.eqp").eqpContServIdDisplay;
        console.log("Contract Id: " + contratId);
        helper.navigateToRecord(component, event, contratId);
    },

    newDemande: function(component, event, helper) {
        var eqp = component.get("v.eqp");
        console.log("#### eqp: ", eqp);
        var createRecordEvent = $A.get("e.force:createRecord");

        var values = {
            AccountId: eqp.eqpAccId == null || eqp.eqpAccId == "" ? null : eqp.eqpAccId,
            ContactId: eqp.eqpContactId == null || eqp.eqpContactId == "" ? null : eqp.eqpContactId,
            Status: "New",
            Priority: "Medium",
            Origin: "Phone",
            AssetId: eqp.eqpId == null || eqp.eqpId == "" ? null : eqp.eqpId,
            Local_Agency__c: eqp.eqpAgenceId == null || eqp.eqpAgenceId == "" ? null : eqp.eqpAgenceId,
            Service_Contract__c: eqp.eqpContServIdCreate,
            Campagne__c : eqp.eqpCampagne, //RRA - 20200720 - CT-64 - l'ajout du code campagne - reference sur la page confluence 
            Contract_Line_Item__c : eqp.eqpSousContractId,
        };
        if (eqp.eqpIsCollectif) {
            values.Contract_Line_Item__c = eqp.eqpSousContractId;
        }

        createRecordEvent.setParams({
            entityApiName: "Case",
            defaultFieldValues: values
        });

        createRecordEvent.fire();
    },

    openVEFlow: function(component, event, helper) {
        var navService = component.find("navService");
        var contractId = component.get("v.eqp").eqpSousContractId;

        var pageRef = {
            type: "standard__webPage",
            attributes: {
                url: "/flow/Schedule_VE?recordId=" + contractId + "&retURL=" + contractId
            }
        };

        event.preventDefault();
        navService.navigate(pageRef);
    },

    handleConfirmDialog: function(component, event, helper) {
        component.set("v.showConfirmDialog", true);
    },

    handleConfirmDialogYes: function(component, event, helper) {
        var dateDemenagement = component.get("v.dateDemenagement");

        if (dateDemenagement != null) {
            component.set("v.showConfirmDialog", false);
            helper.removeLinkEquipementAccount(component);
        }
    },

    handleConfirmDialogNo: function(component, event, helper) {
        component.set("v.showConfirmDialog", false);
    },

    servConCreate: function(component, event, helper) {
        console.log("Enter");
        component.set("v.chkServCon", false);
        helper.newServiceContract(component);
    },

    chkCreationContract: function(component, event, helper) {
        component.set("v.isLoading", true);
        var equipId = component.get("v.eqp").eqpId;

        var action = component.get("c.fetchServiceCon");

        action.setParams({
            assetId: equipId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var lstServcon = [];
                lstServcon = response.getReturnValue();
                console.log("List of service contract: ", lstServcon);
                var lstScCreate = [];

                if (lstServcon.length > 0) {
                    for (var i = 0; i < lstServcon.length; i++) {
                        if (
                            lstServcon[i].Contract_Status__c != "Expired" &&
                            lstServcon[i].Contract_Status__c != "Cancelled" &&
                            lstServcon[i].Contract_Status__c != "Résilié" &&
                            lstServcon[i].Contract_Status__c != "Résiliation pour retard de paiement"
                        ) {
                            component.set("v.chkServCon", true);
                            console.log("Do not pass");
                            break;
                        } else {
                            console.log("Enter Else Part not expired");
                            lstScCreate.push(lstServcon[i]);
                        }
                    }
                    if (lstScCreate.length > 0 && component.get("v.chkServCon") == false) {
                        console.log("All expired");
                        helper.newServiceContract(component);
                    }
                } else {
                    console.log("No service contract present");
                    helper.newServiceContract(component);
                }

                component.set("v.isLoading", false);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                    component.set("v.isLoading", false);
                }
            }
            component.set("v.isLoading", false);
        });

        $A.enqueueAction(action);
    },

    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"
        component.set("v.chkServCon", false);
    }
});