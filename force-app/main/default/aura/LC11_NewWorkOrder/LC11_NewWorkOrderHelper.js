({
    // helperMethod : function() {

    // }

    /** handles create new record and sets default values to screen (account id)
     * @param  {Object} component - Lightning component object
     * @param  {Object} event - lightning event object
     * @param  {Object} helper - lightning helper
     * @return none
     */
    createNewRecord: function(component) {
        // var url = new URL(document.location.href);
        // var recTypeId = url.searchParams.get("recordTypeId");
        // var additionalParams = url.searchParams.get("additionalParams");
        // var recordId = additionalParams.substring(additionalParams.indexOf("a0j1")).slice(0, 15);
        // var recordTypeLabel =  "WorkOrder";

        var recordId = component.get("v.recordId");
        console.log('@@@@@@@@ :', recordId );  

        var action = component.get("c.fetchDefaultValue");
        action.setParams({
            caseId: recordId
            // "recordTypeLabel": recordTypeLabel
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("##### resp success: ", response.getReturnValue());
                // after successful callback
                // build defaultVals
                var defaultVals = response.getReturnValue();
                defaultVals['Quote__c'] = component.get("v.selectedQuote");
                console.log('defaultVals: ' , defaultVals);
                // defaultVals
                this.insertWorkOrder(component, "WorkOrder", defaultVals);
            }
        });

        $A.enqueueAction(action);
    },

    // defaultValues
    createRecord: function(component, objName, defaultValues) {
        // var createRecordEvent = $A.get("e.force:createRecord");
        // createRecordEvent.setParams({
        //     entityApiName: objName,
        //     defaultFieldValues: defaultValues
        // });
        // createRecordEvent.fire();

        

        this.retrieveQuotes(component);



    },

    insertWorkOrder: function(component, objName, defaultValues){
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            entityApiName: objName,
            defaultFieldValues: defaultValues
        });
        createRecordEvent.fire();
    },
    
    retrieveQuotes: function(component){
        var recordId = component.get("v.recordId");
        console.log('@@@@@@@@ :', recordId );  

        var action = component.get("c.getQuotes");
        action.setParams({
            caseId: recordId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("##### retrieveQuotes resp success: ", response.getReturnValue());
                if(response.getReturnValue() != undefined && response.getReturnValue().length == 0){
                    component.set("v.selectedQuote", null);
                    this.createNewRecord(component);
                }else{
                    component.set("v.quotes" , response.getReturnValue());
                    component.set("v.isOpen", true);
                }
                
            }
        });

        $A.enqueueAction(action);

    }

})