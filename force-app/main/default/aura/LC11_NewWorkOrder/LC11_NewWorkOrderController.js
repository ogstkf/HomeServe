({
    /**
     * Method used to launch the create record screen
     * @param  Object component - the lightning component
     * @param  Object event - the lightning event object
     * @param  Object helper - the lightning helper object
     * @return none
     */
    createRecord: function(component, event, helper) {
        helper.createRecord(component);
        // helper.createRecord(component);
    },

    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },

    next: function(component, event, helper){

        console.log('## next');
        component.set("v.isOpen", false);
        var selectedQuote = component.get("v.selectedQuote");
        helper.createNewRecord(component);
        console.log(selectedQuote);
    }
})