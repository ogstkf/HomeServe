({
    handleLinkClick : function(component, event, helper) {
        window.open('/' + component.get("v.pbe").Id); 
        /*var navEvt = $A.get("e.force:navigateToSObject");
        
        navEvt.setParams({
          "recordId": component.get("v.pbe").Id
        });

        navEvt.fire();*/
    },

    handleChange : function(component, event, helper) {
        var selPBE = null;
        var lstPricebkEntryItems = component.get("v.lstPBE");

        for(var i=0; i<lstPricebkEntryItems.length; i++) {
            if(lstPricebkEntryItems[i].selected) {
                selPBE = lstPricebkEntryItems[i].pbe;
                break;
            }
        }

        if(selPBE != null) {
            component.set("v.selPBE", selPBE);
            component.set("v.showAddProd", false);
            component.set("v.showCalcProdAmt", true);
        }
    },

    handleOnChange : function(component, event, helper) {
        var selPBE = [];
        var lstPricebkEntryItems = component.get("v.lstPBE");
        console.log('** lstPricebkEntryItems 12: ', lstPricebkEntryItems);

        for(var i=0; i<lstPricebkEntryItems.length; i++) {
            if(lstPricebkEntryItems[i].selected) {
                selPBE.push(lstPricebkEntryItems[i].pbe);
                
            }
        }
        console.log('** selPBE 12: ', selPBE);

        if(selPBE != null) {
            component.set("v.selPBE", selPBE);
            // component.set("v.showAddProd", false);
            // component.set("v.showCalcProdAmt", true);
        }
    },
})