/**
 * @File Name          : LC56_MesEquipementsController.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/8/2020, 2:38:28 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/13/2020   RRJ     Initial Version
 **/
({
    doInit: function (component, event, helper) {
        helper.fetchAssetDetails(component);
    },

    openResilier: function (component, event, helper) {
        var resilier = component.find('modalResilier');
        resilier.set('v.showModal', true);
    },

    redirectToRenouvellement: function (component, event, helper) {
        var i = event.target.dataset.index;
        var contractId = component.get('v.lstWrapper')[i].renewCli.ServiceContractId;
        // debugger;
        //Get ServiceContractId
        var urlEvent = $A.get('e.force:navigateToURL');
        urlEvent.setParams({
            url: '/renouvellement-contrat?contractId=' + contractId,
        });
        urlEvent.fire();
    },

    handleVoirContrat: function (component, event, helper) {
        var i = event.target.dataset.index;
        var url = component.get('v.lstWrapper')[i].contractPdfUrl;
        window.open(url);
    },

    goToEqpDetail: function (component, event, helper) {
        var i = event.target.dataset.index;
        var lstWrapper = component.get('v.lstWrapper');

        var assetId = lstWrapper[i].asset.Id;
        var contractId = lstWrapper[i].cli.ServiceContractId;

        //Get ServiceContractId
        var urlEvent = $A.get('e.force:navigateToURL');
        urlEvent.setParams({
            url: '/equipementdetail?assetId=' + assetId + '&contractId=' + contractId,
        });
        urlEvent.fire();
    },
});