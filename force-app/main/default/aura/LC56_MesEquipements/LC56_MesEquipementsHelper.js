/**
 * @File Name          : LC56_MesEquipementsHelper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 3/21/2020, 12:37:25 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    3/13/2020   RRJ     Initial Version
 **/
({
    fetchAssetDetails: function(component) {
        component.set('v.isLoading', true);

        var action = component.get('c.fetchAssetDetails');

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                component.set('v.lstWrapper', response.getReturnValue());
                console.log('###### list wrapper: ', response.getReturnValue());
            } else if (state === 'INCOMPLETE') {
                alert('Incomplete');
            } else if (state === 'ERROR') {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Error message: ' + errors[0].message);
                    }
                } else {
                    console.log('Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
        component.set('v.isLoading', false);
    }
});