({
    fetchUserDetails: function(component) {
        var action = component.get('c.fetchUserDetails');

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();

                component.set('v.Account', result);
                console.log('###### Account: ', response.getReturnValue());
            } else {
                console.log('Failed with state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})