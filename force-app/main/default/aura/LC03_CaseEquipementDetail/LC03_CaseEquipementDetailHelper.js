/**
 * @File Name          : LC03_CaseEquipementDetailHelper.js
 * @Description        :
 * @Author             : DMU
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 30/07/2019, 11:56:05
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    09/07/2019, 10:30:00        	DMU     				Initial Version
 **/
({
    navigateToRecord: function(component, event, recordId) {
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__recordPage",
            attributes: {
                recordId: recordId,
                actionName: "view"
            }
        };
        event.preventDefault();
        navService.navigate(pageReference);
    }
});