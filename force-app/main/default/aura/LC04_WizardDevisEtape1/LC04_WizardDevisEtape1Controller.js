/**
 * @File Name          : LC04_WizardDevisEtape1Controller.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 28/10/2019, 18:23:11
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    22/10/2019   RRJ     Initial Version
 **/
({
    doInit: function(component, event, helper) {
        helper.fetchRelatedLogements(component);
        helper.fetchTypeMotif(component);
    },

    logementChange: function(component, event, helper) {
        var logementId = component.get("v.logementId");
        var logements = component.get("v.logements");
        var cas = component.get("v.requete");
        var equipements = [];
        var logement = logements.find(function(log) {
            return log.Id === logementId;
        });
        if (!$A.util.isEmpty(logement) && !$A.util.isEmpty(logement.Equipements__r)) {
            equipements = logement.Equipements__r;
        }
        component.set("v.equipements", equipements);
        component.set("v.equipementId", null);
        cas.AssetId = "";
        cas.Type = "";
        cas.Reason__c = "";
        component.set("v.requete", cas);
        component.set("v.isSousContrat", false);
    },

    equipementChange: function(component, event, helper) {
        var equipementId = component.get("v.equipementId");
        var equipements = component.get("v.equipements");
        var cas = component.get("v.requete");

        var equipment = equipements.find(function(eqp) {
            return eqp.Id === equipementId;
        });
        cas.AssetId = equipment.Id;
        cas.Type = "";
        cas.Reason__c = "";
        component.set("v.requete", cas);
        if (!$A.util.isEmpty(equipment) && equipment.isSousContrat) {
            component.set("v.isSousContrat", true);
            component.set("v.contractId", equipment.contractId);
        } else {
            component.set("v.isSousContrat", false);
            component.set("v.contractId", "");
        }
    },

    typeChange: function(component, event, helper) {
        var type = component.get("v.type");
        var types = component.get("v.types");
        var selectedType = types.find(function(typ) {
            return typ.value == type;
        });
        var motifs = selectedType.children;
        component.set("v.motifs", motifs);
        var cas = component.get("v.requete");
        cas.Type = type;
        console.log(cas);
        component.set("v.requete", cas);
    },

    motifChange: function(component, event, helper) {
        var motif = component.get("v.motif");
        var cas = component.get("v.requete");
        cas.Reason__c = motif;
        component.set("v.requete", cas);
        console.log("###### caseeee: ", cas);
    }
});