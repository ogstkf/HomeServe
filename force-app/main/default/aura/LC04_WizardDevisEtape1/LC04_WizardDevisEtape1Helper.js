/**
 * @File Name          : LC04_WizardDevisEtape1Helper.js
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 28/10/2019, 18:22:21
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    22/10/2019   RRJ     Initial Version
 **/
({
    fetchRelatedLogements: function(component) {
        // console.log("#### comp: ", component);
        component.set("v.showSpinner", true);
        var recordId = component.get("v.recordId");
        var action = component.get("c.fetchLogements");
        action.setParams({
            accId: recordId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log(response.getReturnValue());
                var data = response.getReturnValue();
                this.processData(data);
                component.set("v.logements", data.logements);
            } else if (state === "INCOMPLETE") {
                console.log("Incomplete request");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
    },

    processData: function(data) {
        var logements = data.logements;
        var conLines = data.conLines;

        logements.forEach(function(log) {
            if (!$A.util.isEmpty(log.Equipements__r)) {
                var equipements = log.Equipements__r;
                equipements.forEach(function(eqp) {
                    var isSousContrat = conLines.some(function(conLine) {
                        return conLine.AssetId === eqp.Id;
                    });
                    if (isSousContrat === true) {
                        eqp.isSousContrat = true;
                        var contractLine = conLines.find(function(conLine) {
                            return conLine.AssetId === eqp.Id;
                        });
                        eqp.contractId = contractLine.ServiceContractId;
                    }
                });
            }
        });
    },

    fetchTypeMotif: function(component) {
        var action = component.get("c.fetchCaseTypeMotif");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // console.log(response.getReturnValue());
                component.set("v.types", response.getReturnValue());
            } else if (state === "INCOMPLETE") {
                console.log("Incomplete request");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
});