<!--
  @File Name          : VFP06_FioulAttestation.page
  @Description        : 
  @Author             : RRJ
  @Group              : 
  @Last Modified By   : RRJ
  @Last Modified On   : 5/7/2020, 7:27:11 PM
  @Modification Log   : 
  Ver       Date            Author      		    Modification
  1.0    5/7/2020   RRJ     Initial Version
-->
<apex:page applyHtmlTag="false"
           showHeader="false" 
           standardStylesheets="false" 
          renderAs="pdf"
           controller="VFC06_PACAttestation"
           docType="html-5.0">

    <html>
        <head>
            <style>

            @media print {
               html, body {  
                border: 1px solid white;
                /* height: 99%;               */
                page-break-after: avoid;
                page-break-before: avoid;
              }
            }
                      
            @page {
                    margin-top: 0.5cm;
                    margin-bottom: 0.5cm;
                    margin-left: 0.5cm;
                    margin-right: 0.5cm;
            }
                                   
            </style>
        </head>     
        
        <body class="body">

                <!-- 1st page -->
                <table>
                        <tr>
                            <th style="width:20%">
                                    <apex:image id="header" url="{!$Resource.VPF06_Header_Logo}" width="150px" />
                            </th>
                            <th style="width:45%;color:White; background:#f0522f;">
                               <div style='font-size:14.0pt;font-family:"Arial",sans-serif;text-align: center;'>
                                        Chaudière Fioul - Attestation d'entretien annuel
                                </div>
                                <div style='font-size:8.0pt;font-family:"Arial",sans-serif;text-align: center;font-style: italic;font-weight: 100;'>
                                        au sens de l'arrêté du 15 septembre 2009 
                                        <br/>
                                        A conserver 2 ans minimum
                                </div> 
        
                            </th>
                        </tr>
                </table>
                <div style='color:black;font-size:8.0pt;font-family:"Arial",sans-serif'>
                    <div id='tableHeaderInfo'>
                        <table width="100%" style="border:none;line-height:11px">
                            <tr>
                                <tr>
                                    <td width="45%" style=""></td>
                                    <td width="30%" style="color: #f0522f; font-weight: 800;">
                                        Vos informations :
                                    </td>
                                    <td width="25%" style="color: #f0522f; font-weight: 800;">
                                        Notre intervention : 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {!currentServiceAppointment.ServiceTerritory.Name} &nbsp;
                                        <apex:outputText value="{!IF(currentServiceAppointment.ServiceTerritory.TerritoryStat__c='filiale' , 'Siège - Raison sociale', '')}" />                
                                    </td>
                                    <td>N° Client : {!currentServiceAppointment.Account.ClientNumber__c}</td>
                                    <td>N° : {!currentServiceAppointment.AppointmentNumber}</td>
                                </tr>
                                <tr>
                                    <td>{!currentServiceAppointment.ServiceTerritory.Street}</td>
                                    <td>N° Equipement :  
                                        {!IF(currentServiceAppointment.Work_Order__r.Case.Asset.TECH_Equipement_ID_cham__c != null , currentServiceAppointment.Work_Order__r.Case.Asset.TECH_Equipement_ID_cham__c, currentServiceAppointment.Work_Order__r.Case.Asset.IDEquipmenta__c)}</td>
                                    <td>Objet : &nbsp;<apex:outputField value="{!currentServiceAppointment.Work_Order__r.WorkType.Type__c}"/></td>
                                </tr>
                                <tr>
                                    <td>{!currentServiceAppointment.ServiceTerritory.PostalCode} {!currentServiceAppointment.ServiceTerritory.City}</td>
                                    <td>Tél : {!IF(currentServiceAppointment.Account.Phone != null , currentServiceAppointment.Account.Phone, currentServiceAppointment.Account.PersonMobilePhone)}</td>                                    
                                    <td>Date : &nbsp;<apex:outputText value="{0,date,dd/MM/yyyy}">
                                                <apex:param value="{!currentServiceAppointment.ActualEndTime}" /> 
                                            </apex:outputText>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tél : {!currentServiceAppointment.ServiceTerritory.Phone__c}</td>
                                    <td>Mail : {!currentServiceAppointment.Account.PersonEmail}</td>
                                    <td>Heure arrivée :  &nbsp;<apex:outputText value="{0,date,HH:mm}">
                                                            <apex:param value="{!currentServiceAppointment.ActualStartTime + offset}" /> 
                                                        </apex:outputText>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mail : {!currentServiceAppointment.ServiceTerritory.Email__c}</td>
                                    <td>N° Contrat : {!currentContrat.ContractNumber}</td>
                                    <td>Heure départ : &nbsp;<apex:outputText value="{0,date,HH:mm}">
                                                            <apex:param value="{!currentServiceAppointment.ActualEndTime + offset}" /> 
                                                    </apex:outputText>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SIRET : {!currentServiceAppointment.ServiceTerritory.Siren__c } {!currentServiceAppointment.ServiceTerritory.Siret__c}</td>
                                    <td>Renouvellement : <apex:outputText value="{0,date, dd/MM/yyyy}">
                                        <apex:param value="{!currentContrat.EndDate}" /> 
                                    </apex:outputText> 
                                    </td>
                                    <td>
                                        <!-- Réalisé : -->
                                    </td>
                                </tr>
                                <tr>
                                    <td>TVA : {!currentServiceAppointment.ServiceTerritory.Intra_Community_VAT__c}</td>
                                </tr>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="font-size:10.0pt;line-height:15px">{!currentServiceAppointment.Account.Name}<br/>
                                    <apex:outputPanel rendered="{!currentServiceAppointment.Street != null}" >
                                        <apex:outputPanel rendered="{!currentServiceAppointment.Immeuble_R_sidence__c != null}" >
                                            {!'Bât.: '+currentServiceAppointment.Immeuble_R_sidence__c}
                                            <apex:outputPanel rendered="{!currentServiceAppointment.Porte__c == null}" >
                                                {!' '}<br/>
                                            </apex:outputPanel>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!currentServiceAppointment.Porte__c != null}" >
                                            {!' Porte: '+currentServiceAppointment.Porte__c}<br/>
                                        </apex:outputPanel>
                                        {!currentServiceAppointment.Street}<br/>
                                        {!currentServiceAppointment.Compl_ment__c}<br/>
                                        {!currentServiceAppointment.PostalCode} {!UPPER(currentServiceAppointment.City)}
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!currentServiceAppointment.Street == null}" >
                                    <apex:outputPanel rendered="{!currentServiceAppointment.Residence__r.Imm_Res__c != null}" >
                                            {!'Bât.: '+currentServiceAppointment.Residence__r.Imm_Res__c}
                                            <apex:outputPanel rendered="{!currentServiceAppointment.Residence__r.Door__c == null}" >
                                                {!' '}<br/>
                                            </apex:outputPanel>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!currentServiceAppointment.Residence__r.Door__c != null}" >
                                            {!'Porte: '+currentServiceAppointment.Residence__r.Door__c}<br/>
                                        </apex:outputPanel>
                                        {!currentServiceAppointment.Residence__r.Street__c}<br/>
                                        {!currentServiceAppointment.Residence__r.Adress_complement__c}<br/>
                                        {!currentServiceAppointment.Residence__r.Postal_Code__c} {!UPPER(currentServiceAppointment.Residence__r.City__c)}
                                    </apex:outputPanel>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div id='tableVotreChaudiere'>
                            <table width="100%">
                                    <th style="color: #f0522f; font-weight: 800; padding: 10px 0 10px 0">Votre chaudière fioul : </th>
                                    <tr>
                                        <td width="38%">Marque : {!currentServiceAppointment.Work_Order__r.Case.asset.Product2.Brand__c} </td>
                                        <td width="31%">N° série : {!currentServiceAppointment.Work_Order__r.Case.asset.SerialNumber} </td>
                                        <td width="31%">Brûleur:</td>
                                    </tr>
                                    <tr>
                                        <td>Modèle : {!currentServiceAppointment.Work_Order__r.Case.asset.Product2.Model__c}</td>
                                        <td>Date de mise en service* :&nbsp; 
                                                <apex:outputText value="{0,date,dd/MM/yyyy}">
                                                <apex:param value="{!currentServiceAppointment.Work_Order__r.Case.asset.InstallDate}" />
                                              </apex:outputText>
                                        </td>
                                        <td>Marque, modèle : {!currentServiceAppointment.Work_Order__r.Case.asset.Product2.Model__c}</td>
                                    </tr>
                                    <tr>
                                        <td>Puissance (kW) : {!IF(currentMeasurement.Puissance__c == 0, "", currentMeasurement.Puissance__c)}</td>
                                        <td>Date de dernier entretien* :&nbsp; 
                                                <apex:outputText value="{0,date,dd/MM/yyyy}">
                                                <apex:param value="{!dateEntretien}" />
                                              </apex:outputText>
                                        </td>
                                        <td>Puissance (kW) : {!currentServiceAppointment.Work_Order__r.Case.asset.Burner_power_in_kw__c}</td>
                                    </tr>
                                    <tr>
                                        <td>Type mode d'évacuation : {! currentServiceAppointment.Work_Order__r.Case.asset.Product2.Evacuation_type__c}</td>
                                        <td>Date de dernier ramonage** :&nbsp; 
                                                <apex:outputText value="{0,date,dd/MM/yyyy}">
                                                <apex:param value="{!dateRamonage}" />
                                              </apex:outputText>
                                        </td>
                                        <td>N° Série : {!currentServiceAppointment.Work_Order__r.Case.asset.Burner_Serial_number__c}</td>
                                    </tr>
                                    <tr>
                                        <!-- <td>Adresse d'intervention si différente de ci-dessus : <br/> {!addrLogement}</td> -->
                                        <td></td>
                                        <td style="font-style: italic;font-size:6.0pt;">(*) Si connu (**) Si connu et disponible</td>
                                        <td>Date mise en service :&nbsp; 
                                                <apex:outputText value="{0,date,dd/MM/yyyy}">
                                                <apex:param value="{!currentServiceAppointment.Work_Order__r.Case.asset.Burner_Installation_date__c}" />
                                              </apex:outputText>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" >Adresse d'intervention si différente de ci-dessus : {!IF(addrLogement != adresseCompte , addrLogement , ' ')}
                                    
                                        </td>
                                    </tr>

                                </table>
                        </div>
                        <br/>
                        <div id='tableReleveMesures'>
                            <table width="100%" style="border:2px solid #f0522f">
                                <th colspan="3" style="color: #f0522f;  font-weight: 800; padding: 2px 10px 10px 0px">Relevé des mesures à puissance nominale de fonctionnement : </th>
                                <tr>
                                    <td colspan="3">
                                        Appareil de mesure (marque et référence) : {!currentMeasurement.Appareil_de_mesure__c}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="38%">
                                        CO<sub>2</sub> : {!currentMeasurement.CO2_Max_en__c}% et/ou O<sub>2</sub> : {!currentMeasurement.O2_Max__c}%
                                    </td>
                                    <td width="31%">
                                        T Fumée : {!currentMeasurement.T_Fumee__c} ºC
                                    </td>
                                    <td width="31%">
                                        CO<sub>2</sub> Fumée : {!ROUND(currentMeasurement.CO_fumees_en_ppm__c, 2)} ppm
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        CO ambiant : 
                                        <apex:outputPanel >
                                            {!ROUND(currentMeasurement.CO_Ambiant_ppm__c, 2)}  
                                            <apex:outputPanel rendered="{!coAmbiantStr = 'inferieur_a_10'}">
                                                <span style="padding-left:1px;font-style: italic;font-size:7.0pt;">
                                                    ppm : situation normale
                                                </span> 
                                            </apex:outputPanel>
                                            <apex:outputPanel rendered="{!coAmbiantStr = '10_a_50'}">
                                                    <span style="padding-left:1px;font-style: italic;font-size:7.0pt;">
                                                        ppm : situation anormale nécessitant des investigations complémentaires concernant le tirage du conduit de fumée et de la ventilation du local
                                                    </span> 
                                            </apex:outputPanel>
                                            <apex:outputPanel rendered="{!coAmbiantStr = 'superieur_ou_egale_50'}">
                                                    <span style="padding-left:1px;font-style: italic;font-size:7.0pt;">
                                                        ppm : danger grave et immédiate nécessitant une mise à l'arrêt
                                                    </span> 
                                            </apex:outputPanel>   
                                        </apex:outputPanel>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <td colspan="3">
                                        <apex:outputPanel rendered="{!coAmbiantStr = 'inferieur_a_10'}">
                                               <div style="padding-left:65px;font-style: italic;">
                                                    inférieur à 10 ppm : situation normale
                                                </div> 
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!coAmbiantStr = '10_a_50'}">
                                               <div style="padding-left:65px;font-style: italic;">
                                                   compris entre 10 et 40 ppm : situation anormale nécessitant des investigations complémentaires concernant le tirage du conduit de fumée et de la ventilation du local
                                                </div> 
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!coAmbiantStr = 'superieur_ou_egale_50'}">
                                               <div style="padding-left:65px;font-style: italic;">
                                                   supérieur ou égal à 50 ppm : danger grave et immédiate nécessitant une mise à l'arrêt
                                                </div> 
                                        </apex:outputPanel>                                        
                                    </td>
                                </tr> -->
                                <tr>
                                    <td width="38%">
                                        T eau chaude : {!currentMeasurement.Temperature_eau_chaude_en_C__c} ºC
                                    </td>
                                    <td width="31%">
                                        T eau froide : {!currentMeasurement.Temperature_eau_froide_en_C__c} ºC
                                    </td>
                                    <td width="31%">
                                        Débit d'eau chaude sanitaire : {!ROUND(currentMeasurement.Debit_litre_en_L_min__c, 2)}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="38%">
                                        Rendement PCS : {!currentMeasurement.Rendement_PCI_Percent__c} %
                                    </td>
                                    <td width="31%">
                                        Emissions évaluées de NOx : {!currentMeasurement.Emission_valu_s_de_Nox__c}
                                    </td>
                                    <td width="31%">
                                        Tirage si B1 : {!currentMeasurement.Tirage_si_B1__c}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="38%" style="font-style: italic;">
                                        Rendement de référence : {!IF(currentServiceAppointment.Work_Order__r.Case.asset.Product2.Efficiency__c == 0, "", currentServiceAppointment.Work_Order__r.Case.asset.Product2.Efficiency__c)}
                                        %
                                    </td>
                                    <td width="31%" style="font-style: italic;">
                                        Emissions NOx de référence : {!IF(currentServiceAppointment.Work_Order__r.asset.Nox__c == 0, "", currentServiceAppointment.Work_Order__r.asset.Nox__c)}
                                    </td>
                                    <td width="31%">
                                        Opacité : {!currentMeasurement.Opacite__c}
                                    </td>
                                </tr>
                            </table>
                        </div>               

                        <br/>
                        <br/>
                        <div id='tableCompteRendu'>
                            <table width="100%" style="border:2px solid #f0522f; table-layout: fixed;">
                                <th style="color: #f0522f; font-weight: 800; padding: 2px 10px 10px 0px">Notre compte-rendu d'intervention : </th>
                                <tr>
                                    <td style="vertical-align:top;">
                                    <div style="height: 50px;overflow-y:hidden;">
                                        <apex:outputText value="{!currentServiceAppointment.Work_Order__r.Comments__c}" escape="false"/>
                                    </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <br/>
                        <div id='tableAutreInformations'>
                            <table width="100%">
                                <th colspan="2" style="color: #f0522f; font-weight: 800; padding: 2px 10px 10px 4px">Autre Informations : </th>
                                <tr style="height: 25px">
                                    <td width="63px" style="padding: 0px 0px 0px 4px;vertical-align:top;">
                                        <span style="font-weight: bold">Anomalies : </span>

                                    </td>
                                    <td >
                                        <apex:repeat var="ano" value="{!lstWrapperAnomaly}">
                                            <div>
                                                {!ano.typeReason}
                                            </div>
                                           <div>
                                                {!ano.description}
                                           </div>
                                        </apex:repeat>

                                        <apex:outputText value="Pas d'anomalie constatée" rendered="{!IF(isNull(lstWrapperAnomaly),true,false)}"/>
                                    </td>
                                </tr>
                                <tr style="height: 25px">
                                    <td colspan="2" style="padding: 0px 0px 0px 4px">
                                        <span style="font-weight: bold">Ramonage : </span><apex:outputText value="Ramonage du conduit de fumée réalisé. Encadré par le Règlement Sanitaire du département (Article 31.6)" rendered="{!IF(currentServiceAppointment.Work_Order__r.TECH_WO_Ramonage__c,true,false)}"/>
                                        <div style="padding-left:63px;"><apex:outputText value="Cette attestation vaut également certificat de ramonage au sens de cet Article" rendered="{!IF(currentServiceAppointment.Work_Order__r.TECH_WO_Ramonage__c,true,false)}"/></div>
                                    </td>
                                </tr>
                                <tr style="height: 25px">
                                    <td colspan="2" style="padding: 0px 0px 0px 4px">
                                        <span style="font-weight: bold">TVA : </span>{!currentServiceAppointment.Work_Order__r.TECH_TVA__c}
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <br/>
                        <div id='tableSignature'>
                            <table width="100%">
                                    <tr style="height:100px">
                                        <!-- <td width="49%" style="border:2px solid #f0522f;vertical-align:top;">
                                            <div style="color: #f0522f;  font-weight: bold; padding: 2px 10px 10px 4px">Signature du client : </div>
                                            <apex:outputPanel rendered="{!idSignature!='null'}">
                                                <apex:image id="sig" url="{!idSignature}" width="150px" />
                                            </apex:outputPanel>
                                        </td> -->
                                        <td width="49%" style="border:2px solid #253474;vertical-align:top;">
                                            <div style="color: #253474;  font-weight: bold; padding: 2px 10px 0px 4px">Signature du client : </div>
                                            <div style="padding: 2px 10px 10px 4px">
                                                <apex:outputText style="color: black;font-weight: normal;" value="Client absent" rendered="{! currentServiceAppointment.Signature_Client_absence__c }"/>
                                                <apex:outputText style="color: black;font-weight: normal;" value="Client présent mais refuse de signer" rendered="{! AND(NOT(currentServiceAppointment.Signature_Client_absence__c), currentServiceAppointment.Signature_Client_not_approved__c) }"/>
                                            </div>
                                            <apex:outputPanel rendered="{!idSignature!='null'}">
                                                <apex:image id="sig" url="{!idSignature}" width="150px" rendered="{! AND(NOT(currentServiceAppointment.Signature_Client_absence__c), NOT(currentServiceAppointment.Signature_Client_not_approved__c), currentServiceAppointment.Signature_Client_approved__c) }"/>
                                            </apex:outputPanel>
                                        </td>
                                            <td width="2%"></td>
                                        <td width="49%"  style="border:2px solid #f0522f;vertical-align:top; ">
                                            <div style="padding: 2px 10px 0px 4px">Nom du technicien ayant effectué la visite : {!nomTechnincien}</div>
                                            <div style="color: #f0522f;  font-weight: bold; ; padding: 2px 10px 10px 4px">Signature du technicien : </div>
                                            <apex:outputPanel rendered="{!idSignatureTechnicien!='null'}">
                                                <apex:image id="sig2" url="{!idSignatureTechnicien}" width="150px" />
                                            </apex:outputPanel>
                                        </td>
                                    </tr>
                                </table>
                        </div>
                </div>

                <!-- 2nd page -->
                <div style="page-break-after:always;"> </div>
                     <br/>
                    <br/>
                    <div style='color:black;font-size:8.0pt;font-family:"Arial",sans-serif'>
                        <div id='tableDecret'>
                            <table width="100%" style="border:2px solid #f0522f">
                                <th colspan="2">
                                    <div style="font-size:10.0pt;text-align: center;font-weight: bold;color: #253474;">
                                        Décret n°2009-649 du 9 juin 2009 relatif à l’entretien annuel des chaudières
                                        <br/>
                                        dont la puissance nominale est comprise entre 4 et 400 kW
                                    </div>
                                </th>
                                <tr>
                                    <td colspan="2">
                                        <div style="font-style: italic; padding: 10px 10px 10px 4px;color: #253474;">
                                        Chaque année, une chaudière dont la puissance nominale est comprise entre 4 et 400 kW doit obligatoirement être vérifiée, nettoyée et réglée
                                        par un professionnel qualifié. Cette obligation pèse désormais sur tous les occupants (propriétaires comme locataires) d’un local comportant
                                        une chaudière individuelle. Pour les chaudières collectives, elle pèse sur la propriétaire ou le syndicat des copropriétaires.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div style="font-size: 9.0pt; padding: 0px 10px 0px 4px">
                                        <span style="font-weight: bold;color: #253474;">L’entretien annuel comporte les opérations</span> telles que décrites dans nos Conditions Générales de Vente et conformément
                                        à la norme NF-X 50-011 :
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="50%" style="font-size: 9.0pt; padding: 0 0 0 0">
                                        <ul style="text-align: left">
                                            <li>Vérification de l’état, de la nature et de la géométrie du conduit de raccordement de l’appareil</li>
                                            <li>Démontage et nettoyage du brûleur</li>
                                            <li>Nettoyage du pré-filtre fioul domestique lorsque l’installation en est munie, sinon nettoyage du filtre de
                                                la pompe fioul domestique</li>
                                            <li>Relevé du type de gicleur</li>
                                        </ul>
                                    </td>
                                    <td width="50%" style="font-size: 9.0pt">
                                        <ul>
                                            <li>Vérification fonctionnelle des dispositifs de sécurité du brûleur</li>
                                            <li>Nettoyage du corps de chauffe</li>
                                            <li>Vérification fonctionnelle des dispositifs de sécurité de la chaudière</li>
                                            <li>Vérification fonctionnelle du circulateur de chauffage (si incorporé dans l’appareil)</li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <br/>
                    <br/>
                    <br/>
                    <div style='color:black;font-size:8.0pt;font-family:"Arial",sans-serif'>
                        <div id='tableDecret'>
                            <table width="100%" style="border:2px solid #f0522f">
                                <th colspan="2">
                                    <div style="font-size:10.0pt;text-align: center;font-weight: bold;color: #253474;">
                                        Conseils et recommandations
                                    </div>
                                </th>
                                <tr>
                                    <td>
                                        <div style="font-style: italic; padding: 10px 10px 10px 4px;color: #253474;">
                                            Les conseils et recommandations suivants sont donnés à titre indicatif et ont une valeur informative. Aucun investissement ne revêt un caractère
                                            obligatoire. Il s’agit de conseils et non de prescriptions sauf pour le cas où une teneur anormalement élevée en monoxyde de carbone est constatée.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-size: 9.0pt; padding: 0px 0px 0px 4px">
                                        <span style="font-weight: bold;color: #253474;">Bon usage de la chaudière en place :</span>
                                        <ul>
                                            <li>Baisser la température de chauffage de 1°C permet d’économiser 7% d’énergie (ADEME)</li>
                                            <li>Pour prévenir tout arrêt, surveillez la pression de votre chaudière (manomètre entre 1 et 2 bars) et changez
                                                les piles du régulateur une fois par an</li>
                                        </ul>
                                    </td> 
                                </tr>
                                <tr>
                                    <td style="font-size: 9.0pt; padding: 0px 0px 0px 4px">
                                        <span style="font-weight: bold;color: #253474;">Améliorations possibles de l’installation de chauffage :</span>
                                        <ul>
                                            <li>La pose d’un régulateur, de robinets thermostatiques sur vos radiateurs, vous apporteront confort et économies
                                                d’énergie</li>
                                            <li>Le traitement de votre installation (détartrage, désembouage …) prolongera sa durée de vie</li>
                                        </ul>
                                    </td> 
                                </tr>
                                <tr>
                                    <td style="font-size: 9.0pt; padding: 0px 0px 0px 4px">
                                        <span style="font-weight: bold;color: #253474;">Intérêt éventuel du remplacement de l’installation de chauffage :</span>
                                        <ul>
                                            <li>Nos techniciens sont des professionnels capables de guider votre choix pour définir la solution chauffage /
                                                    eau chaude la mieux adaptée à vos besoins</li>
                                        </ul>
                                    </td> 
                                </tr>
                            </table>
                        </div>
                    </div>

                    <br/>
                    <br/>
                    <br/>
                    <div style='color:black;font-size:8.0pt;font-family:"Arial",sans-serif'>
                        <div id='tableDecret'>
                            <table width="100%" style="border:2px solid #f0522f">
                                <th colspan="2">
                                    <div style="font-size:10.0pt;text-align: center;font-weight: bold;color: #253474;">
                                        Notre garantie commerciale
                                    </div>
                                </th>
                                <tr>
                                    <td style="font-size: 9.0pt; padding: 6px 0px 0px 6px">
                                        <span>Au-delà des garanties légales dont vous disposez :</span>
                                        <ul>
                                            <li style="padding: 0 0 10px 0">Nos pièces de rechange sont garanties 6 mois, sous réserve d’avoir été installées par nos soins et utilisées
                                                    dans des conditions normales. Dans tous les cas, la garantie est limitée au remplacement de la pièce
                                                    défectueuse et ne pourra donc faire l’objet d’aucune autre forme d’indemnisation</li>
                                            <li>Toutes nos interventions bénéficient d’une garantie de 3 mois pour la main-d’oeuvre et les déplacements,
                                                    en cas de renouvellement de la défectuosité constatée à la précédente intervention et sous réserve qu’il n’y ait
                                                    pas eu intervention extérieure ou mauvaise utilisation de l’équipement</li>
                                        </ul>
                                    </td> 
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br/>
                    <apex:image id="footer" url="{!$Resource.chamlogo}" width="100px" />
                    <br/>
                    <apex:image url="{!$Resource.VFP06_Footer}" width="45%" />
                <div class='break2' />

        </body>

    </html>

</apex:page>