<!--
  @description       : 
  @author            : AMO
  @group             : 
  @last modified on  : 28-04-2021
  @last modified by  : ZJO
  Modifications Log 
  Ver   Date         Author   Modification
  1.0   08-09-2020   AMO   Initial Version
-->
<apex:page applyHtmlTag="false" showHeader="false" standardStylesheets="false" controller="VFC72_ServiceContract" renderAs="pdf"
    docType="html-5.0">

    <html>

    <head>
        <style>
            @media print {
                html,
                body {
                    border: 1px solid white;
                    page-break-after: avoid;
                    page-break-before: avoid;
                    font-family: Arial, Helvetica, sans-serif;
                }
            }

            @page {
                size: A4;
                margin-top: 1.0cm;
                margin-bottom: 2.50cm;
                margin-left: 1.1cm;
                margin-right: 1.0cm;
            }

            @page {
                @top-left {
                    content: element(header);
                }
            }

            @page {
                @bottom-right {
                    content: element(footer);
                }
            }

            div.footer {
                display: block;
                position: running(footer);
                margin-bottom: 1.5cm;
            }

            .pagenumber:before {
                content: counter(page);
            }

            .pagecount:before {
                content: counter(pages);
            }

        </style>
    </head>

    <apex:variable var="index" value="{!1}" />

    <apex:repeat value="{!lstServiceContract}" var="actualServCon">

        <body class="body">

            <div class="footer">
                <c:C00_HSRFooter st="{!actualServCon.Agency__r}" />
            </div>

            <div style="position: fixed;bottom: 0px;right: 30px;font-family: Arial,Helvetica, sans-serif;font-size:10px;">
                PAGE
                <span class="pagenumber" />/<span class="pagecount" />
            </div>

            <table id="Logos" style="margin-left:15px; height: 55px; font-size:15px;font-family: Arial, Helvetica, sans-serif; ">
                <tr style=" width:50%">
                    <th style="">
                        <!-- <img src="{!$Resource.logoSBF}" alt="meaningful text"  /> -->
                        <!-- <apex:image style="margin-top: 5px;" value="{!URLFOR($Resource.logoSBF)}" rendered="{!actualServCon.Agency__r.Name == 'SBF Energies'}"/> -->
                        <span class="slds-avatar slds-avatar--small">
                            <apex:variable var="imageVar" value="{!actualServCon.Agency__r.Logo_Name_Agency__c}" />
                            <apex:outputPanel rendered="{!actualServCon.Agency__r.Name != 'VB Gaz'}">
                                <apex:image style="width:269px;height:102px;" value="{!URLFOR($Resource.LogosHSV, 'logoZip/' + imageVar + '.png' )}" rendered="{!NOT(ISBLANK(actualServCon.Agency__r.Logo_Name_Agency__c))}"
                                />
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!actualServCon.Agency__r.Name == 'VB Gaz'}">
                                <apex:image style="width:169px;height:102px;" value="{!URLFOR($Resource.LogosHSV, 'logoZip/' + imageVar + '.png' )}" rendered="{!NOT(ISBLANK(actualServCon.Agency__r.Logo_Name_Agency__c))}"
                                />
                            </apex:outputPanel>

                            <!-- If no logo, holding the space for page layout-->
                            <apex:outputPanel rendered="{!ISBLANK(actualServCon.Agency__r.Name)}">
                                <div style="height: 100px; width: 300px; background-color: transparent">&nbsp;</div>
                            </apex:outputPanel>
                        </span>
                    </th>
                </tr>
                <div style="margin-top:-65px;">
                    <p style="font-size:15px;font-family: Arial, Helvetica, sans-serif; margin-left: 450px; width: 130px; border:1px solid black;border-width:2px;padding:4px;">Une question ?
                        <br/> Appelez-nous au
                        <br/>{! actualServCon.Agency__r.Phone__c }</p>
                </div>
                <div style="margin-left:50px;margin-top:55px;">
                    <apex:image style="width:30px; height:30px;" value="{!URLFOR($Resource.telLogoHSV)}" />
                </div>
                <div style="margin-left:100px; width:250px; margin-top:-35px;">
                    Service client : {! actualServCon.Agency__r.Phone__c }
                    <br/>{! actualServCon.Agency__r.Libelle_horaires__c }
                </div>
            </table>

            <!-- <div style="Position:absolute;top:1.7in; left:4.42in; line-height:0.26in; display: inline-block; align: right;font-weight: 600;background-color:lightgray;font-size:20px;font-family: Arial;"> -->
            <div style="margin-left:425px;Position:static;;top:1.7in; left:4.42in; width:355.3px; display: inline-block; align: right;background-color:lightgray;font-size:15px;font-family: Arial, Helvetica, sans-serif;">
                <div style="padding-top: 25px; padding-left: 10px">
                    <!--{!actualServCon.Payeur_du_contrat__r.salutation} {!actualServCon.Payeur_du_contrat__r.firstname} {!actualServCon.Payeur_du_contrat__r.lastname}-->

                    {!IF(actualServCon.Payeur_du_contrat__r.IsPersonAccount, actualServCon.Payeur_du_contrat__r.Salutation + ' ' + actualServCon.Payeur_du_contrat__r.FirstName
                    + ' ' + actualServCon.Payeur_du_contrat__r.LastName, actualServCon.Payeur_du_contrat__r.Raison_sociale__c)}

                    <br/> {!actualServCon.Payeur_du_contrat__r.BillingStreet} {!IF(actualServCon.Payeur_du_contrat__r.Adress_Complement__c!=null, ' - ' + actualServCon.Payeur_du_contrat__r.Adress_complement__c , null)}
                    <div style="text-align:left;">
                        <span style="{!IF(ISBLANK(actualServCon.Payeur_du_contrat__r.Imm_Res__c), 'display: none', 'display: inline')}">{!'Bât.: '+actualServCon.Payeur_du_contrat__r.Imm_Res__c}</span>
                        <span style="{!IF(OR(ISBLANK(actualServCon.Payeur_du_contrat__r.Imm_Res__c), ISBLANK(actualServCon.Payeur_du_contrat__r.Door__c)), 'display: none', 'display: inline')}">{!' '}</span>
                        <span style="{!IF(ISBLANK(actualServCon.Payeur_du_contrat__r.Floor__c), 'display: none', 'display: inline')}">{!'Etage: '+actualServCon.Payeur_du_contrat__r.Floor__c}</span>
                        <span style="{!IF(ISBLANK(actualServCon.Payeur_du_contrat__r.Door__c), 'display: none', 'display: inline')}">{!'Porte: '+actualServCon.Payeur_du_contrat__r.Door__c}</span>
                    </div>
                    {!actualServCon.Payeur_du_contrat__r.BillingPostalCode} {!actualServCon.Payeur_du_contrat__r.BillingCity}
                    
                    <!--<br/> {!IF(actualServCon.Payeur_du_contrat__r.IsPersonAccount, null, 'Représentée par ' + actualServCon.Contact.Account.Salutation
                    + ' ' + actualServCon.Contact.Account.FirstName + ' ' + actualServCon.Contact.Account.LastName)}-->

                </div>
            </div>
            <!-- </div> -->
            <div style='color:black;font-size:15px;font-family: Arial, Helvetica, sans-serif; margin-top: 125px; margin-left:15px;'>
                <div id='tableHeaderInfo' style="">
                    <table width="100%" style="border:none;line-height:11px;">
                        <tr>
                            <td style="font-weight:bold;"> Renouvellement de votre contrat</td>
                        </tr>
                        <tr>
                            <td>Client N° : {!actualServCon.Payeur_du_contrat__r.ClientNumber__c}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Contrat N° : {!actualServCon.Numero_du_contrat__c}</td>
                            <td></td>
                        </tr>
                        <br/>
                        <!-- <tr>
                                <td></td>
                                <td>{!actualServCon.TECH_Date_d_edition__c}</td>
                            </tr> -->
                    </table>
                </div>
            </div>
            <div style='color:black;font-size:15px;font-family: Arial, Helvetica, sans-serif; margin-top:5px; text-align: right;'>
                <apex:outputText value=" {0,date, dd/MM/yyyy}">
                    Le
                    <apex:param value="{!dateDuJr}" />
                </apex:outputText>
            </div>
            <br/>
            <div style="text-align: center; ">
                Votre contrat d’entretien garantit la sécurité de votre installation
                <br/>et vous offre une grande tranquillité d’esprit.
            </div>
            <br/>
            <br/>
            <div style="margin-left:15px;">
                {!actualServCon.Payeur_du_contrat__r.salutation} {!actualServCon.Payeur_du_contrat__r.lastname},
                <br/>
                <br/> Vous bénéficiez au quotidien des avantages que vous procure votre contrat d’entretien et nous vous remercions
                de votre confiance. Nous vous informons de la prochaine reconduction de votre {!actualConLineItem.tech_nom_produit__c}
                couvrant votre {!actualServCon.Asset__r.Equipment_type_auto__c} pour la période du
                <apex:outputText value=" {0,date, dd/MM/yyyy}">
                    <apex:param value="{!actualServCon.StartDate}" />
                </apex:outputText> au
                <apex:outputText value=" {0,date, dd/MM/yyyy}">
                    <apex:param value="{!actualServCon.EndDate}" />
                </apex:outputText>.
                <br/>
                <br/> Nous vous invitons à régler dès aujourd’hui le montant de votre contrat de {!ScGrandTotal}€
                TTC afin de profiter de nos services sans interruption.
            </div>
            <br/>
            <div style="margin-left:15px;">
                Continuez à bénéficier de votre contrat d’entretien qui comprend :
                <br/>
                <section id="textarea" contenteditable="true">
                    <ul>
                        <li> Programmation automatique de la (des) visite(s) d'entretien périodique(s)</li>
                        <li> Prise en charge de la (des) visite(s) d'entretien périodique(s) (main-d'oeuvre et déplacement
                            <br/> inclus)</li>
                        <li> Délai d'intervention et tarif garantis pour les dépannages</li>
                        <li>
                            <apex:outputText value="Prise en charge des déplacements et de la main-d’oeuvre pour les dépannages" rendered="{!actualConLineItem.Product2.Type_de_garantie__c = 'Avantage'}"
                            />
                        </li>
                        <li>
                            <apex:outputText value="Remise de 10% sur le prix des pièces dans le cadre des dépannages" rendered="{!actualConLineItem.Product2.Type_de_garantie__c = 'Avantage'}"
                            />
                        </li>
                    </ul>
                </section>
                <!-- <div style="position: absolute; margin-top: -20px; margin-left: 15px; font-size:12px;font-family: Arial;">
                        <c:C00_HSRFooter st="{!actualServCon.Agency__r}" /> 
                    </div> -->
            </div>

            <div style="page-break-after:always;"></div>

            <div style="margin-top: -5px; margin-left:15px;">
                Le saviez-vous ? Pour plus de souplesse, il est maintenant possible de régler la facture de votre
                contrat directement sur l’espace de paiement sécurisé.
                <br/> ----------------------------------------------------------------------------------------------------------------------------
                <br/> Il vous suffit de vous connecter à {!actualServCon.agency__r.Site_web__c}/paiement avec les informations suivantes :
                <br/> N° de dossier : {!actualServCon.Numero_du_contrat__c}
                <br/> Nom associé au dossier : {!actualServCon.Payeur_du_contrat__r.lastname}
                <br/> ----------------------------------------------------------------------------------------------------------------------------
                <br/> Si vous le préférez, vous pouvez toujours nous adresser votre règlement par chèque en précisant votre
                numéro de client {!actualServCon.Payeur_du_contrat__r.ClientNumber__c}. Renseignez-vous au {!actualServCon.Agency__r.Phone__c}.
                <br/>
                <br/> Dans le cas particulier où l’entretien de votre {!actualServCon.Asset__r.Equipment_type_auto__c} n’a pas
                encore été réalisé au cours de l’année de contrat qui s’achève, veuillez prendre contact avec nous pour organiser
                un rendez-vous au plus vite.
                <br/>
                <br/> Nous vous prions d’agréer, {!actualServCon.Payeur_du_contrat__r.salutation} {!actualServCon.Payeur_du_contrat__r.lastname},
                l’expression de nos sincères salutations.
                <br/>
                <br/>
            </div>
            <br/>
            <div style="text-align: right;">
                Le Service Client
            </div>
            <br/>
            <br/>

            <div style='font-size:15px;font-family: Arial, Helvetica, sans-serif;'>
                <table width="98%" style="line-height:25px; padding-right: 15px; padding-left: 15px;border-collapse: collapse;">
                    <tr>
                        <th align="center" style="word-wrap: break-word;width:3%;border:1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;">
                            Contrat concerné
                        </th>
                        <th align="center" style="word-wrap: break-word;width:5%;border:1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;">
                            Appareil concerné
                        </th>
                        <th align="center" style="word-wrap: break-word;width:3%;border:1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;">
                            Période du contrat
                        </th>
                        <th align="center" style="word-wrap: break-word;width:3%;border:1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;">
                            Tarif HT
                        </th>
                        <th align="center" style="word-wrap: break-word;width:3%;border:1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;">
                            TVA
                        </th>
                        <th align="center" style="word-wrap: break-word;width:3%;border:1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;">
                            Tarif TTC
                        </th>
                    </tr>
                    <tr align="center" style="page-break-inside: avoid;">
                        <td style="word-wrap: break-word;border: none;
                                    border-right: 1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;
                                    border-left: 1px solid black;
                                    border-bottom: 1px solid black" valign="top">
                            {!actualConLineItem.tech_nom_produit__c}
                        </td>

                        <td align="center" style="word-wrap: break-word;border: none;
                                    border-right: 1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;
                                    border-left: 1px solid black;
                                    border-bottom: 1px solid black" valign="top">
                            {!actualServCon.Asset__r.Equipment_type_auto__c}
                        </td>

                        <td align="center" style="word-wrap: break-word;border: none; font-size:15px;font-family: Arial, Helvetica, sans-serif;
                                    border-right: 1px solid black;
                                    border-left: 1px solid black;
                                    border-bottom: 1px solid black" valign="top">
                            Du
                            <apex:outputText value=" {0,date, dd/MM/yyyy}">
                                <apex:param value="{!actualServCon.StartDate}" />
                            </apex:outputText> au
                            <apex:outputText value=" {0,date, dd/MM/yyyy}">
                                <apex:param value="{!actualServCon.EndDate}" />
                            </apex:outputText>
                        </td>

                        <td align="center" style="word-wrap: break-word;border: none;
                                    border-right: 1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;
                                    border-left: 1px solid black;
                                    border-bottom: 1px solid black" valign="top">
                            {!ScTotalPrice}€
                        </td>

                        <td align="center" style="word-wrap: break-word;border: none;
                                    border-right: 1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;
                                    border-left: 1px solid black;
                                    border-bottom: 1px solid black" valign="top">
                            {!actualServCon.Tax__c}%
                        </td>

                        <td align="center" style="word-wrap: break-word;border: none;
                                    border-right: 1px solid black; font-size:15px;font-family: Arial, Helvetica, sans-serif;
                                    border-left: 1px solid black;
                                    border-bottom: 1px solid black" valign="top">
                            {!ScGrandTotal}€
                        </td>
                    </tr>
                </table>
            </div>
            <p style="font-size:15px; margin-top:0px; font-style:italic; font-family: Arial, Helvetica, sans-serif;">
                TVA en vigueur au moment de la facturation
            </p>
            <!--
            <div style="margin-left: 15px; font-size:15px;font-family: Arial;margin-top: 20px;">
                <apex:outputPanel rendered="{!lstCLIOptionSize}">
                    Options:
                    <apex:repeat var="cliOption" value="{!lstCLIOption}">
                        <li>{!cliOption.Product2.Name }</li>
                    </apex:repeat>
                </apex:outputPanel>                
            </div>
            -->
        </body>

        <div style="page-break-after:{!IF(index == lstServiceContract.size, 'avoid', 'always')}"></div>
        <apex:variable value="{!index + 1}" var="index" />

    </apex:repeat>



    </html>

</apex:page>