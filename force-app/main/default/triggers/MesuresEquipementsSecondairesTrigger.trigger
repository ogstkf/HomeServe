/**
 * @File Name          : MesuresEquipementsSecondairesTrigger.trigger
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 03-25-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * ---    -----------       -------           ------------------------ 
 * 1.0    24/03/2021         LGO              Initial Version
 * ---    -----------       -------           ------------------------  
**/
trigger MesuresEquipementsSecondairesTrigger on Mesures_quipements_secondaires__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
   MesuresEquipementSecondTriggerHandler handler = new MesuresEquipementSecondTriggerHandler();

   if(Trigger.isAfter && Trigger.isInsert) {
       handler.handleAfterInsert(Trigger.new);
    }

    else if(Trigger.isAfter && Trigger.isUpdate) {
       handler.handleAfterUpdate(Trigger.new, Trigger.old);
    }   
}