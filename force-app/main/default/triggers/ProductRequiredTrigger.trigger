trigger ProductRequiredTrigger on ProductRequired (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
        
    ProductRequiredTriggerHandler handler = new ProductRequiredTriggerHandler();

    if(Trigger.isAfter && Trigger.isUpdate){
        System.debug('## Trigger Before update start');
        handler.handleAfterUpdate(Trigger.old, Trigger.new);

    }
}