trigger AssignedResourceTrigger on AssignedResource (after insert, before delete, after update, before update, after delete) {
    System.debug('## AssignedResourceTrigger Start');

    AssignedResourceTriggerHandler handler = new AssignedResourceTriggerHandler();

    if(Trigger.isAfter && Trigger.isInsert){
        System.debug('##AssignedResourceTrigger After insert start');

        handler.handleAfterInsert(Trigger.new);

        System.debug('##AssignedResourceTrigger After insert end'); 
    }

    if(Trigger.isBefore && Trigger.isDelete){
        System.debug('##AssignedResourceBeforeDelete');
        handler.handleBeforeDelete(Trigger.old);
        System.debug('##AssignedResourceBeforeDelete');
    }
    System.debug('## AssignedResourceTrigger END');

    if(Trigger.isBefore && Trigger.isUpdate){
        System.debug('AssignedResource after update start');
        handler.handleAfterUpdate(Trigger.old, Trigger.new);
        System.debug('AssignedResource after update end');
    }

    if(Trigger.isAfter && Trigger.isDelete){
        handler.handleAfterDelete(Trigger.old);
    }
}