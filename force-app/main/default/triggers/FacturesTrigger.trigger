/**
 * @File Name          : FacturesTrigger.trigger
 * @Description        : 
 * @Author             : SSA
 * @Group              : 
 * @Last Modified By   : SSA
 * @Last Modified On   : 15/03/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    15/03/2020            SSA                Initial Version
**/

trigger FacturesTrigger on sofactoapp__Factures_Client__c (before insert, before delete,  after delete) {
    FacturesTriggerHandler handler = new FacturesTriggerHandler();
    
    if (trigger.isbefore && trigger.isInsert)
    {
      handler.handlebeforeinsert(trigger.new);
    }
    
    if (trigger.isbefore && trigger.isdelete) 
    {
      handler.handlebeforedelete(trigger.old);
    }
    
    
   	if (trigger.isAfter && trigger.isdelete)
    {
        handler.handleafterdelete(trigger.old);
    }

}