/**
 * @File Name          : OrderItemTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 27/11/2019, 09:56:42
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/11/2019   AMO     Initial Version
**/
trigger OrderItemTrigger on OrderItem (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    OrderItemTriggerHandler handler = new OrderItemTriggerHandler();

    if(Trigger.isBefore && Trigger.isUpdate){
        System.debug('## Trigger Before update start');
        handler.handleBeforeUpdate(Trigger.new, Trigger.old);
        System.debug('## Trigger Before update end');
    }

    if(Trigger.isBefore && Trigger.isInsert){
        System.debug('## Trigger Before insert start');
        handler.handleBeforeInsert(Trigger.new);
        System.debug('## Trigger Before insert end');
    }  

    if(Trigger.isAfter && Trigger.isInsert){
        handler.handleAfterInsert(Trigger.new);
    }
    if(Trigger.isBefore && Trigger.isDelete){
        System.debug('## Trigger Before delete start');
        handler.handleBeforeDelete(Trigger.Old);
        System.debug('## Trigger Before delete end');
    }

    if(Trigger.isAfter && Trigger.isUpdate){
        System.debug('## Trigger After update start');
        handler.handleAfterUpdate(Trigger.old, Trigger.new);
        System.debug('## Trigger After Update end');
    }   

    if(Trigger.isAfter && Trigger.isDelete){
        handler.handleAfterDelete(Trigger.old);
    }   
}