/**
 * @File Name          : WorkOrderTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 28-10-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/10/2019   AMO     Initial Version
**/
trigger WorkOrderTrigger on WorkOrder (before insert, after insert, before update, after update, before delete ,after delete) {
    TriggerLuncher tl = new TriggerLuncher('WorkOrder');
    tl.execute();

    WorkOrderTriggerHandler handler = new WorkOrderTriggerHandler();
    
    if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.New);
    }

    if(Trigger.isAfter && Trigger.isUpdate){
        handler.handleAfterUpdate(Trigger.old, Trigger.new);
    }
    
    if(trigger.isBefore && trigger.isInsert){
        handler.handlebeforeInsert(trigger.New);
    }
    
}