trigger ProductTransferTrigger on ProductTransfer (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    ProductTransferTriggerHandler handler = new ProductTransferTriggerHandler();

    if(Trigger.isBefore && Trigger.isUpdate){
        System.debug('## ProductTransfer before update start');
        handler.handleBeforeUpdate(Trigger.old, Trigger.new);
        System.debug('## ProductTransfer before update end');
    }

    if(Trigger.isAfter && Trigger.isUpdate){
        System.debug('## Starting after update: ');
        handler.handleAfterUpdate(Trigger.old, Trigger.new);
        System.debug('## Ending after update: ');
    }

    if(Trigger.isBefore && Trigger.isInsert){
        System.debug('## starting before Insert');
         
        handler.handleBeforeInsert(Trigger.new);
        System.debug('## ending before insert');
    }
}