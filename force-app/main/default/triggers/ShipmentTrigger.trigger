trigger ShipmentTrigger on Shipment (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    ShipmentTriggerHandler handler = new ShipmentTriggerHandler();

    if(Trigger.isBefore && Trigger.isUpdate){
        handler.handleBeforeUpdate(Trigger.old, Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isUpdate){
        handler.handleAfterUpdate(Trigger.old, Trigger.new);
    }

   

}