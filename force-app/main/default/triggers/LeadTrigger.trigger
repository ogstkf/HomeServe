trigger LeadTrigger on Lead (after update, after insert) {

	LeadTriggerHandler handler = new LeadTriggerHandler();

	if(trigger.isAfter && trigger.isUpdate){
		handler.handleAfterUpdate(trigger.old, trigger.new);
	}

	if(trigger.isAfter && trigger.isInsert){
		handler.handleAfterInsert(trigger.new);
	}
}