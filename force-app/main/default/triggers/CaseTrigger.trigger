/**
 * @File Name          : CaseTrigger.trigger
 * @Description        : 
 * @Author             : 
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 28/10/2019, 14:06:36
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    01/10/2019   ANA     Initial Version
**/
trigger CaseTrigger on Case (before insert, after insert, before update, after update, before delete ,after delete) {
    TriggerLuncher tl = new TriggerLuncher('Case');
    tl.execute();
    CaseTriggerHandler handler = new CaseTriggerHandler();

    if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }

    if(trigger.isBefore && trigger.isUpdate){
        handler.handleBeforeUpdate(trigger.old, trigger.new);
    }

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
    }
}