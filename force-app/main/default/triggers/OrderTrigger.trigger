/**
 * @File Name          : OrderTrigger.trigger
 * @Description        : 
 * @Author             : xxx
 * @Group              : 
 * @Last Modified By   : ANR
 * @Last Modified On   : 01-19-2021
 * @Modification Log   : 
 * Ver       Date         Author      		Modification
 * 1.0    xx/xx/2019       xxx           Initial Version
 * 1.1    04/11/2019       SHU           CT-1087 : Mise a jour PRLI
**/
trigger OrderTrigger on Order (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    OrderTriggerHandler handler = new OrderTriggerHandler();

    if(Trigger.isBefore && Trigger.isUpdate){
        System.debug('## Trigger Before update start');
        handler.handleBeforeUpdate(Trigger.new, Trigger.old);
        System.debug('## Trigger Before update end');
    }
    else if(Trigger.isAfter && Trigger.isInsert){
        System.debug('## Trigger After insert start');
        handler.handleAfterInsert(Trigger.new);
        System.debug('## Trigger After insert end');
    }  
    else if(Trigger.isAfter && Trigger.isUpdate){
        System.debug('## Trigger After update start');
        handler.handleAfterUpdate(Trigger.new, Trigger.old);
        System.debug('## Trigger After update end');
    }  
    else if(Trigger.isBefore && Trigger.isInsert){
        System.debug('## Trigger Before insert start');
        handler.handleBeforeInsert(Trigger.new);
        System.debug('## Trigger Before insert end');
    }  
}