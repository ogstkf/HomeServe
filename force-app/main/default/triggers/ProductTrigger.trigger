/**
 * @File Name          : ProductTrigger.trigger
 * @Description        : 
 * @Author             : DMG
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 11-05-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    05/12/2019         DMG                    Initial Version
**/
trigger ProductTrigger on Product2 (after insert, after update, before insert, before update) {
    ProductTriggerHandler handler = new ProductTriggerHandler();
    //ZJO 05.11.2020 - added before insert 
    if(trigger.isBefore && trigger.isInsert){
        handler.handleBeforeInsert(trigger.new);
    }

    if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }

    if(trigger.isBefore && trigger.isUpdate){
        handler.handleBeforeUpdate(trigger.old, trigger.new);
    }

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
    }    
}