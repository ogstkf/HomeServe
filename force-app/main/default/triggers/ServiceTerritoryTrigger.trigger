/**
 * @File Name          : ServiceTerritoryTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 11/03/2020, 11:50:12
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/03/2020   AMO     Initial Version
**/
trigger ServiceTerritoryTrigger on ServiceTerritory (after update) {

    ServiceTerritoryTriggerHandler handler = new ServiceTerritoryTriggerHandler();

    if(Trigger.isAfter && Trigger.isUpdate){
		handler.handleAfterUpdate(Trigger.old, Trigger.new);
	}
}