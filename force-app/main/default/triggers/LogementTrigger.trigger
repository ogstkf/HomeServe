/**
 * @File Name          : LogementTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : SH
 * @Last Modified On   : 27/12/2019, 11:33:05
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/10/2019   		  AMO     				Initial Version
** 1.1    27/12/2019   		  SH     				Added before Update
**/
trigger LogementTrigger on Logement__c (before insert, after insert, before update, after update) {

	System.Debug('## >>> Start of LogementAfterInsert <<< run by ' + UserInfo.getName());
	
	Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());

	LogementTriggerHandler handler = new LogementTriggerHandler();

	if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('LogementTriggerHandler_createAccount')){

		if(Trigger.isAfter && Trigger.isInsert){
			handler.createAccount(Trigger.new);
		}
	}	

	if(Trigger.isBefore && Trigger.isInsert){
		handler.handleBeforeInsert(Trigger.new);
	}		

	if(Trigger.isAfter && Trigger.isInsert){
		handler.handleAfterInsert(Trigger.new);
	}

	if(Trigger.isBefore && Trigger.isUpdate){
		handler.handleBeforeUpdate(Trigger.old, Trigger.new);
	}

	if(Trigger.isAfter && Trigger.isUpdate){
		handler.handleAfterUpdate(Trigger.old, Trigger.new);
	}
	
}