/**
 * @File Name          : QuoteSyncTrigger.trigger
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/3/2020, 3:22:10 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/3/2020   RRJ     Initial Version
**/
trigger QuoteSyncTrigger on Quote (after insert, after update) {
    
    if (TriggerStopper.stopQuote) return;
        
    TriggerStopper.stopQuote = true;    

    private static String rtcRecTypeId = AP_Constant.getRecTypeId('Quote', 'Devis_RTC');
    Set<String> quoteFields = QuoteSyncUtil.getQuoteFields();
    List<String> oppFields = QuoteSyncUtil.getOppFields();
    
    String quote_fields = QuoteSyncUtil.getQuoteFieldsString();
    
    String opp_fields = QuoteSyncUtil.getOppFieldsString();

    Map<Id, Id> startSyncQuoteMap = new Map<Id, Id>();
    List<String> quoteIds = new List<String>();
    for (Quote quote : trigger.new) {
        if(quote.RecordTypeId == rtcRecTypeId){
            if (quote.isSyncing && !trigger.oldMap.get(quote.Id).isSyncing) {
                startSyncQuoteMap.put(quote.Id, quote.OpportunityId);
            }
            quoteIds.add(quote.Id);
        }
    }
    
    List<String> oppIds = new List<String>();
    Map<Id, Quote> quoteMap = new Map<Id, Quote>();
    if(quoteIds.size()>0){
        String quoteQuery = 'select Id, OpportunityId, isSyncing' + quote_fields + ' from Quote where Id in :quoteIds AND RecordType.DeveloperName = \'Devis_RTC\' ';
        System.debug(quoteQuery);     
    
        List<Quote> quotes = Database.query(quoteQuery);
        
        
        for (Quote quote : quotes) {
            if (trigger.isInsert || (trigger.isUpdate && quote.isSyncing)) {
                quoteMap.put(quote.OpportunityId, quote);
                oppIds.add(quote.opportunityId);           
            }
        }
    }
    
    if (oppIds.size()>0) {
        String oppQuery = 'select Id, HasOpportunityLineItem' + opp_fields + ' from Opportunity where Id in :oppIds AND RecordType.DeveloperName = \'Opportunite_RTC\'';
        //System.debug(oppQuery);     
    
        List<Opportunity> opps = Database.query(oppQuery);
        List<Opportunity> updateOpps = new List<Opportunity>();
        List<Quote> updateQuotes = new List<Quote>();        
        
        for (Opportunity opp : opps) {
            Quote quote = quoteMap.get(opp.Id);
            
            // store the new quote Id if corresponding opportunity has line items
            if (trigger.isInsert && opp.HasOpportunityLineItem) {
                QuoteSyncUtil.addNewQuoteId(quote.Id);
            }
            
            boolean hasChange = false;
            for (String quoteField : quoteFields) {
                String oppField = QuoteSyncUtil.getQuoteFieldMapTo(quoteField);
                Object oppValue = opp.get(oppField);
                Object quoteValue = quote.get(quoteField);
                if (oppValue != quoteValue) {                   
                    if (trigger.isInsert && (quoteValue == null || (quoteValue instanceof Boolean && !Boolean.valueOf(quoteValue)))) {
                        quote.put(quoteField, oppValue);
                        hasChange = true;                          
                    } else if (trigger.isUpdate) {
                        if (quoteValue == null) opp.put(oppField, null);
                        else opp.put(oppField, quoteValue);
                        hasChange = true;                          
                    }                    
                }                     
            }    
            if (hasChange) {
                if (trigger.isInsert) { 
                    updateQuotes.add(quote);
                } else if (trigger.isUpdate) {
                    updateOpps.add(opp);                
                }               
            }                                  
        } 
   
        if (trigger.isInsert) {
            Database.update(updateQuotes);
        } else if (trigger.isUpdate) {
            TriggerStopper.stopOpp = true;            
            Database.update(updateOpps);
            TriggerStopper.stopOpp = false;              
        }    
    }
       
    TriggerStopper.stopQuote = false; 
}