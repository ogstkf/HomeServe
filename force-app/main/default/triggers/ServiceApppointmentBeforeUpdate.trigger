trigger ServiceApppointmentBeforeUpdate on ServiceAppointment (before update) {

    AP07_CalculAppointmentHour.setAppointmentHourUpdate(Trigger.new, Trigger.oldMap);
}