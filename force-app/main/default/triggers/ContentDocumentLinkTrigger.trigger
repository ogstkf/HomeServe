/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : trigger sur objet ContentDocumentLink
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 28-JAN-2018  YGO    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  

trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {
	System.debug('### trigger');

	ContentDocumentLinkTriggerHandler handler = new ContentDocumentLinkTriggerHandler();
	
    if(Trigger.isAfter && Trigger.isInsert){
    	System.debug('### after insert');
        handler.handleAfterInsert(Trigger.new);
    }
    
}