trigger MaintenanceAssetTrigger on MaintenanceAsset (before update, after update, before Insert, after Insert) {
	MaintenanceAssetTriggerHandler handler = new MaintenanceAssetTriggerHandler();
    
    if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }
}