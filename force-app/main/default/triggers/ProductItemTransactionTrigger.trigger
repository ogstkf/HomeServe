trigger ProductItemTransactionTrigger on ProductItemTransaction (before insert, after insert) {
        
    ProductItemTransactionTriggerHandler handler = new ProductItemTransactionTriggerHandler();

    if(Trigger.isBefore && Trigger.isInsert){
        handler.handleBeforeInsert(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isInsert){
        handler.handleAfterInsert(Trigger.new);
    }
}