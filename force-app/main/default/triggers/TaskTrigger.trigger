/**
 * @File Name          : TaskTrigger
 * @Description        : trigger
 * @Author             : DMU
 * @Group              : Spoon Consulting
 * @Last Modified By   : DMU
 * @Last Modified On   : 21/08/2019, 14:00:00 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    21/08/2019, 14:00:00 PM        DMU                      Initial Version
  **/

trigger TaskTrigger on Task (after insert, after update) {
    TaskTriggerHandler handler = new TaskTriggerHandler();

    if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
    }

}