/**
 * @File Name          		: MobileNotificationsTrigger.cls
 * @Description				  : Trigger for Mobile_notifications
 * @Author					  	:  KRO 
 * @Group              			: Spoon Consulting
 * @Last Modified By   	: KRO
 * @Last Modified On   	: 11/11/2019
 * @Modification Log   	: 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    11/11/2019          		KRO                 Initial Version
 **/
trigger MobileNotificationsTrigger on Mobile_Notifications__c (after update) {
    
    MobileNotificationTriggerHandler handler = new MobileNotificationTriggerHandler();
    
    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
    }
    
}