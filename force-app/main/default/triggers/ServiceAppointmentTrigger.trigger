/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-25-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-25-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
trigger ServiceAppointmentTrigger on ServiceAppointment (after insert, after update, before insert, before update) {
    /*ServiceAppointmentTriggerHandler handler = new ServiceAppointmentTriggerHandler();

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
    }
    Deactivated by Ravi NADARADJANE on 2019-06-26 - Due to issue on FSL optmization process 
    TriggerLuncher tl = new TriggerLuncher('ServiceAppointment');
    tl.execute();
    FSL_ServiceAppointmentHandler fslHndler = new FSL_ServiceAppointmentHandler();
    ServiceAppointmentTriggerHandler handler = new ServiceAppointmentTriggerHandler();

    if(trigger.isBefore && trigger.isInsert){
        fslHndler.onBeforeInsert(trigger.new);
    }

    if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
    }*/
        
    //SRA 16/07/2019
    ServiceAppointmentTriggerHandler handler = new ServiceAppointmentTriggerHandler();

    if(trigger.isAfter && trigger.isUpdate){
       handler.handleAfterUpdate(trigger.old, trigger.new);
    }

    
    if(trigger.isBefore && trigger.isUpdate){
        handler.handleBeforeUpdate(trigger.old, trigger.new);
    }

    if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }

    if(trigger.isBefore && trigger.isInsert){
        handler.handleBeforeInsert(trigger.new);
    }

}