/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 08-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-03-2022   MNA   Initial Version
**/
trigger SauvegardeIBAN on sofactoapp__Coordonnees_bancaires__c (before insert,before update, after insert, after update) {
    
   if (trigger.isBefore){
       if(trigger.isInsert || trigger.isUpdate)
       {
        CoordonneesBancairesHandler.IbanMethod (trigger.new);
       }
    }
    else if (trigger.isAfter && trigger.isUpdate) {
    	CoordonneesBancairesHandler.afterUpdate(trigger.new, trigger.old);    
    }
}