/**
 * @File Name          : ServiceContractTrigger.trigger
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 20-12-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    14/11/2019   AMO     Initial Version
**/
trigger ServiceContractTrigger on ServiceContract (before update, after update, before Insert, after Insert) {

    ServiceContractTriggerHandler handler = new ServiceContractTriggerHandler();

    if(trigger.isBefore && trigger.isUpdate){
        handler.handlebeforeupdate(trigger.old, trigger.new);
    }    

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleafterupdate(trigger.old, trigger.new);
    } 

    if(trigger.isBefore && trigger.isInsert){
        handler.handleBeforeInsert(trigger.new);        
    }

    //TEC 125 - Maintenace Plan
    if(trigger.isAfter && trigger.isInsert){
        system.debug('*** in trigger aft ins');
        handler.handleAfterInsert(trigger.new);
    }
    System.debug('Cpu time SC : '+Limits.getCpuTime());
}