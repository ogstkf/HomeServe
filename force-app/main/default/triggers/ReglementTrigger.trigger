/**
 * @File Name          : ReglementTrigger.trigger
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 31-08-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    17/12/2019           LGO                  Initial Version
 * 1.1    31/08/2021           MNA                  Added handle After Insert
**/
trigger ReglementTrigger on sofactoapp__R_glement__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    ReglementTriggerHandler handler = new ReglementTriggerHandler();    

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleafterupdate(trigger.old, trigger.new);
    }     
    
    if (trigger.isbefore && trigger.isdelete) {
      handler.handlebeforedelete(trigger.old);
    }

    //MNA 31/08/2021
    if (trigger.isAfter && trigger.isInsert) {
        handler.handleAfterInsert(trigger.new);
    }
}