/**
 * @File Name          : QuoteLineItemTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 11/02/2020, 15:11:39
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/10/2019   AMO     Initial Version
**/
trigger QuoteLineItemTrigger on QuoteLineItem (after update,after delete, after insert) {

    QuoteLineItemTriggerHandler handler = new QuoteLineItemTriggerHandler();  

   if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }

    else if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
    }

    else if(trigger.isAfter && trigger.isDelete) {
        handler.handleAfterDelete(trigger.new, trigger.old);
    }
}