/**
 * @File Name          : ContractLineItemTrigger.trigger
 * @Description        : Trigger on contact line item
 * @Author             : Menachem Twersky 
 * @Group              : 
 * @Last Modified By   : RRJ                    |   Emmanuel Bruno<e.bruno@askelia.com>
 * @Last Modified On   : 20-12-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    24/06/2019                    Menachem Twersky          Initial Version
 * 1.1    02/07/2019, 14:06:30          RRJ                       Added trigger for automatic contract line item from bundles
**/
trigger ContractLineItemTrigger on ContractLineItem (before insert, after insert, before update, after update, before delete, after delete,  after undelete) {
    TriggerLuncher tl = new TriggerLuncher('ContractLineItem');
    tl.execute();
    ContractLineItemTriggerHandler handler = new ContractLineItemTriggerHandler();

    //RRJ: 20190702 added logic for trigger handler
    if(trigger.isBefore && trigger.isInsert){
        handler.handleBeforeInsert(trigger.new);
    }
    if(trigger.isAfter && trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }
    if(trigger.isBefore && trigger.isUpdate){  
        handler.handleBeforeUpdate(trigger.old, trigger.new);   
    }
    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
    }
    if(trigger.isBefore && trigger.isDelete){
        handler.handleBeforeDelete(trigger.old);
    }
    System.debug('Cpu time cli : '+Limits.getCpuTime());
}