/**
 * @File Name          : AccountTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 15/10/2019, 14:37:36
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    15/10/2019   AMO     Initial Version
**/
trigger AccountTrigger on Account (after insert) {

	System.Debug('## >>> Start of AccountAfterInsert <<< run by ' + UserInfo.getName());
	
	Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
	AccountTriggerHandler handler = new AccountTriggerHandler();
	
	if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AccountTriggerHandler_UpdLogement')){
		AccountTriggerHandler handler = new AccountTriggerHandler();	

		if(trigger.isAfter && trigger.isInsert){
			handler.UpdLogement(trigger.new);
		}

	}

	if(trigger.isAfter){
		if(trigger.isInsert){
			handler.handleAfterInsert(trigger.new);

		}
	} 
}