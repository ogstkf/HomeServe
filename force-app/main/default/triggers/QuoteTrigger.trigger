/**
 * @File Name          : QuoteTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : SHU
 * @Last Modified On   : 11/12/2019, 11:31:59
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * ---    -----------       -------           ------------------------ 
 * 1.0    26/10/2019         AMO              Initial Version
 * 1.0    11/12/2019         SHU              Before update
 * ---    -----------       -------           ------------------------  
**/
trigger QuoteTrigger on Quote (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    QuoteTriggerHandler handler = new QuoteTriggerHandler();
    
    if(Trigger.isBefore && Trigger.isUpdate){
        handler.handleBeforeUpdate(Trigger.old, Trigger.new);
    } 
    else if(Trigger.isAfter && Trigger.isUpdate){
        handler.handleAfterUpdate(Trigger.old, Trigger.new);
    }      
    else if((Trigger.isBefore && Trigger.isInsert)){
        handler.handleBeforeInsert(Trigger.new);
    }else if(Trigger.isAfter && Trigger.isInsert){
        handler.handleAfterInsert(Trigger.new);
    }
}