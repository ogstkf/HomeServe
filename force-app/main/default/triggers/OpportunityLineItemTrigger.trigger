/**
 * @File Name          : OpportunityLineItemTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 15/10/2019, 14:48:13
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/10/2019   AMO     Initial Version
**/
trigger OpportunityLineItemTrigger on OpportunityLineItem (after update, before update, after insert) {
    OpportunityLineItemTriggerHandler handler = new OpportunityLineItemTriggerHandler();

    if(trigger.isAfter && trigger.isUpdate){
        //handler.handleAfterUpdate(trigger.old, trigger.new);
    }
    // if(Trigger.isInsert && Trigger.isAfter && OpportunityLineItemTriggerHandler.isTriggerFire){
    //     Set<Id> qliIds = Trigger.newMap.keyset();
    //     OpportunityLineItemTriggerHandler.sync(qliIds);
    // }
}