trigger CAPTrigger on CAP__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    	/**
 * @File Name          : 
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         19-12-2019     		ANA         Initial Version
**/
    CAPTriggerHandler handler = new CAPTriggerHandler();

    if(Trigger.isAfter && Trigger.isUpdate){
        handler.handleAfterUpdate(Trigger.new,Trigger.old);
    }
}