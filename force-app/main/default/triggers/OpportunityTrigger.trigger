/**
 * @File Name          : OpportunityTrigger.trigger
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 04/11/2019, 09:33:33
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/10/2019   AMO     Initial Version
**/
trigger OpportunityTrigger on Opportunity (after Update, before Insert) {
    OpportunityTriggerHandler handler = new OpportunityTriggerHandler();

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);
        // handler.handleAfterUpdate1(trigger.old, trigger.new);
    }

    if(trigger.isBefore && trigger.isInsert){
        system.debug('*** in before');
        handler.handleBeforeInsert(trigger.new);        
    }
        
}