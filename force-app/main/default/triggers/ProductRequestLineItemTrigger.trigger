/**
 * @File Name          : ProductRequestLineItemTrigger.trigger
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 06/01/2020, 13:48:07
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0      15/11/2019      LGO     Initial Version
**/

trigger ProductRequestLineItemTrigger on ProductRequestLineItem (after insert, before insert, before update, before delete, after update, after delete, after undelete) {
        
    ProductRequestLineItemTriggerHandler handler = new ProductRequestLineItemTriggerHandler();

    if(trigger.isAfter && trigger.isUpdate){
        handler.handleAfterUpdate(trigger.old, trigger.new);

    }

    if(Trigger.isAfter && Trigger.isInsert){
        handler.handleAfterInsert(trigger.new);
    }
}