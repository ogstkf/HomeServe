trigger CustRollUpSummaryReglement on sofactoapp__R_glement__c (after insert,after update, after delete) {
//New List to perform DML on the final List of remises
List<sofactoapp_remise__c> List2Update = New List <sofactoapp_remise__c>();
//New Set of Remises Ids that'd be used to get Remise & Reglement List
Set<ID> remiseIds = New Set <ID>();
//Using Context variables and populating the asofactoapp__R_glement__cove Set
If (trigger.isInsert) 
{
    for (sofactoapp__R_glement__c c1:Trigger.New)
    {
        if (C1.sofactoapp_Remise__c!=null && (C1.sofactoapp__Mode_de_paiement__c == ('Chèque')||C1.sofactoapp__Mode_de_paiement__c == ('Espèces') ))
        {
        remiseIds.add(c1.sofactoapp_Remise__c);
        }
    }

} else If (trigger.isUpdate) {
    for (sofactoapp__R_glement__c c2:Trigger.New)
    {
        sofactoapp__R_glement__c old = Trigger.oldMap.get(C2.id);
        if ( old.sofactoapp_Remise__c !=null && old.sofactoapp_Remise__c <> C2.sofactoapp_Remise__c)
        {
            remiseIds.add(old.sofactoapp_Remise__c);
        }
    if ( old.sofactoapp_Remise__c ==null &&  C2.sofactoapp_Remise__c !=null)
        {
            remiseIds.add(c2.sofactoapp_Remise__c);
        }

    }

} else If (trigger.isDelete) 
{
    for (sofactoapp__R_glement__c c3:Trigger.Old)
    {
        if (C3.sofactoapp_Remise__c!=null)
        {
        remiseIds.add(c3.sofactoapp_Remise__c);
        }
    }
}
//List of Remises in this trigger populated using SOQL
List <sofactoapp_remise__c> remiseList = New List <sofactoapp_remise__c>();
remiseList = [SELECT Id, tech_montant_total__c, tech_nombre_elemennt_remise__c FROM sofactoapp_remise__c WHERE ID IN:remiseIds
Limit 30000];
//List of Reglements in this trigger populated using SOQL
List <sofactoapp__R_glement__c> reglementList = New List <sofactoapp__R_glement__c>();
reglementList = [SELECT id,sofactoapp_Remise__c,sofactoapp__Montant__c FROM sofactoapp__R_glement__c WHERE sofactoapp_Remise__c
                                         in:remiseIds limit 30000];
//New Map for Remsies and List of its Reglements
Map<Id,List<sofactoapp__R_glement__c>> RemisesRegMap = New Map <Id,List<sofactoapp__R_glement__c>>();
//Iterate through Reglements and populate the above map
For (sofactoapp__R_glement__c Reg:reglementList) {        
If (!RemisesRegMap.keyset().contains(Reg.sofactoapp_Remise__c)) {
RemisesRegMap.put(Reg.sofactoapp_Remise__c, New List<sofactoapp__R_glement__c>());
}
RemisesRegMap.get(Reg.sofactoapp_Remise__c).add(Reg);
}
//Iterate through Remsies 
For (sofactoapp_remise__c Rem:remiseList) 
{
    Double Amount = 0;
    integer count =0;
      
    If (RemisesRegMap.get(Rem.Id) != null && RemisesRegMap.get(Rem.Id).size() > 0)
    {
    //Iterate through List of Reglements and add the amounts in the Salary fields
        For (sofactoapp__R_glement__c con1:RemisesRegMap.get(Rem.Id))
        {   
            If (con1.sofactoapp__Montant__c != null)
            {
            Amount = Amount + con1.sofactoapp__Montant__c;
            Count =count +1;
            }
        }    
    }
    Rem.tech_montant_total__c = Amount; //Update the Total Salary field on remsie
    Rem.tech_nombre_elemennt_remise__c= String.valueOf(count);
    List2Update.add(Rem); //add remsie to final update List
}
update List2Update; //update the final list of Remsies
}