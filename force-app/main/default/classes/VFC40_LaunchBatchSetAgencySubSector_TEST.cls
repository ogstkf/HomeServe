/**
 * @File Name          : VFC40_LaunchBatchSetAgencySubSector_TEST.cls
 * @Description        : 
 * @Author             : DMO
 * @Group              : 
 * @Last Modified By   : DMO
 * @Last Modified On   : 06/02/2020, 09:45
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/02/2020   DMO   Initial Version
 * 1.1    04/06/2020   DMU   Commented class due to HomeServe Project 
 
**/
@isTest
public with sharing class VFC40_LaunchBatchSetAgencySubSector_TEST {
  
    static User mainUser;
    static Account testAcc= new Account();
    static List<ServiceTerritory> lstServiceTerritory = new List<ServiceTerritory>();

    static{
        mainUser = TestFactory.createAdminUser('LC23', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            //Create operating hours
            OperatingHours newOperatingHour1 = TestFactory.createOperatingHour('test1');
            insert newOperatingHour1;
            OperatingHours newOperatingHour2 = TestFactory.createOperatingHour('test2');
            insert newOperatingHour2;

            //create sofacto
            sofactoapp__Raison_Sociale__c sofa1 = new sofactoapp__Raison_Sociale__c(Name= 'TEST1', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa1;
            //create sofacto
            sofactoapp__Raison_Sociale__c sofa2 = new sofactoapp__Raison_Sociale__c(Name= 'TEST2', sofactoapp__Credit_prefix__c= '2341', sofactoapp__Invoice_prefix__c='4321');
            insert sofa2;
            //Insert Service territories/ agencies
            lstServiceTerritory = new List<ServiceTerritory>{
                new ServiceTerritory(
                    Name = 'test1',
                    Agency_Code__c = '0007',
                    OperatingHoursId = newOperatingHour1.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa1.Id
                ),
                new ServiceTerritory(
                    Name = 'test2',
                    Agency_Code__c = '0008',
                    OperatingHoursId = newOperatingHour2.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa2.Id
                )
            };
            insert lstServiceTerritory;
        }
    }

    public static testMethod void testVFC40_LaunchBatchSetAgencySubSector() {

        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(lstServiceTerritory);
        controller.setSelected(lstServiceTerritory);
        Test.startTest();
        VFC40_LaunchBatchSetAgencySubSector vFC40_LaunchBatchSetAgencySubSector = new VFC40_LaunchBatchSetAgencySubSector(controller);
        Test.stopTest(); 

    }

}