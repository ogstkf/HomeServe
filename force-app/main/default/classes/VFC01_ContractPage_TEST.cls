/**
 * @File Name          : VFC01_ContractPage_TEST.cls
 * @Description        : 
 * @Author             : SRM
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 04/02/2020, 20:21:54
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    8/2/2019, 11:52:58 AM   SRM     Initial Version
**/
@IsTest
public with sharing class VFC01_ContractPage_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Test Class for VFC01_ContractPage
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 07-MGR-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/
    static User mainUser;
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<Measurement__c> lstTestMeasurement = new List<Measurement__c>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<Contract> lstContract = new List<Contract>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<Opportunity> opportunityList = new List<Opportunity>();
    static List<ServiceTerritory> lstAgence = new List<ServiceTerritory>();
    static sofactoapp__Raison_Sociale__c raisonSocial;

    static {
        mainUser = TestFactory.createAdminUser('VFC01_ContractPage_TEST@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            DateTime dT = System.now();
            Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());

            //creating account
            testAcc = TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating contract
            Contract testContract = TestFactory.createContract('test', testAcc.Id);
            lstContract.add(testContract);
            insert lstContract;

            //creating logement
            Logement__c lgmt = new Logement__c(Account__c = testAcc.Id);
            lstTestLogement.add(lgmt);
            insert lstTestLogement;

            //creating products
            lstTestProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstTestProd.add(TestFactory.createProduct('Entretien gaz'));
            insert lstTestProd;

            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[0].Id, 0));
            if(lstTestProd[0].IsBundle__c){
                lstPrcBkEnt[0].UnitPrice = 150;
            }
            insert lstPrcBkEnt;

            // creating Assets
            for(Integer i = 0; i<5; i++){
                Asset eqp = TestFactory.createAccount('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[0].Id);
                eqp.Product2Id = lstTestProd[0].Id;
                eqp.Logement__c = lstTestLogement[0].Id;
                eqp.AccountId = testacc.Id;
                lstTestAssset.add(eqp);
            }
            insert lstTestAssset;

            

            //opportunity
            opportunityList.add(TestFactory.createOpportunity('test', testAcc.Id));
            insert opportunityList;

            OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = opportunityList[0].Id,
                                                                Quantity = 5,
                                                                PricebookEntryId = lstPrcBkEnt[0].Id,
                                                                TotalPrice = lstPrcBkEnt[0].UnitPrice);
            insert oli;

            OperatingHours OpHrs = new OperatingHours(Name='op');
            insert OpHrs;

            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            lstAgence.add(new ServiceTerritory(Name='test', OperatingHoursId = OpHrs.Id, Sofactoapp_Raison_Social__c = raisonSocial.Id));
            insert lstAgence; 

            //service contract
            for(Integer i=0; i<5; i++){
                lstServCon.add(TestFactory.createServiceContract('testServCon'+i, testAcc.Id));
                lstServCon[i].PriceBook2Id = lstPrcBk[0].Id;
                lstServCon[i].Tax__c = '5.5';
                lstServCon[i].Asset__c = lstTestAssset[i].Id;
                lstServCon[i].Agency__c = lstAgence[0].Id;
                lstServCon[i].Echeancier_Paiement__c = 'Comptant';
            }
            insert lstServCon;
                   

            // create case
            case cse = new case(AccountId = testacc.id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id);
            cse.Service_Contract__c = lstServCon[0].Id;
            cse.Start_Date__c = myDate;
            cse.recordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_anomaly').getRecordTypeId();
            insert cse;


            WorkType wrktypeGaz = TestFactory.createWorkType('wrktypeGaz','Hours',1);
            WorkType wrktypefioul = TestFactory.createWorkType('wrktypeFioul','Hours',2);
            WorkType wrktypePompe = TestFactory.createWorkType('wrktypePompe','Hours',3);
            wrktypeGaz.Equipment_type__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz;
            wrktypefioul.Equipment_type__c = AP_Constant.wrkTypeEquipmentTypeChaudierefioul;
            wrktypePompe.Equipment_type__c = AP_Constant.wrkTypeEquipmentTypePompeachaleur;


            lstWrkTyp = new list<WorkType>{wrktypeGaz,wrktypefioul,wrktypePompe};
            insert lstWrkTyp;
            System.debug('lstWrkTyp ' + lstWrkTyp);

            for(Integer i=0; i<3; i++){
                WorkOrder wrkOrd = TestFactory.createWorkOrder();
                wrkOrd.caseId = cse.id;
                wrkOrd.WorkTypeId = lstWrkTyp[0].Id;
                wrkOrd.Type__c =  'Maintenance';
                wrkOrd.assetId = lstTestAssset[i].Id;
                lstWrkOrd.add(wrkOrd);
            }
            insert lstWrkOrd;

            //creating measurement
            for(Integer i = 0; i<3; i++){
                Measurement__c mes = new Measurement__c(Parent_Work_Order__c=lstWrkOrd[i].Id); 
                mes.Asset__c = lstTestAssset[i].Id;
                lstTestMeasurement.add(mes);
            }
            insert lstTestMeasurement;

            ServiceAppointment serappGaz = TestFactory.createServiceAppointment(lstWrkOrd[0].Id);
            serappGaz.Contract__c = lstContract[0].Id;
            serappGaz.Opportunity__c = opportunityList[0].Id;
            serappGaz.Service_Contract__c = lstServCon[0].Id;
            
            ServiceAppointment serappFioul = TestFactory.createServiceAppointment(lstWrkOrd[1].Id);
            serappFioul.Contract__c = lstContract[0].Id;
            serappFioul.Opportunity__c = opportunityList[0].Id;
            serappFioul.Service_Contract__c = lstServCon[0].Id;

            ServiceAppointment serappPompe = TestFactory.createServiceAppointment(lstWrkOrd[2].Id);
            serappPompe.Contract__c = lstContract[0].Id;
            serappPompe.Opportunity__c = opportunityList[0].Id;
            serappPompe.Service_Contract__c = lstServCon[0].Id;

            lstServiceApp = new List<ServiceAppointment>{serappGaz,serappFioul,serappPompe};
            insert lstServiceApp;

        }
    }


    @isTest
    public static void doGenerate(){

        System.runAs(mainUser){
            
            Test.startTest();

                List<ServiceContract> lstContract1 = new List<ServiceContract>([select id 
                                                              from ServiceContract
                                                              where id in: lstServCon]);
                System.debug('mgr lstContract1 ' + lstContract1);

                PageReference pageRef = Page.VFP01_ContractPage; 
                pageRef.getParameters().put('id', lstContract1[0].Id);
                Test.setCurrentPage(pageRef);
                
                VFC01_ContractPage t = new VFC01_ContractPage();

            Test.stopTest();

        }
    }

}