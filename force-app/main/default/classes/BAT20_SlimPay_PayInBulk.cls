/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 09-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   25-02-2022   MNA   Initial Version
**/
global with sharing class BAT20_SlimPay_PayInBulk implements Database.Batchable <sObject>, Database.Stateful, Database.AllowsCallouts, Schedulable {
    String query;
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Date today = System.today();
        query = 'SELECT Id, sofactoapp__Montant__c, Sofacto_Service_Contract__c, Sofacto_Service_Contract__r.sofactoapp_RUM__c, Sofacto_Service_Contract__r.Agency__c,'
                    + ' Sofacto_Service_Contract__r.Numero_du_contrat__c, Sofacto_Service_Contract__r.sofactoapp_Rib_prelevement__c, '
                    + ' Sofacto_Service_Contract__r.sofactoapp_Rib_prelevement__r.RUM__c FROM sofactoapp__R_glement__c  WHERE sofactoapp__Date__c =: today AND sofactoapp__Mode_de_paiement__c=\'SLIMPAY\'';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sofactoapp__R_glement__c> lstReg) {
        for (sofactoapp__R_glement__c reg : lstReg) {
            WS16_GestionDesPrelevements.payIn(reg);
        }
    }

    global void finish(Database.BatchableContext BC) {

    }

    global void execute(SchedulableContext sc) {
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT20_SlimPay_PayInBulk');
        if(batchConfig != null && batchConfig.BatchSize__c != null) {
            Database.executeBatch(new BAT20_SlimPay_PayInBulk(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT20_SlimPay_PayInBulk(), 1);
        }
    }
}