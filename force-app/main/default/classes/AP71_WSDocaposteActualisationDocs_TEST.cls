/**
 * @File Name          : AP71_WSDocaposteActualisationDocs_TEST.cls
 * @Description        : 
 * @Author             : RRA
 * @Group              : 
 * @Last Modified By   : RRA
 * @Last Modified On   : 17/07/2020, 16:33:31
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17/07/2020    		     RRA        Initial Version
**/ 
@isTest
public with sharing class AP71_WSDocaposteActualisationDocs_TEST {
    public static User mainUser;
    public static AP71_WSDocaposteActualisationDocs.GEDResponseWrapper response;
    public static String respDownloadFile;
    public static String sObjectId;
    public static String idDoca;
    public static String fileName;

    static {
        mainUser = TestFactory.createAdminUser('AP71_WSDocaposteActualisationDocs_TEST@test.COM', 
        TestFactory.getProfileAdminId());
    insert mainUser;
    
    System.runAs(mainUser){
        sObjectId = '8103W000000GnfsQAC';
        idDoca = 'b9048555-f34b-4bb6-a858-7e75036903ca'; 
        fileName = '8103W000000GnfsQAC_122.PDF';
    }
}


    @isTest public static void testViewFiles (){
		// DML Operation and Web Service do not in the same transaction : 
		//the transaction must be separated into two parts so that the DML transaction is completed before the Web Service Callout occurs.
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new AP71_WSDocaposteResponse());
		response = AP71_WSDocaposteActualisationDocs.viewFiles(sObjectId);
		Test.stopTest();				
        System.assertEquals (3, response.count);
        System.assertEquals ('8103W000000GncDQAS', response.data[0].ID_TECH);
        System.assertEquals ('Sécurité (P2) Chaudière Gaz', response.data[0].nom_produit);
        System.assertEquals ('Reglement', response.data[0].STD_ACTIVITY);
        System.assertEquals ('6c345e3c-59d2-40c7-a227-762be7aaccdd', response.data[0].STD_RECORDTYPE_ID);
        System.assertEquals ('CHAM', response.data[0].STD_ARCHIVE_OWNER);
        System.assertEquals ('PDF', response.data[0].STD_FILEFORMAT);
        System.assertEquals ('2020-06-25T18:52:17.529+02:00', response.data[0].STD_TOELIMINATEDATE);
        System.assertEquals ('pay_roll', response.data[0].STD_ACTIVITY_CODE);
        System.assertEquals ('DOCAPOSTE', response.data[0].STD_ARCHIVER_ID);
        System.assertEquals ('TO_ELIMINATE', response.data[0].STD_LIFE_CYCLE_STATUS);
        System.assertEquals (null, response.data[0].STD_RETURNEDDATE);
        System.assertEquals (null, response.data[0].STD_ANOCODE);
        System.assertEquals (391851, response.data[0].STD_FILEWEIGHT);
        System.assertEquals ('d5c4a966-f390-4014-bfdf-f31e026abafd', response.data[0].STD_CREATOR);
        System.assertEquals ('CHAM', response.data[0].STD_ORGANISATION_CODE);
        System.assertEquals ('Avis de renouvellement Comptant', response.data[0].STD_RECORDTYPE_LABEL);
        System.assertEquals ('', response.data[0].STD_DESCRIPTION);
        System.assertEquals ('8103W000000GncDQAS_122.PDF', response.data[0].STD_FILENAME);
        System.assertEquals (3, response.data[0].STD_ORGANISATION_DEPTH);  
        System.assertEquals ('CL000026802', response.data[0].num_client); 
        System.assertEquals (null, response.data[0].STD_LOGICALLYELIMINATEDDATE);  
        System.assertEquals ('b940db8a-287d-4d56-8299-beaba20e5f8a', response.data[0].STD_ARCHIVE_ID);  
        System.assertEquals ('Série des paiements_a425562e-5d4e-4b94-9553-c2bbf6a423e6_2020-05-25T18:51:37.707+02:00', response.data[0].STD_TRANSFER_LABEL);  
        System.assertEquals (false, response.data[0].STD_VITAL); 
        System.assertEquals ('PUBLIC', response.data[0].STD_CLASSIFICATION);  
        System.assertEquals (null, response.data[0].STD_TORETURNDATE);  
        System.assertEquals ('2021-05-31T00:00:00.000+02:00', response.data[0].End_date);  
        System.assertEquals ('AutoVers_801', response.data[0].STD_CREATORNAME);  
        System.assertEquals ('CHAM', response.data[0].STD_ORGANISATION);  
        System.assertEquals ('613', response.data[0].TYP_AGENCE);  
        System.assertEquals ('a425562e-5d4e-4b94-9553-c2bbf6a423e6', response.data[0].STD_TRANSFER_ID);  
        //System.assertEquals ('2020-06-01T00:00:00.000+02:00', response.data[0].start-date);  
        System.assertEquals ('Paiement', response.data[0].STD_DEPOSIT_SYSTEM);  
        System.assertEquals ('d5c4a966-f390-4014-bfdf-f31e026abafd', response.data[0].STD_OWNER);  
        System.assertEquals (4, response.data[0].STD_ACTIVITY_DEPTH);  
        System.assertEquals (false, response.data[0].STD_FROZEN); 
        System.assertEquals ('2020-05-25T18:52:17.529+02:00', response.data[0].STD_CREATIONDATE);  
        System.assertEquals ('1ce0ca13-29ae-49e5-b015-02833b3e5a66', response.data[0].STD_RECORD_ID);  
        System.assertEquals ('8103W000000GncDQAS_CL000026802', response.data[0].STD_TITLE); 
        System.assertEquals ('Reglement', response.data[0].STD_ACTIVITY);  
        System.assertEquals ('6c345e3c-59d2-40c7-a227-762be7aaccdd', response.data[0].STD_RECORDTYPE_ID);  
        System.assertEquals ('CHAM', response.data[0].STD_ARCHIVE_OWNER);
        System.assertEquals ('4d3e3bf3-c5c2-4793-818e-7691e1bcdf72', response.searchId);
        System.assertEquals (null, response.columnsStruct[0].Id);
        System.assertEquals (null, response.columnsStruct[0].creationDate);
        System.assertEquals ('STD_ARCHIVE_OWNER', response.columnsStruct[0].fieldCode);
        System.assertEquals ('Propriétaire de l\'archive', response.columnsStruct[0].fieldLabel);
        System.assertEquals ('STRING', response.columnsStruct[0].filterType);
        System.assertEquals ('TEXT', response.columnsStruct[0].fieldType);
        System.assertEquals (null, response.columnsStruct[0].fieldFormat);
        System.assertEquals (true, response.columnsStruct[0].hidden);
        
   
    }


	@isTest public static void testDownloadFiles (){
		// DML Operation and Web Service do not in the same transaction : 
		//the transaction must be separated into two parts so that the DML transaction is completed before the Web Service Callout occurs.
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new AP71_WSDocaposteResponse());
		respDownloadFile = AP71_WSDocaposteActualisationDocs.DownloadFiless(sObjectId, idDoca, fileName);
		Test.stopTest();				
    }
    

    // Fake value of Mock
	public class AP71_WSDocaposteResponse implements HttpCalloutMock {
		// Implement this interface method
		public HTTPResponse respond(HTTPRequest request) {
		// Create a fake response
		HttpResponse responseDoca = new HttpResponse();
        responseDoca.setHeader('Content-Type', 'application/json');
        // Fake value of JSESSIONID
        responseDoca.setHeader('Set-Cookie', 'JSESSIONID=1a86cwpndpjg1jeukzmgd7u5l;Path=/');
        responseDoca.setBody('{"data":[{"STD_ACTIVITY":"Reglement","STD_RECORDTYPE_ID":"6c345e3c-59d2-40c7-a227-762be7aaccdd","STD_ARCHIVE_OWNER":"CHAM","STD_FILEFORMAT":"PDF","STD_TOELIMINATEDATE":"2020-06-25T18:52:17.529+02:00","STD_ACTIVITY_CODE":"pay_roll","nom_produit":"Sécurité (P2) Chaudière Gaz",'+
        '"STD_LIFE_CYCLE_STATUS":"TO_ELIMINATE","STD_ARCHIVER_ID":"DOCAPOSTE","STD_ORGANISATION_PATH":["CLIENTS","RACINE","CHAM"],"STD_RETURNEDDATE":null,"STD_ANOCODE":null,"STD_FILEWEIGHT":391851,"STD_CREATOR":"d5c4a966-f390-4014-bfdf-f31e026abafd","STD_ORGANISATION_CODE":"CHAM","STD_RECORDTYPE_LABEL":"Avis de renouvellement Comptant","STD_DESCRIPTION":"","STD_MODIFICATIONDATE":"2020-05-25T18:52:54.266+02:00","STD_FILENAME":"8103W000000GncDQAS_122.PDF","STD_ORGANISATION_DEPTH":3,"num_client":"CL000026802","STD_REVISIONS":[],"STD_LOGICALLYELIMINATEDDATE":null,"STD_ACTIVITY_PATH":["pay_roll","CLIENTS","CHAM","RACINE"],"STD_ARCHIVE_ID":"b940db8a-287d-4d56-8299-beaba20e5f8a","STD_TRANSFER_LABEL":"Série des paiements_a425562e-5d4e-4b94-9553-c2bbf6a423e6_2020-05-25T18:51:37.707+02:00","STD_VITAL":false,"STD_CLASSIFICATION":"PUBLIC","STD_TORETURNDATE":null,"End_date":"2021-05-31T00:00:00.000+02:00","num_contrat":"CT-00164757","STD_CREATORNAME":"AutoVers_801","STD_ORGANISATION":"CHAM","TYP_AGENCE":"613","STD_TRANSFER_ID":"a425562e-5d4e-4b94-9553-c2bbf6a423e6","start-date":"2020-06-01T00:00:00.000+02:00","STD_DEPOSIT_SYSTEM":"Paiement","STD_OWNER":"d5c4a966-f390-4014-bfdf-f31e026abafd","ID_TECH":"8103W000000GncDQAS","STD_ACTIVITY_DEPTH":4,"STD_FROZEN":false,"STD_CREATIONDATE":"2020-05-25T18:52:17.529+02:00","STD_RECORD_ID":"1ce0ca13-29ae-49e5-b015-02833b3e5a66","STD_TITLE":"8103W000000GncDQAS_CL000026802"},{"STD_ACTIVITY":"Reglement","STD_RECORDTYPE_ID":"6c345e3c-59d2-40c7-a227-762be7aaccdd","STD_ARCHIVE_OWNER":"CHAM","STD_FILEFORMAT":"PDF","STD_TOELIMINATEDATE":"2020-06-22T19:12:13.375+02:00","STD_ACTIVITY_CODE":"pay_roll","nom_produit":"Sécurité (P2) Chaudière Gaz","STD_LIFE_CYCLE_STATUS":"TO_ELIMINATE","STD_ARCHIVER_ID":"DOCAPOSTE","STD_ORGANISATION_PATH":["CLIENTS","RACINE","CHAM"],"STD_RETURNEDDATE":null,"STD_ANOCODE":null,"STD_FILEWEIGHT":391851,"STD_CREATOR":"d5c4a966-f390-4014-bfdf-f31e026abafd","STD_ORGANISATION_CODE":"CHAM","STD_RECORDTYPE_LABEL":"Avis de renouvellement Comptant","STD_DESCRIPTION":"","STD_MODIFICATIONDATE":"2020-05-22T19:12:37.853+02:00","STD_FILENAME":"8103W000000GncDQAS_122.PDF","STD_ORGANISATION_DEPTH":3,"num_client":"CL000026802","STD_REVISIONS":[],"STD_LOGICALLYELIMINATEDDATE":null,"STD_ACTIVITY_PATH":["pay_roll","CLIENTS","CHAM","RACINE"],"STD_ARCHIVE_ID":"1aec7a1e-8be2-4875-9597-e0baf0985082","STD_TRANSFER_LABEL":"Série des paiements_72bc2edb-900f-4344-8a8c-852e3a6736f2_2020-05-22T19:11:35.119+02:00","STD_VITAL":false,"STD_CLASSIFICATION":"PUBLIC","STD_TORETURNDATE":null,"End_date":"2021-05-31T00:00:00.000+02:00","num_contrat":"CT-00164757","STD_CREATORNAME":"AutoVers_801","STD_ORGANISATION":"CHAM","TYP_AGENCE":"613","STD_TRANSFER_ID":"72bc2edb-900f-4344-8a8c-852e3a6736f2","start-date":"2020-06-01T00:00:00.000+02:00","STD_DEPOSIT_SYSTEM":"Paiement","STD_OWNER":"d5c4a966-f390-4014-bfdf-f31e026abafd","ID_TECH":"8103W000000GncDQAS","STD_ACTIVITY_DEPTH":4,"STD_FROZEN":false,"STD_CREATIONDATE":"2020-05-22T19:12:13.375+02:00","STD_RECORD_ID":"b9048555-f34b-4bb6-a858-7e75036903ca","STD_TITLE":"8103W000000GncDQAS_CL000026802"},{"STD_ACTIVITY":"Reglement","STD_RECORDTYPE_ID":"6c345e3c-59d2-40c7-a227-762be7aaccdd","STD_ARCHIVE_OWNER":"CHAM","STD_FILEFORMAT":"PDF","STD_TOELIMINATEDATE":"2020-06-22T19:10:13.014+02:00","STD_ACTIVITY_CODE":"pay_roll","nom_produit":"Sécurité (P2) Chaudière Gaz","STD_LIFE_CYCLE_STATUS":"TO_ELIMINATE","STD_ARCHIVER_ID":"DOCAPOSTE",' + 
        '"STD_ORGANISATION_PATH":["CLIENTS","RACINE","CHAM"],"STD_RETURNEDDATE":null,"STD_ANOCODE":null,"STD_FILEWEIGHT":391856,"STD_CREATOR":"d5c4a966-f390-4014-bfdf-f31e026abafd","STD_ORGANISATION_CODE":"CHAM","STD_RECORDTYPE_LABEL":"Avis de renouvellement Comptant","STD_DESCRIPTION":"","STD_MODIFICATIONDATE":"2020-05-22T19:10:35.580+02:00","STD_FILENAME":"8103W000000GncDQAS_122.PDF","STD_ORGANISATION_DEPTH":3,"num_client":"CL000026802","STD_REVISIONS":[],"STD_LOGICALLYELIMINATEDDATE":null,"STD_ACTIVITY_PATH":["pay_roll","CLIENTS","CHAM","RACINE"],"STD_ARCHIVE_ID":"83f0a642-c2d7-4062-b260-557d7fb321b1","STD_TRANSFER_LABEL":"Série des paiements_5e5a2f9b-369b-45b4-87d5-d6a67489726d_2020-05-22T19:09:31.220+02:00","STD_VITAL":false,"STD_CLASSIFICATION":"PUBLIC","STD_TORETURNDATE":null,"End_date":"2021-05-31T00:00:00.000+02:00","num_contrat":"CT-00164757","STD_CREATORNAME":"AutoVers_801","STD_ORGANISATION":"CHAM","TYP_AGENCE":"613","STD_TRANSFER_ID":"5e5a2f9b-369b-45b4-87d5-d6a67489726d","start-date":"2020-06-01T00:00:00.000+02:00","STD_DEPOSIT_SYSTEM":"Paiement","STD_OWNER":"d5c4a966-f390-4014-bfdf-f31e026abafd","ID_TECH":"8103W000000GncDQAS","STD_ACTIVITY_DEPTH":4,"STD_FROZEN":false,"STD_CREATIONDATE":"2020-05-22T19:10:13.014+02:00","STD_RECORD_ID":"049114e4-daf3-41c1-8bba-a5b8e4d180d0","STD_TITLE":"8103W000000GncDQAS_CL000026802"}],"searchId":"4d3e3bf3-c5c2-4793-818e-7691e1bcdf72","count":3,"columnsStruct":[{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_ARCHIVE_OWNER","fieldLabel":"Propriétaire de l\'archive","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_ARCHIVER_ID","fieldLabel":"Identifiant de l\'archiveur","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_DEPOSIT_SYSTEM","fieldLabel":"Système versant","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_MODIFIER","fieldLabel":"Identifiant du dernier modificateur","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_TITLE","fieldLabel":"Titre","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_RECORDTYPE_LABEL","fieldLabel":"Libellé du type du record","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_ACTIVITY","fieldLabel":"Activité","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_ORGANISATION","fieldLabel":"Organisation","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_CREATOR","fieldLabel":"Créé par","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_CREATIONDATE","fieldLabel":"Date de création","filterType":"DATETIME","fieldType":"DATE","fieldFormat":null,"hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_MODIFICATIONDATE","fieldLabel":"Date de modification","filterType":"DATETIME","fieldType":"DATE","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_RECORD_ID","fieldLabel":"Identifiant de l\'enregistrement","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_DESCRIPTION","fieldLabel":"Description","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_FILENAME","fieldLabel":"Nom du fichier","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_FILEFORMAT","fieldLabel":"Format du fichier","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_LIFE_CYCLE_STATUS","fieldLabel":"Statut","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_TORETURNDATE","fieldLabel":"Date prévue de restitution","filterType":"DATETIME","fieldType":"DATE","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_TOELIMINATEDATE","fieldLabel":"Date prévue d\'élimination","filterType":"DATETIME","fieldType":"DATE","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_RETURNEDDATE","fieldLabel":"Date de restitution","filterType":"DATETIME","fieldType":"DATE","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_ARCHIVE_ID","fieldLabel":"Identifiant ESC","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_FILEWEIGHT","fieldLabel":"Poids du fichier","filterType":"NUMERIC","fieldType":"INTEGER","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_TRANSFER_ID","fieldLabel":"Identifiant du versement","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"TYP_AGENCE","fieldLabel":"Code comptable","filterType":"STRING","fieldType":"TEXT","fieldFormat":"DATE_YYYY","hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_FROZEN","fieldLabel":"Gelé","filterType":"BOOLEAN","fieldType":"BOOLEAN","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"ID_TECH","fieldLabel":"ID","filterType":"STRING","fieldType":"TEXT","fieldFormat":"DATE_YYYY","hidden":false},' + 
        '{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"num_contrat","fieldLabel":"Contrat","filterType":"STRING","fieldType":"TEXT","fieldFormat":"DATE_YYYY","hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"nom_produit","fieldLabel":"Nom","filterType":"STRING","fieldType":"TEXT","fieldFormat":"DATE_YYYY","hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"start-date","fieldLabel":"Date début","filterType":"DATETIME","fieldType":"DATE","fieldFormat":"DATE_DD_MM_YYYY","hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"End_date","fieldLabel":"Date fin","filterType":"DATETIME","fieldType":"DATE","fieldFormat":"DATE_DD_MM_YYYY","hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"num_client","fieldLabel":"Numéro client","filterType":"STRING","fieldType":"TEXT","fieldFormat":"DATE_YYYY","hidden":false},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_REVISIONS","fieldLabel":"Révisions","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true},{"id":null,"creationDate":null,"modificationDate":null,"fieldCode":"STD_CLASSIFICATION","fieldLabel":"Classification","filterType":"STRING","fieldType":"TEXT","fieldFormat":null,"hidden":true}]}');
		responseDoca.setStatusCode(200);   
		responseDoca.setStatus('OK');   
		return responseDoca; 
		}
    }
}