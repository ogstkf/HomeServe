/**
 * @File Name          : AP31_ValideDevisIntervention_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/02/2020   AMO     Initial Version
 * 1.1    05/02/2020   YGO     
**/
@isTest
public with sharing class AP31_ValideDevisIntervention_TEST {
/**
 * @File Name          : AP31_ValideDevisIntervention_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         24-10-2019     		    KZE         Initial Version
**/
static User mainUser;
    static Order ord ;
    static Account testAcc = new Account();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<Quote> lstQuote = new List<Quote>();
    static List<WorkOrder> lstWO = new List<WorkOrder>();
    //bypass
    static Bypass__c bp = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('AP24@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;


        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //create opportunity
            lstOpp =  new List<Opportunity>{ TestFactory.createOpportunity('Test1',testAcc.Id) ,
                                                TestFactory.createOpportunity('Test2',testAcc.Id) 
            };
            lstOpp[0].APP_intervention_immediate__c = true;
            lstOpp[1].APP_intervention_immediate__c = false;
            insert lstOpp;

            //create Quotes
            lstQuote =  new List<Quote>{    
                                new Quote(A_planifier__c = false, 
                                            Name ='Test1',  
                                            Devis_signe_par_le_client__c = true, 
                                            OpportunityId = lstOpp[0].Id, 
                                            Date_de_debut_des_travaux__c = System.today(),
                                            tech_deja_facture__c = true,
                                            Mode_de_paiement_de_l_acompte__c = 'CB',
                                            Montant_de_l_acompte_verse__c=500),
                                            
                                new Quote(A_planifier__c = false, 
                                            Name ='Test2',  
                                            Devis_signe_par_le_client__c = true, 
                                            OpportunityId = lstOpp[1].Id, 
                                            Date_de_debut_des_travaux__c = System.today(),
                                            tech_deja_facture__c = true,
                                            Mode_de_paiement_de_l_acompte__c = 'CB',
                                            Montant_de_l_acompte_verse__c=500)
                                            
            };
            insert lstQuote;

            lstOpp[0].Tech_Synced_Devis__c = lstQuote[0].Id;
            lstOpp[1].Tech_Synced_Devis__c = lstQuote[1].Id;
            update lstOpp;

            lstWO = new List<WorkOrder>{
                new WorkOrder(Quote__c = lstQuote[0].Id, Status = 'New', Type__c = 'Maintenance', Reason__c = 'Visite sous contrat'),
                new WorkOrder(Quote__c = lstQuote[1].Id, Status = 'New', Type__c = 'Maintenance', Reason__c = 'Visite sous contrat')
            };
            insert lstWO;
        }
    }

    @isTest
    public static void testInsertWorkOrders(){
        System.runAs(mainUser){
            Test.startTest();
                lstQuote[0].Devis_signe_par_le_client__c = true;
                lstQuote[1].Devis_signe_par_le_client__c = true;
                lstQuote[0].A_planifier__c = true;
                lstQuote[1].A_planifier__c = true;
                lstQuote[0].Ordre_d_execution__c = lstWO[0].Id;
                lstQuote[1].Ordre_d_execution__c = lstWO[1].Id;

                lstQuote[0].Status = AP_Constant.quoteStatusValideSigne;
                lstQuote[1].Status = AP_Constant.quoteStatusValideSigne;
                update lstQuote;
            Test.stopTest();

            list<WorkOrder> lstNewQuo = [ SELECT Quote__c, ParentWorkOrderId  
                                            FROM WorkOrder
                                            WHERE Quote__c = :lstQuote[0].Id
                                            ORDER BY CreatedDate DESC] ;       
            // System.assertEquals(lstNewQuo[0].ParentWorkOrderId, lstWO[0].ID );

            list<WorkOrder> lstNewQuo2 = [ SELECT Quote__c, ParentWorkOrderId  
                                            FROM WorkOrder
                                            WHERE Quote__c = :lstQuote[1].Id
                                            ORDER BY CreatedDate DESC] ;       
            System.assertNotEquals(lstNewQuo2[0].ParentWorkOrderId, lstWO[1].ID );
        }
    }

}