/**
 * @File Name          : OpportunityTriggerHandler_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 05/02/2020, 10:35:24
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/02/2020   AMO     Initial Version
**/
@isTest
private class OpportunityTriggerHandler_TEST{
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : test class OpportunityTriggerHandler
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 13-JUIN-2019  DMU    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/
	static User mainUser;
    static List<ServiceAppointment> lstServiceApp;
    static List<Account> lstAcc;
    static List<Opportunity> lstOpportunity = new List<Opportunity>();
    static List<Logement__c> lstLogement;
    static List<ServiceTerritory> lstAgence;
    static List<Contract> lstContract = new List<Contract>();
    static List<OperatingHours> lstOpHrs;
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static List<WorkOrder> lstWorkOrder = new List<WorkOrder>();
    static Quote quo = new Quote();


     static{
        mainUser = TestFactory.createAdminUser('AP01_BlueWSCallout_TEST@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        lstAcc = new List<Account>{TestFactory.createAccount('test_create2RDV')                            
        };
        lstAcc[0].AccountSource = AP_Constant.accSourceSiteMyChauffage;
        lstAcc[0].Status__c = AP_Constant.accStatusEnAttenteVisit;
        insert lstAcc;

        sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAcc.get(0).Id);
        lstAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
        update lstAcc;

        lstOpHrs = new List<OperatingHours>{TestFactory.createOperatingHour('op')
        };
        insert lstOpHrs;

        lstContract.add(TestFactory.createContract('test', lstAcc[0].Id));
        insert lstContract;

        //Create WorkOrder
        lstWorkOrder = new List<WorkOrder>{
            new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal),
            new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal)
        };

        insert lstWorkOrder;

        lstOpportunity = new List<Opportunity>{TestFactory.createOpportunity('test', lstAcc[0].Id), 
            TestFactory.createOpportunity('test', lstAcc[0].Id)}; 
        lstOpportunity[0].StageName = AP_Constant.oppStageQua;
        lstOpportunity[1].StageName = AP_Constant.oppStageQua;
        lstOpportunity[0].Ordre_d_execution__c = lstWorkOrder[0].Id;
        insert lstOpportunity;

        raisonSocial = DataTST.createRaisonSocial();
        insert raisonSocial;

        lstAgence = new List<ServiceTerritory>{TestFactory.createServiceTerritory('test', lstOpHrs[0].Id, raisonSocial.Id)
        };
        insert lstAgence;
        
        lstLogement = new List<Logement__c>{TestFactory.createLogement(lstAcc[0].Id, lstAgence[0].Id)};
        lstLogement[0].Street__c='rue test, stpierre, spoon, testing moka helvetiaaqwdweftgryjutjujybgryheyje';
        lstLogement[0].Postal_Code__c = '75009';
        lstLogement[0].City__c = 'Paris';
        insert lstLogement;

        lstServiceApp = new List<ServiceAppointment>{TestFactory.createServiceAppointment(lstAcc[0].Id)
        											,TestFactory.createServiceAppointment(lstAcc[0].Id)                               
        };
        lstServiceApp[0].Status = 'Scheduled';
        lstServiceApp[0].Subject = 'this is a test subject';
        lstServiceApp[0].TECH_SA_payed__c = false;
        lstServiceApp[0].EarliestStartTime = System.now();
        lstServiceApp[0].DueDate = System.now() + 30;
        lstServiceApp[0].Opportunity__c = lstOpportunity[0].Id;
        lstServiceApp[0].Contract__c = lstContract[0].Id;
        lstServiceApp[0].Residence__c = lstLogement[0].Id;
        lstServiceApp[0].Category__c = AP_Constant.ServAppCategoryVisiteEntr;
        lstServiceApp[0].ServiceTerritoryId = lstAgence[0].Id; 

        lstServiceApp[1].Status = 'Scheduled';
        lstServiceApp[1].Subject = 'this is a test subject';
        lstServiceApp[1].TECH_SA_payed__c = false;
        lstServiceApp[1].EarliestStartTime = System.now();
        lstServiceApp[1].DueDate = System.now() + 30;
        lstServiceApp[1].Opportunity__c = lstOpportunity[1].Id;
        lstServiceApp[1].Contract__c = lstContract[0].Id;
        lstServiceApp[1].Residence__c = lstLogement[0].Id;
        lstServiceApp[1].Category__c = AP_Constant.ServAppCategoryVisiteEntr;
        lstServiceApp[1].ServiceTerritoryId = lstAgence[0].Id;
        insert lstServiceApp;

        //create Quote
        quo = new Quote(Name ='Test1',
                        Devis_signe_par_le_client__c = false,
                        OpportunityId = lstOpportunity[0].Id,
                        Ordre_d_execution__c= lstWorkOrder[1].Id);
        insert quo;


    }

    @isTest
    public static void createServiceAppointment_updatetest(){
        System.runAs(mainUser){
            Test.startTest();

            lstOpportunity[0].StageName = AP_Constant.oppStageClosedWon;
            lstOpportunity[1].StageName = AP_Constant.oppStageClosedWon;
        	update lstOpportunity;             	
            
            Test.stopTest();

            list<ServiceAppointment> serAppNew = [select id from ServiceAppointment where Opportunity__c in:lstOpportunity];           
        	System.assertEquals(2, serAppNew.size());
        }
    }

    @isTest
    public static void UpdateOpportunityWorkOrder(){
        System.runAs(mainUser){
            Test.startTest();

            lstOpportunity[0].Ordre_d_execution__c = lstWorkOrder[1].Id;
        	update lstOpportunity;    
            update quo;         	
            
            Test.stopTest();


            Quote quot = [select id, Ordre_d_execution__c from Quote where Id =:quo.Id];           
        	System.assertEquals(lstOpportunity[0].Ordre_d_execution__c, quot.Ordre_d_execution__c);
        }
    }
}