public class CoordonneesBancairesHandler {
    static long Result ;
    static integer A;
    static integer B;
    static long C;
    static String X;
    static String X1;
    static String X2;
   	public static Boolean notifiedBySlimpay = false;
    
    public static void afterUpdate(List<sofactoapp__Coordonnees_bancaires__c> lstCBNew, List<sofactoapp__Coordonnees_bancaires__c> lstCBOld) {
        Set<Id> setCBIdToMandates = new Set<Id>();
        for (Integer i = 0; i < lstCBNew.size() ; i++) {
            if (lstCBNew[i].Date_signature_du_mandat__c != lstCBOld[i].Date_signature_du_mandat__c && lstCBNew[i].Date_signature_du_mandat__c != null && !notifiedBySlimpay) {
                setCBIdToMandates.add(lstCBNew[i].Id);
            }
        }
        if (setCBIdToMandates.size() > 0)
            AP81_SlimPayCallOut.createMandates(setCBIdToMandates);
    }
    
    public static void IbanMethod (list<sofactoapp__Coordonnees_bancaires__c> NvIban){
 
   		
        for (sofactoapp__Coordonnees_bancaires__c RIB : NvIban) {
        	
            
           if (!String.isBlank( RIB.sofactoapp__IBAN__c ))  {
                 
                if(ValidateIban(RIB) == 1){ 
                    System.debug(' valeur IBAN ' + RIB.Id  );
                    RIB.tech_Rib_Valid__c = true ;
                    
                	}
                else {
                    System.debug(' on est ici  ligne52 '  );
                    RIB.tech_Rib_Valid__c = false ;
                	}
                }    
           
           else if  (String.isBlank(  RIB.sofactoapp__IBAN__c  )){
               System.debug(' on est ici  ligne52 '  );
                    RIB.tech_Rib_Valid__c = false ;
           }
            
		}
        
      
      }   
    
    //Methode for after trigger
     //public static void contractupdate (list<sofactoapp__Coordonnees_bancaires__c> IbanList){
        
       // Set <Id> ContractsID = new set <Id>();
       // for (sofactoapp__Coordonnees_bancaires__c RIB :IbanList) {
        //	if (RIB.Sofactoapp_Contrat_de_service__c != null) {
         //   ContractsID.add(RIB.Sofactoapp_Contrat_de_service__c);
        	//}
      	//} 
        
       //	Map<Id,ServiceContract> MesContrats = new Map<Id, ServiceContract> ([SELECT id, AccountId, sofactoapp_Rib_prelevement__c From ServiceContract where Id IN :ContractsID]) ;
       
       // for (sofactoapp__Coordonnees_bancaires__c Iban : IbanList) {
        	//if (Iban.Sofactoapp_Contrat_de_service__c != null) {
        //        ServiceContract CTR = MesContrats.get(Iban.Sofactoapp_Contrat_de_service__c);
               // CTR.sofactoapp_Rib_prelevement__c = Iban.Id;
          //      
            //}
        //}
       // update MesContrats.values() ;
    //}
    
    
    // Methode to validate IBAN
    public static Long ValidateIban (sofactoapp__Coordonnees_bancaires__c RIB){
                X = RIB.sofactoapp__IBAN__c;
                X =X.replaceAll('[^a-zA-Z0-9\\s]', '36');
                X =X.replaceAll('[\\s]', '');
            	X1 = X.left(4);
            	X2 = X.right((X.length()) - 4);
            	X=X2+X1;
              
            	X = X.replaceAll('[Aa]','10');
                X = X.replaceAll('[Bb]','11');
                X = X.replaceAll('[Cc]','12');
                X = X.replaceAll('[Dd]','13');
                X = X.replaceAll('[Ee]','14');
                X = X.replaceAll('[Ff]','15');
                X = X.replaceAll('[Gg]','16');
                X = X.replaceAll('[Hh]','17');
                X = X.replaceAll('[Ii]','18');
                X = X.replaceAll('[Jj]','19');
                X = X.replaceAll('[Kk]','20');
                X = X.replaceAll('[Ll]','21');
                X = X.replaceAll('[Mm]','22');
                X = X.replaceAll('[Nn]','23');
                X = X.replaceAll('[Oo]','24');
                X = X.replaceAll('[Pp]','25');
                X = X.replaceAll('[Qq]','26');
                X = X.replaceAll('[Rr]','27');
                X = X.replaceAll('[Ss]','28');
                X = X.replaceAll('[Tt]','29');
                X = X.replaceAll('[Uu]','30');
                X = X.replaceAll('[Vv]','31');
                X = X.replaceAll('[Ww]','32');
                X = X.replaceAll('[Xx]','33');
                X = X.replaceAll('[Yy]','34');
                X = X.replaceAll('[Zz]','35');
                // decomposition du Numero IBan pour calcul du modulo (97)
                While ((X.length())>12) {
                    System.debug(' X au total ' +  X);
                    X1 = X.left(9);
                
                    A = integer.valueOf(X1);
    
                    Result =math.mod(A,97);
                
                    X1 = X.right((X.length()) - 9);
                
                    X = Result + X1;
                    }
               
                C = long.valueOf(X) ;
                Result= math.mod(C,97) ;
                
                    return Result ;
                  
            
        
        }

}