/**
 * @File Name          : AP22_HandleProductDelivery_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class AP22_HandleProductDelivery_TEST {
 /**
 * @File Name          : AP22_HandleProductDelivery_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-10-2019     		LGO         Initial Version
**/
static User mainUser;
    static Order ord ;
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
    static Shipment ship = new Shipment();
    static ProductTransfer prodTrans = new ProductTransfer();
	static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
	static PricebookEntry PrcBkEnt = new PricebookEntry();
    static OrderItem ordItem = new OrderItem();
    static Account FournisseurAcc;

    static{
        mainUser = TestFactory.createAdminUser('AP22@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccount('AP22_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            FournisseurAcc= new Account(Name='ChamFournissuer');
            FournisseurAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            insert FournisseurAcc;

             //create products
            prod = TestFactory.createProduct('testProd');
            insert prod;


            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            // System.debug('##### TEST lstPrcBk: '+lstPrcBk);

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;


            //create Order
            ord = new Order(AccountId = FournisseurAcc.Id
                                    ,Name = 'Test1'
                                    ,EffectiveDate = System.today()
                                    ,Status = 'Annulé'
                                    ,Pricebook2Id = lstPrcBk[0].Id
                                    ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId());

            insert ord;

            ordItem = new OrderItem(
				            OrderId=ord.Id,
				            Quantity=decimal.valueof('1'),
				            PricebookEntryId=PrcBkEnt.Id,
				            UnitPrice = 145);
        
            insert ordItem;

            System.debug('##### TEST ord: '+ord);

            ship = new Shipment (
                ShipToName = 'Test1',
                Order__c = ord.Id,
                // N_de_bon_de_livraison__c = '1234',
                Status = 'En attente',
                RecordTypeId=Schema.SObjectType.Shipment.getRecordTypeInfosByName().get(AP_Constant.RTReceptionFournisseur).getRecordTypeId(), 
                ExpectedDeliveryDate = System.today().addDays(14),
                // Date_du_bon_de_livraison__c = System.today().addDays(-1), 
                Satisfaction_qualit_de_la_commande__c = 'Moyenne' 
            );

            insert ship;

            prodTrans = new ProductTransfer(ShipmentId = ship.Id,
                        QuantityReceived = 2,
                        QuantitySent = 2);

            insert prodTrans;
        }
    }

    @isTest
    public static void testcheckProductTransfer(){
        System.runAs(mainUser){
            ship.Status = AP_Constant.ShipStatusLivre;
            System.debug('##### TEST ord1: '+ord);
            Test.startTest();
                ship.N_de_bon_de_livraison__c = '1234';
                ship.Date_du_bon_de_livraison__c = System.today().addDays(-1);
                update ship;
            Test.stopTest();

            list<Order> lstNewOrd = [SELECT Id,
                                    Date_de_livraison_reelle__c, 
                                    Status ,
                                    (SELECT Id
                                        FROM Reception__r
                                        WHERE Id = :ship.Id
                                    )
                                    FROM Order] ;
            

            System.assertEquals(lstNewOrd[0].Status,AP_Constant.OrdStatusSolde);
        }
    }

}