@isTest
public class AP16_MobileNotificationControllerTest {
static User mainUser;
    
    static{
        mainUser = TestFactory.createAdminUser('MobileTest@test.COM', 
                                                TestFactory.getProfileAdminId());
    }
    
    
    @isTest
    public static void onServiceAppointmentAssignedTEST(){
        System.runAs(mainUser){
     		List<Map<String,Object>> messages = AP16_MobileNotificationsController.createMessage('Test Message', null,true);
            String notifId = (String)messages.get(0).get('Id');
            AP16_MobileNotificationsController.sendMessage(notifId);
            AP16_MobileNotificationsController.deleteMessage(notifId);
        }
    }
}