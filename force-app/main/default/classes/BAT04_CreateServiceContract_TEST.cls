/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 09-07-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-24-2020   ZJO   Initial Version
**/
@IsTest
private class BAT04_CreateServiceContract_TEST {
    static User adminUser;
    static List<Account> lstAcc = new List<Account>();
    static List<Contact> lstContacts = new List<Contact>();
    static List<Product2> lstProd = new List<Product2>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PriceBookEntry> lstPrcBkEnt = new List<PriceBookEntry>();
    static List<sofactoapp__Raison_Sociale__c> lstSofacto = new List<sofactoapp__Raison_Sociale__c>();
    static List<ServiceTerritory> lstAgences = new List<ServiceTerritory>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static sofactoapp__Coordonnees_bancaires__c bnkDet = new sofactoapp__Coordonnees_bancaires__c();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstCli = new List<ContractLineItem>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<Bundle__c> lstTestBundle = new List<Bundle__c>();
    static List<Product_Bundle__c> lstTestBundleProd = new List<Product_Bundle__c>();
  

    static {
        adminUser = TestFactory.createAdminUser('testuser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser) {

             //Create Accounts
             lstAcc = new List<Account>{
                TestFactory.CreateAccount('TestAcc1'),
                TestFactory.CreateAccount('TestAcc2')
             };

             lstAcc[0].recordtypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
             lstAcc[1].recordtypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
             lstAcc[0].BillingPostalCode = '123456';
             lstAcc[1].BillingPostalCode = '135712';
            
            insert lstAcc;
            
            //Update Accounts
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstAcc.get(1).Id);     
			lstAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;

            update lstAcc;
           
            //Create Contacts
            lstContacts = new List<Contact>{
                new Contact(LastName = 'Test Con 1'),
                new Contact(LastName = 'Test Con 2')
            };
            insert lstContacts;
            
            //Create list of sofactos
            lstSofacto = new List<sofactoapp__Raison_Sociale__c>{
                new sofactoapp__Raison_Sociale__c(
                    Name = 'test sofacto 1',
                    sofactoapp__Credit_prefix__c = '1234',
                    sofactoapp__Invoice_prefix__c = '2134'
                ),
                new sofactoapp__Raison_Sociale__c(
                    Name = 'test sofacto 2',
                    sofactoapp__Credit_prefix__c = '1321',
                    sofactoapp__Invoice_prefix__c = '2132'
                )
            };         
            insert lstSofacto;

            //Create Operating Hour
            OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
            insert OprtHour;

            //Create list of Agences
            List<ServiceTerritory> lstAgences = new List<ServiceTerritory>{
                new ServiceTerritory(
                    Name = 'test agence 1',
                    Agency_Code__c = '0001',
                    IsActive = True,
                    OperatingHoursId = OprtHour.Id,
                    Sofactoapp_Raison_Social__c = lstSofacto[0].Id
                ),
                new ServiceTerritory(
                    Name = 'test agence 2',
                    Agency_Code__c = '0002',
                    IsActive = True,
                    OperatingHoursId = OprtHour.Id,
                    Sofactoapp_Raison_Social__c = lstSofacto[1].Id
                )
            };
            insert lstAgences;
           
            //Create Products
            lstProd = new List<Product2>{
                TestFactory.createProduct('TestProd1')
            };
            
            lstProd[0].IsBundle__c = true;
            lstProd[0].Equipment_family__c = 'Chauffe eau';          
            lstProd[0].Equipment_type1__c = 'Chauffe-eau thermodynamique hermétique';
            lstProd[0].recordtypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Product').getRecordTypeId();
            insert lstProd;
            
            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true, Ref_Agence__c = true, Agence__c=lstAgences[0].Id);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create List of PricebookEntry
            lstPrcBkEnt = new List<PricebookEntry>{
                TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 100)
            };              
            insert lstPrcBkEnt;            
          
            //Create list of Logements
            lstLogement = new List<Logement__c>{
              TestFactory.createLogement(lstAcc[0].Id, lstAgences[0].Id),
              TestFactory.createLogement(lstAcc[1].Id, lstAgences[1].Id)
            };
            insert lstLogement;

            //Create list of Assets
            lstAsset = new List<Asset>{
                new Asset(
                    Name = 'Test Asset 1',
                    Product2Id = lstProd[0].Id,
                    AccountId = lstAcc[0].Id,
                    Status = 'Actif',
                    Logement__c = lstLogement[0].Id
                ),
                new Asset(
                    Name = 'Test Asset 2',
                    Product2Id = lstProd[0].Id,
                    AccountId = lstAcc[1].Id,
                    Status = 'Actif',
                    Logement__c = lstLogement[1].Id
                )
            };
            insert lstAsset;

            //Create Bank Details
            bnkDet = new sofactoapp__Coordonnees_bancaires__c(
                sofactoapp__Compte__c = lstAcc[0].Id, 
                Sofacto_Actif__c = true,
                tech_Rib_Valid__c = true,
                sofactoapp__IBAN__c = 'FR14 2004 1010 0505 0001 3M02 606',
                sofactoapp__Raison_sociale__c = lstSofacto[0].Id
            );
            insert bnkDet;

            //create bundles
            for(Integer i=0; i<1; i++){
                lstTestBundle.add(TestFactory.createBundle('testBundle'+i, 150));
            }
            insert lstTestBundle;

            //create bundle products related to bundle and product
            for(Integer i=0; i<1; i++){
                lstTestBundleProd.add(TestFactory.createBundleProduct(lstTestBundle[0].Id, lstProd[0].Id));
                lstTestBundleProd.add(TestFactory.createBundleProduct(lstTestBundle[0].Id, lstProd[0].Id));
            }

            lstTestBundleProd[0].Bundle__c = lstTestBundle[0].Id;
            lstTestBundleProd[0].Optional_Item__c = false;
            lstTestBundleProd[1].Bundle__c = lstTestBundle[0].Id;
            lstTestBundleProd[1].Optional_Item__c = true;
            insert lstTestBundleProd;
           

            //Create List of ServiceContract
            lstServCon = new List<ServiceContract>{
                new ServiceContract(
                    Name = 'Test ServCon 1',
                    RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels'),
                    AccountId = lstAcc[0].Id,
                    Payeur_du_contrat__c = lstAcc[0].Id,
                    Agency__c = lstAgences[0].Id,
                    ContactId = lstContacts[0].Id,
                    Logement__c = lstLogement[0].Id,
                    Asset__c = lstAsset[0].Id,
                    Echeancier_Paiement__c = 'Comptant',
                    Sofactoapp_Mode_de_paiement__c = 'CB',
                    StartDate = System.today().addDays(Integer.valueOf(System.Label.NbDaysPriorRenewal)).addYears(-1),
                    EndDate = System.today().addDays(Integer.valueOf(System.Label.NbDaysPriorRenewal)),
                    CreatedDate = System.today(),
                    TECH_BypassBAT__c = false,
                    Contract_Status__c = 'Actif - en retard de paiement',
                    Contrat_resilie__c = false,
                    Type__c = 'Individual',
                    Pricebook2Id = lstPrcBk[0].Id,
                    sofactoapp_Rib_prelevement__c = bnkDet.Id,
                    Contrat_signe_par_le_client__c = false,
                    Tax__c = '10',
                    tech_facture_OK__c = false,
                    Contrat_Cham_Digital__c = false,
                    Contract_Renewed__c = false,
                    Frequence_de_maintenance__c = 3,
                    Type_de_frequence__c = 'Days',
                    Debut_de_la_periode_de_maintenance__c = 1,
                    Fin_de_periode_de_maintenance__c = 6,
                    Periode_de_generation__c = 5,
                    Type_de_periode_de_generation__c = 'Days',
                    Date_du_prochain_OE__c = Date.Today().addDays(7),
                    Generer_automatiquement_des_OE__c = true,
                    Horizon_de_generation__c = 4
                )
            };       
            insert lstServCon;

            //Create List of CLIs
            lstCli = new List<ContractLineItem>{
                new ContractLineItem(
                    ServiceContractId = lstServCon[0].Id,
                    PricebookEntryId = lstPrcBkEnt[0].Id,
                    AssetId = lstAsset[0].Id,
                    UnitPrice = 100,
                    Quantity = 25,
                    StartDate = Date.Today(),
                    EndDate = Date.Today().addDays(10),
                    IsActive__c = true
                ),
                new ContractLineItem(
                    ServiceContractId = lstServCon[0].Id,
                    PricebookEntryId = lstPrcBkEnt[0].Id,
                    AssetId = lstAsset[0].Id,
                    UnitPrice = 150,
                    Quantity = 30,
                    StartDate = Date.Today(),
                    EndDate = Date.Today().addDays(20),
                    IsActive__c = true
                )
            };
            insert lstCli;
        }          
    }

    static testMethod void testBatch() {
        System.runAs(adminUser){
            Test.startTest();
            BAT04_CreateServiceContract batch = new BAT04_CreateServiceContract();
            Database.executeBatch(batch);
            Test.stopTest();
        }       
    }

    static testMethod void testScheduleBatch() {
        System.runAs(adminUser){
            Test.startTest();
            BAT04_CreateServiceContract.scheduleBatch();
            Test.stopTest();
        }       
    }
}