/**
 * @File Name          : BAT15_ResilServContr.cls
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 03-26-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * ---    -----------       -------           ------------------------ 
 * 1.0    11/01/2021          LGO             Initial Version (TEC-381)
 * ---    -----------       -------           ------------------------ 
**/
global with sharing class BAT15_ResilServContr implements Database.Batchable <sObject>, Database.Stateful, Database.AllowsCallouts, Schedulable { 
    
    global String query;
    global Date execdate;
    

    // To run batch for specific date : Database.executeBatch(new BAT15_ResilServContr( Date.newInstance(2019,11,27) , null ));
    global BAT15_ResilServContr(){
        execdate = System.Today().addMonths(-6);

        system.debug('Batch 15 execdate: '+execdate);

        query = 'SELECT Id, Contrat_resilie__c, Contract_Status__c, StartDate, Payeur_du_contrat__c ';
        query +=' FROM ServiceContract WHERE StartDate <= : execdate'; // limiting records to before specificDate provided
        query +=' AND Contract_Status__c IN (\'Actif - en retard de paiement\')';

    }    

    global Database.QueryLocator start(Database.BatchableContext BC){
        system.Debug('### query: ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ServiceContract> lstServContr) {
        system.debug('### : ' + lstServContr.size() );
        
        list<ServiceContract> lstServCntEnded = new list<ServiceContract>();
        for(ServiceContract servContr : lstServContr){
            servContr.Contract_Status__c = 'Résiliation pour retard de paiement';
            servContr.Contrat_resilie__c = true;
            lstServCntEnded.add(servContr);
        }
        if(lstServCntEnded.size() >0){
           update lstServCntEnded;
        }
    }

    global void finish(Database.BatchableContext BC) {
    }

    global static String scheduleBatch() {
        BAT15_ResilServContr sc = new BAT15_ResilServContr();
        //String scheduleTime = '0 0 00 * * ?';
        //return System.schedule('Batch:', scheduleTime, scheduler);
        return System.schedule('Batch BAT15_ResilServContr:' + Datetime.now().format(),  '0 0 0 * * ?', sc);
    }

    global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT15_ResilServContr');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT15_ResilServContr(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT15_ResilServContr());
        }
    }
}