/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 10-20-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-20-2020   AMO   Initial Version
**/
public with sharing class VFC75_CompteRenduIntervention{
    
    public ServiceAppointment actualSA {get;set;}
    public string idSignature {get;set;}
    public string phone {get;set;}
    public string idSignatureTechnicien {get;set;}
    public Datetime ast_time {get;set;}
    public Datetime aet_time {get;set;}
    public string nomTechnincien {get;set;}
    public Measurement__c measurement {get;set;}
    public Boolean ImpossibletogetMeasurement {get;set;}    

    public VFC75_CompteRenduIntervention(){
        System.debug('##Starting VFC75_CompteRenduIntervention');
        
        List <ServiceAppointment> lstSA;
        string scId = ApexPages.currentPage().getParameters().get('id');
        system.debug('***ServAppId: ' + scId);
        if(String.isNotBlank(scId)){
            lstSA = new List<ServiceAppointment>([SELECT Id
            ,ServiceTerritory.Name
            ,ServiceTerritory.Logo_Name_Agency__c
            ,Account.salutation
            ,Account.firstname
            ,Account.lastname
            ,Account.Raison_sociale__c
            ,Account.BillingStreet
            ,Account.Adress_Complement__c
            ,Account.BillingPostalCode
            ,Account.BillingCity
            ,Account.Imm_Res__c
            ,Account.Door__c
            ,Account.Floor__c
            ,ServiceTerritory.Street
            ,ServiceTerritory.Street2__c
            ,ServiceTerritory.Phone__c
            ,ServiceTerritory.Email__c
            ,ServiceTerritory.Site_web__c
            ,ServiceTerritory.Legal_Form__c
            ,ServiceTerritory.Capital_in_Eur__c
            ,ServiceTerritory.APE__c
            ,ServiceTerritory.Siren__c
            ,ServiceTerritory.RCS__c
            ,ServiceTerritory.SIRET__c
            ,ServiceTerritory.Intra_Community_VAT__c
            ,ServiceTerritory.PostalCode
            ,ServiceTerritory.IBAN__c
            ,ServiceTerritory.City
            ,Account.ClientNumber__c
            ,Work_Order__r.ServiceContract.Asset__r.IDEquipmenta__c
            ,Account.Phone
            ,Account.IsPersonAccount
            ,Account.PersonMobilePhone
            ,Account.PersonHomePhone
            ,Account.PersonEmail
            ,Contact.Account.salutation
            ,Contact.Account.firstname
            ,Contact.Account.lastname
            ,Work_Order__r.ServiceContract.Numero_du_contrat__c 
            ,AppointmentNumber
            ,toLabel(Work_Order__r.WorkType.Type__c)
            ,ActualEndTime
            ,ActualStartTime
            ,Work_Order__r.Comments__c
            ,Work_Order__r.Diagnostic__c
            ,Work_Order__r.ServiceContract.Asset__r.Equipment_type_auto__c
            ,Work_Order__r.ServiceContract.Asset__r.Product2.Brand__c
            ,Work_Order__r.ServiceContract.Asset__r.Product2.Model__c
            ,Work_Order__r.ServiceContract.Asset__r.SerialNumber
            ,Work_Order__r.Impossible_to_get_measurements__c
            ,Signature_Hash_Client__c
            ,Signature_Hash_Technician__c
            ,Signature_Client_absence__c
            ,Signature_Client_not_approved__c
            ,Signature_Client_approved__c
            ,Work_Order__c
            ,Work_Order__r.Asset.IDEquipmenta__c
            ,Work_Order__r.Asset.Equipment_type_auto__c
            ,toLabel(Work_Order__r.Asset.Product2.Brand__c)
            ,Work_Order__r.Asset.Product2.Model__c
            ,Work_Order__r.Asset.SerialNumber
            
            FROM ServiceAppointment
            WHERE Id = :scId]);

            system.debug('***lstSA: ' + lstSA);
        }
        
        if(lstSA.size()>0){
            actualSA = lstSA[0];
            system.debug('***ServApp Obj: ' + actualSA);

            system.debug('***ActualStartTime');
            //ActualStarTime/ActualEndTime en timezone du user
            Datetime ast = actualSA.ActualStartTime;
            if(ast != null){
                 Integer offsetast = UserInfo.getTimezone().getOffset(ast);
                 ast_time = ast.addSeconds(offsetast/1000);
                 system.debug('***ActualStartTime1');
            }
           
            Datetime aet = actualSA.ActualEndTime;
            if(aet != null){
                Integer offsetaet = UserInfo.getTimezone().getOffset(aet);
                aet_time = aet.addSeconds(offsetaet/1000);  
                system.debug('***ActualEndTime');   
            }
               

           //Logique pour la signature
            List<Attachment> lstAttachmentClient = new List<Attachment>();
            lstAttachmentClient = [select Id,Name from Attachment where ParentId=:actualSA.Id  and  Name='SignatureClient.png' order by createdDate desc];
            if(lstAttachmentClient.size() > 0){
                idSignature = '/servlet/servlet.FileDownload?file='+lstAttachmentClient[0].id;
            }else{
                idSignature = 'null';
            }      
            List<Attachment> lstAttachmentTechnicien = new List<Attachment>();
            lstAttachmentTechnicien = [select Id,Name from Attachment where ParentId=:actualSA.Id  and  Name='SignatureTechnician.png' order by createdDate desc];
                if(lstAttachmentTechnicien.size() > 0){
                idSignatureTechnicien = '/servlet/servlet.FileDownload?file='+lstAttachmentTechnicien[0].id;
            }else{
                idSignatureTechnicien = 'null';
            } 

            system.debug('***signature');

            //Assigned Resources
            List<AssignedResource> lstAssignedResource = [select ServiceResource.Name from AssignedResource where ServiceAppointmentId =:actualSA.Id order by CreatedDate desc limit 1];
            if(lstAssignedResource.size() > 0){
                nomTechnincien =lstAssignedResource[0].ServiceResource.Name;
            }  

            if(actualSA.Work_Order__r.Impossible_to_get_measurements__c == false){
                ImpossibletogetMeasurement = false;
            }else{
                ImpossibletogetMeasurement = true;
            }

            system.debug('***Assigned Resources');

            //Phone Logic
            if(actualSA.Account.PersonMobilePhone != null){
                phone = actualSA.Account.PersonMobilePhone;
            }
            if(actualSA.Account.PersonMobilePhone == null && actualSA.Account.PersonHomePhone != null){
                phone = actualSA.Account.PersonHomePhone;
            }
            if(actualSA.Account.PersonMobilePhone == null && actualSA.Account.PersonHomePhone == null){
                phone = actualSA.Account.Phone;
            }

            system.debug('***Phone logic');

            //Relevé de mesures utiles AMO
            list<Measurement__c> lstmeasure = [SELECT Id, RecordType.Name, Parent_Work_Order__c, Tension__c, Intensite__c, Haute_pression__c, Basse_pression__c, Temp_aspiration_compresseur__c, Temp_refoulement_compresseur__c, Temperature_depart_eau__c, 
                            Temperature_retour_eau__c, Pression_du_vase_d_expansion__c ,
                            Etat_general_equipement__c, Verification_des_performances__c, Recherche_des_fuites_reseau_frigorifique__c, Coupure_interrupteur_de_proximite__c, Resserrage_des_connexions_electriques__c, Ecoulements__c,
                            Pompes_de_relevage_si_presentes__c, Vibrations__c, Bac_du_condenseur__c, Bac_de_l_evaporateur__c, Temperature_de_soufflage_unite_int__c, Fonctionnement_de_la_regulation__c, Organes_de_securite__c, Temperatures_de_fonctionnement__c,
                            Etat_du_ou_des_ventilateurs__c, Degivrages__c, Proprete_turbine_de_ventilation__c, Etat_de_l_isolant_sur_tuyauterie__c, Parametrage_de_la_regulation__c, Vitesses_de_ventilation_de_l_unite_int__c, Condenseur__c, Filtres_air_unites_interieures__c,
                            Carrosseries_de_l_unite_interieure__c, Desinfection_et_rincage_de_l_echangeur__c, Releve_des_references_et_n_de_serie__c, Pose_de_la_pastille_de_conformite__c, Etablissement_du_Cerfa_fluide__c, Niveau_huile__c, Niveau_de_fluide_frigorigene__c,
                            Purgeurs_et_vannes_de_securite__c, Eau_chaude_sanitaire__c, Filtres_eau_unites_interieures__c, Degommage_du_circulateur__c, Fermet_ouvert_de_vanne_ECS_chauffage__c,
							Temperature_eau_chaude_en_C__c, Temperature_eau_froide_en_C__c, Pression_dynamique_de_l_eau_en_bar__c, Pression_statique_de_l_eau_en_bar__c,
							Debit_litre_en_L_min__c, T_Fumee__c, CO_Ambiant_ppm__c, CO_fumees_en_ppm__c, CO2_Max_en__c, Puissance__c,
							Emission_valu_s_de_Nox__c, O2_Max__c, Pression_gaz_amont_Statique_mbar__c, Pression_gaz_amont_Dynamique_mbar__c, T_ambiant__c, T_consigne_ECS__c, Tirage_si_B1__c, 
							Rendement_PCI_Percent__c, O2_Min__c, CO2_Min_en__c, Depression_en_pascal__c, Opacite__c, Controle_visuel_de_l_anode__c, Controle_de_la_tension_de_l_anode_du_bal__c, Controle_de_l_intensite_de_l_anode_du_ba__c, Fuite__c, 
							Depression_au_niveau_du_caisson__c, Depression_Colonne_la_plus_defavorisee__c
                            FROM Measurement__c WHERE Parent_Work_Order__c = :actualSA.Work_Order__c LIMIT 1];
            measurement = lstmeasure.size()>0?lstmeasure[0]:new Measurement__c() ;
            system.debug('***After measurement '+lstmeasure[0]);

        }
    }
}