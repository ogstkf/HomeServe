/**
 * @File Name          : LC59_CloneContractCollectif.cls
 * @Description        : 
 * @Author             : CLA
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         04/06/2020               CLA                       Initial Version
**/
public with sharing class LC59_CloneContractCollectif {
    @AuraEnabled
    public static resultWrapper createNewChildContrat(String conId){
        resultWrapper result = new resultWrapper(null, null);

        Integer numDaysPrior = Integer.valueOf(System.Label.NbDaysPriorRenewalCollectifs);

        String query = BAT12_CreateServiceContractCollectifs.query;
        query += ' AND Id = \''+ conId + '\'';

        query = query.replace('SELECT ', 'SELECT TECH_BypassBAT__c, TECH_NumberOfDaysPriorEndDate__c, ');
        query = query.replace('TECH_BypassBAT__c = false  AND', '');

        System.debug('query: ' + query);

        List<ServiceContract> lstOldContract = Database.query(query);

        if(lstOldContract.size() > 0){
            if(lstOldContract[0].TECH_NumberOfDaysPriorEndDate__c > numDaysPrior){
                result.msg = 'Nouveau contrat ne peut etre créer car le TECH_NumberOfDaysPriorEndDate__c est plus grand que '+ numDaysPrior;
            }
            else if(lstOldContract[0].TECH_BypassBAT__c){
                result.msg = 'Nouveau contrat déjà créer';
            }
            else{
                List<ServiceContract> lstNewContracts = BAT12_CreateServiceContractCollectifs.createNewContracts(lstOldContract);

                if(lstNewContracts.size() > 0){
                    result.msg = 'success';
                    result.conId = lstNewContracts[0].Id;
                }
                else{
                    result.msg = 'Aucun nouveau contrat créé';
                }
            }
        }
        else{
            result.msg = 'Nouveau contrat ne peut etre créer';
        }
        return result;
    }

    public class resultWrapper{
        @AuraEnabled public String msg {get; set;}
        @AuraEnabled public String conId {get;set;}

        public resultWrapper(String msg, String conId){
            this.msg = msg;
            this.conId = conId;
        }
    } 
}