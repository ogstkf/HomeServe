/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-16-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-16-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class CreateUpdateMetadataUtils {
    public static void handleResult(Metadata.DeployResult result, Metadata.DeployCallbackContext context) {
        if (result.status == Metadata.DeployStatus.SUCCEEDED) {
            System.debug('success : ' + result);
        }
        else {
            System.debug('fail : ' + result);
        }
    }
    public static void CreateUpdateMetadata(/*String fullname, Map<String, Object> fieldWithValuesMap*/) {
        /*Metadata.CustomMetadata cstmdt = new Metadata.CustomMetadata();
        cstmdt.fullname = fullname;

        for (String key: fieldWithValuesMap.keySet()) {
            Metadata.CustomMetadataValue cstField = new Metadata.CustomMetadataValue();
            cstField.field = key;
            cstField.value = fieldWithValuesMap.get(key);
            cstmdt.values.add(cstField);
        }

        Metadata.DeployContainer mdtContainer = new Metadata.DeployContainer();
        mdtContainer.addMetadata(cstmdt);
        CreateUpdateMetadataUtils callback = new CreateUpdateMetadataUtils();
        Id jobId = Metadata.Operations.enqueueDeployment(mdtContainer, callback);
        */
        Metadata.DeployContainer mdContainer = new Metadata.DeployContainer(); 
        Metadata.CustomMetadata mdt = new Metadata.CustomMetadata();
        mdt.fullName = 'WebServiceConfiguration__mdt.ChaineEditiquePOST';
        mdt.label = 'Chaine Editique (post)';
        Metadata.CustomMetadataValue customFieldtoUpdate = new Metadata.CustomMetadataValue();
        customFieldtoUpdate.field = 'documentId__c';
        customFieldtoUpdate.value = 8;
        mdt.values.add(customFieldtoUpdate);
        mdContainer.addMetadata(mdt);
        system.debug('mdContainer**'+mdContainer);
        Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);
        System.debug('jobId=' +  jobId);
    }
}