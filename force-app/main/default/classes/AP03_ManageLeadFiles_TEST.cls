/**
 * @File Name          : AP03_ManageLeadFiles_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 09-08-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/01/2020   RRJ     Initial Version
**/
@isTest
public with sharing class AP03_ManageLeadFiles_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : 
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 28-JAN-2019  YGO    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/

	static User mainUser;
	static List<ContentDocumentLink> lstCdl = new List<ContentDocumentLink>();
	static List<ContentVersion> lstConVer = new List<ContentVersion>();
    static List<ServiceAppointment> lstServiceApp;
    static List<Account> lstAcc;
    static List<Asset> lstTestAssset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();

    static HttpCalloutMock mock = new MultiRequestMock(TestFactory.multiMockMap('https://preprod-cham.hellocasa.io'));
    static SingleRequestMock mockError = new SingleRequestMock(400, 'Error', '', null); 
    static SingleRequestMock mockUnexpected = new SingleRequestMock(200, 'Success', '', null);

	static{
		mainUser = TestFactory.createAdminUser('AP03_ManageLeadFiles_TEST@test.COM', 
												TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){   
            
            List<WebServiceConfiguration__mdt> blueConnect = new List<WebServiceConfiguration__mdt>([SELECT Id
                                                                                              ,Endpoint__c
                                                                                        FROM WebServiceConfiguration__mdt
                                                                                        WHERE DeveloperName = 'Blue_UpdateStatus' AND
                                                                                              Active__c = True]);
            if(blueConnect.size() > 0)
            mock = new MultiRequestMock(TestFactory.multiMockMap(blueConnect[0].Endpoint__c));  	

            lstAcc = new List<Account>{new Account(Name='test', 
                                        ClientCreeSurSAV__c = true,
                                        Client_doesnt_have_mail__c=true)
            };
            insert lstAcc;

            Logement__c lo = TestFactory.createLogement('Lo_Test', 'test01.sc@mauritius.com', 'test_lname');
            lo.Blue_Order_Id__c = '9711';             
            insert lo;

            //creating products
            lstTestProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstTestProd.add(TestFactory.createProduct('Entretien gaz'));
            
            insert lstTestProd;

            // creating Assets
            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lo.Id);
            eqp.Product2Id = lstTestProd[0].Id;
            eqp.Logement__c = lo.Id;
            eqp.AccountId = lstAcc[0].Id;
            lstTestAssset.add(eqp);
            insert lstTestAssset;

            WorkType wrkType1 = TestFactory.createWorkType('wrkType1','Hours',1);
            wrkType1.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            insert wrkType1;
            
            case cse = new case(AccountId = lstAcc[0].id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id);
            insert cse;

            WorkOrder wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse.id;
            wrkOrd.AccountId = lstAcc[0].Id;
            wrkOrd.WorkTypeId = wrkType1.Id;
            insert wrkOrd;

            lstServiceApp = new List<ServiceAppointment>{
                                new ServiceAppointment(
                                        Status = 'Done OK',
                                        Subject = 'this is a test subject',
                                        ParentRecordId = lstAcc[0].Id,
                                        EarliestStartTime = System.now(),
                                        DueDate = System.now() + 30,                                        
                                        Category__c = AP_Constant.ServAppCategoryVisiteEntr     ,
                                        ActualEndTime = Date.Today(),
                                        Work_order__c = wrkOrd.Id                                   
                                )
            };
            insert lstServiceApp;
            
  			lstConVer = new List<ContentVersion>{
                                            new ContentVersion(
                                                        Title = 'testFile.pdf', 
                                                        Description = 'This is a test class file',
                                                        OwnerId = mainUser.Id,
                                                        PathOnClient = 'testFile.pdf',
                                                        VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body')
                                            )
            };
            insert lstConVer;
        }
	}	

	@isTest
	public static void callWSBlue_test(){
		System.runAs(mainUser){
            ServiceAppointmentTriggerHandler.isTest = false;
            Test.setMock(HttpCalloutMock.class, mock);
			ContentVersion cv = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :lstConVer[0].Id];
            
            Map <String, ContentDocumentLink> mapLeadContentDocLink = new Map<String, ContentDocumentLink>();
            
            lstCdl = new List<ContentDocumentLink>{
                new ContentDocumentLink(ContentDocumentId = cv.ContentDocumentId,
                                          ShareType = 'V',
                                          Visibility ='AllUsers',
                                          LinkedEntityId = lstServiceApp[0].Id)
                                                            
                                            
            };    
            insert lstCdl;
            mapLeadContentDocLink.put(lstCdl[0].LinkedEntityId, lstCdl[0]);

            Test.startTest();
              AP03_ManageLeadFiles.callWSBlue(mapLeadContentDocLink);
			Test.stopTest();
		}
	}
}