public with sharing class AP19_CreateShipmentOrPT {
    /**
 * @File Name          : AP19_CreateShipmentOrPT_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         15-10-2019           ANA         Initial Version (CT-1085)
**/

    public static void checkQuantityReceived(list<Shipment> lstShip){

        list<ProductTransfer> lstProT = [SELECT Quantity_lot_sent__c, Quantity_Lot_received__c, Id,Product2Id,SourceLocationId, Commande__c, DestinationLocationId,  QuantityReceived, QuantitySent, ShipmentId FROM ProductTransfer WHERE ShipmentId IN :lstShip];
        list<ProductTransfer> lstNewProT = new list<ProductTransfer>();
        Map<Id,Shipment> mapIdShipInsert = new Map<Id,Shipment>();
        Map<Id, Order> mapOrderToUpdate = new Map<Id, Order>();

        Map<Id,list<ProductTransfer>> mapShipToProT = new Map<Id,list<ProductTransfer>>();
        List<Order> lstOrdersToUpdate = new List<Order>();

        for(ProductTransfer ProT : lstProT){
            if(mapShipToProT.ContainsKey(ProT.ShipmentId))
            {
                mapShipToProT.get(ProT.ShipmentId).add(ProT);
            }
            else{
                mapShipToProT.put(ProT.ShipmentId,new List<ProductTransfer>{ProT});
            }
        }

        for(Shipment Sh : lstShip){

            for(ProductTransfer Pro : mapShipToProT.get(Sh.Id)){
                
                if(Pro.QuantityReceived == null){
                    Sh.addError(label.QuantityReceivedNull);
                    if(mapIdShipInsert.containsKey(Sh.Id)){
                        mapIdShipInsert.remove(Sh.Id);
                    }
                    break;
                }else if(Pro.QuantityReceived < Pro.QuantitySent){
                    if(!mapIdShipInsert.containsKey(Sh.Id)){
                        Shipment Ship = new Shipment(Order__c = Sh.Order__c,
                                    SourceLocationId = Sh.SourceLocationId,
                                    DestinationLocationId = Sh.DestinationLocationId,
                                    ShipToName = Sh.ShipToName,
                                    DeliveredToId = Sh.DeliveredToId,
                                    ExpectedDeliveryDate = Sh.TECH_OrderExpectedDeliveryDate__c  != null ? Datetime.newInstance(Sh.TECH_OrderExpectedDeliveryDate__c, Time.newInstance(14,0,0,0)) : null
                                    );
                        

                        mapIdShipInsert.put(Sh.Id,Ship);
                    }

                    mapOrderToUpdate.put(
                        Sh.Order__c,
                        new Order(Id = Sh.Order__c, 
                            Status = AP_Constant.ordStatusReceptPartiellement
                        )
                    );
                }
            }
        }

        System.debug('mapIdShipInsert AN : '+mapIdShipInsert);

        if(mapIdShipInsert.size()>0){
            System.debug('## mapIdShipment values: ' + mapIdShipInsert.values());
            insert mapIdShipInsert.values();

            for(Integer i = 0 ; i < lstShip.size(); i++){

                // Update product transfers START
                list<ProductTransfer> lstPro = mapShipToProT.get(lstShip[i].Id);
                if(lstPro.size()>0){
                    for(ProductTransfer Pro : lstPro){
                        if(Pro.Quantity_Lot_received__c < Pro.Quantity_lot_sent__c){
                            lstNewProT.add(
                                new ProductTransfer(shipmentId=mapIdShipInsert.get(lstShip[i].Id).Id , Commande__c = Pro.Commande__c
                                                    ,SourceLocationId=Pro.SourceLocationId , DestinationLocationId=Pro.DestinationLocationId
                                                    ,Quantity_lot_sent__c=Pro.Quantity_lot_sent__c-Pro.Quantity_Lot_received__c  
                                                    ,Product2Id=Pro.Product2Id
                                                    ,QuantitySent = Pro.QuantitySent - Pro.QuantityReceived
                                                    //29/01/2021 - BCH - TEC-451 - Set by default the Quantity Lot Received = QuantitySent (lot) - Quantity Received (lot) of the previous product transfer record
                                                    ,Quantity_lot_received__c = Pro.Quantity_lot_sent__c-Pro.Quantity_Lot_received__c                                                   
                                                    )
                                );
                            
                        }
                    }
                }
                // update product transferts END
            }

            if(lstNewProT.size()>0){
                insert lstNewProT;
            }

            if(!mapOrderToUpdate.isEmpty()){
                update mapOrderToUpdate.values();
            }   
        }
    }

    public static void updateQuantitySentReceived(List<ProductTransfer> lstPts){
        
        
        for(ProductTransfer pts: lstPts){
            
            if(pts.Quantity_Lot_received__c != null){
                pts.QuantityReceived = pts.TECH_ProductLot__c != null ? pts.Quantity_Lot_received__c * pts.TECH_ProductLot__c : pts.Quantity_Lot_received__c; 
            }

            if(pts.Quantity_lot_sent__c != null){
                pts.QuantitySent =pts.TECH_ProductLot__c != null ? pts.Quantity_lot_sent__c * pts.TECH_ProductLot__c : pts.Quantity_lot_sent__c;
            }
        }
    }

}