/**
 * @File Name          : AP15_AttachCasePdfToAttachment_TEST.cls
 * @Description        : 
 * @Author             : ANA
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    01/10/2019   ANA     Initial Version
**/
@isTest
public with sharing class AP15_AttachCasePdfToAttachment_TEST {

    static User mainUser;
    static Contact con;
    static Account acc;
    static List<Case> lstCase;
    static Bypass__c bp = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('AP14_SetCaseStatus@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP16,WorkOrderTrigger';
        bp.BypassValidationRules__c = True;
        insert bp;


        System.runAs(mainUser){

            acc = new Account(
                RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='John');
            insert acc;

            con = new Contact(LastName='TestLast',AccountId = acc.Id);

            insert con;

            lstCase = new List<Case>{
                new Case(AccountId=acc.Id , ContactId=con.Id,Type='A1 - Logement')
                , new Case(AccountId=acc.Id , ContactId=con.Id,Type='A2 Equipement')
                , new Case(AccountId=acc.Id , ContactId=con.Id,Type='DGI Equipement')
            };
        }
    }

    @isTest
    public static void AttachToCaseTest(){
        System.runAs(mainUser){
            Test.startTest();
            insert lstCase;
            Test.stopTest();
            List<Attachment> lstAtt = [SELECT Id FROM Attachment WHERE ParentId=:lstCase[0].Id];
            System.assertEquals(lstAtt.size(),1);
        }
    }

}