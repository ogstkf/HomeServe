/**
 * @File Name          : AP10_GenerateCompteRenduPDF_Test.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : MNA
 * @Last Modified On   : 08-06-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/1/2019, 2:00:09 PM   ARA     Initial Version
**/
@isTest
public with sharing class AP10_GenerateCompteRenduPDF_Test {
    static User adminUser;
    
    static OperatingHours opHrs = new OperatingHours();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static OrgWideEmailAddress orgWEmail = new OrgWideEmailAddress();

    static List<Account> testLstAcc = new List<Account>();
    static List<Contact> testLstCon = new List<Contact>();
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAsset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<Case> lstTestCas = new List<Case>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<Attachment> lstAttachment = new List<Attachment>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<ServiceTerritory> lstAgence = new List<ServiceTerritory>();
    static List<OperatingHours> lstOpHrs;
    static List<Logement__c> lstLogement = new List<Logement__c>();

    static Bypass__c bp = new Bypass__c();
    static {
        adminUser = TestFactory.createAdminUser('AP10_GenerateCompteRenduPDF_Test@test.COM', TestFactory.getProfileAdminId());
		insert adminUser;
        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP14,AP16,WorkOrderTrigger,MobileNotificationsTrigger,AssignedResourceTrigger';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(adminUser){

            //creating accounts
            testLstAcc.add(TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test'));
            testLstAcc.add(TestFactory.createAccountBusiness('AP10_GenerateCompteRenduPDF_Test2'));

            testLstAcc[0].BillingPostalCode = '1233456';
            testLstAcc[0].PersonEmail = 'test@gmail.com'; 
            testLstAcc[0].recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testLstAcc[1].recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();

            insert testLstAcc;

            //Creating contact
            testLstCon = new List<Contact>{
                new Contact(LastName = 'test Contact', Contact_principal__c = true, Email = 'test2@gmail.com')
            };
            insert testLstCon;

            //Creating OperatingHours
            lstOpHrs = new List<OperatingHours>{
                            new OperatingHours(Name='op')
            };
            insert lstOpHrs;

            //Creating Raison Sociale
            Sofactoapp__Raison_Sociale__c raisonSoc = TestFactory.createSofactoapp_Raison_Sociale('test raison Soc');
            insert raisonSoc;

            //creating ServiceTerritory
            lstAgence = new List<ServiceTerritory>{
                    new ServiceTerritory(Name='test', OperatingHoursId = lstOpHrs[0].Id, IsActive = true, Sofactoapp_Raison_Social__c = raisonSoc.Id, Email__c = 'contact@sbf-energies.com')
            };
            insert lstAgence;

            //creating logement
            for(integer i = 0; i < 2 ; i++){
                Logement__c lgmt = TestFactory.createLogement(testLstAcc[0].Id, lstAgence[0].Id);
                lgmt.Postal_Code__c = 'lgmtPC ' + i;
                lgmt.City__c = 'lgmtCity ' + i;
                lgmt.Account__c = testLstAcc[0].Id;
                lgmt.Inhabitant__c = testLstAcc[0].Id;
                lstTestLogement.add(lgmt);
            }
            insert lstTestLogement;

            //creating products
            lstTestProd = new List<Product2> {
                new Product2(Name = 'Product X1',
                                ProductCode = 'Pro-X1',
                                isActive = true,
                                Equipment_family__c = 'Chaudière',
                                Equipment_type__c = 'Chaudière gaz',
                                Statut__c = 'Approuvée',
                                Equipment_type1__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz
                ),
                new Product2(Name = 'Product X2',
                                ProductCode = 'Pro-X2',
                                isActive = true,
                                Equipment_family__c = 'Chaudière',
                                Equipment_type__c = 'Chaudière gaz',
                                Statut__c = 'Approuvée',
                                Equipment_type1__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz
                )/*,
                new Product2(Name = 'Product X3',
                                ProductCode = 'Pro-X3',
                                isActive = true,
                                Equipment_family__c = 'Pompe à chaleur',
                                Statut__c = 'Approuvée',
                                Equipment_type1__c = 'Pompe à chaleur air/eau'
                )*/
            };
            insert lstTestProd;
            
            // creating Assets
            for(Integer i = 0; i < 2; i++){
                Asset eqp = TestFactory.createAccount('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[i].Id);
                eqp.Product2Id = lstTestProd[i].Id;
                eqp.Logement__c = lstTestLogement[i].Id;
                eqp.AccountId = testLstAcc[0].Id;
                lstTestAsset.add(eqp);
            }
            insert lstTestAsset;

            
            // create case
            lstTestCas = new List<Case>{
                new case(AccountId = testLstAcc[0].id, type = 'Commissioning', AssetId = lstTestAsset[0].Id),
                new case(AccountId = testLstAcc[1].id, type = 'Commissioning', AssetId = lstTestAsset[0].Id)
            };
            insert lstTestCas;
            
            // Create WorkType
            lstWrkTyp.add(TestFactory.createWorkType('wrktypeGaz','Hours',1));
            lstWrkTyp.add(TestFactory.createWorkType('wrktypeGaz2','Hours',3));
            lstWrkTyp.add(TestFactory.createWorkType('wrktypeGaz3','Hours',3));
            lstWrkTyp.add(TestFactory.createWorkType('wrktypeFioul1','Hours',3));
            lstWrkTyp.add(TestFactory.createWorkType('wrktypeFioul2','Hours',3));
            lstWrkTyp.add(TestFactory.createWorkType('wrktypeFioul3','Hours',3));
            lstWrkTyp.add(TestFactory.createWorkType('wrkPompechaleur1','Hours',3));
            lstWrkTyp.add(TestFactory.createWorkType('wrkPompechaleur2','Hours',3));

            lstWrkTyp[0].Type__c = 'Maintenance';
            lstWrkTyp[0].Type_de_client__c = 'Tous';
            lstWrkTyp[0].Equipment_type__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz;
            lstWrkTyp[0].Agence__c = 'Toutes';
            
            lstWrkTyp[1].Type__c = 'Commissioning';
            lstWrkTyp[1].Type_de_client__c = 'Tous';
            lstWrkTyp[1].Equipment_type__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz;
            lstWrkTyp[1].Agence__c = 'Toutes';

            lstWrkTyp[2].Type__c = 'Information Request';
            lstWrkTyp[2].Type_de_client__c = 'Tous';
            lstWrkTyp[2].Equipment_type__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz;
            lstWrkTyp[2].Agence__c = 'Toutes';
            
            lstWrkTyp[3].Type__c = 'Maintenance';
            lstWrkTyp[3].Type_de_client__c = 'Tous';
            lstWrkTyp[3].Equipment_type__c = 'Chaudière fioul';
            lstWrkTyp[3].Agence__c = 'Toutes';
            
            lstWrkTyp[4].Type__c = 'Commissioning';
            lstWrkTyp[4].Type_de_client__c = 'Tous';
            lstWrkTyp[4].Equipment_type__c = 'Chaudière fioul';
            lstWrkTyp[4].Agence__c = 'Toutes';
            
            lstWrkTyp[5].Type__c = 'Information Request';
            lstWrkTyp[5].Type_de_client__c = 'Tous';
            lstWrkTyp[5].Equipment_type__c = 'Chaudière fioul';
            lstWrkTyp[5].Agence__c = 'Toutes';
            
            lstWrkTyp[6].Type__c = 'Maintenance';
            lstWrkTyp[6].Type_de_client__c = 'Tous';
            lstWrkTyp[6].Equipment_type__c = 'Pompe à chaleur air/eau';
            lstWrkTyp[6].Agence__c = 'Toutes';
            
            lstWrkTyp[7].Type__c = 'Information Request';
            lstWrkTyp[7].Type_de_client__c = 'Tous';
            lstWrkTyp[7].Equipment_type__c = 'Pompe à chaleur air/eau';
            lstWrkTyp[7].Agence__c = 'Toutes';

            insert lstWrkTyp;
            System.debug('lstWrkTyp ' + lstWrkTyp);

            lstWrkOrd = new List<WorkOrder>{
                new WorkOrder(Type__c = 'Maintenance', caseId = lstTestCas[0].id, WorkTypeId = lstWrkTyp[0].Id, AssetId = lstTestAsset[0].Id),
                new WorkOrder(Type__c = 'Commissioning', caseId = lstTestCas[1].id, WorkTypeId = lstWrkTyp[1].Id, AssetId = lstTestAsset[1].Id),
                new WorkOrder(Type__c = 'Information Request', caseId = lstTestCas[0].id, WorkTypeId = lstWrkTyp[2].Id, AssetId = lstTestAsset[1].Id),
                new WorkOrder(Type__c = 'Maintenance', caseId = lstTestCas[0].id, WorkTypeId = lstWrkTyp[3].Id, AssetId = lstTestAsset[0].Id),
                new WorkOrder(Type__c = 'Commissioning', caseId = lstTestCas[1].id, WorkTypeId = lstWrkTyp[4].Id, AssetId = lstTestAsset[1].Id),
                new WorkOrder(Type__c = 'Information Request', caseId = lstTestCas[0].id, WorkTypeId = lstWrkTyp[5].Id, AssetId = lstTestAsset[1].Id),
                new WorkOrder(Type__c = 'Maintenance', caseId = lstTestCas[0].id, WorkTypeId = lstWrkTyp[6].Id, AssetId = lstTestAsset[0].Id),
                new WorkOrder(Type__c = 'Information Request', caseId = lstTestCas[0].id, WorkTypeId = lstWrkTyp[7].Id, AssetId = lstTestAsset[1].Id)
            };
            
            insert lstWrkOrd;
            

            //creating Service Appointment
                           
            lstServiceApp = new List<ServiceAppointment>{
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Work_Order__c = lstWrkOrd[0].Id,
                    Residence__c =   lstTestLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done OK'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[1].Id,
                    Work_Order__c = lstWrkOrd[1].Id,
                    Residence__c =   lstTestLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done OK',
                    ContactId = testLstCon[0].Id
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Work_Order__c = lstWrkOrd[0].Id,
                    Residence__c =   lstTestLogement[1].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done KO'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[2].Id,
                    Work_Order__c = lstWrkOrd[2].Id,
                    Residence__c =   lstTestLogement[1].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done KO'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[3].Id,
                    Work_Order__c = lstWrkOrd[3].Id,
                    Residence__c =   lstTestLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done OK'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[3].Id,
                    Work_Order__c = lstWrkOrd[3].Id,
                    Residence__c =   lstTestLogement[1].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done KO'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[4].Id,
                    Work_Order__c = lstWrkOrd[4].Id,
                    Residence__c =   lstTestLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done OK',
                    ContactId = testLstCon[0].Id
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[5].Id,
                    Work_Order__c = lstWrkOrd[5].Id,
                    Residence__c =   lstTestLogement[1].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done KO'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[6].Id,
                    Work_Order__c = lstWrkOrd[6].Id,
                    Residence__c =   lstTestLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done OK'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[6].Id,
                    Work_Order__c = lstWrkOrd[6].Id,
                    Residence__c =   lstTestLogement[1].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done KO'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[7].Id,
                    Work_Order__c = lstWrkOrd[7].Id,
                    Residence__c =   lstTestLogement[1].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstAgence[0].Id,
                    Signature_Client__c = 'testsignature',
                    Signature_Technician__c = 'testsignature',
                    status = 'Done KO'
                )
            };
            insert lstServiceApp;
        }
    }

    @isTest static void createServiceAppointmentFilesTest1() {                                  //PersonAccount with Email 
                                                                                                //Chaudière gaz Maintenance Done OK
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[0].Tech_Generate_Service_Report__c = true;
            lstServiceApp[0].SchedEndTime = lstServiceApp[0].SchedEndTime.addDays(1);
            update lstServiceApp[0];
            Test.stopTest();
        }
    }

    @isTest static void createServiceAppointmentFilesTest2() {                                  //PersonAccount without Email
                                                                                                //Chaudière gaz Maintenance Done OK
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            testLstAcc[0].PersonEmail = null;
            update testLstAcc;
            lstServiceApp[0].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[0];
            Test.stopTest();
        }
    }
    
    @isTest static void createServiceAppointmentFilesTest3() {                                  //BusinessAccount with Email
                                                                                                //Chaudière gaz Commissioning 
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[1].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[1];
            Test.stopTest();
        }
    }
    
    @isTest static void createServiceAppointmentFilesTest4() {                                  //BusinessAccount without Email
                                                                                                //Chaudière gaz Commissioning 
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[1].ContactId = null;
            lstServiceApp[1].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[1];
            Test.stopTest();
        }
    }

    @isTest static void createServiceAppointmentFilesTest5() {                                  //Chaudière gaz Maintenance Done KO
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[2].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[2];
            Test.stopTest();
        }
    }
    
    @isTest static void createServiceAppointmentFilesTest6() {                                  //Chaudière gaz Autres
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[3].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[3];
            Test.stopTest();
        }
    }
    
    @isTest static void createServiceAppointmentFilesTest7() {                                  //Chaudière fioul Maintenance Done OK
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[4].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[4];
            Test.stopTest();
        }
    }

    @isTest static void createServiceAppointmentFilesTest8() {                                  //Chaudière fioul Maintenance Done KO
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[5].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[5];
            Test.stopTest();
        }
    }

    @isTest static void createServiceAppointmentFilesTest9() {                                  //Chaudière fioul Commissioning
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[6].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[6];
            Test.stopTest();
        }
    }

    @isTest static void createServiceAppointmentFilesTest10() {                                 //Chaudière fioul Autres
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[7].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[7];
            Test.stopTest();
        }
    }

    @isTest static void createServiceAppointmentFilesTest11() {                                  //Pompe à chaleur Maintenance Done OK
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[8].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[8];
            Test.stopTest();
        }
    }

    @isTest static void createServiceAppointmentFilesTest12() {                                  //Pompe à chaleur Maintenance Done KO
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[9].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[9];
            Test.stopTest();
        }
    }

    @isTest static void createServiceAppointmentFilesTest13() {                                  //Pompe à chaleur Commissioning
        System.runAs(adminUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            lstServiceApp[10].Tech_Generate_Service_Report__c = true;
            update lstServiceApp[10];
            Test.stopTest();
        }
    }

    public class calloutStatusPrinted implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"printed"}');
            response.setStatusCode(200);
            return response; 
        }
    }
    
    public class calloutStatusEmitted implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"emitted"}');
            response.setStatusCode(200);
            return response; 
        }
    }
}