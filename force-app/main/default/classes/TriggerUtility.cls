public with sharing class TriggerUtility {
    public static boolean hasChanged(Sobject newSobject, Sobject oldSobject, string fieldName){
        return (newSobject.get(fieldName)!= oldSobject.get(fieldName));
    }
}