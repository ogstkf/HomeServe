/**
* @File Name          : AP50_GestionRemise.cls
* @Description        : 
* @Author             : Spoon Consulting (ANA)
* @Group              : 
* @Last Modified By   : LGO
* @Last Modified On   : 07-01-2021
* @Modification Log   : 
*==============================================================================
* Ver         Date                     Author      Modification
*==============================================================================
* 1.0         08-12-2019            ANA         Initial Version
**/
public with sharing class AP50_GestionRemise {
    
    public static List<QuoteLineItem> getRemises(List<String> lstQuoId){
        return GestionRemise([SELECT Id, Contrat_de_service__c FROM Quote WHERE Id IN: lstQuoId AND Quote.RecordType.DeveloperName = 'Devis_standard']);
    }
    
    public static List<QuoteLineItem> getRemises(List<Quote> lstQuo){
        return GestionRemise([SELECT Id, Contrat_de_service__c FROM Quote WHERE Id IN: lstQuo AND Quote.RecordType.DeveloperName = 'Devis_standard']);
    }
    
    public static list<QuoteLineItem> GestionRemise(list<Quote> lstQuo){
        
        List<QuoteLineItem> lstQLI = [SELECT Product2Id, PriceBookEntryId, QuoteId, UnitPrice FROM QuoteLineItem WHERE QuoteId IN:lstQuo AND Quote.RecordType.DeveloperName = 'Devis_standard'];
        
        Map<String, List<QuoteLineItem>> mapQuoProd = new Map<String, List<QuoteLineItem>>();
        for(QuoteLineItem qli : lstQLI){
            if(mapQuoProd.containsKey(qli.QuoteId)){
                mapQuoProd.get(qli.QuoteId).add(qli);
            }else{
                mapQuoProd.put(qli.QuoteId, new List<QuoteLineItem>{qli});
            }
        }
        
        Map<String, Map<String, QuoteLineItem>> mapRemises = getRemiseQuoQli(mapQuoProd);
        
        for(QuoteLineItem qli: lstQLI){
            if(mapRemises.containsKey(qli.QuoteId)){
                if(mapRemises.get(qli.QuoteId).containsKey(qli.Product2Id)){
                    qli.Remise_en100__c = mapRemises.get(qli.QuoteId).get(qli.Product2Id).Remise_en100__c;
                    qli.Remise_en_euros__c = mapRemises.get(qli.QuoteId).get(qli.Product2Id).Remise_en_euros__c;
                }else{
                    qli.Remise_en100__c = null;
                    qli.Remise_en_euros__c = null;
                }
            }
        }
        
        return lstQLI;
    }
    
    public static Map<String, Map<String, QuoteLineItem>> getRemiseQuoQli(Map<String, List<QuoteLineItem>> mapQuoIdToQli){
        Map<Id, Quote> mapQuo = new Map<Id, Quote>([SELECT Id, Contrat_de_service__c, Contrat_de_service__r.Type__c, Type_de_devis__c, Equipement__r.InstallDate, AccountId, Pricebook2Id, Equipement__c FROM Quote WHERE Id IN: mapQuoIdToQli.keySet() AND Quote.RecordType.DeveloperName = 'Devis_standard']);
        
        Set<Id> setSrvConId = new Set<Id>();
        Set<Id> setAstId = new Set<Id>();
        for(Quote quo: mapQuo.values()){
            setSrvConId.add(quo.Contrat_de_service__c);
            setAstId.add(quo.Equipement__c);
        }
        
        List<ContractLineItem> lstCLI = [SELECT Id, 
                                         ServiceContractId, 
                                         ServiceContract.Type__c, 
                                         ServiceContract.RootServiceContractId, 
                                         ServiceContract.RootServiceContract.Option_P2_avec_seuil__c, 
                                         ServiceContract.RootServiceContract.Type_de_seuil__c, 
                                         ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c, 
                                         Product2Id, 
                                         Product2.Name,
                                         Product2.ProductCode, 
                                         Product2.RecordTypeId, 
                                         Product2.RecordType.DeveloperName, 
                                         Product2.Famille_d_articles__c, 
                                         PricebookEntry.UnitPrice,
                                         Collective_Account__c,
                                         Asset.AccountId,
                                         IsActive__c,
                                         AssetId,
                                         Remplacement_effectue__c
                                         FROM ContractLineItem 
                                         WHERE ServiceContractId IN: setSrvConId
                                         AND IsActive__c = true
                                         AND AssetId IN :setAstId
                                         // AND Product2.ProductCode != null
                                         AND (ServiceContract.Contract_Status__c = 'Active' 
                                              OR ServiceContract.Contract_Status__c = 'Pending first visit' 
                                              OR ServiceContract.Contract_Status__c = 'Actif - en retard de paiement') 
                                         AND ServiceContract.StartDate <= TODAY 
                                         AND ServiceContract.EndDate >= TODAY];
        
        Map<Id, List<ContractLineItem>> mapSrvConConLine = new Map<Id, List<ContractLineItem>>();
        for(ContractLineItem CLI : lstCLI){
            if(mapSrvConConLine.containsKey(cli.ServiceContractId)){
                mapSrvConConLine.get(cli.ServiceContractId).add(cli);
            }else{
                mapSrvConConLine.put(cli.ServiceContractId, new List<ContractLineItem>{cli});
            }
        }
        
        List<String> lstPbeId = new List<String>();
        for(String quoId : mapQuoIdToQli.keySet()){
            for(QuoteLineItem qli: mapQuoIdToQli.get(quoId)){
                lstPbeId.add(qli.PriceBookEntryId);
            }
        }
        
        List<PricebookEntry> lstPbe = [SELECT Id, Product2Id, Pricebook2Id, UnitPrice, Product2.ProductCode, Product2.RecordTypeId, Product2.Famille_d_articles__c, Product2.RecordType.DeveloperName, Product2.Piece_d_usure__c, Product2.Name FROM PricebookEntry WHERE Id IN :lstPbeId];
        Map<String, PriceBookEntry> mapPbe = new Map<String, PriceBookEntry>();
        for(PriceBookEntry pbe: lstPbe){
            mapPbe.put(pbe.Product2Id +'-'+ pbe.Pricebook2Id, pbe);
        }
        
        Map<Id, PriceBookEntry> mapIdPbe = new Map<Id, PriceBookEntry>(lstPbe);
        
        Set<String> setQuoP3r = new Set<String>();
        Map<String, Map<String, QuoteLineItem>> mapRemisesQLI = new Map<String, Map<String, QuoteLineItem>>();
        System.debug('### OK1');
        for(String quoId : mapQuoIdToQli.keySet()){
            if(mapQuo.containsKey(quoId)){
                Quote quo = mapQuo.get(quoId);
                for(QuoteLineItem qli : mapQuoIdToQli.get(quoId)){
                    if(mapIdPbe.containsKey(qli.PriceBookEntryId)){
                        System.debug('### OK2');
                        PricebookEntry pbe = mapIdPbe.get(qli.PriceBookEntryId);
                        if(quo.Contrat_de_service__c != null && mapSrvConConLine.containsKey(quo.Contrat_de_service__c)){
                            System.debug('### OK3');
                            for(ContractLineItem cli: mapSrvConConLine.get(quo.Contrat_de_service__c)){
                                if(cli.IsActive__c && cli.Asset.AccountId == quo.AccountId && cli.AssetId == quo.Equipement__c){
                                    System.debug('### OK4');
                                    if(quo.Contrat_de_service__r.Type__c == 'Collective'){
                                        if(cli.Product2.ProductCode == 'P3R' && cli.IsActive__c && quo.Type_de_devis__c == 'Vente déquipement / Pose' && cli.AssetId == quo.Equipement__c && cli.Remplacement_effectue__c == false){
                                            setQuoP3r.add(quoId);
                                            break;
                                        }else if(
                                            (cli.Product2.ProductCode == 'COUV-MO-GAR-CHAM' && cli.IsActive__c && (pbe.Product2.ProductCode == 'MO' || pbe.Product2.ProductCode == 'FORFAIT-MO' || pbe.Product2.ProductCode == 'MO-SAMEDI'))
                                            || (cli.Product2.ProductCode == 'COUV-DEPL-GAR-CHAM' && cli.IsActive__c && pbe.Product2.ProductCode == 'DEPL-ZONE')
                                            || (cli.Product2.Name != null && cli.Product2.Name.contains('P2') && cli.IsActive__c && pbe.Product2.RecordType.DeveloperName == 'Article' && pbe.Product2.Famille_d_articles__c == 'Consommables')
                                            || (cli.Product2.Name != null && cli.Product2.ProductCode == 'P3' && cli.IsActive__c && pbe.Product2.RecordType.DeveloperName == 'Article' && pbe.Product2.Famille_d_articles__c == 'Pièces détachées' && pbe.Product2.Piece_d_usure__c == false)
                                        ){  
                                            System.debug('### OK');

                                            System.debug('### mapRemisesQLIQuoId : '+ mapRemisesQLI.containsKey(quoId));

                                            if(mapRemisesQLI.containsKey(quoId)){
                                                mapRemisesQLI.get(quoId).put(pbe.Product2Id, new QuoteLineItem(Remise_en100__c = 100, Remise_en_euros__c = null));
                                                System.debug('### remise 100% 1: '+ pbe.Product2.Name+' 100%');
                                            }else{
                                                mapRemisesQLI.put(quoId, new Map<String, QuoteLineItem>{pbe.Product2Id => new QuoteLineItem(Remise_en100__c = 100, Remise_en_euros__c = null)});
                                                System.debug('### remise 100% 2: '+ pbe.Product2.Name+' 100%');
                                            }
                                        }
                                        else if(cli.ServiceContract.RootServiceContract.Option_P2_avec_seuil__c == true && cli.ServiceContract.RootServiceContract.Type_de_seuil__c == 'Seuil fixe : les pièces dont le montant est supérieur sont à régler en intégralité' && cli.IsActive__c && (pbe.Product2.Famille_d_articles__c == 'Pièces détachées' || pbe.Product2.Famille_d_articles__c == 'Accessoires')){
                                            System.debug('### ROK');
                                            System.debug('### mapRemisesQLI' + mapRemisesQLI);
                                            if(mapRemisesQLI.containsKey(quoId)){
                                                System.debug('### ROK');
                                                if(!mapRemisesQLI.get(quoId).containsKey(pbe.Product2Id)){
                                                    QuoteLineItem qliRem = new QuoteLineItem();
                                                    qliRem.Remise_en_euros__c = qli.UnitPrice <= cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c ? null : null;
                                                    qliRem.Remise_en100__c = qli.UnitPrice <= cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c ? 100 : null;
                                                    mapRemisesQLI.get(quoId).put(pbe.Product2Id, qliRem);
                                                    System.debug('### remise fix 1: '+ pbe.Product2.Name+' '+qliRem.Remise_en100__c+' '+qliRem.Remise_en_euros__c);
                                                }
                                            }else{
                                                QuoteLineItem qliRem = new QuoteLineItem();
                                                qliRem.Remise_en_euros__c = null;
                                                qliRem.Remise_en100__c = qli.UnitPrice <= cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c ? 100 : null;
                                                mapRemisesQLI.put(quoId, new Map<String, QuoteLineItem>{pbe.Product2Id => qliRem});
                                                System.debug('### remise fix 2: '+ pbe.Product2.Name+' '+qliRem.Remise_en100__c+' '+qliRem.Remise_en_euros__c);
                                            }
                                        }
                                        else if(
                                            (cli.ServiceContract.RootServiceContract.Option_P2_avec_seuil__c == true && cli.ServiceContract.RootServiceContract.Type_de_seuil__c == 'Seuil remise : les pièces dont le montant est supérieur sont remisées de ce seuil' && cli.IsActive__c && (pbe.Product2.Famille_d_articles__c == 'Pièces détachées' || pbe.Product2.Famille_d_articles__c == 'Accessoires'))
                                        ){
                                            if(mapRemisesQLI.containsKey(quoId)){
                                                if(!mapRemisesQLI.get(quoId).containsKey(pbe.Product2Id)){
                                                    QuoteLineItem qliRem = new QuoteLineItem();
                                                    qliRem.Remise_en_euros__c = qli.UnitPrice >= cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c ? cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c : null;
                                                    qliRem.Remise_en100__c = qli.UnitPrice >= cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c ? null : 100;
                                                    mapRemisesQLI.get(quoId).put(pbe.Product2Id, qliRem);
                                                    System.debug('### remise seuil 1: '+ pbe.Product2.Name+' '+qliRem.Remise_en100__c+' '+qliRem.Remise_en_euros__c);
                                                }
                                            }else{
                                                QuoteLineItem qliRem = new QuoteLineItem();
                                                qliRem.Remise_en_euros__c = qli.UnitPrice >= cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c ? cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c : null;
                                                qliRem.Remise_en100__c = qli.UnitPrice >= cli.ServiceContract.RootServiceContract.Valeur_du_seuil_P2__c ? null : 100;
                                                mapRemisesQLI.put(quoId, new Map<String, QuoteLineItem>{pbe.Product2Id => qliRem});
                                                System.debug('### remise seuil 2: '+ pbe.Product2.Name+' '+qliRem.Remise_en100__c+' '+qliRem.Remise_en_euros__c);
                                            }
                                        }
                                    }
                                //}else{
                                    //System.debug('##### in the else individual');
                                    else if(                                    
                                    //BCH 16/02/21 - TEC-521
                                        //(cli.Product2.ProductCode == 'COUV-DEPL-GAR-CHAM' && cli.IsActive__c && pbe.Product2.ProductCode == 'DEPL-ZONE')
                                        (cli.Product2.ProductCode.startsWith('COUV-DEP-GAR') && cli.IsActive__c && (pbe.Product2.ProductCode.startsWith('DEP-') || pbe.Product2.ProductCode.startsWith('FORFAIT-DEP') ))
                                        //|| (cli.Product2.ProductCode == 'COUV-MO-GAR-CHAM' && cli.IsActive__c && (pbe.Product2.ProductCode == 'MO' || pbe.Product2.ProductCode == 'FORFAIT-MO' || pbe.Product2.ProductCode == 'MO-SAMEDI'))
                                        || (cli.Product2.ProductCode.startsWith('COUV-MO-GAR') && cli.IsActive__c && (pbe.Product2.ProductCode.startsWith('MO') || pbe.Product2.ProductCode == 'FORFAIT-MO' || pbe.Product2.ProductCode == 'MO-SAMEDI'))
                                        //|| (cli.Product2.ProductCode == 'COUV-PIECES-GAR-CHAM' && cli.IsActive__c && pbe.Product2.RecordType.DeveloperName == 'Article' && pbe.Product2.Famille_d_articles__c == 'Pièces détachées' && pbe.Product2.Piece_d_usure__c == false)
                                        || (cli.Product2.ProductCode.startsWith('COUV-PIECES-GAR') && cli.IsActive__c && pbe.Product2.RecordType.DeveloperName == 'Article')
                                        || (cli.Product2.ProductCode == pbe.Product2.ProductCode )
                                    ){
                                        System.debug('##### fter condition');
                                        if(mapRemisesQLI.containsKey(quoId)){
                                            System.debug('##### fter condition2');
                                            if(!mapRemisesQLI.get(quoId).containsKey(pbe.Product2Id)){
                                                System.debug('##### fter condition3');
                                                mapRemisesQLI.get(quoId).put(pbe.Product2Id, new QuoteLineItem(Remise_en100__c = 100, Remise_en_euros__c = null));
                                            }
                                        }else{
                                            System.debug('##### fter condition4');
                                            mapRemisesQLI.put(quoId, new Map<String, QuoteLineItem>{pbe.Product2Id => new QuoteLineItem(Remise_en100__c = 100, Remise_en_euros__c = null)});
                                        }
                                    }
                                }
                            }
                        }
                        //LGO 1July2021 - TEC-682 
                        // else if(quo.Equipement__r.InstallDate != null && quo.Equipement__r.InstallDate.monthsBetween(Date.today()) <= 24 && pbe.Product2.RecordType.DeveloperName == 'Article' && pbe.Product2.Famille_d_articles__c == 'Pièces détachées' && pbe.Product2.Piece_d_usure__c == false ){
                        //     if(mapRemisesQLI.containsKey(quoId)){
                        //         if(!mapRemisesQLI.get(quoId).containsKey(pbe.Product2Id)){
                        //             mapRemisesQLI.get(quoId).put(pbe.Product2Id, new QuoteLineItem(Remise_en100__c = 100, Remise_en_euros__c = null));
                        //         }
                        //     }else{
                        //         mapRemisesQLI.put(quoId, new Map<String, QuoteLineItem>{pbe.Product2Id => new QuoteLineItem(Remise_en100__c = 100, Remise_en_euros__c = null)});
                        //     }
                        // }
                        
                        if(mapRemisesQLI.containsKey(quoId)){
                            if(mapRemisesQLI.get(quoId).containsKey(pbe.Product2Id)){
                                QuoteLineItem qliRem = mapRemisesQLI.get(quoId).get(pbe.Product2Id);
                                qlirem.Id = qli.Id;
                                qliRem.UnitPrice = qli.UnitPrice;
                            }
                        }
                    }
                }
            }
        }
        
        for(String quoId: setQuoP3r){
            if(mapQuoIdToQli.containsKey(quoId)){
                for(QuoteLineItem qli : mapQuoIdToQli.get(quoId)){
                    if(mapRemisesQLI.containsKey(quoId)){
                        qli.Remise_en100__c = 100;
                        qli.Remise_en_euros__c = null;
                        mapRemisesQLI.get(quoId).put(mapIdPbe.get(qli.PriceBookEntryId).Product2Id, qli);
                    }else{
                        qli.Remise_en100__c = 100;
                        qli.Remise_en_euros__c = null;
                        mapRemisesQLI.put(quoId, new Map<String, QuoteLineItem>{mapIdPbe.get(qli.PriceBookEntryId).Product2Id => qli});
                    }
                }
            }
        }
        System.debug('##### mapRemisesQLI: '+mapRemisesQLI);
        return mapRemisesQLI;
    }
}