/**
* @File Name          : LC01_Acc360Equipements.cls
* @Description        : Controller class for lightning component LC01_Acc360Equipements
* @Author             : RRJ
* @Group              : Spoon Consulting
* @Last Modified By   : ZJO
* @Last Modified On   : 26/06/2020, 12:15:41
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                 Modification
*==============================================================================
* 1.0    24/06/2019, 18:48:28          RRJ                       Initial Version
* 1.1    03/10/2019                    RRJ                       Modified Address
* 1.2    08/06/2020                    DMU                       Added logic to consider Collectif SC
* 1.3    16/06/2020                    DMU                       CT-1812 conditions on query CLI
**/
public class LC01_Acc360Equipements {
    
    @AuraEnabled
    public static List<EqpData> fetchEqp(Id accId){
        system.debug('*** in fetchEqp: '+accId );
        
        Map<Id, Asset> mapEqp = new Map<Id, Asset>([SELECT Id, Name, Logement__r.Owner__c, Logement__r.Inhabitant__c, Logement__r.Street__c, Logement__r.Postal_Code__c, Logement__r.City__c, AccountId,  Account.PersonContactId, Logement__r.Agency__r.Name, Logement__r.Agency__c, Account.Date_fin_retombees__c, Account.Campagne__c FROM Asset WHERE AccountId = : accId AND Status != 'Inactif' AND ParentId=null]);
        system.debug('*** mapEqp.keySet(): '+mapEqp.keySet() );
        
        //ZJO CT-1796 26/06/2020 added contract status ('En attente de renouvellement', 'Pending Payment')
        List<ServiceContract> lstServConAll = [SELECT Id, Name, Contract_Status__c, Asset__c, TECH_VE_Min_Date__c, TECH_VE_Max_Date__c, LastModifiedDate, VE_completion_date__c 
                                                FROM ServiceContract 
                                                WHERE Asset__c = :mapEqp.keySet() 
                                                AND (Contract_Status__c IN ('Pending first visit','Actif - en retard de paiement','Active', 'Pending Client Validation', 'En attente de renouvellement', 'Pending Payment')  OR (Contract_Status__c = 'Draft' AND Type__c = 'Individual')) 
                                                ORDER BY LastModifiedDate ASC];
        system.debug('*** lstServConAll: '+lstServConAll);

        //DMU 08-06-2020 Added below logic to consider Collectif SC
        //DMU 16-06-2020 - CT-1812 - Added below condition (IsBundle__c = true || (IsBundle__c = true AND TotalPrice > 0) 
        list <contractlineItem> listCLICollective = new list <contractlineItem>();
        //ZJO CT-1796 26/06/2020 added contract status ('En attente de renouvellement', 'Pending Payment')
        listCLICollective = [SELECT Id, Customer_Absences__c, Customer_Allowed_Absences__c, ServiceContractId, ServiceContract.Name, VE_Status__c, VE_Min_Date__c, VE_Max_Date__c, Completion_Date__c, AssetId, ServiceContract.Contract_Status__c, IsBundle__c, Product2.Name, ServiceContract.Type__c, LastModifiedDate, ServiceContract.LastModifiedDate, TotalPrice
                            ,ServiceContract.Asset__c, ServiceContract.TECH_VE_Min_Date__c, ServiceContract.VE_completion_date__c, ServiceContract.TECH_VE_Max_Date__c 
                            FROM contractlineItem 
                            WHERE Collective_Account__c=: accId 
                            AND AssetId = :mapEqp.keySet()
                            AND (IsBundle__c = true OR (IsBundle__c = false AND TotalPrice > 0))
                            AND (ServiceContract.Contract_Status__c IN ('Pending first visit','Actif - en retard de paiement','Active', 'Pending Client Validation', 'En attente de renouvellement', 'Pending Payment')) 
                            ];
        system.debug('*** listCLICollective: '+listCLICollective.size() );
        list<Id> lstSCId = new list<Id>();
        for(contractlineItem cli : listCLICollective){
            lstSCId.add(cli.ServiceContractId);
        }
        List<ServiceContract> lstServConCollectif = [SELECT Id, Name, Contract_Status__c, Asset__c, TECH_VE_Min_Date__c, TECH_VE_Max_Date__c, LastModifiedDate, VE_completion_date__c                
                                                    FROM ServiceContract where Id in:lstSCId ];
        lstServConAll.addAll(lstServConCollectif);
        system.debug('*** lstServConAll: '+lstServConAll.size() );

        Map<Id, ServiceContract> mapServConCreateActive = new Map<Id, ServiceContract>();
        Map<Id, ServiceContract> mapServConCreatePendingCV = new Map<Id, ServiceContract>();
        Map<Id, ServiceContract> mapServConCreateDraft = new Map<Id, ServiceContract>();

        //ZJO CT-1796 26/06/2020 added new map for contract ('En attente de renouvellement', 'Pending Payment')
        Map<Id, ServiceContract> mapServConCreateRenouvellement = new Map<Id, ServiceContract>();

        system.debug('*** bef loop' );
        for(ServiceContract servCon : lstServConAll){
            system.debug('*** in loop' );
            //DMU 26-05-2020 added condition to check if sc.asset null since collectif SC.asset is null
            //ZJO CT-1796 26/06/2020 added condition 'En attente de renouvellement' ou 'Pending Payment'
            if(servCon.Asset__c != null){                
                if(servCon.Contract_Status__c == 'Pending first visit' || servCon.Contract_Status__c == 'Actif - en retard de paiement' || servCon.Contract_Status__c == 'Active'){
                    mapServConCreateActive.put(servCon.Asset__c, servCon);
                }
                else if(servCon.Contract_Status__c == 'Pending Client Validation'){
                    mapServConCreatePendingCV.put(servCon.Asset__c, servCon);
                }
                else if(servCon.Contract_Status__c == 'Draft'){
                    mapServConCreateDraft.put(servCon.Asset__c, servCon);
                }
                else if (servCon.Contract_Status__c == 'En attente de renouvellement' || servCon.Contract_Status__c == 'Pending Payment'){
                    mapServConCreateRenouvellement.put(servCon.Asset__c, servCon);
                }
            }
        }

        //ZJO CT-1796 26/06/2020 added condition 'En attente de renouvellement' ou 'Pending Payment'
        for(contractlineItem cliCollectif : listCLICollective){            
            if(cliCollectif.ServiceContract.Contract_Status__c == 'Pending first visit' || cliCollectif.ServiceContract.Contract_Status__c == 'Actif - en retard de paiement' || cliCollectif.ServiceContract.Contract_Status__c == 'Active'){
                mapServConCreateActive.put(cliCollectif.AssetId, cliCollectif.ServiceContract);
            }
            else if(cliCollectif.ServiceContract.Contract_Status__c == 'Pending Client Validation'){
                mapServConCreatePendingCV.put(cliCollectif.AssetId, cliCollectif.ServiceContract);
            }
            else if(cliCollectif.ServiceContract.Contract_Status__c == 'Draft'){
                mapServConCreateDraft.put(cliCollectif.AssetId, cliCollectif.ServiceContract);
            }
        }

        system.debug('*** mapServConCreateActive size: '+mapServConCreateActive.size() );
        system.debug('*** mapServConCreatePendingCV size: '+mapServConCreatePendingCV.size() );
        system.debug('*** mapServConCreateDraft size: '+mapServConCreateDraft.size() );
        
        //ZJO CT-1796 26/06/2020 added map values contrat renouvelé in lstConId
        List<ServiceContract> lstConId = mapServConCreateActive.values();
        lstConId.addAll(mapServConCreateDraft.values());
        lstConId.addAll(mapServConCreatePendingCV.values());
        lstConId.addAll(mapServConCreateRenouvellement.values());

        system.debug('*** lstConId: '+lstConId );
        
        //DMU 26-05-2020 added condition acc.Type for Collectif acc
        //DMU 16-06-2020 - CT-1812 - Added below condition (IsBundle__c = true || (IsBundle__c = true AND TotalPrice > 0) 
        List<ContractLineItem> lstCLI = (listCLICollective.size()!=0) ? listCLICollective : [SELECT Id, Customer_Absences__c, Customer_Allowed_Absences__c, ServiceContractId, ServiceContract.Name, VE_Status__c, VE_Min_Date__c, VE_Max_Date__c, Completion_Date__c, AssetId, ServiceContract.Contract_Status__c, IsBundle__c, Product2.Name, ServiceContract.Type__c, LastModifiedDate, ServiceContract.LastModifiedDate, TotalPrice 
                                                                                            FROM ContractLineItem 
                                                                                            WHERE ServiceContractId IN :lstConId 
                                                                                            AND (IsBundle__c = true OR (IsBundle__c = false AND TotalPrice > 0))];
        system.debug('*** lstCLI size: '+lstCLI.size() );
        
        Map<Id, List<ContractLineItem>> mapConToOptions = new Map<Id, List<ContractLineItem>>();

        for(ContractLineItem cli : lstCLI){
            if(cli.TotalPrice > 0){
                if(mapConToOptions.containsKey(cli.ServiceContractId)){
                    mapConToOptions.get(cli.ServiceContractId).add(cli);
                }else {
                    mapConToOptions.put(cli.ServiceContractId,new List<ContractLineItem>{cli});
                }
            }
        }

        system.debug('*** mapConToOptions: '+mapConToOptions );
        system.debug('*** mapConToOptions size: '+mapConToOptions.size() );
        system.debug('*** mapEqp.keySet() size: '+mapEqp.keySet().size() );
        system.debug('*** mapEqp.keySet() : '+mapEqp.keySet()  );
        
        List<EqpData> lstEqpDataLines = new List<EqpData>();
        for(Id eqpId : mapEqp.keySet()) {
            EqpData rowData;
            if(mapServConCreateActive.containsKey(eqpId)){
                system.debug('*** in if 1: ' );
                rowData = new EqpData(mapEqp.get(eqpId), mapServConCreateActive.get(eqpId));
            }else if(mapServConCreatePendingCV.containsKey(eqpId)){
                rowData = new EqpData(mapEqp.get(eqpId), mapServConCreatePendingCV.get(eqpId));
            }else if(mapServConCreateDraft.containsKey(eqpId)){
                rowData = new EqpData(mapEqp.get(eqpId), mapServConCreateDraft.get(eqpId));
            }else{
                rowData = new EqpData(mapEqp.get(eqpId));
            }
            //DMU 17-06-2020 - CT-1812 - Added || mapServConCreatePendingCV.containsKey(eqpId)
            if(mapServConCreateActive.containsKey(eqpId) || mapServConCreateDraft.containsKey(eqpId) || mapServConCreatePendingCV.containsKey(eqpId)){
                system.debug('*** in if A: ' );
                Id conId = rowData.eqpContServIdDisplay;
                rowData.lstOption = new List<ContractLineItem>();
                
                if(mapConToOptions.containsKey(conId)){
                    for(ContractLineItem cli : mapConToOptions.get(conId)){
                        if(cli.IsBundle__c == false){
                            rowData.lstOption.add(cli);
                        }
                        else{
                            rowData.eqpSousContractId = cli.Id;
                            rowData.eqpStatutVe = getPicklistLabel('ContractLineItem', 'VE_Status__c', cli.VE_Status__c);
                            rowData.eqpNbAbs = cli.Customer_Absences__c == null ? 0 : cli.Customer_Absences__c.intValue();
                            rowData.eqpNbAbsAllow = cli.Customer_Allowed_Absences__c == null ? 0 : cli.Customer_Allowed_Absences__c.intValue();
                            if(cli.ServiceContract.Type__c == 'Collective'){
                                rowData.eqpIsCollectif = true;
                            }
                        }
                    }
                }
            }
            
            lstEqpDataLines.add(rowData);
        }
        return lstEqpDataLines;
        
    }
    
    public class EqpData {
        @AuraEnabled public String eqpId;
        @AuraEnabled public String eqpAccId;
        @AuraEnabled public String eqpContactId;
        @AuraEnabled public String eqpNom;
        @AuraEnabled public String eqpAddresse;
        @AuraEnabled public String eqpAgence;
        @AuraEnabled public String eqpAgenceId;
        @AuraEnabled public String eqpCampagne;  //RRA - 20200717 - CT-64 - l'ajout du code campagne - reference sur la page confluence
        @AuraEnabled public String statutCompte;

        @AuraEnabled public Boolean eqpSousContrat;
        @AuraEnabled public String eqpContServIdDisplay;
        @AuraEnabled public String eqpContServIdCreate;
        @AuraEnabled public String eqpSonServName;
        @AuraEnabled public Datetime eqpVeMin;
        @AuraEnabled public Datetime eqpVeMax;
        @AuraEnabled public Datetime eqpVeCompleted;
        @AuraEnabled public String eqpConServStatus;
        @AuraEnabled public Boolean eqpIsCollectif;

        @AuraEnabled public List<ContractLineItem> lstOption;
        @AuraEnabled public String eqpSousContractId;
        @AuraEnabled public String eqpStatutVe;
        @AuraEnabled public Integer eqpNbAbs;
        @AuraEnabled public Integer eqpNbAbsAllow;

        //public String dateDuJour = System.today();  //RRA - 20200717 - CT-64 - l'ajout du code campagne - reference sur la page confluence
        public Date todaydate = Date.newInstance(system.today().year(), system.today().month(), system.today().day());

        public EqpData(Asset eqp, ServiceContract con){
            //System.debug(dateDuJour);
            this.eqpId = eqp.Id;
            this.eqpAccId = eqp.AccountId;
            this.eqpContactId = eqp.Account.PersonContactId;
            this.eqpCampagne = (todaydate < eqp.Account.Date_fin_retombees__c) ? eqp.Account.Campagne__c : null; //RRA - 20200717 - CT-64 - l'ajout du code campagne - reference sur la page confluence
            //this.eqpCampagne = eqp.Account.Campagne__c;
            this.eqpNom = eqp.Name;
            this.eqpAddresse = eqp.Logement__r.Street__c +' '+ eqp.Logement__r.Postal_Code__c +' '+eqp.Logement__r.City__c;
            this.eqpAgence  = eqp.Logement__r.Agency__r.Name;
            this.eqpAgenceId  = eqp.Logement__r.Agency__c;
            this.statutCompte = eqp.Logement__r.Owner__c == eqp.Logement__r.Inhabitant__c ? 'Propriétaire' : eqp.Logement__r.Owner__c == null ? 'Locataire' : eqp.Logement__r.Owner__c != eqp.Logement__r.Inhabitant__c ? 'Locataire' : null;
            
            this.eqpSousContrat = true;
            this.eqpContServIdDisplay = con.Id;
            this.eqpContServIdCreate = con.Id;
            this.eqpSonServName = con.Name;
            this.eqpVeMin  = con.TECH_VE_Min_Date__c;
            this.eqpVeMax  = con.TECH_VE_Max_Date__c;
            this.eqpVeCompleted  = con.VE_completion_date__c;
            this.eqpConServStatus = getPicklistLabel('ServiceContract', 'Contract_Status__c', con.Contract_Status__c);
            this.eqpIsCollectif = false;
        }
        
        public EqpData(Asset eqp){
            this.eqpId = eqp.Id;
            this.eqpAccId = eqp.AccountId;
            this.eqpContactId = eqp.Account.PersonContactId;
            this.eqpCampagne = (todaydate < eqp.Account.Date_fin_retombees__c) ? eqp.Account.Campagne__c : null; //RRA - 20200717 - CT-64 - l'ajout du code campagne - reference sur la page confluence
            //this.eqpCampagne = eqp.Account.Campagne__c;
            this.eqpNom = eqp.Name;
            this.eqpSousContrat = false;
            this.eqpAddresse = eqp.Logement__r.Street__c +' '+ eqp.Logement__r.Postal_Code__c +' '+eqp.Logement__r.City__c;
            this.eqpAgence  = eqp.Logement__r.Agency__r.Name;
            this.eqpAgenceId  = eqp.Logement__r.Agency__c;
            this.statutCompte = eqp.Logement__r.Owner__c == eqp.Logement__r.Inhabitant__c ? 'Propriétaire' : eqp.Logement__r.Owner__c == null ? 'Locataire' : eqp.Logement__r.Owner__c != eqp.Logement__r.Inhabitant__c ? 
                'Locataire' : null;
        }
        
    }
    
    public static String getPicklistLabel(String strObj, String strFld, String strPickValue){
        String retValue = '';
        for(Schema.PicklistEntry pickEntry : Schema.getGlobalDescribe().get(strObj).getDescribe().fields.getMap().get(strFld).getDescribe().getPicklistValues()) {
            if(pickEntry.getValue() == strPickValue) {
                retValue =  pickEntry.getLabel();
            }
        }
        return retValue;
    }
}