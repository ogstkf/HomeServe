/**
 * @File Name          : VFC07_MassAvisDePassagePDF.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRA
 * @Last Modified On   : 05/08/2020, 15:19:54
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    21/08/2019, 17:55:46   RRJ     Initial Version
**/
public without sharing class VFC07_MassAvisDePassagePDF {

    public List<ServiceAppointment> lstSrvApp {get; set;}
    List<ServiceTerritory> lstSrvt {get;set;}
    public map<String,String> mapArrivalWindowStartTime {get; set;}
    public map<String,String> mapArrivalWindowEndTime {get; set;}

    // RRA - TEC104 - Add fields : ServiceTerritory.Agency_number__c, ServiceTerritory.Site_web__c, ServiceTerritory.Intra_Community_VAT__c, ArrivalWindowEndTime
    public VFC07_MassAvisDePassagePDF(){
        String ids = ApexPages.currentPage().getParameters().get('lstIds');
        //String idSA = ApexPages.currentPage().getParameters().get('lstIdSA');
        List<Id> lstIds = (List<Id>)JSON.deserialize(ids, List<Id>.class);
        //List<Id> lstIdSA = (List<Id>)JSON.deserialize(idSA, List<Id>.class);
        System.debug('lstIds = ' + lstIds);
        System.debug('ids = ' + ids);
        
        /*String idAg = ApexPages.currentPage().getParameters().get('idAgence');
        Id idAgence = (Id)JSON.deserialize(ids, Id.class);
        System.debug('idAg = ' + idAg);*/
        
        //DMU 20190909 - CT-970 Added condition order by SchedStartTime
         List<ServiceAppointment> lstSA = [SELECT Id, ServiceTerritory.Capital_in_Eur__c,  ServiceTerritory.Logo_Name_Agency__c, ServiceTerritory.Libelle_horaires__c, ServiceTerritory.Agency_number__c, ServiceTerritory.Email__c, ServiceTerritory.Site_web__c, ServiceTerritory.Intra_Community_VAT__c, Account.ClientNumber__c, TECH_AccountSource__c, ServiceTerritory.TECH_CapitalInEur__c, ServiceTerritory.APE__c, ServiceTerritory.Legal_Form__c,  
        ServiceTerritory.Siren__c, ServiceTerritory.SIRET__c, ServiceTerritory.RCS__c, ServiceTerritory.IBAN__c, ArrivalWindowStartTime, ArrivalWindowEndTime, Residence__r.Inhabitant__r.IsPersonAccount, Residence__r.Imm_Res__c, Residence__r.Door__c, Residence__r.Floor__c, Residence__r.Street__c, Residence__r.Adress_complement__c, Residence__r.Postal_Code__c, Residence__r.City__c, Residence__r.Account__r.BillingCity, Residence__r.Inhabitant__r.Adress_Complement__c, Residence__r.Account__r.BillingPostalCode, Residence__r.Account__r.BillingStreet, Residence__r.Account__r.Adress_Complement__c, Residence__r.Account__r.FirstName, Residence__r.Account__r.LastName, Residence__r.Account__r.Salutation, Residence__r.Inhabitant__r.BillingCity, Residence__r.Inhabitant__r.BillingPostalCode, Residence__r.Inhabitant__r.BillingStreet, Residence__r.Inhabitant__r.FirstName, Residence__r.Inhabitant__r.LastName, Residence__r.Inhabitant__r.Salutation, Residence__r.Legal_Guardian__r.BillingCity, Residence__r.Legal_Guardian__r.BillingPostalCode , Residence__r.Legal_Guardian__r.BillingStreet, Residence__r.Legal_Guardian__r.FirstName, Residence__r.Legal_Guardian__r.LastName, Residence__r.Legal_Guardian__r.Salutation, Residence__r.Owner__r.BillingCity, Residence__r.Owner__r.BillingPostalCode, Residence__r.Owner__r.BillingStreet, Residence__r.Owner__r.Adress_Complement__c, Residence__r.Owner__r.FirstName, Residence__r.Owner__r.LastName, Residence__r.Owner__r.Salutation, Residence__r.Visit_Notice_Recipient__c, ServiceTerritory.City, ServiceTerritory.Corporate_Name__c, ServiceTerritory.Name, ServiceTerritory.Phone__c, ServiceTerritory.PostalCode, ServiceTerritory.Street, ServiceTerritory.Street2__c, ServiceTerritory.TerritoryStat__c, SchedStartTime, Residence__r.Inhabitant__r.Imm_Res__c, Residence__r.Inhabitant__r.Door__c, Residence__r.Owner__r.Imm_Res__c, Residence__r.Owner__r.Door__c, Residence__r.Legal_Guardian__r.Adress_Complement__c, Residence__r.Legal_Guardian__r.Imm_Res__c, Residence__r.Legal_Guardian__r.Door__c, Residence__r.Account__r.Imm_Res__c, Residence__r.Account__r.Door__c, Residence__r.Inhabitant__r.Floor__c, Residence__r.Account__r.Floor__c, Residence__r.Legal_Guardian__r.Floor__c, Residence__r.Owner__r.Floor__c                        
        ,Contact.Account.salutation, Contact.Account.firstname, Contact.Account.lastname, Residence__r.Inhabitant__r.Raison_sociale__c                            
        FROM ServiceAppointment
        WHERE Id IN :lstIds AND TECH_AccountSource__c != 'SOWEE'
        ORDER BY SchedStartTime ASC];
        
        this.lstSrvApp = lstSA;
        
        for(ServiceAppointment s : lstSA){         
            TimeZone tz = UserInfo.getTimeZone();
            
            Datetime aet = s.ArrivalWindowEndTime;
            mapArrivalWindowEndTime = new map<String,String> ();
            mapArrivalWindowEndTime.put(string.valueOf(s.Id), (aet.format('HH:mm',tz.toString())).replace(':', 'h') );
            
            Datetime ast = s.ArrivalWindowStartTime;   
            mapArrivalWindowStartTime = new map<String,String> ();
            mapArrivalWindowStartTime.put(s.Id, (ast.format('HH:mm',tz.toString())).replace(':', 'h') );
        }
    }
    
     



 /*   public void getLogo (){
      //  System.debug('--@@Check the received NameAgence in Method@@--'+ nameAg);
       // imageURL='/servlet/servlet.FileDownload?file=';
       lstSrvApp = [SELECT Id, ServiceTerritory.Name FROM ServiceAppointment WHERE ServiceTerritory.Name  IN: lstSrvApp];
        for (ServiceAppointment srA : lstSrvt){
            for (ServiceTerritory svApp : lstSrvApp)
            if (srt.Name ==  lstSrvApp.){
              documentList = [SELECT Id, Name FROM document WHERE Name =:nameAg];
                if(documentList.size()>0){
                  imageURL=imageURL+documentList[0].id;
                  System.debug('imageURL = ' + imageURL);
                }
            }
        } 
    }*/
  
}