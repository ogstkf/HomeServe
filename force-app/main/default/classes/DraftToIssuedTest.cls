/**
 * @File Name          : DraftToIssuedTest.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 07/02/2020, 10:21:53
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    07/02/2020   AMO     Initial Version
**/
@isTest
public class DraftToIssuedTest {
    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<sofactoapp__Factures_Client__c> lstFacturesClients = new List<sofactoapp__Factures_Client__c>();
    static List<sofactoapp__R_glement__c> lstRGlements = new List<sofactoapp__R_glement__c>();
    static List<OperatingHours> lstOpHrs;
    static List<ServiceTerritory> lstAgence;

    static {
        adminUser = TestFactory.createAdminUser('testuser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser) {
            for (Integer i = 0; i < 5; i++) {
                lstTestAcc.add(TestFactory.createAccountBusiness('testAcc' + i));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'BusinessAccount');
                lstTestAcc[i].BillingPostalCode = '1234' + i;
                lstTestAcc[i].Rating__c = 4;
            }
            insert lstTestAcc;

            lstOpHrs = new List<OperatingHours>{
                            new OperatingHours(Name='op')
            };
            insert lstOpHrs;

            Sofactoapp__Raison_Sociale__c raisonSoc = TestFactory.createSofactoapp_Raison_Sociale('test raison Soc');
            insert raisonSoc;

            lstAgence = new List<ServiceTerritory>{ TestFactory.createServiceTerritory('test', lstOpHrs[0].Id, raisonSoc.Id)
            };
            insert lstAgence;

            for (Integer i = 0; i < 5; i++) {
                lstOpp.add(TestFactory.createOpportunity('testiTf', lstTestAcc[i].Id));
                lstOpp[i].Agency__c = lstAgence[0].Id;
                
            }
            
            insert lstOpp;

            sofactoapp__Raison_Sociale__c RS = new sofactoapp__Raison_Sociale__c(Name= 'CHAM2', sofactoapp__Credit_prefix__c= '145', sofactoapp__Invoice_prefix__c='982');
            insert RS;

            for (Integer i = 0; i < 5; i++) {
                lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                        sofactoapp__Compte__c = lstTestAcc[i].Id
                        , sofactoapp__Opportunit__c = lstOpp[i].Id
                        , sofactoapp__emetteur_facture__c = RS.Id
                    	, sofactoapp__Etat__c= 'Brouillon'
                ));
            }
            
            
            insert lstFacturesClients;

            for (Integer i = 0; i < 1; i++) {
                lstRGlements.add(new sofactoapp__R_glement__c(
                        sofactoapp__Facture__c = lstFacturesClients[i].Id
                        , sofactoapp__Montant__c = 22
                        , sofactoapp__Mode_de_paiement__c = 'Prélèvement'
                        , statut_du_paiement__c = 'Impayé à représenter'
                ));
            }
            lstRGlements[0].sofactoapp__Date__c = Date.newInstance(2019, 12, 20);
            // lstRGlements[1].sofactoapp__Date__c = Date.newInstance(2019, 12, 5);
            insert lstRGlements;
            
        }
    }
    static testMethod void TestdraftToIssued() {
        list <Opportunity> opp = new list<Opportunity>([SELECT Id from 
                                                        Opportunity where Name ='testiTf']);
         
        Set<Id> oppIds = (new Map<Id,Opportunity>(opp)).keySet().clone();
        List<Id> lStrings = new List<Id>(oppIds);
        Test.startTest();
            DraftToIssuedOpp.OppInvoiceIssued(lStrings);
        Test.stopTest();
        
        } 

}