/**
 * @File Name          : AP51_AddPricebookEntryAuto.cls
 * @Description        : 
 * @Author             : DMG
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    05/12/2019         DMG                    Initial Version
**/
public with sharing class AP51_AddPricebookEntryAuto {
    
    public static void createPriceBookEntry(List<Product2> lstProduct){
        System.debug('## Start AP51_AddPricebookEntryAuto createPriceBookEntry: ');
        List<PriceBookEntry> lstPBEntry = new List<PriceBookEntry>();
        List<PriceBook2> pStandard = [SELECT Id, Name FROM PriceBook2 WHERE IsStandard = true];
        if(!pStandard.isEmpty()){
            for(Product2 p:lstProduct){
                PriceBookEntry pbe = new PriceBookEntry(Product2Id=p.Id, UnitPrice=1, Pricebook2Id=pStandard[0].Id, IsActive = true);
                lstPBEntry.add(pbe);
            }

            if(lstPBEntry.size()>0) {
                insert lstPBEntry;
            }
        }
        
       
    }
    public static void checkPriceBookEntry(List<Product2> lstProduct){
        System.debug('## Start AP51_AddPricebookEntryAuto checkPriceBookEntry: ');
        List<PriceBookEntry> lstExistingPBEntry = new List<PriceBookEntry>();
        Map<Id, PriceBookEntry> mapProPriceBookEntry = new Map<Id, PriceBookEntry>();
        List<Product2> listProductTocreate = new List<Product2>();
        lstExistingPBEntry = [SELECT Id, Name, Product2Id FROM PriceBookEntry WHERE Product2Id IN:lstProduct ];
        //build map existing PriceBookEntry - Products
        for(PriceBookEntry p :lstExistingPBEntry){
            if(!mapProPriceBookEntry.containsKey(p.Product2Id)){
                mapProPriceBookEntry.put(p.Product2Id,p);
            }
        }
        //Find products not having PriceBookEntry
        for(Product2 pb: lstProduct){
            if(mapProPriceBookEntry.get(pb.Id)==null){
                listProductTocreate.add(pb);
            }
        }
        //create PBE
        if(listProductTocreate.size()>0){
            createPriceBookEntry(listProductTocreate);
        }
    }
}