/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 08-27-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-25-2020   AMO   Initial Version
**/
public with sharing class LC72_GenerateContrat {
     @AuraEnabled
    public static map<string,Object> saveContratPDF(String servConId){
        
        List<String> lstServConId = new List<String>();
        List<ContentVersion> lstCV = new List<ContentVersion>();
        map<string,Object> mapOfResult = new map<string,Object>();

        lstServConId.add(servConId);
        // ServiceContract sc = [Select Id, ContractNumber, (Select Id From ContractLineItems) From ServiceContract Where Id=:servConId];
        
        Savepoint sp = Database.setSavepoint();
        try{

            PageReference template = new PageReference('/apex/VFP73_Contrat');
            Blob reportPDF;
            template.getParameters().put('lstIds',  JSON.serialize(lstServConId));
            reportPDF = (Test.isRunningTest() ? Blob.valueOf('Unit.Test') : template.getContentAsPDF());
            ContentVersion CV = new ContentVersion();
            Datetime now = Datetime.now();
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);
            CV.PathOnClient = 'Contrat_' + local + '.pdf';
            CV.Title = 'Contrat_' + local + '.pdf';
            CV.VersionData = reportPDF;
            CV.IsMajorVersion = true;

            Insert CV;
            lstCV.add(CV);

            //Get Content Documents
            List<ContentVersion> docList = [SELECT ContentDocumentId FROM ContentVersion where Id=:lstCV];
            System.debug('## doc list:' + docList);
            Map<Id, Id> mapIdCVIdCDL = new Map<Id, Id>();
            for(ContentVersion contentV : docList){
                mapIdCVIdCDL.put(contentV.Id,contentV.ContentDocumentId);
            }

            //Create ContentDocumentLink 
            ContentDocumentLink cdl_ADP = New ContentDocumentLink();
            cdl_ADP.LinkedEntityId = servConId;
            cdl_ADP.ContentDocumentId = mapIdCVIdCDL.get(cv.Id);
            cdl_ADP.shareType = 'V';
            Insert cdl_ADP;

            //Update Generated Date
            // sa.Date_Visit_Notice_Generated__c = System.today();
            // Update sa;
           
            mapOfResult.put('error',false);
	        mapOfResult.put('message', 'SUCESS!!!');
            mapOfResult.put('ADP',cdl_ADP.ContentDocumentId);

        }catch(Exception e){

            Database.rollback(sp);
            mapOfResult.put('error',true);
	        mapOfResult.put('message',e.getMessage());   
            throw new AuraHandledException(e.getMessage());           
        }

        return mapOfResult;
    }

}