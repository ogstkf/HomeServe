/**
 * @File Name          : VFC12_GenerateReport.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 04/03/2020, 17:24:53
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/03/2020   AMO     Initial Version
**/
public with sharing class VFC12_GenerateReport {
    private ApexPages.StandardSetController setCon;
    public List<ProductRequestLineItem>lstSelectedRecs {get;set;}
    public String selectedRecords{get;set;}
    public String filterId{get;set;}

    public VFC12_GenerateReport(ApexPages.StandardSetController controller) {
        System.debug('##lst Product request line');
        setCon = controller;
        lstSelectedRecs = (List<ProductRequestLineItem>) setCon.getSelected();
        filterId = setCon.getFilterId();
        System.debug('## FilterId : ' + filterId);
        selectedRecords = '';
        for(ProductRequestLineItem prli : lstSelectedRecs){
            System.debug('## Prli: ' + prli);
            selectedRecords += prli.Id + ',';
        }

        selectedRecords = selectedRecords.removeEnd(',');

        System.debug('## selected Records: ' + selectedRecords);
        System.debug('## lstSelectedProdReqLineItem : ' + lstSelectedRecs);
        System.debug('##VFC12_GenerateReport end');
    }
}