/**
 * @File Name          : BAT10_UpdateInventairePILocation.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 4/14/2020, 12:01:41 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/03/2020   AMO     Initial Version
**/
global with sharing class BAT10_UpdateInventairePILocation implements Database.Batchable<SObject> {

    private Set<Id> setAgenceId;
    
    public BAT10_UpdateInventairePILocation(Set<Id> setServTerritory){
        setAgenceId = setServTerritory;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'SELECT Id, Product2Id, DestinationLocation.LocationType, DestinationLocation.Agence__c, ShipmentId, Inventaire_en_cours__c, Agence_destination__c, SourceProductItemId FROM ProductTransfer WHERE';
        query += ' DestinationLocation.Agence__c in :setAgenceId AND SourceLocation.LocationType = \'Virtual\' AND ShipmentId != null AND Inventaire_en_cours__c = true';
        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext BC, List<ProductTransfer> lstProdTrans) {
        Map<Id, ProductTransfer> mapProdTrans = new Map<Id, ProductTransfer>(lstProdTrans);
        Map<String,ProductTransfer> mapProdLocationToProdTrnsfr = new Map<String,ProductTransfer>();
        List<ProductTransfer> lstProductTrnsfrtoUpdate = new list<ProductTransfer>();
        List<ProductTransfer> lstDuplicateProdTrnsfr = new List<ProductTransfer>();
        
        for(ProductTransfer prdT : lstProdTrans){

          String prodLocation = prdT.Product2Id + '_' + prdT.Agence_destination__c;

          prdT.IsReceived = true;
          prdT.Inventaire_en_cours__c = false;

        }

        if(lstProdTrans.size() > 0){
            updatePrdTransfers(lstProdTrans);
        }
    }

    private static void updatePrdTransfers(List<ProductTransfer> lstPrdTrans){
        Map<Id, ProductTransfer> mapProdTrans = new Map<Id, ProductTransfer>(lstPrdTrans);
        Map<String, ProductTransfer> mapExtIdPrdTrans = new Map<String, ProductTransfer>();
        List<ProductTransfer> lstRemaining = new List<ProductTransfer>();
        List<ProductTransfer> lstToUpdt = new List<ProductTransfer>();
        for(ProductTransfer prdTrans : lstPrdTrans){
            System.debug('###### prdTrans: '+prdTrans);
            String extId = String.join(new List<String>{prdTrans.Product2Id, prdTrans.DestinationLocation.Agence__c, prdTrans.SourceProductItemId}, '_');
            System.debug('#### extId: '+extId);
            if(mapExtIdPrdTrans.containsKey(extId)){
                lstRemaining.add(prdTrans);
            }else{
                mapExtIdPrdTrans.put(extId, prdTrans);
                lstToUpdt.add(prdTrans);
            }
        }
        System.debug('###### lstRemaining: '+lstRemaining);
        System.debug('###### lstToUpdt: '+lstToUpdt);

        if(lstToUpdt.size()>0){
            update lstToUpdt;
        }
        if(lstRemaining.size()>0){
            update lstRemaining;
        }
    }

    global void finish(Database.BatchableContext BC) {
        
        if(setAgenceId.size() > 0){
            BAT10_UpdateInventaireTypeVehicule BAT10Vehicule = new BAT10_UpdateInventaireTypeVehicule(setAgenceId);
            Database.executeBatch(BAT10Vehicule, 2);
        }
        
    }
}