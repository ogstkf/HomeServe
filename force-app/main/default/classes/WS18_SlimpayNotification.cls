/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 08-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   04-03-2022   MNA   Initial Version
**/

@RestResource(urlMapping='/sendnotification')
global class WS18_SlimpayNotification {
    @HttpPost
    global static String postMethod() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        ServiceContract sc;
        sofactoapp__External_Api__c extAPI;
        slimPayObjectWrp.responsePost resPost;
        slimPayObjectWrp.ResponseOrder resOrder = (slimPayObjectWrp.ResponseOrder) JSON.deserialize(request.requestBody.toString(), slimPayObjectWrp.responseOrder.class);
        
        try {
            extAPI = [SELECT Id, sofactoapp__Response__c FROM sofactoapp__External_Api__c WHERE transaction__c =: resOrder.Id];
            resPost = (slimPayObjectWrp.responsePost) JSON.deserialize(extAPI.sofactoapp__Response__c, slimPayObjectWrp.responsePost.class);
            sc = [SELECT Id, sofactoapp_Rib_prelevement__c FROM ServiceContract WHERE Numero_du_contrat__c =: resPost.payload.contract_id];
        } catch (Exception ex) {
            insert WebServiceLog.generateLog(System.now(), request.requestURI, '404', String.valueof(request.requestBody), ex.getMessage() );
         	
            response.statusCode = 404;
            return ex.getMessage();   
        }

            
        if (resOrder.state == 'closed.completed' && dtvalue(resOrder.dateClosed) != null) {
            CoordonneesBancairesHandler.notifiedBySlimpay = true;
            update new sofactoapp__Coordonnees_bancaires__c(Id = sc.sofactoapp_Rib_prelevement__c, Date_signature_du_mandat__c = dtvalue(resOrder.dateClosed));
            
            insert WebServiceLog.generateLog( System.now(), request.requestURI, '201', request.requestBody.toString(), null);
            
        } else if (resOrder.state != 'closed.completed') {
            return 'Le statut entré n\'est pas complété';
        }
        else if (resOrder.dateClosed == null) {
            return 'La date de fin est vide';
        }
        
        response.statusCode = 201;
        return null;
    }
    private static DateTime dtvalue(String str) {
        if (str == null)
            return null;
        
        str = str.substringBeforeLast('.');

        str = str.replace('T', ' ');
        //str = str.replace('.', ':');
        return Datetime.valueOfGMT(str);
    }
}