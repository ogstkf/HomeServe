/**
 * @File Name          : LogementTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 21/01/2020, 11:49:15
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0     17/10/2019         AMO                   Initial Version
 * 1.1     27/12/2019         SH                    Changes in after Insert to handle the new Name notation
 * 1.2     27/12/2019         SH                    Added before Update 
 * 1.3     04/06/2020         DMU                   Commented Code related to AP59_UpdtLogSousSecteur due to HomeServe Project
**/
public with sharing class LogementTriggerHandler {
    
    Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    
    public void createAccount(List<Logement__c> lstNewLogement){
        system.debug('### In createAccount_UpdLogement');

        list<Account> accListIns = new list<Account>();
        List<Logement__c> lstLogementToUpd = new List<Logement__c>();

        for(Logement__c lo : lstNewLogement){

            if(string.IsNotBlank(lo.Owner_Name__c)){
                system.debug('### Owner_Name__c not null');
                
                Account accToIns = new Account(TECH_LogementId__c=lo.Id, FirstName=lo.Owner_Name__c, LastName=lo.Owner_Surname__c, PersonEmail=lo.Owner_E_mail__c, Phone=lo.Owner_Phone__c, PersonMobilePhone=lo.Owner_Mobile_Phone__c);
                accListIns.add(accToIns);                
            }
        }
        try{
            Insert accListIns;
        }catch(Exception e){
            system.debug('### Account insert Fail: '+e.getMessage());

        }
    }

    //CT-1102
    public void handleBeforeInsert(List<Logement__c> lstLogement){

        List<Logement__c> lstLogementToUpdOwner = new list <Logement__c>();
        
        //DMU - 4/6/20 - commented below variable related to AP59_UpdtLogSousSecteur due to HomeServe
        // List<Logement__c> lstLogToUpdateSubAgency = new List<Logement__c>();

        for(integer i=0;i<lstLogement.size();i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP25')){
                if(lstLogement[i].Inhabitant__c != null){
                    lstLogementToUpdOwner.add(lstLogement[i]);
                }
            }
            //DMU - 4/6/20 - commented below code related to AP59_UpdtLogSousSecteur due to HomeServe
            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP59')){
            //     if(String.isNotBlank(lstLogement[i].Postal_Code__c)){
            //         lstLogToUpdateSubAgency.add(lstLogement[i]);
            //     }
            // }           
        }
        if(lstLogementToUpdOwner.size()>0){
            AP25_UpdateAddressWhenLogementCreated.UpdateOwner(lstLogementToUpdOwner);
        }

        //DMU - 4/6/20 - commented below code related to AP59_UpdtLogSousSecteur due to HomeServe
        // if(lstLogToUpdateSubAgency.size()>0){
        //     AP59_UpdtLogSousSecteur.updateSousSecteurFromAddress(lstLogToUpdateSubAgency);
        // }
    }

    public void handleAfterInsert(List<Logement__c> lstLogement){
        // Added SH - 2019-12-27
        Id RecordTypeId_Logement =  Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Logement').getRecordTypeId();
        Id RecordTypeId_Lot =       Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Lot').getRecordTypeId();
        Id RecordTypeId_Market =    Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Market').getRecordTypeId();
        // Added BCH TEC-519 16/02/2021
        Id RecordTypeId_Souslot =    Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Sous_lot').getRecordTypeId();

        List<Id> logementIds = new List<Id>();

        for (Logement__c logement : lstLogement) {
            logementIds.add (logement.Id);
        }

        List<Logement__c> Logements = [SELECT Id, Name, RecordTypeId, TECH_Auto_Number__c FROM Logement__c WHERE Id IN : logementIds];

        for (Logement__c logement : Logements) {
                 if (logement.RecordTypeId == RecordTypeId_Logement) { logement.Name = 'LG-'; }
            else if (logement.RecordTypeId == RecordTypeId_Lot)      { logement.Name = 'LT-'; }
            else if (logement.RecordTypeId == RecordTypeId_Market)   { logement.Name = 'MA-'; }
            // Added BCH TEC-519 16/02/2021
            else if (logement.RecordTypeId == RecordTypeId_Souslot)   { logement.Name = 'SL-'; }
            else { logement.Name = 'XX-'; }
            logement.Name += logement.TECH_Auto_Number__c;
        }
        update Logements; // Mandatory, because TECH_Auto_Number__c has its value only AFTER the after insert
        // End added SH

        List<Logement__c> lstLogementToUpdAccAddr = new list <Logement__c>();
        // SH - 2020-01-27 - Not to be used in PROD
        // List<Logement__c> lstUpdtAcc = new List<Logement__c>();

        for(integer i=0;i<lstLogement.size();i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP25')){
                if(lstLogement[i].Inhabitant__c != null){
                    lstLogementToUpdAccAddr.add(lstLogement[i]);
                }
            }

            // SH - 2020-01-27 - Not to be used in PROD
            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP43')){
            //     if(lstLogement[i].Adresse_de_facturation__c == 'Adresse du Logement' && lstLogement[i].Inhabitant__c != null){
            //         lstUpdtAcc.add(lstLogement[i]);
            //     }
            // }       
        }
        if(lstLogementToUpdAccAddr.size()>0){
            AP25_UpdateAddressWhenLogementCreated.UpdateAddressLatest(lstLogementToUpdAccAddr);
        }
        // SH - 2020-01-27 - Not to be used in PROD
        // if(lstUpdtAcc.size()>0){
        //     AP43_UpdateAddressLgmnt.UpdateAccountAddress(lstUpdtAcc);
        // }      
    }

    public void handleBeforeUpdate(List<Logement__c> lstOldLogement, List<Logement__c> lstNewLogement){
        // Added SH - 2019-12-27
        Id RecordTypeId_Logement =  Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Logement').getRecordTypeId();
        Id RecordTypeId_Lot =       Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Lot').getRecordTypeId();
        Id RecordTypeId_Market =    Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Market').getRecordTypeId();
        // Added BCH TEC-519 16/02/2021
        Id RecordTypeId_Souslot =    Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Sous_lot').getRecordTypeId();
        for (Logement__c logement : lstNewLogement) {
                 if (logement.RecordTypeId == RecordTypeId_Logement) { logement.Name = 'LG-'; }
            else if (logement.RecordTypeId == RecordTypeId_Lot)      { logement.Name = 'LT-'; }
            else if (logement.RecordTypeId == RecordTypeId_Market)   { logement.Name = 'MA-'; }
            // Added BCH TEC-519 16/02/2021
            else if (logement.RecordTypeId == RecordTypeId_Souslot)   { logement.Name = 'SL-'; }
            else { logement.Name = 'XX-'; }
            logement.Name += logement.TECH_Auto_Number__c;
        }
        // End added SH

        //DMU - 4/6/20 - commented below code related to AP59_UpdtLogSousSecteur due to HomeServe
        // List<Logement__c> lstLogToUpdateSubAgency = new List<Logement__c>();
        // for(Integer i=0; i<lstNewLogement.size(); i++){
        //     if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP59')){
        //         if(lstNewLogement[i].Postal_Code__c != lstOldLogement[i].Postal_Code__c){
        //             lstLogToUpdateSubAgency.add(lstNewLogement[i]);
        //         }
        //     }
        // }

        // if(lstLogToUpdateSubAgency.size()>0){
        //     AP59_UpdtLogSousSecteur.updateSousSecteurFromAddress(lstLogToUpdateSubAgency);
        // }
    }

    public void handleAfterUpdate(List<Logement__c> lstOldLogement, List<Logement__c> lstNewLogement){
        // SH - 2020-01-27 - Not to be used in PROD
        // List<Logement__c> lstUpdtLgmnt = new List<Logement__c>();

        // for(integer i=0;i<lstNewLogement.size();i++){
        //     if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP43')){
        //         if(lstNewLogement[i].Adresse_de_facturation__c == 'Adresse du Logement' && lstNewLogement[i].Inhabitant__c != null && lstNewLogement[i].Inhabitant__c != lstOldLogement[i].Inhabitant__c){
        //             lstUpdtLgmnt.add(lstNewLogement[i]);
        //         }
        //     }

        //     System.debug('List of logement to update: ' + lstUpdtLgmnt);            
        // }

        // if(lstUpdtLgmnt.size()>0){
        //     AP43_UpdateAddressLgmnt.LogementUpdated(lstUpdtLgmnt);
        // }      
    }

}