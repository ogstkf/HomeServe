/**
 * @File Name         : WS15_DiabolocomApi_TEST.cls
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 19-01-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   13-01-2022   MNA   Initial Version
**/
@isTest
public with sharing class WS15_DiabolocomApi_TEST {
    static User mainUser;
    static List<Account> testLstAcc;
    static Contact testCon;
    static List<String> lstAccFlds = new List<String>();
    static List<String> lstConFlds = new List<String>();

    static {
        mainUser = TestFactory.createAdminUser('WS14@test.com', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser) {
            lstAccFlds = getPhoneFields('Account');
            lstConFlds = getPhoneFields('Contact');

            testLstAcc = new List<Account> {
                TestFactory.createAccount('WS15_DiabolocomApi1_TEST'),
                TestFactory.createAccountBusiness('WS15_DiabolocomApi2_TEST')
            };

            testLstAcc[0].BillingPostalCode = '1233456';
            testLstAcc[0].recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            for (Integer i = 0; i < lstAccFlds.size(); i++) {
                testLstAcc[0].put(lstAccFlds[i], '12345' + String.valueOf(i).leftPad(3,'0'));
            }

            testLstAcc[1].BillingPostalCode = '1233457';
            testLstAcc[1].recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('BusinessAccount').getRecordTypeId();

            insert testLstAcc;

            testCon = new Contact(LastName = 'Test Con', AccountId = testLstAcc[1].Id, Email = 'test@spoon.com');
            for (Integer i = 0; i < lstConFlds.size(); i++) {
                testCon.put(lstConFlds[i], '12346' + String.valueOf(i).leftPad(3,'0'));
            }
            insert testCon;
        }
    }

    @isTest
    static void testCheckClient1() {

    }
    
    @isTest
    static void testCheckClient2() {
        
    }
    
    @isTest
    static void testCheckClient3() {
        
    }

    static List<String> getPhoneFields (String ObjType) {
        List<String> lstPhoneFld = new List<String>();
        Map<String, Schema.SObjectField> mapFields =  Schema.getGlobalDescribe().get(objType).getDescribe().fields.getMap();

        
        for (String key : mapFields.keySet()) {
            if (String.valueOf(mapFields.get(key).getDescribe().getType()) == 'PHONE' ) 
                lstPhoneFld.add(key);
        }

        return lstPhoneFld;
    }
}