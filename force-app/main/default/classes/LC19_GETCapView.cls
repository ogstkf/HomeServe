public with sharing class LC19_GETCapView {
            /**
 * @File Name          : 
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         29-11-2019           ANA         Initial Version
**/

     @AuraEnabled 
    public static List<CAP__c> getCaps(Integer index,Integer currYear) { 

        
        System.debug('index cont : '+index);
        System.debug('Yaerssss : '+currYear);

        Integer currentMonth = index+1;
        if(currentMonth>12){
            currentMonth = 1;
            currYear = currYear + 1;
        }
        Integer m = currentMonth-1;
        Integer mPlus1 = currentMonth;
        Integer mPlus2 = currentMonth+1;

        Integer previousYr = 0;
        if(m==0){
            m=12;
            previousYr = currYear-1;
        }

        //SBH Start
        Integer monthM;
        Integer monthMplus1;
        Integer monthMMinu1;
        Integer yearM;
        Integer yearMplus1;
        Integer yearMminus1;

        Date d0 = Date.newInstance(currYear, currentMonth , 01);
        Date dPlus1 = d0.addMonths(1);
        Date dMinus1 = d0.addMonths(-1);
        
        monthM = currentMonth;
        monthMplus1 = dPlus1.Month();
        monthMMinu1 = dMinus1.Month();

        yearM = currYear;
        yearMplus1 = dPlus1.Year();
        yearMminus1 = dMinus1.Year();
        //SBH End

        System.debug('##months: ' + monthM + '#' + monthMplus1 + '#' + monthMMinu1);
        System.debug('##Years: ' + yearM + '#' + yearMplus1 + '#' + yearMminus1);
        // System.debug('## variables : m, m+1, m+2: ' + m + ' - ' + mPlus1 + ' - ' + mPlus2);
        // System.debug('## currentYear/previous year: ' + currYear + ' - ' + previousYr);

        user userConnected = [SELECT id, companyName FROM User WHERE id=: UserInfo.getUserId()];

        List<CAP__c> lstCp = [SELECT Service_Territory__r.Name,Year__c,Month__c,Agency_VE_Capacity_Forecast__c,BudgetVEMoisM__c,BudgetVEActualisLisse__c,CapacitVEConfigure__c 
                            FROM CAP__c 
                            WHERE Service_Territory__r.Name like :('%' + userConnected.companyName + '%') 
                            AND ( (Month__c = :monthMMinu1 AND Year__c = :String.valueOf(yearMminus1) ) OR (Month__c = :monthM AND Year__c = :String.valueOf(yearM)) OR (Month__c = :monthMplus1 AND Year__c = :String.valueOf(yearMplus1)))
                            ORDER BY Service_Territory__r.Name,Year__c,Month__c DESC
                            LIMIT 30];
        system.debug(lstCp); 

        return lstCp;

    }

}