/**
 * @File Name          : AP21_PartialOrderManagement_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 03/02/2020, 09:46:18
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    03/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class AP21_PartialOrderManagement_TEST {
    
static User mainUser;

    static Order ord ;
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
    static Shipment ship = new Shipment();
    static ProductTransfer prodTrans = new ProductTransfer();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static OrderItem ordItem = new OrderItem();

    static{
        mainUser = TestFactory.createAdminUser('AP22@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccountBusiness('AP22_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();

            insert testAcc;

             //create products
            prod = TestFactory.createProduct('testProd');
            prod.Lot__c = 1;
            insert prod;


            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            // System.debug('##### TEST lstPrcBk: '+lstPrcBk);

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;


            //create Order
            ord = new Order(AccountId = testAcc.Id
                                    ,Name = 'Test1'
                                    ,EffectiveDate = System.today()
                                    ,Status = 'Annulé'
                                    ,Pricebook2Id = lstPrcBk[0].Id
                                    ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId());

            insert ord;

            ordItem = new OrderItem(
                            OrderId=ord.Id,
                            Product2Id=prod.Id,
                            Quantity=decimal.valueof('1'),
                            PricebookEntryId=PrcBkEnt.Id,
                            UnitPrice = 145);
        
            insert ordItem;

            System.debug('##### TEST ord: '+ord);

            ship = new Shipment (ShipToName = 'Test1',
                                    Order__c = ord.Id,
                                    Status = 'En attente',
                                    RecordTypeId=Schema.SObjectType.Shipment.getRecordTypeInfosByName().get(AP_Constant.RTReceptionFournisseur).getRecordTypeId());

            insert ship;

            prodTrans = new ProductTransfer(ShipmentId = ship.Id,
                        Product2Id = prod.Id,
                        Quantity_Lot_received__c = 2,
                        QuantityReceived = 2,
                        QuantitySent = 3,
                        IsReceived =false);

            insert prodTrans;
             
        }
    }

    @isTest
    public static void testupdateOrderItem(){
        System.runAs(mainUser){
            prodTrans.IsReceived = true;
            Test.startTest();
                update prodTrans;
            Test.stopTest();

            OrderItem lstNewOrdItem = [SELECT Id , Quantite_recue__c
                                                FROM OrderItem
                                                WHERE OrderId =: ord.Id];
            

            System.assertEquals(lstNewOrdItem.Quantite_recue__c ,prodTrans.Quantity_Lot_received__c);
        }
    }

}