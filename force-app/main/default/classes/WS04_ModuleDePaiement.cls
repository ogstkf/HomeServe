@RestResource(urlMapping='/WS04_ModuleDePaiement_V2/*')
global without sharing class WS04_ModuleDePaiement {    
	
    global static map<string,string> mapTestData = new map <string,string>();

	@HttpPost
    global static ResponseWrapper savePayment(){
    	system.debug('### In method savePayment');

    	Map <String, String> mapResponse = new Map <String, String>();
		try{
			system.debug('###RestContext.request: '+RestContext.request);
            //system.debug('###params: '+RestContext.request.params);

            map <string,string> listParams = Test.isRunningTest() == true ?  mapTestData : RestContext.request.params; 
            system.debug('###listParams size: '+listParams.size());

            //Testing purpose only - To get all params sent in request from Clic&Pay
            //for(string s : listParams.keySet()){
            //    system.debug('###listParams s: '+s);
            //}
            system.debug('### RestContext.request.params.get signature : '+RestContext.request.params.get('signature'));
            system.debug('### calculated sig from AP06: '+AP06_CreateSignature.calculateSignature(listParams)); 

            if (AP06_CreateSignature.calculateSignature(listParams) != RestContext.request.params.get('signature')){
                //le script devra lever une exception et avertir le marchand de l'anomalie.
                mapResponse.put('ERREUR', 'signature non conforme');
            }else{
                system.debug('### in else traitement donnee: ');

                list <Contract> listContract = new list<Contract>();
                if(RestContext.request.params.get('vads_order_id') != null 
                    || RestContext.request.params.get('vads_order_id') != '' 
                    || string.IsNotBlank(RestContext.request.params.get('vads_order_id')))
                {
                    listContract = [select id from Contract where ContractNumber =: RestContext.request.params.get('vads_order_id') limit 1];
                }  

                list<Account> acclist = new list <Account>();  
                  if(RestContext.request.params.get('vads_cust_id') != null 
                    || RestContext.request.params.get('vads_cust_id') != '' 
                    || string.IsNotBlank(RestContext.request.params.get('vads_cust_id')))
                {
                    acclist = [select id from Account where ClientNumber__c =: RestContext.request.params.get('vads_cust_id') limit 1];
                }

                system.debug('### RestContext.request.params.get vads_trans_id:  '+ RestContext.request.params.get('vads_trans_id'));
                Payment__c pa = new Payment__c();
                list<Payment__c> listPaymentExis = [select id from Payment__c where Payment_ID__c =: RestContext.request.params.get('vads_trans_id') limit 1];

                if(listPaymentExis.size()>0){
                    pa = listPaymentExis[0]; //To update this pa
                }
                //Insert new pa
                    pa.vads_amount__c = RestContext.request.params.get('vads_amount');
                    pa.vads_auth_result__c = RestContext.request.params.get('vads_auth_result');
                    pa.vads_bank_code__c = RestContext.request.params.get('vads_bank_code');
                    pa.vads_bank_product__c = RestContext.request.params.get('vads_bank_product');
                    pa.vads_brand_management__c = '';
                    pa.vads_capture_delay__c = RestContext.request.params.get('vads_capture_delay');
                    pa.vads_card_brand__c = RestContext.request.params.get('vads_card_brand');
                    pa.vads_card_country__c = RestContext.request.params.get('vads_card_country');
                    pa.vads_ctx_mode__c = RestContext.request.params.get('vads_ctx_mode');
                    pa.vads_currency__c = RestContext.request.params.get('vads_currency');
                    pa.vads_order_id__c = RestContext.request.params.get('vads_order_id');
                    pa.vads_payment_config__c = RestContext.request.params.get('vads_payment_config');
                    pa.vads_sequence_number__c = Integer.valueOf(RestContext.request.params.get('vads_sequence_number'));
                    pa.vads_threeds_enrolled__c = RestContext.request.params.get('vads_threeds_enrolled');
                    pa.vads_threeds_status__c = RestContext.request.params.get('vads_threeds_status');
                    pa.vads_trans_date__c = RestContext.request.params.get('vads_trans_date');
                    pa.vads_trans_status__c = RestContext.request.params.get('vads_trans_status');
                    pa.vads_url_check_src__c = RestContext.request.params.get('vads_url_check_src');
                    pa.vads_version__c = RestContext.request.params.get('vads_version');         
                    pa.vads_site_id__c = RestContext.request.params.get('vads_site_id');
                    pa.vads_page_action__c = RestContext.request.params.get('vads_page_action');
                    pa.vads_action_mode__c = RestContext.request.params.get('vads_action_mode');
                    pa.vads_product_labelN__c = RestContext.request.params.get('vads_product_labelN');
                    pa.vads_product_refN__c = RestContext.request.params.get('vads_product_refN');
                    pa.vads_cust_email__c = RestContext.request.params.get('vads_cust_email');
                    pa.vads_cust_id__c = RestContext.request.params.get('vads_cust_id');
                    pa.vads_cust_title__c = RestContext.request.params.get('vads_cust_title');
                    pa.vads_cust_status__c = RestContext.request.params.get('vads_cust_status');
                    pa.vads_cust_first_name__c = RestContext.request.params.get('vads_cust_first_name');
                    pa.vads_cust_last_name__c = RestContext.request.params.get('vads_cust_last_name');
                    pa.vads_cust_country__c = RestContext.request.params.get('vads_cust_country');
                    pa.vads_trans_id__c = RestContext.request.params.get('vads_trans_id');
                                    
                List<Payment__c> paymentList = new List<Payment__c>();                

                if(listContract.size()>0){
                    pa.Contrat__c = listContract[0].Id;
                }

                if(acclist.size()>0){
                    pa.Account__c = acclist[0].Id;
                }

                paymentList.add(pa);

                try{
                    if(listPaymentExis.size()>0){
                        system.debug('###In update paymentList');
                        update paymentList;
                        mapResponse.put('SUCCESS', 'Paiement mis a jour sur Salesforce');
                    }else{
                        system.debug('###In insert paymentList');
                        insert paymentList;
                        mapResponse.put('SUCCESS', 'Paiement créé sur Salesforce');
                    }                  
                }catch(exception ex){
                    system.debug('###In catch FAIL 0000'+ ex.getMessage());
                    mapResponse.put('ERREUR', ex.getMessage());                    
                }            
            }            
        }catch(exception ex){
        	system.debug('###In catch FAIL 111' + ex.getMessage());
            mapResponse.put('ERREUR', ex.getMessage());            
        }
        system.debug('###mapResponse: '+mapResponse);
        return new ResponseWrapper(mapResponse);       
    }
    
    global class ResponseWrapper{
        global List <Map <String, String>> PaymentResponse;
        global ResponseWrapper(Map <String, String> mapResponse){
            PaymentResponse = new List <Map <String, String>>();
            PaymentResponse.add(mapResponse);
        }
    }
}