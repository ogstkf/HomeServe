/**
 * @File Name          : AP14_SetCaseStatus.cls
 * @Description        : Set Case Status depending on RDV's and WO's status
 * @Author             : DMU/MGR
 * @Group              : Spoon Consulting
 * @Last Modified By   : RRJ
 * @Last Modified On   : 13-12-2021
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    06/09/2019, 13:30:30          DMU                     Initial Version
 * 1.1    21/11/2019					RRJ						Added rules for work Order CT-1205
 * 1.2	  17/01/2020					SH						Correct problem when update on multiple SA that belong to the same CASE
 * 1.3    28/01/2020               		SH          			Added WO StartDate & EndDate
**/
public class AP14_SetCaseStatus {

	//Case status
	public static string caStatusPendingActFromClient = 'Pending Action from Client';
	public static string caStatusInProg = 'In Progress';
	public static string caStatusClosedVEAbs = 'Closed VE Absence';
	public static string caStatusPendingActFromCham = 'Pending Action from Cham';
	public static string caStatusEnAttenteCham = 'En attente de Cham';
	public static string caStatusEnAttenteClient = 'En attente du Client';
	public static string caStatusClosed = 'Closed';

	//SA Status
	public static string saStatusDoneClientAbsent = 'Done client absent';
	public static string saStatusImpossibleToFinish = 'Impossible to finish';
	public static string saStatusDoneOK = 'Done OK';
	public static string saStatusDoneKO = 'Done KO';

	//WO Type__c
	public static string woTypeMaintenance = 'Maintenance';
	public static string woTypeInstallation = 'Installation';

	//WO Status
	public static string woStatusTermineEtNonConcluant = 'Done KO';
	public static string woStatusTermineEtConcluant = 'Done OK';
	public static string woStatusTermineEtClientAbsent = 'Done client absent';

	//WO and SA Open status
	public static List<String> lstOpenWoStatus = new List<String>{
		AP_Constant.wrkOrderStatusNouveau
		,AP_Constant.wrkOrderStatusInProgress
	};

	public static List<String> lstOpenSaStrings = new List<String>{
		AP_Constant.servAppStatusInProgress,
		AP_Constant.servAppStatusEnAttenteClient,
		AP_Constant.servAppStatusDispatched,
		AP_Constant.servAppStatusOnHold,
		AP_Constant.servAppStatusNone,
		AP_Constant.servAppStatusScheduled
	};

	public static void caseStatus(list<ServiceAppointment> lstNewServApp, Set<Id> setCasesIds) {
		System.debug ('*** in caseStatus');

		map<String, String> mapServAppIdWOCaseId = new map<String, String>();
		Set<case> lstCaseToUpd = new Set<case>(); // Changed SH
		map<string, ContractLineItem> mapContractLineItem = new map<string, ContractLineItem>();
		map<string, string> mapSACLI = new map<string, string>();
		Map<String, String> mapServAppIdToWOId = new Map<String, String>(); // RRJ 20191121 CT-1205
		List<WorkOrder> lstWoToUpd = new List<WorkOrder>(); // RRJ 20191121 CT-1205
		Set<Id> setCaseIdOpenWO = new Set<Id>();
		Set<Id> setCaseIdOpenSa = new Set<Id>();

		for(ServiceAppointment sa :lstNewServApp) {
			// if(sa.TECH_WorkOrderStatus__c == AP_Constant.wrkOrderStatusNouveau || sa.TECH_WorkOrderStatus__c == AP_Constant.wrkOrderStatusInProgress){
			// 	setCaseIdOpenWO.add(sa.TECH_WorkOrderCaseId__c);
			// }
			if(sa.Status == AP_Constant.servAppStatusInProgress || sa.Status == AP_Constant.servAppStatusEnAttenteClient || sa.Status == AP_Constant.servAppStatusDispatched || sa.Status == AP_Constant.servAppStatusOnHold || sa.Status == AP_Constant.servAppStatusNone || sa.Status == AP_Constant.servAppStatusScheduled ){
				setCaseIdOpenSa.add(sa.TECH_WorkOrderCaseId__c);
			}
			mapServAppIdWOCaseId.put(sa.Id, sa.TECH_WorkOrderCaseId__c);
			mapSACLI.put(sa.Id, sa.TECH_WOCaseCLI__c);
			mapServAppIdToWOId.put(sa.Id, sa.Work_Order__c); // RRJ 20191121 CT-1205
		}

		//SBH: Select other cases and WOs
		List<WorkOrder> lstWosForCases = [
			SELECT Id, Status, CaseId
			FROM WorkOrder 
			WHERE CaseId IN :setCasesIds
			AND Status IN :lstOpenWoStatus
			AND Id NOT IN :mapServAppIdToWOId.values()
		];

		List<ServiceAppointment> lstSasForCases = [
			SELECT Id, Status, TECH_WorkOrderCaseId__c
			FROM ServiceAppointment
			WHERE TECH_WorkOrderCaseId__c IN :setCasesIds
			AND Status IN :lstOpenWoStatus
			AND Id NOT IN :lstNewServApp
		]; 

		System.debug('## lstWosCases : ' + lstWosForCases);
		System.debug('## lstSasForCases' + lstSasForCases);

		for(WorkOrder wo: lstWosForCases){
			System.debug('## wo: ' + wo);
			setCaseIdOpenWO.add(wo.CaseId);
		}

		for(ServiceAppointment sa : lstSasForCases){
			System.debug('## sa: ' + sa);
			setCaseIdOpenSa.add(sa.TECH_WorkOrderCaseId__c);
		}	

		System.debug('## setCaseIdOpenWO : ' + setCaseIdOpenWO);
		System.debug('## setCaseIdOpenSa : ' + setCaseIdOpenSa);
		//END SBH

		for(ContractLineItem cli :[SELECT id, Customer_Absence_Range__c, Customer_Absences__c, Customer_Allowed_Absences__c FROM ContractLineItem WHERE id IN :mapSACLI.values()]) {
			mapContractLineItem.put(cli.Id, cli);
		}
		System.debug ('** mapContractLineItem size: ' + mapContractLineItem.size());

		for(ServiceAppointment sa :lstNewServApp) {
			System.debug ('###### sa status: ' + sa.Status);
			Case caToUpd = new Case(id = mapServAppIdWOCaseId.get(sa.Id));
			WorkOrder woToUpd = new WorkOrder(Id = mapServAppIdToWOId.get(sa.Id));

			if((sa.TECH_WorkOrderType__c != woTypeMaintenance && sa.Status == saStatusDoneClientAbsent) || (sa.Status == saStatusImpossibleToFinish || sa.Unattainable_installation__c == true)) {

				System.debug ('** in condition 1');
				caToUpd.status = caStatusPendingActFromClient;
			}

			// RRJ 20191121 CT-1205
			if(sa.Status == saStatusDoneClientAbsent){
				woToUpd.Status = woStatusTermineEtClientAbsent;
				woToUpd.StartDate = System.now().addHours(-1); // SH - 2020-01-28
				woToUpd.EndDate = System.now(); // SH - 2020-01-28
			}

			if(sa.Status == saStatusDoneClientAbsent
			   && sa.TECH_WorkOrderType__c == woTypeMaintenance
			   && mapContractLineItem.containsKey(mapSACLI.get(sa.Id))
			   && mapContractLineItem.get(mapSACLI.get(sa.Id)).Customer_Absences__c <= mapContractLineItem.get(mapSACLI.get(sa.Id)).Customer_Allowed_Absences__c) {
				System.debug ('** in condition 2');
				caToUpd.status = caStatusInProg;
			}

			if(sa.Status == saStatusDoneClientAbsent
			   && sa.TECH_WorkOrderType__c == woTypeMaintenance
			   && mapContractLineItem.containsKey(mapSACLI.get(sa.Id))
			   && mapContractLineItem.get(mapSACLI.get(sa.Id)).Customer_Absences__c > mapContractLineItem.get(mapSACLI.get(sa.Id)).Customer_Allowed_Absences__c) {

				System.debug ('** in condition 3');
				caToUpd.status = caStatusClosedVEAbs;
			}

			if(sa.Status == saStatusDoneClientAbsent && sa.TECH_WorkOrderType__c == woTypeInstallation) {
				System.debug ('** in condition 4');
				caToUpd.status = caStatusPendingActFromCham;
			}

			if(sa.Status == saStatusDoneKO) {
				System.debug ('** in condition 5');
				caToUpd.status = caStatusPendingActFromCham;
				woToUpd.Status = woStatusTermineEtNonConcluant; // RRJ 20191121 CT-1205
				woToUpd.StartDate = System.now().addHours(-1); // SH - 2020-01-28
				woToUpd.EndDate = System.now(); // SH - 2020-01-28
			}

			if(sa.Status == saStatusDoneOK) {
				System.debug ('** in condition 6');
				woToUpd.Status = woStatusTermineEtConcluant; // RRJ 20191121 CT-1205
				// woToUpd.StartDate = System.now().addHours(-1); // SH - 2020-01-28
				// woToUpd.EndDate = System.now(); // SH - 2020-01-28
				woToUpd.StartDate = sa.EarliestStartTime;
				woToUpd.EndDate = sa.DueDate;
				if(!setCaseIdOpenSa.contains(caToUpd.Id) && !setCaseIdOpenWO.contains(caToUpd.Id)){
					caToUpd.status = caStatusClosed;
				}
			}

			// RRJ 20191121 CT-1205
			if(sa.status == saStatusImpossibleToFinish){
				woToUpd.Status = woStatusTermineEtNonConcluant;
				woToUpd.StartDate = System.now().addHours(-1); // SH - 2020-01-28
				woToUpd.EndDate = System.now(); // SH - 2020-01-28
			}

			//DGI
			if(sa.status == saStatusImpossibleToFinish && sa.TECH_WorkOrderStatus__c == woStatusTermineEtNonConcluant) {
				caToUpd.status = caStatusEnAttenteClient;
			}

			lstCaseToUpd.add(caToUpd);
			if(woToUpd != null) lstWoToUpd.add(woToUpd);
		}
		if(lstWoToUpd.size()>0){
			update lstWoToUpd;
		}
		if(lstCaseToUpd.size() > 0) {
			// When we update many SA at the same time that belong to a unique CASE, in the previous LIST, were some duplicates.
			// So, it was changed to a SET and then converted to a LIST before the final UPDATE
			List<Case> lstCaseToUpd2 = new List<Case>(); // Added SH
			lstCaseToUpd2.addAll(lstCaseToUpd); // Added SH
			update lstCaseToUpd2; // Changed SH
		}
	}
}