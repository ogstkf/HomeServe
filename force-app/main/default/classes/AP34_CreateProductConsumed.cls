/**
 * @File Name          : AP34_CreateProductConsumed.cls
 * @Description        : 
 * @Author             : SBH
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 4/22/2020, 12:09:44 PM
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    28/10/2019         SBH                    Initial Version  (CT-1162)
**/
public with sharing class AP34_CreateProductConsumed {

    public static void createProdConsumed(List<ServiceAppointment> lstSas, Set<Id> setWkOrderIds, Map<Id, Set<Id>> mapWoToSas){

        // Maps for calculations: 
        Map<Id, Id> mapWorkOrderToLocation = new Map<Id, Id>();
        Set<String> setProductLocations = new Set<String>();
        List<ProductItem> lstProductItems = new List<ProductItem>();
        Map<String, ProductItem> mapProductLocationToProductItem = new Map<String, ProductItem>();
        Map<Id, ServiceAppointment> mapSaToUpdate = new Map<Id, ServiceAppointment>();
        
        //CT-1584
        List<WorkOrder> lstWowithPR = [SELECT Id, Location.Agence__c, Location.Agence__r.Inventaire_en_cours__c, LocationId, (SELECT Id, Quantite_utilisee__c, Product2Id, Product2.RecordType.DeveloperName FROM ProductsRequired  ) FROM WorkOrder WHERE Id IN :setWkOrderIds];
        List<WorkOrder> lstWoToCreateProductCon = new List<WorkOrder>();
        for(WorkOrder wo : lstWowithPR){

            if(wo.Location.Agence__c != null && wo.Location.Agence__r.Inventaire_en_cours__c == true){
                if(mapWoToSas.containsKey(wo.Id)){
                    for(Id SAid: mapWoToSas.get(wo.Id)){
                        System.debug('## current SAId : ' + SAid);
                        mapSaToUpdate.put(SAId, new ServiceAppointment(Id = SAId, Inventaire_en_cours__c=true));
                    }
                }

            }else{
                mapWorkOrderToLocation.put(wo.Id, wo.LocationId);
                lstWoToCreateProductCon.add(wo);
            }
        }
        
        // CT-1584
        for(WorkOrder wo: lstWoToCreateProductCon){

            mapWorkOrderToLocation.put(wo.Id, wo.LocationId); 
            
            if(!wo.ProductsRequired.isEmpty()){ 
                for(ProductRequired pr : wo.ProductsRequired){
                    if(pr.Product2Id != null){
                        setProductLocations.add(pr.Product2Id + '_' + wo.LocationId);
                    }
                }
            }

        }

        //CT 1417
        Map<Id, ServiceAppointment> mapAppToUpdateInvEnCours = new Map<Id, ServiceAppointment>(); 
        //retrieve product items: 
        if(setProductLocations != null){
            lstProductItems = [SELECT ID, Location.Agence__c, Location.Agence__r.Inventaire_en_cours__c, Product2Id, TECH_ProductLocation__c, LocationId, QuantityOnHand FROM ProductItem WHERE TECH_ProductLocation__c IN :setProductLocations ]; 

            for(ProductItem pi : lstProductItems){
                mapProductLocationToProductItem.put(pi.TECH_ProductLocation__c , pi);
            }
        }

        List<ProductConsumed> lstProductConsumedToInsert = new List<ProductConsumed>();

        // CT-1584
        for(WorkOrder wo : lstWoToCreateProductCon){
            for(ProductRequired pr : wo.ProductsRequired){
                if(pr.Product2Id != null && (pr.Product2.RecordType.DeveloperName == 'Article' || pr.Product2.RecordType.DeveloperName == 'Product')){
                    String productLocation = pr.Product2Id + '_' + wo.LocationId;

                    ProductItem associatedProductItem = mapProductLocationToProductItem.get(productLocation);
                    if(associatedProductItem <> null){
                        if (associatedProductItem.QuantityOnHand > 0 && pr.Quantite_utilisee__c > 0){
                            ProductConsumed prConsumed = new ProductConsumed();
                            prConsumed.WorkOrderId = wo.Id;
                            prConsumed.ProductItemId = associatedProductItem.Id;
    
                            if(associatedProductItem.QuantityOnHand < pr.Quantite_utilisee__c){
                                prConsumed.QuantityConsumed = associatedProductItem.QuantityOnHand;
                            }else{
                                prConsumed.QuantityConsumed = pr.Quantite_utilisee__c;
                            }
    
                            lstProductConsumedToInsert.add(prConsumed);
                        }
                    }
                }
            }
        }

        if(!lstProductConsumedToInsert.isEmpty()){
            insert lstProductConsumedToInsert;
        }

        if(!mapSaToUpdate.isEmpty()){
            update mapSaToUpdate.values();
        }
        
    }
}