/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 08-06-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-03-2020   AMO   Initial Version
**/
@isTest
public with sharing class FSL_WorkOrderHandler_Test {
    static User adminUser;
    static MaintenancePlan mp = new MaintenancePlan();
    static MaintenancePlan mp2 = new MaintenancePlan();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static Campaign testCampaign = new Campaign();
    static ServiceTerritory servTerritory = new ServiceTerritory();
    static OperatingHours opHrs = new OperatingHours();
    static Account a = new Account();
    static Contact ct = new Contact();
    static workType wt = new workType();
    static workType wt1 = new workType();
    static Logement__c  l = new Logement__c ();
    static Asset as1 = new Asset ();
    static Entitlement e = new  Entitlement ();
    static Entitlement e1 = new  Entitlement ();
    static Case c = new Case();
    static Map<Id, Asset> assets = new Map<Id, Asset>();
    static Set<String> requiredValues = new Set<String>();
    static Set<String> requiredValues1 = new Set<String>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<Bundle__c> lstTestBundle = new List<Bundle__c>();
    static List<Product_Bundle__c> lstTestBundleProd = new List<Product_Bundle__c>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<Account> lstTestAcc = new List<Account>();
    
    static Set<String> retWType = new Set<String>();
    static Set<String> retWReason = new Set<String>();
    static Set<String> retWEquipment_type = new Set<String>();
    
    static Bypass__c bp = new Bypass__c();
    
    @TestSetup
    static void makeData(){
        adminUser = TestFactory.createAdminUser('FSL_WorkOrderHandler@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;
        
        bp.SetupOwnerId  = adminUser.Id;
        bp.Bypass_Process_Builder__c = true;
        insert bp;
        
        System.runAs(adminUser){   
            
            //create accounts
            lstTestAcc.add(TestFactory.createAccount('testAcc'+1));
            lstTestAcc[0].RecordTypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
            lstTestAcc[0].BillingPostalCode = '1234'+0;
            insert lstTestAcc;
            
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc[0].Id);
            
            lstTestAcc[0].sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update lstTestAcc;
            
            Contact ct =new Contact(LastName='Test', FirstName = 'Test');
            insert ct;
            
            
            mp = new MaintenancePlan();
            mp.Frequency = 2;
            mp.FrequencyType = 'Weeks';
            mp.MaintenanceWindowStartDays = 1;
            mp.MaintenanceWindowEndDays = 5;
            mp.GenerationTimeframe = 5;
            mp.GenerationTimeframeType = 'Weeks';
            mp.NextSuggestedMaintenanceDate = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            mp.DoesAutoGenerateWorkOrders = true;
            mp.GenerationHorizon = 5;
            mp.StartDate =  Date.newInstance(2019, 11, 20);
            mp.EndDate  = Date.newInstance(2020, 1, 20);
            insert mp;
            
            mp2 = new MaintenancePlan();
            mp2.Frequency = 2;
            mp2.FrequencyType = 'Weeks';
            mp2.MaintenanceWindowStartDays = 1;
            mp2.MaintenanceWindowEndDays = 5;
            mp2.GenerationTimeframe = 5;
            mp2.GenerationTimeframeType = 'Weeks';
            mp2.NextSuggestedMaintenanceDate = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            mp2.DoesAutoGenerateWorkOrders = true;
            mp2.GenerationHorizon = 5;
            mp2.StartDate =  Date.newInstance(2019, 11, 20);
            mp2.EndDate  = Date.newInstance(2020, 1, 20);
            insert mp2;
            
            a=new Account(Name='Test');
            insert a;
            
            Product2 product2  = new Product2(Name = 'Test' , Equipment_type1__c = 'Chaudière gaz', isActive = true) ;
            insert Product2;
            wt=new workType();
            wt.Type__c='Maintenance' ;
            wt.Reason__c='Visite sous contrat';
            wt.Equipment_type__c='Chaudière gaz';
            wt.FSL__Due_Date_Offset__c=12;
            wt.Name='Test';
            wt.EstimatedDuration=12;
            wt.Type_de_client__c = 'Tous';
            //wt.DurationType='';
            //insert wt;
            wt1=new workType();
            wt1.Type__c='Maintenance' ;
            wt1.Reason__c='Visite hors contrat';
            wt1.Equipment_type__c='Chaudière gaz';
            wt1.FSL__Due_Date_Offset__c=12;
            wt1.Name='Test';
            wt1.EstimatedDuration=12;
            wt1.Type_de_client__c = 'Tous';
            //wt.DurationType='';
            //insert wt1;
            
            
            List<workType> lstWt = new List<workType>{
                new workType(Type__c='Maintenance', Reason__c='Visite sous contrat', Equipment_type__c='Chaudière gaz', FSL__Due_Date_Offset__c=12,
                        Name='Test', EstimatedDuration=12, Agence__c = '520', Type_de_client__c ='Tous'),
                new workType(Type__c='Maintenance', Reason__c='Visite hors contrat', Equipment_type__c='Chaudière gaz', FSL__Due_Date_Offset__c=12,
                        Name='Test', EstimatedDuration=12, Agence__c = '520', Type_de_client__c ='Tous'),
                new workType(Type__c='Troubleshooting', Equipment_type__c='Chaudière gaz', FSL__Due_Date_Offset__c=12,
                        Name='Test', EstimatedDuration=12, Agence__c = '520', Type_de_client__c ='Tous'),

                new workType(Type__c='Maintenance', Reason__c='Visite sous contrat', Equipment_type__c='Chaudière gaz', FSL__Due_Date_Offset__c=12,
                        Name='Test', EstimatedDuration=12, Agence__c = 'Toutes', Type_de_client__c ='Tous'),
                new workType(Type__c='Maintenance', Reason__c='Visite hors contrat', Equipment_type__c='Chaudière gaz', FSL__Due_Date_Offset__c=12,
                        Name='Test', EstimatedDuration=12, Agence__c = 'Toutes', Type_de_client__c ='Tous'),
                new workType(Type__c='Troubleshooting', Equipment_type__c='Chaudière gaz', FSL__Due_Date_Offset__c=12,
                        Name='Test', EstimatedDuration=12, Agence__c = 'Toutes', Type_de_client__c ='Tous')
                        
            };
            insert lstWt;
            retWType = new Set<String>();
            retWType.add('Maintenance');
            
            retWReason = new Set<String>();
            retWReason.add('Visite sous contrat');
            
            retWEquipment_type = new Set<String>();
            retWEquipment_type.add('Chaudière gaz');
            
            l=new Logement__c();
            insert l;
            
            as1=new Asset();
            as1.AccountId=a.Id;
            as1.Product2Id=product2.id;
            as1.Logement__c=l.id;
            as1.Name='Test';
            insert as1;
            //create products
            lstTestProd.add(TestFactory.createProduct('testProd'+0));
            lstTestProd[0].IsBundle__c = true;
            insert lstTestProd;
            
            //create bundles
            lstTestBundle.add(TestFactory.createBundle('testBundle', 150));
            insert lstTestBundle;
            // System.debug('##### TEST lstTestBundle: '+lstTestBundle);
            
            //create bundle products related to bundle and product
            lstTestBundleProd.add(TestFactory.createBundleProduct(lstTestBundle[0].Id, lstTestProd[0].Id));
            insert lstTestBundleProd;
            
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );
            
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            // System.debug('##### TEST lstPrcBk: '+lstPrcBk);
            
            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[0].Id, 0));
            insert lstPrcBkEnt;
            
            
            
            testCampaign = new Campaign(Name = 'TestCampaign');
            insert testCampaign;
            
            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;
            
            sofactoapp__Raison_Sociale__c raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;
            
            servTerritory = TestFactory.createServiceTerritory('SGS - test Territory', opHrs.Id,raisonSocial.Id);
            servTerritory.Agency_Code__c = '520';
            insert servTerritory;
            
            e=new Entitlement();
            e.AssetId=as1.id;
            e.Name='E_test';
            e.Type='Total Failure';
            e.accountId=a.id;
            insert e;
            
            e1=new Entitlement();
            e1.AssetId=as1.id;
            e1.Name='E_test';
            e1.Type='Partial failure';
            e1.accountId=a.id;
            insert e1;
            
            requiredValues = new Set<String>();
            requiredValues.add(e.Id);
            
            requiredValues1 = new Set<String>();
            requiredValues1.add(e1.Id);
            
            c=new Case();
            c.Number_of_WO__c=1;
            c.AssetId=as1.id;
            c.Type='Troubleshooting';
            c.EntitlementId=e.id;
            c.Local_Agency__c = servTerritory.Id;
            insert c;
            
            Case c2=new Case();
            c2.Number_of_WO__c=-1;
            c2.AccountId = a.Id;
            c2.ContactId = ct.Id;
            c2.Status = 'In Progress';
            c2.Type='Maintenance';
            c2.Reason__c='Visite hors contrat';
            c2.EntitlementId=e.id;
            c2.Campagne__c = testCampaign.Id;
            c2.Local_Agency__c = servTerritory.Id;
            c2.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            
            insert c2;
            
            WorkOrder w2=new WorkOrder();
            w2.Type__c='Maintenance';
            w2.AccountId = c2.AccountId ;
            w2.Reason__c='Visite hors contrat';
            w2.ContactId = c2.ContactId;
            w2.MaintenancePlanId = mp.Id;
            //w2.ServiceContractId = lstServCon[0].Id;
            w2.SuggestedMaintenanceDate = Date.newInstance(2019, 9, 20);
            w2.CaseId=c.id;
            w2.AssetId = as1.Id;
            w2.Campagne__c = c2.Campagne__c;
            w2.ServiceTerritoryId =  c2.Local_Agency__c;
            insert w2;
            lstServCon = new List<ServiceContract>{
                new ServiceContract(Agency__c = servTerritory.Id, Name = 'Test1', AccountId = a.Id,
                                          RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId()),
                new ServiceContract(Agency__c = servTerritory.Id, Name = 'Test2', AccountId = a.Id, 
                                          RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId()),
                new ServiceContract(Agency__c = servTerritory.Id, Name = 'Test3', AccountId = a.Id,
                                          RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_collectifs_publics').getRecordTypeId())
            };         
            insert lstServCon;
            
            assets.put(w2.assetId, as1);
            c2.Subject = w2.Type__c;
            c2.TECH_WO_FSL__c = w2.Id;
            update c2;
            
        }
    }
    @isTest
    public static void test_FSL_WorkOrderHandler_Test2(){
        //    System.runAs(adminUser){
        // lstServCon.add(TestFactory.createServiceContract('test serv con 1', lstTestAcc[0].Id));
        // lstServCon[0].Type__c = 'Individual';
        // lstServCon[0].Contract_Status__c = 'Active';
        // lstServCon[0].Pricebook2Id = lstPrcBk[0].Id;
        // lstServCon[0].Asset__c = as1.Id;
        // lstServCon[0].Frequence_de_maintenance__c = 2;
        // lstServCon[0].Type_de_frequence__c = 'Weeks';
        // lstServCon[0].Debut_de_la_periode_de_maintenance__c = 3;
        // lstServCon[0].Fin_de_periode_de_maintenance__c = 5;
        // lstServCon[0].Periode_de_generation__c = 6;
        // lstServCon[0].Type_de_periode_de_generation__c = 'Weeks';
        // lstServCon[0].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
        // lstServCon[0].Generer_automatiquement_des_OE__c = true;
        // lstServCon[0].Horizon_de_generation__c = 5;
        // lstServCon[0].StartDate =  Date.newInstance(2019, 12, 20);
        // insert lstServCon;
        
        //contract line item
        // lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));
        // insert lstConLnItem;
        FSL_WorkOrderHandler.isCreateCA = true;
        as1 = [SELECT Id FROM Asset LIMIT 1];
        
        MaintenancePlan mp = new MaintenancePlan();
        mp.Frequency = 2;
        mp.FrequencyType = 'Weeks';
        mp.MaintenanceWindowStartDays = 1;
        mp.MaintenanceWindowEndDays = 5;
        mp.GenerationTimeframe = 5;
        mp.GenerationTimeframeType = 'Weeks';
        mp.NextSuggestedMaintenanceDate = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
        mp.DoesAutoGenerateWorkOrders = true;
        mp.GenerationHorizon = 5;
        mp.StartDate =  Date.newInstance(2019, 11, 20);
        mp.EndDate  = Date.newInstance(2020, 1, 20);
        insert mp;
        
        MaintenancePlan mp2 = new MaintenancePlan();
        mp2.Frequency = 2;
        mp2.FrequencyType = 'Weeks';
        mp2.MaintenanceWindowStartDays = 1;
        mp2.MaintenanceWindowEndDays = 5;
        mp2.GenerationTimeframe = 5;
        mp2.GenerationTimeframeType = 'Weeks';
        mp2.NextSuggestedMaintenanceDate = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
        mp2.DoesAutoGenerateWorkOrders = true;
        mp2.GenerationHorizon = 5;
        mp2.StartDate =  Date.newInstance(2019, 11, 20);
        mp2.EndDate  = Date.newInstance(2020, 1, 20);
        insert mp2;
        
        Id ServConInd = [SELECT Id FROM ServiceCOntract WHERE RecordType.DeveloperName = 'Contrats_Individuels' LIMIT 1].Id;
        Id ServConPublic = [SELECT Id FROM ServiceCOntract WHERE RecordType.DeveloperName = 'Contrats_collectifs_publics' LIMIT 1].Id;
        Id ServConPriv = [SELECT Id FROM ServiceCOntract WHERE RecordType.DeveloperName = 'Contrats_Collectifs_Prives' LIMIT 1].Id;
        
        WorkOrder w=new WorkOrder();
        w.Type__c='Troubleshooting';
        w.AccountId = a.Id;
        w.ContactId = ct.Id;
        w.MaintenancePlanId = mp.Id;
        w.CaseId=c.id;
        w.AssetId = as1.Id;
        w.Campagne__c = testCampaign.Id;
        w.ServiceTerritoryId = servTerritory.Id;
        w.SuggestedMaintenanceDate = Date.newInstance(2019, 9, 20);
        // w.ServiceContractId = lstServCon[0].Id;
        
        WorkOrder w3=new WorkOrder();
        w3.Type__c='Troubleshooting';
        w3.AccountId = a.Id;
        w3.ContactId = ct.Id;
        w3.Reason__c='';
        w3.caseId = null;
        w3.MaintenancePlanId = mp.Id;
        w3.AssetId = as1.Id;
        w3.Campagne__c = testCampaign.Id;
        w3.ServiceTerritoryId = servTerritory.Id;
        w3.SuggestedMaintenanceDate = Date.newInstance(2019, 9, 20);
        w3.ServiceContractId = ServConPublic;
        
        WorkOrder w0=new WorkOrder();
        w0.Type__c='Maintenance';
        w0.Reason__c='Visite hors contrat';
        w0.AccountId = a.Id;
        w0.ContactId = ct.Id;
        w0.MaintenancePlanId = mp.Id;
        //w0.CaseId=c.id;
        w0.AssetId = as1.Id;
        w0.Campagne__c = testCampaign.Id;
        w0.ServiceTerritoryId = servTerritory.Id;
        w0.SuggestedMaintenanceDate = Date.newInstance(2019, 9, 20);
        w0.ServiceContractId = ServConPriv;
        
        WorkOrder w1=new WorkOrder();
        w1.Type__c='Troubleshooting';
        w1.AccountId = a.Id;
        w1.ContactId = ct.Id;
        w1.MaintenancePlanId = mp.Id;
        w1.AssetId = as1.Id;
        w1.Campagne__c = testCampaign.Id;
        w1.ServiceTerritoryId = servTerritory.Id;
        w1.SuggestedMaintenanceDate = Date.newInstance(2019, 9, 20);
        // w1.ServiceContractId = lstServCon[0].Id;
        
        WorkOrder w2=new WorkOrder();
        w2.Type__c='Maintenance';
        w2.Reason__c='Visite sous contrat';
        w2.AccountId = a.Id;
        w2.ContactId = ct.Id;
        w2.MaintenancePlanId = mp.Id;
        w2.AssetId = as1.Id;
        w2.Campagne__c = testCampaign.Id;
        w2.ServiceTerritoryId = servTerritory.Id;
        w2.SuggestedMaintenanceDate = Date.newInstance(2019, 9, 20);
        w2.CaseId=[select id from Case where Reason__c=null limit 1].id;
        w2.WorkTypeId = wt1.Id;
        //  w2.ServiceContractId = lstServCon[0].Id;
        WorkOrder w5=new WorkOrder();
        w5.Type__c='Maintenance';
        w5.Reason__c='Visite sous contrat';
        w5.AssetId = as1.Id;
        w5.ServiceTerritoryId = servTerritory.Id;
        w5.ServiceContractId = ServConInd;
        
        List<WorkOrder> list_w=new List<WorkOrder>{w,w1,w0, w3, w2, w5};
        insert list_w;
        update list_w;
        delete list_w;
        
        Case c1=[select id, Number_of_WO__c, AssetId, Type, EntitlementId from Case where Reason__c=null limit 1];
        update c1;
        delete c1;
        Test.startTest();
        Set<Id> idCase = FSL_WorkOrderHandler.getCasesIds(list_w);
        Id idwoGetEntitlementValue = FSL_WorkOrderHandler.woGetEntitlementValue (w,assets, 'Type', requiredValues); 
        Id idwoGetEntitlementValue1 = FSL_WorkOrderHandler.woGetEntitlementValue (w3,assets, 'Type', requiredValues1); 
        Map<String,Map<String,Map<String, WorkType>>> ret = FSL_WorkOrderHandler.buildWorkTypesMap(retWType, retWReason, retWEquipment_type);
        
        Test.stopTest();
        System.assertNotEquals(null, idCase);
        System.assertEquals(null, idwoGetEntitlementValue);
        System.assertEquals(null, idwoGetEntitlementValue1);
       
    }



    @isTest
    public static void test_FSL_WorkOrderHandler_Test() {
       
       /* WorkType worktype = new WorkType(Name = 'Test', Type__c = 'Troubleshooting' , Reason__c = 'Noise',  EstimatedDuration = 7, Equipment_type__c = 'Chaudière gaz');
        insert worktype;
        WorkType worktype2 = new WorkType( Name = 'Test2', Type__c ='Claim' , Reason__c = 'Intervention', EstimatedDuration = 7, Equipment_type__c = 'Chaudière gaz');
        insert worktype2;

        Product2 product2  = new Product2(Name = 'Test' , Equipment_type1__c = 'Chaudière gaz') ;
        insert Product2;
        Logement__c logement = new Logement__c();
        insert logement;
        Account account = new Account(Name = 'Test');
        insert account;
        Asset asset = new Asset(Name = 'Test',Product2Id = product2.Id ,AccountId = account.Id, Logement__c = logement.Id, Equipment_type__c = 'Chaudière gaz');
        insert asset;
        Case case1  = new Case(AssetId = asset.Id, Type = 'Troubleshooting', Reason__c = 'Noise', Origin = 'Email', Status = 'New');
        insert case1;
        Case case2  = new Case(AssetId = asset.Id, Type = 'Troubleshooting', Reason__c = 'Total Failure', Origin = 'Phone', Status = 'New');
        insert case2;
        WorkOrder workorder = new workorder(CaseId = case1.Id);
        insert workorder;
        WorkOrder workorder1 = new workorder(CaseId = case1.Id, Type__c = 'Claim', Reason__c = 'Intervention');
        insert workorder1;
        WorkOrder workorder2 = new workorder(CaseId = case2.Id);
        insert workorder2;


        WorkOrder WorkOrderTest = [SELECT WorkTypeId FROM WorkOrder WHERE Id = :workorder.Id limit 1];
        system.assertEquals(WorkOrderTest.WorkTypeId, worktype.Id);

        WorkOrder WorkOrderTest1 = [SELECT WorkTypeId FROM WorkOrder WHERE Id = :workorder1.Id limit 1];
        system.assertEquals(WorkOrderTest1.WorkTypeId, worktype2.Id);

        WorkOrder WorkOrderTest2 = [SELECT WorkTypeId FROM WorkOrder WHERE Id = :workorder2.Id limit 1];
        system.assertNotEquals(WorkOrderTest2.WorkTypeId, worktype.Id);*/

    }
    
}