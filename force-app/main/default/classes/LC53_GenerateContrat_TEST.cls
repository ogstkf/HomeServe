/**
 * @File Name          : LC53_GenerateContrat_TEST.cls
 * @Description        : 
 * @Author             : YGO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 09-11-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         16/01/2020               YGO                       Initial Version
**/
@isTest
public with sharing class LC53_GenerateContrat_TEST {

    static User mainUser;
    static List<ContractLineItem> lstConlineItems = new List<ContractLineItem>();
    static List<Product2> lstProduct2 = new List<Product2>();
    static List<ServiceContract> lstServiceContracts = new List<ServiceContract>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static Account testAcc = new Account();
    static Product2 product;
    static List<ContentDocumentLink> lstCdl = new List<ContentDocumentLink>();
	static List<ContentVersion> lstConVer = new List<ContentVersion>();

    static{
        
        mainUser = TestFactory.createAdminUser('James@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){

            testAcc = TestFactory.createAccount('Test1');
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;


            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //service contract
            lstServiceContracts.add(TestFactory.createServiceContract('GenereContract0',testAcc.Id));
            lstServiceContracts.add(TestFactory.createServiceContract('GenereContract1',testAcc.Id));
            lstServiceContracts.add(TestFactory.createServiceContract('GenereContract2',testAcc.Id));
            lstServiceContracts.add(TestFactory.createServiceContract('GenereContract3',testAcc.Id));
            lstServiceContracts.add(TestFactory.createServiceContract('GenereContract4',testAcc.Id));
            lstServiceContracts.add(TestFactory.createServiceContract('GenereContract5',testAcc.Id));
            lstServiceContracts.add(TestFactory.createServiceContract('GenereContract6',testAcc.Id));
            lstServiceContracts.add(TestFactory.createServiceContract('GenereContract7',testAcc.Id));
            lstServiceContracts[0].PriceBook2Id = lstPrcBk[0].Id;
            lstServiceContracts[1].PriceBook2Id = lstPrcBk[0].Id;
            lstServiceContracts[2].PriceBook2Id = lstPrcBk[0].Id;
            lstServiceContracts[3].PriceBook2Id = lstPrcBk[0].Id;
            lstServiceContracts[4].PriceBook2Id = lstPrcBk[0].Id;
            lstServiceContracts[5].PriceBook2Id = lstPrcBk[0].Id;
            lstServiceContracts[6].PriceBook2Id = lstPrcBk[0].Id;
            lstServiceContracts[7].PriceBook2Id = lstPrcBk[0].Id;
            insert lstServiceContracts;

            //create products            
            lstProduct2.add(TestFactory.createProduct('testProd0'));  
            lstProduct2.add(TestFactory.createProduct('testProd1'));  
            lstProduct2.add(TestFactory.createProduct('testProd2'));  
            lstProduct2.add(TestFactory.createProduct('testProd3'));  
            lstProduct2.add(TestFactory.createProduct('testProd4'));  
            lstProduct2.add(TestFactory.createProduct('testProd5'));  
            lstProduct2.add(TestFactory.createProduct('testProd6'));  
            lstProduct2.add(TestFactory.createProduct('productelse'));  

            lstProduct2[0].IsBundle__c = true;          
            lstProduct2[0].Equipment_family__c = 'Chaudière';          
            lstProduct2[0].Equipment_type1__c = 'Chaudière gaz';

            lstProduct2[1].Equipment_family__c = 'Pompe à chaleur';  
            lstProduct2[1].IsBundle__c = true;          
            
            lstProduct2[2].IsBundle__c = true;          
            lstProduct2[2].Equipment_family__c = 'Chauffe eau';          
            lstProduct2[2].Equipment_type1__c = 'Chauffe-eau solaire';         
            
            lstProduct2[3].IsBundle__c = true;          
            lstProduct2[3].Equipment_family__c = 'Chauffe eau';                    
            lstProduct2[3].Equipment_type1__c = 'Chauffe-eau gaz';             
            
            lstProduct2[4].IsBundle__c = true;           
            lstProduct2[4].Equipment_family__c = 'Chaudière';          
            lstProduct2[4].Equipment_type1__c = 'Chaudière fioul';    
            
            lstProduct2[5].IsBundle__c = true;             
            lstProduct2[5].Equipment_family__c = 'Chaudière';                 
            lstProduct2[5].Equipment_type1__c = 'Chaudière fioul';      
            
            lstProduct2[6].IsBundle__c = true;                  
            lstProduct2[6].Equipment_family__c = 'Chaudière';          
            lstProduct2[6].Equipment_type1__c = 'Poêle à granulés';     
            
            lstProduct2[7].IsBundle__c = true;                  
            lstProduct2[7].Equipment_family__c = 'Chaudière';          
            lstProduct2[7].Equipment_type1__c = 'Poêle à granulés'; 
            insert lstProduct2;

            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProduct2[0].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProduct2[1].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProduct2[2].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProduct2[3].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProduct2[4].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProduct2[5].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProduct2[6].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProduct2[7].Id, 150));
            insert lstPrcBkEnt;

            //contract line item
            lstConlineItems.add(TestFactory.createContractLineItem(lstServiceContracts[0].Id, lstPrcBkEnt[0].Id, 150, 1)); 
            lstConlineItems.add(TestFactory.createContractLineItem(lstServiceContracts[1].Id, lstPrcBkEnt[1].Id, 150, 1));  
            lstConlineItems.add(TestFactory.createContractLineItem(lstServiceContracts[2].Id, lstPrcBkEnt[2].Id, 150, 1));  
            lstConlineItems.add(TestFactory.createContractLineItem(lstServiceContracts[3].Id, lstPrcBkEnt[3].Id, 150, 1));  
            lstConlineItems.add(TestFactory.createContractLineItem(lstServiceContracts[4].Id, lstPrcBkEnt[4].Id, 150, 1));  
            lstConlineItems.add(TestFactory.createContractLineItem(lstServiceContracts[5].Id, lstPrcBkEnt[5].Id, 150, 1));  
            lstConlineItems.add(TestFactory.createContractLineItem(lstServiceContracts[6].Id, lstPrcBkEnt[6].Id, 150, 1));  
            lstConlineItems.add(TestFactory.createContractLineItem(lstServiceContracts[7].Id, lstPrcBkEnt[7].Id, 150, 1));  
            insert lstConlineItems;

            //content version
            lstConVer = new List<ContentVersion>{
                                        new ContentVersion(
                                                    Title = 'testFile.pdf', 
                                                    Description = 'This is a test class file',
                                                    OwnerId = mainUser.Id,
                                                    PathOnClient = 'testFile.pdf',
                                                    VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body')
                                        )
            };
            insert lstConVer;
        }
    }

    @isTest
    public static void test_saveContrat(){
        system.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.saveContrat(lstServiceContracts[0].Id);
            Test.stopTest();
        }
    }

    @isTest
    public static void test_pompeChaleur(){
        system.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.saveContrat(lstServiceContracts[1].Id);
            Test.stopTest();
        }
    }

    @isTest
    public static void test_chauffeEauSolaire(){
        system.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.saveContrat(lstServiceContracts[2].Id);
            Test.stopTest();
        }
    }

    @isTest
    public static void test_chauffeEauGaz(){
        system.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.saveContrat(lstServiceContracts[3].Id);
            Test.stopTest();
        }
    }

    @isTest
    public static void test_chaudierefioul(){
        system.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.saveContrat(lstServiceContracts[4].Id);
            Test.stopTest();
        }
    }

    @isTest
    public static void test_radiateurGaz(){
        system.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.saveContrat(lstServiceContracts[5].Id);
            Test.stopTest();
        }
    }

    @isTest
    public static void test_poeleGranule(){
        system.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.saveContrat(lstServiceContracts[6].Id);
            Test.stopTest();
        }
    }

    @isTest
    public static void test_contractFalse(){
        system.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.saveContrat(lstServiceContracts[7].Id);
            Test.stopTest();

            System.assertEquals(mapOfResult.get('message'), 'SUCESS!!!');
        }
    }

    @isTest
    public static void test_contractinsertImage(){
        system.runAs(mainUser){
            String id = lstServiceContracts[7].Id;
            Test.startTest();
                Map<String, Object> mapOfResult =  LC53_GenerateContrat.insertImageAttachment(id);
            Test.stopTest();
        }
    }
}