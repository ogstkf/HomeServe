/**
 * @description       : 
 * @author            : ANR
 * @group             : 
 * @last modified on  : 01-19-2021
 * @last modified by  : ANR
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   01-19-2021   ANR   Initial Version
**/
public with sharing class AP77_SetPriceBookOnOrder {

    public static void setPriceBook(List<Order> lstNewOrder){
		system.debug('** in AP77_SetPriceBookOnOrder');
               
        map<String, String> mapPBAccAgence = new map<String, String>();
        map<String, String> mapPBAcc = new map<String, String>();

        for (Pricebook2 p : [SELECT Id,Compte__c,Agence__c FROM Pricebook2 WHERE RecordType.DeveloperName = 'Achat' AND isActive = true AND Compte__c !=null]){
            mapPBAccAgence.put(p.Compte__c+'_'+p.Agence__c, p.Id);
            if(string.IsBlank(p.Agence__c)){
                mapPBAcc.put(p.Compte__c, p.Id);
            }            
        }

        for(Order ord : lstNewOrder){       
            ord.Pricebook2Id = mapPBAccAgence.containsKey(ord.AccountId+'_'+ord.Agence__c) ? mapPBAccAgence.get(ord.AccountId+'_'+ord.Agence__c) : (mapPBAcc.containsKey(ord.AccountId) ? mapPBAcc.get(ord.AccountId) : null) ;              
        }
    }

}