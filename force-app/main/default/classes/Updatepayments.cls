/** 
 * author: Souleymane from Sofacto
 * Version 1.0
 * Date 24/11/2019
 */

global class Updatepayments implements 
    Database.Batchable<sObject>, Database.Stateful {
    
    // instance member to retain state across transactions
    private Integer currentYear;
    private Integer currentMonth;    
    global Integer recordsProcessed = 0;
   	global String [] email = new String[] {'souleymane12@gmail.com'};
    global Database.QueryLocator start(Database.BatchableContext bc) {
        this.currentYear = Date.today().year();
        this.currentMonth = Date.today().month();
        System.debug('currentYear = ' + currentYear);
        System.debug('currentMonth = ' + currentMonth);
        return Database.getQueryLocator(
            'SELECT ID, sofactoapp__Mode_de_paiement__c, sofactoapp__En_attente__c, tech_year__c, tech_month__c, statut_du_paiement__c '  +
            'FROM sofactoapp__R_glement__c ' + 
            'Where sofactoapp__Mode_de_paiement__c = \'Prélèvement\' AND (tech_year__c = :currentYear AND sofactoapp__En_attente__c = true) AND TECH_AccountSource__c != \'SOWEE\' AND tech_month__c = :currentMonth '
        );
    }
    global void execute(Database.BatchableContext bc, List<sofactoapp__R_glement__c> scope){
        // process each batch of records
        List<sofactoapp__R_glement__c> listReglement = new List<sofactoapp__R_glement__c>();
        for (sofactoapp__R_glement__c reg : scope) {
                reg.statut_du_paiement__c = AP_Constant.reglementStatutPaiementEncaisse;
                
                // add the reglement  to list to be updated
                listReglement.add(reg);
            	System.debug('### reglement  processed');
                // increment the instance member counter
                recordsProcessed = recordsProcessed + 1;
            
        }
       
        try{
            System.debug('### in upd');
            update listReglement;
        }catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
        }
    }    
        
    global void finish(Database.BatchableContext bc){
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      
      // Below code will fetch the job Id
      AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors,
      a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById,
      a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];
      
      // get the job Id
      System.debug('$$$ Jobid is'+BC.getJobId());
      
      // below code will send an email to User about the status
      mail.setToAddresses(email);
      mail.setReplyTo('s.sidahmed@sofacto.com'); // Add here your email address
      mail.setSenderDisplayName('Apex Batch updte reglement Module');
      mail.setSubject('Batch Processing '+a.Status);
      mail.setPlainTextBody('The Batch Apex job processed'
         + a.TotalJobItems+'batches with '+a.NumberOfErrors+'failures'+'Job Item processed are'+a.JobItemsProcessed);
      
      Messaging.sendEmail(new Messaging.Singleemailmessage [] {mail});

    }   
  

}