/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 26-10-2021
 * Modifications Log 
 * ===========================================================
 * Ver   Date               Author          Modification
 * ===========================================================
 * 1.0   15-10-2021         MNA             Initial Version
**/
public without sharing class LC79_CloneQuote {
    @AuraEnabled
    public static String clone(String quoId, String quoName) {
        String oppId;
        String cloneQuoId;
        String cloneOppId;
        String quoQuery = buildQuery('Quote');
        String qliQuery = buildQuery('QuoteLineItem');
        String oppQuery = buildQuery('Opportunity');
        String oliQuery = buildQuery('OpportunityLineItem');

        Quote quo;
        Opportunity opp;
        Opportunity cloneOpp;
        List<QuoteLineItem> lstQli;
        List<OpportunityLineItem> lstOli;

        quo = Database.query(quoQuery);
        lstQli = Database.query(qliQuery);

        if (quo != null)
            oppId = quo.OpportunityId;

        opp = Database.query(oppQuery);
        lstOli = Database.query(oliQuery);

        cloneOppId = cloneOpp(opp);
        cloneOLI(lstOli, cloneOppId);

        cloneQuoId = cloneQuote(quo, quoName, cloneOppId);
        cloneQLI(lstQli, cloneQuoId);

        if (cloneOppId != '') {
            try {
                update new Opportunity(Id = cloneOppId, Tech_Synced_Devis__c = cloneQuoId);
                update new Quote(Id = cloneQuoId, Name = quoName);
            }
            catch(DmlException ex) {
                throw new AP_Constant.CustomException('Error update clone quote : ' + ex);
            }
        }
        return cloneQuoId;
    }

    private static String cloneQuote(Quote quo, String quoName, String newOppId) {
        Quote cloneQuo = quo.clone(false, false, false, false);
        cloneQuo.Name = quoName;
        cloneQuo.Status = 'Draft';
        cloneQuo.Devis_signe_par_le_client__c = false;
        cloneQuo.Date_de_signature_du_client__c = null;
        cloneQuo.Acompte_verse__c = false;
        cloneQuo.Sofacto_Collectedbytech__c = false;
        cloneQuo.Facturation_post_intervention__c = false;
        cloneQuo.Mode_de_paiement_du_devis__c = null;
        cloneQuo.Mode_de_paiement_de_l_acompte__c = null;
        cloneQuo.Montant_de_l_acompte_verse__c = null;
        cloneQuo.Date_du_paiement_de_l_acompte__c = null;
        cloneQuo.Deuxieme_acompte__c = false;
        cloneQuo.Pourcentage_du_deuxieme_acompte__c = null;
        cloneQuo.OpportunityId = newOppId;
        try {
            insert cloneQuo;
            return cloneQuo.Id;
        }
        catch(DmlException ex) {
            throw new AP_Constant.CustomException('Error clone quote : ' + ex);
        }
    }
    
    private static void cloneQLI(List<QuoteLineItem> lstQli, String newQuoId) {
        List<QuoteLineItem> lstCloneQLI = new List<QuoteLineItem>();
        for (QuoteLineItem qli : lstQli) {
            QuoteLineItem cloneQli = qli.clone(false, false, false, false);
            cloneQli.QuoteId = newQuoId;
            lstCloneQLI.add(cloneQli);
        }
        if(lstCloneQLI.size() > 0) {
            try {
                insert lstCloneQLI;
            }
            catch(DmlException ex) {
                throw new AP_Constant.CustomException('Error clone quote line item : ' + ex);
            }
        }
    }

    private static String cloneOpp(Opportunity opp) {
        Opportunity cloneOpp = opp.clone(false, false, false, false);
        try {
            insert cloneOpp;
            return cloneOpp.Id;
        }
        catch(DmlException ex) {
            throw new AP_Constant.CustomException('Error clone   : ' + ex);
        }
    }

    private static void cloneOLI(List<OpportunityLineItem> lstOli, String newOppId) {
        List<OpportunityLineItem> lstCloneOLI = new List<OpportunityLineItem>();
        for (OpportunityLineItem oli : lstOli) {
            OpportunityLineItem cloneOli = oli.clone(false, false, false, false);
            cloneOli.OpportunityId = newOppId;
            lstCloneOLI.add(cloneOli);
        }
        if(lstCloneOLI.size() > 0) {
            try {
                insert lstCloneOLI;
            }
            catch(DmlException ex) {
                throw new AP_Constant.CustomException('Error clone opportunity line item : ' + ex);
            }
        }
    }

    private static String buildQuery(String objType) {
        String query = 'SELECT ';
        List<String> queryFields = new List<String>();
        Map<String, Schema.SObjectField> mapMesField =  Schema.getGlobalDescribe().get(objType).getDescribe().fields.getMap();
        Integer i = 0;
        for (String key : mapMesField.keySet()) {
            if (objType.toLowerCase() != 'opportunitylineitem'
                    || (objType.toLowerCase() == 'opportunitylineitem' && key.toLowerCase() != 'totalprice'))   //only one of unit price or total price may be specified  
                
                
                queryFields.add(key);

        }
        query += String.join(queryFields, ',');
        query += ' FROM ' + objType;
        query += ' WHERE ';
        if (objType.toLowerCase() == 'quote') {
            query += ' Id =: quoId';
        }
        else if (objType.toLowerCase() == 'quotelineitem') {
            query += ' QuoteId =: quoId';
        }
        else if (objType.toLowerCase() == 'opportunity') {
            query += ' Id =: oppId';
        }
        else if (objType.toLowerCase() == 'opportunitylineitem') {
            query += ' OpportunityId =: oppId';
        }
        
        return query;
    }
}