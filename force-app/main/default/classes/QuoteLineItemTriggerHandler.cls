/**
 * @File Name          : QuoteLineItemTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 18-05-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/10/2019   AMO     Initial Version
**/
/**
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0        11-10-2019            AMO         Initial Version
 * 1.01       21-10-2019            LGO         HandleAfterDelete: delete all opportunitylineitem relate to quotelineitem
**/

    

public with sharing class QuoteLineItemTriggerHandler {

    Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    public static Boolean notRun = false;

    public void handleAfterInsert(List<QuoteLineItem> lstNewQLI){
        system.debug('*** in handleAfterInsert');
        
        list <QuoteLineItem> lstQLI = new list <QuoteLineItem> ();
        list <QuoteLineItem> lstQliStandard = new list <QuoteLineItem> ();

        //SBH: 1086 retour 2020_02_05
        Map<Id, Id> mapQuoteToServiceTerritory = new Map<Id, Id>();


        //List<QuoteLineItem> lstPrReq = new List<QuoteLineItem>();
        for(QuoteLineItem qli : lstNewQLI){

            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('LC08')){
                if(qli.TECH_QuoteIsSync__c && qli.TECH_Counter__c == 0 && !notRun
                    && qli.TECH_QuoteRecordTypeDevName__c == 'Devis_standard'
                ){
                    lstQLI.add(qli);
                    // System.debug('###### in QLI after ins trigger for SYNC'+notRun);
                }
                // else{
                //     System.debug('###### NOT in QLI  after ins trigger for SYNC'+notRun);
                // }
            }
            
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP36')){
                if(qli.TECH_QuoteRecordTypeDevName__c == 'Devis_standard'){
                    lstQliStandard.add(qli);
                    mapQuoteToServiceTerritory.put(qli.QuoteId, qli.TECH_QuoteAgency__c);
                }
            }

        }
        if(lstQLI.size()>0){
            LC08_SyncQuote.createOppLine(lstQLI);
        }
        
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP36')){
            if(!notRun){ // RRJ 20200505 CT-1717
                AP36_QuoteProductTransfers.createProductRequestLineItem(lstQliStandard, mapQuoteToServiceTerritory);
            }
            
        }
        
         if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP30')){
            // AP30_QuoteProductManagement.createProductRequiredLineItem(lstQliStandard);
        }
    }
    
    public void handleAfterDelete(List<QuoteLineItem> lstQuoNew,List<QuoteLineItem> lstQuoOld) {
        System.debug('### handleAfterDelete Quote- START');

        System.debug('### lstQuoNew = ' + lstQuoNew);
        System.debug('### lstQuoOld = ' + lstQuoOld);
        
        Set<Id> setQuoId = new Set<Id>();
        Set<Id> setQuoteId = new Set<Id>();    
        List<Quote> toDeleteAIde = new List<Quote>();
        List<Quote> lstValueQuoteAide = new List<Quote>(); 
        List<Opportunity> lstValueOppAide = new List<Opportunity>();

        for(integer i=0;i<lstQuoOld.size();i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('LC08')){
                if(lstQuoOld[i].TECH_QuoteIsSync__c && !notRun
                    && lstQuoOld[i].TECH_QuoteRecordTypeDevName__c == 'Devis_standard'
                ){
                    setQuoId.add(lstQuoOld[i].Id); 
                    //setQuoteId.add(lstQuoOld[i].QuoteId);      
                    // System.debug('###### in QLI after del trigger for SYNC'+notRun);
                }
                // else{
                //     System.debug('######  NOT in QLI after del trigger for SYNC'+notRun);
                // } 

                 //RRA 20200825- CT-143 Added logic remove value of field Aide_CEE__c in Quote and Opportunity if TECH_Aide_CEE__c is true
                if(lstQuoOld[i].TECH_Aide_CEE__c == true && !notRun
                    && lstQuoOld[i].TECH_QuoteRecordTypeDevName__c == 'Devis_standard'
                ){
                    setQuoteId.add(lstQuoOld[i].QuoteId); 
                   /* System.debug('##### OK ');
                    System.debug('##### setQuoId ' + setQuoteId);
                    for(Quote q : [SELECT id, OpportunityId, Aide_CEE__c FROM Quote WHERE id IN: setQuoteId ]) {
                        System.debug('##### Aide_CEE__c ' + q.Aide_CEE__c);
                        if(q.Aide_CEE__c != null) {
                            q.Aide_CEE__c = null;
                            lstValueQuoteAide.add(q);
                            System.debug('#####toDeleteAIde== ' + lstValueQuoteAide);
                        }

                        for(Opportunity opp : [SELECT Id, Aide_CEE__c FROM Opportunity WHERE Id =: q.OpportunityId ]) {
                            if(opp.Aide_CEE__c != null) {
                                opp.Aide_CEE__c = null;
                                lstValueOppAide.add(opp);
                            }
                        }
                    }*/

                }   
            }           
        }
        
        
        if(!setQuoId.isEmpty()) {
            LC08_SyncQuote.deleteRelateOppLineItem(setQuoId);         
        } 

        if(!setQuoteId.isEmpty()) {
            LC08_SyncQuote.deleteValueAideCEE(setQuoteId); 
           // update lstValueQuoteAide;
            //update lstValueOppAide;           
        } 

    }

    public void handleAfterUpdate(List<QuoteLineItem> lstOldQLI, List<QuoteLineItem> lstNewQLI){
     System.debug('QuoteLineItemTriggerHandler handleAfterUpdate');
        
        List<QuoteLineItem> setQLI = new List<QuoteLineItem> ();
        for(Integer i=0; i<lstOldQLI.size(); i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('LC08')){
                if(lstNewQLI[i].TECH_QuoteIsSync__c && !notRun && lstNewQLI[i].TECH_Counter__c == lstOldQLI[i].TECH_Counter__c
                    && lstNewQLI[i].TECH_QuoteRecordTypeDevName__c == 'Devis_standard'
                ){
                    setQLI.add(lstNewQLI[i]);
                    //System.debug('###### in QLI after updt trigger for SYNC'+notRun);
                }
                // else{
                //     System.debug('###### NOT in QLI after updt trigger for SYNC'+notRun);
                // }
            }
        }
        
        if(setQLI.size()>0){
            LC08_SyncQuote.updateOppLine(setQLI);
        }
    }


}