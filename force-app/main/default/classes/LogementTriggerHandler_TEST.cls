@isTest
private class LogementTriggerHandler_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Test Class for LogementTriggerHandler
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 06-MAR-2019  DMU    1.0     Initial version
-- 27-DEC-2019  SH     1.1     Added "update Logement"
--------------------------------------------------------------------------------------
**************************************************************************************/

	static User mainUser;
	static list <Logement__c> listLogement;

	static {
		mainUser = TestFactory.createAdminUser('AP01_BlueWSCallout_TEST@test.COM', 
												TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

        	listLogement = new list <Logement__c>{TestFactory.createLogement('lo_001', 'test.test1@sc-mauritius.com', 'lo_L1_001'), 
        										  TestFactory.createLogement('lo_002', 'test.test2@sc-mauritius.com', 'lo_L2_002')

        	};        	
        }
    }

	static testMethod void insertLogement_success(){   
		
		System.runAs(mainUser){  
			Test.startTest();
				insert listLogement;
				update listLogement; // Added 27/12/2019 - SH
            Test.stopTest();
	       	list <Account> acclist = [SELECT id FROM Account WHERE TECH_LogementId__c != null];
	       	System.assertEquals(2, acclist.size());
		}  
	}

	static testMethod void insertLogement_fail(){   
		
		System.runAs(mainUser){  
			Test.startTest();
				insert new Logement__c(Owner_Name__c = 'lo_003', Owner_E_mail__c = 'test.test3@sc-mauritius.com');  //LastName Account not mapped to cause bug       
            Test.stopTest();
	       	list <Account> acclist = [SELECT id FROM Account WHERE TECH_LogementId__c != null];
	       	System.assertEquals(0, acclist.size());
		}  
	}
}