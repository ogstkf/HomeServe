/**
 * @description Contract Controller for VF
 */

public with sharing class SSA_ServiceContractControllerExtension {

    private Id subscriptionId {get;set;} 
    private List<Id> subscriptionIds {get;set;}    
    private ApexPages.StandardController stdController {get;set;}
    private ApexPages.StandardSetController stdSetController {get;set;}
    /**
    *@description SSA_ServiceContractControllerExtension init
    *@param p_standardController 
    */
    public SSA_ServiceContractControllerExtension (ApexPages.StandardController p_standardController) {
        this.stdController = p_standardController;
        this.subscriptionId = p_standardController.getId();
        this.subscriptionIds = new List<Id>();
        this.subscriptionIds.add(this.subscriptionId);
    }
    /**
    *@description SSA_ServiceContractControllerExtension init
    *@param p_standardSetController
    */
    public SSA_ServiceContractControllerExtension (ApexPages.StandardSetController p_standardSetController) {
        this.stdSetController = p_standardSetController;
        this.subscriptionIds = new List<Id>();
        for (sObject lv_obj : p_standardSetController.getSelected()) {
            this.subscriptionIds.add(lv_obj.Id);
        }
    }
    /**
    *@description Generate invoices based on the subscription and the subscription lines
    *@return Pagereference
    */
    public PageReference generateInvoices() { 
        PageReference lv_returnPage = null;
        if (this.subscriptionId != null) {
            try {
                //AbonnementUtils.generateInvoicesPlans(new List<Id>{this.subscriptionId});
                Contrat_2_facture.CreateFacture(this.subscriptionId); 
            } catch (Exception e) {
                ApexPages.addMessage(
                    new ApexPages.Message(
                        ApexPages.Severity.ERROR,
                        e.getMessage() + '<br />' + e.getStackTraceString()
                    )
                );
                return null;
            }
            
            lv_returnPage = stdController.view();
            lv_returnPage.setRedirect(true);
            
      } 
        /**  else {
            ApexPages.addMessage(
                new ApexPages.Message(
                    ApexPages.Severity.FATAL,
                    sofactoapp__FacturesClientUtils.decodeCustomLabel(System.Label.Error_IdNonValide, 
                        new List<String>{'Abonnement'})
                )
            );
        }  */
        return lv_returnPage;
    }
}