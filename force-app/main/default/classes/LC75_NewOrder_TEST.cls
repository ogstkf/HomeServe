/**
 * @File Name          : LC75_NewOrder_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting
 * @Group              : 
 * @Last Modified By   : MNA
 * @Last Modified On   : 03-02-2021
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0    03-02-2021, 10:53:00          MNA         Initial Version
**/
@isTest
public with sharing class LC75_NewOrder_TEST {
    static User usr1;
    static ServiceTerritory svcTer;
    static OperatingHours oh;
    static sofactoapp__Raison_Sociale__c corpName; 
    static {
        oh = new OperatingHours(Name = 'test', TimeZone='Europe/Paris');
        insert oh;
        corpName = new sofactoapp__Raison_Sociale__c(Name='Test',sofactoapp__Invoice_prefix__c = '1000'
                                                     ,sofactoapp__Credit_prefix__c='1000');
        insert corpName;
        svcTer = new ServiceTerritory(Name = 'HomeServe',OperatingHoursId = oh.Id, Sofactoapp_Raison_Social__c = corpName.Id);
        insert svcTer;
        System.debug([SELECT Name FROM ServiceTerritory]);
        usr1 = TestFactory.createAdminUser('LC75@test.com', TestFactory.getProfileAdminId());
        System.debug(usr1.Societe_Agence__c);
        usr1.Societe_Agence__c = 'HomeServe';
        insert usr1;
    }
    @isTest
    static void test() {
        System.runAs(usr1){
            String resId = LC75_NewOrder.getSTforUserconnected();
            System.assertEquals(resId, svcTer.Id);
        }
    }
}