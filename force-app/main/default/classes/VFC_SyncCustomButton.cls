/**
 * @File Name          : VFC_SyncCustomButton.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 11/10/2019, 14:44:49
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    07/10/2019   AMO     Initial Version
**/
public with sharing class VFC_SyncCustomButton {
    
    private ApexPages.StandardController c;
    Quote qu;
    public String msg{get; set;}
    public Boolean inserted {get; set;}

    public VFC_SyncCustomButton(ApexPages.StandardController stdController){
        c = stdController;
        c.addFields(new List<String>{'OpportunityId'});
        qu = (Quote)stdController.getRecord();
        System.debug('** ally q: '+ qu);
    }

    
    
    public void continuing(){
        System.debug('** continuing: '+ qu.Id);

        List<SyncingQLI__c> lstCustomSetting = new List<SyncingQLI__c>();
        lstCustomSetting = SyncingQLI__c.getAll().values();
        System.debug('** ally lstCustomSetting: '+ lstCustomSetting);

        Map<String, String> mapCustomSetting = new Map<String, String>();
        for(SyncingQLI__c q : lstCustomSetting){
            mapCustomSetting.put(q.Name, q.QLI__c);
        }        
        String qLineFlds = String.join(mapCustomSetting.values(), ',');
        String qLineQuery = 'SELECT '+qLineFlds+' FROM QuoteLineItem WHERE QuoteId =\''+ qu.Id+'\'';
        
        System.debug('######## ally: qLineQuery: '+qLineQuery);
        List<QuoteLineItem> lstquoteline = Database.query(qLineQuery);
        // lstquoteline = [SELECT Id FROM QuoteLineItem WHERE QuoteId =: qu.Id];
        System.debug('** ally lstquoteline: '+ lstquoteline.size());

        //Delete oppotunity line items before inserting new
        List<OpportunityLineItem> lstOpportunityLineDel = new List<OpportunityLineItem>();
        for(OpportunityLineItem qq : [SELECT Id, OpportunityId FROM OpportunityLineItem WHERE OpportunityId =: qu.OpportunityId]){
            lstOpportunityLineDel.add(qq);
        }
        Delete lstOpportunityLineDel;

        List<OpportunityLineItem> lstOpportunityline = new List<OpportunityLineItem>();
        for(QuoteLineItem q : lstquoteline){
            OpportunityLineItem oppline = new OpportunityLineItem();
            for(String fld: mapCustomSetting.keySet()){
                oppline.put(fld, getFld(q, mapCustomSetting.get(fld)));
            }
            lstOpportunityline.add(oppline);

        }
        
        try{
            //Insert lstOpportunityline;
            inserted = true;
            msg = 'Synchronisation completee';
        }
        catch(Exception ex){
            system.debug('** ally fail to insert: '+ ex.getMessage());
            inserted = false;
            msg = ex.getMessage();
        }
        
        //System.debug('** ally lstOpportunityline: '+ lstOpportunityline);       

    }

    public PageReference cancel(){ 
        PageReference p = new PageReference('/' + '');  
        return p;
    }

    public static Object getFld(sObject obj, String fld){
        String[] fldArr = fld.split('([.])');
        Object retVal;
        while(fldArr.size()>1){
            String fldStr = fldArr.remove(0);
            obj = obj.getSobject(fldStr);
        }
        retVal = obj.get(fldArr[0]);
        
        return retVal;
    }
    
    
}