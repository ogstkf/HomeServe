/**
 * @File Name          : CoordonneesBancairesHandlerTest.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRA
 * @Last Modified On   : 24/08/2020, 09:00:01
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    05/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class CoordonneesBancairesHandlerTest {
   
        static User adminUser;
    
     static {    
        
        adminUser = TestFactory.createAdminUser('BAT02_PricebookInactif.COM', TestFactory.getProfileAdminId());
        insert adminUser;
        
         System.runAs(adminUser) {
       
            Account acc = new Account();
            acc.Name = 'test';
            acc.RecordTypeId = AP_Constant.getRecTypeId('Account', 'BusinessAccount');
            insert acc;

            Product2 testProd = new Product2();

            //Create Product
            testProd = TestFactory.createProduct('TestProd');
            testProd.Equipment_family__c = 'Chaudière';          
            testProd.Equipment_type1__c = 'Chaudière gaz';
            insert testProd;

            //Create Operating Hour
            OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
            insert OprtHour;

            sofactoapp__Raison_Sociale__c testSofacto = new sofactoapp__Raison_Sociale__c();  
            //Create Sofacto
            testSofacto = new sofactoapp__Raison_Sociale__c(
                Name = 'test sofacto 1',
                sofactoapp__Credit_prefix__c = '1234',
                sofactoapp__Invoice_prefix__c = '2134'
            );
            insert testSofacto;


            ServiceTerritory testAgence = new ServiceTerritory();

             //Create Agence
             testAgence = new ServiceTerritory(
                Name = 'test agence 1',
                Agency_Code__c = '0001',
                IsActive = True,
                OperatingHoursId = OprtHour.Id,
                Sofactoapp_Raison_Social__c = testSofacto.Id
            );
            insert testAgence;

            Logement__c testLogement = new Logement__c();

            //Create Logement
            testLogement = TestFactory.createLogement(acc.Id, testAgence.Id);
            insert testLogement;

             Asset testAsset = new Asset();
             
            //Create Asset
            testAsset = new Asset(
                Name = 'Test Asset',
                Product2Id = testProd.Id,
                AccountId = acc.Id,
                Status = 'Actif',
                Logement__c = testLogement.Id
            );
            insert testAsset;


            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(acc.Id);
            acc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update acc;
            
            ServiceContract ctr = new ServiceContract();
            ctr.Name = 'tzst';
            ctr.Asset__c = testAsset.Id;
            ctr.AccountId = acc.id;
            ctr.Type__c = 'Individual';
            ctr.Frequence_de_maintenance__c = 2;
            ctr.Type_de_frequence__c = 'Weeks';
            ctr.Debut_de_la_periode_de_maintenance__c = 3;
            ctr.Fin_de_periode_de_maintenance__c = 5;
            ctr.Periode_de_generation__c = 6;
            ctr.Type_de_periode_de_generation__c = 'Weeks';
            ctr.Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            ctr.Generer_automatiquement_des_OE__c = true;
            ctr.Horizon_de_generation__c = 5;
            ctr.Contract_Status__c = 'Active';
            ctr.StartDate =  Date.newInstance(2019, 11, 20);
            ctr.EndDate  = Date.newInstance(2020, 1, 20);
            ctr.Contrat_resilie__c = false;
            
            insert ctr;
        }
     }
     
        
    
    static testMethod void test_cooordoneeBancaireHelperTest() {
        
                
            list <sofactoapp__Coordonnees_bancaires__c> NewRibs = new List<sofactoapp__Coordonnees_bancaires__c>();
            
            Account acc = [SELECT Id, Name FROM Account WHERE Name = 'test' LIMIT 1];

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(acc.Id);
            acc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update acc;

            ServiceContract crt = [SELECT Id, Name FROM ServiceContract WHERE Name = 'tzst' LIMIT 1];
            
            Test.startTest();
            // Test in bulk (200+ records)!
            for(Integer i=1;i<=2;i++){
                sofactoapp__Coordonnees_bancaires__c Iban = new sofactoapp__Coordonnees_bancaires__c ();
                Iban.Name ='testiban'+i;
                Iban.sofactoapp__IBAN__c = 'FR0330002056460000117256J51';
                Iban.sofactoapp__Compte__c = acc.Id;
                
                NewRibs.add(Iban);
            }
            insert NewRibs;
        
        Test.stopTest();
   }
    
  
}