/**
 * @description       : 
 * @author            : ARA
 * @group             : 
 * @last modified on  : 04-08-2021
 * @last modified by  : ARA
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   04-08-2021   ARA   Initial Version
**/
@isTest
public with sharing  class FacturesTriggerHandlerTest {
  static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<ServiceContract> lstSerCon = new List<ServiceContract>();
    static List<sofactoapp__Factures_Client__c> lstFacturesClients = new List<sofactoapp__Factures_Client__c>();
    static List<sofactoapp__R_glement__c> lstRGlements = new List<sofactoapp__R_glement__c>();
	static List<Opportunity> lstOpp = new List<Opportunity>();
    static sofactoapp__Factures_Client__c FacInitial = new sofactoapp__Factures_Client__c();
    
    static {
        adminUser = TestFactory.createAdminUser('testuser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser) {
            for (Integer i = 0; i < 5; i++) {
                lstTestAcc.add(TestFactory.createAccountBusiness('testAcc' + i));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'BusinessAccount');
                lstTestAcc[i].BillingPostalCode = '1234' + i;
                lstTestAcc[i].Rating__c = 4;
            }
            insert lstTestAcc;

            Product2 testProd = new Product2();

            //Create Product
            testProd = TestFactory.createProduct('TestProd');
            testProd.Equipment_family__c = 'Chaudière';          
            testProd.Equipment_type1__c = 'Chaudière gaz';
            insert testProd;

            //Create Operating Hour
            OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
            insert OprtHour;

            sofactoapp__Raison_Sociale__c testSofacto = new sofactoapp__Raison_Sociale__c();  
            //Create Sofacto
            testSofacto = new sofactoapp__Raison_Sociale__c(
                Name = 'test sofacto 1',
                sofactoapp__Credit_prefix__c = '1234',
                sofactoapp__Invoice_prefix__c = '2134'
            );
            insert testSofacto;


            ServiceTerritory testAgence = new ServiceTerritory();

             //Create Agence
             testAgence = new ServiceTerritory(
                Name = 'test agence 1',
                Agency_Code__c = '0001',
                IsActive = True,
                OperatingHoursId = OprtHour.Id,
                Sofactoapp_Raison_Social__c = testSofacto.Id
            );
            insert testAgence;

            Logement__c testLogement = new Logement__c();

            //Create Logement
            testLogement = TestFactory.createLogement(lstTestAcc[0].Id, testAgence.Id);
            insert testLogement;

             Asset testAsset = new Asset();
             
            //Create Asset
            testAsset = new Asset(
                Name = 'Test Asset',
                Product2Id = testProd.Id,
                AccountId = lstTestAcc[0].Id,
                Status = 'Actif',
                Logement__c = testLogement.Id
            );
            insert testAsset;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstTestAcc.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstTestAcc.get(2).Id);
            sofactoapp__Compte_auxiliaire__c aux3 = DataTST.createCompteAuxilaire(lstTestAcc.get(3).Id);
            sofactoapp__Compte_auxiliaire__c aux4 = DataTST.createCompteAuxilaire(lstTestAcc.get(4).Id);
			lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstTestAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstTestAcc.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            lstTestAcc.get(3).sofactoapp__Compte_auxiliaire__c = aux3.Id;
            lstTestAcc.get(4).sofactoapp__Compte_auxiliaire__c = aux4.Id;
            
            update lstTestAcc;

            for (Integer i = 0; i < 5; i++) {
                lstSerCon.add(TestFactory.createServiceContract('test' + i, lstTestAcc[i].Id));
                lstSerCon[i].Asset__c = testAsset.Id;
                lstSerCon[i].Type__c = 'Individual';
                lstSerCon[i].Contract_Status__c = 'Active';
                lstSerCon[i].StartDate =  Date.newInstance(2019, 11, 20);
                lstSerCon[i].EndDate  = Date.newInstance(2020, 1, 20);
                lstSerCon[i].Contrat_resilie__c = false;
                lstSerCon[i].tech_facture_OK__c = true;
                lstSerCon[i].Frequence_de_maintenance__c = 2;
                lstSerCon[i].Type_de_frequence__c = 'Weeks';
                lstSerCon[i].Debut_de_la_periode_de_maintenance__c = 3;
                lstSerCon[i].Fin_de_periode_de_maintenance__c = 5;
                lstSerCon[i].Periode_de_generation__c = 6;
                lstSerCon[i].Type_de_periode_de_generation__c = 'Weeks';
                lstSerCon[i].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
                lstSerCon[i].Generer_automatiquement_des_OE__c = true;
                lstSerCon[i].Horizon_de_generation__c = 5;
                
            }
            lstSerCon[0].StartDate =  Date.newInstance(2019, 12, 20);
            lstSerCon[3].StartDate =  Date.newInstance(2018, 12, 20);
            lstSerCon[4].StartDate =  Date.newInstance(2018, 1, 12);
            insert lstSerCon;
            
            
			lstOpp = new List<Opportunity>{ TestFactory.createOpportunity('Opp1', lstTestAcc[0].Id)};
            insert lstOpp;
            
           // create raison sociale
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'CHAM2', sofactoapp__Credit_prefix__c= '145', sofactoapp__Invoice_prefix__c='982');
            insert sofa;
            
            //create Operating Hours
			OperatingHours oh = new OperatingHours(Name='Test1');
			insert oh;
            
           //create Service Territory
          ServiceTerritory  st = new ServiceTerritory(Name='Test1',
                                      OperatingHoursId=oh.Id,
                                      Sofactoapp_Raison_Social__c=sofa.Id);
            insert st;

                                                 
            
            
            
                                                 
           	//create Quote
           Quote quo = new Quote(Name ='Test1',
                            OpportunityId=lstOpp[0].Id,
                            Agency__c=st.Id);
            insert quo; 
            

            for (Integer i = 0; i < 2; i++) {
                lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                          Sofactoapp_Contrat_de_service__c = lstSerCon[i].Id
                    	, SofactoappType_Prestation__c ='Installation/Remplacement'
                        , sofactoapp__Compte__c = lstTestAcc[i].Id
                    	, sofactoapp__Compte2__c = lstTestAcc[i].Id
                        , sofactoapp__emetteur_facture__c = sofa.Id
                        , sofactoapp__Opportunit__c = lstOpp[0].Id
                        , sofactoapp__Type_de_facture__c = 'Facture'
                        , sofactoapp__Payment_mode__c = 'CB'
                        , sofactoapp__Etat__c  = 'Brouilon'
                    	, sofactoapp__Date_de_facture__c = System.today()
                        ,Beneficiaire_Prime__c = adminUser.id
                ));
            }
            lstFacturesClients[0].sofacto_Devis__c = quo.id ;
            lstFacturesClients[1].sofactoapp__Etat__c = 'Emise' ;
            
            
            insert lstFacturesClients;
            // creation des regleemnts
            for (Integer i = 0; i < 1 ; i++) {
                lstRGlements.add(new sofactoapp__R_glement__c(
                        sofactoapp__Facture__c = lstFacturesClients[i].Id
                        , sofactoapp__Montant__c = 22
                        , sofactoapp__Mode_de_paiement__c = 'Espèces'
                        , statut_du_paiement__c = 'Collecté'
                    	,sofactoapp__Date__c = System.today()
                        
                ));
            }
            insert lstRGlements;
            
            

        }
    }
      
    static testMethod void testfacturehelper(){
       
            Test.startTest();
        
		list <sofactoapp__Factures_Client__c >  ListFACT = [select id, Sofacto_Total_reglements__c,sofactoapp__Etat__c, sofactoapp__Type_de_facture__c
                                                            ,sofactoapp__Compte__c,sofactoapp__emetteur_facture__c,SofactoappType_Prestation__c
                                                            ,Sofactoapp_Contrat_de_service__c,sofacto_Devis__c
                                                      		from sofactoapp__Factures_Client__c  where Sofacto_Total_reglements__c = 0 
                                                            limit 1 ];
       	list <sofactoapp__Factures_Client__c >  ListFACT2 = [select id, Sofacto_Total_reglements__c,sofactoapp__Etat__c, sofactoapp__Type_de_facture__c
                                                            ,sofactoapp__Compte__c,sofactoapp__emetteur_facture__c
                                                      		from sofactoapp__Factures_Client__c  where Sofacto_Total_reglements__c > 0 
                                                            limit 1 ];
         FacInitial = ListFACT.get(0);
        System.debug ('facture en tets ' + FacInitial );  
        sofactoapp__Factures_Client__c Avoirtest = new sofactoapp__Factures_Client__c (
                 		sofactoapp__Facture_initiale__c = FacInitial.Id
                        , sofactoapp__Compte__c = FacInitial.sofactoapp__Compte__c
                    	, sofactoapp__Compte2__c = FacInitial.sofactoapp__Compte__c
                        , sofactoapp__emetteur_facture__c = FacInitial.sofactoapp__emetteur_facture__c
                        , sofactoapp__Type_de_facture__c = 'Avoir'

                        , sofactoapp__Etat__c  = 'Brouilon'
                        , sofactoapp__Date_de_facture__c = System.today()

            );
            insert Avoirtest;
        FacInitial = [select id, Sofacto_Total_reglements__c,sofactoapp__Etat__c, sofactoapp__Type_de_facture__c
                                                            ,sofactoapp__Compte__c,sofactoapp__emetteur_facture__c,SofactoappType_Prestation__c
                                                            ,Sofactoapp_Contrat_de_service__c,sofacto_Devis__c
                                                      		from sofactoapp__Factures_Client__c  where sofactoapp__Type_de_facture__c = 'avoir' 
                                                            limit 1 ];
        System.debug ('avoir aprés insertion ' + FacInitial );  
        
        try{
            delete ListFACT2; 
            system.debug ('Facture supprime');
        }
        catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
           // System.assert(ex.getMessage().contains('La suppression de cette facture n\'est pas autorisée'));
        	}	            	
            
            Test.stopTest();

       
    }
}