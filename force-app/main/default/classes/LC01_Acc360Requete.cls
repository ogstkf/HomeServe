/**
 * @File Name          : LC01_Acc360Requete.cls
 * @Description        : Apex controller for lightning component LC01_Acc360Requete
 * @Author             : DMU
 * @Group              : Spoon Consulting
 * @Last Modified By   : AMO
 * @Last Modified On   : 17/02/2020, 14:49:48
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    25/06/2019, 11:00:00          DMU                       Initial Version
 **/
public with sharing class LC01_Acc360Requete {

    @AuraEnabled
    public static List<CaseWrapper> goToCase(String accId) {
        List<CaseWrapper> lstCseWrap = new List<CaseWrapper>();
        List<Case> lstCase = [SELECT CaseNumber
                              , toLabel(Status)
                              , CreatedDate
                              , ClosedDate
                              , toLabel(Type)
                              , Reason__c
                              , Number_of_WO__c
                              , Subject
                              , isClosed
                              , toLabel(Priority)
                              , RecordType.DeveloperName
                              , AssetId
                              , Asset.Name
                              , Asset.Logement__c
                              , Asset.Logement__r.Agency__c
                              , Asset.Logement__r.Owner__c
                              , Asset.Logement__r.Inhabitant__c
                              , Asset.Logement__r.Street__c
                              , Asset.Logement__r.City__c
                              , Asset.Logement__r.Postal_Code__c
                              , Asset.Logement__r.Country__c
                              , (SELECT Id FROM WorkOrders WHERE Case.AccountId = :accId) 
                              FROM Case
                              WHERE AccountId = :accId
                              //BCH modif 25/03 - display all non closed cases and case cloesd within the last 20 days
                              //AND (Status = 'New' OR Status = 'In Progress' OR Status = 'Pending Action from Client') ORDER BY TECH_StatusNumber__c, CreatedDate DESC
                              AND (isClosed = False ) ORDER BY TECH_StatusNumber__c, CreatedDate DESC
        ];

        List<Case> lstCase2 = [SELECT CaseNumber
                                , toLabel(Status)
                                , CreatedDate
                                , ClosedDate
                                , toLabel(Type)
                                , Reason__c
                                , Number_of_WO__c
                                , Subject
                                , isClosed
                                , toLabel(Priority)
                                , RecordType.DeveloperName
                                , AssetId
                                , Asset.Name
                                , Asset.Logement__c
                                , Asset.Logement__r.Agency__c
                                , Asset.Logement__r.Owner__c
                                , Asset.Logement__r.Inhabitant__c
                                , Asset.Logement__r.Street__c
                                , Asset.Logement__r.City__c
                                , Asset.Logement__r.Postal_Code__c
                                , Asset.Logement__r.Country__c
                                , (SELECT Id FROM WorkOrders WHERE Case.AccountId = :accId) FROM Case
                                WHERE AccountId = :accId
                                AND (isClosed = True AND ClosedDate = LAST_N_DAYS :20) //
                                ORDER BY ClosedDate DESC
        ];

        for(Case cse :lstCase) {
            System.debug('#####  ally Wos: ' + cse.WorkOrders);
            lstCseWrap.add(new CaseWrapper(cse));
        }

        for(Case cse :lstCase2) {
            System.debug('#####  ally Wos: ' + cse.WorkOrders);
            lstCseWrap.add(new CaseWrapper(cse));
        }

        return lstCseWrap;
    }



    @AuraEnabled
    public static Account getAcountDetails(Id accId) {
        Account acc = [SELECT Id, Name, PersonContactId, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Campagne__c, Date_fin_retombees__c FROM Account WHERE Id = :accId]; //RRA - 20200720 - CT-64 - l'ajout du code campagne - reference sur la page confluence
        return acc;
    }

    public class CaseWrapper {
        @AuraEnabled public Case cse;
        @AuraEnabled public String statusLabel;
        @AuraEnabled public String typeLabel;
        @AuraEnabled public String priorityLabel;
        @AuraEnabled public Integer numWO;
        @AuraEnabled public String woId;

        public CaseWrapper(Case cse) {
            this.cse = cse;
            this.statusLabel = cse.Status;
            this.typeLabel = cse.Type;
            this.priorityLabel = cse.Priority;
            //this.statusLabel = getPicklistLabel('Case', 'Status', cse.Status);
            //this.typeLabel = getPicklistLabel('Case', 'Type', cse.Type);
            //this.priorityLabel = getPicklistLabel('Case', 'Priority', cse.Priority);
            this.numWO = cse.WorkOrders.size();
        }
    }

    public static String getPicklistLabel(String strObj, String strFld, String strPickValue) {
        for(Schema.PicklistEntry pickEntry :Schema.getGlobalDescribe().get(strObj).getDescribe().fields.getMap().get(strFld).getDescribe().getPicklistValues()) {
            if(pickEntry.getValue() == strPickValue) {
                return pickEntry.getLabel();
            }
        }
        return null;
    }
}