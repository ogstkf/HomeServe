/**
 * @File Name          : ProductTransferTriggerHandler.cls
 * @Description        : 
 * @Author             : SBH
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 02-09-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    16/10/2019         SBH                    Initial Version  (CT-1114)
 * 1.1    10/03/2020         MDO                    CT-1417: Gestion des inventaires - freeze des mouvements de stocks pendant la période d'inventaire
 * 1.2    19/06/2020         DMU                    Commented AP19_CreateShipmentOrPT, AP21_PartialOrderManagement due to homeserve project
**/
public with sharing class ProductTransferTriggerHandler {

    public static boolean byPassTriggerAP69 = false;

    Bypass__c userBypass;

    public ProductTransferTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void handleAfterUpdate(List<ProductTransfer> lstOld, List<ProductTransfer> lstNew){
        System.debug('## Product transfer handleBeforeUpdate start');

        //Init: 
        List<ProductTransfer> lstPtToUpdateOrderLineItem = new List<ProductTransfer>();
        Set<String> setOrdersForPartialOrder = new Set<String>();
        Set<Id> setProductIds = new Set<Id>();
        //end Init: 


        // Loop start
        for(Integer i = 0; i < lstOld.size(); i++){
            
            // Filterings
            if(
                ((lstOld[i].IsReceived == false && lstNew[i].IsReceived == true) && lstNew[i].ShipmentId != null &&  lstNew[i].TECH_OrderId__c != null)
                || 
                (lstNew[i].IsReceived == false && lstOld[i].Inventaire_en_cours__c == false && lstNew[i].Inventaire_en_cours__c == true)
            )
            {
                
                   lstPtToUpdateOrderLineItem.add(lstNew[i]);
                   setOrdersForPartialOrder.add(lstNew[i].TECH_OrderId__c);
                   setProductIds.add(lstNew[i].Product2Id);
            }
            // End filterings
        }
        // Loop end

       
        //AP calls start
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP21;')){
            AP21_PartialOrderManagement.updateOrderItem(lstPtToUpdateOrderLineItem, setOrdersForPartialOrder, setProductIds);
        }
        //AP calls end
        System.debug('## Product transfer handleBeforeUpdate end');
    }


    public void handleBeforeUpdate(List<ProductTransfer> lstOld, List<ProductTransfer> lstNew){
        System.debug('##ProductTranfer handle before update start');
        // CT 1204 vars
        List<ProductTransfer> lstPrdTrasferToUpdateQuantity = new List<ProductTransfer>();
        //end CT 1204 vars

        //CT-1417 variables
        List<ProductTransfer> lstPrdTrasferToUpdate = new List<ProductTransfer>();
        set<Id> setIdLocation = new set<Id>();
        //end CT-1417 variables

        for(Integer i = 0 ; i < lstOld.size(); i++){
            if(lstOld[i].Quantity_Lot_received__c != lstNew[i].Quantity_Lot_received__c
                || lstOld[i].Quantity_lot_sent__c != lstNew[i].Quantity_lot_sent__c
            ){
                lstPrdTrasferToUpdateQuantity.add(lstNew[i]);
            }

            //CT-1417
            if(lstOld[i].IsReceived != lstNew[i].IsReceived && lstNew[i].IsReceived == true
                && (lstNew[i].DestinationLocationId != null || lstNew[i].SourceLocationId != null))
            {
                lstPrdTrasferToUpdate.add(lstNew[i]);
                if (lstNew[i].DestinationLocationId != null)
                    setIdLocation.add(lstNew[i].DestinationLocationId);

                if (lstNew[i].SourceLocationId != null)
                    setIdLocation.add(lstNew[i].SourceLocationId);
            }
        
        }
        
        //AP19 
        //  CT-1204
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP19;')) 
         {
            if(!lstPrdTrasferToUpdateQuantity.isEmpty()){
               AP19_CreateShipmentOrPT.updateQuantitySentReceived(lstPrdTrasferToUpdateQuantity); 
            }
        }

        //AP69 
        //CT-1417
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP69;')) 
        {
            System.debug('Trigger ByPass' + byPassTriggerAP69);
            
            if(!lstPrdTrasferToUpdate.isEmpty()){
                if (!byPassTriggerAP69){
                    AP69_FreezeProductTransfer.checkProductTransfer(lstPrdTrasferToUpdate, setIdLocation); 
                }       
            }
        }
        System.debug('##ProductTranfer handle before update end');
    }

    public void handleBeforeInsert(List<ProductTransfer> lstNew){
        System.debug('##ProductTranfer handle before update start');
        // CT 1204 vars
        List<ProductTransfer> lstPrdTrasferToUpdateQuantity = new List<ProductTransfer>();
        //end CT 1204 vars

        //CT-1417 variables
        List<ProductTransfer> lstPrdTrasferToUpdate = new List<ProductTransfer>();
        set<Id> setIdLocation = new set<Id>();
        //end CT-1417 variables

        for(Integer i = 0 ; i < lstNew.size(); i++){
            if(lstNew[i].Quantity_Lot_received__c != null
                 || lstNew[i].Quantity_lot_sent__c != null
            ){
                lstPrdTrasferToUpdateQuantity.add(lstNew[i]);
            }

            //CT-1417
            if(lstNew[i].IsReceived == true && (lstNew[i].DestinationLocationId != null || lstNew[i].SourceLocationId != null))
            {
                lstPrdTrasferToUpdate.add(lstNew[i]);
                if (lstNew[i].DestinationLocationId != null)
                    setIdLocation.add(lstNew[i].DestinationLocationId);

                if (lstNew[i].SourceLocationId != null)
                    setIdLocation.add(lstNew[i].SourceLocationId);
            }
        }
        
        //AP19 
        //  CT-1204
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP19;')) 
         {
            if(!lstPrdTrasferToUpdateQuantity.isEmpty()){
                AP19_CreateShipmentOrPT.updateQuantitySentReceived(lstPrdTrasferToUpdateQuantity); 
            }
        }

        //AP69 
        //CT-1417
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP69;')) 
        {
            if(!lstPrdTrasferToUpdate.isEmpty()){
                AP69_FreezeProductTransfer.checkProductTransfer(lstPrdTrasferToUpdate, setIdLocation); 
            }
        }
        System.debug('##ProductTranfer handle before update end');
    }

}