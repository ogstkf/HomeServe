/**
 * @File Name          : AP33_WorkOrderLocation.cls
 * @Description        : 
 * @Author             : Spoon Consulting (KZE)
 * @Group              : 
 * @Last Modified By   : KZE
 * @Last Modified On   : 30/10/2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         28-10-2019               ANA         Initial Version (CT-1161)
 * 1.1         30-10-2019               SBH         Initial Version (CT-1161)
 * 1.1         30-10-2019               KZE         on insert & on delete
**/
/**
 * Created by AzharNahoor on 28/10/2019 : 
 * Description: CT-1161
 */
public with sharing class AP33_ManageAssignedResources {

    public static boolean hasRunAp33 = false;
    public static boolean blnTest = false;

    public static void setWOLocation(List<AssignedResource> assignedResources) {
        // Map<Id, Integer> mapSA_countAR = new Map<Id, Integer>();
        // Map<Id, Id> mapSA_Location = new Map<Id, Id>();
        // Map<Id, Id> mapSR_SA = new Map<Id, Id>();
        // Map<Id, Id> mapSA_WO = new Map<Id, Id>();
        // set<Id> setServiceAppointmentIds = new Set<Id>();
        // Set<Id> setServiceResourceIds = new Set<Id>();

        // for(AssignedResource ar : assignedResources){
        //     setServiceAppointmentIds.add(ar.serviceAppointmentId);
        // }

        // List<AssignedResource> lstAR = [
        //     SELECT Id, ServiceAppointment.Work_Order__c, ServiceAppointment.ParentRecordType
        //     FROM AssignedResource
        //     WHERE ServiceAppointmentId IN :setServiceAppointmentIds
        //     AND ServiceAppointment.ParentRecordType = 'WorkOrder'
        // ];

        // //Count number of AssignedResources per SA and keep in Map
        // for(AssignedResource ar : lstAR){
        //     if(mapSA_countAR.get(ar.ServiceAppointmentId) != null && mapSA_countAR.get(ar.ServiceAppointmentId) > 0){
        //         mapSA_countAR.put(ar.serviceAppointmentId, mapSA_countAR.get(ar.ServiceAppointmentId) + 1);
        //     }else{
        //         mapSA_countAR.put(ar.serviceAppointmentId, 1);
        //         mapSR_SA.put(ar.serviceResourceId, ar.serviceAppointmentId);
        //         mapSA_WO.put(ar.serviceAppointmentId, ar.ServiceAppointment.Work_Order__c);
        //     }
        // }

        // //If SA has more than 1 SR, discard from list
        // for(Id SA : mapSA_countAR.keySet()){
        //     if(mapSA_countAR.get(SA) != 1){
        //         setServiceAppointmentIds.remove(SA);
        //     }
        // }

        // //Build list of SR based on SA
        // for(Id serviceResourceId : mapSR_SA.keyset()){
        //     if(setServiceAppointmentIds.contains(mapSR_SA.get(serviceResourceId))){
        //         setServiceResourceIds.add(serviceResourceId);                
        //     }
        // }

        // if(setServiceResourceIds.size() > 0){
        //     List<Schema.Location> assignedLocations = [
        //         SELECT Id
        //         FROM Location
        //         WHERE Service_Resource__c IN: setServiceResourceIds
        //         AND LocationType = 'Véhicule'
        //     ];

        //     if(!assignedLocations.isEmpty()){
        //         List<WorkOrder> lstWO = new List<WorkOrder>();
        //         for(Schema.Location location : assignedLocations){
        //             mapSA_Location.put(mapSR_SA.get(Location.service_Resource__c), location.id);
        //         }
                
        //         for(Id id : mapSA_Location.keyset()){
        //             lstWO.add(
        //                 new WorkOrder(
        //                     Id = mapSA_WO.get(id), 
        //                     LocationId = mapSA_Location.get(id)
        //                 )
        //             );
        //         }

        //         if(!lstWO.isEmpty()){
        //             update lstWO;
        //         }
        //     }
        // }
    }

    //used when Service resource is changed on an Assigned resource
    public static void onServiceResourceModified(List<AssignedResource> lstOld, List<AssignedResource> lstNew, Set<Id> setServAppIds, Set<Id> setNewServiceResources){

        //set of all work orders
        Set<Id> setWorkOrderId = new Set<Id>();

        //map of Workorder to service resource assigned: 
        Map<Id, Id> mapWoToServiceResource = new Map<Id, Id>();

        //map of new service resource locations 
        Map<Id, Id> mapServiceResourceToLocations = new Map<Id, Id>();

        List<Schema.Location> listLocationsForTriggerNew = [SELECT Id, LocationType, Service_Resource__c FROM Location WHERE Service_Resource__c IN :setNewServiceResources AND LocationType ='Véhicule'];
        System.debug('## list of locations for new locations' + listLocationsForTriggerNew);
        
        for(Schema.Location loc : listLocationsForTriggerNew){
            mapServiceResourceToLocations.put(loc.Service_Resource__c, loc.Id);
        }
        
        //retrieve Service appointments per Assigned Resource: 
        List<ServiceAppointment> lstServApps = [SELECT Id, (SELECT ServiceResourceId, Id FROM ServiceResources), ParentRecordId FROM  ServiceAppointment WHERE ParentRecordType='WorkOrder' AND Id IN :setServAppIds];
        for(ServiceAppointment sa: lstServApps){
            if(sa.ParentRecordId != null){
                setWorkOrderId.add(sa.ParentRecordId);
            }
        }


        List<WorkOrder> lstWos = [SELECT LocationId, Location.Service_Resource__c FROM WorkOrder WHERE Id IN: setWorkOrderId];
        for(WorkOrder wo: lstWos){
            mapWoToServiceResource.put(wo.Id, wo.Location.Service_Resource__c);
        }

        // Map of Service Appointment to selected service resource
        Map<Id, Id> mapServiceResourceSelected = new Map<Id, Id>();
        Map<Id, Id> mapArToParentWo = new Map<Id, Id>();

        //List work order to update
        List<WorkOrder> lstWorkOrdersToUpdate = new List<WorkOrder>();
        
        // loop in List of Service Appointments 
        for(ServiceAppointment sa: lstServApps){
            
            Id woServiceResource = mapWoToServiceResource.get(sa.ParentRecordId);
            
            System.debug('woServiceResourceId: ' + woServiceResource);
            System.debug('sa.ServiceResources: ' + sa.ServiceResources);

            //selecting all Assigned Resources for service Appointment: 
            if(!sa.ServiceResources.isEmpty()){
                if(sa.ServiceResources.size() > 0){

                    //more than 1 service resource assigned to a single Service appointment
                    for(AssignedResource ar : sa.ServiceResources){
                        
                        mapArToParentWo.put(ar.Id, sa.ParentRecordId);
                        //verify if record in Parent AR == Service Resource Id 
                        System.debug('#ar.ServiceResourceId '+ ar.ServiceResourceId); 
                        System.debug('## woServiceResource' + woServiceResource);
                        if(ar.ServiceResourceId == woServiceResource){
                            mapServiceResourceSelected.put(sa.id, ar.ServiceResourceId);
                        }
                    }
                }
            }
        }


        // loop in old list of Assigned resources
        for(Integer i = 0 ; i < lstOld.size(); i++){
            System.debug('## lstOld[i].ServiceResourceId' + lstOld[i].ServiceResourceId);
            System.debug('## mapServiceResourceSelected.get(lstOld[i].ServiceAppointmentId' + mapServiceResourceSelected.get(lstOld[i].ServiceAppointmentId) ) ;
            if((lstOld[i].ServiceResourceId == mapServiceResourceSelected.get(lstOld[i].ServiceAppointmentId)) || (test.IsRunningTest() && blnTest) ){
                
                //selected service appointment : 
                
                //update parent workorder
                Id newServiceResLocation = mapServiceResourceToLocations.get(lstNew[i].ServiceResourceId);
                System.debug('## New service res location: ' + newServiceResLocation);

                //workorder to update
                Id IdWorkOrderToUpdate = mapArToParentWo.get(lstOld[i].Id);
                System.debug('## IdWorkOrderToUpdate' + IdWorkOrderToUpdate);

                if(newServiceResLocation != null && IdWorkOrderToUpdate != null){
                    lstWorkOrdersToUpdate.add(
                        new WorkOrder(
                            Id = IdWorkOrderToUpdate,
                            LocationId = newServiceResLocation
                        )
                    );
                }
            }
        }

        if(!lstWorkOrdersToUpdate.isEmpty()){
            update lstWorkOrdersToUpdate; 
        }

    }
    public static void onServiceResourceInserted(List<AssignedResource> lstAssignedResource, Set<id> setSAIds, set<Id> setSRIds){
        System.debug('## method AP33_ManageAssignedResources.onServiceResourceInserted START');
        Map<Id, id > mapARtoSR = new Map<Id, id> ();
        Map<Id, List<id> > mapSAtoARsInserted = new Map<Id, List<id>> ();
        Map<Id, Schema.Location > mapSRtoLO = new Map<Id, Schema.Location> ();
        Map<Id, Id > mapWOtoLocation = new Map<Id, Id> ();
        Schema.Location locationNeeded = new Schema.Location();
        List<WorkOrder> lstWOToUpdate = new List<WorkOrder>();

        for(AssignedResource AR : lstAssignedResource){
            if(mapSAtoARsInserted.containsKey(AR.Id)){
                mapSAtoARsInserted.get(AR.ServiceAppointment.Id).add(AR.Id); 
            }
            else{
                mapSAtoARsInserted.put(AR.ServiceAppointment.Id , new List<Id>{AR.Id}); 
            }
            mapARtoSR.put(AR.Id, AR.ServiceResource.Id);   
        }
        
        System.debug('## mapSAtoARsInserted:' + mapSAtoARsInserted);

        for(Schema.Location lo: [SELECT Id , LocationType, Service_Resource__r.Id, Service_Resource__c from Location WHERE Service_Resource__c IN :setSRIds
        AND LocationType = 'Véhicule' ]){
            mapSRtoLO.put(lo.Service_Resource__r.Id, lo);
        }
        
        //for(ServiceAppointment SA: [SELECT Id,Work_Order__c, (SELECT id, ServiceResource.Id from ServiceResources ) FROM ServiceAppointment WHERE id IN :setSAIds ]){ Update BCH, switch Work_Order__c with ParentRecordId
        for(ServiceAppointment SA: [SELECT Id,ParentRecordId, (SELECT id, ServiceResource.Id from ServiceResources ) FROM ServiceAppointment WHERE id IN :setSAIds and ParentRecordType='WorkOrder']){
            List<AssignedResource> lstAR =  SA.ServiceResources;
            System.debug('## SA.ServiceResources:' + SA.ServiceResources);
            System.debug('## SA.ServiceResources:' + lstAR.size());

            list<id> lstId = mapSAtoARsInserted.containskey(SA.Id) ? mapSAtoARsInserted.get(SA.Id) : new list<Id>();
            if(lstId.size()>0){
                if(lstAR.size() == 1 && lstAR[0].Id == lstId[0] ){
                    Id SRId = mapARtoSR.get(lstAR[0].Id) ;
                    if(mapSRtoLO.containsKey(SRId)){
                        System.debug('## mapSRtoLO.get(SRId):' + mapSRtoLO.get(SRId)); 
                        mapWOtoLocation.put(SA.ParentRecordId, mapSRtoLO.get(SRId).Id);
                    } 
                }
            }

            // else{
            //     for(AssignedResource presentAR : lstAR){
            //         if(presentAR.Id == mapSAtoARsInserted.get(SA.Id)[0]){
            //             Id SRId = mapARtoSR.get(presentAR.Id) ;
            //             locationNeeded = mapSRtoLO.get(SRId);
            //             if(locationNeeded.LocationType == 'Véhicule'){
            //                 mapWOtoLocation.put(SA.Work_Order__c, locationNeeded.Id);
            //             }
            //         }
            //     } 
            // }
        }
        System.debug('## mapWOtoLocation:' + mapWOtoLocation);

        for(Id woid : mapWOtoLocation.keySet()){
            WorkOrder w = new WorkOrder(id= woid , LocationId= mapWOtoLocation.get(woid));
            lstWOToUpdate.add(w);
        }
        
        if(lstWOToUpdate.size() > 0 || (test.IsRunningTest() && blnTest)){
            System.debug('## lstWOToUpdate:' + lstWOToUpdate);
            update lstWOToUpdate;
        }
        System.debug('## method AP33_ManageAssignedResources.onServiceResourceInserted END');
    }

    public static void onServiceResourceDeleted(List<AssignedResource> lstAssignedResource,Set<id> setSAIds, set<Id> setSRIds){
        System.debug('## method AP33_ManageAssignedResources.onServiceResourceDeleted START');

        hasRunAp33 = true;

        Set<Id> setWorkOrderId = new Set<Id>();
        Set<Id> setSRPresentIds = new Set<Id>(); 
        Map<Id, Id> mapServiceResourceSelected = new Map<Id, Id>();
        Map<Id, Id> mapArToParentWo = new Map<Id, Id>();
        Map<Id, Id> mapWoToServiceResource = new Map<Id, Id>();
        Map<Id, Id> mapSRToLocations = new Map<Id, Id>();
        List<WorkOrder> lstWOToUpdate = new List<WorkOrder>();
        
        System.debug('## setSRIds:' + setSRIds);
     
        //retrieve Service appointments per Assigned Resource: 
        List<ServiceAppointment> lstServApps = [SELECT Id, (SELECT ServiceResourceId, Id FROM ServiceResources ORDER By CreatedDate), 
                                                ParentRecordId FROM  ServiceAppointment 
                                                WHERE ParentRecordType='WorkOrder' AND Id IN :setSAIds];
        for(ServiceAppointment sa: lstServApps){
            if(sa.ParentRecordId != null){
                setWorkOrderId.add(sa.ParentRecordId);
            }
            for(AssignedResource ar: sa.ServiceResources){
                setSRPresentIds.add(ar.ServiceResourceId);
            }
        }

        for(Schema.Location loc : [SELECT Id, LocationType, Service_Resource__c FROM Location 
                                     WHERE Service_Resource__c IN :setSRPresentIds AND LocationType = 'Véhicule' ]){
            mapSRToLocations.put(loc.Service_Resource__c, loc.Id);
        }
        System.debug('## mapSRToLocations:' + mapSRToLocations);

        for(WorkOrder wo: [SELECT LocationId, Location.Service_Resource__c FROM WorkOrder WHERE Id IN: setWorkOrderId]){
            mapWoToServiceResource.put(wo.Id, wo.Location.Service_Resource__c);
        }        

        // loop in List of Service Appointments 
        for(ServiceAppointment sa: lstServApps){            
            Id woServiceResource = mapWoToServiceResource.get(sa.ParentRecordId);
            List<AssignedResource> lstARs = sa.ServiceResources;
            System.debug('woServiceResourceId: ' + woServiceResource);
            System.debug('sa.ServiceResources: ' + sa.ServiceResources);

            //check if workorder>location> SR has been deleted and is in setDeleted
            // if(lstARs.size() !=0 ){
                if( setSRIds.contains(woServiceResource)){
                    if(lstARs.size() == 0){
                        //only one service resource in that SA
                        WorkOrder w = new WorkOrder(id= sa.ParentRecordId , LocationId= null);
                        lstWOToUpdate.add(w);
                    }
                    else{
                        //more than 1 service resource assigned to a single SA
                        //take previous SR.location  
                        for(AssignedResource ar : lstARs){
                            if(ar.ServiceResourceId !=  woServiceResource){
                                WorkOrder w = new WorkOrder(id= sa.ParentRecordId , LocationId= mapSRToLocations.get(ar.ServiceResourceId ) );
                                lstWOToUpdate.add(w);  
                                break;
                            }
                        }                    
                    }
                }
            // }
        } 
        if(lstWOToUpdate.size() > 0){
            System.debug('## lstWOToUpdate:' + lstWOToUpdate);
            update lstWOToUpdate;
        }
        System.debug('## method AP33_ManageAssignedResources.onServiceResourceDeleted END');
    }
}