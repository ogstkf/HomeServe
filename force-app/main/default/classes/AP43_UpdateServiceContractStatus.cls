/**
 * @File Name          : AP43_UpdateServiceContractStatus.cls
 * @Description        : 
 * @Author             : DMU
 * @Group              : 
 * @Last Modified By   : DMU
 * @Last Modified On   : 14/11/2019, 21:00:00
 * @Modification Log   : 
 * Ver       Date            Author              Modification
 * 1.0    14/11/2019   DMU     Initial Version
**/
public with sharing class AP43_UpdateServiceContractStatus{
    public static void UpdateStatus(List<ServiceContract> lstNewServCon){
        system.debug('*** in AP43_UpdateServiceContractStatus UpdateStatus');

        for(ServiceContract sc : lstNewServCon){
            sc.Contract_Status__c = AP_Constant.serviceContractStatutEnAttentePremiereVisitValid;
        }        
    }
}