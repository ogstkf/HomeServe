/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 31-08-2021
 * @last modified by  : AMO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-24-2020   AMO   Initial Version
**/
@isTest
public with sharing  class ReglementTriggerHandlerTest {
        static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<ServiceContract> lstSerCon = new List<ServiceContract>();
    static List<sofactoapp__Factures_Client__c> lstFacturesClients = new List<sofactoapp__Factures_Client__c>();
    static List<sofactoapp__R_glement__c> lstRGlements = new List<sofactoapp__R_glement__c>();
	static List<Opportunity> lstOpp = new List<Opportunity>();
    
    static {
        adminUser = TestFactory.createAdminUser('testuser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser) {
            for (Integer i = 0; i < 5; i++) {
                lstTestAcc.add(TestFactory.createAccountBusiness('testAcc' + i));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'BusinessAccount');
                lstTestAcc[i].BillingPostalCode = '1234' + i;
                lstTestAcc[i].Rating__c = 4;
            }
            insert lstTestAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstTestAcc.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstTestAcc.get(2).Id);
            sofactoapp__Compte_auxiliaire__c aux3 = DataTST.createCompteAuxilaire(lstTestAcc.get(3).Id);
            sofactoapp__Compte_auxiliaire__c aux4 = DataTST.createCompteAuxilaire(lstTestAcc.get(4).Id);
			lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstTestAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstTestAcc.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            lstTestAcc.get(3).sofactoapp__Compte_auxiliaire__c = aux3.Id;
            lstTestAcc.get(4).sofactoapp__Compte_auxiliaire__c = aux4.Id;
            
            update lstTestAcc;

            for (Integer i = 0; i < 5; i++) {
                lstSerCon.add(TestFactory.createServiceContract('test' + i, lstTestAcc[i].Id));
                lstSerCon[i].Type__c = 'Individual';
                lstSerCon[i].Contract_Status__c = 'Cancelled';
                lstSerCon[i].StartDate =  Date.newInstance(2019, 11, 20);
                lstSerCon[i].EndDate  = Date.newInstance(2020, 1, 20);
                lstSerCon[i].Contrat_resilie__c = false;
            }
            lstSerCon[0].StartDate =  Date.newInstance(2019, 12, 20);
            lstSerCon[3].StartDate =  Date.newInstance(2018, 12, 20);
            lstSerCon[4].StartDate =  Date.newInstance(2018, 1, 12);
            insert lstSerCon;
            
            
			lstOpp = new List<Opportunity>{ TestFactory.createOpportunity('Opp1', lstTestAcc[0].Id)};
            insert lstOpp;
            
            
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'CHAM2', sofactoapp__Credit_prefix__c= '145', sofactoapp__Invoice_prefix__c='982');
            insert sofa;

            for (Integer i = 0; i < 2; i++) {
                lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                        Sofactoapp_Contrat_de_service__c = lstSerCon[i].Id
                        , sofactoapp__Compte__c = lstTestAcc[i].Id
                    	, sofactoapp__Compte2__c = lstTestAcc[i].Id
                        , sofactoapp__emetteur_facture__c = sofa.Id
                        , sofactoapp__Opportunit__c = lstOpp[0].Id
                        , sofactoapp__Type_de_facture__c = 'Facture'
                        , sofactoapp__Payment_mode__c = 'Direct Debit'
                        , sofactoapp__Etat__c  = 'Emise'
                        , sofactoapp__Date_de_facture__c = System.today()
                        , SofactoappType_Prestation__c = 'Contrat individuel'
                ));
            }
            
            insert lstFacturesClients;
            // creation des regleemnts
            for (Integer i = 0; i < 1; i++) {
                lstRGlements.add(new sofactoapp__R_glement__c(
                        sofactoapp__Facture__c = lstFacturesClients[i].Id
                        , sofactoapp__Montant__c = 22
                        , sofactoapp__Mode_de_paiement__c = 'Prélèvement'
                      //  , statut_du_paiement__c = 'En attente de prélèvement'
                        , statut_du_paiement__c = 'Collecté'
                    	, sofactoapp__Date__c =System.today()
                        
                ));
            }
            insert lstRGlements;

        }
    }
      
    static testMethod void testreglementhelper(){    
            Test.startTest();
        try{
            delete lstRGlements; 
        }
        catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
        	}	            	
            
            Test.stopTest();     
    }

    static testMethod void testhandleafterupdate(){
        lstRGlements = [SELECT Id,sofactoapp__Mode_de_paiement__c FROM sofactoapp__R_glement__c WHERE Id = :lstRGlements[0].Id];
        Test.startTest();
        lstRGlements[0].statut_du_paiement__c = 'Soldé anticipé';
        update lstRGlements;
        Test.stopTest();

   
	}
    
    static testMethod void testhandleafterinsert() {
        sofactoapp__R_glement__c reg = new sofactoapp__R_glement__c(
                        sofactoapp__Facture__c = lstFacturesClients[0].Id
                        , sofactoapp__Montant__c = 22
                        , sofactoapp__Mode_de_paiement__c = 'Virement'
                        , statut_du_paiement__c = 'Collecté'
                    	, sofactoapp__Date__c =System.today());
        Test.startTest();
        insert reg;
        Test.stopTest();
    }
    
}