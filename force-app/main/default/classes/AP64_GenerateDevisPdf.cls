/**
 * @File Name          : AP64_GenerateDevisPdf.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 04/02/2020, 17:26:49
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/02/2020   RRJ     Initial Version
**/
public class AP64_GenerateDevisPdf {
    
    @future(callout=true)
    public static void generatePdfDevis(List<Id> lstQteId){

        List<Quote> lstQte = [SELECT Id, QuoteNumber__c FROM Quote WHERE Id IN :lstQteId];
        System.debug('##### lstQte: '+lstQte);
        List<ContentVersion> lstCV = new List<ContentVersion>();
        Map<Id, ContentVersion> mapQteIdToCv = new Map<Id, ContentVersion>();

        for(Quote qte: lstQte){
            //Create PDF Devis
		    PageReference pageRefDevis = null;
            pageRefDevis = page.VFP10_Devis;
            String title;
            title = qte.QuoteNumber__c; 
            Blob reportPDF;
            pageRefDevis.getParameters().put('id', qte.Id);
            if(Test.isRunningTest()) { 
                reportPDF = blob.valueOf('Unit.Test');
            } else {
                reportPDF = pageRefDevis.getContentAsPDF();
            }
            ContentVersion cv = new ContentVersion();

            Datetime now = Datetime.now();
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);

            cv.Title = title + '_' + local +'.pdf';
            cv.PathOnClient = title + '.pdf';
            cv.VersionData = reportPDF;
            cv.IsMajorVersion = true;
            lstCV.add(cv);
            mapQteIdToCv.put(qte.Id, cv);
        }

        if(mapQteIdToCv.values().size()>0){
            insert mapQteIdToCv.values();
        }

        //Get Content Documents
        List<ContentVersion> docList = [SELECT ContentDocumentId FROM ContentVersion where Id IN :mapQteIdToCv.values()];
        Map<Id, Id> mapIdCVIdCDL = new Map<Id, Id>();
        for(ContentVersion contentV : docList){
            mapIdCVIdCDL.put(contentV.Id,contentV.ContentDocumentId);
        }

        List<ContentDocumentLink> lstCdl = new List<ContentDocumentLink>();
        for(Quote qte: lstQte){
            //Create ContentDocumentLink devis
            ContentDocumentLink cdl_Devis = New ContentDocumentLink();
            cdl_Devis.LinkedEntityId = qte.Id;
            cdl_Devis.ContentDocumentId = mapIdCVIdCDL.get(mapQteIdToCv.get(qte.Id).Id);
            cdl_Devis.shareType = 'V';
            lstCdl.add(cdl_Devis);
        }
        if(lstCdl.size()>0){
            insert lstCdl;
        }
    }
}