/**
 * @File Name          : VFC08_AnomalieA1A2_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 09-11-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         03-10-2019               ANA         Initial Version
**/
@isTest
public with sharing class VFC08_AnomalieA1A2_TEST {
    static Case cse;
    static Case cse1;
    static List<ServiceAppointment> lstServiceApp;
    static User mainUser;
    static List<Asset> lstTestAssset = new List<Asset>();
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Product2> lstTestProd = new List<Product2>();
    static WorkOrder wrkOrd;
    static WorkOrder wrkOrd2;
    static ServiceTerritory servTerritory = new ServiceTerritory();
    static OperatingHours opHrs = new OperatingHours();

    static{
        mainUser = TestFactory.createAdminUser('VFC08_AnomalieA1A2_TEST@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            testAcc = TestFactory.createAccount('Test Account');
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating logement
            Logement__c lgmt = new Logement__c(Account__c = testAcc.Id);
            lstTestLogement.add(lgmt);
            insert lstTestLogement;

            //creating products
            lstTestProd.add(TestFactory.createProduct('Ramonage gaz'));
            insert lstTestProd;

            // creating Assets
            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lstTestLogement[0].Id);
            eqp.Product2Id = lstTestProd[0].Id;
            eqp.Logement__c = lstTestLogement[0].Id;
            eqp.AccountId = testacc.Id;
            lstTestAssset.add(eqp);
            insert lstTestAssset;

            // create case
            cse = new case(AccountId = testacc.id
                                ,type = 'Commissioning'
                                ,AssetId = lstTestAssset[0].Id);
            cse.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            insert cse;



            wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse.id;
            wrkOrd.Type__c =  'Maintenance';
            wrkOrd.assetId = lstTestAssset[0].Id;
            insert wrkOrd;         

            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;

			//create Corporate Name
			sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(
                Name='Test1',
                sofactoapp__Credit_prefix__c='Test1',
                sofactoapp__Invoice_prefix__c='Test1'
            );  
            insert srs;

            //create Service Territory
            servTerritory = new ServiceTerritory(Name='SGS - test Territory',
                                      OperatingHoursId=opHrs.Id,
                                      Sofactoapp_Raison_Social__c=srs.Id, IsActive = true);
            insert servTerritory;


            ServiceAppointment serappGaz = TestFactory.createServiceAppointment(wrkOrd.Id);
            serappGaz.ServiceTerritoryId = servTerritory.Id;
            serappGaz.Status='None';
            serappGaz.SchedStartTime = Datetime.now().addYears(1);
            serappGaz.SchedEndTime = Datetime.now().addYears(2);

            insert serappGaz;

            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 

            User Use = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Test User ANA', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='TestUserANA@testorg.com');

            insert Use;

            ServiceResource SR = new ServiceResource(IsActive=true,Name='Test User ANA',RelatedRecordId=Use.Id);
            insert SR;

            ServiceTerritoryMember  STM = new ServiceTerritoryMember(EffectiveStartDate=Datetime.now(),ServiceTerritoryId=servTerritory.Id,ServiceResourceId=SR.Id);
            insert STM;

            AssignedResource ASR = new AssignedResource(ServiceAppointmentId = serappGaz.Id,ServiceResourceId=SR.Id);
            insert ASR;

            cse1 = new case(AccountId = testacc.id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id
                                ,ParentId = cse.Id);
            cse1.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_anomaly').getRecordTypeId();
            insert cse1;
            
            wrkOrd2 = TestFactory.createWorkOrder();
            wrkOrd2.caseId = cse1.id;
            wrkOrd2.Type__c =  'Maintenance';
            wrkOrd2.assetId = lstTestAssset[0].Id;
            insert wrkOrd2;          
            
            lstServiceApp = new List<ServiceAppointment>{TestFactory.createServiceAppointment(wrkOrd2.Id)};
            lstServiceApp[0].Work_Order__c = wrkOrd2.Id;
            insert lstServiceApp;
            
            VFC08_AnomalieA1A2.testSa =  lstServiceApp[0];                         
        }
    }

    @isTest
    public static void VFC08_AnomalieA1A2TEST(){
        System.runAs(mainUser){
            Test.startTest();
            PageReference pageRef = Page.VFP08_AnomalieA1A2; 
            pageRef.getParameters().put('id', cse1.Id);
            Test.setCurrentPage(pageRef);
            VFC08_AnomalieA1A2 pac = new VFC08_AnomalieA1A2();
            Test.stopTest();
        }
    }
}