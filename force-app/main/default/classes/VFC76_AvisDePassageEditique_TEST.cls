/**
 * @File Name         : VFC76_AvisDePassageEditique_TEST
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 25-10-2021
 * Modifications Log 
 * ===============================================================
 * Ver   Date         Author                Modification
 * 1.0   04-05-2021   MNA                   Initial Version
**/
@isTest
public with sharing class VFC76_AvisDePassageEditique_TEST {
    static User mainUser;
    static Bypass__c bp = new Bypass__c();
    static OperatingHours opHrs = new OperatingHours();
    static sofactoapp__Raison_Sociale__c raisonSocial;

    static List<ServiceAppointment> lstSA = new List<ServiceAppointment>();
    static List<ServiceTerritory> lstSrvTerr = new List<ServiceTerritory>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();

    static {
        mainUser = TestFactory.createAdminUser('VFC76@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;
        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53,MobileNotificationTrigger,AssignedResourceTrigger,AP10';


        System.runAs(mainUser) {
            //Creating operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            // Creating raison sociale
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            //Creating Service Territory
            lstSrvTerr.add(TestFactory.createServiceTerritory('SBF Energies',opHrs.Id, raisonSocial.Id));

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());  

            //Creating Service Appointment
            lstSA = new List<ServiceAppointment>{
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Work_Order__c = lstWrkOrd[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstSrvTerr[0].Id
                )
            };
        }
    }
    
    @isTest
    static void test() {
        System.runAs(mainUser) {
            Test.setMock(HttpCalloutMock.class, new Callout());
            Test.startTest();
            Test.setCurrentPage(Page.VFP76_AvisDePassageEditique);
            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(lstSA);
            VFC76_AvisDePassageEditique ext = new VFC76_AvisDePassageEditique(controller);
            Test.stopTest();
        }
    }

    
    class Callout implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String body;
            Map<String, Object> mapBody = new Map<String, Object>();
            List<Map<String, Object>> columns = new List<Map<String, Object>>();
            Map<String, Object> mapColumn = new Map<String, Object>();
            HttpResponse response = new HttpResponse();

            mapColumn.put('fieldNameOrPath', (Object)'AppointmentNumber');
            mapColumn.put('hidden', (Object)false);
            mapColumn.put('label', (Object)'Appointment Number');
            mapColumn.put('type', (Object)'string');
            columns.add(mapColumn);
            
            mapColumn = new Map<String, Object>();
            mapColumn.put('fieldNameOrPath', (Object)'ArrivalWindowStartTime');
            mapColumn.put('hidden', (Object)false);
            mapColumn.put('label', (Object)'Arrival Window Start');
            mapColumn.put('type', (Object)'datetime');
            columns.add(mapColumn);

            mapColumn = new Map<String, Object>();
            mapColumn.put('fieldNameOrPath', (Object)'ArrivalWindowEndTime');
            mapColumn.put('hidden', (Object)true);
            mapColumn.put('label', (Object)'Arrival Window End');
            mapColumn.put('type', (Object)'datetime');
            columns.add(mapColumn);

            mapBody.put('query', (Object) 'SELECT Id, AppointmentNumber FROM ServiceAppointment');
            mapBody.put('columns', (Object) columns);

            body = JSON.serialize((Object) mapBody);

            response.setHeader('Content-Type', 'application/json');
            response.setBody(body);
            response.setStatusCode(200);
            return response; 
        }
    }
}