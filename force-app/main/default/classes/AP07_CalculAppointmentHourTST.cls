@isTest
private class AP07_CalculAppointmentHourTST{
    
  static Integer nbRecordsToCreate = 10;
  static List<Account> accountList;
  static List<User> userList;
  static List<OperatingHours> operatingHourList;
  static List<ServiceTerritory> serviceTerritoryList;
  static List<Logement__c> logementList;
  static List<Asset> assetList;
  static List<Contract> contractList;
  static List<Product2> productList;
  static List<Agency_Catalogue__c> agencyCatalogList;
  static List<Agency_intervention_zones__c> agencyInterventionList;
  static List<Opportunity> opportunityList;
  static List<Agency_Fixed_Slots__c> agencyFixedSlotList;
  static List<ServiceAppointment> serviceAppointmentList;

  static void createAllData(){
    accountList = DataTST.createAccounts(nbRecordsToCreate);
    userList = DataTST.createUsers(nbRecordsToCreate, accountList);
    operatingHourList = DataTST.createOperatigHours(nbRecordsToCreate);
    serviceTerritoryList = DataTST.createServiceTerritories(nbRecordsToCreate, operatingHourList[0]);
    logementList = DataTST.createLogements(nbRecordsToCreate, accountList, serviceTerritoryList);
    assetList = DataTST.createAssets(2, accountList, logementList);
    contractList = DataTST.createContracts(nbRecordsToCreate, accountList, assetList, serviceTerritoryList, Date.today(), 12);
    productList = DataTST.createProductsAndPriceBookEntries(nbRecordsToCreate);
    agencyInterventionList = DataTST.createAgencyInterventionZones(serviceTerritoryList);
    agencyFixedSlotList = DataTST.createAgencyFixedSlots(nbRecordsToCreate, serviceTerritoryList);
    opportunityList = DataTST.createOpportunities(nbRecordsToCreate, accountList[0], productList[0], agencyInterventionList[0]);    
  }

  // Avec identifiant d'opportunité transmise
  static testMethod void setAppointmentHourInsertTST(){
    createAllData();

    Test.startTest();
    
    System.runAs(userList[0]){
      serviceAppointmentList = DataTST.createServiceAppointments(nbRecordsToCreate, contractList, assetList, opportunityList, logementList, agencyFixedSlotList);
    }
    
    Test.stopTest();
  }

  // Avec identifiant d'opportunité transmise
  static testMethod void setAppointmentHourUpdateTST1(){
    createAllData();

    serviceAppointmentList = DataTST.createServiceAppointments(nbRecordsToCreate, contractList, assetList, opportunityList, logementList, agencyFixedSlotList);
    ServiceAppointment aServiceAppointment = serviceAppointmentList[0];
    Date today = Date.today();
    DateTime now = DateTime.newInstanceGMT(today.year(), today.month(), today.day(), 08, 05, 0);
    DateTime newMeeting = now.addDays(15);
    System.debug('--- now : ' + now);
    System.debug('--- newMeeting : ' + newMeeting);
    Test.startTest();
    
    System.runAs(userList[0]){
        aServiceAppointment.DueDate = now.addDays(16);
        aServiceAppointment.EarliestStartTime = newMeeting;
        aServiceAppointment.SchedEndTime = newMeeting.addHours(2);

        update aServiceAppointment;
    }
    
    Test.stopTest();
  }

  // Avec identifiant d'opportunité transmise
  static testMethod void setAppointmentHourUpdateTST2(){
    createAllData();

    serviceAppointmentList = DataTST.createServiceAppointments(nbRecordsToCreate, contractList, assetList, opportunityList, logementList, agencyFixedSlotList);
    ServiceAppointment aServiceAppointment = serviceAppointmentList[0];
    Date today = Date.today();
    DateTime now = DateTime.newInstanceGMT(today.year(), today.month(), today.day(), 08, 05, 0);
    DateTime newMeeting = now.addDays(15);
    Test.startTest();
    
    System.runAs(userList[0]){
        aServiceAppointment.DueDate = now.addDays(16);
        aServiceAppointment.SchedStartTime = now;
        aServiceAppointment.EarliestStartTime = newMeeting;
        aServiceAppointment.SchedEndTime = newMeeting.addHours(2);

        update aServiceAppointment;
    }
    
    Test.stopTest();

    aServiceAppointment = [
        SELECT Id, TECH_AppointmentHour__c
        FROM ServiceAppointment
        WHERE Id = :aServiceAppointment.Id
    ];

    // System.assertEquals(NULL, aServiceAppointment.TECH_AppointmentHour__c);
  }
}