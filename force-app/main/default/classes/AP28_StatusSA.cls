/**
 * @File Name          : AP28_StatusSA.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/15/2020, 12:25:52 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/14/2020   RRJ     Initial Version
**/
public with sharing class AP28_StatusSA {
/**
 * @File Name          : AP28_StatusSA.cls
 * @Description        : 
 * @Author             : Spoon Consulting
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/15/2020, 12:25:52 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         20-10-2019               DMG         Initial Version
 * 1.1         29-11-2019               DMG         Utilise Devis dans related list du WO
**/

 public static void updateSADoneOk(Set<Id> setIdSA){
    System.debug('##starting method updateSADoneOk: '+setIdSA);

    List<ServiceAppointment> lstRetrieveSA = new List <ServiceAppointment>();
    List<ServiceAppointment> lstSAToUpdate = new List <ServiceAppointment>();
    List<WorkOrder> lstWOToUpdate = new List <WorkOrder>();
    List<Case> lstCaseToUpdate = new List <Case>();
    List<Quote> lstQuotes = new List <Quote>();
    List<Quote> lstQuoteToUpdate = new List <Quote>();
    List<Opportunity> lstOPPToUpdate = new List <Opportunity>();
    Map<Id,List<WorkOrder>> mapCaseWO = new Map<Id,List<WorkOrder>>();
    Set<Id> setCaseID = new Set<Id>();
    Set<Id> setWOID = new Set<Id>();
    List<WorkOrder> lstWO = new List<WorkOrder>();
    Map<Id,Quote> mapWOQuote = new Map<Id,Quote>();

    lstRetrieveSA = [Select Id,Status,Work_Order__c,Work_Order__r.Quote__c,Work_Order__r.CaseId,Work_Order__r.Quote__r.OpportunityId from ServiceAppointment where Id in:setIdSA];
    //lstRetrieveSA = [Select Id,Status,Work_Order__c,Work_Order__r.Quote__c,Work_Order__r.CaseId,Work_Order__r.Quote__r.OpportunityId from ServiceAppointment where Id in:setIdSA];
    System.debug('## lstRetrieveSA: '+lstRetrieveSA);
    System.debug('## Size lstRetrieveSA: '+lstRetrieveSA.size());

    for(ServiceAppointment s : lstRetrieveSA){
        //check if there is a Quote on Work Order
        // if(s.Work_Order__c !=null){
        //     lstSAToUpdate.add(s);
        //     setCaseID.add(s.Work_Order__r.CaseId);
        // }

        //build set WordOrder Id
        setWOID.add(s.Work_Order__c);
    }
    System.debug('## Size lstSAToUpdate: '+lstSAToUpdate.size());

    //Retrieve Quotes
    lstQuotes = [Select id,Name,Ordre_d_execution__c,OpportunityId, TECH_Devis_P3R__c, Contrat_de_service__c, Equipement__c from Quote where Ordre_d_execution__c in:setWOID and status='Validé, signé - en attente d\'intervention'/*and IsSync__c =true*/ order by createdDate desc];
    System.debug('## Size lstQuotes: '+lstQuotes.size());

    for(Quote q:lstQuotes){
        if(!mapWOQuote.containsKey(q.Ordre_d_execution__c)){
            mapWOQuote.put(q.Ordre_d_execution__c,q);
        }
    }
    // System.debug('## Size mapWOQuote: '+mapWOQuote.size());

    //Build map Case/WorkOrders 

    // lstWO =[Select id,CaseId from WorkOrder where CaseId in:setCaseID];
    // for(WorkOrder w:lstWO){
    //     if(!mapCaseWO.containsKey(w.CaseId)){
    //         mapCaseWO.put(w.CaseId,new List<WorkOrder>{w});
    //     }else{
    //         mapCaseWO.get(w.CaseId).add(w);
    //     }
    // }
    // System.debug('## mapCaseWO ' + mapCaseWO);


    // for(ServiceAppointment sa : lstSAToUpdate){
        //Update related objects - close 
        // WorkOrder w = new WorkOrder(Id=sa.Work_Order__c,Status='Done OK');
        //Quote q = new Quote (Id=sa.Work_Order__r.Quote__c,Status='Validé, signé et terminé');
        //Opportunity o = new Opportunity(Id=sa.Work_Order__r.Quote__r.OpportunityId,StageName='Closed Won');

        //check if only one workorder on case, then update, else dont update case
        // System.debug('mapCaseWO.get(sa.Work_Order__r.CaseId).size()='+mapCaseWO.get(sa.Work_Order__r.CaseId).size());
        
        
        //TestBCH - 14/05 - point à revoir
        /*
        if(mapCaseWO.get(sa.Work_Order__r.CaseId).size()==1){
            Case c = new Case (Id=sa.Work_Order__r.CaseId,Status='Closed');
            
            lstCaseToUpdate.add(c);
        }*/
        
        // lstWOToUpdate.add(w);
        //lstQuoteToUpdate.add(q);
        //lstOPPToUpdate.add(o);
    // }

    List<Quote> lstQuoP3r = new List<Quote>();//RRJ 20200529 CT-1281
    for(Quote quo:mapWOQuote.values()){
        quo.Status = 'Validé, signé et terminé';
        Opportunity o = new Opportunity(Id=quo.OpportunityId,StageName='Closed Won');
        lstQuoteToUpdate.add(quo);
        lstOPPToUpdate.add(o);
    }

    try{
        // if(!lstWOToUpdate.isEmpty()) update lstWOToUpdate;
        // if(!lstCaseToUpdate.isEmpty()) update lstCaseToUpdate;
        if(!lstQuoteToUpdate.isEmpty()) update lstQuoteToUpdate;
        if(!lstOPPToUpdate.isEmpty()) update lstOPPToUpdate;

    }catch(Exception e){
        System.debug('## Exception ' + e);
    }
    
 }

// RRJ 20200529 CT-1281
 public static void updateCliP3r(List<Quote> lstQuo){
     Set<Id> setContractId = new Set<Id>();
     Set<Id> setAssetId = new Set<Id>();
     for(Quote quo : lstQuo){
         if(quo.TECH_Devis_P3R__c && quo.Status == 'Validé, signé et terminé' ){
             setContractId.add(quo.Contrat_de_service__c);
             setAssetId.add(quo.Equipement__c);
         }
     }

    List<ContractLineItem> lstCliP3R = [SELECT Id FROM ContractLineItem WHERE ServiceContractId IN :setContractId AND AssetId IN :setAssetId AND Product2.ProductCode = 'P3R' AND Remplacement_effectue__c = false];
    for(ContractLineItem cli : lstCliP3R){
        cli.Remplacement_effectue__c = true;
    }

    if(lstCliP3R.size()>0){
        update lstCliP3R;
    }
 }

}