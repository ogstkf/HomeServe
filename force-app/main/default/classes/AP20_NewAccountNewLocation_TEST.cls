@isTest
public with sharing class AP20_NewAccountNewLocation_TEST {
 /**
 * @File Name          : AP22_HandleProductDelivery_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-10-2019     		LGO         Initial Version
**/
static User mainUser;

    static Account testAcc = new Account();


    static{
        mainUser = TestFactory.createAdminUser('AP22@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccountBusiness('AP22_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = AP_Constant.getRecTypeId('Account', 'Fournisseurs');
             
        }
    }

    @isTest
    public static void testAccountLocation(){
        System.runAs(mainUser){

            Test.startTest();
                insert testAcc;
            Test.stopTest();


            Schema.Location lstNewLoc = [SELECT Id , Name,
                                                LocationType
                                                FROM Location
                                                WHERE Name =: testAcc.Name];
            

            System.assertEquals(lstNewLoc.LocationType ,AP_Constant.virtuallocation);
        }
    }

}