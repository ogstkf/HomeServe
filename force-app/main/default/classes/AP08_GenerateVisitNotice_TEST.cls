/**
 * @File Name          : AP08_GenerateVisitNotice_TEST.cls
 * @Description        : Test class for AP08_GenerateVisitNotice_TEST
 * @Author             : Spoon Consulting
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 26-05-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-JUN-2019              RRJ         Initial Version
**/
@isTest
public class AP08_GenerateVisitNotice_TEST {

    static User adminUser;
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static Product2 lstTestProd = new Product2();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static Bypass__c bp = new Bypass__c();

    static {
        adminUser = TestFactory.createAdminUser('AP08_GenerateVisitNotice_TEST@test.COM', TestFactory.getProfileAdminId());
		insert adminUser;
        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP16,WorkOrderTrigger,AP53';
        bp.BypassValidationRules__c = True;
        insert bp;           

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('AP08_GenerateVisitNotice_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            //creating logement
            for(integer i =0; i<5; i++){
                Logement__c lgmt = TestFactory.createLogement('logementTest'+i, 'logement@test.com'+i, 'lgmt'+i);
                lgmt.Postal_Code__c = 'lgmtPC'+i;
                lgmt.City__c = 'lgmtCity'+i;
                lgmt.Account__c = testAcc.Id;
                lstTestLogement.add(lgmt);
            }
            insert lstTestLogement;

            //creating products
            // lstTestProd.add(TestFactory.createProduct('Ramonage gaz'));
            // lstTestProd.add(TestFactory.createProduct('Entretien gaz'));
            
            // insert lstTestProd;

                                Product2 lstTestProd = new Product2(Name = 'Product X',
                                    ProductCode = 'Pro-X',
                                    isActive = true,
                                    Statut__c = 'Approuvée',
                                    Equipment_family__c = 'Chaudière',
                                    Equipment_type__c = 'Chaudière gaz'
        );
        insert lstTestProd;

                Product2 lstTestProds = new Product2(Name = 'Product XY',
                                    ProductCode = 'Pro-XY',
                                    isActive = true,
                                    Statut__c = 'Approuvée',
                                    Equipment_family__c = 'Chaudière',
                                    Equipment_type__c = 'Chaudière gaz'
        );
        insert lstTestProds;


            // creating Assets
            for(Integer i = 0; i<5; i++){
                Asset eqp = TestFactory.createAccount('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[i].Id);
                eqp.Product2Id = lstTestProd.Id;
                eqp.Logement__c = lstTestLogement[i].Id;
                eqp.AccountId = testacc.Id;
                lstTestAssset.add(eqp);
            }
            insert lstTestAssset;

            // create case
            case cse = new case(AccountId = testacc.id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id);
            insert cse;


            WorkType wrkType1 = TestFactory.createWorkType('wrkType1','Hours',1);
            wrkType1.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType1.Type_de_client__c = 'Tous';
            WorkType wrkType2 = TestFactory.createWorkType('wrkType2','Hours',2);
            wrkType2.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType2.Type_de_client__c = 'Tous';
            WorkType wrkType3 = TestFactory.createWorkType('wrkType3','Hours',3);
            wrkType3.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType3.Type_de_client__c = 'Tous';

            lstWrkTyp = new list<WorkType>{wrkType1, wrkType2, wrkType3};
            insert lstWrkTyp;

            System.debug('lstWrkTyp ' + lstWrkTyp);

            for(Integer i=0; i<3; i++){
                WorkOrder wrkOrd = TestFactory.createWorkOrder();
                wrkOrd.caseId = cse.id;
                wrkOrd.WorkTypeId = lstWrkTyp[0].Id;
                lstWrkOrd.add(wrkOrd);
            }
            insert lstWrkOrd;

            ServiceAppointment serApp1 = TestFactory.createServiceAppointment(lstWrkOrd[0].Id);
            ServiceAppointment serApp2 = TestFactory.createServiceAppointment(lstWrkOrd[1].Id);
            ServiceAppointment serApp3 = TestFactory.createServiceAppointment(lstWrkOrd[2].Id);


            lstServiceApp = new List<ServiceAppointment>{serApp1, serApp2, serApp3};
            // insert lstServiceApp[1];
        }
    }

    /**
     * testAttCreation test method to check if work orders are successfully updated
     */
    @isTest
    static void testAttCreation(){
        insert lstServiceApp;
        lstServiceApp[1].Category__c = 'VE Individuelle';
        lstServiceApp[1].Generate_Visit_Notice__c = true;
        lstServiceApp[1].VE_Planning__c = false;
        lstServiceApp[1].SchedStartTime = Datetime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 1));
        lstServiceApp[1].SchedEndTime = Datetime.newInstance(Date.today(), Time.newInstance(0, 0, 1, 1));
        Test.startTest();
        update lstServiceApp;
        Test.stopTest();

        List<ServiceAppointment> lstUpdatedServApp = [SELECT Id, Account.ClientNumber__c, VisitNoticeSent__c FROM ServiceAppointment WHERE Id = :lstServiceApp[1].Id];
        System.debug('TEST = ' + lstUpdatedServApp[0].VisitNoticeSent__c);
        System.assertEquals(lstUpdatedServApp[0].VisitNoticeSent__c, true);
    }

    @isTest
    static void testInsert(){
        lstServiceApp[0].Category__c = 'VE Individuelle';
        lstServiceApp[0].Generate_Visit_Notice__c = true;
        lstServiceApp[0].VE_Planning__c = false;
        lstServiceApp[0].SchedStartTime = Datetime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 1));
        lstServiceApp[0].SchedEndTime = Datetime.newInstance(Date.today(), Time.newInstance(0, 0, 1, 1));
        Test.startTest();
        insert lstServiceApp[0];
        Test.stopTest();

        List<ServiceAppointment> lstUpdatedServApp = [SELECT Id, Account.ClientNumber__c, VisitNoticeSent__c FROM ServiceAppointment WHERE Id = :lstServiceApp[0].Id];
        System.assertEquals(lstUpdatedServApp[0].VisitNoticeSent__c, true);
    }
}