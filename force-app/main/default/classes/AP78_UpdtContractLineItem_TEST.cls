/**
 * @description       : 
 * @author            : LGO
 * @group             : 
 * @last modified on  : 04-01-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   31-03-2021   LGO   Initial Version
**/
@isTest
public with sharing class AP78_UpdtContractLineItem_TEST {
    static User adminUser;
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAsset = new List<Asset>();
    static List<Asset> lstTestAssetChild = new List<Asset>();
    static Product2 lstTestProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();
    static List<PriceBookEntry> lstPrcBkEnt = new List<PriceBookEntry>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static Bypass__c bp = new Bypass__c();
    static Mesures_quipements_secondaires__c mesEquip = new Mesures_quipements_secondaires__c();
    static List<Case> lstCase = new List<Case>();




    static {

            Integer parentNum = 1;
            Integer childNum = 5;

        adminUser = TestFactory.createAdminUser('AP10_GenerateCompteRenduPDF_Test@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;
        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('AP78_UpdContrat_Test');
            testAcc.BillingPostalCode = '1233456';
            //testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            //creating logement
            for(integer i =0; i<5; i++){
                Logement__c lgmt = TestFactory.createLogement('logementTest'+i, 'logement@test.com'+i, 'lgmt'+i);
                lgmt.Postal_Code__c = 'lgmtPC'+i;
                lgmt.City__c = 'lgmtCity'+i;
                lgmt.Account__c = testAcc.Id;
                lstTestLogement.add(lgmt);
            }
            insert lstTestLogement;

            lstTestProd  = new Product2(Name = 'Product X1',
                ProductCode = 'MXZ-4F80VF3',
                isActive = true,
                Equipment_family__c = 'Pompe à chaleur',
                Statut__c   = 'Approuvée',
                Equipment_type__c = 'Pompe à chaleur air/eau'
            );

            upsert lstTestProd;


            // creating Assets
            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lstTestLogement[0].Id);
            eqp.Product2Id = lstTestProd.Id;
            eqp.Logement__c = lstTestLogement[0].Id;
            eqp.AccountId = testAcc.Id;
            lstTestAsset.add(eqp);
            insert lstTestAsset;

            // creating Assets child
            Asset eqpEnf = TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstTestLogement[0].Id);
            eqpEnf.Product2Id = lstTestProd.Id;
            eqpEnf.Logement__c = lstTestLogement[0].Id;
            eqpEnf.AccountId = testacc.Id;
            eqpEnf.parentId = lstTestAsset[0].Id;
            lstTestAssetChild.add(eqpEnf);
           
            insert lstTestAssetChild;


            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd.Id, 0));

            insert lstPrcBkEnt;

            //service contract
            lstServCon.add(TestFactory.createServiceContract('testServCon', testAcc.Id));
            lstServCon[0].Asset__c = lstTestAsset[0].Id; //asset 0 with product equipment type Chaudière fioul
            lstServCon[0].PriceBook2Id = lstPrcBk[0].Id;
            lstServCon[0].EndDate = Date.Today().addYears(1);
            lstServCon[0].StartDate = Date.Today();
            lstServCon[0].RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives') ;
            lstServCon[0].Type__c = 'Collective';

            insert lstServCon;

            // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'Maintenance', lstTestAsset[0].Id));
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase[0].Origin = 'Cham Digital';
            lstCase[0].Reason__c = 'Visite sous contrat';
            lstCase[0].RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            insert lstCase;


            ContractLineItem cli1 = new ContractLineItem(
                ServiceContractId = lstServCon[0].Id,
                unitPrice = 150,
                quantity = 1,
                Collective_Account__c = testAcc.Id,
                TECH_Code_produit__c = 'XXVFXX',
                AssetId = lstTestAssetChild[0].id,
                PriceBookEntryId = lstPrcBkEnt[0].Id
            );
            lstConLnItem.add(cli1);


            insert lstConLnItem;


            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Maintenance';
            lstWrkTyp[0].Type_de_client__c = 'Tous';
            lstWrkTyp[0].Agence__c = 'Toutes';
            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());       
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].AssetId = lstTestAsset[0].Id;
            lstWrkOrd[0].CaseId = lstCase[0].Id;
            lstWrkOrd[0].AccountId = testAcc.Id;

            insert lstWrkOrd;

             // creating service appointment
             lstServiceApp = new List<ServiceAppointment>{
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Service_Contract__c = lstServCon[0].Id,
                   Work_Order__c = lstWrkOrd[0].Id)                   
            };

            insert lstServiceApp;   

            mesEquip = new Mesures_quipements_secondaires__c(
                Name = 'MesureTest',
                R_f_rence_mesure__c= true,
                Rendez_vous_de_service__c = lstServiceApp[0].Id

            );

            insert mesEquip;
        }        
    }
    @IsTest(SeeAllData=true)
    public static void testUpdateSA(){
        System.runAs(adminUser){

            lstServiceApp[0].Status = 'Done OK';
            
            Test.startTest();            
                update lstServiceApp[0];
            Test.stopTest();

            list <ContractLineItem> lstCLI = [SELECT Id,  VE_Status__c FROM ContractLineItem 
                    WHERE Id =: lstConLnItem[0].Id];
            //System.assertEquals(lstCLI[0].VE_Status__c, 'Complete');
        }
    }    
}