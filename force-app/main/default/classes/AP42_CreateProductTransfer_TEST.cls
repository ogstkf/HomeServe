/**
 * @File Name          : AP42_CreateProductTransfer_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/04/2020   ZJO     Initial Version
**/
@isTest
public with sharing class AP42_CreateProductTransfer_TEST {
 /**
 * @File Name          : AP42_CreateProductTransfer.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         14-11-2019     		LGO         Initial Version
**/
static User mainUser;
    static List<Product2> lstProduct;
    static Account testAcc;
    static Account FournisseurAcc;
    static List<WorkOrder> lstWO = new List<WorkOrder>();
    static Schema.Location locItem;
    static ServiceTerritory serTerItem;
    static List<Asset> lstTestAssset = new List<Asset>();
    static ServiceTerritoryLocation stl = new ServiceTerritoryLocation(); 
    static ProductRequestLineItem prli = new ProductRequestLineItem();
    static List<ProductRequired> lstProdReq;
    static List<ProductItem> lstProdItem;
    static List<ProductRequest> lstProdRequest = new List<ProductRequest>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<Order> lstOrder = new List<Order>();
    //bypass
    static Bypass__c bp = new Bypass__c();

    static{

    //    List<sofactoapp__Parameters__c> paramConf = new List<sofactoapp__Parameters__c>();
    //    Map<String,sofactoapp__Parameters__c> mapParams = Parameters__c.getAll();
    //     if (mapParams.get('BYPASS_FMA') == null) {
    //         // Gestion des FMA
    //         sofactoapp__Parameters__c namingParam = new sofactoapp__Parameters__c (
    //             Name = 'BYPASS_FMA',
    //             Label__c = 'BYPASS_FMA',
    //             Option__c = true
    //         );
    //         paramConf.add(namingParam);
    //     } 
    //     else {
    //         mapParams.get('BYPASS_FMA').Label__c = 'BYPASS_FMA';
    //         paramConf.add(mapParams.get('BYPASS_FMA'));
    //     }            
        
    //     if (!paramConf.isEmpty())
    //         upsert paramConf;
        

        mainUser = TestFactory.createAdminUser('AP42@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;


        System.runAs(mainUser){

            OperatingHours oH = new OperatingHours(Name='op1');
            OperatingHours newOperatingHour = TestFactory.createOperatingHour('test1');
            insert newOperatingHour;


            TimeSlot newTimeSlot = TestFactory.createTimeSlot('Monday', newOperatingHour);
            insert newTimeSlot;

            //create account
            testAcc = TestFactory.createAccount('Test Account');
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            // testAcc.Type = 'Autre';
            // testAcc.sofactoapp__Compte_auxiliaire__c = null;

            insert testAcc;

            FournisseurAcc= new Account(Name='ChamFournissuer');
            FournisseurAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            insert FournisseurAcc;

            //creating logement
            Logement__c lgmt = new Logement__c(Account__c = testAcc.Id);
            insert lgmt;
            
            //List of products
            lstProduct = new List<Product2>{
                TestFactory.createProduct('testProd'),
                TestFactory.createProduct('testProd1'),
                TestFactory.createProduct('testProd2')
            };
            insert lstProduct;

            // creating Assets
            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lgmt.Id);
            eqp.Logement__c = lgmt.Id;
            eqp.AccountId = testacc.Id;
            lstTestAssset.add(eqp);
            insert lstTestAssset;

            //create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa;

            //Service Territory
            serTerItem = new ServiceTerritory(Name = 'test',
                                                         Agency_Code__c = '0007',
                                                         OperatingHoursId = newOperatingHour.Id,
                                                         IsActive = True,
                                                         Inventaire_en_cours__c = true,
                                                         Sofactoapp_Raison_Social__c= sofa.Id
                                                        );
            insert serTerItem;

            //Create location
            locItem = new Schema.Location();
            locItem.Name='Test_AP42Location';
            locItem.LocationType='Entrepôt';
            locItem.IsInventoryLocation=True;
            locItem.Agence__c = serTerItem.Id;
            insert locItem;

            //List of Work Order
            WorkOrder WO1 = TestFactory.createWorkOrder();
            WO1.Type__c = 'Maintenance';
            WO1.assetId = lstTestAssset[0].Id;
            WO1.LocationId = locItem.Id;
            WO1.ServiceTerritoryId = serTerItem.Id;

            WorkOrder WO2 = TestFactory.createWorkOrder();
            WO2.Type__c = 'Maintenance';
            WO2.assetId = lstTestAssset[0].Id;
            WO2.LocationId = locItem.Id;
            WO2.ServiceTerritoryId = serTerItem.Id;

            WorkOrder WO3 = TestFactory.createWorkOrder();
            WO3.Type__c = 'Maintenance';
            WO3.assetId = lstTestAssset[0].Id;
            WO3.LocationId = locItem.Id;
            WO3.ServiceTerritoryId = serTerItem.Id;

            lstWO.add(WO1);
            lstWO.add(WO2);
            lstWO.add(WO3);

            insert lstWO;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2( Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            lstOrder.add(new Order( AccountId = FournisseurAcc.Id
                                    ,Name = 'Test3'
                                    ,EffectiveDate = System.today()
                                    ,Status = 'Demande_d_achats_envoyee' //To add to AP_Constants
                                    ,Pricebook2Id = lstPrcBk[0].Id
                                    ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId())
            );     
            insert lstOrder;

            lstProdRequest = new List<ProductRequest> {
                new ProductRequest()
            };  
            insert lstProdRequest;   

            prli = new ProductRequestLineItem( Product2Id = lstProduct[0].Id
                                                ,QuantityRequested = 1
                                                ,ParentId = lstProdRequest[0].Id
                                                ,Status = 'En cours de commande'
                                                ,Commande__c = lstOrder[0].Id
                                                ,WorkOrderId = lstWO[0].Id);
            insert prli;


            //create Product Required
            lstProdReq = new List<ProductRequired>{
                new ProductRequired(
                    ParentRecordId = lstWO[0].Id,
                    Product2Id =  lstProduct[0].Id, 
                    QuantityRequired = 50,
                    APP_Checked__c = true
                ),
                new ProductRequired(
                    ParentRecordId = lstWO[1].Id,
                    Product2Id =  lstProduct[1].Id, 
                    QuantityRequired = 150,
                    APP_Checked__c = false
                ),
                new ProductRequired(
                    ParentRecordId = lstWO[2].Id,
                    Product2Id =  lstProduct[2].Id, 
                    QuantityRequired = 100,
                    APP_Checked__c = true
                )       
            };
           
            insert lstProdReq;
            
            //create Service Territory Location
            stl = new ServiceTerritoryLocation(ServiceTerritoryId=serTerItem.Id,
                                               LocationId=locItem.Id);
            insert stl;

            //Create product Item linked to product and location
            lstProdItem = new List<ProductItem>{
                new ProductItem(
                    Product2Id=lstProduct[0].Id,
                    LocationId=stl.LocationId,
                    QuantityOnHand=100
                ),
                new ProductItem(
                    Product2Id=lstProduct[1].Id,
                    LocationId=stl.LocationId,
                    QuantityOnHand=100
                ),
                new ProductItem(
                    Product2Id=lstProduct[2].Id,
                    LocationId=stl.LocationId,
                    QuantityOnHand=100
                )
            };
          
            insert lstProdItem;
        }
    }

    @isTest
    public static void testVerifyAPPtrue(){
        System.runAs(mainUser){
            lstProdReq[0].APP_Checked__c = false;
           
            Test.startTest();
                 update lstProdReq[0];
            Test.stopTest();


            list<ProductTransfer> lstPt = [SELECT Id,
                                    DestinationLocationId, 
                                    SourceLocationId ,
                                    IsReceived,
                                    QuantitySent,
                                    QuantityReceived

                                    FROM ProductTransfer
                                    WHERE SourceLocationId =: locItem.Id] ;
            

            System.assertEquals(lstPt.size(),1);
        }
    }
    @isTest
    public static void testVerifyAPPfalse(){
        System.runAs(mainUser){
            lstProdReq[1].APP_Checked__c = false;
            update lstProdReq[1];
            lstProdReq[1].APP_Checked__c = true;
          
            Test.startTest();
                 update lstProdReq[1];
            Test.stopTest();

            list<ProductTransfer> lstPt = [
                SELECT Id,
                        DestinationLocationId, 
                        SourceLocationId ,
                        IsReceived,
                        QuantitySent,
                        QuantityReceived
                FROM ProductTransfer
                WHERE DestinationLocationId =: locItem.Id];
    

            System.assertEquals(1, lstPt.size());
        }
    }
}