/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 03-03-2022
 * @last modified by  : MNA
 * Modifications Log 
 * =============================================================================
 * Ver          Date            Author              Modification
 * 1.0          07-16-2020      ZJO                 Initial Version
**/
@RestResource(urlMapping='/WS10_SendSms_v1')
global with sharing class WS10_SendSms {
    @HttpPost
    global static Map<String,String> doPost(Request request) {
         Map <String, String> mapReponseMsg = new Map <String, String>();
        Map<String,Object> result = new  Map<String,Object>();
        
      try {
          result = sendSMS(request.to,request.body);
          if(Boolean.valueOf(result.get('error'))){
              mapReponseMsg.put('Error',String.valueOf(result.get('message')));
          }else{
              mapReponseMsg.put('Success','Message sent!');
          }

      } catch(Exception e){
              mapReponseMsg.put('Error', 'An error occured!');
      } 
    
        return mapReponseMsg;
    }

    public static void sendSMS(String to, String body, String agencyNomCours) {
        Map<String, String> mapSMS = Util.getSendSMSInfo();
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        req.setHeader('Content-Type', 'application/json; charset=utf-8');
        req.setHeader('Accept','application/json');
        req.setHeader('Authorization', mapSMS.get('header_token'));          
        req.setEndpoint(mapSMS.get('endpoint'));
        req.setMethod('POST');
        
        String content = '{"number":'+ '"' + to + '"' +',"message":"'+ body +'","from":"'+agencyNomCours+'"}';
                             
        System.debug(content);
        System.debug(JSON.deserializeUntyped(content));
        req.setBody(content);
        
        httpResponse response = http.send(req);
        System.debug(response.getBody());
    }

     public static Map<String,Object> sendSMS(string to, string body) {
         System.debug('## Starting method sendSMS --> To :'+to + ' body:' +body);
         Map<String, Object> result = new Map<String,Object>();

         Http m_http = new Http();
         HttpRequest req = new HttpRequest();

         Esendex_Settings__c wsSetup = Esendex_Settings__c.getInstance();
         Blob headerValue = Blob.valueOf(wsSetup.MessageDispatcher_Username__c + ':' + wsSetup.MessageDispatcher_Password__c);
         String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

         req.setHeader('Content-Type','application/json'); 
         req.setHeader('Accept','application/json');
         req.setHeader('Authorization','Bearer 58c6711cd5fa9d1316376a58515f711c3ed7400d98a63f77b809a56c'); //authorizationHeader
        //  req.setEndpoint(wsSetup.MessageDispatcher_Endpoint__c);  
         req.setEndpoint('https://api-pprod.homeserve.fr/services/sms/v1/sms-alert');  
         req.setMethod('POST');

        //  String content = '{"accountreference":"'+wsSetup.MessageDispatcher_AccountRef__c+'","messages":[{"to":"'+to+'","body":"'+body+'"}]}';
        String content = '{"number":23059273140,"message":"test message","from":"SBF"}';
                                    
         system.debug('content : ' + content);
         req.setBody(content);
        
         httpResponse response = m_http.send(req);
         system.debug('## response :' + response);

         system.debug('## status code:' + response.getStatusCode());

         if(response.getStatusCode() == 201){
             //result = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
             result.put('error',false); 
             result.put('result',response.getBody()); 
             String error = String.valueOf(result.get('errors'));

             system.debug('## result OK');  
           
         }else {
             system.debug('## result ERROR');  
             result.put('error',true); 
             result.put('message', response.getStatusCode() + ' : '+response.getStatus()); 
             system.debug('## Error :' + response.getStatus());     
         }

         return result;
      
    }

      @future(callout=true)
      public static void sendSMSAsync(string to, string body, string endpt, string token, string idserviceapp, string agencyNomCours, String AccountId, String DateRdv, String SchedStartTime) {
         System.debug('## Starting method sendSMS --> To :'+to + ' body:' +body);
         System.debug('## endpt & token: '+ endpt + ': '+ token);
         Map<String, Object> result = new Map<String,Object>();

         Http m_http = new Http();
         HttpRequest req = new HttpRequest();

        //  req.setHeader('Content-Type','application/json'); 
         req.setHeader('Content-Type', 'application/json; charset=utf-8');
         req.setHeader('Accept','application/json');
         req.setHeader('Authorization', token); //'Bearer 58c6711cd5fa9d1316376a58515f711c3ed7400d98a63f77b809a56c'          
         req.setEndpoint(endpt);  //'https://api-pprod.homeserve.fr/services/sms/v1/sms-alert'
         req.setMethod('POST');
        
         String content = '{"number":'+ '"' + to + '"' +',"message":"'+ body +'","from":"'+agencyNomCours+'"}';
        // String content = '{"number":"23059273140","message":"test message 17-07-2020_6","from":"SBF"}';
                                    
         system.debug('content : ' + content);
         req.setBody(content);
        
         httpResponse response = m_http.send(req);
         system.debug('## response :' + response);

         system.debug('## status code:' + response.getStatusCode());

         if(response.getStatusCode() == 201){
             //result = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
             result.put('error',false); 
             result.put('result',response.getBody()); 
             String error = String.valueOf(result.get('errors'));

             system.debug('## result OK');  
           
         }else {
             system.debug('## result ERROR');  
             result.put('error',true); 
             result.put('message', response.getStatusCode() + ' : '+response.getStatus()); 
             system.debug('## Error :' + response.getStatus());     
         }

         //Update SA
        ServiceAppointment saToUpdate = new ServiceAppointment(id=idserviceapp, StatutSMS__c='Status Code: '+response.getStatusCode() + ' response msg: ' + response.getBody());
        update saToUpdate;

        //Update Account
        Account accToUpdate = new Account(id = AccountId);
        if (DateRdv == null || DateRdv == '')
            accToUpdate.TECH_DateRdv__c = SchedStartTime;
        else
            accToUpdate.TECH_DateRdv__c = DateRdv + ',' + SchedStartTime;

        update accToUpdate;
     }

    public static String buildSMSMessage(Datetime ScheduleStartTime, Datetime ScheduleEndTime, String serviceTerritoryPhone, String agencyName) {
        String day = (Integer.valueOf(ScheduleStartTime.hour()) > 12 ||
                     (Integer.valueOf(ScheduleStartTime.hour()) == 12 && Integer.valueOf(ScheduleStartTime.minute()) == 30))  ? 'après-midi' : 'matin';
        System.debug('####heure:'+Integer.valueOf(ScheduleStartTime.hour()));
        Integer startHour = Integer.valueOf(ScheduleStartTime.hour());
        Integer endHour = startHour + 1;
        String servicePhone = String.isNotEmpty(serviceTerritoryPhone) ? serviceTerritoryPhone : 'xxxx';
        String msg = 'Bonjour, sauf modification de votre part, nous vous rappelons que votre rendez-vous aura lieu le ' + ScheduleStartTime.format('dd/MM/yyyy')
                    + ((agencyName == 'SBF Energies') ? ' entre ' + startHour + 'h00 et ' + endHour +'h00'
                       :((agencyName == 'Electrogaz') ? ' ' + day : '') )
                    + ' pour l\'entretien de votre appareil par un technicien ' + agencyName 
                    + '. En cas d\'absence merci d\'appeler l\'agence au ' + serviceTerritoryPhone;
        // String msg = 'test message 17-07-2020_10';
        return msg;
    }

    // public static String buildSMSMessageConfirmation(Datetime ScheduleStartTime, String serviceTerritoryPhone) {
    //     String day = Integer.valueOf(ScheduleStartTime.hourGmt()) >= 13 ? 'après-midi' : 'matin';
    //     String servicePhone = String.isNotEmpty(serviceTerritoryPhone) ? serviceTerritoryPhone : 'xxxx';

    //     String msg = 'Cham vous confirme votre rendez-vous qui aura lieu le ' + ScheduleStartTime.format('dd/MM/yyyy') + ' ' + day +
    //                 '. En cas d\'indisponibilité nous vous remercions de contacter votre agence au : ' + servicePhone;

    //     return msg;
    // }

    // public static void checkServiceAccount(List<ServiceAppointment> lstServiceAppointment) {

    //     for(ServiceAppointment service : [SELECT AccountId, Status, SchedStartTime, Account.Privileged_Communication_Channel__c, ServiceTerritoryPhone__c, Account.PersonMobilePhone
    //                                       FROM ServiceAppointment 
    //                                       WHERE Id IN :lstServiceAppointment
    //                                       AND Account.Privileged_Communication_Channel__c = 'SMS']) {

    //         String msgBody = buildSMSMessageConfirmation(service.SchedStartTime, service.ServiceTerritoryPhone__c);
    //         System.debug('##msgBody ' + msgBody);
    //         if(service.Account.PersonMobilePhone != null) {
    //          sendSMSAsync(String.valueOf(service.Account.PersonMobilePhone), msgBody);
    //         }
    //     }
    // }

    global class Request{
        public string to;
        public string body;
    } 
}