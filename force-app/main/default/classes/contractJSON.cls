/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-18-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-13-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global with sharing class contractJSON {
    @future(callout=true)
    public static void createJSON(String ContractId) {
        ServiceContract sc = [SELECT Agency__r.Corporate_Street__c, Agency__r.Corporate_Street2__c, Agency__r.Corporate_ZipCode__c, Agency__r.Corporate_City__c,
                                    Agency__r.Phone__c, Agency__r.Email__c, Agency__r.site_web__c, Agency__r.Legal_Form__c, Agency__r.Capital_in_Eur__c, Agency__r.RCS__c,
                                    Agency__r.IBAN__c, Agency__r.Libelle_horaires__c, Agency__r.horaires_astreinte__c,
                                    Account.Type, Account.Name, Account.FirstName, Account.LastName, Account.Salutation, Account.PersonEmail, Account.ClientNumber__c,
                                    Account.BillingStreet, Account.Adress_complement__c, Account.BillingPostalCode, Account.BillingCity,
                                    Logement__r.Floor__c, Logement__r.Street__c, Logement__r.Adress_complement__c, Logement__r.Postal_code__c, Logement__r.City__c,
                                    Account.PersonHomePhone, Account.Phone,
                                    Asset__r.Equipment_Family__c, Asset__r.Equipment_type_auto__c, Asset__c, Asset__r.Date_de_mise_en_service__c,
                                    Asset__r.Date_de_fin_de_garantie__c,
                                    StartDate, EndDate,
                                    Payeur_du_contrat__r.PersonOptInEMailSMSHSV__pc, Payeur_du_contrat__r.PersonOptInEMailSMSCompany__pc, 
                                    Payeur_du_contrat__r.PersonOptInCallMailCompany__pc, Payeur_du_contrat__c,
                                    Logement__r.Owner__c, Logement__r.Owner__r.Type, Logement__r.Owner__r.Name, Logement__r.Owner__r.Raison_sociale__c,
                                    Logement__r.Owner__r.Salutation, Logement__r.Owner__r.Phone, Logement__r.Owner__r.PersonMobilePhone,
                                    Logement__r.Inhabitant__c, Logement__r.Inhabitant__r.Type, Logement__r.Inhabitant__r.Name, Logement__r.Inhabitant__r.Raison_sociale__c,
                                    Logement__r.Inhabitant__r.Salutation, Logement__r.Inhabitant__r.Phone, Logement__r.Inhabitant__r.PersonMobilePhone,
                                    Tax, Tax__c, GrandTotal, Subtotal, Discount, TotalPrice, Nombre_d_interventions__c, Date_de_signature_client__c
                                FROM ServiceContract WHERE Id =: ContractId];

        Quote quo = [SELECT Name, Equipement__r.Equipment_family__c, Description, Prix_HT_avant_remise__c, Montant_total_TTC_avant_aides__c, Prix_TTC__c, ExpirationDate,
                            Le_client_souhaite_conserver_les_pieces__c, Acompte_verse__c,
                            (SELECT  Product2.Unique_product_code__c, Product2.Name, Product2.Designation_complementaire__c, Product2.ProductCode,
                                        Quantity, UnitPrice, Remise_en100__c, Remise_en_euros__c, Prix_de_vente_apres_remise__c,
                                        Taux_de_TVA__c, Prix_TTC__c
                                             FROM QuoteLineItems)

                                FROM Quote WHERE Contrat_de_service__c =: ContractId LIMIT 1];

        sofactoapp__Factures_Client__c fac = [SELECT Id, sofacto_devis__c, sofacto_devis__r.Name, sofacto_devis__r.Description, Sofacto_Restant_payer__c,
                                                    sofactoapp__R_glement_attendu_le__c, sofactoapp__Amount_exVAT__c, Sofacto_TVA_5__c, Sofacto_TVA_10__c,
                                                    Sofacto_TVA_20__c, Montant_TTC_avant_CEE__c, Sofacto_Montant_prime_CEE__c, sofactoapp__Amount_VAT__c
                                                    FROM sofactoapp__Factures_Client__c WHERE Sofactoapp_Contrat_de_service__c =: ContractId LIMIT 1];
                                                    
        List<sofactoapp__Ligne_de_Facture__c> lstLneFac = [SELECT sofactoapp__Produit__r.Unique_product_code__c, sofactoapp__Produit__r.Name, sofactoapp__Quantit__c,
                                                                sofactoapp__Prix_unitaire_HT__c, sofactoapp__Taux_remise_2__c, sofactoapp__Montant_total_remise__c,
                                                                sofactoapp__Montant_Total_HT__C, sofactoapp__Taux_de_TVA_list__c, sofactoapp__Montant_Total_TTC_2__c
                                                FROM sofactoapp__Ligne_de_Facture__c WHERE sofactoapp__Facture__c=: fac.Id];

        List<sofactoapp__R_glement__c> lstPay = [SELECT sofactoapp__Montant__c, sofactoapp__Date__c, sofactoapp__Mode_de_paiement__c
                                                    FROM sofactoapp__R_glement__c WHERE sofactoapp__Facture__c=: fac.Id ORDER BY CreatedDate DESC];

        List<Asset> lstAsset = new List<Asset>([SELECT Brand_auto__c, Product2.Name, SerialNumber  FROM Asset WHERE ParentId =: sc.Asset__c]);

        ContractLineItem ctrLneIsBundle = [SELECT Product2.Name, UnitPrice, Discount FROM ContractLineItem WHERE ServiceContractId =: ContractId AND IsBundle__c = TRUE LIMIT 1];
        List<ContractLineItem> lstCtrLneOption = new List<ContractLineItem>();
        List<ContractLineItem> lstCtrLneNotOption = new List<ContractLineItem>();
        for(ContractLineItem ctrLne : [SELECT IsOptionPieces__c, Product2.Name, Quantity, UnitPrice, Discount
                                                    FROM ContractLineItem WHERE ServiceContractId =: ContractId AND IsBundle__c = FALSE]){
                                                    
            if (ctrLne.IsOptionPieces__c)
                lstCtrLneOption.add(ctrLne);
            else
                lstCtrLneNotOption.add(ctrLne);
        }

        List<ServiceAppointment> lstSvcApp = [SELECT Work_order__r.Asset.Equipment_type_auto__c, Work_order__r.Asset.Brand_auto__c, Work_order__r.Asset.Product2.Name,
                                                    Work_order__r.Asset.SerialNumber, AppointmentNumber, Service_Contract__r.Id, ActualEndTime, Category__c,
                                                    Technician_comment__c, SchedStartTime
                                                        FROM ServiceAppointment WHERE Service_Contract__c =: ContractId];
                                
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        //req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Bearer 5f43a32ed87f7b00015f0758a7b4ae1f48d14fb6a3e479b28057b859');
        req.setHeader('Content-Type', 'application/json; charset=utf-8');
        req.setHeader('Accept','application/json');
         
        
        req.setEndpoint('https://api-pprod.homeserve.fr/postal-middleware/v1/prints/');
        req.setMethod('POST');

        

        String body = ',"body": { "root": {'
                //meta
                + '"meta" : {'
                    + '"company": {'
                        
                        + '"address": {'
                            + ' "streetName1": "' + sc.Agency__r.Corporate_Street__c + '"'
                            + ', "streetName2": "' + sc.Agency__r.Corporate_Street2__c + '"'
                            + ', "zipcode": "' + sc.Agency__r.Corporate_ZipCode__c + '"'
                            + ', "city": "' + sc.Agency__r.Corporate_City__c + '"'
                        + '}, "phoneNumbers": {'
                            + ' "commercial": "' + sc.Agency__r.Phone__c + '"' 
                            + ', "emergency": ""'
                        + '}, "contactEmail": "' + sc.Agency__r.Email__c + '"'
                        + ', "website": "' + sc.Agency__r.site_web__c + '"'
                        + ', "legalMentions": "' + sc.Agency__r.Legal_Form__c + ' au capital de '+ sc.Agency__r.Capital_in_Eur__c 
                                                + ' euros, enregistrée sous le numéro RCS ' + sc.Agency__r.RCS__c + '"'
                        + ', "IBAN": "' + sc.Agency__r.IBAN__c + '"'
                        + ', "openingHours": "' + newline(sc.Agency__r.Libelle_horaires__c) + '"'
                        + ', "onduty": "' + newline(sc.Agency__r.horaires_astreinte__c) + '"'
                            
                    + '}, "agency": {'
                        

                    + '}, "recipient": {'
                        + '"type": "' + sc.Account.Type + '"'
                        + ', "displayName": "' + sc.Account.Name + '"'
                        + ', "firstName": "' + sc.Account.FirstName + '"'
                        + ', "lastName": "' + sc.Account.LastName + '"'
                        + ', "salutation": "' + sc.Account.Salutation + '"'

                        + ', "addressInvoice": {'
                            + ' "streetName1": "' + sc.Account.BillingStreet + '"'
                            + ', "streetName2": "' + sc.Account.Adress_complement__c + '"'
                            + ', "zipcode": "' + sc.Account.BillingPostalCode + '"'
                            + ', "city": "' + sc.Account.BillingCity + '"'

                        + '}, "addressDwelling": {'
                            + ' "streetName": "' + sc.Logement__r.Floor__c + '"'
                            + ', "streetName1": "' + sc.Logement__r.Street__c + '"'
                            + ', "streetName2": "' + sc.Logement__r.Adress_complement__c + '"'
                            + ', "zipcode": "' + sc.Logement__r.Postal_code__c + '"'
                            + ', "city": "' + sc.Logement__r.City__c + '"'

                        + '}, "phoneNumbers": {'
                            + '"landline": "' + sc.Account.PersonHomePhone + '"'
                            + ', "mobile": "' + sc.Account.Phone + '"'
                        + '}'
                                                
                        + ', "email": "' + sc.Account.PersonEmail + '"'
                        + ', "customerId": "' + sc.Account.ClientNumber__c + '"'

                    + '}'

                //broadcast
                + '}, "broadcast": {'
                    + '"central_print": "true"'
                    + ', "local_print": "true"'
                    + ', "arch": "true"'
                    + ', "email": "true"'
                    + ', "sms": "true"'
                    + ', "watermark": "true"'

                //content
                + '}, "content": {'
                    + '"type": "' + + '"'
                    +', "emissionDate": "' + String.valueOf(System.today()) + '"'
                    +', "documentId": "' + + '"'
                    +', "currency": "EUR"'
                    +', "devis": {'
                        + '"title": "' + quo.Name + '"'
                        + ', "equipment_type": "'+ quo.Equipement__r.Equipment_family__c +'"'
                        + ', "description": "' + newline(quo.Description) +'"'
                        +  ', "elements": [';
        for (Integer i = 0; i < quo.QuoteLineItems.size(); i++) {
            if (i != 0)
                body += ',';

            body +=         '{'
                                + '"reference": "' + quo.QuoteLineItems[i].Product2.Unique_product_code__c + '"'
                                + ', "details_ref": {'
                                    + '"description": "' + quo.QuoteLineItems[i].Product2.Name + '"'
                                    + ', "codeArticle": "' + quo.QuoteLineItems[i].Product2.Unique_product_code__c + '"'
                                    + ', "details": "' + quo.QuoteLineItems[i].Product2.Designation_complementaire__c + '"'
                                    + ', "quantity": "' + quo.QuoteLineItems[i].Quantity + '"'
                                    + ', "unit": "' +  + '"'
                                    + ', "unit_price_amount": "' + quo.QuoteLineItems[i].UnitPrice + '"'
                                    + ', "discount_percentage": "' + quo.QuoteLineItems[i].Remise_en100__c + '"'
                                    + ', "unit_price_amount": "' + quo.QuoteLineItems[i].Remise_en_euros__c + '"'
                                    + ', "total_wo_vat": "' + quo.QuoteLineItems[i].Prix_de_vente_apres_remise__c + '"'
                                    + ', "vat_rate": "' + quo.QuoteLineItems[i].Taux_de_TVA__c + '"'
                                    + ', "total_w_vat": "' + quo.QuoteLineItems[i].Prix_TTC__c + '"'

                                + '}'
                            + '}';
        }

        body            += '], "summary": {'
                            + '"total_ht": "' + quo.Prix_HT_avant_remise__c + '"'
                            + ', "tva": ['

                            + ']'
                            //optionnel
                            + ', "total_ttc_before_prime": "' + quo.Montant_total_TTC_avant_aides__c + '"'
                            //optionnel
                            
                            + ', "prime": {';

        

        body                += '}, "total_ttc": "' + quo.Prix_TTC__c + '"' 
                            + ', "prime_description": "' + quo.Acompte_verse__c + '"'
                        + '}'
                        + ', "validity_date": "' + String.valueOf(quo.ExpirationDate) + '"'
                        + ', "keep_old_material": "' + quo.Le_client_souhaite_conserver_les_pieces__c +'"'
                        + ', "advance_payment_amount": "' + quo.Acompte_verse__c + '"'

                    +'}, "facture": {';

        if (fac.sofacto_devis__c != null) {
            body        += '"title": "' + fac.sofacto_devis__r.Name + '"'
                        + ', "description": "' + fac.sofacto_devis__r.Description + '"';
        }
        else {
            body        += '"title": ""'
                        + ', "description": ""';
        }
        body            += ', "equipment_type": "' + + '"'

                        + ', "elements": [';
        for (Integer i = 0; i < lstLneFac.size(); i++) {
            if (i != 0)
                body += ',';
            body        += '{'
                            + '"reference": "' + lstLneFac[i].sofactoapp__Produit__r.Unique_product_code__c + '"'
                            + ', "details_ref": {'
                                + '"description": "' + lstLneFac[i].sofactoapp__Produit__r.Name + '"'
                                + ', "codeArticle": "' + lstLneFac[i].sofactoapp__Produit__r.Unique_product_code__c + '"'
                                + ', "details": "' +  + '"'
                                + ', "quantity": "' + lstLneFac[i].sofactoapp__Quantit__c + '"'
                                + ', "unit": "' +  + '"'
                                + ', "unit_price_amount": "' + lstLneFac[i].sofactoapp__Prix_unitaire_HT__c + '"'
                                + ', "discount_percentage": "' + lstLneFac[i].sofactoapp__Taux_remise_2__c + '"'
                                + ', "discount_amount": "' + lstLneFac[i].sofactoapp__Montant_total_remise__c + '"'
                                + ', "total_wo_vat": "' + lstLneFac[i].sofactoapp__Montant_Total_HT__C + '"'
                                + ', "vat_rate": "' + lstLneFac[i].sofactoapp__Taux_de_TVA_list__c + '"'
                                + ', "total_w_vat": "' + lstLneFac[i].sofactoapp__Montant_Total_TTC_2__c + '"'
                            + '}'
                        +  '}';
        }
        body            += '], "payments": [';

        for (Integer i = 0; i < lstPay.size(); i++) {
            if (i != 0 )
                body += ',';

            body        += '{'
                            + '"amount": "' + lstPay[i].sofactoapp__Montant__c + '"'
                            + ', "date": "' + String.valueOf(lstPay[i].sofactoapp__Date__c) + '"'
                            +', "sofactoapp__Mode_de_paiement__c": "' + lstPay[i].sofactoapp__Mode_de_paiement__c + '"' 
                        + '}';
        }

        body            += '], "solde_restant": {'
                            + '"amount": "' + fac.Sofacto_Restant_payer__c + '"';
        body            += (fac.Sofacto_Restant_payer__c == 0 && fac.sofactoapp__R_glement_attendu_le__c !=null) ? ', "date": ""' : ', "date": "' + fac.sofactoapp__R_glement_attendu_le__c + '"';
    
        body            += '}, "summary": {'
                            + '"total_ht": "' + fac.sofactoapp__Amount_exVAT__c + '"'
                            + ', "tva": ['
                                + '{'
                                    + '"label": "TVA (5,5%)"'
                                    + ', "amount": "' + fac.Sofacto_TVA_5__c + '"'
                                + '}'
                                + ', {'
                                    + '"label": "TVA (10%)"'
                                    + ', "amount": "' + fac.Sofacto_TVA_10__c + '"'
                                + '}'
                                + ', {'
                                    + '"label": "TVA (20%)"'
                                    + ', "amount": "' + fac.Sofacto_TVA_20__c + '"'
                                + '}'
                            + ']'
                            + ', "total_ttc_before_prime": "' + fac.Montant_TTC_avant_CEE__c + '"'
                            + ', "prime": {'
                                + '"label": "' + + '"'
                                + ', "amount": "' + fac.Sofacto_Montant_prime_CEE__c + '"' 
                            + '}, "total_ttc": "' + fac.sofactoapp__Amount_VAT__c + '"'
                            +', "prime_description": "' +  + '"'

                        + '}, "advance_payments": [';

        for (Integer i = 0; i < lstPay.size(); i++) {
            if (i != 0)
                body += ',';

            body            += '{'
                                + '"amount": "' + lstPay[i].sofactoapp__Montant__c + '"'
                                + ', "method": "' + lstPay[i].sofactoapp__Mode_de_paiement__c + '"'
                                + ', "date": "' + String.valueOf(lstPay[i].sofactoapp__Date__c) + '"'
                            + '}';
        }

        body            += '], "balance_amount": "' + fac.Sofacto_Restant_payer__c + '"';
        body            += (fac.Sofacto_Restant_payer__c == 0) ? ', "invoice_totally_paid_at" : "' + String.valueOf(lstPay[0].sofactoapp__Date__c) + '"' 
                                                                : ', "invoice_totally_paid_at" : ""';

        body        += '}, "contrat": {'
                        + '"product_family": "' + sc.Asset__r.Equipment_Family__c + '"'
                        + ', "product_type" : "' + sc.Asset__r.Equipment_type_auto__c + '"'
                        +  ', "elements": [';
        
        for (Integer i = 0; i < lstAsset.size(); i++) {
            if (i != 0)
                body += ',';

            body            += '{'
                                + '"brand": "' + lstAsset[i].Brand_auto__c + '"'
                                + ', "ref": "' + lstAsset[i].Product2.Name + '"'
                                + ', "serial_nbr": "' + lstAsset[i].SerialNumber + '"'
                            + '}';
        }
                        

        body            += '], "commisionning_date" : "' + sc.Asset__r.Date_de_mise_en_service__c + '"'
                        + ', "waranty_manufacturer_end_date" : "' + sc.Asset__r.Date_de_fin_de_garantie__c + '"'
                        + ', "start_date" : "' + sc.StartDate + '"'
                        + ', "end_date" : "' + sc.EndDate + '"'
                        +  ', "optins_optouts": {'
                            + '"email_sms_hsv": "' + sc.Payeur_du_contrat__r.PersonOptInEMailSMSHSV__pc + '"'
                            + ', "email_sms_company": "' + sc.Payeur_du_contrat__r.PersonOptInEMailSMSCompany__pc + '"'
                            + ', "mail_phone_company": "' + sc.Payeur_du_contrat__r.PersonOptInCallMailCompany__pc + '"'

                        +  '}'
                        + ', "occupancy_status" : "'; 
        
        if (sc.Payeur_du_contrat__c == sc.Logement__r.Owner__c && sc.Payeur_du_contrat__c == sc.Logement__r.Inhabitant__c)
            body            += 'Propriétaire occupant';
        else if (sc.Payeur_du_contrat__c == sc.Logement__r.Owner__c && sc.Payeur_du_contrat__c != sc.Logement__r.Inhabitant__c)
            body            += 'Propriétaire non occupant';
        else if (sc.Payeur_du_contrat__c != sc.Logement__r.Owner__c && sc.Payeur_du_contrat__c == sc.Logement__r.Inhabitant__c)
            body            += 'Locataire';

        body            += '"'
                        +  ', "occupant": {';

        if (sc.Logement__r.Owner__r.Type == 'Particulier') {
            body            += '"type": "individual"'
                            + ', "displayName": "' + sc.Logement__r.Owner__r.Name + '"';
        }
        else {
            body            += '"type": "business"'
                            + ', "displayName": "' + sc.Logement__r.Owner__r.Raison_sociale__c + '"';
        }

        body                += ', "salutation": "' + sc.Logement__r.Owner__r.Salutation + '"'
                            +  ', "address": {'
                                + '"streetNumber": "' + sc.Logement__r.Floor__c + '"'
                                + ', "streetName1": "' + sc.Logement__r.Street__c + '"'
                                + ', "streetName2": "' + sc.Logement__r.Adress_complement__c + '"'
                                + ', "zipcode": "' + sc.Logement__r.Postal_Code__c + '"'
                                + ', "city": "' + sc.Logement__r.City__c + '"'
                            +  '}, "phoneNumbers": {'
                                + '"landline": "' + sc.Logement__r.Owner__r.Phone  + '"'
                                + ', "mobile": "' + sc.Logement__r.Owner__r.PersonMobilePhone + '"'
                            +  '}' 
                        +  '}, "representative": {';
                        
        if (sc.Logement__r.Inhabitant__r.Type == 'Particulier') {
            body            += '"type": "individual"'
                            + ', "displayName": "' + sc.Logement__r.Inhabitant__r.Name + '"';
        }
        else {
            body            += '"type": "business"'
                            + ', "displayName": "' + sc.Logement__r.Inhabitant__r.Raison_sociale__c + '"';
        }

        body                += ', "salutation": "' + sc.Logement__r.Inhabitant__r.Salutation + '"'
                            +  ', "address": {'
                                + '"streetNumber": "' + sc.Logement__r.Floor__c + '"'
                                + ', "streetName1": "' + sc.Logement__r.Street__c + '"'
                                + ', "streetName2": "' + sc.Logement__r.Adress_complement__c + '"'
                                + ', "zipcode": "' + sc.Logement__r.Postal_Code__c + '"'
                                + ', "city": "' + sc.Logement__r.City__c + '"'
                            +  '}, "phoneNumbers": {'
                                + '"landline": "' + sc.Logement__r.Inhabitant__r.Phone  + '"'
                                + ', "mobile": "' + sc.Logement__r.Inhabitant__r.PersonMobilePhone + '"'
                            +  '}' 
                        
                        +  '}, "contract_bundle": {'
                            + '"name": "' + ctrLneIsBundle.Product2.Name + '"'
                            + ', "price_without_tax": "' + ctrLneIsBundle.UnitPrice + '"';
        body                += (ctrLneIsBundle.Discount == null) ? ', "rebate": ""' : ', "rebate": "' + ctrLneIsBundle.Discount + '"';
        body                += ', "vat_rate": "' + sc.Tax__c + '"'
                            + ', "vat_amount": "' + sc.Tax + '"'
                            + ', "price_with_tax": "' + sc.GrandTotal + '"'
                            + ', "included_coverage": [';
        for (Integer i = 0; i < lstCtrLneNotOption.size(); i++) {
            if (i != 0)
                body += ',';

            body                += '{'
                                    + '"label": "' + lstCtrLneNotOption[i].Product2.Name + '"'
                                    + ', "quantity": "' + lstCtrLneNotOption[i].Quantity + '"'
                                + '}';
        }

        body                += ']'
                        + '}, "options": [';
        for (Integer i = 0; i < lstCtrLneOption.size(); i++) {
            if (i != 0)
                body += ',';

            body                += '{'
                                    + '"name": "' + lstCtrLneOption[i].Product2.Name + '"'
                                    + ', "price_without_tax": "' + lstCtrLneOption[i].UnitPrice + '"';
            body                    += (lstCtrLneOption[i].Discount == null) ? ', "rebate": ""' : ', "rebate": "' + lstCtrLneOption[i].Discount + '"';
            body                    += ', "quantity": "' + lstCtrLneOption[i].Quantity + '"'
                                + '}';
        }                    

        body            += '], "recap": {'
                            + '"total_without_tax": "' + sc.Subtotal + '"'
                            + ', "rebate": "' + sc.Discount + '"'
                            + ', "total_without_tax_after_rebate_amount": "' + sc.Discount + '"'
                            + ', "vat_amount": "' + sc.TotalPrice + '"'
                            + ', "vat_amount": "' + sc.Tax__c + '"'
                            + ', "total_with_tax": "' + sc.GrandTotal + '"'
                            + ', "scheduled_interventions": "' + sc.Nombre_d_interventions__c + '"'

                        + '}, "signature_date": "' + sc.Date_de_signature_client__c + '"'
                        + ', "customer_signature": "' + + '"'
                        + ', "technician_signature": "' + + '"'
                        + ', "technician_id": "' + + '"'
                        + ', "conditions_version": "' + + '"'

                    +'}, "report": {'
                        + '"installation": {'
                            + '"equipment_type": "' + lstSvcApp[0].Work_order__r.Asset.Equipment_type_auto__c + '"'
                            + ', "elements": [';
        for(Integer i = 0; i < lstSvcApp.size(); i++) {
            if (i != 0)
                body += ',';
            
            body                += '{'
                                    + '"brand": "' + lstSvcApp[i].Work_order__r.Asset.Brand_auto__c + '"'
                                    + ', "reference": "' + lstSvcApp[i].Work_order__r.Asset.Product2.Name + '"'
                                    + ', "serial_number": "' + lstSvcApp[i].Work_order__r.Asset.SerialNumber + '"'
                                + '}';
        }

        body                += ']'
                        + '}, "intervention": {'
                            + '"id": "' + lstSvcApp[0].AppointmentNumber + '"';
        body                += (lstSvcApp[0].Service_Contract__r.Id == null) ? ', "contract_id": ""' : ', "contract_id": "' + lstSvcApp[0].Service_Contract__r.Id + '"';
        body                += ', "date": "' + lstSvcApp[0].ActualEndTime + '"'
                            + ', "type": "' + lstSvcApp[0].Category__c + '"'
                            + ', "comment": "' + lstSvcApp[0].Technician_comment__c + '"'
                            + ', "results": ['
                            + '], "checklist": ['
                            + ']'
                        + '}, "technician_name": "' + + '"'
                        + ', "technician_signature": "' + + '"'
                        + ', "customer_signature": "' + + '"'

                    +'}, "intervention_notice": {'
                        + '"date_planned": "' + dateOnly(lstSvcApp[0].SchedStartTime) + '"'
                        + ', "slot": "' + + '"'
                        + ', "phone_number": "' + + '"'

                    +'}, "renewal": {'
                        + '"type": "' + + '"'
                        + ', "start_date": "' + + '"'
                        + ', "end_date": "' + + '"'
                        + ', "contract_name": "' + + '"'
                        + ', "product_type": "' + + '"'
                        + ', "price_with_tax": "' + + '"'
                        + ', "price_without_tax": "' + + '"'
                        + ', "vat_rate": "' + + '"'
                        + ', "is_advantage_contract": "' + + '"'
                        + ', "phone_number": "' + + '"'

                    +'}'
                + '}'
                //fin content
        
        + '}}';
        String content = '{"templateId":"test", "autoEmit":true, "callbackUrl":null' + body +'}';
        req.setBody(content);
        httpResponse response = http.send(req);
        System.debug(response.getStatusCode()+'         '+response.getStatus()+'       '+response.getBody() );
        
    }

    public static String newline(String str) {
        if (str == null)
            return '';
        String res = '';
        List<String> lstStr = str.split('\n');
        for  (Integer i = 0 ; i < lstStr.size(); i++ ) {
            if (i == lstStr.size() - 1)
                res += lstStr[i];
            else {
                res += lstStr[i].substring(0, lstStr[i].length() - 2) + '\\n';
            }
        }
        return res;
    }
    
    public static String dateOnly(Datetime dt) {
        return String.valueOf(Date.newInstance(dt.year(), dt.month(), dt.day()));
    }
}