public with sharing class CAPTriggerHandler {
        	/**
 * @File Name          : 
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         19-12-2019     		ANA         Initial Version
 * 1.1         24-06-2020           DMU         Commented AP54_UpdateBudgetActualiseeCAP due to homeserce project
**/

    public CAPTriggerHandler(){}

    public void handleAfterUpdate(List<Cap__c> lstNewCap,list<Cap__c> lstOldCap){

        // list<Cap__c> lstCapChange = new List<Cap__c>();

        // for(integer i=0;i<lstNewCap.size() ; i++){
        //     if(lstOldCap[i].BudgetVEActualisLisse__c != lstNewCap[i].BudgetVEActualisLisse__c){
        //         lstCapChange.add(lstNewCap[i]);
        //     }
        // }

        //20200624 - Commented AP54_UpdateBudgetActualiseeCAP due to homeserce project
        // if(lstCapChange.size()>0 && !AP54_UpdateBudgetActualiseeCAP.RunOnce){
        // if(lstCapChange.size()>0){
        //     AP54_UpdateBudgetActualiseeCAP.UpdateActualisee(lstCapChange);
        // }
    }

}