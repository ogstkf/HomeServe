@isTest
public class ssa_Completion_CompteComptableTest {
    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<ServiceContract> lstSerCon = new List<ServiceContract>();
    static List<sofactoapp__Factures_Client__c> lstFacturesClients = new List<sofactoapp__Factures_Client__c>();
    Static list <sofactoapp__Ecriture_comptable__c> lstEC = new list <sofactoapp__Ecriture_comptable__c>();
    static List<sofactoapp__R_glement__c> lstRGlements = new List<sofactoapp__R_glement__c>();
    static List<sofactoapp__Ligne_de_Facture__c> lstlineFacture = new List<sofactoapp__Ligne_de_Facture__c>();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static sofactoapp__Factures_Client__c FacInitial = new sofactoapp__Factures_Client__c();
    
    static {
        adminUser = TestFactory.createAdminUser('testuser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser) {
            for (Integer i = 0; i < 10; i++) {
                lstTestAcc.add(TestFactory.createAccountBusiness('testAcc' + i));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'BusinessAccount');
                lstTestAcc[i].BillingPostalCode = '1234' + i;
                lstTestAcc[i].Rating__c = 4;
            }
            insert lstTestAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstTestAcc.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstTestAcc.get(2).Id);
            sofactoapp__Compte_auxiliaire__c aux3 = DataTST.createCompteAuxilaire(lstTestAcc.get(3).Id);
            sofactoapp__Compte_auxiliaire__c aux4 = DataTST.createCompteAuxilaire(lstTestAcc.get(4).Id);
            sofactoapp__Compte_auxiliaire__c aux5 = DataTST.createCompteAuxilaire(lstTestAcc.get(5).Id);
            sofactoapp__Compte_auxiliaire__c aux6 = DataTST.createCompteAuxilaire(lstTestAcc.get(6).Id);
            sofactoapp__Compte_auxiliaire__c aux7 = DataTST.createCompteAuxilaire(lstTestAcc.get(7).Id);
            sofactoapp__Compte_auxiliaire__c aux8 = DataTST.createCompteAuxilaire(lstTestAcc.get(8).Id);
            sofactoapp__Compte_auxiliaire__c aux9 = DataTST.createCompteAuxilaire(lstTestAcc.get(9).Id);
            lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstTestAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstTestAcc.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            lstTestAcc.get(3).sofactoapp__Compte_auxiliaire__c = aux3.Id;
            lstTestAcc.get(4).sofactoapp__Compte_auxiliaire__c = aux4.Id;
            lstTestAcc.get(5).sofactoapp__Compte_auxiliaire__c = aux5.Id;
            lstTestAcc.get(6).sofactoapp__Compte_auxiliaire__c = aux6.Id;
            lstTestAcc.get(7).sofactoapp__Compte_auxiliaire__c = aux7.Id;
            lstTestAcc.get(8).sofactoapp__Compte_auxiliaire__c = aux8.Id;
            lstTestAcc.get(9).sofactoapp__Compte_auxiliaire__c = aux9.Id;
            
            update lstTestAcc;

            for (Integer i = 0; i < 10; i++) {
                lstSerCon.add(TestFactory.createServiceContract('test' + i, lstTestAcc[i].Id));
                lstSerCon[i].Type__c = 'Individual';
                lstSerCon[i].Contract_Status__c = 'Draft';
                lstSerCon[i].StartDate =  System.today().addDays(-3);
                lstSerCon[i].EndDate  = System.today().addDays(-1);
                lstSerCon[i].Contrat_resilie__c = true;
                lstSerCon[i].tech_facture_OK__c = true;
                
            }

            insert lstSerCon;
            
            
            lstOpp = new List<Opportunity>{ TestFactory.createOpportunity('Opp1', lstTestAcc[0].Id)};
            insert lstOpp;
            
           // create raison sociale
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'CHAM2', sofactoapp__Credit_prefix__c= '145', sofactoapp__Invoice_prefix__c='982');
            insert sofa;
            
            //create Operating Hours
            OperatingHours oh = new OperatingHours(Name='Test1');
            insert oh;
            
           //create Service Territory
          ServiceTerritory  st = new ServiceTerritory(Name='Test1',
                                      OperatingHoursId=oh.Id,
                                      Sofactoapp_Raison_Social__c=sofa.Id);
            insert st;

                                                 
            
            
            
                                                 
          
            

            for (Integer i = 0; i < 10; i++) {
                lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                          Sofactoapp_Contrat_de_service__c = lstSerCon[i].Id
                        , SofactoappType_Prestation__c ='Prestations / travaux'
                        , sofactoapp__Compte__c = lstTestAcc[i].Id
                        , sofactoapp__Compte2__c = lstTestAcc[i].Id
                        , sofactoapp__emetteur_facture__c = sofa.Id
                        , sofactoapp__Opportunit__c = lstOpp[0].Id
                        , sofactoapp__Type_de_facture__c = 'Facture'
                        , sofactoapp__Payment_mode__c = 'CB'
                        , sofactoapp__Etat__c  = 'Brouillon'
                        , sofactoapp__Date_de_facture__c = Date.newInstance(2025, 12, 9)
                        ,Beneficiaire_Prime__c = adminUser.id
                ));
            }
        for (Integer i = 5; i < 10; i++)
        {
            lstFacturesClients[i].sofactoapp__Compte2__c = null;
        }
             insert lstFacturesClients;

             sofactoapp__Ligne_de_Facture__c Ligne1 = new sofactoapp__Ligne_de_Facture__c (
                        sofactoapp__Facture__c = lstFacturesClients[0].Id
                        ,sofactoapp__Taux_de_TVA_list__c ='10%'
                        , sofactoapp__Description__c = 'Ligne1'
                        , sofactoapp__Prix_Unitaire_HT__c = 150
                        ,sofactoapp__Quantit__c =121

            );  
            insert Ligne1;

        }
    }
    
     static testMethod void testbatch(){
         Test.startTest();
         list <sofactoapp__Factures_Client__c >  ListFACT = [select id, sofactoapp__Date_de_facture__c from sofactoapp__Factures_Client__c where sofactoapp__Date_de_facture__c >today
                                                                and sofactoapp__IsSent__c =false and Sofactoapp_Contrat_de_service__c !=null and SofactoappType_Prestation__c ='Contrat individuel' 
                                                                and Sofacto_Total_reglements__c >0 and sofactoapp__IsCreditNote__c =false  ];
         system.debug('liste facture = '+ ListFACT);  
         database.executeBatch(new ssa_Completion_CompteComptable());
        Test.stopTest();
         
     }
        static testMethod void testSchedule() { 
        System.runAs(adminUser){

            list <CronTrigger> cronlist = new list <CronTrigger> ();
            cronlist = [Select Id From CronTrigger
                       Limit 100];
            if (cronlist.size() != 0){
            for(CronTrigger c : cronlist){ 
            System.abortJob(c.id);
            } 
            }
            String jobId = ssa_Completion_CompteComptable.scheduleBatch();
            CronTrigger ct = [Select Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where Id = :jobId]; 

            System.assertEquals(ct.TimesTriggered,0); 
        }
    }

}