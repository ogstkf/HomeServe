/**
 * @File Name          : WS10_SendSMS_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 02-03-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/02/2020   AMO     Initial Version
**/
@isTest
public class WS10_SendSMS_TEST {

    static User mainUser;
    static List<ServiceAppointment> lstServiceApp;
    static Account testAcc;
    static List<Account> lstAcc;
        static List<Opportunity> lstOpportunity = new List<Opportunity>();
    static List<Logement__c> lstLogement;
    static List<ServiceTerritory> lstAgence;
        static List<ServiceContract> lstContract = new List<ServiceContract>();
    static List<OperatingHours> lstOpHrs;
    static OperatingHours opHrs = new OperatingHours(); 
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static String token = 'Bearer 58c6711cd5fa9d1316376a58515f711c3ed7400d98a63f77b809a56c';
    static String endPoint = 'https://api-pprod.homeserve.fr/services/sms/v1/sms-alert';

    static{
        mainUser = TestFactory.createAdminUser('AP01_BlueWSCallout_TEST@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;
        System.runAs(mainUser) {
            testAcc = TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            lstOpHrs = new List<OperatingHours>{
                                new OperatingHours(Name='op')
            };
            insert lstOpHrs;

            lstContract.add(TestFactory.createServiceContract('test', testAcc.Id));
            insert lstContract;

            lstOpportunity.add(TestFactory.createOpportunity('test', testAcc.Id));
            insert lstOpportunity;

            Id pricebookId = Test.getStandardPricebookId();

            //Create your product
            Product2 prod = new Product2(Name = 'Product X',
                                                                ProductCode = 'Pro-X',
                                                                isActive = true,
                                                                Equipment_family__c = 'Chaudière',
                                                                Equipment_type1__c = 'Chaudière gaz',
                                                                Statut__c = 'Approuvée'
            );
            insert prod;

            //Create your pricebook entry
            PricebookEntry pbEntry = new PricebookEntry(Pricebook2Id = pricebookId,
                                                                                                Product2Id = prod.Id,
                                                                                                UnitPrice = 100.00,
                                                                                                IsActive = true
            );
            insert pbEntry;

            OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = lstOpportunity[0].Id,
                                                                                                        Quantity = 5,
                                                                                                        PricebookEntryId = pbEntry.Id,
                                                                                                        TotalPrice = pbEntry.UnitPrice
            );
            insert oli;

            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            lstAgence = new List<ServiceTerritory>{
                    new ServiceTerritory(Name='test', OperatingHoursId = lstOpHrs[0].Id,Phone__c='1234556666', Sofactoapp_Raison_Social__c = raisonSocial.Id)
            };
            insert lstAgence;
            
            lstLogement = new List<Logement__c>{
                                    new Logement__c(Street__c='rue test, stpierre, spoon, testing moka helvetiaaqwdweftgryjutjujybgryheyje',
                                                    Account__c = testAcc.Id,
                                                    Postal_Code__c = '75009',
                                                    City__c = 'Paris',
                                                    Agency__c = lstAgence[0].Id)
            };
            insert lstLogement;
            lstServiceApp = new List<ServiceAppointment>{
                                new ServiceAppointment(
                                        Status = 'Scheduled',
                                        Subject = 'this is a test subject',
                                        ParentRecordId = testAcc.Id,
                                        TECH_SA_payed__c = false,
                                        EarliestStartTime = System.now(),
                                        DueDate = System.now() + 30,
                                        Opportunity__c = lstOpportunity[0].Id,
                                        Service_Contract__c = lstContract[0].Id,
                                        Residence__c = lstLogement[0].Id,
                                        Category__c = 'VE Individuelle'
                                )
            };
            insert lstServiceApp;
            Esendex_Settings__c setting = new Esendex_Settings__c();
            setting.MessageDispatcher_AccountRef__c = 'EX0051959';
            setting.MessageDispatcher_Endpoint__c = 'https://api.esendex.com/v1.0/messagedispatcher';
            setting.MessageDispatcher_Password__c = 'NDX5107';
            setting.MessageDispatcher_Username__c = 'RDUPIN@CHALEUR-MAINTENANCE.FR';
            insert setting;
        }
    }

    @IsTest static void testSendSMSTrigger(){
            System.runAs(mainUser){
                testAcc.PersonMobilePhone='1232343245';
                update testAcc;
                lstServiceApp[0].Tech_Generate_Service_Report__c = true;

                Date myDate = Date.newInstance(2019, 11, 18);
                Time myTime = Time.newInstance(3, 3, 3, 0);
                DateTime dt = DateTime.newInstance(myDate, myTime);

                Date myDate2 = Date.newInstance(2019, 11, 19);
                Time myTime2 = Time.newInstance(3, 3, 3, 0);
                DateTime dt2 = DateTime.newInstance(myDate2, myTime2);

                lstServiceApp[0].SchedStartTime =dt; 
                lstServiceApp[0].SchedEndTime =dt2;
                lstServiceApp[0].Status = 'Scheduled';
                update lstServiceApp[0];
               
                Test.setMock(HttpCalloutMock.class, new createSuccessMock());
                Test.startTest();
                    lstServiceApp[0].Send_SMS__c = true;
                   
                    update lstServiceApp[0];
                    // List<ServiceAppointment> lstResult = [Select id,Send_SMS__c from ServiceAppointment where Id=:lstServiceApp[0].Id];
                    // System.debug('lstResult:'+lstResult);
                    // lstServiceApp[0].Status = 'Dispatched';
                    // update lstServiceApp[0];

                Test.stopTest();

            }
	}

    @IsTest static void testSendSMSTrigger2(){
            System.runAs(mainUser){
                testAcc.PersonMobilePhone='1232343245';
                update testAcc;
                lstServiceApp[0].Tech_Generate_Service_Report__c = true;

                Date myDate = Date.newInstance(2019, 11, 18);
                Time myTime = Time.newInstance(3, 3, 3, 0);
                DateTime dt = DateTime.newInstance(myDate, myTime);

                Date myDate2 = Date.newInstance(2019, 11, 19);
                Time myTime2 = Time.newInstance(3, 3, 3, 0);
                DateTime dt2 = DateTime.newInstance(myDate2, myTime2);

                lstServiceApp[0].SchedStartTime =dt; 
                lstServiceApp[0].SchedEndTime =dt2;
                lstServiceApp[0].Status = 'Scheduled';
                update lstServiceApp[0];
               
                Test.setMock(HttpCalloutMock.class, new createSuccessMock());
                Test.startTest();
                    lstServiceApp[0].Send_SMS__c = true;
                    lstServiceApp[0].VE_Planning__c = false;
                   
                    update lstServiceApp[0];
                    // List<ServiceAppointment> lstResult = [Select id,Send_SMS__c from ServiceAppointment where Id=:lstServiceApp[0].Id];
                    // System.debug('lstResult:'+lstResult);
                    // lstServiceApp[0].Status = 'Dispatched';
                    // update lstServiceApp[0];

                Test.stopTest();

            }
	}

    @IsTest static void testSendSMSMobile(){
            System.runAs(mainUser){
                WS10_SendSMS.Request req = new WS10_SendSMS.Request();
                req.to='12424324324';
                req.body='Test Message';
                
                Test.setMock(HttpCalloutMock.class, new createSuccessMock());
                Test.startTest();
                   Map<String,String> mapResponse = WS10_SendSMS.doPost(req);

                Test.stopTest();
            }
	}

    @IsTest static void testSendSMSMobileFail(){
            System.runAs(mainUser){
                WS10_SendSMS.Request req = new WS10_SendSMS.Request();
                req.to='';
                req.body='Test Message';
                
                Test.setMock(HttpCalloutMock.class, new createErrorMock());
                Test.startTest();
                   Map<String,String> mapResponse = WS10_SendSMS.doPost(req);

                Test.stopTest();
            }
	}
    
    
    @IsTest static void testSendSMSAsyncSuccess(){
        System.runAs(mainUser){
            WS10_SendSMS.Request req = new WS10_SendSMS.Request();
            req.to='13659874558';
            req.body='Test Message Success';
            
            Test.setMock(HttpCalloutMock.class, new createSuccessAsyncMock());
            Test.startTest();
            WS10_SendSms.sendSMSAsync(req.to, req.body, endPoint, token, lstServiceApp[0].Id, 'Test Agence 1', testAcc.Id, null, '26-05-2021');
            Test.stopTest();
        }
    }
   
    @IsTest static void testSendSMSAsyncFail(){
        System.runAs(mainUser){
            WS10_SendSMS.Request req = new WS10_SendSMS.Request();
            req.to=null;
            req.body='Test Message Error';
            
            Test.setMock(HttpCalloutMock.class, new createFailAsyncMock());
            Test.startTest();
                WS10_SendSms.sendSMSAsync(req.to, req.body, endPoint, token, lstServiceApp[0].Id, 'Test Agence 2', testAcc.Id, null, '26-05-2021');
            Test.stopTest();
        }
    }

    @IsTest static void testbuildSMSMessage(){
        System.runAs(mainUser){
            Datetime dt = Datetime.newInstance(2020, 07, 28, 12, 23, 05);
            String svcTerPho = 'serviceTerritoryPhone';
            String agency = 'Test agence';
            Test.startTest();
                String msg = WS10_SendSms.buildSMSMessage(dt,dt, svcTerPho, agency);
            Test.stopTest();
            System.assertEquals('Bonjour, sauf modification de votre part, nous vous rappelons que votre rendez-vous aura lieu le '
                                + dt.format('dd/MM/yyyy') +' pour l\'entretien de votre appareil par un technicien Test agence.'
                				+ ' En cas d\'absence merci d\'appeler l\'agence au serviceTerritoryPhone', msg);
            
            
        }
    }


    
    @IsTest static void testSendSMSMobileStatus201(){
        System.runAs(mainUser){
            WS10_SendSMS.Request req = new WS10_SendSMS.Request();
            req.to='12424324324';
            req.body='Test Message';
            
            Test.setMock(HttpCalloutMock.class, new createSuccessMock201());
            Test.startTest();
               Map<String,String> mapResponse = WS10_SendSMS.doPost(req);

            Test.stopTest();
        }
}


@IsTest static void testSendSMSAsyncSuccess201(){
    System.runAs(mainUser){
        WS10_SendSMS.Request req = new WS10_SendSMS.Request();
        req.to='13659874558';
        req.body='Test Message Success';
        
        Test.setMock(HttpCalloutMock.class, new createSuccessAsyncMock201());
        Test.startTest();
        WS10_SendSms.sendSMSAsync(req.to, req.body, endPoint, token, lstServiceApp[0].Id, 'Test Agence 1', testAcc.Id, null, '26-05-2021');
        Test.stopTest();
    }
}


public class createSuccessAsyncMock201 implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        String responseHttp;
        responseHttp = '{"batch":{"batchid":"08530bd8-8196-1a20-0408-13d6a7b70681","messageheaders":[{"uri":"http://api.esendex.com/v1.0/messageheaders/da875179-20b4-1067-031a-13d6a7b6ff01","id":"da875179-20b4-1067-031a-13d6a7b6ff01"}]},"errors":null}';        
        HttpResponse res = new HttpResponse();
        res.setBody(responseHttp);
        res.setStatusCode(201);
        return res;
    }
}

    public class createSuccessMock201 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseHttp;

            responseHttp = '{error=false, result={"batch":{"batchid":"ac1a14c3-20b7-106f-0590-10d7ac6a2d01","messageheaders":[{"uri":"http://api.esendex.com/v1.0/messageheaders/7e14851e-be90-1f9b-0407-10d7ac6a2781","id":"7e14851e-be90-1f9b-0407-10d7ac6a2781"}]},"errors":null}}';
           
            HttpResponse res = new HttpResponse();
            res.setBody(responseHttp);
            res.setStatusCode(201);
            return res;
        }
    }

    public class createSuccessMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseHttp;

            responseHttp = '{error=false, result={"batch":{"batchid":"ac1a14c3-20b7-106f-0590-10d7ac6a2d01","messageheaders":[{"uri":"http://api.esendex.com/v1.0/messageheaders/7e14851e-be90-1f9b-0407-10d7ac6a2781","id":"7e14851e-be90-1f9b-0407-10d7ac6a2781"}]},"errors":null}}';
           
            HttpResponse res = new HttpResponse();
            res.setBody(responseHttp);
            res.setStatusCode(200);
            res.setStatus('OK');
            return res;
        }
    }
    public class createErrorMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseHttp;

            responseHttp = '{error=true, result={"batch":{"batchid":"ac1a14c3-20b7-106f-0590-10d7ac6a2d01","messageheaders":[{"uri":"http://api.esendex.com/v1.0/messageheaders/7e14851e-be90-1f9b-0407-10d7ac6a2781","id":"7e14851e-be90-1f9b-0407-10d7ac6a2781"}]},"errors":null}}';
           
            HttpResponse res = new HttpResponse();
            res.setBody(responseHttp);
            res.setStatusCode(400);
            res.setStatus('OK');
            return res;
        }
    }
    
    public class createSuccessAsyncMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseHttp;
            responseHttp = '{"batch":{"batchid":"08530bd8-8196-1a20-0408-13d6a7b70681","messageheaders":[{"uri":"http://api.esendex.com/v1.0/messageheaders/da875179-20b4-1067-031a-13d6a7b6ff01","id":"da875179-20b4-1067-031a-13d6a7b6ff01"}]},"errors":null}';        
            HttpResponse res = new HttpResponse();
            res.setBody(responseHttp);
            res.setStatusCode(200);
            res.setStatus('OK');
            return res;
        }
    }

    public class createFailAsyncMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseHttp;
            responseHttp = 'Bad Request';        
            HttpResponse res = new HttpResponse();
            res.setBody(responseHttp);
            res.setStatusCode(400);
            res.setStatus('OK');
            return res;
        }
    }
}