/**
 * @File Name          : BAT05_UnitPricePBE_TEST.cls
 * @Description        : 
 * @Author             : KJB
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * ---    -----------       -------           ------------------------ 
 * 1.0    17/12/2019          KJB             Initial Version (CT-1222)
 * ---    -----------       -------           ------------------------ 
**/

@isTest
public class BAT05_UnitPricePBE_TEST {
    static User mainUser;
    static Account testAcc1 = new Account();
    static Account testAcc2 = new Account();
    static List<Product2> lstProd = new List<Product2>();
    static Pricebook2 standardPricebook = new Pricebook2();
    static List<PricebookEntry> lstPBE = new List<PricebookEntry>();
    static List<Conditions__c> lstConds = new List<Conditions__c>();

    
    static {
		mainUser = TestFactory.createAdminUser('LC23', TestFactory.getProfileAdminId());
        //Create Account
        testAcc1 = TestFactory.createAccount('Test1');
        testAcc1.BillingPostalCode = '1233456';
        testAcc1.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        testAcc1.Type = 'Constructeur';
        testAcc1.Actif__c = true;
        insert testAcc1;

        testAcc2 = TestFactory.createAccount('Test2');
        testAcc2.BillingPostalCode = '654321';
        testAcc2.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        testAcc2.Actif__c = true;
        insert testAcc2;

        //Create Products
        lstProd = new list<Product2>{ TestFactory.createProduct('Prod1'),
                                        TestFactory.createProduct('Prod2'),
                                        TestFactory.createProduct('Prod3'),
                                        TestFactory.createProduct('Prod4'),
                                        TestFactory.createProduct('Prod5')
        };

        lstProd[0].Famille_d_articles__c='Pièces détachées';
        lstProd[0].Sous_familles_d_articles__c = 'Autre';
        lstProd[0].Stock_non_gere__c = false;
        lstProd[0].Brand__c = 'INCONNU';
        lstProd[0].Lot__c = 1;

        lstProd[1].Famille_d_articles__c='Pièces détachées';
        lstProd[1].Sous_familles_d_articles__c = 'Autre';
        lstProd[1].Stock_non_gere__c = false;
        lstProd[1].Brand__c = 'INCONNU';
        lstProd[1].Lot__c = 1;

        lstProd[2].Famille_d_articles__c='Pièces détachées';
        lstProd[2].Sous_familles_d_articles__c = 'Autre';
        lstProd[2].Stock_non_gere__c = false;
        lstProd[2].Brand__c = 'CHAFFOTEAUX';
        lstProd[2].Lot__c = 1;

        lstProd[3].Famille_d_articles__c='Pièces détachées';
        lstProd[3].Sous_familles_d_articles__c = 'Autre';
        lstProd[3].Stock_non_gere__c = false;
        lstProd[3].Brand__c = 'CHAFFOTEAUX';
        lstProd[3].Lot__c = NULL;

        lstProd[4].Famille_d_articles__c='Pièces détachées';
        lstProd[4].Sous_familles_d_articles__c = 'Autre';
        lstProd[4].Stock_non_gere__c = false;
        lstProd[4].Brand__c = 'INCONNU';
        lstProd[4].Lot__c = 1;

        insert lstProd;

        //Create Pricebook
        PriceBook2 pb1 = new PriceBook2();
        pb1.name = 'Pricebook 1';
        pb1.isActive = true;
        pb1.RecordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId();
        pb1.Compte__c = testAcc1.Id;
        insert pb1;

        PriceBook2 pb2 = new PriceBook2();
        pb2.name = 'Pricebook 2';
        pb2.isActive = true;
        pb2.RecordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId();
        pb2.Compte__c = testAcc1.Id;
        insert pb2;

        PriceBook2 pb3 = new PriceBook2();
        pb3.name = 'Pricebook 3';
        pb3.isActive = true;
        pb3.RecordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId();
        pb3.Compte__c = testAcc2.Id;
        insert pb3;

        PriceBook2 pb4 = new PriceBook2();
        pb4.name = 'Pricebook 4';
        pb4.isActive = true;
        pb4.RecordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId();
        pb4.Compte__c = testAcc1.Id;
        insert pb4;

        //Create Pricebook Entry
        lstPBE = new list<PricebookEntry>{
            new PricebookEntry(Product2Id = lstProd[0].Id,
                               PriceBook2Id = Test.getStandardPricebookId(),
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5)
                /*,

            new PricebookEntry(Product2Id = lstProd[0].Id,
                               PriceBook2Id = pb1.Id,
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5), 

            new PricebookEntry(Product2Id = lstProd[1].Id,
                               PriceBook2Id = Test.getStandardPricebookId(),
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5),

            new PricebookEntry(Product2Id = lstProd[1].Id,
                               PriceBook2Id = pb2.Id,
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5),

            new PricebookEntry(Product2Id = lstProd[1].Id,
                               PriceBook2Id = pb3.Id,
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5),

             new PricebookEntry(Product2Id = lstProd[2].Id,
                               PriceBook2Id = Test.getStandardPricebookId(),
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5), 

            new PricebookEntry(Product2Id = lstProd[2].Id,
                               PriceBook2Id = pb3.Id,
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5),

            new PricebookEntry(Product2Id = lstProd[3].Id,
                               PriceBook2Id = Test.getStandardPricebookId(),
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5),

            new PricebookEntry(Product2Id = lstProd[3].Id,
                               PriceBook2Id = pb3.Id,
                               Override__c = false,
                               UnitPrice = 5,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5),

            new PricebookEntry(Product2Id = lstProd[4].Id,
                               PriceBook2Id = Test.getStandardPricebookId(),
                               Override__c = false,
                               UnitPrice = 110,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5),                             
            
            new PricebookEntry(Product2Id = lstProd[4].Id,
                               PriceBook2Id = pb4.Id,
                               Override__c = false,
                               UnitPrice = 110,
                               Prix_achat_brut_prix_public__c = 10,
                               Remise__c = 0.5)      */                                                      
        };
        insert lstPBE;

        //Create Conditions
        lstConds = new list<Conditions__c>{
            new Conditions__c(Compte__c = testAcc1.Id,
                              Produit__c = lstProd[0].Id,
                              Famille_d_articles__c='Pièces détachées',
                              Sous_famille_d_articles__c = 'Autre',
                              marque__c = 'INCONNU',
                              Debut__c = Date.today().addDays(-10),
                              Fin__c = Date.today()),

            new Conditions__c(Compte__c = testAcc2.Id,
                              Produit__c = lstProd[0].Id,
                              Famille_d_articles__c='Pièces détachées',
                              Sous_famille_d_articles__c = 'Autre',
                              marque__c = 'INCONNU',
                              Debut__c = Date.today().addDays(-10),
                              Fin__c = Date.today())
        };
        insert lstConds;
    }

    @isTest
    public static void Test(){
        System.runAs(mainUser){
            Test.startTest();
     
           new BAT05_UnitPricePBE().doExecute(lstPBE);
            
                        BAT05_UnitPricePBE.UpdateInt();
             BAT05_UnitPricePBE.UpdateLong();
             BAT05_UnitPricePBE.UpdateDecimal();
            BAT05_UnitPricePBE.UpdateDate();
             BAT05_UnitPricePBE.UpdateTime();
            BAT05_UnitPricePBE.UpdateInt1();
            BAT05_UnitPricePBE.UpdateLong1();
             BAT05_UnitPricePBE.UpdateDecimal1();
            BAT05_UnitPricePBE.UpdateDate1();
             BAT05_UnitPricePBE.UpdateTime1();
            
            BAT05_UnitPricePBE.UpdateInt2();
            BAT05_UnitPricePBE.UpdateLong2();
             BAT05_UnitPricePBE.UpdateDecimal2();
            BAT05_UnitPricePBE.UpdateDate2();
             BAT05_UnitPricePBE.UpdateTime2();
            Test.stopTest();
        }
        
    }
    
}