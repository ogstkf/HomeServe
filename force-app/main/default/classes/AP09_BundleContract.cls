/**
 * @File Name          : AP09_BundleContract.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 23/06/2020, 12:23:25
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    02/07/2019, 15:50:51   RRJ     Initial Version
**/
public class AP09_BundleContract {

    public static void populateConLineItems(Map<Id, ContractLineItem> mapProdIdToConLnItem){
        // System.debug('##### mapProdIdToConLnItem: '+mapProdIdToConLnItem);
        System.debug('enter populate func');
        Map<Id, ContractLineItem> mapConLineItem = new Map<Id, ContractLineItem>();
        for(ContractLineItem conLnItem : mapProdIdToConLnItem.values()){
            mapConLineItem.put(conLnItem.Id, conLnItem);
        }

        Map<Id, Id> mapConLineToServCon = new Map<Id, Id>();
        for(ContractLineItem conLnItem: mapProdIdToConLnItem.values()){
            if(conLnItem.ServiceContractId != null){
                mapConLineToServCon.put(conLnItem.Id, conLnItem.ServiceContractId);
            }
        }

        Set<Id> setPrcBkId = new Set<Id>();
        Map<Id, ServiceContract> mapServCon = new Map<Id, ServiceContract>([SELECT Id, StartDate, EndDate, PriceBook2Id FROM ServiceContract WHERE Id IN :mapConLineToServCon.values()]);
        Map<Id, ServiceContract> mapConLineToServiceCon = new Map<Id, ServiceContract>();
        for(ContractLineItem conLnItem: mapConLineItem.values()){
            if(mapServCon.containsKey(conLnItem.ServiceContractId)){
                mapConLineToServiceCon.put(conLnItem.Id, mapServCon.get(conLnItem.ServiceContractId));
                setPrcBkId.add(mapConLineToServiceCon.get(conLnItem.Id).PriceBook2Id);
            }
        }
        // System.debug('##### setPrcBkId: '+setPrcBkId);

        Map<Id, Product_Bundle__c> mapBunToProdBun = new Map<Id, Product_Bundle__c>();
        for(Product_Bundle__c prodBun : [SELECT Id, Name, Bundle__c, Product__c FROM Product_Bundle__c WHERE Product__c IN :mapProdIdToConLnItem.keySet() AND IsBundle__c = true]){
            mapBunToProdBun.put(prodBun.Bundle__c, prodBun);
        }
        // System.debug('##### mapBunToProdBun: '+mapBunToProdBun);

        List<Product_Bundle__c> lstProdBun = [SELECT Id, Name, Product__c, Bundle__c FROM Product_Bundle__c WHERE Bundle__c IN :mapBunToProdBun.keySet()  AND Optional_Item__c = false AND IsBundle__c = false];
        // System.debug('##### lstProdBun: '+ lstProdBun);

        Set<Id> setProdId = new Set<Id>();
        for(Product_Bundle__c prodBun : lstProdBun){
            setProdId.add(prodBun.Product__c);
        }
        // System.debug('##### setProdId: '+setProdId);

        //build map of external key to pricebook entry. ExternalKey = Product2Id + PriceBook2Id
        Map<String, Id> mapExternalIdToPrcBkId = new Map<String, Id>();
        for(PriceBookEntry prcBkEntry : [SELECT Id, Product2Id, PriceBook2Id FROM PriceBookEntry WHERE Product2Id IN :setProdId AND PriceBook2Id IN :setPrcBkId]){
            String extId = ''+prcBkEntry.Product2Id+prcBkEntry.PriceBook2Id;
            mapExternalIdToPrcBkId.put(extId, prcBkEntry.Id);
        }
        // System.debug('##### mapExternalIdToPrcBkId: '+mapExternalIdToPrcBkId);

        List<ContractLineItem> lstConLineItemToInsert = new List<ContractLineItem>();
        for(Product_Bundle__c prodBun : lstProdBun){
            // System.debug('##### extId: '+''+prodBun.Product__c+mapConLineToServiceCon.get(mapProdIdToConLnItem.get(mapBunToProdBun.get(prodBun.Bundle__c).Product__c).Id).PriceBook2Id);
            if(mapExternalIdToPrcBkId.get(''+prodBun.Product__c+mapConLineToServiceCon.get(mapProdIdToConLnItem.get(mapBunToProdBun.get(prodBun.Bundle__c).Product__c).Id).PriceBook2Id) != null){
                System.debug('creating clis');
                ContractLineItem conLineItem = new ContractLineItem(
                    ParentContractLineItemId = mapProdIdToConLnItem.get(mapBunToProdBun.get(prodBun.Bundle__c).Product__c).Id,
                    ServiceContractId = mapProdIdToConLnItem.get(mapBunToProdBun.get(prodBun.Bundle__c).Product__c).ServiceContractId,
                    AssetId = mapProdIdToConLnItem.get(mapBunToProdBun.get(prodBun.Bundle__c).Product__c).AssetId,
                    StartDate = mapConLineToServiceCon.get(mapProdIdToConLnItem.get(mapBunToProdBun.get(prodBun.Bundle__c).Product__c).Id).StartDate,
                    EndDate = mapConLineToServiceCon.get(mapProdIdToConLnItem.get(mapBunToProdBun.get(prodBun.Bundle__c).Product__c).Id).EndDate,
                    PricebookEntryId = mapExternalIdToPrcBkId.get(''+prodBun.Product__c+mapConLineToServiceCon.get(mapProdIdToConLnItem.get(mapBunToProdBun.get(prodBun.Bundle__c).Product__c).Id).PriceBook2Id),
                    Quantity = 1,
                    UnitPrice = 0,
                    Discount = 0
                    //ZJO added active
                    // IsActive__c = True
                ); 
                lstConLineItemToInsert.add(conLineItem);
            }
        }

        // System.debug('#### lstConLineItemToInsert: '+lstConLineItemToInsert);
        if(lstConLineItemToInsert.size()>0){
            insert lstConLineItemToInsert;
        }
    }

    public static void deleteBundleConLineItems(Set<Id> setConLnItemId){
        // System.debug('#### setConLnItemId: '+setConLnItemId);
        List<Id> lstConLnItemToDelete = new List<Id>();
        for(ContractLineItem cnLnItem : [SELECT Id FROM ContractLineItem WHERE ParentContractLineItemId IN :setConLnItemId AND ParentContractLineItem.Product2.IsBundle__c = true AND Id NOT IN :setConLnItemId]){
            lstConLnItemToDelete.add(cnLnItem.Id);
        }
        // System.debug('### lstConLnItemToDelete: '+ lstConLnItemToDelete);
        // System.debug('### lstConLnItemToDelete: '+ lstConLnItemToDelete.size());
        
        if(lstConLnItemToDelete.size()>0) deleteConLnItems(lstConLnItemToDelete);
    }

    @future
    public static void deleteConLnItems(List<Id> lstItemToDelete){
        List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();
        // List<ContractLineItem> lstConLnItem = [SELECT Id FROM ContractLineItem WHERE Id IN :lstItemToDelete];
        for(Id cnLnItemId : lstItemToDelete){
            lstConLnItem.add(new ContractLineItem(Id = cnLnItemId));
        }
        // System.debug('##### lstConLnItem: '+lstConLnItem);
        if(lstConLnItem.size()>0) delete lstConLnItem;
    }
}