/**
 * @File Name          : LC04_MoveInWizard_TEST.cls
 * @Description        : test class for LC04_MoveInWizard
 * @Author             : SH
 * @Group              : 
 * @Last Modified By   : SH
 * @Last Modified On   : 06/01/2020, 18:14:51
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    06/01/2020, 18:06:39   SH     Initial Version
**/
@isTest
public class LC04_MoveInWizard_TEST {

    static User adminUser;
    static Bypass__c bp = new Bypass__c();

    static {
        adminUser = TestFactory.createAdminUser('AP12_ServiceAppointmentRules_TEST@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;

        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassValidationRules__c = True;
        insert bp;
    }
   
    @isTest
    static void testResearchLogement(){
        LC04_MoveInWizard.search('stringOver3chars');
    }

    @isTest
    static void testGetPickLists(){
        String strPickVals = '[{"label":"Logement__c","value":"Professional_Use__c"}]';
        LC04_MoveInWizard.getPicklists(strPickVals);
    }

    @isTest
    static void testAnalyseGeo(){
        String strData = '{"clSoc":false,"clPays":"France","clPaysLog":"France","clSocPays":"France","clCPLog":"59000","clMotif":""}';
        LC04_MoveInWizard.analyseGeo(strData);
    }
	
    @isTest
    static void testCreerLogement(){
        System.runAs(adminUser){
            Account testAcc1 = new Account();
            Product2 produit = new Product2();

            // Create Account
            testAcc1 = TestFactory.createAccount('test');
            testAcc1.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc1.FirstName = 'acc';
            testAcc1.LastName = 'test';
            testAcc1.BillingPostalCode = '1233456';
            testAcc1.Phone = '1234567890';
            testAcc1.PersonEmail = 'test@mail.com';
            insert testAcc1;

            // Create Product
            produit = TestFactory.createProduct('Ramonage gaz');
            produit.Brand__c = 'INCONNU';
            produit.Equipment_type1__c = 'Chaudière gaz';
            insert produit;

            String strData = '{"clPays":"France","clPaysLog":"France","clOccupant":true,"clProprietaire":true,"idActualInhabitant":"' + testAcc1.Id + '",' + 
                    '"dateEmmenagement":"2020-01-14","clUsagePro":"Non (moins de 50% d\'usage professionnel)","clAgeLogement":"Plus_2ans",' + 
                    '"clRueLog":"11Foch","clCPLog":"56000","clVilleLog":"VANNES","clCP":"56000","clRue":"11Foch","clVille":"VANNES",' +
                    '"clEqpType":"Chaudière gaz","clEquipementHPE":true,"clAstEqpType":"Chauffe eau"}';
            LC04_MoveInWizard.creerLogement(strData);
        }
    }

    @isTest
    static void testMoveInToExistingLogement(){
        Account testAcc1 = new Account();
        OperatingHours opHrs = new OperatingHours();
        ServiceTerritory srvTerr = new ServiceTerritory();
        Logement__c Logement = new Logement__c();
        sofactoapp__Raison_Sociale__c rs = new sofactoapp__Raison_Sociale__c();

        // Create Account
        testAcc1 = TestFactory.createAccount('test');
        testAcc1.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        testAcc1.FirstName = 'acc';
        testAcc1.LastName = 'test';
        testAcc1.BillingPostalCode = '1233456';
        testAcc1.Phone = '1234567890';
        testAcc1.PersonEmail = 'test@mail.com';
        insert testAcc1;

        //create operating hours
        opHrs = TestFactory.createOperatingHour('test OpHrs');
        insert opHrs;

        //Create Corporate Name
        rs.Name = 'Test Corporate';
        rs.sofactoapp__Invoice_prefix__c = 'Invoice';
        rs.sofactoapp__Credit_prefix__c = 'Credit';
        insert rs;

        //create service territory
        srvTerr = TestFactory.createServiceTerritory('test Territory', opHrs.Id, rs.Id);
        insert srvTerr;

        //creating logement
        Logement = TestFactory.createLogement(testAcc1.Id, srvTerr.Id);
        Logement.Postal_Code__c = '12345';
        Logement.City__c = 'lgmtCity 1';
        Logement.Account__c = testAcc1.Id;
        Logement.Inhabitant__c = testAcc1.Id;
        insert Logement;

        String strData = '{"clPays":"France","clPaysLog":"France","clOccupant":true,"idActualInhabitant":"' + testAcc1.Id + '","dateEmmenagement":"2020-01-14","clProprietaire":true,"idActualLogement":"' + Logement.Id + '"}';
        LC04_MoveInWizard.moveInToExistingLogement(strData);
    }

}