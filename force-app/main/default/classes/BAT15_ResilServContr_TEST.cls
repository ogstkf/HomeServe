/**
 * @File Name          : BAT15_ResilServContr_TEST.cls
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : LGO
 * @Last Modified On   : 11-01-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/01/2021   LGO     Initial Version
**/
@IsTest
public with sharing class BAT15_ResilServContr_TEST {
    static User adminUser;
    static List<Account> lstAccount;
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<ServiceContract> lstServCon;

    static{
        adminUser = TestFactory.createAdminUser('BAT15@test.com', TestFactory.getProfileAdminId());
        insert adminUser;


        System.runAs(adminUser){
             //List of Accounts 
             lstAccount = new List<Account>{
                TestFactory.createAccount('Test Acc 1'),
                TestFactory.createAccount('Test Acc 2'),
                TestFactory.createAccount('Test Acc 3')
            };

            insert lstAccount;
             
            //Update Accounts
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAccount.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstAccount.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstAccount.get(2).Id);
			lstAccount.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstAccount.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstAccount.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            
            update lstAccount;

            //Create Product
            Product2 prod = TestFactory.createProduct('testProd');
            insert prod;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //List of Service Contracts
            lstServCon = new List<ServiceContract>{
                new ServiceContract(
                  Name = 'serv con 1',
                  AccountId = lstAccount[0].Id,
                  StartDate = System.today().addDays(-3),
                  EndDate = System.today().addDays(-1),
                  Contract_Status__c = 'Draft',
                  Type__c = 'Individual',
                  Contrat_resilie__c = true,
                  Pricebook2Id = lstPrcBk[0].Id
                ),
                new ServiceContract(
                  Name = 'serv con 2',
                  AccountId = lstAccount[1].Id,
                  StartDate = System.today().addDays(-3),
                  EndDate = System.today().addDays(-1),
                  Contract_Status__c = 'Draft',
                  Type__c = 'Individual',
                  Contrat_resilie__c = true,
                  Pricebook2Id = lstPrcBk[0].Id
                ),
                new ServiceContract(
                  Name = 'serv con 3',
                  AccountId = lstAccount[2].Id,
                  StartDate = System.today().addDays(-3),
                  EndDate = System.today().addDays(-1),
                  Contract_Status__c = 'Actif - en retard de paiement',
                  Type__c = 'Individual',
                  Contrat_resilie__c = false,
                  Pricebook2Id = lstPrcBk[0].Id
                )
            };
            insert lstServCon;

        }
    }

    @IsTest
    public static void testBatch(){
        System.runAs(adminUser){
            Test.startTest();
            BAT15_ResilServContr batch = new BAT15_ResilServContr();
            Database.executeBatch(batch);
            Test.stopTest();
        }
    }
    
    @IsTest
    public static void testScheduleBatch(){
        System.runAs(adminUser){
            Test.startTest();
            BAT15_ResilServContr.scheduleBatch();
            Test.stopTest();
        }
    }
 
}