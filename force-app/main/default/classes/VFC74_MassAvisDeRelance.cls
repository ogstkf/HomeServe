/**
 * @File Name          : VFC74_MassAvisDeRelance.cls
 * @Description        : Génération PDF en masse de plusieurs avis de relance contrat
 * @Author             : RRA
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 26/08/2020, 12:25:00
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    26/08/2020, 12:25:00   RRA     Initial Version */
public with sharing class VFC74_MassAvisDeRelance {

    public ApexPages.StandardSetController controller {get; set;}
    public List<ServiceContract> lstSelected {get; set;}
    public String lstId {get; set;}
    public Boolean isEmpty {get; set;}
    
    public VFC74_MassAvisDeRelance(ApexPages.StandardSetController controller){
        System.debug('Controller');
        this.controller = controller;
        lstSelected = controller.getSelected();
        System.debug('lstSelected' + lstSelected);
        lstId = serializeIds();
        System.debug('lstId' + lstId);
        isEmpty = controller.getSelected().isEmpty();         
    }

    public String serializeIds(){
        List<String> lstStr = new List<String>();
        System.debug('lstSelected' + lstSelected);
        for(ServiceContract srvCt : lstSelected){
            lstStr.add(srvCt.Id);
        }
        System.debug('lstStr' + lstStr);
        return JSON.serialize(lstStr);
    }

    @RemoteAction
    public static String openPDF(List<String> lstsrvCtId){
        System.debug('##### lstsrvCtId: '+ lstsrvCtId);
        String resp = '';
        Savepoint sp = Database.setSavepoint();
        List<ContentDocumentLink> lstCDLId = new List<ContentDocumentLink>();
        Map<String, String> mapCVTitleAgencyName = new Map<String, String>(); 
        list<ContentVersion> contentVersionLst = new List<ContentVersion>();
        map<String, String> mapCWNAmeId = new map<String, String>();     
        Map<String, String> mapCVTitleSAId = new Map<String, String>(); 
        string userAlias = [SELECT Id, Alias FROM USER WHERE Id = :UserInfo.getUserId() LIMIT 1].Alias;

        try{

            for (ServiceContract sc : [SELECT Id, Agency__r.Name FROM ServiceContract WHERE Id IN : lstsrvCtId]) {
                System.debug('##### sc Id: '+ JSON.serialize(sc.Id));
                PageReference template = new PageReference('/apex/VFP74_RelanceContrat');
                template.getParameters().put('lstIds', '['+JSON.serialize(sc.Id)+']');
                Blob attData = Blob.valueOf('t');
                if(!Test.isRunningTest()) attData = template.getContentAsPDF();
                                
                //create attachments to insert
                ContentVersion conVer = new ContentVersion();
                conVer.ContentLocation = 'S';
                conVer.PathOnClient = generateName(userAlias);
                conVer.Title = generateName(userAlias);
                conVer.VersionData = attData;  
                mapCVTitleAgencyName.put(conVer.Title, sc.Agency__r.Name);
                mapCVTitleSAId.put(conVer.Title, sc.Id);
                contentVersionLst.add(conVer); 
                System.debug('*** nomAgence: ' + sc.Agency__r.Name);     
                System.debug('*** Title: ' + conVer.Title);                   
            }

            //Considering listViews will be filtered by agence, in list of SC below, all agence will be same.
            String agenceName = mapCVTitleAgencyName.size()>0 ? mapCVTitleAgencyName.values()[0] : '' ;
            System.debug('*** agenceName: ' + agenceName);    

            String foldertoUse = 'Avis de relance '+ agenceName;

            System.debug('*** mapCVTitleAgencyName222: ' + mapCVTitleAgencyName);
            //PDF qui englobe tous les pdf unitaire
            PageReference template1 = new PageReference('/apex/VFP74_RelanceContrat'); 
            template1.getParameters().put('lstIds',  JSON.serialize(lstsrvCtId));
            Blob attData1 = Blob.valueOf('t');
            if(!Test.isRunningTest()) attData1 = template1.getContentAsPDF();                                     
            //create attachments to insert
            ContentVersion conVer1 = new ContentVersion();
            conVer1.ContentLocation = 'S';
            conVer1.PathOnClient = generateName(userAlias);
            conVer1.Title = generateName(userAlias);
            conVer1.VersionData = attData1;  
            contentVersionLst.add(conVer1);

            if(contentVersionLst.size()>0) {
                insert contentVersionLst;
            }

            //Get Content Documents                               
            List<ContentVersion> docList = [SELECT Id, ContentDocumentId, Title FROM ContentVersion WHERE Id IN : contentVersionLst order by ContentSize desc];            
            System.debug('docList : ' + docList);
            System.debug('docListSize: ' + docList.size());

            if(docList.size()>0){
                ContentDocument conDoc = new ContentDocument(Id = docList[0].ContentDocumentId);
                System.debug('conDoc : ' + conDoc);
                Pagereference resp1 = new ApexPages.StandardController(conDoc).view();
                resp = resp1.getUrl();
            }

            for(ContentWorkspace cw: [SELECT Id, RootContentFolderId, Name FROM ContentWorkspace WHERE Name =: foldertoUse OR Name LIKE '%Avis de relance%']){ 
                mapCWNAmeId.put(cw.Name,cw.Id);
            }
            System.debug('*** mapCWNAmeId: ' + mapCWNAmeId);
            System.debug('*** mapCVTitleAgencyName: ' + mapCVTitleAgencyName);

            for(ContentVersion cv : docList) {              //first element of docList is the consolidated pdf when more than 1 Sa selected
                ContentDocumentLink cdl = new ContentDocumentLink();                        
                cdl.ContentDocumentId =  cv.ContentDocumentId; 
                cdl.ShareType = 'I';
                cdl.Visibility = 'AllUsers';                
                cdl.LinkedEntityId = mapCVTitleSAId.containsKey(cv.Title) ? mapCVTitleSAId.get(cv.Title) : (mapCWNAmeId.containsKey(foldertoUse) ? mapCWNAmeId.get(foldertoUse) : mapCWNAmeId.get('Avis de relance'));
                //mapCWNAmeId.containsKey(foldertoUse) ? mapCWNAmeId.get(foldertoUse) : mapCWNAmeId.get('Avis De Passage'); //mapCWNAmeId.containsKey(mapCVTitleAgencyName.get(cv.Title)) ? mapCWNAmeId.get(mapCVTitleAgencyName.get(cv.Title)) : !Test.isRunningTest() ?  mapCWNAmeId.get('Avis De Passage') : mapCWNAmeId.get('Avis De Passage test');
                lstCDLId.add(cdl);
            }
            System.debug('*** keyFolder: ' + mapCWNAmeId.containsKey(foldertoUse));
            System.debug('*** folder: ' + mapCWNAmeId.get(foldertoUse));
            System.debug('*** AvisRelance: ' + mapCWNAmeId.get('Avis de relance'));
            
            if (lstCDLId.size() > 0){
                upsert lstCDLId;
            }
            
            List<ServiceContract> lstScToUpdate = new List<ServiceContract>();
            for(String strScId: lstsrvCtId){
                ServiceContract servCt  = new ServiceContract(
                    Id = strScId,
                    //Visit_Notice_Generated_In_Mass__c = true,
                    TECH_Date_Edition_R1__c = system.today()
                );
                lstScToUpdate.add(servCt);
            }
            update lstScToUpdate;
                        
        } catch(Exception e){
            Database.rollback(sp);
            System.debug('##### error occured: '+e.getMessage()+'-'+e.getStackTraceString());
            throw new AP_Constant.CustomException(e.getMessage());
        }
        System.debug('resp' + resp);
        
        return resp;        
    }

    private static String generateName(String alias){ 
        system.debug('*** in generateName alias:  '+alias);        

        string randomUniqueIdentifier = randomizeStringVFC07('');
        system.debug('*** in generateName randomUniqueIdentifier:  '+randomUniqueIdentifier);

        String pdfName = '';
        DateTime datToday = DateTime.now();
        pdfName += datToday.day() < 10 ? '0'+datToday.day() : datToday.day().format();
        pdfName += datToday.month() < 10 ? '0'+datToday.month() : datToday.month().format();
        pdfName += datToday.year();
        pdfName += datToday.hour() < 10 ? '0'+datToday.hour() : datToday.hour().format();
        pdfName += datToday.minute() < 10 ? '0'+datToday.minute() : datToday.minute().format();
        pdfName += datToday.second() < 10 ? '0'+datToday.second() : datToday.second().format();
        pdfName += '-';
        pdfName += alias;
        pdfName += '-' + randomUniqueIdentifier;
        pdfName += '.pdf';
        return pdfName;
    }

    public static String randomizeStringVFC07(String name) {
        String charsForRandom = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < 6) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), charsForRandom.length());
            randStr += charsForRandom.substring(idx, idx + 1);
        }
        return name + randStr;
    }

}