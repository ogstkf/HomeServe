/**
 * @File Name          : AP44_UpdateCLIStatus.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 23/01/2020, 10:38:12
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    14/11/2019   AMO     Initial Version
**/
public with sharing class AP44_UpdateCLIStatus {
    public static Date toDate = System.Today();
    public static void UpdateStatus(List<ServiceContract> lstNewServCon){

        List<ContractLineItem> lstUpdateCLI = new List<ContractLineItem>();
        for(ContractLineItem cli : [SELECT Id, Debut_de_la_suspension__c, Fin_de_la_suspension__c, IsActive__c, ServiceContractId FROM ContractLineItem WHERE ServiceContractId IN :lstNewServCon]){
            cli.IsActive__c = (cli.Debut_de_la_suspension__c != null && cli.Fin_de_la_suspension__c != null) &&
            (cli.Debut_de_la_suspension__c <= toDate && cli.Fin_de_la_suspension__c >= toDate) ? false : false;
            //cli.IsActive__c = false;
            lstUpdateCLI.add(cli);
        }

        System.debug('Contract line items to update: ' + lstUpdateCLI.size());
        if(lstUpdateCLI.size() > 0){
            Update lstUpdateCLI;
        }
        
    }

        public static void UpdateStatusActive(List<ServiceContract> lstNewServCon){
        List<ContractLineItem> lstUpdateCLI = new List<ContractLineItem>();     
        for(ContractLineItem cli : [SELECT Id, Debut_de_la_suspension__c, Fin_de_la_suspension__c, IsActive__c, ServiceContractId FROM ContractLineItem WHERE ServiceContractId IN :lstNewServCon]){
            cli.IsActive__c = (cli.Debut_de_la_suspension__c != null && cli.Fin_de_la_suspension__c != null) &&
            (cli.Debut_de_la_suspension__c <= toDate && cli.Fin_de_la_suspension__c >= toDate) ? false : true;
            lstUpdateCLI.add(cli);
        }

        System.debug('Contract line items to update: ' + lstUpdateCLI.size());
        if(lstUpdateCLI.size() > 0){
            Update lstUpdateCLI;
        }
        
    }
}