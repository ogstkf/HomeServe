global class ssa_Completion_CompteComptable implements 
Database.Batchable<sObject>, Database.Stateful, Schedulable {

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'select id, sofactoapp__Compte_comptable_produit__c, sofactoapp__Compte_comptable_TVA__c,sofactoapp__Taux_de_TVA_list__c ';
            query += ' from sofactoapp__Ligne_de_Facture__c ';
            query += ' where sofactoapp__Compte_comptable_produit__c =null ';
            query += ' and sofactoapp__Facture__r.SofactoappType_Prestation__c = \'Prestations / travaux\'';
            query += ' and sofactoapp__Taux_de_TVA_list__c !=\'0%\'';
            query += ' and sofactoapp__Facture__r.sofactoapp__Amount_VAT__c !=0 and CreatedDate =LAST_90_DAYS';
            system.Debug('### : ' + query);
            return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sofactoapp__Ligne_de_Facture__c> scope){
        list<sofactoapp__Ligne_de_Facture__c> lstFac = new list <sofactoapp__Ligne_de_Facture__c>();
        for (sofactoapp__Ligne_de_Facture__c FAC :scope)
            {
                if (FAC.sofactoapp__Taux_de_TVA_list__c == '5,5%' )
                {
                    FAC.sofactoapp__Compte_comptable_produit__c = System.Label.Sofacto_Compte_produit_TVA_5;
                    FAC.sofactoapp__Compte_comptable_TVA__c = System.Label.Sofacto_compte_TVa_5;
                    lstFac.add (FAC);
                }
                if (FAC.sofactoapp__Taux_de_TVA_list__c == '10%'  )
                {
                    FAC.sofactoapp__Compte_comptable_produit__c = System.Label.Sofacto_Compte_Produit_10 ;
                    FAC.sofactoapp__Compte_comptable_TVA__c = System.Label.Sofacto_Compte_Comptable_TVA_10;
                    lstFac.add (FAC);
                }
                if  (FAC.sofactoapp__Taux_de_TVA_list__c == '20%'  )
                {
                    FAC.sofactoapp__Compte_comptable_produit__c = System.Label.Sofacto_compte_produit_TVA_20;
                    FAC.sofactoapp__Compte_comptable_TVA__c = System.Label.Sofacto_compte_tva_20;
                    lstFac.add (FAC);
                }
            }
            if (lstFac.size()>0){
                try {
                    update lstFac;
                }
                catch(Exception ex){
                System.debug('*** error message: '+ ex.getMessage());
                }
            }
    }

    global static String scheduleBatch() {
        ssa_Completion_CompteComptable scheduler = new ssa_Completion_CompteComptable();
        //String scheduleTime = '0 0 00 * * ?';
        //return System.schedule('Batch:', scheduleTime, scheduler);
        return System.schedule('Batch Update Compte comptable ligne facture :' + Datetime.now().format(),  '0 0 0 * * ?', scheduler);
    }

    global void execute(SchedulableContext sc){
        Database.executeBatch(new ssa_Completion_CompteComptable());
        }
        
        global void finish(Database.BatchableContext bc){
        } 
}