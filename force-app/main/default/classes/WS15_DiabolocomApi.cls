/**
 * @File Name         : WS15_DiabolocomApi.cls
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 13-01-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   21-12-2021   MNA   Initial Version
**/
@RestResource(urlMapping='/customer/*')
global with sharing class WS15_DiabolocomApi {
    @HttpPost
    global static customerObject checkClient() {
        RestRequest req = RestContext.request;
        Map<String, String> mapParam = getParams(req);

        String phone = mapParam.get('phone');
        List<List<SObject>> lstSO = [FIND :phone IN PHONE FIELDS RETURNING Account(Id), Contact(Id)];

        List<Account> lstAcc = lstSO[0];
        List<Contact> lstCon = lstSO[1];

        if (lstAcc.size() > 0 || lstCon.size() > 0) {
            return new customerObject(true);
        }
        else {
            return new customerObject(false);
        }
    }

    global static Map<String, String> getParams (RestRequest req) {
        List<String> params = req.params.toString().removeStart('{').removeEnd('}').split(',');
        Map<String, String> mapParam = new Map<String, String>();
        for (String param : params) {
            List<String> paramValue = param.split('=');
            mapParam.put(paramValue[0].toLowerCase(), paramValue[1]);
        }
        return mapParam;
    }

    global class customerObject {
        global Boolean customer_known;
        global customerObject (Boolean customer_known) {
            this.customer_known = customer_known;
        }
    }

}