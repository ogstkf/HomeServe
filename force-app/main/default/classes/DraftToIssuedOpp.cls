public class DraftToIssuedOpp {
     @InvocableMethod
    public static void OppInvoiceIssued(List<ID> recordIds){
        
            List <sofactoapp__Factures_Client__c> Factures = new  List <sofactoapp__Factures_Client__c>() ;       
        	For (sofactoapp__Factures_Client__c Fact : [select id, sofactoapp__Date_de_facture__c,sofacto_Devis__r.Agency__r.Sofactoapp_Raison_Social__c,
                                                        sofacto_Devis__r.Ordre_d_execution__c,sofacto_Devis__r.Salarie_primable__c,sofacto_Devis__r.Nom_du_payeur__c
                                                        from sofactoapp__Factures_Client__c 
                          where  sofactoapp__Opportunit__c  in :recordIds
                          and sofactoapp__Etat__c  = 'Brouillon' and  sofactoapp__Opportunit__c != null ] ) {
            	
            	 Fact.sofactoapp__Date_de_facture__c = system.today(); 
                 Fact.sofactoapp__Ech_ance__c = system.today(); 
                 Fact.Beneficiaire_Prime__c= Fact.sofacto_Devis__r.Salarie_primable__c;  
                 Fact.sofactoapp__emetteur_facture__c= Fact.sofacto_Devis__r.Agency__r.Sofactoapp_Raison_Social__c;
                 Fact.sofactoapp_Ordre_d_ex_cution__c= Fact.sofacto_Devis__r.Ordre_d_execution__c;
                 Fact.sofactoapp__Compte2__c= Fact.sofacto_Devis__r.Nom_du_payeur__c;           
            	 Fact.sofactoapp__Etat__c ='Emise';
                 Factures.add(Fact);
        	}
            try{
            System.debug('### in upd');
            update Factures;
        	}catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
        	}
        
    }
}