/**
 * @File Name          : BAT10_UpdateInventaireSA_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 02/04/2020, 15:12:50
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/04/2020   ZJO     Initial Version
**/
@isTest
public with sharing class BAT10_UpdateInventaireSA_TEST {
    static User adminUser;
    static List<Account> lstAccounts;
    static List<sofactoapp__Raison_Sociale__c> lstSofactos;
    static List<ServiceTerritory> lstAgences;
    static Set<Id> setAgenceId = new Set<Id>();
    static List<Logement__c> lstLogements;
    static List<Asset> lstAssets;
    static List<Case> lstCases;
    static List<WorkOrder> lstWorkOrders;
    static List<ServiceAppointment> lstSAs;
   
    static{
        adminUser = TestFactory.createAdminUser('batch3@test.com', TestFactory.getProfileAdminId());
        insert adminUser;
        
        System.runAs(adminUser){

            //List of Accounts
            lstAccounts = new List<Account>{
                TestFactory.createAccount('Test Acc 1'),
                TestFactory.createAccount('Test Acc 2'),
                TestFactory.createAccount('Test Acc 3')
            };
            insert lstAccounts;

             //List of Sofactos
             lstSofactos = new List<sofactoapp__Raison_Sociale__c>{
                new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1234',
                  sofactoapp__Invoice_prefix__c = '2134'
                ),
                new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1342',
                  sofactoapp__Invoice_prefix__c = '2431'
                ),
                new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1432',
                  sofactoapp__Invoice_prefix__c = '2341'
                )
             };
 
             insert lstSofactos;
 
             //Create Operating Hour
             OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
             insert OprtHour;
 
             //List of agences
             lstAgences = new List<ServiceTerritory>{
                 new ServiceTerritory(
                     Name = 'test agence 1',
                     Agency_Code__c = '0001',
                     IsActive = True,
                     Inventaire_en_cours__c = false,
                     OperatingHoursId = OprtHour.Id,
                     Sofactoapp_Raison_Social__c = lstSofactos[0].Id
                 ),
                 new ServiceTerritory(
                     Name = 'test agence 2',
                     Agency_Code__c = '0002',
                     IsActive = True,
                     Inventaire_en_cours__c = false,
                     OperatingHoursId = OprtHour.Id,
                     Sofactoapp_Raison_Social__c = lstSofactos[1].Id
                 ),
                 new ServiceTerritory(
                     Name = 'test agence 3',
                     Agency_Code__c = '0003',
                     IsActive = True,
                     Inventaire_en_cours__c = false,
                     OperatingHoursId = OprtHour.Id,
                     Sofactoapp_Raison_Social__c = lstSofactos[2].Id
                 )
             };
 
             insert lstAgences;
 
             //Populate Set Agence Id
             for (ServiceTerritory agence : lstAgences){
                 setAgenceId.add(agence.Id);
             }

             //List of Logements
             lstLogements = new List<Logement__c>{
                  TestFactory.createLogement(lstAccounts[0].Id, lstAgences[0].Id),
                  TestFactory.createLogement(lstAccounts[1].Id, lstAgences[1].Id),
                  TestFactory.createLogement(lstAccounts[2].Id, lstAgences[2].Id)
             };

             insert lstLogements;

             //List of Assets
             lstAssets = new List<Asset>{
                new Asset (
                    Name = 'equipment 1',
                    Status = AP_Constant.assetStatusActif,
                    Logement__c = lstLogements[0].Id,
                    AccountId = lstAccounts[0].Id
                ),
                new Asset (
                    Name = 'equipment 2',
                    Status = AP_Constant.assetStatusActif,
                    Logement__c = lstLogements[1].Id,
                    AccountId = lstAccounts[1].Id
                ),
                new Asset (
                    Name = 'equipment 3',
                    Status = AP_Constant.assetStatusActif,
                    Logement__c = lstLogements[2].Id,
                    AccountId = lstAccounts[2].Id
                )   
             };

             insert lstAssets;

             //List of Cases
             lstCases = new List<Case>{
                 TestFactory.createCase(lstAccounts[0].Id, 'A1 - Logement', lstAssets[0].Id),
                 TestFactory.createCase(lstAccounts[0].Id, 'A1 - Logement', lstAssets[0].Id),
                 TestFactory.createCase(lstAccounts[0].Id, 'A1 - Logement', lstAssets[0].Id)
             };

             insert lstCases;

             //List of WorkOrders
            lstWorkOrders = new List<WorkOrder>{
                 new WorkOrder(
                    caseId = lstCases[0].Id,
                    Status = AP_Constant.wrkOrderStatusNouveau,
                    Priority = AP_Constant.wrkOrderStatusInProgress
                 ),
                 new WorkOrder(
                    caseId = lstCases[1].Id,
                    Status = AP_Constant.wrkOrderStatusNouveau,
                    Priority = AP_Constant.wrkOrderStatusInProgress
                 ),
                 new WorkOrder(
                    caseId = lstCases[2].Id,
                    Status = AP_Constant.StatusClosed,
                    Priority = AP_Constant.wrkOrderStatusInProgress
                 )
            };

            insert lstWorkOrders;

            //List of Service Appointments
            lstSAs = new List<ServiceAppointment>{
                new ServiceAppointment(
                    Status = 'In Progress',
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(5),
                    ParentRecordId = lstWorkOrders[0].Id,
                    Work_Order__c = lstWorkOrders[0].Id,
                    ServiceTerritoryId = lstAgences[0].Id,
                    Inventaire_en_cours__c = True
                ),
                new ServiceAppointment(
                    Status = 'In Progress',
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(10),
                    ParentRecordId = lstWorkOrders[1].Id,
                    Work_Order__c = lstWorkOrders[1].Id,
                    ServiceTerritoryId = lstAgences[1].Id,
                    Inventaire_en_cours__c = True
                ),
                new ServiceAppointment(
                    Status = 'In Progress',
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(30),
                    ParentRecordId = lstWorkOrders[2].Id,
                    Work_Order__c = lstWorkOrders[2].Id,
                    ServiceTerritoryId = lstAgences[2].Id,
                    Inventaire_en_cours__c = True
                )
            };

            insert lstSAs;
        }
    }

    @isTest
    public static void testBatch3(){
        System.runAs(adminUser){
            Test.startTest();
                BAT10_UpdateInventaireSA batch = new BAT10_UpdateInventaireSA(setAgenceId);
                Database.executeBatch(batch);
            Test.stopTest();  

        List<ServiceAppointment> lstServiceAppointments = [SELECT Id, Inventaire_en_cours__c FROM ServiceAppointment WHERE ServiceTerritoryId IN :setAgenceId];
        system.assertEquals(lstServiceAppointments[0].Inventaire_en_cours__c, False);
        }  
    }
}