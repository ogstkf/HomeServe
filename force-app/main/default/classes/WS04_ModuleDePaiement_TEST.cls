@isTest
private class WS04_ModuleDePaiement_TEST {

	static User mainUser;
	static List<Account> lstAccount = new List<Account>();
	static List<Contract> lstContract = new List<Contract>();

	static {

		mainUser = TestFactory.createAdminUser('WS04_ModuleDePaiement@test.COM', TestFactory.getProfileAdminId());
		//[select id from user where Name='Module de Paiement IPN Site Guest User'];
		//
		insert mainUser;

		System.runAs(mainUser){
			CreateSignature__c cs = new CreateSignature__c();
	        cs.Name='SecretKey';
	       	cs.SecretKeyValue__c = 'test';
	     	insert cs;

	     	lstAccount.add(TestFactory.createAccount('test'));
        	insert lstAccount;

        	lstContract.add(TestFactory.createContract('test', lstAccount[0].Id));
			insert lstContract;
		}
	}

	@IsTest static void testInsertPayment(){
		System.runAs(mainUser){
			Test.startTest();
				Map<String, String> mapData = new Map<String, String>();

				Restrequest req = new Restrequest();
	            req.requestURI = 'https://dev-cham.cs106.force.com/sf/services/apexrest/WS04_ModuleDePaiement_V2?vads_amount=j&vads_auth_mode=k&vads_auth_number=l&vads_auth_result=m&vads_capture_delay=n&vads_card_brand=o&vads_card_number=p&vads_payment_certificate=q&vads_ctx_mode=r&vads_currency=s&vads_effective_amount=t&vads_site_id=xxxx&vads_trans_date=a&vads_trans_id=b&vads_validation_mode=c&vads_version=d&vads_warranty_result=e&vads_payment_src=f&vads_sequence_number=25&vads_contract_used=h&vads_trans_status=i&vads_expiry_month=a&vads_expiry_year=b&vads_bank_code=c&vads_bank_product=d&vads_pays_ip=e&vads_presentation_date=f&vads_effective_creation_date=g&vads_operation_type=h&vads_threeds_enrolled=i&vads_threeds_cavv=g&vads_threeds_eci=h&vads_threeds_xid=i&vads_threeds_cavvAlgorithm=g&vads_threeds_status=h&vads_threeds_sign_valid=i&vads_threeds_error_code=g&vads_threeds_exit_status=h&vads_trans_uuid=i&vads_risk_control=g&vads_result=h&vads_extra_result=i&vads_card_country=h&vads_language=i&vads_hash=h&vads_url_check_src=i&vads_action_mode=kkkkkkkkkkkkkkkkk&vads_payment_config=i&vads_page_action=h&vads_product_labelN=yyyy&vads_product_refN=yyyy&vads_cust_email=yyyy&vads_cust_id=yyyy&vads_cust_title=yyyy&vads_cust_status=yyyy&vads_cust_first_name=yyyy&vads_cust_last_name=yyyy&vads_cust_country=yyyy&signature=y92NJI6ADkMRuPRAv1BpYHX23XK19oXePd9sIuSpVck=';
	           	req.params.put('vads_amount', '11');
				req.params.put('vads_auth_result', '11');
				req.params.put('vads_bank_code', '11');
				req.params.put('vads_bank_product', '11');
				req.params.put('vads_capture_delay', '11');
				req.params.put('vads_card_brand', '11');
				req.params.put('vads_card_country', '11');
				req.params.put('vads_ctx_mode', '11');
				req.params.put('vads_currency', '11');
				req.params.put('vads_order_id', '11');
				req.params.put('vads_payment_config', '11');
				req.params.put('vads_sequence_number', '11');
				req.params.put('vads_threeds_enrolled', '11');
				req.params.put('vads_threeds_status', '11');
				req.params.put('vads_trans_date', '11');
				req.params.put('vads_trans_status', '11');
				req.params.put('vads_url_check_src', '11');
				req.params.put('vads_version', '11');
				req.params.put('vads_site_id', '11');
				req.params.put('vads_page_action', '11');
				req.params.put('vads_action_mode', '11');
				req.params.put('vads_product_labelN', '11');
				req.params.put('vads_product_refN', '11');
				req.params.put('vads_cust_email', '11');
				req.params.put('vads_cust_id', '11');
				req.params.put('vads_cust_title', '11');
				req.params.put('vads_cust_status', '11');
				req.params.put('vads_cust_first_name', '11');
				req.params.put('vads_cust_last_name', '11');
				req.params.put('vads_cust_country', '11');
				req.params.put('vads_trans_id', '11');
				req.params.put('signature', AP06_CreateSignature.calculateSignature(mapData));
	            system.debug('****requestURI: ' + (String)req.requestURI);
	            RestContext.request = req;

	            //AP06_CreateSignature.calculateSignature(req.params);
				//String sig = AP06_CreateSignature.calculateSignature(req.params);
				//WS04_ModuleDePaiement.mapTestData = req.params;
				WS04_ModuleDePaiement.savePayment();

			Test.stopTest();
		}
	}

	@IsTest static void testUpdatePayment(){
		System.runAs(mainUser){

		Payment__c paEx = new Payment__c();
		Insert paEx;

			Test.startTest();
				Map<String, String> mapData = new Map<String, String>();

				Restrequest req = new Restrequest();
	            req.requestURI = 'https://dev-cham.cs106.force.com/sf/services/apexrest/WS04_ModuleDePaiement_V2?vads_amount=j&vads_auth_mode=k&vads_auth_number=l&vads_auth_result=m&vads_capture_delay=n&vads_card_brand=o&vads_card_number=p&vads_payment_certificate=q&vads_ctx_mode=r&vads_currency=s&vads_effective_amount=t&vads_site_id=xxxx&vads_trans_date=a&vads_trans_id=b&vads_validation_mode=c&vads_version=d&vads_warranty_result=e&vads_payment_src=f&vads_sequence_number=25&vads_contract_used=h&vads_trans_status=i&vads_expiry_month=a&vads_expiry_year=b&vads_bank_code=c&vads_bank_product=d&vads_pays_ip=e&vads_presentation_date=f&vads_effective_creation_date=g&vads_operation_type=h&vads_threeds_enrolled=i&vads_threeds_cavv=g&vads_threeds_eci=h&vads_threeds_xid=i&vads_threeds_cavvAlgorithm=g&vads_threeds_status=h&vads_threeds_sign_valid=i&vads_threeds_error_code=g&vads_threeds_exit_status=h&vads_trans_uuid=i&vads_risk_control=g&vads_result=h&vads_extra_result=i&vads_card_country=h&vads_language=i&vads_hash=h&vads_url_check_src=i&vads_action_mode=kkkkkkkkkkkkkkkkk&vads_payment_config=i&vads_page_action=h&vads_product_labelN=yyyy&vads_product_refN=yyyy&vads_cust_email=yyyy&vads_cust_id=yyyy&vads_cust_title=yyyy&vads_cust_status=yyyy&vads_cust_first_name=yyyy&vads_cust_last_name=yyyy&vads_cust_country=yyyy&signature=y92NJI6ADkMRuPRAv1BpYHX23XK19oXePd9sIuSpVck=';
	           	req.params.put('vads_amount', '11');
				req.params.put('vads_auth_result', '11');
				req.params.put('vads_bank_code', '11');
				req.params.put('vads_bank_product', '11');
				req.params.put('vads_capture_delay', '11');
				req.params.put('vads_card_brand', '11');
				req.params.put('vads_card_country', '11');
				req.params.put('vads_ctx_mode', '11');
				req.params.put('vads_currency', '11');
				req.params.put('vads_order_id', lstContract[0].ContractNumber);
				req.params.put('vads_payment_config', '11');
				req.params.put('vads_sequence_number', '11');
				req.params.put('vads_threeds_enrolled', '11');
				req.params.put('vads_threeds_status', '11');
				req.params.put('vads_trans_date', '11');
				req.params.put('vads_trans_status', '11');
				req.params.put('vads_url_check_src', '11');
				req.params.put('vads_version', '11');
				req.params.put('vads_site_id', '11');
				req.params.put('vads_page_action', '11');
				req.params.put('vads_action_mode', '11');
				req.params.put('vads_product_labelN', '11');
				req.params.put('vads_product_refN', '11');
				req.params.put('vads_cust_email', '11');
				req.params.put('vads_cust_id', lstAccount[0].ClientNumber__c);
				req.params.put('vads_cust_title', '11');
				req.params.put('vads_cust_status', '11');
				req.params.put('vads_cust_first_name', '11');
				req.params.put('vads_cust_last_name', '11');
				req.params.put('vads_cust_country', '11');
				req.params.put('vads_trans_id', paEx.Payment_ID__c);
				req.params.put('signature', AP06_CreateSignature.calculateSignature(mapData));
	            system.debug('****requestURI: ' + (String)req.requestURI);
	            RestContext.request = req;

	            //AP06_CreateSignature.calculateSignature(req.params);
				//String sig = AP06_CreateSignature.calculateSignature(req.params);
				//WS04_ModuleDePaiement.mapTestData = req.params;
				WS04_ModuleDePaiement.savePayment();

			Test.stopTest();
		}
	}
}