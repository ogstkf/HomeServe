/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 08-26-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-26-2020   ZJO   Initial Version
**/
public with sharing class VFC72_MassAvisDeRenouvellement {
    public ApexPages.StandardSetController controller {get; set;}
    public List<ServiceContract> lstSelected {get; set;}
    public String lstId {get; set;}
    public Boolean isEmpty {get; set;}

    public VFC72_MassAvisDeRenouvellement(ApexPages.StandardSetController controller){
        this.controller = controller;
        lstSelected = controller.getSelected();
        lstId = serializeIds();
        isEmpty = controller.getSelected().isEmpty(); 
    }

    public String serializeIds(){
        List<String> lstServConIds = new List<String>();

        System.debug('lstSelected' + lstSelected);
        for(ServiceContract servCon : lstSelected){
            lstServConIds.add(servCon.Id);
        }
        System.debug('lstServConIds' + lstServConIds);
        return JSON.serialize(lstServConIds);
    }

    @RemoteAction
    public static string openPDF(List<String> lstServConIds){

        List<ContentDocumentLink> lstCDLId = new List<ContentDocumentLink>();
        List<ContentVersion> lstConVer = new List<ContentVersion>();
        Map<String, String> mapConVerToAgency = new Map<String, String>();
        Map<String, String> mapConverToScId = new Map<String, String>();
        Map<String, String> mapCWnameToId = new Map<String, String>();
        string userAlias = [SELECT Id, Alias FROM USER WHERE Id = :UserInfo.getUserId() LIMIT 1].Alias;

        string result = '';
        Savepoint sp = Database.setSavepoint();
        try{

            List<ServiceContract> lstServCon = [SELECT Id, TECH_Date_d_edition__c, Agency__r.Name FROM ServiceContract WHERE Id IN:lstServConIds];

            if(lstServCon.size() > 0){
                for(ServiceContract sc : lstServCon){
                
                    PageReference AvisDeRenTemplate = new PageReference('/apex/VFP72_ServiceContract');
                    AvisDeRenTemplate.getParameters().put('lstIds', '['+JSON.serialize(sc.Id)+']');
                    Blob pdfDoc = Blob.valueOf('t');
                    if(!Test.isRunningTest()) pdfDoc = AvisDeRenTemplate.getContentAsPDF();
    
                    ContentVersion conVer = new ContentVersion();
                    conVer.ContentLocation = 'S';
                    conVer.PathOnClient = generateName(userAlias);
                    conVer.Title = generateName(userAlias);
                    conVer.VersionData = pdfDoc;
                    mapConVerToAgency.put(conVer.Title, sc.Agency__r.Name);
                    mapConverToScId.put(conVer.Title, sc.Id);
                    lstConVer.add(conVer);
    
                    System.debug('*** nomAgence: ' + sc.Agency__r.Name);     
                    System.debug('*** Title: ' + conVer.Title);  
                }
            }

            String agenceName = mapConVerToAgency.size()>0 ? mapConVerToAgency.values()[0] : '';
            System.debug('*** agenceName: ' + agenceName);

            String folderName = 'Avis de renouvellement '+ agenceName;

            //PDF qui englobe tous les pdf unitaire
            PageReference combinedTemplate = new PageReference('/apex/VFP72_ServiceContract');
            combinedTemplate.getParameters().put('lstIds',  JSON.serialize(lstServConIds));
            Blob combinedPdf = Blob.valueOf('t');
            if(!Test.isRunningTest()) combinedPdf = combinedTemplate.getContentAsPDF();

            ContentVersion conVer1 = new ContentVersion();
            conVer1.ContentLocation = 'S';
            conVer1.PathOnClient = generateName(userAlias);
            conVer1.Title = generateName(userAlias);
            conVer1.VersionData = combinedPdf;  
            lstConVer.add(conVer1);

            if(lstConVer.size() > 0){
                insert lstConVer;
            }

            //Get Content Documents                               
            List<ContentVersion> docList = [SELECT Id, ContentDocumentId, Title FROM ContentVersion WHERE Id IN:lstConVer order by ContentSize desc];            
            System.debug('docList : ' + docList);

            if(docList.size()>0){
                ContentDocument conDoc = new ContentDocument(Id = docList[0].ContentDocumentId);
                System.debug('conDoc : ' + conDoc);
                Pagereference pdfResult = new ApexPages.StandardController(conDoc).view();
                result = pdfResult.getUrl();
            }

            for(ContentWorkspace cw: [SELECT Id, RootContentFolderId, Name FROM ContentWorkspace WHERE Name =:folderName OR Name LIKE '%Avis de renouvellement%']){ 
                mapCWnameToId.put(cw.Name,cw.Id);
            }
            
            //first element of docList is the consolidated pdf when more than 1 servcon selected
            for(ContentVersion cv : docList) {              
                ContentDocumentLink cdl = new ContentDocumentLink();                        
                cdl.ContentDocumentId =  cv.ContentDocumentId; 
                cdl.ShareType = 'I';
                cdl.Visibility = 'AllUsers';                
                cdl.LinkedEntityId = mapConverToScId.containsKey(cv.Title) ? mapConverToScId.get(cv.Title) : (mapCWnameToId.containsKey(folderName) ? mapCWnameToId.get(folderName) : mapCWnameToId.get('Avis de renouvellement'));      
                lstCDLId.add(cdl);
            }
            
            if (lstCDLId.size() > 0){
                upsert lstCDLId;
            }

            //Update TECH_Date_d_edition__c to today
            List<ServiceContract> lstServConToUpdate = new List<ServiceContract>();
            for(ServiceContract con : lstServCon){
                con.TECH_Date_d_edition__c = System.today();
                lstServConToUpdate.add(con);
            }

            if(lstServConToUpdate.size() > 0){
                Update lstServConToUpdate;
            }

        }catch(Exception e){
            Database.rollback(sp);
            System.debug('*** error occurred: '+e.getMessage()+'-'+e.getStackTraceString());
            throw new AP_Constant.CustomException(e.getMessage());
        }

        System.debug('pdf result:' + result);      
        return result;        
    }

    private static String generateName(String alias){ 
        system.debug('*** in generateName alias:  '+alias);        

        string randomUniqueIdentifier = randomizeStringVFC07('');
        system.debug('*** in generateName randomUniqueIdentifier:  '+randomUniqueIdentifier);

        String pdfName = '';
        DateTime datToday = DateTime.now();
        pdfName += datToday.day() < 10 ? '0'+datToday.day() : datToday.day().format();
        pdfName += datToday.month() < 10 ? '0'+datToday.month() : datToday.month().format();
        pdfName += datToday.year();
        pdfName += datToday.hour() < 10 ? '0'+datToday.hour() : datToday.hour().format();
        pdfName += datToday.minute() < 10 ? '0'+datToday.minute() : datToday.minute().format();
        pdfName += datToday.second() < 10 ? '0'+datToday.second() : datToday.second().format();
        pdfName += '-';
        pdfName += alias;
        pdfName += '-' + randomUniqueIdentifier;
        pdfName += '.pdf';
        return pdfName;
    }

    public static String randomizeStringVFC07(String name) {
        String charsForRandom = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < 6) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), charsForRandom.length());
            randStr += charsForRandom.substring(idx, idx + 1);
        }
        return name + randStr;
    }

}