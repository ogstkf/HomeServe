/**
 * @File Name          : ServiceTerritoryTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 31/03/2020, 14:25:22
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/03/2020   AMO     Initial Version
**/
public with sharing class ServiceTerritoryTriggerHandler {

    Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());

    public void handleAfterUpdate(List<ServiceTerritory> lstOldAgence, List<ServiceTerritory> lstNewAgence){
        System.debug('Service Territory Handle After Update');

        Set<Id> setAgenceId = new Set<Id>();
        
        Set<Id> setAgenceId2 = new Set<Id>();

        for(integer i=0;i<lstNewAgence.size();i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('BAT10')){
                if(lstNewAgence[i].Inventaire_en_cours__c == false && lstNewAgence[i].Inventaire_en_cours__c != lstOldAgence[i].Inventaire_en_cours__c){
                    setAgenceId.add(lstNewAgence[i].Id);
                }
            }
        }
        
        
        for(integer i=0;i<lstNewAgence.size();i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP69')){
                if(lstNewAgence[i].Inventaire_en_cours__c == true && lstNewAgence[i].Inventaire_en_cours__c != lstOldAgence[i].Inventaire_en_cours__c){
                    setAgenceId2.add(lstNewAgence[i].Id);
                }
            }
        }


        if(setAgenceId.size() > 0){
            AP69_FreezeProductTransfer.setPIQuantityFinInventaire(setAgenceId);
            BAT10_UpdateInventairePILocation bat10 = new BAT10_UpdateInventairePILocation(setAgenceId);
            Database.executeBatch(bat10,5);
        }
        
        
        if(setAgenceId2.size() > 0){
            AP69_FreezeProductTransfer.setPIQuantityInventaire(setAgenceId2);            
        }
        
        
    }
}