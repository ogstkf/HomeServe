/**
 * @description       : 
 * @author            : ANR
 * @group             : 
 * @last modified on  : 01-19-2021
 * @last modified by  : ANR
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   01-19-2021   ANR   Initial Version
**/
@isTest
public with sharing class AP77_SetPriceBookOnOrder_TEST {

    static User mainUser;
    static List<Account> lstAccount = new List<Account>();
    static List<ServiceTerritory> lstServ = new List<ServiceTerritory>();
    static Pricebook2  testPB1;
    static Pricebook2  testPB2;
    static List<Order> lstOrder;
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static OperatingHours opHrs = new OperatingHours();

    static{
        
        mainUser = TestFactory.createAdminUser('AP77@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            Account testAcc1;
            testAcc1 = TestFactory.createAccountBusiness('Test1');
            testAcc1.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();

            lstAccount.add(testAcc1);

            Account testAcc2;
            testAcc2 = TestFactory.createAccountBusiness('Test2');
            testAcc2.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();

            lstAccount.add(testAcc2);                        

            insert lstAccount;

            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;
            
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            //create service territory
            lstServ.add(TestFactory.createServiceTerritory('Zone sud1',opHrs.Id, raisonSocial.Id));
            lstServ.add(TestFactory.createServiceTerritory('Zone sud2',opHrs.Id, raisonSocial.Id));
            insert lstServ;

            testPB1 = new Pricebook2(Name = 'PriceBook1',Compte__c = testAcc1.Id , Agence__c = lstServ[0].Id,isActive = true);
            testPB1.recordtypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId();
            testPB2 = new Pricebook2(Name = 'PriceBook2',Compte__c = testAcc2.Id , Agence__c = lstServ[1].Id,isActive = true);
            testPB2.recordtypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId();
            List<Pricebook2> lstPri = new List<PriceBook2>{testPB1,testPB2};
            insert lstPri;    

             lstOrder = new List<Order> {
                new Order( AccountId = testAcc1.Id
                          ,Name = 'Test1'
                          ,Agence__c = lstServ[0].Id
                          ,EffectiveDate = System.today()
                          ,Status = 'Demande d\'achats' //To add to AP_Constants
                          ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId()),
                new Order( AccountId = testAcc2.Id
                          ,Name = 'Test2'
                          ,Agence__c = lstServ[1].Id
                          ,EffectiveDate = System.today()
                          ,Status = 'Demande d\'achats' //To add to AP_Constants
                          ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId())
            };     
        }

    }

    @isTest
    public static void testsetPriceBook(){
        System.runAs(mainUser){
            Test.startTest();
            insert lstOrder;
            Test.stopTest();

            List<Order> lstNewOrder = [SELECT Id,Pricebook2Id FROM Order];

            System.assertEquals(lstNewOrder[0].Pricebook2Id,testPB1.Id);
            System.assertEquals(lstNewOrder[0].Pricebook2Id,testPB1.Id);
        }
    }

}