/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 13-09-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   10-09-2021   MNA   Initial Version
**/
public with sharing class testAnonymous {
    public testAnonymous() {
        DateTime dt1 = System.now();
        List<ServiceContract> lstSc = new List<ServiceContract>();
        for (ServiceContract sc :[SELECT  Id, Name, Contract_Status__c, StartDate, Date_du_prochain_OE__c, Asset__c  FROM ServiceContract WHERE Id ='8100Q0000009VzOQAU']) {
            for (Integer i = 0 ; i < 200; i++) {
                ServiceContract scT = sc.clone(false, false, false, false);
                scT.Name += ' '+ (i +1);
                sct.Contract_Status__c = 'Active';
                lstSc.add(sct);
            }
        }
        insert lstSc;
        DateTime dt2 = System.now();
        System.debug(dt1.minute()+ ' mn ' + dt1.second() + ' s');
        System.debug(dt2.minute()+ ' mn ' + dt2.second() + ' s');
        System.debug('Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
        //8100Q0000009GmOQAU
    }
    public testAnonymous1() {
        DateTime dt1 = System.now();
        List<ServiceContract> lstSc = new List<ServiceContract>();
        for (ServiceContract sc :[SELECT  Id, Name, Contract_Status__c, StartDate, Date_du_prochain_OE__c, Asset__c  FROM ServiceContract WHERE Name LIKe '%Contrat Test AINA%'
                                                ORDER BY LastModifiedDate DESC LIMIT 200]) {
            //sc.Contract_Status__c = 'Actif - en retard de paiement';
            sc.Contract_Status__c = 'Active';
            lstSc.add(sc);
        }
        update lstSc;
        DateTime dt2 = System.now();
        System.debug(dt1.minute()+ ' mn ' + dt1.second() + ' s');
        System.debug(dt2.minute()+ ' mn ' + dt2.second() + ' s');
        System.debug('Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime());
    }
}
