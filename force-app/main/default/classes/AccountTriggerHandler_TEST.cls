@isTest
private class AccountTriggerHandler_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Test Class for AccountTriggerHandler
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 06-MAR-2019  DMU    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/

    static User mainUser;
    static list <Logement__c> listLogement;

    static {
        mainUser = TestFactory.createAdminUser('AP01_BlueWSCallout_TEST@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            listLogement = new list <Logement__c>{TestFactory.createLogement('lo_001', 'test.test1@sc-mauritius.com', 'lo_L1_001'), 
                                                    TestFactory.createLogement('lo_002', 'test.test2@sc-mauritius.com', 'lo_L2_002')

            };          
        }

    }

    static testMethod void checkUpdatedLogement(){   
        
        system.runAs(mainUser){  
            Test.startTest();
                insert listLogement;               
            Test.stopTest();
            list <Account> acclist = [select id from Account where TECH_LogementId__c != null];
            System.assertEquals(0, acclist.size());
            list<Logement__c> updatedlistLo =  [select id, Owner__c from Logement__c where id in: listLogement];
            system.debug('ANY = ' +updatedlistLo[0].Owner__c);
            system.assertNotEquals(null, updatedlistLo[0].Owner__c);
            system.assertNotEquals(null, updatedlistLo[1].Owner__c);
        }  
    }
}