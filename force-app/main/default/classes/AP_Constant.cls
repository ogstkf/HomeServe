/**
 * @File Name          : AP_Constant.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 09-04-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    20/06/2019, 10:50:14   RRJ     Initial Version
 * 1.1    01/08/2019, 16:50:14   ARA     added picklist values for equipment type for work type  
 * 1.2    07/10/2019, 15:26:37   ANA     added picklist values for Service Appointment Status
**/
public with sharing class AP_Constant {

    //get recordTypeId
    public static Id getRecordTypeId(String objectType, String RecordTypeLabel){
        System.Debug('### getContactRecordType' + UserInfo.getUserName());        
        SObject obj; 
        
        // Describing Schema  
        Schema.SObjectType res = Schema.getGlobalDescribe().get(ObjectType);
        
        if (res != null){  
            obj = res.newSObject();
            
            // Describing Object 
            Schema.DescribeSObjectResult DesRes = obj.getSObjectType().getDescribe();
            if (DesRes != null){
                Map<string,schema.recordtypeinfo> recordTypeMap = DesRes.getRecordTypeInfosByName();

                if (RecordTypeMap != null) {
                    Schema.RecordTypeInfo RecordTypeRes = RecordTypeMap.get(RecordTypeLabel);

                    if (RecordTypeRes != null){
                        return RecordTypeRes.getRecordTypeId();
                    }
                }
            }
        }
        
        return null;  
    }

    public static Id getRecTypeId(String objName, String recTypeDevName){
        return Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfosByDeveloperName().get(recTypeDevName).getRecordTypeId();
    }

    //RRJ 20190730
    public class CustomException extends Exception {}
    
    public static String leadVisitStatusDoneOk = 'Done and OK';
    public static String leadVisitStatusDoneFailed = 'Done and failed';
    public static String leadVisitStatusNeedToBePlanned = 'Need to be replanned';

    public static String leadLeadSourceBlue = 'Site BLUE';

    public static String ws_Authorization = 'Authorization';
    public static String ws_ContentType = 'Content-Type';
    public static String ws_XAuthToken = 'X-Auth-Token';
    public static String ws_CacheControl = 'Cache-Control';
    public static String ws_Accept = 'Accept';
    public static String ws_AcceptValue = '*/*';
    public static String ws_Username = 'username';
    public static String ws_Password = 'password';
    public static String ws_Token = 'token';

    //Webservice Constants
    public static String ws_Error = 'Error';
    public static String ws_Status = 'Status';
    public static String ws_Message = 'Message';
    public static String ws_URL = 'URL';


    public static String ws_Blue_State = 'state';

    //Lead
    public static String leadStatusNew = 'New';

    //ANA START  Case Type picklist values

    public static String caseTypeInstallation='Installation';
    public static String caseTypeMaintenance='Maintenance';

    //CASE ORIGIN

    public static String caseOriginChamDigital='Cham Digital';

    //Case Stats
    public static String caseStatusAttenteCham='En attente de Cham';
    public static String caseStatusInProgress='In Progress';


    //picklist Values Object: ServiceAppointment, Field: Category__c
    public static String ServAppCategoryMisEnServ = 'Mise en service';
    public static String ServAppCategoryVisiteEntr = 'Visite d_entretien';
    public static String ServAppCategoryRemDappa = 'Remplacement dappareil';
    public static String ServAppCategoryDepannage = 'Depannage';
    public static String ServAppCategoryVisiteConf = 'Premiere_visite';
    public static String ServAppCategoryRamonage = 'Ramonage';
    public static String ServAppCategoryControle = 'Controle';
    public static String ServAppCategoryDivers = 'Divers';
    public static String ServAppCategoryDevis = 'Devis';
    public static String ServAppCategoryPose = 'Pose';
    public static String ServAppCategoryVisiteForfaitaire = 'Visite forfaitaire';
    public static String ServAppCategoryVisiteTec = 'Visite technique';
    public static String ServAppCategoryVEInd = 'VE Individuelle';
    

    //Mapping ServiceAppointment.Category__c code
    public static String ServAppCategoryMES = 'MES';
    public static String ServAppCategoryVEN = 'VEN';
    public static String ServAppCategoryREM = 'REM';
    public static String ServAppCategoryDEP = 'DEP';
    public static String ServAppCategoryVIC = 'VIC';
    public static String ServAppCategoryRAM = 'RAM';
    public static String ServAppCategoryCTL = 'CTL';
    public static String ServAppCategoryDIV = 'DIV';
    public static String ServAppCategoryDEV = 'DEV';
    public static String ServAppCategoryVFO = 'VFO';
    public static String ServAppCategoryVIT = 'VIT';


    //These variables are used in AP05_WSCalculateTVA
    public static string qu1_autr = 'Autres';
    public static string qu1_plus2ans = 'Particulier logement de PLUS de 2 ans';
    public static string qu1_moins2ans = 'Particulier logement moins de 2 ans';
    public static string qu2_oui = 'Oui';
    public static string qu2_non = 'Non';
    public static string errorMsg = 'Merci de bien vouloir sélectionner votre profil et indiquer si votre équipement est un équipement à Haute Performance Énergétique pour passer à l\'étape suivante';  

    //ServiceAppointment values
    public static string servAppStatusNone = 'None';
    public static string servAppStatusPlanned = 'Planned';
    public static String servAppCatVisitEntretien = 'Visite d_entretien';
    public static String servAppStatusCancelled = 'Cancelled';
    public static String servAppStatCatCancelled = 'Cancelled';
    public static String servAppStatCatCompleted = 'Completed';
    public static String servAppStatusScheduled = 'Scheduled';

    //ANA 07/10/2019 Service Appointment Status Picklist values START
    public static String servAppStatusImpoToFinish = 'Impossible to finish';
    public static String servAppStatusEfectueConc = 'Done OK';
    public static String servAppStatusTravauxAprevoir='Client works to be planned';
    public static String servAppStatusClientAbsent='Done client absent';
    public static String servAppStatusChaudiereGaz='Premier entretien Chaudière gaz';
    public static String servAppStatusImposFinish = 'Impossible to finish';	
    public static String servAppStatusWorkPlan = 'Client works to be planned';

    //SBH 22/10/2019 Added picklist values
    public static String servAppStatusInProgress = 'In Progress';
    public static String servAppStatusEnAttenteClient = 'En attente appel client';
    public static String servAppStatusDispatched = 'Dispatched';
    public static String servAppStatusOnHold = 'On Hold';

    //ANA  07/10/2019 Service Appointment Status Picklist values END
    //SBH: Added status for AP27
    //work order values
    public static String wrkOrderStatusNouveau = 'New';
    public static String wrkOrderPriorityNormal = 'Normale';
    public static String wrkOrderStatusInProgress = 'In Progress';


    //work type values
    public static String wrkTypeTypeMaintenance = 'Maintenance';
    // START ARA added picklist values for Equipment_type__c on Object work type 
    public static String wrkTypeEquipmentTypeChaudieregaz = 'Chaudière gaz';
    public static String wrkTypeEquipmentTypeChaudierefioul = 'Chaudière fioul';
    public static String wrkTypeEquipmentTypePompeachaleur = 'Pompe à chaleur hybride Fioul';

    //asset values
    public static String assetStatusActif = 'Actif';
    public static String equipTypeChaudiereGaz = 'Chaudière gaz';
    public static String equipTypePompechaleurAirEau = 'Pompe à chaleur air/eau';

    //account values
    public static String accSourceSiteMyChauffage = 'Site MyChauffage';
    public static String accStatusEnAttenteVisit = 'En attente de visite';

    //RRJ 20190802 AP_constant FROM PROD

    //Lead RecordType
    public static String leadRTPisteBlue = 'Pistes_BLUE';

    public static String serviceAppointmentStatusDoneOK = 'Done OK';
    public static String serviceAppointmentStatusDoneKO = 'Done KO';

    //Opportunity Variable
    public static String oppStageQua = 'Qualification';
    public static String oppStageProposal = 'Proposal';
    public static String oppStageDevisValideEnAttenteInter = 'Devis validé - en attente d\'intervention';    
    public static String oppStageClosedWon = 'Closed Won';
    public static String oppStageClosedLost = 'Closed Lost';
    public static String oppStageExpired = 'Expired';
    public static String oppStageNegotiation = 'Negotiation';
    public static String oppStageOpen = 'Open';
    public static String oppStageAutreOptionRetenue = 'Autre option retenue';

    //Account picklist Values:
    public static String accAccountSourceSiteBlue = 'Site BLUE';

    //Status values
    public static String StatusClosed = 'Closed';
    public static String ServiceResourceLevelJunior = 'Junior';
  
    //RRJ 20190802 END

    //ANA START 20191015 Shipment Status
    public static String ShipStatusLivre = 'Livré';
    public static String ShipSatisfactionElevee = 'Elevee';
    public static String ShipSatisfactionMoyenne = 'Moyenne';
    public static String ShipSatisfactionFaible = 'Faible';

    //Shipment record type
    public static String RTReceptionFournisseur = 'Reception Fournisseur';

    //ANA 20191015 END

    //LocationType variables
    public static String virtuallocation = 'Virtual';

    //Order variables
    public static String OrdStatusAchat ='Demande d\'achats';
    public static String OrdStatusAchatApprove = 'Demande_d\'achats_approuvée';
    public static String OrdStatusAccuseReceptionInterne = 'Accusé de réception interne';
    public static String ordStatusCommandeEmise = 'Accusé de réception fournisseurs';
    public static String OrdStatusAccuseReception = 'Accusé de Réception';
    public static String ordStatusEnCoursLivraison= 'En_cours_de_livraison';
    public static String OrdStatusSolde = 'Soldée';
    public static String ordStatusReceptPartiellement = 'Receptionne partiellement';
    public static String ordStatusAnnule= 'Annulé';
    public static String ordRtCommandeFournisseur = 'Commandes_fournisseurs';

    //Order rt names
    public static String orderRtCommandeFournisseur = 'Commandes_fournisseurs';

    //quote variables
    public static String quoteStatusValideSigne = 'Validé, signé - en attente d\'intervention';
    public static String quoteStatusDraft = 'Draft';
    public static String quoteStatusInReview = 'In Review';
    public static String quoteStatusValideSigneTermine = 'Validé, signé et terminé';
    public static String quoteStatusValideSigneAbandonne = 'Validé,signé mais abandonné';
    public static String quoteStatusExpired = 'Expired';
    public static String quoteStatusDenied = 'Denied';
    public static String quoteStatusNeedsReview = 'Needs Review';
    public static String quoteStatusAutreOptionRetenue = 'Autre option retenue';

    //product request variables : 
    public static String productRequestStatusBrouillon = 'Brouillon';
    public static String productRquestTypeDemandeAchat = 'demande_achats';

    //product request line item variables: 
    public static String prliStatusEnCourDeDemande = 'En cours de commande';
    public static String prliReserveAPreparer = 'Réservé à préparer';
    public static String prliStatusACommander = 'A commander';

    // product variables
    public static String productFamilleArticleAccessoir = 'Accessoires';
    public static String productFamilleAppareils = 'Appareils';
    public static String productFamilleConsommables = 'Consommables';
    public static String productFamilleOutillage = 'Outillage';
    public static String productFamillePieceDetache = 'Pièces détachées';

    public static String productCodeCouvMoGarCham = 'COUV-MO-GAR-CHAM';
    public static String productCodeDeplZone = 'DEPL-ZONE';
    public static String productCodeCouvDeplGarCham = 'COUV-DEPL-GAR-CHAM';

    public static String productStatutApprouvee = 'Approuvée';
    
    //conditions variables 
    public static String conditionRtLigne = 'Ligne';
    
    // Règlement variables
    public static String reglementStatutPaiementEncaisse = 'Encaissé';
    public static String reglementStatutPaiementCollecte = 'Collecté';

    //Service Contract variables
    // Contract_Status__c
    public static String serviceContractStatutContratAttenteRen = 'En attente de renouvellement';
    public static String serviceContractStatutContratActive = 'Active';
    public static String serviceContractStatutContratPendingPayment = 'Pending Payment';
    public static String  serviceContractStatutContratResilie = 'Résilié';
    public static String  serviceContractStatutContratExpired = 'Expired';
    public static String  serviceContractStatutEnAttentePremiereVisitValid = 'Pending first visit';
    public static String  serviceContractStatutActifenretarddepaiement = 'Actif - en retard de paiement';
    public static String  serviceContractStatutResilieenretarddepaiement = 'Résiliation pour retard de paiement';
    public static String  serviceContractStatutCancelled = 'Cancelled';
    public static String serviceContractStatutEnAttente = 'Pending Client Validation';
    
    //Type__c
    public static String  serviceContractTypeInd = 'Individual';
    public static String  serviceContractTypeCollective = 'Collective';

    //ProductRequestLineItem variables
    public static String PRLIStatutReservePrep = 'Réservé à préparer';
    public static String PRLIStatutCommande = 'A commander';
    public static String PRLIStatutEnCourCommande = 'En cours de commande';

    //contract line item variables: 
    public static String cliStatusComplete = 'Complete';


}