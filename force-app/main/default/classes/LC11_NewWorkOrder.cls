/**
 * @File Name          : LC11_NewWorkOrder.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/10/2020, 12:24:58 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/10/2019   AMO     Initial Version
 * 2.0	  17/12/2019   SH	   Split the code with if statement to check if WO already exist(s), then take info from the WO
**/
public with sharing class LC11_NewWorkOrder {
    @AuraEnabled
    public static Object fetchDefaultValue(String caseId){
        Id CsID = (Id)caseId;
        Map<String, String> mapCase = new Map<String, String>();

        // 2019-12-17 - SH - BEGIN Old Code 1.0
        // Case cas = [SELECT Id, Service_Contract__c, AssetId, Local_Agency__c, AccountId FROM Case WHERE Id = :CsID LIMIT 1];
        // mapCase.put('ServiceContractId', cas.Service_Contract__c);
        // mapCase.put('AssetId', cas.AssetId);
        // mapCase.put('ServiceTerritoryId', cas.Local_Agency__c);
        // mapCase.put('CaseId', cas.Id);
        // mapCase.put('AccountId', cas.AccountId);

        // // mapCase.put('')
        // // mapLgmnt.put('AccountId', lgmnt.Account__c);
        // // mapLgmnt.put('Logement__c', lgmnt.Id);
        // System.debug ('Map Case ' + mapCase);
        // 2019-12-17 - SH - END Old Code 1.0

        // Check if there is already a WorkOrder for this case.
        List<WorkOrder> listWOs = [
            SELECT Id, ServiceContractId, AssetId, ServiceTerritoryId, CaseId, AccountId, ContactId, AgencySubSector__c, Type__c, 
                   Reason__c, WorkTypeId, Street, City, State, PostalCode, Country, Campagne__c, Account.Campagne__c
            FROM WorkOrder 
            WHERE CaseId = :CsID
            ORDER BY EndDate DESC NULLS FIRST, CreatedDate DESC
            LIMIT 1];

        if (listWOs.isEmpty()) {
            // Get info from current CASE
            Case cas = [
                SELECT Id, Service_Contract__c, AssetId, Local_Agency__c, AccountId, ContactId, Type, Reason__c,
                       Asset.Product2.Equipment_type1__c, Asset.Logement__r.Street__c, Asset.Logement__r.Adress_complement__c,
                       Asset.Logement__r.Postal_Code__c, AgencySubSector__c, Asset.Logement__r.City__c, Asset.Logement__r.Country__c, Campagne__c
                FROM Case 
                WHERE Id = :CsID 
                LIMIT 1];

            // Get the corresponding WorkType
            Set<String> types = new Set<String>();
            Set<String> reasons = new Set<String>();
            Set<String> equipementTypes = new Set<String>();
            types.add(cas.Type);
            reasons.add(cas.Reason__c);
            equipementTypes.add(cas.Asset.Product2.Equipment_type1__c);
            Map<String,Map<String,Map<String, WorkType>>> workTypeFull = FSL_WorkOrderHandler.buildWorkTypesMap(types,reasons,equipementTypes);

            // if(workTypeFull.containsKey(cas.Type)) {
            //     if(workTypeFull.get(cas.Type).containsKey(cas.Reason__c)) {
            //         if(workTypeFull.get(cas.Type).get(cas.Reason__c).containsKey(cas.Asset.Product2.Equipment_type1__c)) {
            //             mapCase.put('WorkTypeId', workTypeFull.get(cas.Type).get(cas.Reason__c).get(cas.Asset.Product2.Equipment_type1__c).Id);
            //         }
            //     }
            // }

            mapCase.put('ServiceContractId', cas.Service_Contract__c);
            mapCase.put('AssetId', cas.AssetId);
            mapCase.put('Campagne__c', cas.Campagne__c);
            mapCase.put('ServiceTerritoryId', cas.Local_Agency__c);
            mapCase.put('CaseId', cas.Id);
            mapCase.put('AccountId', cas.AccountId);
            mapCase.put('ContactId', cas.ContactId);
            mapCase.put('AgencySubSector__c', cas.AgencySubSector__c);
            mapCase.put('Type__c', cas.Type);
            mapCase.put('Reason__c', cas.Reason__c);
            mapCase.put('Street', cas.Asset.Logement__r.Street__c + '\n' + cas.Asset.Logement__r.Adress_complement__c);
            mapCase.put('PostalCode', cas.Asset.Logement__r.Postal_Code__c);
            mapCase.put('City', cas.Asset.Logement__r.City__c);
            mapCase.put('Country', cas.Asset.Logement__r.Country__c);
        } else {
            // Get info from last WorkOrder
            WorkOrder wo = listWOs[0];
            mapCase.put('ServiceContractId', wo.ServiceContractId);
            mapCase.put('AssetId', wo.AssetId);
            mapCase.put('ServiceTerritoryId', wo.ServiceTerritoryId);
            mapCase.put('CaseId', wo.CaseId);
            mapCase.put('AccountId', wo.AccountId);
            mapCase.put('ContactId', wo.ContactId);
            mapCase.put('AgencySubSector__c', wo.AgencySubSector__c);
            mapCase.put('Type__c', wo.Type__c);
            mapCase.put('Reason__c', wo.Reason__c);
            //mapCase.put('WorkTypeId', wo.WorkTypeId);
            mapCase.put('Street', wo.Street);
            mapCase.put('City', wo.City);
            mapCase.put('State', wo.State);
            mapCase.put('PostalCode', wo.PostalCode);
            mapCase.put('Country', wo.Country);
            mapCase.put('Campagne__c', wo.Account.Campagne__c);
        }
        
        return mapCase;
    }

    @AuraEnabled 
    public static List<Quote> getQuotes(String caseId){

        //select all workorder: 
        List<WorkOrder> lstWo = [SELECT Id FROM WorkOrder WHERE CaseId = :caseId];

        //Select all quotes for the workorders: 
        if(!lstWo.isEmpty()){
            List<Quote> lstQuote = [SELECT Id, Name, TotalPrice FROM Quote WHERE Ordre_d_execution__c IN :lstWo AND Status =:AP_Constant.quoteStatusValideSigne 
            AND Quote.RecordType.DeveloperName = 'Devis_standard'
            ];
            
            System.debug('## Quotes retrieved: ' + lstQuote);
            
            System.debug('##getQuotes');
            return lstQuote;
        }
        return null;

    }

}