public class VFC01_ContractPage {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Controller for Visualforce Page: VFP01_ContractPage
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 07-MGR-2018  MGR    1.0     Initial version
-- 12-JUL-2019  DMU    1.1     Move queries on contract to ServiceContract
--------------------------------------------------------------------------------------
**************************************************************************************/

    private ApexPages.StandardController ctrl;
    public ServiceContract currentContract {get;set;}
    public List<ServiceContract> lstContract {get;set;}
    public List<ServiceAppointment> currentSA {get;set;}
    public String dateDeCreation {get;set;}
    public String dateChoisie {get;set;}
    public Boolean displayRemise {get;set;}
    public Boolean displaySecurite {get;set;}
    public Boolean displayTotal {get;set;}
    public Boolean displayLogementAddress {get;set;}
    public OpportunityLineItem oppLineItem {get;set;}

    public String homePhone {get;set;}

    //DMU - 20190718 - Changes done dur to retofitting for Necia Core 
    //Due to issue on diplaying ServiceContract field on VF 
    //the below varibles were created
    public string nameTEST {get;set;}
    public string nomAgence {get;set;}
    public string streetAgence {get;set;}
    public string street2Agence {get;set;}
    public string postalCodeAgence {get;set;}
    public string cityAgence {get;set;}
    public string sirenAgence  {get;set;}
    public string siretAgence  {get;set;}
    public string intraCommVAT  {get;set;}
    public string salutationAcc  {get;set;}
    public string nomAcc {get;set;}
    public string billingStreetAcc  {get;set;}
    public string billingPostalCode  {get;set;}
    public string billingCityAcc  {get;set;}
    // public string personEmailAcc  {get;set;}
    public string clientNoAcc  {get;set;}
    public string assetObj  {get;set;}
    public string assetIDEqup  {get;set;}
    public string assetLogementStr  {get;set;}
    public string assetLogementCompAddr {get;set;}
    public string assetLogementPosCode {get;set;}
    public string assetLogementCity {get;set;}
    public string assetEqptype {get;set;}
    public Decimal contractSubtotal {get;set;}
    public Decimal contractTax  {get;set;}
    public Decimal contractTotalPrice {get;set;}
    public string promocodeOfferName {get;set;}
    public Decimal contractDiscount {get;set;}
    public string agencyCorpName {get;set;}
    public string agencyLegalName {get;set;}
    public Decimal agencyTechCapEur  {get;set;}
    public string agencyCorpStreet  {get;set;}
    public string agencyCorpStreet2 {get;set;}
    public string agencyCorpZipCode  {get;set;}
    public string agencyCorpCity {get;set;}
    public string agencyRCS {get;set;}
    public string agencyProfIndIns  {get;set;}
    public string agencyDecenalIns  {get;set;}
    public string agencyInsCover  {get;set;}
    public string email  {get;set;}
    
    public VFC01_ContractPage() {
        try {
            displayRemise = false;
            displaySecurite = false;
            displayTotal = false;
            oppLineItem = new OpportunityLineItem();
            currentContract = new ServiceContract();
            displayLogementAddress = false;

            //System.debug('--- id contract : ' + ApexPages.currentPage().getParameters().get('id'));
            
            string contractId = ApexPages.currentPage().getParameters().get('id');

            if(String.isNotBlank(contractId))
            lstContract = new List<ServiceContract>([SELECT Id,
                                      Tax,
                                      TotalPrice,
                                      CreatedDate,
                                      Discount,
                                      Discount__c,
                                      GrandTotal,
                                      Tax__c,
                                      Subtotal,
                                     /* Promotional_Code__c,
                                      Promotional_Code__r.Offer__c,
                                      Promotional_Code__r.Offer__r.Name,
                                      Retractation_rightrenonciation__c,*/
                                      Agency__c,
                                      Agency__r.Id,
                                      Agency__r.Name,
                                      Agency__r.Street,
                                      Agency__r.Street2__c,
                                      Agency__r.PostalCode,
                                      Agency__r.City,
                                      Agency__r.Email__c,
                                      Agency__r.Siret__c,
                                      Agency__r.Siren__c,
                                      Agency__r.Intra_Community_VAT__c,
                                      Agency__r.Legal_Form__c,
                                      Agency__r.Capital_in_Eur__c,
                                      Agency__r.Tech_Capital_In_Eur__c,
                                      Agency__r.Corporate_Name__c,
                                      Agency__r.Corporate_Street__c,
                                      Agency__r.Corporate_Street2__c,
                                      Agency__r.Corporate_ZipCode__c,
                                      Agency__r.Corporate_City__c,
                                      Agency__r.Decennal_Inurance__c,
                                      Agency__r.Insurance_Cover__c,
                                      Agency__r.Professional_Indemnity_Insurance__c,
                                      Agency__r.RCS__c,
                                      AccountId,
                                      Account.Salutation,
                                      Account.Name,
                                      Account.LastName,
                                      Account.FirstName,
                                      Account.BillingPostalCode,
                                      Account.BillingStreet,
                                      Account.BillingCity,
                                      Account.PersonEmail,
                                      Account.PersonHomePhone,
                                      Account.PersonMobilePhone,
                                      Account.ClientNumber__c,
                                      Account.IsPersonAccount, 
                                      Asset__c,
                                      Asset__r.IDEquipment__c,
                                      Asset__r.IDEquipmenta__c,
                                      toLabel(Asset__r.Equipment_type__c),
                                      Asset__r.Logement__r.Owner__c,
                                      Asset__r.Logement__r.Inhabitant__c,
                                      Asset__r.Logement__r.Inhabitant__r.Name,
                                      Asset__r.Logement__r.Inhabitant__r.PersonMobilePhone,
                                      Asset__r.Logement__r.Street__c,
                                      Asset__r.Logement__r.Adress_complement__c,
                                      Asset__r.Logement__r.Postal_Code__c,
                                      Asset__r.Logement__r.City__c,
                                      Asset__r.Logement__r.Tech_SameAddress__c
                               FROM ServiceContract 
                               WHERE Id = :contractId]);

            System.debug('mgr lstContract ' + lstContract);
            /*System.debug('--- Agence : ' + lstContract[0].Promotional_Code__c);
            // System.debug('--- Agence : ' + lstContract[0].Promotional_Code__r.Offer__c);
            // System.debug('--- Agence : ' + lstContract[0].Promotional_Code__r.Offer__r.Name);
            // System.debug('--- Agence : ' + lstContract[0].Issuing_Agency__r.Name);
            // System.debug('--- Agence : ' + lstContract[0].Issuing_Agency__c);
            // System.debug('--- Agence : ' + lstContract[0].Issuing_Agency__r.Corporate_Street__c);
            // System.debug('--- Agence : ' + lstContract[0].Issuing_Agency__r.Corporate_Street2__c);
            // System.debug('--- Agence : ' + lstContract[0].Issuing_Agency__r.Corporate_ZipCode__c);

            System.debug('mgr home Phone : ' + lstContract[0].Account.PersonHomePhone);
            System.debug('mgr mobile Phone : ' + lstContract[0].Account.PersonMobilePhone);*/

            email = ''; 

            if(lstContract[0].Account != null){
                if(lstContract[0].Account.IsPersonAccount==true){
                    if(lstContract[0].Account.PersonMobilePhone != null)
                    homePhone = lstContract[0].Account.PersonMobilePhone;
                    else 
                    homePhone = lstContract[0].Account.PersonHomePhone;

                    email = lstContract[0].Account.PersonEmail;
                }else{
                    list <Contact> coLst = [select id, MobilePhone, HomePhone, Email from Contact where AccountId =:lstContract[0].AccountId order by createdDate desc limit 1 ];
                    if(coLst.size()>0){
                        email = coLst[0].Email;

                        if(coLst[0].MobilePhone != null)
                        homePhone = coLst[0].MobilePhone;
                        else 
                        homePhone = coLst[0].HomePhone;
                    }                    
                }                
            }

            if(lstContract[0].Asset__c != null 
              && lstContract[0].Asset__r.Logement__c != null 
              && !lstContract[0].Asset__r.Logement__r.Tech_SameAddress__c){
                displayLogementAddress = True;
            }

            if(lstContract.size() > 0){

                currentContract = lstContract[0];

                displayRemise = (lstContract[0].Discount != null && lstContract[0].Discount != 0) ? True : false ;
                System.debug('mgr displayRemise ' + displayRemise);

                if(currentContract.CreatedDate != null)
                dateDeCreation = currentContract.CreatedDate.format('dd/MM/yyyy');

                currentSA = new List<ServiceAppointment>([SELECT Id, SchedStartTime, Opportunity__c, Opportunity__r.Id
                                                          FROM ServiceAppointment
                                                          WHERE Service_Contract__c =: currentContract.Id]);
                //SRA 
                //changed dateChoisie to Case StartDate
                List<Case> lstCase = [SELECT Id, Start_Date__c
                                     FROM Case 
                                     WHERE Service_Contract__c =: currentContract.Id];

                System.debug('##lstCase ' + lstCase);

                if(lstCase[0].Start_Date__c != null) {
                    // dateChoisie = String.valueOf(lstCase[0].Start_Date__c);
                    dateChoisie = DateTime.newInstance(lstCase[0].Start_Date__c.year(), lstCase[0].Start_Date__c.month(), lstCase[0].Start_Date__c.day()).format('dd-MM-yyyy');
                }

                List<ContractLineItem> lstContractsItems =  [SELECT Id, Product2.Name
                                                            FROM ContractLineItem
                                                            WHERE ServiceContractId = :currentContract.Id
                                                            ORDER BY LastModifiedDate ASC LIMIT 1];

                if(lstContractsItems.size() > 0) {
                    if(lstContractsItems[0].Product2.Name.contains('Sécurité')) {
                        displaySecurite = True;
                    }
                }

                if(currentSA.size() > 0){
                    // if(currentSA[0].SchedStartTime != null)
                    // dateChoisie = currentSA[0].SchedStartTime.format('dd/MM/yyyy');

                    if(currentSA[0].Opportunity__c != null){

                        List<OpportunityLineItem> lstOpplineItem = new List<OpportunityLineItem>([SELECT Id, Discount, TotalPrice, Name
                                                                                                  FROM OpportunityLineItem
                                                                                                  WHERE OpportunityId =: currentSA[0].Opportunity__c]);

                        
                        if(lstOpplineItem.size() > 0){
                            oppLineItem = lstOpplineItem[0];

                            if(lstOpplineItem[0].Discount != null){
                                //displayRemise = True;
                            }

                            if(lstOpplineItem[0].Name.contains('Sécurité'))
                            displaySecurite = True;
                            else if(lstOpplineItem[0].Name.contains('Total'))
                            displayTotal = True;
                        }

                    }
                }
                System.debug('***currentContract: ' + currentContract);

                if(string.IsNotBlank(currentContract.Agency__c)){
                  nomAgence = currentContract.Agency__r.Name;
                  streetAgence = currentContract.Agency__r.Street;
                  street2Agence = currentContract.Agency__r.Street2__c;
                  postalCodeAgence = currentContract.Agency__r.PostalCode;
                  cityAgence = currentContract.Agency__r.City;
                  sirenAgence = currentContract.Agency__r.Siren__c;
                  siretAgence = currentContract.Agency__r.Siret__c;
                  intraCommVAT = currentContract.Agency__r.Intra_Community_VAT__c;
                  agencyCorpName = currentContract.Agency__r.Corporate_Name__c;
                  agencyLegalName = currentContract.Agency__r.Legal_Form__c;
                  agencyTechCapEur = currentContract.Agency__r.Tech_Capital_In_Eur__c;
                  agencyCorpStreet = currentContract.Agency__r.Corporate_Street__c;
                  agencyCorpStreet2 = currentContract.Agency__r.Corporate_Street2__c;
                  agencyCorpZipCode = currentContract.Agency__r.Corporate_ZipCode__c;
                  agencyCorpCity = currentContract.Agency__r.Corporate_City__c;
                  agencyRCS = currentContract.Agency__r.RCS__c;
                  agencyProfIndIns = currentContract.Agency__r.Professional_Indemnity_Insurance__c;
                  agencyDecenalIns = currentContract.Agency__r.Decennal_Inurance__c;
                  agencyInsCover = currentContract.Agency__r.Insurance_Cover__c;
                }

                if(string.IsNotBlank(currentContract.AccountId)){
                  salutationAcc = currentContract.Account.Salutation;
                  nomAcc = currentContract.Account.Name;
                  billingStreetAcc = currentContract.Account.BillingStreet;
                  billingPostalCode = currentContract.Account.BillingPostalCode;
                  billingCityAcc = currentContract.Account.BillingCity;
                //   personEmailAcc = currentContract.Account.PersonEmail;
                  clientNoAcc = currentContract.Account.ClientNumber__c;
                }


                if(string.IsNotBlank(currentContract.Asset__c)){
                  assetObj = currentContract.Asset__c;
                  assetIDEqup = currentContract.Asset__r.IDEquipmenta__c;
                  assetLogementStr = currentContract.Asset__r.Logement__r.Street__c;
                  assetLogementCompAddr = currentContract.Asset__r.Logement__r.Adress_complement__c;
                  assetLogementPosCode = currentContract.Asset__r.Logement__r.Postal_Code__c;
                  assetLogementCity = currentContract.Asset__r.Logement__r.City__c;
                  assetEqptype = currentContract.Asset__r.Equipment_type__c;
                }                

                contractSubtotal = currentContract.Subtotal;
                contractTax = Decimal.valueOf(currentContract.Tax__c);
                contractTotalPrice = currentContract.GrandTotal;
                //promocodeOfferName = currentContract.Promotional_Code__r.Offer__r.Name;
                contractDiscount = currentContract.Discount;
            }

        } catch (QueryException e) {
            //RSA - 20190304
            System.debug('Entering catch exception - currentContract: ' + currentContract);
            //End RSA - 20190304
            
            //currentContract = new Contract();
        }

        //ctrl = new ApexPages.StandardController(currentContract);
    }



}