/**
 * @File Name          : VFC06_PACAttestation_TEST.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/24/2020, 5:56:55 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/1/2019, 3:18:29 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
@IsTest
public with sharing class VFC06_PACAttestation_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Test Class for VFC06_PACAttestation
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 01-AOU-2018  SRM    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/
    static User mainUser;
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<Measurement__c> lstTestMeasurement = new List<Measurement__c>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<ServiceContract> lstContract = new List<ServiceContract>();
    //RRJ 20190812START
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory servTerritory = new ServiceTerritory();
    static sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c();
    static List<PricebookEntry> lstPrcBkEntry = new List<PricebookEntry>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();
    //RRJ 20190812 END

	static {
		mainUser = TestFactory.createAdminUser('VFC06_PACAttestation_TEST@test.COM', 
												TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            //creating account
            testAcc = TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating contract
            ServiceContract testContract = TestFactory.createServiceContract('test', testAcc.Id);
            testContract.Pricebook2Id = Test.getStandardPricebookId();
            lstContract.add(testContract);
			insert lstContract;

            //creating logement
            for(integer i =0; i<5; i++){
                Logement__c lgmt = new Logement__c(Account__c = testAcc.Id);
                lstTestLogement.add(lgmt);
            }
            insert lstTestLogement;

            //creating products
            lstTestProd.add(TestFactory.createProductAsset('Ramonage gaz'));
            lstTestProd.add(TestFactory.createProductAsset('Entretien gaz'));
            lstTestProd[0].IsBundle__c = true;
            lstTestProd[1].IsBundle__c = true;
            insert lstTestProd;

            // creating Assets
            for(Integer i = 0; i<5; i++){
                Asset eqp = TestFactory.createAsset('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[i].Id);
                eqp.Product2Id = lstTestProd[0].Id;
                eqp.Logement__c = lstTestLogement[i].Id;
                eqp.AccountId = testacc.Id;
                lstTestAssset.add(eqp);
            }
            insert lstTestAssset;

            // create case
            case cse = new case(AccountId = testacc.id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id);
            cse.recordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_anomaly').getRecordTypeId();
            insert cse;


            WorkType wrktypeGaz = TestFactory.createWorkType('wrktypeGaz','Hours',1);
            WorkType wrktypefioul = TestFactory.createWorkType('wrktypeFioul','Hours',2);
            WorkType wrktypePompe = TestFactory.createWorkType('wrktypePompe','Hours',3);

            wrktypeGaz.Equipment_type__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz;
            wrktypefioul.Equipment_type__c = AP_Constant.wrkTypeEquipmentTypeChaudierefioul;
            wrktypePompe.Equipment_type__c = AP_Constant.wrkTypeEquipmentTypePompeachaleur;


            lstWrkTyp = new list<WorkType>{wrktypeGaz,wrktypefioul,wrktypePompe};
            insert lstWrkTyp;
            System.debug('lstWrkTyp ' + lstWrkTyp);

            for(Integer i=0; i<3; i++){
                WorkOrder wrkOrd = TestFactory.createWorkOrder();
                wrkOrd.caseId = cse.id;
                wrkOrd.WorkTypeId = lstWrkTyp[0].Id;
                wrkOrd.Type__c =  'Maintenance';
                wrkOrd.assetId = lstTestAssset[i].Id;
                lstWrkOrd.add(wrkOrd);
            }
            insert lstWrkOrd;

            //creating measurement
            for(Integer i = 0; i<3; i++){
                Measurement__c mes = new Measurement__c(Parent_Work_Order__c=lstWrkOrd[i].Id); 
                mes.Asset__c = lstTestAssset[i].Id;
                lstTestMeasurement.add(mes);
            }
            insert lstTestMeasurement;

            //RRJ 20190812 START
            //creating operating hours
            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;

            //create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa;

            //creating service territory
            servTerritory = TestFactory.createServiceTerritory('SGS - test Territory', opHrs.Id, sofa.Id);
            // servTerritory.Sofactoapp_Raison_Social__c =;  
            insert servTerritory;

            //creating bricebookentry
            lstPrcBkEntry.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstTestProd[0].Id, 100));
            lstPrcBkEntry.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstTestProd[1].Id, 150));
            insert lstPrcBkEntry;

            //creating contract line item
            ContractLineItem conLn = TestFactory.createContractLineItem(lstContract[0].Id, lstPrcBkEntry[0].Id, 0, 1);
            conLn.StartDate = Date.today().addDays(-10);
            conLn.EndDate = Date.today().addDays(10);
            insert conLn;
            //RRJ 20190812 END

            ServiceAppointment serappGaz = TestFactory.createServiceAppointment(lstWrkOrd[0].Id);
            serappGaz.Service_Contract__c = lstContract[0].Id;
            serappGaz.ServiceTerritoryId = servTerritory.Id;
            serappGaz.SchedStartTime = Datetime.now().addYears(-1);
            serappGaz.SchedEndTime = Datetime.now();
            ServiceAppointment serappFioul = TestFactory.createServiceAppointment(lstWrkOrd[1].Id);
            serappFioul.Service_Contract__c = lstContract[0].Id;
            serappFioul.ServiceTerritoryId = servTerritory.Id;
            serappFioul.SchedStartTime = Datetime.now().addYears(-1);
            serappFioul.SchedEndTime = Datetime.now();
            ServiceAppointment serappPompe = TestFactory.createServiceAppointment(lstWrkOrd[2].Id);
            serappPompe.Service_Contract__c = lstContract[0].Id;
            serappPompe.ServiceTerritoryId = servTerritory.Id;
            serappPompe.SchedStartTime = Datetime.now().addYears(-1);
            serappPompe.SchedEndTime = Datetime.now();


            lstServiceApp = new List<ServiceAppointment>{serappGaz,serappFioul,serappPompe};
            insert lstServiceApp;

        }
	}


	@isTest
	public static void doGenerate(){

		System.runAs(mainUser){
			
			Test.startTest();

				List<ServiceAppointment> lstServiceAppointment1 = new List<ServiceAppointment>([SELECT id 
															  FROM ServiceAppointment
															  WHERE id in: lstServiceApp]);
				System.debug('mgr lstServiceAppointment1 ' + lstServiceAppointment1);

				PageReference pageRef = Page.VFP06_AAE_PAC; 
				pageRef.getParameters().put('id', lstServiceAppointment1[0].Id);
				Test.setCurrentPage(pageRef);

                VFC06_PACAttestation pac = new VFC06_PACAttestation();

	        Test.stopTest();

		}
	}

}