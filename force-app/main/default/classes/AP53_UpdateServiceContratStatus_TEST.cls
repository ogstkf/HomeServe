/**
* @File Name          : AP53_UpdateServiceContratStatus_TEST.cls
* @Description        : 
* @Author             : AMO
* @Group              : 
* @Last Modified By   : ARA
* @Last Modified On   : 04-09-2021
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    16/12/2019         LGO                    Initial Version
**/
@isTest(SeeAllData=true)
public with sharing class AP53_UpdateServiceContratStatus_TEST {
    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<Case> lstCse = new List<Case>();
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static OperatingHours opHrs = new OperatingHours();
    static List<ServiceTerritory> lstAgences = new List<ServiceTerritory>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstClis = new List<ContractLineItem>();
    static List<ServiceAppointment> lstServApp = new List<ServiceAppointment>();
    static List<sofactoapp__R_glement__c> lstRGlements = new List<sofactoapp__R_glement__c>();
    static List<sofactoapp__Factures_Client__c> lstFacturesClients = new List<sofactoapp__Factures_Client__c>();
    static List<Opportunity> lstOpportunity = new List<Opportunity>();
    static list<Product2> lstProd = new list<Product2>();
    // static  Bypass__c userBypass;
    static Bypass__c bp = new Bypass__c();
    static sofactoapp__Coordonnees_bancaires__c bnkDet;
    
    
    
    static {
        
        adminUser = TestFactory.insertSofactoAdminUser('AP53_UpdateServiceContratStatus_TEST@test.COM');
        
        
        System.runAs(adminUser){
            // userBypass = new Bypass__c(BypassValidationRules__c = true);
            // insert userBypass;

            bp.SetupOwnerId  = adminUser.Id;
            bp.BypassValidationRules__c = True;
            bp.BypassTrigger__c = 'AP16,WorkOrderTrigger';

            insert bp;
         
            //create accounts
            for(Integer i=0; i<3; i++){
                lstTestAcc.add(TestFactory.createAccount('testAcc'+i));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
                lstTestAcc[i].BillingPostalCode = '1234'+i;
                lstTestAcc[i].sofactoapp__Compte_auxiliaire__c =lstTestAcc[0].Id;
            }
            
            insert lstTestAcc;
            
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstTestAcc.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstTestAcc.get(2).Id);
            lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstTestAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstTestAcc.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            
            update lstTestAcc;
            
            Logement__c lgmt1 = TestFactory.createLogement('logementTest', 'logement@test.com', 'lgmt');
            lgmt1.Postal_Code__c = 'lgmt1PC';
            lgmt1.City__c = 'lgmt1City';
            lgmt1.Account__c = lstTestAcc[0].Id;
            lstTestLogement.add(lgmt1);
            
            Logement__c lgmt2 = TestFactory.createLogement('logementTest', 'logement@test.com', 'lgmt');
            lgmt2.Postal_Code__c = 'lgmt2PC';
            lgmt2.City__c = 'lgmt2City';
            lgmt2.Account__c = lstTestAcc[1].Id;
            lstTestLogement.add(lgmt2);
            
            Logement__c lgmt3 = TestFactory.createLogement('logementTest', 'logement@test.com', 'lgmt');
            lgmt3.Postal_Code__c = 'lgmt3PC';
            lgmt3.City__c = 'lgmt3City';
            lgmt3.Account__c = lstTestAcc[2].Id;
            lstTestLogement.add(lgmt3);
            
            insert lstTestLogement;

           
            lstProd = new list<Product2>{
                new Product2(Name = 'Product X1',
                    ProductCode = 'VF-Pro-X1',
                    isActive = true,
                    Equipment_family__c = 'Chaudière',
                    Equipment_type__c = 'Chaudière gaz',
                    Statut__c = 'Approuvée',
                    IsBundle__c = true
                    ),
                new Product2(Name='Prod1',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId(),
                             IsBundle__c = true),
                    
                    
                new Product2(Name='Prod2',
                                RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
                };
                insert lstProd;
            
            //creating products
            lstTestProd.add(TestFactory.createProductAsset('Ramonage gaz'));
            lstTestProd.add(TestFactory.createProductAsset('Entretien gaz'));
            lstTestProd.add(TestFactory.createProductAsset('Depanage'));
            
            insert lstTestProd;
            
            // creating Assets
            Asset eqp1 = TestFactory.createAccount('equipement1', AP_Constant.assetStatusActif, lstTestLogement[0].Id);
            eqp1.Product2Id = lstProd[0].Id;
            eqp1.Logement__c = lstTestLogement[0].Id;
            eqp1.AccountId = lstTestAcc[0].Id;
            lstTestAssset.add(eqp1);
            
            Asset eqp2 = TestFactory.createAccount('equipement2', AP_Constant.assetStatusActif, lstTestLogement[1].Id);
            eqp2.Product2Id = lstTestProd[1].Id;
            eqp2.Logement__c = lstTestLogement[1].Id;
            eqp2.AccountId = lstTestAcc[1].Id;
            lstTestAssset.add(eqp2);
            
            Asset eqp3 = TestFactory.createAccount('equipement3', AP_Constant.assetStatusActif, lstTestLogement[2].Id);
            eqp3.Product2Id = lstTestProd[2].Id;
            eqp3.Logement__c = lstTestLogement[2].Id;
            eqp3.AccountId = lstTestAcc[2].Id;
            lstTestAssset.add(eqp3);
            
            insert lstTestAssset;
            
            case cse1 = new case(AccountId = lstTestAcc[0].Id, type = 'Troubleshooting', AssetId = lstTestAssset[0].Id);
            //cse1.Reason__c = 'Visite sous contrat';
            lstCse.add(cse1);
            case cse2 = new case(AccountId = lstTestAcc[1].Id, type = 'Troubleshooting', AssetId = lstTestAssset[1].Id);
            //cse2.Reason__c = 'Visite sous contrat';
            lstCse.add(cse2);
            case cse3 = new case(AccountId = lstTestAcc[2].Id, type = 'Troubleshooting', AssetId = lstTestAssset[2].Id);
            //cse3.Reason__c = 'Visite sous contrat';
            lstCse.add(cse3);
            case cse4 = new case(AccountId = lstTestAcc[0].Id, type = 'Troubleshooting', AssetId = lstTestAssset[0].Id);
            // cse4.Reason__c = 'Total Failure';
            lstCse.add(cse4);
            // case cse5 = new case(AccountId = lstTestAcc[1].Id, type = 'Troubleshooting', AssetId = lstTestAssset[1].Id);
            // cse5.Reason__c = 'Total Failure';
            // lstCse.add(cse5);
            // case cse6 = new case(AccountId = lstTestAcc[2].Id, type = 'Troubleshooting', AssetId = lstTestAssset[2].Id);
            // cse6.Reason__c = 'Total Failure';
            // lstCse.add(cse6);
            
            insert lstCse;
            
            WorkType wrkType1 = TestFactory.createWorkType('wrkType1','Hours',1);
            wrkType1.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType1.Type_de_client__c = 'Tous';
            WorkType wrkType2 = TestFactory.createWorkType('wrkType2','Hours',2);
            wrkType2.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType2.Type_de_client__c = 'Tous';
            WorkType wrkType3 = TestFactory.createWorkType('wrkType3','Hours',3);
            wrkType3.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType3.Type_de_client__c = 'Tous';
            
            lstWrkTyp = new list<WorkType>{wrkType1, wrkType2, wrkType3};
                insert lstWrkTyp;
            
            System.debug('lstWrkTyp ' + lstWrkTyp);
            
            WorkOrder wrkOrd1 = TestFactory.createWorkOrder();
            wrkOrd1.caseId = lstCse[0].id;
            wrkOrd1.AccountId = lstTestAcc[0].Id;
            wrkOrd1.WorkTypeId = lstWrkTyp[0].Id;
            wrkOrd1.AssetId = lstTestAssset[0].Id;
            lstWrkOrd.add(wrkOrd1);
            
            WorkOrder wrkOrd2 = TestFactory.createWorkOrder();
            wrkOrd2.caseId = lstCse[1].id;
            wrkOrd2.AccountId = lstTestAcc[1].Id;
            wrkOrd2.WorkTypeId = lstWrkTyp[1].Id;
            wrkOrd2.AssetId = lstTestAssset[1].Id;
            lstWrkOrd.add(wrkOrd2);
            
            WorkOrder wrkOrd3 = TestFactory.createWorkOrder();
            wrkOrd3.caseId = lstCse[2].id;
            wrkOrd3.AccountId = lstTestAcc[2].Id;
            wrkOrd3.WorkTypeId = lstWrkTyp[2].Id;
            wrkOrd3.AssetId = lstTestAssset[2].Id;
            lstWrkOrd.add(wrkOrd3);
            
            WorkOrder wrkOrd4 = TestFactory.createWorkOrder();
            wrkOrd4.caseId = lstCse[3].id;
            wrkOrd4.AccountId = lstTestAcc[0].Id;
            wrkOrd4.WorkTypeId = lstWrkTyp[0].Id;
            wrkOrd4.AssetId = lstTestAssset[0].Id;
            lstWrkOrd.add(wrkOrd4);
            
            // WorkOrder wrkOrd5 = TestFactory.createWorkOrder();
            // wrkOrd5.caseId = lstCse[4].id;
            // wrkOrd5.AccountId = lstTestAcc[1].Id;
            // wrkOrd5.WorkTypeId = lstWrkTyp[1].Id;
            // wrkOrd5.AssetId = lstTestAssset[1].Id;
            // lstWrkOrd.add(wrkOrd5);
            
            // WorkOrder wrkOrd6 = TestFactory.createWorkOrder();
            // wrkOrd6.caseId = lstCse[5].id;
            // wrkOrd6.AccountId = lstTestAcc[2].Id;
            // wrkOrd6.WorkTypeId = lstWrkTyp[2].Id;
            // wrkOrd6.AssetId = lstTestAssset[2].Id;
            // lstWrkOrd.add(wrkOrd6);
            
            insert lstWrkOrd;
            
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );
            
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            
            PricebookEntry PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            insert PrcBkEnt;
            lstPrcBkEnt.add(PrcBkEnt);

            //Create Invoice Template
            // sofactoapp__Modele__c InvoiceTemp = new sofactoapp__Modele__c(Name = 'Test Template', sofactoapp__Page__c = '2', sofactoapp__Type__c = 'Facture');
            // insert InvoiceTemp;

            sofactoapp__Modele__c template = [SELECT Id, Name FROM sofactoapp__Modele__c WHERE Name = 'Facture standard' LIMIT 1];
            
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST2', sofactoapp__Credit_prefix__c= '244', sofactoapp__Invoice_prefix__c='422', sofactoapp__Modele__c = template.Id);
            insert sofa;  
            
            bnkDet = new sofactoapp__Coordonnees_bancaires__c(
                sofactoapp__Compte__c = lstTestAcc[0].Id
                // , sofactoapp__Compte_comptable__c = 
                , Sofacto_Actif__c = true
                , tech_Rib_Valid__c = true
                , sofactoapp__IBAN__c = 'FR14 2004 1010 0505 0001 3M02 606'
                , sofactoapp__Raison_sociale__c = sofa.Id
            );
            insert bnkDet;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('Business Operating hours');
            insert opHrs;

            //Create ServiceTerritory
            lstAgences = new List<ServiceTerritory>{
                TestFactory.createServiceTerritory('Agence 1', opHrs.Id, sofa.Id),
                TestFactory.createServiceTerritory('Agence 2', opHrs.Id, sofa.Id),
                TestFactory.createServiceTerritory('Agence 3', opHrs.Id, sofa.Id),
                TestFactory.createServiceTerritory('Agence 4', opHrs.Id, sofa.Id)
                // TestFactory.createServiceTerritory('Agence 5', opHrs.Id, sofa.Id),
                // TestFactory.createServiceTerritory('Agence 6', opHrs.Id, sofa.Id)
            };
            insert lstAgences;
       
            //service contract
            // for(Integer i=0; i<1; i++){
            lstServCon.add(TestFactory.createServiceContract('testServCon', lstTestAcc[0].Id));
            lstServCon[0].PriceBook2Id = lstPrcBk[0].Id;
            lstServCon[0].Contract_Renewed__c = true;
            lstServCon[0].Sofactoapp_Mode_de_paiement__c = 'Prélèvement';
            lstServCon[0].Type__c = AP_Constant.serviceContractTypeInd ;
            lstServCon[0].Contrat_signe_par_le_client__c = true;
            lstServCon[0].Contract_Status__c = 'Draft';
            lstServCon[0].Tax__c = '5.5';
            lstServCon[0].sofactoapp_Rib_prelevement__c = bnkDet.Id;
            lstServCon[0].Sofacto_DateOfSignature__c = system.today().addDays(1);
            lstServCon[0].Statut_VE__c = 'VE effectuée';
            lstServCon[0].Agency__c = lstAgences[0].Id;
            
            lstServCon.add(TestFactory.createServiceContract('testServCon1', lstTestAcc[0].Id));
            lstServCon[1].PriceBook2Id = lstPrcBk[0].Id;
            lstServCon[1].Contract_Renewed__c = true;
            lstServCon[1].Sofactoapp_Mode_de_paiement__c = 'Chèque';
            lstServCon[1].Type__c = AP_Constant.serviceContractTypeInd ;
            lstServCon[1].Contrat_signe_par_le_client__c = false;
            lstServCon[1].Contract_Status__c = 'Draft';
            lstServCon[1].Tax__c = '5.5';
            lstServCon[1].Statut_VE__c = 'VE effectuée';
            lstServCon[1].Agency__c = lstAgences[1].Id;
            
            lstServCon.add(TestFactory.createServiceContract('testServCon2', lstTestAcc[0].Id));
            lstServCon[2].PriceBook2Id = lstPrcBk[0].Id;
            lstServCon[2].Contract_Renewed__c = false;
            lstServCon[2].Sofactoapp_Mode_de_paiement__c = 'Prélèvement';
            lstServCon[2].Type__c = AP_Constant.serviceContractTypeInd;
            lstServCon[2].Contrat_signe_par_le_client__c = true;
            lstServCon[2].Contract_Status__c = 'Draft';
            lstServCon[2].Tax__c = '5.5';
            lstServCon[2].Statut_VE__c = 'VE effectuée';
            lstServCon[2].Agency__c = lstAgences[2].Id;
            
            
            lstServCon.add(TestFactory.createServiceContract('testServCon3', lstTestAcc[0].Id));
            lstServCon[3].PriceBook2Id = lstPrcBk[0].Id;
            lstServCon[3].Contract_Renewed__c = true;
            lstServCon[3].Sofactoapp_Mode_de_paiement__c = 'Chèque';
            lstServCon[3].Type__c = AP_Constant.serviceContractTypeInd ;
            lstServCon[3].Contrat_signe_par_le_client__c = false;     
            lstServCon[3].Contract_Status__c = 'Draft';
            lstServCon[3].Tax__c = '5.5';
            lstServCon[0].Statut_VE__c = 'VE non nécessaire';
            lstServCon[3].Agency__c = lstAgences[3].Id;
            
            
            // lstServCon.add(TestFactory.createServiceContract('testServCon4', lstTestAcc[0].Id));
            // lstServCon[4].PriceBook2Id = lstPrcBk[0].Id;
            // lstServCon[4].Contract_Renewed__c = true;
            // lstServCon[4].Sofactoapp_Mode_de_paiement__c = 'Chèque';
            // lstServCon[4].Type__c = AP_Constant.serviceContractTypeInd ;
            // lstServCon[4].Contrat_signe_par_le_client__c = true; 
            // lstServCon[4].Contract_Status__c = 'Draft';
            // lstServCon[4].Tax__c = '5.5';
            // lstServCon[0].Statut_VE__c = 'VE non nécessaire';
            // lstServCon[4].Agency__c = lstAgences[4].Id;
            
            // lstServCon.add(TestFactory.createServiceContract('testServCon5', lstTestAcc[0].Id));
            // lstServCon[5].PriceBook2Id = lstPrcBk[0].Id;
            // lstServCon[5].Contract_Renewed__c = true;
            // lstServCon[5].Sofactoapp_Mode_de_paiement__c = 'Chèque';
            // lstServCon[5].Type__c = AP_Constant.serviceContractTypeInd ;
            // lstServCon[5].Contrat_signe_par_le_client__c = false;   
            // lstServCon[5].Contract_Status__c = 'Draft';
            // lstServCon[5].Tax__c = '5.5';
            // lstServCon[0].Statut_VE__c = 'VE non nécessaire';
            // lstServCon[5].Agency__c = lstAgences[5].Id;
            
            // }
               
            // lstServCon = [SELECT Account.sofactoapp__Compte_auxiliaire__c, Id FROM ServiceContract WHERE Id IN: lstServCon];
            insert lstServCon;    
                      
            //service appointment
            // for(Integer i=0; i<1; i++){
            lstServApp.add(TestFactory.createServiceAppointment(lstTestAcc[0].Id));
            lstServApp[0].Service_Contract__c = lstServCon[0].Id;
            lstServApp[0].Status = 'Done OK';
            lstServApp[0].TECH_SA_payed__c = true;
            lstServApp[0].Work_Order__c = lstWrkOrd[0].Id;
            lstServApp[0].ActualEndTime = DateTime.now().addDays(7).addHours(2);
            lstServApp[0].ActualStartTime = DateTime.now().addDays(7);
            
            lstServApp.add(TestFactory.createServiceAppointment(lstTestAcc[0].Id));
            lstServApp[1].Service_Contract__c = lstServCon[1].Id;
            lstServApp[1].Status = 'Done KO';
            lstServApp[1].TECH_SA_payed__c = true;
            lstServApp[1].Work_Order__c = lstWrkOrd[1].Id;
            lstServApp[1].ActualEndTime = DateTime.now().addDays(7).addHours(2);
            lstServApp[1].ActualStartTime = DateTime.now().addDays(7);
            
            lstServApp.add(TestFactory.createServiceAppointment(lstTestAcc[0].Id));
            lstServApp[2].Service_Contract__c = lstServCon[2].Id;
            lstServApp[2].Status = 'Done OK';
            lstServApp[2].TECH_SA_payed__c = true;
            lstServApp[2].Work_Order__c = lstWrkOrd[2].Id;
            lstServApp[2].ActualEndTime = DateTime.now().addDays(7).addHours(2);
            lstServApp[2].ActualStartTime = DateTime.now().addDays(7);
            
            lstServApp.add(TestFactory.createServiceAppointment(lstTestAcc[0].Id));
            lstServApp[3].Service_Contract__c = lstServCon[3].Id;
            lstServApp[3].Status = 'Done KO';
            lstServApp[3].TECH_SA_payed__c = true;     
            lstServApp[3].Work_Order__c = lstWrkOrd[3].Id;
            lstServApp[3].ActualEndTime = DateTime.now().addDays(7).addHours(2);
            lstServApp[3].ActualStartTime = DateTime.now().addDays(7);
            
            // lstServApp.add(TestFactory.createServiceAppointment(lstTestAcc[0].Id));
            // lstServApp[4].Service_Contract__c = lstServCon[4].Id;
            // lstServApp[4].Status = 'Done OK';
            // lstServApp[4].TECH_SA_payed__c = false;
            // lstServApp[4].Signature_Client_approved__c = true;
            // lstServApp[4].Work_Order__c = lstWrkOrd[4].Id;
            // lstServApp[4].ActualEndTime = DateTime.now().addDays(7).addHours(2);
            // lstServApp[4].ActualStartTime = DateTime.now().addDays(7);
            
            // lstServApp.add(TestFactory.createServiceAppointment(lstTestAcc[0].Id));
            // lstServApp[5].Service_Contract__c = lstServCon[5].Id;
            // lstServApp[5].Status = 'Done KO';
            // lstServApp[5].TECH_SA_payed__c = false;    
            // lstServApp[5].Signature_Client_approved__c = true;                                          
            // lstServApp[5].Work_Order__c = lstWrkOrd[5].Id;
            // lstServApp[5].ActualEndTime = DateTime.now().addDays(7).addHours(2);
            // lstServApp[5].ActualStartTime = DateTime.now().addDays(7);
            
            insert lstServApp;            
            
            lstOpportunity = new List<Opportunity>{TestFactory.createOpportunity('test', lstTestAcc[0].Id), 
                TestFactory.createOpportunity('test', lstTestAcc[0].Id)}; 
                    lstOpportunity[0].StageName = AP_Constant.oppStageQua;
            lstOpportunity[1].StageName = AP_Constant.oppStageQua;
            insert lstOpportunity;     
            
            
            lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                Sofactoapp_Contrat_de_service__c = lstServCon[0].Id
                , sofactoapp__Compte__c = lstTestAcc[0].Id
                , sofactoapp__emetteur_facture__c = sofa.Id
                , SofactoappType_Prestation__c = 'Contrat individuel'
            ));
            
            lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                Sofactoapp_Contrat_de_service__c = lstServCon[1].Id
                , sofactoapp__Compte__c = lstTestAcc[1].Id
                , sofactoapp__emetteur_facture__c = sofa.Id
                , SofactoappType_Prestation__c = 'Contrat individuel'
            ));
            
            lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                Sofactoapp_Contrat_de_service__c = lstServCon[2].Id
                , sofactoapp__Compte__c = lstTestAcc[2].Id
                , sofactoapp__emetteur_facture__c = sofa.Id
                , SofactoappType_Prestation__c = 'Contrat individuel'
            ));
            
            // lstFacturesClients.add(new sofactoapp__Factures_Client__c(
            //     Sofactoapp_Contrat_de_service__c = lstServCon[3].Id
            //     , sofactoapp__Compte__c = lstTestAcc[0].Id
            //     , sofactoapp__emetteur_facture__c = sofa.Id
            //     , SofactoappType_Prestation__c = 'Contrat individuel'
            // ));
            
            // lstFacturesClients.add(new sofactoapp__Factures_Client__c(
            //     Sofactoapp_Contrat_de_service__c = lstServCon[4].Id
            //     , sofactoapp__Compte__c = lstTestAcc[1].Id
            //     , sofactoapp__emetteur_facture__c = sofa.Id
            //     , SofactoappType_Prestation__c = 'Contrat individuel'
            // ));
            
            // lstFacturesClients.add(new sofactoapp__Factures_Client__c(
            //     Sofactoapp_Contrat_de_service__c = lstServCon[5].Id
            //     , sofactoapp__Compte__c = lstTestAcc[2].Id
            //     , sofactoapp__emetteur_facture__c = sofa.Id
            //     , SofactoappType_Prestation__c = 'Contrat individuel'
            // ));
            
            insert lstFacturesClients;
            
            for (Integer i = 0; i < 3; i++) {
                lstRGlements.add(new sofactoapp__R_glement__c(
                    sofactoapp__Facture__c = lstFacturesClients[i].Id
                    , sofactoapp__Montant__c = 22
                    , sofactoapp__Mode_de_paiement__c = 'Chèque'
                    , statut_du_paiement__c = 'Collecté'
                    , sofactoapp__En_attente__c = false
                ));
            }


            // //    insert lstRGlements; 
            
            // Product2 lstTestProd = new Product2(Name = 'Product X1',
            // ProductCode = 'VF-Pro-X1',
            // isActive = true,
            // Equipment_family__c = 'Chaudière',
            // Equipment_type__c = 'Chaudière gaz',
            // Statut__c = 'Approuvée'
            // );
            // insert lstTestProd;

            // // creating Assets
            // for(Integer i = 0; i<5; i++){
            //     Asset eqp = TestFactory.createAccount('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[0].Id);
            //     eqp.Product2Id = lstTestProd.Id;
            //     eqp.Logement__c = lstTestLogement[0].Id;
            //     eqp.AccountId = lstTestAcc[0].Id;
            //     lstTestAssset.add(eqp);
            // }
            // upsert lstTestAssset;            
        }
    }
    
    @isTest
    static void testUpdateStatusContrat(){
        System.runAs(adminUser){

            try{
                insert lstRGlements; 
            }catch(Exception e){
                System.debug('### error: '+e);
            }

            try{
                Test.startTest();
          
                    lstServCon[0].Contrat_signe_par_le_client__c = false;    
                    lstServCon[0].Contract_Renewed__c = false;   
                    lstServCon[0].Contract_Status__c = 'Pending Payment';

                    update lstServCon[0];     
                Test.stopTest();
            }catch(Exception e){
                System.debug('#### error: '+e);
            }
        }
    }

    @isTest
    static void testUpdateConStatus(){
        System.runAs(adminUser){

            try{
                insert lstRGlements; 
            }catch(Exception e){
                System.debug('### error: '+e);
            }

            try{
                Test.startTest();
                lstServCon[2].Contrat_signe_par_le_client__c = false;
                lstServCon[2].Contract_Renewed__c = true;
                lstServCon[2].Contract_Status__c = 'Pending Payment';
    
                update lstServCon[2];
                        
                Test.stopTest();
            }catch(Exception e){
                System.debug('#### error: '+e);
            }
        }
    }

    @isTest
    static void testupdateStatusFromPayment(){
        System.runAs(adminUser){
            
            
            Test.startTest();
            List<sofactoapp__R_glement__c> lstPaymt = new List<sofactoapp__R_glement__c>{
                new sofactoapp__R_glement__c(
                    sofactoapp__Facture__c = lstFacturesClients[0].Id
                    , sofactoapp__Montant__c = 22
                    , sofactoapp__Mode_de_paiement__c = 'Chèque'
                    , statut_du_paiement__c = 'Collecté'
                    , sofactoapp__En_attente__c = false
                    , sofactoapp_numeroro_de_Cheque__c = 'Ghyjh87654'
                    ,sofactoapp_Nom_de_la_banque__c = 'Banque Populaire'
                )
                
            };
            insert lstPaymt;

            AP53_UpdateServiceContratStatus.updateStatusFromPayment(lstPaymt);
            Test.stopTest();
        }
    }

    @isTest
    static void testUpdateStatutVE(){
        System.runAs(adminUser){
            
            ContractLineItem cli = TestFactory.createContractLineItem(lstServCon[1].Id, lstPrcBkEnt[0].Id, 100, 50);
            cli.AssetId = lstTestAssset[0].Id;
            insert cli;
            ContractLineItem cli2 = TestFactory.createContractLineItem(lstServCon[3].Id, lstPrcBkEnt[0].Id, 110, 50);
            cli2.AssetId = lstTestAssset[0].Id;
            cli2.VE_Status__c = 'Complete';
            insert cli2;


            Test.startTest();
            lstServCon[1].Statut_VE__c = 'VE non nécessaire';
            lstServCon[1].Contrat_signe_par_le_client__c = true;
            lstServCon[3].Statut_VE__c = 'VE effectuée';
            lstServCon[3].Contrat_signe_par_le_client__c = true;
            // lstServCon[1].StartDate = System.Today() + 4;


            // update lstServCon[1];
            update lstServCon;
            Test.stopTest();
        }
    }

    
}