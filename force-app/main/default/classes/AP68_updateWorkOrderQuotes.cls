/**
 * @File Name          : AP68_updateWorkOrderQuotes.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08-25-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/13/2020   RRJ     Initial Version
**/
public with sharing class AP68_updateWorkOrderQuotes {
    
    public static void updateWorkOrderQuote(List<Id> lstWos, Map<Id, Id> mapQuoteToNewWo){
        System.debug('## updateWorkOrderQuote start');
        System.debug('## lstWos ' + lstWos);
        System.debug('## mapQuoteToNewWO: ' + mapQuoteToNewWo);

        List<Quote> lstQuotesToUpd = new List<Quote>(); 
        List<Opportunity> lstOppToUpdate = new List<Opportunity>();
        List<ProductRequestLineItem> lstPrliToUpdate = new List<ProductRequestLineItem>();
        Map<Id, ProductRequest> mapProductRequestToUpd = new Map<Id, ProductRequest>();

        //ajouter le WO sur le devis 
        for(Id qId : mapQuoteToNewWo.keySet()){
            lstQuotesToUpd.add(
                new Quote(
                    Id= qId,
                    Ordre_d_execution__c = mapQuoteToNewWo.get(qId)
                )
            
            );
        }

        System.debug('## lstQuotesToUpd ' + lstQuotesToUpd);

        if(!lstQuotesToUpd.isEmpty()){
            update lstQuotesToUpd; 
        }

        List<Quote> lstQuotes = [SELECT OpportunityId FROM Quote WHERE Id IN :lstQuotesToUpd AND OpportunityId <> null AND RecordType.DeveloperName = 'Devis_standard'];
        for(Quote q : lstQuotes){
            lstOppToUpdate.add(
                new Opportunity(
                    Id = q.OpportunityId
                    ,Ordre_d_execution__c = mapQuoteToNewWO.get(q.Id)
                )
            );
        }

        System.debug('## lstOppToUpdate ' + lstOppToUpdate);

        if(!lstOppToUpdate.isEmpty()){
            update lstOppToUpdate;
        }

        List<ProductRequestLineItem> lstPrli = [SELECT Id, ParentId, Quote__c FROM ProductRequestLineItem WHERE Quote__c IN :lstQuotesToUpd];
        for(ProductRequestLineItem prli: lstPrli){
            System.debug('## curr Prli: ' + prli);
            lstPrliToUpdate.add(
                new ProductRequestLineItem(
                    Id = prli.Id, 
                    WorkOrderId = mapQuoteToNewWO.get(prli.Quote__c)
                )
            );

            if(prli.ParentId != null){
                mapProductRequestToUpd.put(prli.ParentId, 
                    new ProductRequest(
                        WorkOrderId = mapQuoteToNewWO.get(prli.Quote__c),
                        Id = prli.ParentId
                    )
                );
            }   
            
        }

        System.debug('## lstPrliToUpdate : ' + lstPrliToUpdate);
        System.debug('## mapProductRequestToUpd ' + mapProductRequestToUpd);
        if(!lstPrliToUpdate.isEmpty()){
            update lstPrliToUpdate;
        }
        
        if(!mapProductRequestToUpd.isEmpty()){
            update mapProductRequestToUpd.values();
        }

        //create product required: 
        if(!lstQuotes.isEmpty()){
            AP30_QuoteProductManagement.createRequiredProducts(lstQuotes);
        }        
    }    
}