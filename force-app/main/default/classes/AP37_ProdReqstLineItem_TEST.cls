/**
 * @File Name          : AP37_ProdReqstLineItem_TEST.cls
 * @Description        : 
 * @Author             : SHU
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         04-11-2019     		     SHU        Initial Version
**/
@ isTest
public with sharing class AP37_ProdReqstLineItem_TEST {

    static User mainUser;
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
	static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
	static PricebookEntry PrcBkEnt = new PricebookEntry();
    static  Bypass__c userBypass;
    static List<Order> lstOrder = new List<Order>();
    static List<ProductRequestLineItem > lstPRLI = new List<ProductRequestLineItem >();
    static List<ProductRequest> lstProdRequest = new List<ProductRequest>();
    
    static{
        mainUser = TestFactory.createAdminUser( 'AP37@test.COM', TestFactory.getProfileAdminId() );
        insert mainUser;

        System.runAs(mainUser){
            //create account
            testAcc = TestFactory.createAccountBusiness('OrderAccount');
            // testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //create products
            prod = TestFactory.createProduct('testProd');
            insert prod;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2( Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;
            
            //Create product
            Product2 prod = new Product2(Name = 'Product X',
                                        ProductCode = 'Pro-X',
                                        isActive = true,
                                        Famille_d_articles__c = 'Pièces détachées',
                                        Sous_familles_d_articles__c = 'PAC',
                                         Statut__c = 'Approuvée',
                                        RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()
            );
            insert prod;

            //Create Orders
            lstOrder = new List<Order> {
                new Order( AccountId = testAcc.Id
                          ,Name = 'Test1'
                          ,EffectiveDate = System.today()
                          ,Status = 'Demande d\'achats' //To add to AP_Constants
                          ,Pricebook2Id = lstPrcBk[0].Id
                          ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId()),
                new Order( AccountId = testAcc.Id
                          ,Name = 'Test2'
                          ,EffectiveDate = System.today()
                          ,Status = 'Demande_d_achats_envoyee' //To add to AP_Constants
                          ,Pricebook2Id = lstPrcBk[0].Id
                          ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId()),
                new Order( AccountId = testAcc.Id
                          ,Name = 'Test3'
                          ,EffectiveDate = System.today()
                          ,Status = 'Demande_d_achats_envoyee' //To add to AP_Constants
                          ,Pricebook2Id = lstPrcBk[0].Id
                          ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId())
            };     
            insert lstOrder;

            lstProdRequest = new List<ProductRequest> {
                new ProductRequest(),
                new ProductRequest(),
                new ProductRequest()
            };  
            insert lstProdRequest;          

            lstPRLI = new List<ProductRequestLineItem > {
                //Order0 0-2
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[0].Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Transféré'
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[0].Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[0].Id),
                //Order1 3-5
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[1].Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[1].Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[1].Id), 
                //Order2 6-8
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'En cours de commande'
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[2].Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Transféré'
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[2].Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'En cours de commande'
                                            ,Commande__c = lstOrder[2].Id)                                                                                    
            };                   
            insert lstPRLI;

        }
    }

    @isTest
    public static void testSetOdersAnnule(){
        System.runAs(mainUser){
            lstOrder[0].Status = label.ORD_Annule;
            lstOrder[2].Status = label.ORD_Annule;

            Test.startTest();
            update lstOrder;
            Test.stopTest();

            for( ProductRequestLineItem prli1  : [SELECT Id, Status, Commande__c  FROM ProductRequestLineItem where Commande__c IN (:lstOrder[0].Id,:lstOrder[2].Id)] ){
                system.assertEquals(prli1.Status, 'A commander');
            }
            for( ProductRequestLineItem prli1  : [SELECT Id, Status, Commande__c  FROM ProductRequestLineItem where Commande__c = :lstOrder[1].Id] ){
                system.assertEquals(prli1.Status, 'En cours de commande');
            }
            //system.assertEquals(Actual, Expected);
        }
    }
}