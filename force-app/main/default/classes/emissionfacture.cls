global  class emissionfacture implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
        
    global Integer recordsProcessed = 0;
   	global String [] email = new String[] {'souleymane12@gmail.com'};
        
    global Database.QueryLocator start(Database.BatchableContext bc) {
             
             return Database.getQueryLocator(
            [SELECT Id, sofactoapp__Etat__c
    			FROM sofactoapp__Factures_client__c
    			WHERE sofactoapp__Etat__c = 'Brouillon'
            ]);
    }
    global void execute(Database.BatchableContext bc, List<sofactoapp__Factures_Client__c> scope){
        // process each batch of records
        List<sofactoapp__Factures_Client__c> Fact2update = new List<sofactoapp__Factures_Client__c>();
        for (sofactoapp__Factures_Client__c fact : scope) {
                fact.sofactoapp__Etat__c = 'Emise';
            
                
                // add the facture to list to be updated
                Fact2update.add(fact);
            	System.debug('### reglement  processed');
                // increment the instance member counter
                recordsProcessed = recordsProcessed + 1;
            
        }
        
        try{
            System.debug('### in upd'+ recordsProcessed);
            update Fact2update;
        }catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
        }
       } 
    
    
    global static String scheduleBatch() {
        emissionfacture scheduler = new emissionfacture();
        //String scheduleTime = '0 0 00 * * ?';
        //return System.schedule('Batch:', scheduleTime, scheduler);
        return System.schedule('Batch Update facture :' + Datetime.now().format(),  '0 0 0 * * ?', scheduler);
    
    	}
        
        global void execute(SchedulableContext sc){
        Database.executeBatch(new emissionfacture());
    	}  
   
  
        global void finish(Database.BatchableContext bc){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      
      // Below code will fetch the job Id
      AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors,
      a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById,
      a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];
      
      // get the job Id
      System.debug('$$$ Jobid is'+BC.getJobId());
      
      // below code will send an email to User about the status
      mail.setToAddresses(email);
      mail.setReplyTo('s.sidahmed@sofacto.com'); // Add here your email address
      mail.setSenderDisplayName('Apex Batch updte reglement Module');
      mail.setSubject('Batch Processing '+a.Status);
      mail.setPlainTextBody('The Batch Apex job processed'
         + a.TotalJobItems+'batches with '+a.NumberOfErrors+'failures'+'Job Item processed are'+a.JobItemsProcessed);
      
      Messaging.sendEmail(new Messaging.Singleemailmessage [] {mail});

            }
}