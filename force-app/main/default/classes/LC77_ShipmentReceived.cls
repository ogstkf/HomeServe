/**
 * @description       : 
 * @author            : Spoon Consulting
 * @group             : 
 * @last modified on  : 09-02-2021
 * @last modified by  : MNA
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   09-02-2021   MNA								   Initial Version
**/
public without sharing class LC77_ShipmentReceived {
    @AuraEnabled
    public static void recu(Id shipId) {
        List<ProductTransfer> lstProTtoUpdate = new List<ProductTransfer>();
        List<ProductTransfer> lstProT = [SELECT id, quantity_lot_received__c, IsReceived FROM ProductTransfer WHERE ShipmentId =: shipId];
        for (ProductTransfer proT : lstProT) {
            if (proT.isReceived != true) {
                if (proT.quantity_lot_received__c == null) 
                    proT.quantity_lot_received__c = 0;
                proT.isReceived = true;

                lstProTtoUpdate.add(proT);
                system.debug(proT);
            }
        }
        if(lstProTtoUpdate.size() > 0 ) 
            update lstProTtoUpdate;
    }
}