/**
 * @File Name          : AP12_ServiceAppointmentRules.cls
 * @Description        : Class for populating service appointment fields before insert/update
 * @Author             : RRJ
 * @Group              : Spoon Consulting
 * @Last Modified By   : LGO
 * @Last Modified On   : 21-01-2022
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    22/08/2019, 15:38:21   RRJ     Initial Version
**/
public without sharing class AP12_ServiceAppointmentRules {
    public static void populateCategory(Set<Id> setWoId) {
        List<ServiceAppointment> lstSAToUpdt = new List<ServiceAppointment>();
		Map<Id, WorkOrder> mapWO = new Map<Id, WorkOrder>([SELECT Id,  CaseId,Case.Service_Contract__r.RecordTypeId, Case.Service_Contract__r.RecordType.DeveloperName,
                                                           Case.Service_Contract__r.Type__c, WorkType.Type__c, WorkOrderNumber, WorkTypeId, case.Contract_Line_Item__c,
                                                           Case.Service_Resource_Level__c, Case.Asset.Logement__c, Case.Service_Contract__c, Case.Service_Contract__r.Logement__c,
                                                           Case.Type_de_collectif_WT__c, IsGeneratedFromMaintenancePlan, Clientnoncontacte__c
                                                           			FROM WorkOrder WHERE Id IN :setWoId]);
        
        for (ServiceAppointment SA: [SELECT Id, TECH_WorkType_Type__c, Category__c, TECH_ServiceContractType__c FROM ServiceAppointment WHERE ParentRecordId IN :setWoId]) {               
            system.debug('** populateCategory 1'); 
           if(SA.TECH_WorkType_Type__c == 'Commissioning')
                SA.Category__c = 'Mise en service';
            
            else if(SA.TECH_WorkType_Type__c == 'Installation')
                SA.Category__c = 'Pose';
            
            else if(SA.TECH_WorkType_Type__c == 'Maintenance'){                
                system.debug('** populateCategory 2'); 
                if(SA.TECH_ServiceContractType__c == 'Collective'){
                    system.debug('** populateCategory 3'); 
                    SA.Category__c = 'VE Collective';
                }
                if(SA.TECH_ServiceContractType__c == 'Individual'){
                    system.debug('** populateCategory 4'); 
                    SA.Category__c = 'VE Individuelle';
                }
            }
                
            else if(SA.TECH_WorkType_Type__c == 'Troubleshooting' || SA.TECH_WorkType_Type__c == 'Contrôle' )
                SA.Category__c = 'Depannage';
                
            else if(SA.TECH_WorkType_Type__c == 'Various Services' || SA.TECH_WorkType_Type__c == 'Information request')
                SA.Category__c = 'Divers';
                 
            lstSAToUpdt.add(SA);
        }
        
        if (!lstSAToUpdt.isEmpty()) 
            update lstSAToUpdt;
        
        
    }

    public static void populateClientNonContact(Set<Id> setWoId) {
        List<ServiceAppointment> lstSAToUpdt = new List<ServiceAppointment>();
        for (ServiceAppointment SA : [SELECT Id, Clientnoncontacte__c, Work_Order__r.Clientnoncontacte__c FROM ServiceAppointment
                                      		WHERE ParentRecordId IN :setWoId AND Work_Order__r.Clientnoncontacte__c = true]) {
            SA.Clientnoncontacte__c = SA.Work_Order__r.Clientnoncontacte__c;
            lstSAToUpdt.add(SA);                              
        }
        if (!lstSAToUpdt.isEmpty())
            update lstSAToUpdt;
    }
    
    public static void populateBeforeUpdate(List<ServiceAppointment> lstOldSa, List<ServiceAppointment> lstNewSa){
        system.debug('Entered RSA');
        Map<Id, Id> mapWSaIdToWo = new Map<Id, Id>();
        Map<Id, ServiceAppointment> mapSa = new Map<Id, ServiceAppointment>(lstNewSa);
        Map<Id, WorkOrder> mapWO = new Map<Id, WorkOrder>();
        Map<Id, Id> mapSAWO = new Map<Id, Id>();

        for(Integer i=0; i<lstNewSa.size(); i++){
            if(lstNewSa[i].TECH_Cham_Digital__c == false && lstNewSa[i].Work_Order__c != null && lstOldSa[i].Work_Order__c == null){
                system.debug('** populateBeforeUpdate 2'); 
                mapWSaIdToWo.put(lstNewSa[i].Id, lstNewSa[i].Work_Order__c);
            }         
        }

        if(mapWSaIdToWo.size()>0){
            mapWO = new Map<Id, WorkOrder>([SELECT Id, 
            CaseId,Case.Service_Contract__r.RecordTypeId, Case.Service_Contract__r.RecordType.DeveloperName
            ,Case.Service_Contract__r.Type__c, WorkType.Type__c, WorkOrderNumber, WorkTypeId, case.Contract_Line_Item__c, Case.Service_Resource_Level__c, Case.Asset.Logement__c,
             Case.Service_Contract__c, Case.Service_Contract__r.Logement__c, Case.Type_de_collectif_WT__c, IsGeneratedFromMaintenancePlan, Clientnoncontacte__c FROM WorkOrder WHERE Id IN :mapWSaIdToWo.values()]);
        }
        for (String key: mapWO.KeySet()){
            System.debug(key);
        }


        for(Integer i=0; i<lstNewSa.size(); i++){
            system.debug('** populateBeforeUpdate 1');            
            // if(lstNewSa[i].TECH_Cham_Digital__c == false && lstNewSa[i].Work_Order__c != null && lstOldSa[i].Work_Order__c == null){
            //     system.debug('** populateBeforeUpdate 2'); 
            //     mapWSaIdToWo.put(lstNewSa[i].Id, lstNewSa[i].Work_Order__c);
            // }    
            system.debug('** populateBeforeUpdate 1 new '+lstNewSa[i].TECH_WorkTypeId__c); 
            system.debug('** populateBeforeUpdate 1 old '+lstOldSa[i].TECH_WorkTypeId__c); 
            system.debug('** populateBeforeUpdate 1 '+lstNewSa[i].Work_Order__c); 
                    
            
            if(lstNewSa[i].TECH_WorkTypeId__c != null && lstNewSa[i].Work_Order__c != null && lstNewSa[i].TECH_WorkTypeId__c != lstOldSa[i].TECH_WorkTypeId__c){
                system.debug('** populateBeforeUpdate 3'); 
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Commissioning'){
                    lstNewSa[i].Category__c = 'Mise en service';
                }       
                //if(lstNewSa[i].TECH_WorkType_Type__c == 'Commissioning'){
                  //  lstNewSa[i].Category__c = 'Commissioning';
                //}                           
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Installation'){
                    lstNewSa[i].Category__c = 'Pose';
                }

                //Added DMU 20200925
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Maintenance'){
                    /*system.debug('** populateBeforeUpdate 4' + mapWO.get(lstNewSa[i].ParentRecordId).WorkOrderNumber ); 
                    system.debug('** populateBeforeUpdate 4-1' + mapWO.get(lstNewSa[i].ParentRecordId).WorkTypeId ); 
                    system.debug('** populateBeforeUpdate 4-2' + mapWO.get(lstNewSa[i].ParentRecordId).WorkType.Type__c ); 
                    system.debug('** populateBeforeUpdate 4-3' + mapWO.get(lstNewSa[i].ParentRecordId).Case.Service_Contract__r.Type__c ); 
                    system.debug('** populateBeforeUpdate 4-4' + mapWO.get(lstNewSa[i].ParentRecordId).CaseId ); 
                    system.debug('** populateBeforeUpdate 4-5' + mapWO.get(lstNewSa[i].ParentRecordId).Case.Service_Contract__c );
                    system.debug('** populateBeforeUpdate 4-6' + mapWO.get(lstNewSa[i].ParentRecordId).Case.Service_Contract__r.RecordTypeId ); 
                    system.debug('** populateBeforeUpdate 4-7' + mapWO.get(lstNewSa[i].ParentRecordId).Case.Service_Contract__r.RecordType.DeveloperName ); */

                    //Set Category to Individual by default if ServiceContract==null
                    lstNewSa[i].Category__c = 'VE Individuelle';

                    /*if(mapWO.get(lstNewSa[i].ParentRecordId).Case.Service_Contract__r.Type__c == 'Collective'){
                        system.debug('** populateBeforeUpdate 5'); 
                            lstNewSa[i].Category__c = 'VE Collective';
                    }
                    if(mapWO.get(lstNewSa[i].ParentRecordId).Case.Service_Contract__r.Type__c == 'Individual'){
                        system.debug('** populateBeforeUpdate 6'); 
                        lstNewSa[i].Category__c = 'VE Individuelle';
                    }*/

                    if (mapWO.get(lstNewSa[i].ParentRecordId).Case.Type_de_collectif_WT__c == null) {
                        lstNewSa[i].Category__c = 'VE Individuelle';
                    }
                    if (mapWO.get(lstNewSa[i].ParentRecordId).Case.Type_de_collectif_WT__c <> null 
                        && (lstNewSa[i].TECH_WorkTypeReason__c == 'Visite sous contrat' || lstNewSa[i].TECH_WorkTypeReason__c == 'Visite hors contrat')) {
                            lstNewSa[i].Category__c = 'VE Collective';
                    }
                }
                
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Maintenance'){
                    system.debug('** populateBeforeUpdate 4'); 
                    if(lstNewSa[i].TECH_ServiceContractType__c == 'Collective'){
                        system.debug('** populateBeforeUpdate 5'); 
                        lstNewSa[i].Category__c = 'VE Collective';
                    }
                    if(lstNewSa[i].TECH_ServiceContractType__c == 'Individual'){
                        system.debug('** populateBeforeUpdate 6'); 
                        lstNewSa[i].Category__c = 'VE Individuelle';
                    }
                }
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Troubleshooting' || lstNewSa[i].TECH_WorkType_Type__c == 'Contrôle' ){
                    lstNewSa[i].Category__c = 'Depannage';
                }
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Various Services' || lstNewSa[i].TECH_WorkType_Type__c == 'Information request'){
                    lstNewSa[i].Category__c = 'Divers';
                }
            }
        }//end loop

        // if(mapWSaIdToWo.size()>0){
        //     mapWO = new Map<Id, WorkOrder>([SELECT Id, case.Contract_Line_Item__c, Case.Service_Resource_Level__c, Case.Asset.Logement__c, Case.Service_Contract__c, Case.Service_Contract__r.Logement__c, IsGeneratedFromMaintenancePlan FROM WorkOrder WHERE Id IN :mapWSaIdToWo.values()]);
        // }

        for(Id saId: mapWSaIdToWo.keySet()){
            if(mapWO.containsKey(mapWSaIdToWo.get(saId))){
                if(!mapWo.get(mapWSaIdToWo.get(saId)).IsGeneratedFromMaintenancePlan){
                    mapSa.get(saId).Residence__c = mapWO.get(mapWSaIdToWo.get(saId)).Case.Asset.Logement__c != null ? mapWO.get(mapWSaIdToWo.get(saId)).Case.Asset.Logement__c : null;
                }
                mapSa.get(saId).Service_Contract__c = mapWO.get(mapWSaIdToWo.get(saId)).Case.Service_Contract__c != null ? mapWO.get(mapWSaIdToWo.get(saId)).Case.Service_Contract__c : null;
                mapSa.get(saId).Lot__c = mapWO.get(mapWSaIdToWo.get(saId)).Case.Service_Contract__r.Logement__c != null && mapSa.get(saId).TECH_ServiceContractType__c == 'Collective' ? mapWO.get(mapWSaIdToWo.get(saId)).Case.Service_Contract__r.Logement__c : null;
				//ADDED BY Ravi NADARADJANE on 2020-09-17 (PB_WORKORDER_V1 - APEX Cpu Time Limit Exceeded)
                mapSa.get(saId).El_ment_de_ligne_de_contrat__c = mapWo.get(mapWSaIdToWo.get(saId)).Case.Contract_Line_Item__c != null ? mapWo.get(mapWSaIdToWo.get(saId)).Case.Contract_Line_Item__c : null;
                mapSa.get(saId).Service_Resource_Level__c = mapWo.get(mapWSaIdToWo.get(saId)).Case.Service_Resource_Level__c != null ? mapWo.get(mapWSaIdToWo.get(saId)).Case.Service_Resource_Level__c : null;

            }
        }
    }

    public static void populateBeforeInsert(List<ServiceAppointment> lstNewSA){
        system.debug('*** in populateBeforeInsert');
        //MNA 18/02/2021
        Set<Id> SetWoIds = new Set<Id>();
        Map<Id, WorkOrder> mapWO = new Map<Id, WorkOrder>();
        for (ServiceAppointment sa : lstNewSA) {
            if (string.valueOf(sa.ParentRecordId).startsWith('0WO'))
                    SetWoIds.add(sa.ParentRecordId);
        }
        for(WorkOrder wo :[SELECT Id, Case.Type_de_collectif_WT__c FROM WorkOrder WHERE Id IN :SetWoIds]) {
            mapWO.put(wo.Id, wo);
        }

        for(Integer i=0; i<lstNewSa.size(); i++){
            system.debug('*** in pbI 1');
            if(lstNewSA[i].ParentRecordId != null && (string.valueOf(lstNewSA[i].ParentRecordId)).startsWith('0WO')){  //lstNewSA[i].TECH_WorkTypeId__c != null && 
                system.debug('*** in pbI 2');
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Commissioning'){
                    system.debug('*** in pbI 3');
                    lstNewSa[i].Category__c = 'Mise en service';
                }                
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Installation'){
                    system.debug('*** in pbI 4');
                    lstNewSa[i].Category__c = 'Pose';
                }
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Maintenance'){
                    system.debug('*** in pbI 5');
                    /*if(lstNewSa[i].TECH_ServiceContractType__c == 'Collective'){
                        system.debug('*** in pbI 6');
                        lstNewSa[i].Category__c = 'VE Collective';
                    }
                    if(lstNewSa[i].TECH_ServiceContractType__c == 'Individual'){
                        system.debug('*** in pbI 7');
                        lstNewSa[i].Category__c = 'VE Individuelle';
                    }*/
                    if(mapWo.get(lstNewSa[i].ParentRecordId).Case.Type_de_collectif_WT__c == null){
                        lstNewSa[i].Category__c = 'VE Individuelle';
                    }
                    if (mapWo.get(lstNewSa[i].ParentRecordId).Case.Type_de_collectif_WT__c <> null 
                            && (lstNewSa[i].TECH_WorkTypeReason__c == 'Visite sous contrat' || lstNewSa[i].TECH_WorkTypeReason__c == 'Visite hors contrat')) {

                        lstNewSa[i].Category__c = 'VE Collective';
                    }
                }
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Troubleshooting' || lstNewSa[i].TECH_WorkType_Type__c == 'Contrôle' ){
                    lstNewSa[i].Category__c = 'Depannage';
                }
                if(lstNewSa[i].TECH_WorkType_Type__c == 'Various Services' || lstNewSa[i].TECH_WorkType_Type__c == 'Information request'){
                    lstNewSa[i].Category__c = 'Divers';
                }
            }            
        }
    }
    
    //ADDED BY Ravi NADARADJANE on 2020-09-17 (PB_WORKORDER_V1 - APEX Cpu Time Limit Exceeded)
    public static void updateCLISA(List<ServiceAppointment> lstNewSa){
        system.debug('*** updateCLISA');
        List<ServiceAppointment> lstSAToUpd = new List<ServiceAppointment>();

        map<Id, Id> mapSAidWOId = new map <Id, Id>();
        map<Id, WorkOrder> mapWo = new map <Id, WorkOrder>();
        
        for(ServiceAppointment sa : lstNewSa){
             mapSAidWOId.put(sa.Id, sa.ParentRecordId);
        }
        
        for(WorkOrder wo : [SELECT id, Case.Contract_Line_Item__c, ServiceContractId, Case.Service_Resource_Level__c FROM WorkOrder WHERE Id in: mapSAidWOId.values()]){
             mapWo.put(wo.Id, wo);
        }
        
        for(ServiceAppointment sa : lstNewSa){
            if(mapWo.containsKey(sa.ParentRecordId)){
                sa.Service_Contract__c = mapWo.get(sa.ParentRecordId).ServiceContractId;
                sa.El_ment_de_ligne_de_contrat__c = mapWo.get(sa.ParentRecordId).Case.Contract_Line_Item__c;
                sa.Service_Resource_Level__c = mapWo.get(sa.ParentRecordId).Case.Service_Resource_Level__c!=null?mapWo.get(sa.ParentRecordId).Case.Service_Resource_Level__c:'Junior'; //UPDATED BY Ravi N. ON 2020-09-21
            }
        }
    }
    
    // public static void updateCLISA(List<ServiceAppointment> lstNewSa){
    //     List<ServiceAppointment> lstSAToUpd = new List<ServiceAppointment>();

    //     map<Id, Id> mapSAidWOId = new map <Id, Id>();
    //     map<Id, Id> mapWoIdCLIid = new map <Id, Id>();

    //     for(ServiceAppointment sa : lstNewSa){
    //         mapSAidWOId.put(sa.Id, sa.ParentRecordId);
    //     }
        
    //     for(WorkOrder wo : [SELECT id, Case.Contract_Line_Item__c FROM WorkOrder WHERE id in: mapSAidWOId.values()]){
    //         mapWoIdCLIid.put(wo.Id, wo.Case.Contract_Line_Item__c);
    //     }

    //     for(ServiceAppointment sa : lstNewSa){
    //         lstSAToUpd.add(new ServiceAppointment(id=sa.Id, El_ment_de_ligne_de_contrat__c=mapWoIdCLIid.get(sa.ParentRecordId))); //sa.TECH_WOCaseCLI__c
    //     }

    //     if(lstSAToUpd.size()>0){
    //         update lstSAToUpd;
    //     }
    // }

    public static void populateLogement( map<string, string> mapSAWO, List<ServiceAppointment> lstNewSa){
        system.debug('Entered populateLogement');

        map<String, WorkOrder> mapWO  = new map <String, WorkOrder>();
        
        for(WorkOrder wo :[SELECT id, case.Contract_Line_Item__c, Case.Service_Resource_Level__c, TECH_LogementId__c, Asset.Logement__c, IsGeneratedFromMaintenancePlan FROM WorkOrder WHERE id IN: mapSAWO.values()]){
            mapWO.put(wo.Id, wo);            
        }      
        system.debug('**mapWO: '+mapWO.size());

        for(ServiceAppointment sa : lstNewSa){            
            sa.Residence__c = mapWO.containsKey(sa.ParentRecordId) ? (mapWO.get(sa.ParentRecordId).IsGeneratedFromMaintenancePlan ? mapWO.get(sa.ParentRecordId).Asset.Logement__c : null) : null;            
        }
    }

}