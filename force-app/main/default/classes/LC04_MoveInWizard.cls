/**
* @File Name          : LC04_MoveInWizard.cls
* @Description        : Apex controller for LC04_MoveInWizard
* @Author             : SH
* @Group              : 
* @Last Modified By   : SH
* @Last Modified On   : 03/01/2020, 13:45:26
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      		      Modification
*==============================================================================
* 1.0    03/01/2020, 10:55:28   SH     Initial Version
**/
public class LC04_MoveInWizard {

    @AuraEnabled
    public static List<Logement__c> search(String clientAddress){
        System.debug ('##### clientAddress: ' + clientAddress);

        clientAddress = String.escapeSingleQuotes(clientAddress).trim().replace(' ','%');
        List<Logement__c> lstData = new List<Logement__c>();

        if (clientAddress.length() > 3) {
            clientAddress = '%' + clientAddress + '%';
            lstData = [
                SELECT Id, Name, Street__c, Postal_Code__c, City__c, Door__c, Floor__c, Inhabitant__c, Agency__c , Agency__r.Compte_pour_VACANT__c
                FROM Logement__c 
                WHERE Address__c LIKE :clientAddress
                LIMIT 50];
        }
        System.debug ('lstData : ' + lstData);
        System.debug ('lstData.size : ' + lstData.size());
        return lstData;
    }

    @AuraEnabled
    public static Map<String, List<PicklistEntryWrapper>> getPicklists(String pickFlds){
        // System.debug ('##### pickFlds: '+pickFlds);
        List<PicklistEntryWrapper> mapObjFld = (List<PicklistEntryWrapper>)JSON.deserialize(pickFlds, List<PicklistEntryWrapper>.class);
        Map<String, List<PicklistEntryWrapper>> mapPicklists = new Map<String, List<PicklistEntryWrapper>>();
        for(PicklistEntryWrapper objName : mapObjFld){
            List<PicklistEntryWrapper> lstPicklistValPair = new List<PicklistEntryWrapper>();
            Schema.DescribeSObjectResult res = Schema.getGlobalDescribe().get(objName.label).getDescribe();
            Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(objName.value).getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                lstPicklistValPair.add(new PicklistEntryWrapper(pickListVal.getValue(), pickListVal.getLabel()));
            }
            mapPicklists.put(objName.label+'-'+objName.value, lstPicklistValPair);
        }
        return mapPicklists;
    }

    private class PicklistEntryWrapper{
        @AuraEnabled public String active {get;set;}
        @AuraEnabled public String defaultValue {get;set;}
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String value {get;set;}
        @AuraEnabled public String validFor {get;set;}
        @AuraEnabled public List<PicklistEntryWrapper> children {get;set;}
        public PicklistEntryWrapper(){}

        public PicklistEntryWrapper(String value, String label){
            this.value = value;
            this.label = label;
        }
    }

    public static Id getRecTypeId(String objName, String recTypeDevName){
        return Schema.getGlobalDescribe().get(objName).getDescribe().getRecordTypeInfosByDeveloperName().get(recTypeDevName).getRecordTypeId();
    }

    @AuraEnabled
    public static Map<String, String> analyseGeo(String jsonInput){
        // System.debug ('##### jsonInput: '+jsonInput);
        Map<String, String> mapResult = new Map<String, String>();
        mapResult.put('isEligible', 'false');
        Map<String, String> searchData = (Map<String, String>)JSON.deserialize(jsonInput, Map<String, String>.class);

        List<Agency_intervention_zones__c> lstZone = [SELECT Agence__c, Agence__r.name FROM Agency_intervention_zones__c WHERE Zip_Code__c = :String.escapeSingleQuotes(searchData.get('clCPLog'))];
        if(lstZone.size()>0){
            mapResult.put('isEligible', 'true');
            mapResult.put('agencyName', lstZone[0].Agence__r.name);
            mapResult.put('agencyId', lstZone[0].Agence__c);
        }
        return mapResult;
    }

    @AuraEnabled
    public static String creerLogement(String jsonInput){
        System.debug ('Créer Logement');
        System.debug ('### json input: '+jsonInput);
        Map<String, String> searchData = (Map<String, String>)JSON.deserializeStrict(jsonInput, Map<String, String>.class);

        Map<String, String> mappingObj = new Map<String, String>{
            'clCP' => 'Account', 'clRue' => 'Account', 'clVille' => 'Account', 'clPays' => 'Account', 
            'clRueLog' => 'Logement__c', 'clCPLog' => 'Logement__c', 'clVilleLog' => 'Logement__c', 'clPaysLog' => 'Logement__c', 
            'clProprietaire' => 'Logement__c', 'clOccupant' => 'Logement__c', 
            'clUsagePro' => 'Logement__c','clAgeLogement' => 'Logement__c', 
            'clAstEqpType' => 'Asset', 'clEquipementHPE' => 'Asset',
            'dateEmmenagement' => 'Compte_Logement__c'
        };
        Map<String, String> mappingFlds = new Map<String, String>{
            'clCP' => 'BillingPostalCode', 'clRue' => 'BillingStreet', 'clVille' => 'BillingCity', 'clPays' => 'BillingCountry', 
            'clRueLog' => 'street__c', 'clCPLog' => 'Postal_Code__c', 'clVilleLog' => 'City__c', 'clPaysLog' => 'Country__c',
            'clProprietaire' => 'Owner__c', 'clOccupant' => 'Inhabitant__c',
            'clUsagePro' => 'Professional_Use__c', 'clAgeLogement' => 'Age__c',
            'clAstEqpType' => 'Equipment_family__c', 'clEquipementHPE' => 'HEP_Equipment__c',
            'dateEmmenagement' => 'Start__c'
        };
        Map<String, String> mapAstRecType = new Map<String, String>{
            'Chaudière' => 'Boiler', 'Appareil hybride'=>'Hybrid_machine', 'Pompe à chaleur'=>'Heat_pump', 'Chauffe eau'=>'Water_heater',
            'Produit VMC'=>'VMC_product', 'Conduit d\'évacuation'=>'Chimney', 'Autres'=>'Others'
        };

        Account acc = [SELECT Id, BillingPostalCode, BillingStreet, BillingCity, BillingCountry, ClientNumber__c FROM Account WHERE Id = :searchData.get('idActualInhabitant')];
        acc.BillingPostalCode = null;
        acc.BillingStreet = null;
        acc.BillingCity = null;
        acc.BillingCountry = null;

        Logement__c log = new Logement__c();
        Compte_Logement__c cl = new Compte_Logement__c();
        Asset ast = new Asset();
        
        Boolean owner = false;
        Boolean inhabitant = false;
        Boolean typeEquipement = false;
        for(String dataFld : searchData.keySet()){
            if (dataFld == 'clProprietaire') { owner = true; }
            if (dataFld == 'clOccupant')     { inhabitant = true; }
            if (dataFld == 'clEqpType')      { typeEquipement = true; }

            if(mappingFlds.containsKey(dataFld) && mappingObj.containsKey(dataFld)){
                // System.debug ('###### fld: '+ mappingObj.get(dataFld)+': '+mappingFlds.get(dataFld)+' = '+searchData.get(dataFld));
                switch on mappingObj.get(dataFld) { 
                    when 'Account' { 
                        acc.put(mappingFlds.get(dataFld), searchData.get(dataFld)); 
                    }
                    when 'Logement__c' { 
                        if (searchData.get(dataFld) != 'true') { log.put(mappingFlds.get(dataFld), searchData.get(dataFld)); }
                    }
                    when 'Compte_Logement__c' {
                        if (dataFld == 'dateEmmenagement') { cl.put(mappingFlds.get(dataFld), date.valueOf(searchData.get(dataFld))); } 
                        else { cl.put(mappingFlds.get(dataFld), searchData.get(dataFld)); }
                    }
                    when 'Asset' {
                        if(searchData.get(dataFld) != 'true') { ast.put(mappingFlds.get(dataFld), searchData.get(dataFld)); }
                    }
                }
            }else{
                System.debug ('#### Field not in mapping perso: '+dataFld);
            }
        }

        // Raise errors
        if(cl.Start__c == null)             throw new AP_Constant.CustomException('La date d’emménagement est obligatoire.');

        if(log.street__c == null)           throw new AP_Constant.CustomException('La rue du Logement est obligatoire.');
        if(log.Postal_Code__c == null)      throw new AP_Constant.CustomException('Le code postal du Logement est obligatoire.');
        if(log.City__c == null)             throw new AP_Constant.CustomException('La ville du Logement est obligatoire.');

        if(!owner && !inhabitant)           throw new AP_Constant.CustomException('Propriétaire et/ou Occupant doivent être cochés.');

        if(acc.BillingStreet == null)       throw new AP_Constant.CustomException('La rue du Client est obligatoire.');
        if(acc.BillingPostalCode == null)   throw new AP_Constant.CustomException('Le code postal du Client est obligatoire.');
        if(acc.BillingCity == null)         throw new AP_Constant.CustomException('La ville du Client est obligatoire.');
        if(acc.BillingCountry == null)      throw new AP_Constant.CustomException('Le pays du Client est obligatoire.');

        if(ast.Equipment_family__c == null) throw new AP_Constant.CustomException('Le Type d’équipement est obligatoire.');

        // Update info about address for the Client
        update acc;

        // Insert Logement
        log.RecordTypeId = getRecTypeId('Logement__c', 'Logement');
        if(searchData.get('clProprietaire') == 'true')  { log.put(mappingFlds.get('clProprietaire'), acc.Id); }
        if(searchData.get('clOccupant') == 'true')      { log.put(mappingFlds.get('clOccupant'), acc.Id); }
          
        Map<String, String> mapGeo = analyseGeo(jsonInput);
        if(mapGeo.containsKey('agencyId'))              { log.Agency__c = mapGeo.get('agencyId'); }
        insert log;

        // Insert Compte/Logement
        cl.Actif__c = true;
        cl.Account__c = acc.Id;
        cl.Logement__c = log.Id;
        if(searchData.get('clOccupant') == 'true')     { cl.Role__c = 'Occupant'; }
        if(searchData.get('clProprietaire') == 'true') { cl.Role__c = 'Propriétaire'; }
        insert cl;

        // Insert Asset
        List<Product2> lstProd = [SELECT Id  FROM Product2 WHERE Brand__c = 'INCONNU' AND Equipment_type1__c = :searchData.get('clEqpType') LIMIT 1];
        if(lstProd.size() == 0) { throw new AP_Constant.CustomException('Aucun produit retrouvé pour associer à l\'équipement'); }
        ast.Product2Id = lstProd[0].Id;
        ast.AccountId = acc.Id;
        ast.Name = searchData.get('clAstEqpType')+'-'+acc.ClientNumber__c;
        ast.RecordTypeId = getRecTypeId('Asset', mapAstRecType.get(searchData.get('clAstEqpType')));
        ast.Logement__c = log.Id;
        ast.Status = 'Actif';
        ast.HEP_Equipment__c = searchData.get('clEquipementHPE') == 'true' ? true : false;
        insert ast;
        
        // Return Id of Account for final redirection
        return String.valueOf(acc.Id);
    }

    @AuraEnabled
    public static String moveInToExistingLogement(String jsonInput){
        System.debug ('Emménagement dans Logement existant');
        System.debug ('### json input: '+jsonInput);
        Map<String, String> searchData = (Map<String, String>)JSON.deserializeStrict(jsonInput, Map<String, String>.class);
        Boolean owner = searchData.containsKey('clProprietaire');
        Boolean inhabitant = searchData.containsKey('clOccupant');

        // Raise errors
        if (!searchData.containsKey('dateEmmenagement')) throw new AP_Constant.CustomException('La date d’emménagement est obligatoire.');
        if(!owner && !inhabitant)                        throw new AP_Constant.CustomException('Propriétaire et/ou Occupant doivent être cochés.');
        
        // Update Logement
        Logement__c log = [SELECT Id, Owner__c, Inhabitant__c FROM Logement__c WHERE Id = :searchData.get('idActualLogement')];
        if(searchData.get('clProprietaire') == 'true')  { log.Owner__c = searchData.get('idActualInhabitant'); }
        if(searchData.get('clOccupant') == 'true')      { log.Inhabitant__c = searchData.get('idActualInhabitant'); }
        update log;

        // Update Assets
        List<Asset> lstAsset = [SELECT Id, AccountId FROM Asset WHERE Logement__c = :searchData.get('idActualLogement')];
        for (Asset ast : lstAsset) {
            ast.AccountId = searchData.get('idActualInhabitant');
        }
        update lstAsset;

        // Insert Compte/Logement (if both are checked, insert 2 records)
        if(searchData.get('clProprietaire') == 'true') {
            Compte_Logement__c cl1 = new Compte_Logement__c();
            cl1.Actif__c = true;
            cl1.Account__c = searchData.get('idActualInhabitant');
            cl1.Logement__c = searchData.get('idActualLogement');
            cl1.Start__c = date.valueOf(searchData.get('dateEmmenagement'));
            cl1.Role__c = 'Propriétaire';
            insert cl1;
        }
        if(searchData.get('clOccupant') == 'true') {
            Compte_Logement__c cl1 = new Compte_Logement__c();
            cl1.Actif__c = true;
            cl1.Account__c = searchData.get('idActualInhabitant');
            cl1.Logement__c = searchData.get('idActualLogement');
            cl1.Start__c = date.valueOf(searchData.get('dateEmmenagement'));
            cl1.Role__c = 'Occupant';
            insert cl1;
        }

         // Return Id of Account for final redirection
        return searchData.get('idActualInhabitant');
    }
}