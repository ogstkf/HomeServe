/**
 * @File Name          : LC12_QuotesCase.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 16/12/2019, 16:19:41
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    01/11/2019   AMO     Initial Version
**/
public with sharing class LC12_QuotesCase {
    
    @AuraEnabled
    public static List<Quote> fetchQuote(Id caseId){
        Case cse = [SELECT Id, Quote__c, Opportunity__c from Case WHERE Id = :caseId LIMIT 1];

        Map<Id, Id> mapOppId = new Map<Id, Id>();
        mapOppId.put(cse.Id, cse.Opportunity__c);

        List<WorkOrder> lstWO = [SELECT Id, CaseId, Quote__c FROM WorkOrder WHERE CaseId = :cse.Id];
        List<Opportunity> lstOpp = [SELECT Id, SyncedQuoteId, Quotes__c, Tech_Synced_Devis__c FROM Opportunity WHERE Id IN :mapOppId.values()];
        // Retour UAT V1 17/12/2019 - Besoin de retirer le filtre sur les statuts de devis pour afficher tous les devis
        //List<Quote> lstQu = [SELECT Id, Ordre_d_execution__c, QuoteNumber, ExpirationDate, IsSyncing, Name FROM Quote WHERE (Ordre_d_execution__c IN :lstWO OR OpportunityId IN :lstOpp) AND (Status = 'Validé, signé et terminé' OR Status = 'Validé, signé - en attente d\'intervention') ];
        List<Quote> lstQu = [SELECT Id, Ordre_d_execution__c, QuoteNumber, ExpirationDate, IsSyncing, Name, toLabel(Status) FROM Quote WHERE (Ordre_d_execution__c IN :lstWO OR OpportunityId IN :lstOpp) ];

        return lstQu;
    }
}