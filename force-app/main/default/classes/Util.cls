/**
 * @File Name          : Util.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 07-03-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/21/2020   RRJ     Initial Version
**/
public with sharing class Util {


	//public static String getBlueAccessToken(){
	//	String token;
	//	Map<String, Object> mapReponse = new Map<String, Object>();
	//	mapReponse = getBlueAccessTokenInfo();

	//	if(mapReponse != null && mapReponse.containsKey(AP_Constant.ws_Token))
	//		token = String.valueOf( mapReponse.get(AP_Constant.ws_Token) );

	//	return token;
	//}
  public static Map<String, String> getSendSMSInfo(){
		Map<String, String> mapReponse = new Map<String, String>();
		List<WebServiceConfiguration__mdt> blueConnect = new List<WebServiceConfiguration__mdt>([SELECT Id
																							  ,Endpoint__c
																							  ,Endpoint_Access_Token__c
																							  ,Endpoint_Update_Status__c 
                                                ,Endpoint_UpdatePDF__c
																							  ,Header_Authorization__c
																							  ,Header_Cache_Control__c
																							  ,Header_Content_Type__c
																							  ,Username__c
																							  ,Password__c
																							  ,Method__c
																					   	FROM WebServiceConfiguration__mdt
																					  	WHERE DeveloperName = 'Send_SMS' AND
																					          Active__c = True]);
		System.debug('mgr blueConnect '+ blueConnect);
    mapReponse.put('endpoint', blueConnect[0].Endpoint__c);
		mapReponse.put('header_token', blueConnect[0].Endpoint_Access_Token__c);
    return mapReponse;
  }

  
  public static String generateUniqueString(){        
    String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    String returnValue = '';
    while (returnValue.length() < 6) {
       Integer i = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
       returnValue += chars.substring(i, i+1);
    }
    return returnValue;
  }


	/*
	public static Map<String, Object> getBlueAccessTokenInfo(){
		Map<String, Object> mapReponse = new Map<String, Object>();
		List<WebServiceConfiguration__mdt> blueConnect = new List<WebServiceConfiguration__mdt>([SELECT Id
																							  ,Endpoint__c
																							  ,Endpoint_Access_Token__c
																							  ,Endpoint_Update_Status__c 
                                                ,Endpoint_UpdatePDF__c
																							  ,Header_Authorization__c
																							  ,Header_Cache_Control__c
																							  ,Header_Content_Type__c
																							  ,Username__c
																							  ,Password__c
																							  ,Method__c
																					   	FROM WebServiceConfiguration__mdt
																					  	WHERE DeveloperName = 'Blue_UpdateStatus' AND
																					          Active__c = True]);
		System.debug('mgr blueConnect '+ blueConnect);

		if (blueConnect.isEmpty())
		 	return null;

		String body = AP_Constant.ws_Username + '=' + blueConnect[0].Username__c + '&' +
					  AP_Constant.ws_Password + '=' + blueConnect[0].Password__c;

		Map<String, String> mapHeader = new Map<String, String>();
		mapHeader.put(AP_Constant.ws_Authorization, blueConnect[0].Header_Authorization__c);

		HTTPResponse res = doRequest(blueConnect[0].Endpoint__c + blueConnect[0].Endpoint_Access_Token__c, 
									mapHeader,
									body,
									blueConnect[0].Method__c);

		if(res.getBody() != null){
			try{

				mapReponse = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        System.debug('mgr mapReponse:  ' + mapReponse);

			}catch(Exception e){
				System.debug('mgr Exception e ' + e);
				return null;
			}
		}

		mapReponse.put('endpoint', blueConnect[0].Endpoint__c);
		mapReponse.put('endpoint_updateStatus', blueConnect[0].Endpoint_Update_Status__c);
		mapReponse.put('header_authorization', blueConnect[0].Header_Authorization__c);
		mapReponse.put('header_cacheControl', blueConnect[0].Header_Cache_Control__c);
		mapReponse.put('header_contentType', blueConnect[0].Header_Content_Type__c);
        mapReponse.put('header_updatePDF', blueConnect[0].Endpoint_UpdatePDF__c);

		return mapReponse;
	}
  */
  
  /*
	public static HTTPResponse doRequest(String url, Map<String, String> mapHeader, String data, String method){
 		System.debug('mgr Sending payload: ' + data);
        System.debug('mgr on url: ' + url);
        HttpRequest req = new HttpRequest();

        for(String current : mapHeader.keyset()){
            req.setHeader(current, mapHeader.get(current));
            System.debug('mgr current header ' + current + ' -- ' + mapHeader.get(current));
        }

        req.setEndpoint(url);
 		req.setMethod(method);

        System.debug('mgr do request ' + req);
       
        if(data != null && data.length() > 0){
        	req.setBody(data);
        }
      	
        try{
 			Http http = new Http();
 			HTTPResponse res = http.send(req);
            System.debug('mgr res'+res);
        	
            //String response = res.getBody();
            //System.debug('mgr response '+response);
            return res;

        }catch(Exception e){
        	System.debug('mgr Exception doRequest ' + e);
			return null;      
        }
  	}
    */

    /*
  	public static map <string, WebServiceConfiguration__mdt> getSAVAccess(){
  		map <string, WebServiceConfiguration__mdt> mapSAVParams = new map <string, WebServiceConfiguration__mdt>();
  		List<WebServiceConfiguration__mdt> SAVConnect = new List<WebServiceConfiguration__mdt>([SELECT Id
  																							  ,DeveloperName 
																							  ,Endpoint__c
																							  ,Endpoint_Access_Token__c
																							  ,Endpoint_Update_Status__c
																							  ,Header_Authorization__c
																							  ,Header_Cache_Control__c
																							  ,Header_Content_Type__c
																							  ,Username__c
																							  ,Password__c
																							  ,Method__c
																					   	FROM WebServiceConfiguration__mdt
																					  	WHERE (DeveloperName = 'SAVCreateClient' OR DeveloperName = 'SAVCreateDemande')
																					  	AND Active__c = True
																					  	]);
		System.debug('## SAVConnect '+ SAVConnect);
		for(WebServiceConfiguration__mdt wsf : SAVConnect){
			mapSAVParams.put(wsf.DeveloperName, wsf);
		}

		return mapSAVParams;
  	}*/

    /*
    public Static String getImageURL(ContentVersion contentVersion, Id contentDocumentId){
     List<ContentDistribution> cdList = new List<ContentDistribution>([SELECT ID,
                                                                                 DistributionPublicUrl,
                                                                                 ContentDownloadURL  
                                                                          FROM ContentDistribution 
                                                                          WHERE ContentVersionId = : contentVersion.Id AND 
                                                                                ContentDocumentId =: contentDocumentId]);
        if(cdList.size()>0){
          return cdList.get(0).ContentDownloadUrl;
        } else {
            ContentDistribution newItem = new ContentDistribution();
            newItem.Name = contentVersion.Title;
            newItem.ContentVersionId = contentVersion.Id;
            newItem.PreferencesAllowViewInBrowser= true;
            newItem.PreferencesLinkLatestVersion=true;
            newItem.PreferencesNotifyOnVisit=false;
            newItem.PreferencesPasswordRequired=false;
            newItem.PreferencesAllowOriginalDownload= true;
            Insert NewItem;
            
            newItem = [SELECT ID,
                              DistributionPublicUrl, 
                              ContentDownloadURL,
                              PdfDownloadUrl
                       FROM ContentDistribution               
                       WHERE Id =: newItem.Id];

            System.debug('mgr new ' + newItem);

            return newItem.DistributionPublicUrl;
        }
    }
    */
    
    /*
   @future(callout=true) 
   public static void fileUploadCallout(Blob file_body, String fileName, Id leadId, string orderIDBlue) 
   {
    System.debug('#### starting fileUploadCallout');

    map<String, Object> mapToken = new Map<String, Object>();
    mapToken = Util.getBlueAccessTokenInfo();
    System.debug('#### mapToken 1st endpt: '+ mapToken.get('endpoint'));
    System.debug('#### mapToken endpoint pdf: '+ mapToken.get('header_updatePDF'));
    System.debug('#### mapToken token: '+ String.valueOf(mapToken.get(AP_Constant.ws_Token))); 
    System.debug('#### mapToken final: '+  mapToken.get('endpoint')+mapToken.get('header_updatePDF')+orderIDBlue+'/files'); 

    String endpoint = mapToken.get('endpoint')+''+mapToken.get('header_updatePDF')+orderIDBlue+'/files';
    //'https://preprod-cham.hellocasa.io/api/v2.2/order-groups/'+orderIDBlue+'/files';
                       
    //if(!Test.isRunningTest()) {
    //  endpoint += '&id=' + contractId +'&j_token=' + tokenId;
    //}
    
    System.debug('###endpoint ' + endpoint);
    //callout ePOR service
    string contentType = FileMultiparts.GetContentType();
    System.debug('###contentType ' + contentType);

    //  Compose the form
    string form64 = '';
    //******This was the boundary I was missing****
    form64 += FileMultiparts.WriteBoundary();
    form64 += FileMultiparts.WriteBlobBodyParameter('proAttestation', EncodingUtil.base64Encode(file_body), fileName);
    System.debug('###form64 ' + form64);

    blob formBlob = EncodingUtil.base64Decode(form64);
    string contentLength = string.valueOf(formBlob.size());

    System.debug('Callout Log 3:' + formBlob.size());
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint(endpoint);
    req.setBodyAsBlob(formBlob);
    //req.setHeader('apiKey', xAPIKey);
    //req.setHeader('vertid', verticalId);
    //req.setHeader('callback', callbackURL);
     
    //req.setHeader('Connection', 'keep-alive');
    //req.setHeader('Content-Length', contentLength);
    req.setHeader('Content-Type', contentType);
    //req.setHeader('Content-Type', 'multipart/form-data; boundary=1ff13444ed8140c7a32fc4e6451aa76d');
    System.debug('## authau: '+mapToken.get('header_authorization'));
    req.setHeader('Authorization', string.valueOf(mapToken.get('header_authorization')));
    //req.setHeader('X-Auth-Token', 'eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX0FQSV9DSEFNIl0sInVzZXJuYW1lIjoiY2hhbUBpemktYnktZWRmLmZyIiwicm9sZSI6IlJPTEVfQVBJX0NIQU0iLCJpZCI6MTE2MjIxLCJjc3JmVG9rZW4iOiJYb293RW1rZHpKVG5McE9sbDZJOTBMOVZIUk4xS1E5dG1yQnBVekJ1N0xVIiwiaWF0IjoxNTQ4NDE0NjUyLCJleHAiOjE1NDk2MjQyNTJ9.RpJ1z-TKQPT0gSQYz-zNqHHVcAq0mAZ6pUd7oM5UBCfyiImD2cqPjMDOh6WZDcDGqF1Yi76PokTEGn0hnkuQWu016L30BisGekMZ6VBbnChBu9AL2Z32-gHXIerovAe-hgbnbgLoL9Wi9PZuxtiBJTNFyHH4Pg_3TylVGwkM1Y7xdoMQaLIPtVnRZMw5CUhp0X5-Wcx6fk194HTv099yMfNx8uX6FOkoUuRdyBOiLU1-kahLR3nUU6CGZLOllibBu6SeUG_cqVVjO5xEGfOSzcx-GMdemiOXrLIk-UUO2mEsWxjBGMwLa8caJVfV0Rw0rT3g7OD_1SAGHpfaMHPSe4BovL3eEefOzl6HlR_DIP02jNZYTenHymHLb0QXR1JtGoTtGJ3cNDswYvIyamqMrnW9otKSsFWxHvZTXaHhcNh3CI_qYk5_4fCdUGUNnDUyGIK_ZABWom5Ag-pWujRXHFvdvfDt3CuYkqPDMScRh7CNTVbOQR_P_vJJYn43_fF0Br5rj7rXYWqPaSe-VcGyP4JRvDKg9ByvvBbW8E5W9T8sSRrGXg5LO5J-F9B6rabVk_kT9T4PETjf3ZONgponBC2nPeA4cmXcVWld19zrKcc8hEboYKfG0naJkvYKew9e12YgMwsFhEBYMBYy7l3fdWIFudrP615qc9LIbUKshgk');
    req.setHeader('X-Auth-Token', String.valueOf(mapToken.get(AP_Constant.ws_Token)));
    req.setHeader('Cache-Control', 'no-cache');
    req.setHeader('Accept', 'application/json'); 
    //req.setHeader('Content-Disposition', 'form-data; name="proAttestation"; filename="testPDF.pdf"'); 


    //req.setTimeout(120000);
       
    Http http =new Http();
    HTTPResponse res;
    try {
       res = http.send(req);
    }catch(System.CalloutException e) {
      //System.debug('Callout error: '+ e.getMessage());
      //System.debug(res.toString());
      //return new Map<String, Object>{'success' => false, 'message' => e.getMessage()};
    }
     
    System.debug('Callout Log 4:' + res.getStatusCode());
    System.debug('Callout Log 5:' + String.valueof(res.getbody()));
    System.debug('Callout Log 5:' + String.valueof(res.getStatus()));

    string responseStatusCode = String.valueof(res.getStatusCode());
    string responseBody = String.valueof(res.getbody());
    string responseStatus = String.valueof(res.getStatus());

    Lead leadToUpdate = new Lead(id=leadId);

    if(res.getStatusCode() == 200 || res.getStatusCode() == 201){
      leadToUpdate.SynchroBlueReussiePDF__c = true;
    }else{
      LeadToUpdate.SynchroBlueReussiePDF__c = false;
    }
    leadToUpdate.MessageBlueMisAjourPDF__c = res.getBody();

    list <Lead> listLeadToUpd = new list<Lead>();
    listLeadToUpd.add(leadToUpdate);
    update listLeadToUpd;       
  }
  */

}