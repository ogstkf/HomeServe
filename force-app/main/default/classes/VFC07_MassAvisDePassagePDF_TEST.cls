/**
 * @File Name          : VFC07_MassAvisDePassagePDF_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 08-20-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    27/08/2019, 17:59:12   RRJ     Initial Version
**/
@IsTest
public with sharing class VFC07_MassAvisDePassagePDF_TEST {
    static User adminUser;
    static Account testAcc;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Case> lstCase = new List<Case>();
    static List<Product2> lstProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static ContentWorkspace contentWs = new ContentWorkspace();
    static sofactoapp__Raison_Sociale__c raisonSocial;

    static {
        adminUser = TestFactory.createAdminUser('VFC07_MassAvisDePassagePDF_TEST@test.COM', TestFactory.getProfileAdminId());
		insert adminUser;

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('test Acc');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon[0].Type__c = 'Individual';
            lstServCon.add(TestFactory.createServiceContract('test serv con 2', testAcc.Id));
            lstServCon[1].Type__c = 'Individual';

            insert lstServCon;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            //create service territory
            srvTerr = TestFactory.createServiceTerritory('test Territory',opHrs.Id, raisonSocial.Id);
            insert srvTerr;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc.Id;
            insert lstLogement;

            //creating products
            lstProd.add(TestFactory.createProductAsset('Ramonage gaz'));
            lstProd.add(TestFactory.createProductAsset('Entretien gaz'));
            
            insert lstProd;

            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstProd[0].Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstLogement[1].Id));
            lstAsset[1].Product2Id = lstProd[0].Id;
            lstAsset[1].Logement__c = lstLogement[1].Id;
            lstAsset[1].AccountId = testAcc.Id;
            insert lstAsset;

            // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[0].Id));
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[1].Id));
            lstCase[1].Service_Contract__c = lstServCon[1].Id;
            insert lstCase;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Commissioning';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType2', 'Hours', 1));
            lstWrkTyp[1].Type__c = 'Maintenance';
            // lstWrkTyp[1].Reason__c = 'Flat-rate visit';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType3', 'Hours', 1));
            lstWrkTyp[2].Type__c = 'Maintenance';
            // lstWrkTyp[2].Reason__c = 'First maintenance';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType4', 'Hours', 1));
            lstWrkTyp[3].Type__c = 'Installation';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType5', 'Hours', 1));
            lstWrkTyp[4].Type__c = 'Maintenance';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType6', 'Hours', 1));
            lstWrkTyp[5].Type__c = 'Maintenance';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType7', 'Hours', 1));
            lstWrkTyp[6].Type__c = 'Troubleshooting';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType8', 'Hours', 1));
            lstWrkTyp[7].Type__c = 'Various Services';

            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[1].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[2].WorkTypeId = lstWrkTyp[2].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[3].WorkTypeId = lstWrkTyp[3].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[4].WorkTypeId = lstWrkTyp[4].Id;
            lstWrkOrd[4].CaseId = lstCase[0].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[5].WorkTypeId = lstWrkTyp[5].Id;
            lstWrkOrd[5].CaseId = lstCase[1].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[6].WorkTypeId = lstWrkTyp[6].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[7].WorkTypeId = lstWrkTyp[7].Id;
            insert lstWrkOrd;

            // creating service appointment
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[0].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[1].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[2].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[3].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[4].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[5].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[6].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[7].Id));
            
            insert lstServiceApp;

            contentWs = TestFactory.createContentWorkSpace('Avis De Passage test');
            insert contentWs;
        }
    }

    @IsTest
    public static void generateMassPdfTest(){
        System.runAs(adminUser){
			
			Test.startTest();
            List<ServiceAppointment> lstServiceAppTest = new List<ServiceAppointment>([SELECT Id FROM ServiceAppointment WHERE Id IN: lstServiceApp]);
            List<String> lstSAId = new List<String>();
            for(ServiceAppointment srvApp: lstServiceAppTest){
                lstSAId.add(srvApp.Id);
            }

            PageReference pageRef = Page.VFP07_MassAvisDePassagePDF;
            pageRef.getParameters().put('lstIds', JSON.serialize(lstSAId));
            Test.setCurrentPage(pageRef);
            VFC07_MassAvisDePassagePDF testPDF = new VFC07_MassAvisDePassagePDF();
	        Test.stopTest();

		}
    }
}