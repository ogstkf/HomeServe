/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 07-03-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-03-2020   ZJO   Initial Version
**/
@isTest
public with sharing class VFC_CustomTableCmp_TEST {
    static User adminUser;
    static Account testAcc = new Account();
    static sofactoapp__Raison_Sociale__c testSofacto;
    static ServiceTerritory testAgence = new ServiceTerritory();
    static Logement__c testLogement = new Logement__c();
    static Product2 testProd = new Product2();
    static Asset testAsset = new Asset();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static ServiceContract ServCon = new ServiceContract();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static List<ContractLineItem> lstClis = new List<ContractLineItem>();
    
    
    static{

        adminUser = TestFactory.createAdminUser('VFC@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){
            
            //Create Account
            testAcc = TestFactory.createAccount('testAcc');
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            //Update Account
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //Create Sofacto
            testSofacto = new sofactoapp__Raison_Sociale__c(
                Name = 'test sofacto 1',
                sofactoapp__Credit_prefix__c = '1234',
                sofactoapp__Invoice_prefix__c = '2134'
            );
            insert testSofacto;

            //Create Operating Hour
            OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
            insert OprtHour;

            //Create Agence
            testAgence = new ServiceTerritory(
                Name = 'test agence 1',
                Agency_Code__c = '0001',
                IsActive = True,
                OperatingHoursId = OprtHour.Id,
                Sofactoapp_Raison_Social__c = testSofacto.Id
            );
            insert testAgence;

            //Create Logement
            testLogement = TestFactory.createLogement(testAcc.Id, testAgence.Id);
            insert testLogement;

            //Create Product
            testProd = TestFactory.createProductAsset('testProd');
            testProd.IsBundle__c = true;
            insert testProd;

            //Create Asset
            testAsset = TestFactory.createAsset('testAsset', AP_Constant.assetStatusActif, testLogement.Id);
            testAsset.AccountId = testAcc.Id;
            testAsset.Product2Id = testProd.Id;
            insert testAsset;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create Service Contract
            ServCon = TestFactory.createServiceContract('testCon', testAcc.Id);
            ServCon.Pricebook2Id = lstPrcBk[0].Id;
            ServCon.Type__c = 'Collective';
            ServCon.recordtypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId();
            insert ServCon;

            //Create Pricebook Entry
            PrcBkEnt = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, testProd.Id, 100);
            insert PrcBkEnt;

            //Create list of CLIs
            ContractLineItem cli1 = TestFactory.createContractLineItem(ServCon.Id, PrcBkEnt.Id, 100, 50);
            ContractLineItem cli2 = TestFactory.createContractLineItem(ServCon.Id, PrcBkEnt.Id, 100, 50);
            ContractLineItem cli3 = TestFactory.createContractLineItem(ServCon.Id, PrcBkEnt.Id, 100, 50);

            lstClis.add(cli1);
            lstClis.add(cli2);
            lstClis.add(cli3);

            insert lstClis;
        }     
    }

    @isTest
    public static void testGetContractId(){
        System.runAs(adminUser){
            Test.startTest();

            PageReference pageRef = Page.VFP_CustomTableCmp;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id', String.valueOf(lstClis[0].Id));
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstClis);
            VFC_CustomTableCmp ext = new VFC_CustomTableCmp(sc);
          
            Test.stopTest();

        }
    }
}