/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 03-06-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   03-06-2021   MNA   Initial Version
**/
public class ChaineEditiqueQueueable implements Queueable {   
    public Decimal newVal;
    
    public ChaineEditiqueQueueable(Decimal newVal){
        this.newVal = newVal;
    }
    
    public void execute(QueueableContext qc){          
		updateDocumentId(newVal);
    }
    public static void updateDocumentId(Decimal count) {
        /*
        Metadata.DeployContainer mdContainer = new Metadata.DeployContainer(); 
        Metadata.CustomMetadata mdt = new Metadata.CustomMetadata();
        mdt.fullName = 'WebServiceConfiguration__mdt.ChaineEditiquePOST';
        mdt.label = 'Chaine Editique (post)';
        Metadata.CustomMetadataValue customFieldtoUpdate = new Metadata.CustomMetadataValue();
        customFieldtoUpdate.field = 'documentId__c';
        customFieldtoUpdate.value = newValue;
        mdt.values.add(customFieldtoUpdate);
        mdContainer.addMetadata(mdt);
        system.debug('mdContainer**'+mdContainer);
        
        if (!Test.isRunningTest()) {
            Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);
            System.debug('jobId=' +  jobId);
        }*/
        List<Editique_Document_ID__c> lstEDocId = new List<Editique_Document_ID__c>();
        for (integer i = 0; i < count; i++) {
            lstEDocId.add(new Editique_Document_ID__c());
        }

        List<Database.SaveResult> lstSr = Database.insert(lstEDocId);
    }
}