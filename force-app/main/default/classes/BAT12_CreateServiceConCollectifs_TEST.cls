/**
 * @File Name          : BAT12_CreateServiceContractCollectifs_TEST.cls
 * @Description        : 
 * @Author             : CLA
 * @Group              : 
 * @Last Modified By   : CLA
 * @Last Modified On   : 15/06/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    15/06/2020   CLA     Initial Version
**/
@IsTest
private class BAT12_CreateServiceConCollectifs_TEST {
    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<ServiceContract> lstSerCon = new List<ServiceContract>();
    static List<ServiceContract> lstSerConParent = new List<ServiceContract>();
    static List<ContractLineItem> lstContLineItem = new List<ContractLineItem>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<Product_Bundle__c> lstProductBundle = new List<Product_Bundle__c>();
    static sofactoapp__Coordonnees_bancaires__c bnkDet;
    static List<Tarifs_Unifies_Locaux__c> lstTUL = new List<Tarifs_Unifies_Locaux__c>();
    static List<ServiceTerritory> lstServiceTerritory = new List<ServiceTerritory>();
    static List<sofactoapp__Valeur_indice__c> lstValeurIndice = new List<sofactoapp__Valeur_indice__c>();
    static List<sofactoapp__Indice__c> lstIndice = new List<sofactoapp__Indice__c>();
    static List<Revalorisation_annuelle__c> lstRevalorisation = new List<Revalorisation_annuelle__c>();
    static Integer numDaysPrior = Integer.valueOf(System.Label.NbDaysPriorRenewalCollectifs);
    static Bypass__c bp = new Bypass__c();

    static {
        adminUser = TestFactory.createAdminUser('testuser', TestFactory.getProfileAdminId());
        insert adminUser;

        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP74';
        bp.BypassValidationRules__c = True;
        bp.BypassWorkflows__c = True;
        bp.Bypass_Process_Builder__c = True;
        insert bp;

        System.runAs(adminUser) {
            Id serviceContractRecordTypeId = TestFactory.getRecordTypeId('ServiceContract', 'Contrats Collectifs Privés');

            for (Integer i = 0; i < 5; i++) {
                lstTestAcc.add(TestFactory.createAccount(TestFactory.randomizeString('test Acc') + i));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
                lstTestAcc[i].BillingPostalCode = '1234' + i;
                lstTestAcc[i].PersonEmail = TestFactory.randomizeString('test_acc').toLowerCase() + i+'@test.com';
                lstTestAcc[i].Rating__c = 4;
            }
            insert lstTestAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstTestAcc.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstTestAcc.get(2).Id);
            sofactoapp__Compte_auxiliaire__c aux3 = DataTST.createCompteAuxilaire(lstTestAcc.get(3).Id);
            sofactoapp__Compte_auxiliaire__c aux4 = DataTST.createCompteAuxilaire(lstTestAcc.get(4).Id);
			lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstTestAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstTestAcc.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            lstTestAcc.get(3).sofactoapp__Compte_auxiliaire__c = aux3.Id;
            lstTestAcc.get(4).sofactoapp__Compte_auxiliaire__c = aux4.Id;
            
            update lstTestAcc;

            //create products
            for(Integer i=0; i<10; i++){
                lstTestProd.add(TestFactory.createProduct('testProd'+i));
                lstTestProd[i].ProductCode = 'Test Code' + i;				//MNA 26/08/2021
                if(Math.mod(i, 5)== 0){
                    lstTestProd[i].IsBundle__c = true;
                    lstTestProd[i].Name = 'testBundleProd'+i/5;
                    // lstTestProd[i].Type_de_garantie__c =  'Totale';
                }
                // lstTestProd[i].RecordTypeId =  Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Cham_offer').getRecordTypeId();
            }
           
            
            
            insert lstTestProd;

            Bundle__c bundleParent = TestFactory.createBundle('testProdBundleParent',100);
            insert bundleParent;

            Product_Bundle__c productBundleParent0 = TestFactory.createBundleProduct(bundleParent.Id, lstTestProd[0].Id);
            lstProductBundle.add(productBundleParent0);
            Product_Bundle__c productBundleParent1 = TestFactory.createBundleProduct(bundleParent.Id, lstTestProd[1].Id);
            productBundleParent1.Price__c = 50;
            productBundleParent1.Optional_Item__c = true;
            lstProductBundle.add(productBundleParent1);
            insert lstProductBundle;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
                // ,Ref_Agence__c = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST2', sofactoapp__Credit_prefix__c= '244', sofactoapp__Invoice_prefix__c='422');
            insert sofa; 

            bnkDet = new sofactoapp__Coordonnees_bancaires__c(
                sofactoapp__Compte__c = lstTestAcc[0].Id
                // , sofactoapp__Compte_comptable__c = 
                , Sofacto_Actif__c = true
                , tech_Rib_Valid__c = true
                , sofactoapp__IBAN__c = 'FR14 2004 1010 0505 0001 3M02 606'
                , sofactoapp__Raison_sociale__c = sofa.Id
            );
            insert bnkDet;

            OperatingHours newOperatingHour = TestFactory.createOperatingHour('test1');
            insert newOperatingHour;

            lstServiceTerritory = new List<ServiceTerritory>{
                new ServiceTerritory(
                    Name = 'test',
                    Agency_Code__c = '0007',
                    OperatingHoursId = newOperatingHour.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa.Id
                )};
            insert lstServiceTerritory;

            lstTUL.add(TestFactory.createTarifsUnifiesLocaux('testTarif',20,bundleParent.Id,lstServiceTerritory[0].Id,230));
            lstTUL[0].Augmentation_nationale_annuelle__c = 20;
            insert lstTUL;

            for (Integer i = 0; i < 5; i++) {
                lstSerConParent.add(TestFactory.createServiceContract('test' + i, lstTestAcc[i].Id));
                lstSerConParent[i].Type__c = 'Collective';
                lstSerConParent[i].Agency__c = lstServiceTerritory[0].Id;
                lstSerConParent[i].Contract_Status__c = 'Active';
                lstSerConParent[i].StartDate = System.today().addYears(-2);
                lstSerConParent[i].EndDate = System.today().addYears(1).addDays(-90);
                lstSerConParent[i].Contrat_resilie__c = false;
                lstSerConParent[i].PriceBook2Id = lstPrcBk[0].Id;
                lstSerConParent[i].sofactoapp_Rib_prelevement__c = bnkDet.Id;
                lstSerConParent[i].Contrat_signe_par_le_client__c = false;
                lstSerConParent[i].tech_facture_OK__c = false;
                lstSerConParent[i].Tech_generate_digital_invoice__c = false;
                lstSerConParent[i].recordTypeId = serviceContractRecordTypeId;
                lstSerConParent[i].TECH_BypassBAT__c = false;
                lstSerConParent[i].Prise_en_compte_des_indices__c = 'Indices de l\'année précédente';
            }
            insert lstSerConParent;

            for (Integer i = 0; i < 5; i++) {
                lstSerCon.add(TestFactory.createServiceContract('test' + i, lstTestAcc[i].Id));
                lstSerCon[i].Type__c = 'Collective';
                lstSerCon[i].Agency__c = lstServiceTerritory[0].Id;
                lstSerCon[i].Contract_Status__c = 'Active';
                lstSerCon[i].StartDate =  System.today().addYears(-1).addDays(numDaysPrior);
                lstSerCon[i].EndDate = System.today().addDays(numDaysPrior);
                lstSerCon[i].Contrat_resilie__c = false;
                lstSerCon[i].PriceBook2Id = lstPrcBk[0].Id;
                lstSerCon[i].sofactoapp_Rib_prelevement__c = bnkDet.Id;
                lstSerCon[i].Contrat_signe_par_le_client__c = false;
                lstSerCon[i].tech_facture_OK__c = false;
                lstSerCon[i].Tech_generate_digital_invoice__c = false;
                lstSerCon[i].Nombre_de_renouvellements_restants__c = 2;
                lstSerCon[i].ParentServiceContractId = lstSerConParent[i].Id;
                lstSerCon[i].recordTypeId = serviceContractRecordTypeId;
                lstSerCon[i].TECH_BypassBAT__c = false;
            }

            lstSerCon[0].StartDate =  System.today().addDays(Integer.valueOf(System.Label.NbDaysPriorRenewalCollectifs)).addYears(-1);
            lstSerCon[0].EndDate =  System.today().addDays(Integer.valueOf(System.Label.NbDaysPriorRenewalCollectifs)+10);
            lstSerCon[0].tech_facture_OK__c = true;

            lstSerCon[3].TECH_BypassBAT__c =  true;

            lstSerCon[4].Duree_d_application_de_la_remise__c = 2;
            lstSerCon[4].Echeancier_Paiement__c = 'Comptant';
            lstSerCon[4].Contract_Status__c = 'Résilié';
            insert lstSerCon;



            //pricebook entry
            for(Integer i=0; i<10; i++){
                lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[i].Id, 0));
                if(lstTestProd[i].IsBundle__c){
                    lstPrcBkEnt[i].UnitPrice = 150;
                }
            }

            insert lstPrcBkEnt;

            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[1].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[2].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[3].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[4].Id, 0, 1));

            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[1].Id, lstPrcBkEnt[0].Id, 150, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[1].Id, lstPrcBkEnt[1].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[1].Id, lstPrcBkEnt[2].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[1].Id, lstPrcBkEnt[3].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[1].Id, lstPrcBkEnt[4].Id, 0, 1));

            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[2].Id, lstPrcBkEnt[0].Id, 150, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[2].Id, lstPrcBkEnt[1].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[2].Id, lstPrcBkEnt[2].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[2].Id, lstPrcBkEnt[3].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[2].Id, lstPrcBkEnt[4].Id, 0, 1));

            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[3].Id, lstPrcBkEnt[0].Id, 150, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[3].Id, lstPrcBkEnt[1].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[3].Id, lstPrcBkEnt[2].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[3].Id, lstPrcBkEnt[3].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[3].Id, lstPrcBkEnt[4].Id, 0, 1));

            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[4].Id, lstPrcBkEnt[0].Id, 150, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[4].Id, lstPrcBkEnt[1].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[4].Id, lstPrcBkEnt[2].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[4].Id, lstPrcBkEnt[3].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[4].Id, lstPrcBkEnt[4].Id, 0, 1));

            for(integer i=0; i<lstContLineItem.size(); i++ ){
                lstContLineItem[i].IsActive__c = true;
            }
            insert lstContLineItem;

            lstIndice = new List<sofactoapp__Indice__c>{
                new sofactoapp__Indice__c(
                    Name = 'Indice 1',
                    sofactoapp__Etat__c = 'Actif'
                ),
                new sofactoapp__Indice__c(
                    Name = 'Indice 2',
                    sofactoapp__Etat__c = 'Actif'
                ),
                new sofactoapp__Indice__c(
                    Name = 'Indice 3',
                    sofactoapp__Etat__c = 'Actif'
                ),
                new sofactoapp__Indice__c(
                    Name = 'Indice 4',
                    sofactoapp__Etat__c = 'Actif'
                ),
                new sofactoapp__Indice__c(
                    Name = 'Indice 5',
                    sofactoapp__Etat__c = 'Actif'
                ),
                new sofactoapp__Indice__c(
                    Name = 'Indice 5',
                    sofactoapp__Etat__c = 'Actif'
                )
            };
            insert lstIndice;

            for(integer i=0; i<6;i++){
                Date lastyear = system.today().addYears(-1);

                lstValeurIndice.add(new sofactoapp__Valeur_indice__c(
                                        Name = 'Valeur Indice 1 '+String.valueOf(lastyear),
                                        sofactoapp__Coefficient__c = 110.2,
                                        sofactoapp__Debut__c = lastyear,
                                        sofactoapp__Indice__c = lstIndice[i].Id));
            }

            for(integer i=0; i<6;i++){
                Date thisyear = system.today();

                lstValeurIndice.add(new sofactoapp__Valeur_indice__c(
                                        Name = 'Valeur Indice 1 '+String.valueOf(thisyear),
                                        sofactoapp__Coefficient__c = 110.2,
                                        sofactoapp__Debut__c = thisyear,
                                        sofactoapp__Indice__c = lstIndice[i].Id));
            }
            insert lstValeurIndice;


            for(integer i=0; i<5;i++){
                lstRevalorisation.add(new Revalorisation_annuelle__c(Formule_de_revalorisation__c = '3_Fsd1_ICHTTS',
                                                                    A__c = 0.5,
                                                                    B__c = 0.15,
                                                                    C__c = 0.35,
                                                                    Contrat_de_service__c = lstSerCon[i].Id,
                                                                    Indice_1_en_cours__c = lstValeurIndice[i].Id,
                                                                    Indice_1_origine__c = lstValeurIndice[i].Id,
                                                                    Indice_2_en_cours__c = lstValeurIndice[i].Id,
                                                                    Indice_2_origine__c = lstValeurIndice[i].Id,
                                                                    Indice_3_en_cours__c = lstValeurIndice[i].Id,
                                                                    Indice_3_origine__c = lstValeurIndice[i].Id,
                                                                    Indice_4_en_cours__c = lstValeurIndice[i].Id,
                                                                    Indice_4_origine__c = lstValeurIndice[i].Id,
                                                                    // Produit__c = lstPrcBk[0].Id,
                                                                    Type__c = 'Entretien'
                                                                ));
            }
            insert lstRevalorisation;
        }
    }

    static testMethod void testBatch() {
        system.RunAs(adminUser){
            Test.startTest();
            BAT12_CreateServiceContractCollectifs batch = new BAT12_CreateServiceContractCollectifs();
            Database.executeBatch(batch);
            Test.stopTest();
        }
    }
    
	static testMethod void testSchedule() {
        system.RunAs(adminUser){
            Test.startTest();
            System.schedule('BAT12' + Datetime.now().format(),  '0 0 0 * * ?', new BAT12_CreateServiceContractCollectifs());
            Test.stopTest();
        }
    }
    
}