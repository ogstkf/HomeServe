/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 04-07-2021
 * @last modified by  : ARA
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-15-2020   AMO   Initial Version
**/
@isTest
public with sharing class AP75_UpdateWorkOrdersFromTEST {

    static User mainUser;
    static Account testAcc = new Account();
    static List<WorkOrder> lstWorkOrder = new List<WorkOrder>();
    static ServiceContract servCon = new ServiceContract();
    static ServiceContract servCon2 = new ServiceContract();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static Asset testAsset = new Asset();
    static sofactoapp__Raison_Sociale__c testSofacto;
    static ServiceTerritory testAgence = new ServiceTerritory();
    static Logement__c testLogement = new Logement__c();
    static Product2 testProd = new Product2();
    static List<Contact> lstContacts = new List<Contact>();
    //bypass
    static Bypass__c bp = new Bypass__c();

    static{

        mainUser = TestFactory.createAdminUser('AP75_UpdateWorkOrdersFromMaintenancePlan@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

                        //Create Sofacto
            testSofacto = new sofactoapp__Raison_Sociale__c(
                Name = 'test sofacto 1',
                sofactoapp__Credit_prefix__c = '1234',
                sofactoapp__Invoice_prefix__c = '2134'
            );
            insert testSofacto;

            //Create Operating Hour
            OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
            insert OprtHour;

            //Create Agence
            testAgence = new ServiceTerritory(
                Name = 'test agence 1',
                Agency_Code__c = '0001',
                IsActive = True,
                OperatingHoursId = OprtHour.Id,
                Sofactoapp_Raison_Social__c = testSofacto.Id
            );
            insert testAgence;

            //Create Logement
            testLogement = TestFactory.createLogement(testAcc.Id, testAgence.Id);
            testLogement.Inhabitant__c = testAcc.Id;
            insert testLogement;

            //Create Product
            testProd = TestFactory.createProduct('TestProd');
            testProd.IsBundle__c = true;
            testProd.Equipment_family__c = 'Chaudière';          
            testProd.Equipment_type1__c = 'Chaudière gaz';
            insert testProd;

            //Create Asset
            testAsset = new Asset(
                Name = 'Test Asset',
                Product2Id = testProd.Id,
                AccountId = testAcc.Id,
                Status = 'Actif',
                Logement__c = testLogement.Id
            );
            insert testAsset;

            //Create Contacts
            lstContacts = new List<Contact>{
                new Contact(LastName = 'Test Con 1'),
                new Contact(LastName = 'Test Con 2')
            };
            insert lstContacts;


            //Create Service Contract
            servCon = new ServiceContract(name = 'Service Con',
                                                  Pricebook2Id = lstPrcBk[0].Id,
                                                  AccountId = testAcc.Id,
                                                  Asset__c = testAsset.Id,
                                                  Agency__c = testAgence.Id,
                                                  RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId(),
                                                  ContactId = lstContacts[0].Id );
            insert servCon;

            //Create Service Contract
            servCon2 = new ServiceContract(name = 'Service Con',
                                                  Pricebook2Id = lstPrcBk[0].Id,
                                                  AccountId = testAcc.Id,
                                                  Asset__c = testAsset.Id,
                                                  Agency__c = testAgence.Id,
                                                  RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId(),
                                                  ContactId = lstContacts[1].Id );
            insert servCon2;

            //Create WorkOrder
            lstWorkOrder = new List<WorkOrder>{
                new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal, ServiceContractId = servCon.Id, AssetId = testAsset.Id),
                new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal, ServiceContractId = servCon2.Id)
            };

            insert lstWorkOrder;

        }
    }

    @isTest
    public static void testInsert(){
        System.runAs(mainUser){
            Map<String, String> mapWOIdSCId = new Map<String, String>();
            mapWOIdSCId.put(lstWorkOrder[0].Id, lstWorkOrder[0].ServiceContractId);
            mapWOIdSCId.put(lstWorkOrder[1].Id, lstWorkOrder[1].ServiceContractId);
            System.debug('mapWOIdSCId'+ mapWOIdSCId);

            Test.startTest();    
                AP75_UpdateWorkOrdersFromMaintenancePlan.updWOGenerateFromMaintenancePlan(lstWorkOrder, mapWOIdSCId);
            Test.stopTest();
            
        }
    }
}