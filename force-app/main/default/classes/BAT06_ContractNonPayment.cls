/**
 * @File Name          : BAT06_ContractNonPayment.cls
 * @Description        : 
 * @Author             : KJB
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-16-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/12/2019         KJB                    Initial Version
**/

global class BAT06_ContractNonPayment implements Database.Batchable <sObject>, Schedulable  {

    global Date todayDate = date.today();
    global Integer numDays;
    global static Integer recordsProcessed = 0;
    global String seuilContratRenouvle = Label.SeuilContratRenouvle; //30
    global String seuilContratNONRenouvle = Label.SeuilContratNonRenouvle; //60

    global Database.QueryLocator start(Database.BatchableContext BC){

        // TEC-510: ARA 16/04/2021
        
        List<String>lstStrAgencyExclude = new list<String>();
        for(BAT06_ExcludeAgency__c b : [SELECT Id, Name FROM BAT06_ExcludeAgency__c limit 100]){
        lstStrAgencyExclude.add(b.Name);
        }
        system.debug('*** lstStrAgencyExclude: '+lstStrAgencyExclude);
        system.debug('## start method ');
        Date sixMonthEarlier = System.Today().addMonths(-6);
        String query = 'SELECT Id, Name, Contract_Renewed__c, Contract_Status__c, TECH_DateStatusActifEnRetard__c FROM ServiceContract WHERE  Contract_Status__c = \'Actif - en retard de paiement\' AND StartDate =: sixMonthEarlier AND Agency__r.Agency_Code__c NOT IN : lstStrAgencyExclude';
        
        //query += ' AND (id = \'8101X000000CbbvQAC\' ) ' ; //for testing purposes

        system.Debug('### query: ' + query);

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ServiceContract> scope) {

        system.debug('##execute method ');
        system.debug('### : ' + scope.size() );

        list<ServiceContract> lstServContr = new list<ServiceContract>();

        system.debug('###scope : ' + scope );

        
        for(ServiceContract servContr : scope){
            numDays = (Date.valueOf(servContr.TECH_DateStatusActifEnRetard__c)).daysBetween(todayDate);
            system.debug('###DateStatusActifEnRetard : ' + servContr.TECH_DateStatusActifEnRetard__c );
            system.debug('###numDays : ' + numDays );
            //DMU 20/04/2021 TEC-510: Simplifie la logique enlever la condition basant sur les custom label (30/60jr)
            //if((numDays > Integer.valueOf(seuilContratNONRenouvle) && servContr.Contract_Renewed__c == false) || (numDays > Integer.valueOf(seuilContratRenouvle) && servContr.Contract_Renewed__c == true)){
                servContr.Contract_Status__c = 'Résiliation pour retard de paiement';
                lstServContr.add(servContr);
                recordsProcessed = recordsProcessed + 1;
            //}
        }
        update lstServContr;

    }

    global void finish(Database.BatchableContext BC) {

        system.debug('## finish method ');
        system.debug('Number of records processed: ' + recordsProcessed);
        
    }

    // global static String scheduleBatch() {

    //     system.debug('## execute method ');
    //     BAT06_ContractNonPayment scheduler = new BAT06_ContractNonPayment();
    //     return System.schedule('BAT06_ContractNonPayment:' + Datetime.now().format(),  '0 0 6 * * ?', scheduler);

    // }

    global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT06_ContractNonPayment');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT06_ContractNonPayment(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT06_ContractNonPayment());
        }
    }

}