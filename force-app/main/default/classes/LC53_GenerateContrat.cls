/**
 * @File Name          : LC53_GenerateContrat.cls
 * @Description        : 
 * @Author             : DMG
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 07/02/2020, 12:02:51
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         13/12/2019               DMG                       Initial Version
**/
public with sharing class LC53_GenerateContrat {
    @AuraEnabled
    public static map<string,Object> saveContrat(String serConId){
	    
	    map<string,Object> mapOfResult = new map<string,Object>();
        List<ContentVersion> lstCV = new List<ContentVersion>();
        Boolean isChaudiere = false;
        Boolean createContract = true;
        
		try {
            PageReference pageRef_CP = null;
		    PageReference pageRef_CGV = null;
            ServiceContract sc = [Select Id,Name, Numero_du_contrat__c, ContractNumber from ServiceContract where Id=:serConId];
            List<ContractLineItem> lstContractLineItem = new List<ContractLineItem>();
            lstContractLineItem = [SELECT Id, LineItemNumber,Product2Id,Product2.Equipment_family__c,Product2.Equipment_type1__c,tech_nom_produit__c, ServiceContractId, TotalPrice,ListPrice, IsBundle__c FROM ContractLineItem WHERE (ServiceContractId=:serConId AND IsBundle__c = True ) limit 1];
            system.debug('## lstContractLineItem :'+lstContractLineItem);
            if(lstContractLineItem.size()>0){
                if(lstContractLineItem[0].Product2Id!=null){
                    System.debug('#### prod family: '+lstContractLineItem[0].Product2.Equipment_family__c);
                    System.debug('### eqp type: '+lstContractLineItem[0].Product2.Equipment_type1__c);
                    //Valeurs du cham pEquipment type1 sur Product - MAPPING Type Contract CP/CGV
                    if(lstContractLineItem[0].Product2.Equipment_family__c == 'Chaudière'){
                        if(lstContractLineItem[0].Product2.Equipment_type1__c == 'Chaudière gaz'){
                            pageRef_CP = page.VFP38_ConditionParticuliere;
                            pageRef_CGV = page.CGV_chauffage;
                        }else if(lstContractLineItem[0].Product2.Equipment_type1__c == 'Chaudière fioul'){
                            pageRef_CP = page.VFP38_ConditionParticuliereFioul;
                            pageRef_CGV = page.CGV_NvxclientsAgencespilotes;
                        }else if(lstContractLineItem[0].Product2.Equipment_type1__c == 'Radiateur gaz'){
                            pageRef_CP = page.VFP38_ConditionParticuliere;
                            pageRef_CGV = page.CGV_chauffage;
                        }else if(lstContractLineItem[0].Product2.Equipment_type1__c == 'Poêle à granulés'){
                            pageRef_CP = page.VFP38_ConditionParticuliereBois;
                            pageRef_CGV = page.CGV_Nouveauxclientsautresagences;
                        }else{
                            //No Need to create contract
                        	createContract = false;
                        }
                    }else if(lstContractLineItem[0].Product2.Equipment_family__c == 'Pompe à chaleur'){
                            pageRef_CP = page.VFP38_ConditionParticulierePAC;
                            pageRef_CGV = page.CGV_NouveauxclientsAgencespilotes;

                    }else if(lstContractLineItem[0].Product2.Equipment_family__c == 'Chauffe eau'){
                        if(lstContractLineItem[0].Product2.Equipment_type1__c == 'Chauffe-eau solaire'){
                            pageRef_CP = page.VFP38_ConditionParticuliereSolaire;
                            pageRef_CGV = page.CGV_ContratSolaireNouveauxclients;
                        }else if(lstContractLineItem[0].Product2.Equipment_type1__c == 'Chauffe-eau gaz'){
                            pageRef_CP = page.VFP38_ConditionParticuliere;
                            pageRef_CGV = page.CGV_chauffage;
                        }else{
                            //No Need to create contract
                            createContract = false;
                        }
                    }else{
                        //No Need to create contract
                        createContract = false;
                    }
                }
            }else{
                //No Need to create contract
                createContract = false;
            }
            System.debug('createContract = ' + createContract);
            if(createContract == true){
                //Create PDF CP
                    //pageRef_CP = (isChaudiere ? page.VFP38_ConditionParticuliere : page.VFP38_ConditionParticulierePAC);
                Blob reportPDF_CP;
                System.debug('#### pageRef_CP '+pageRef_CP);
                pageRef_CP.getParameters().put('id', serConId);
                reportPDF_CP = (Test.isRunningTest() ? Blob.valueOf('Unit.Test') : pageRef_CP.getContentAsPDF());
                ContentVersion cv_CP = new ContentVersion();
                cv_CP.Title = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c + '_CP.pdf' : sc.ContractNumber + '_CP.pdf';
                cv_CP.PathOnClient = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c + '_CP.pdf' : sc.ContractNumber + '_CP.pdf';
                cv_CP.VersionData = reportPDF_CP;
                cv_CP.IsMajorVersion = true;
                Insert cv_CP;
                lstCV.add(cv_CP);

                //Create PDF CGV 
                    //pageRef_CGV = (isChaudiere ? page.CGV_chauffage : page.CGV_NouveauxclientsAgencespilotes); 
                Blob reportPDF_CGV;
                reportPDF_CGV = (Test.isRunningTest() ? Blob.valueOf('Unit.Test') : pageRef_CGV.getContentAsPDF());
                ContentVersion cv_CGV = new ContentVersion();
                cv_CGV.Title = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c +'_CGV.pdf' : sc.ContractNumber +'_CGV.pdf';
                cv_CGV.PathOnClient = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c + '_CGV.pdf' : sc.ContractNumber + '_CGV.pdf';
                cv_CGV.VersionData = reportPDF_CGV;
                cv_CGV.IsMajorVersion = true;
                Insert cv_CGV;
                lstCV.add(cv_CGV);

                //Create PDF Bulletin de paiement
                PageReference pageRef_Bulletin = null;
                pageRef_Bulletin = page.VFP38_BulletindePaiement;
                Blob reportPDF_Bulletin;
                pageRef_Bulletin.getParameters().put('id', serConId);
                reportPDF_Bulletin = (Test.isRunningTest() ? Blob.valueOf('Unit.Test') : pageRef_Bulletin.getContentAsPDF());
                ContentVersion cv_Bulletin= new ContentVersion();
                cv_Bulletin.Title = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c + '_BULLETIN DE PAIEMENT.pdf' : sc.ContractNumber + '_BULLETIN DE PAIEMENT.pdf';
                cv_Bulletin.PathOnClient = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c + '_BULLETIN DE PAIEMENT.pdf' : sc.ContractNumber + '_BULLETIN DE PAIEMENT.pdf';
                cv_Bulletin.VersionData = reportPDF_Bulletin;
                cv_Bulletin.IsMajorVersion = true;
                Insert cv_Bulletin;
                lstCV.add(cv_Bulletin);

                //Create PDF Formulaire de rétractation
                PageReference pageRef_Retr = null;
                pageRef_Retr = page.VFP38_Retractation;
                Blob reportPDF_Retr;
                reportPDF_Retr = (Test.isRunningTest() ? Blob.valueOf('Unit.Test') : pageRef_Retr.getContentAsPDF());
                ContentVersion cv_Retr= new ContentVersion();
                cv_Retr.Title = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c + '_BULLETIN DE PAIEMENT.pdf' : sc.ContractNumber + '_FORMULAIRE DE RÉTRACTATION.pdf';
                cv_Retr.PathOnClient = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c + '_BULLETIN DE PAIEMENT.pdf' : sc.ContractNumber + '_FORMULAIRE DE RÉTRACTATION.pdf';
                cv_Retr.VersionData = reportPDF_Retr;
                cv_Retr.IsMajorVersion = true;
                Insert cv_Retr;
                lstCV.add(cv_Retr);

                //Get Content Documents
                List<ContentVersion> docList = [SELECT ContentDocumentId FROM ContentVersion where Id=:lstCV];
                Map<Id, Id> mapIdCVIdCDL = new Map<Id, Id>();
                for(ContentVersion contentV : docList){
                    mapIdCVIdCDL.put(contentV.Id,contentV.ContentDocumentId);
                }
                
                //Create ContentDocumentLink CP
                ContentDocumentLink cdl_CP = New ContentDocumentLink();
                cdl_CP.LinkedEntityId = serConId;
                cdl_CP.ContentDocumentId = mapIdCVIdCDL.get(cv_CP.Id);
                cdl_CP.shareType = 'V';
                Insert cdl_CP;
                //Create ContentDocumentLink CGV
                ContentDocumentLink cdl_CGV = New ContentDocumentLink();
                cdl_CGV.LinkedEntityId = serConId;
                cdl_CGV.ContentDocumentId = mapIdCVIdCDL.get(cv_CGV.Id);
                cdl_CGV.shareType = 'V';
                Insert cdl_CGV;
                //Create ContentDocumentLink Bulletin de paiement
                ContentDocumentLink cdl_Bulletin = New ContentDocumentLink();
                cdl_Bulletin.LinkedEntityId = serConId;
                cdl_Bulletin.ContentDocumentId = mapIdCVIdCDL.get(cv_Bulletin.Id);
                cdl_Bulletin.shareType = 'V';
                Insert cdl_Bulletin;
                //Create ContentDocumentLink Formulaire de rétractation
                ContentDocumentLink cdl_Retr = New ContentDocumentLink();
                cdl_Retr.LinkedEntityId = serConId;
                cdl_Retr.ContentDocumentId = mapIdCVIdCDL.get(cv_Retr.Id);
                cdl_Retr.shareType = 'V';
                Insert cdl_Retr;
                
                mapOfResult.put('error',false);
                mapOfResult.put('message', 'SUCESS!!!');
                mapOfResult.put('createContract',createContract);
                mapOfResult.put('cp',cdl_CP.ContentDocumentId);
                mapOfResult.put('cgv',cdl_CGV.ContentDocumentId);
                mapOfResult.put('Bulletin',cdl_Bulletin.ContentDocumentId);
                mapOfResult.put('FRetract',cdl_Retr.ContentDocumentId);
            }else{
                mapOfResult.put('error',true);
                mapOfResult.put('message', 'Le produit de votre équipement n’a pas de type d\'équipement renseigné, nous ne pouvons donc pas générer le contrat avec les bonnes CGV');
                mapOfResult.put('createContract',createContract);
            }
		}
		catch (Exception e) {
		    System.debug('error msg:' + e.getMessage()+ e.getStackTraceString());
	        mapOfResult.put('error',true);
	        mapOfResult.put('message',e.getMessage());
		}

    return mapOfResult;
    }
    @AuraEnabled
    public static map<string,Object> insertImageAttachment(String serConId) {
        system.debug('## starting method insertImageAttachment :'+serConId);
        map<string,Object> mapOfResult = new map<string,Object>();
        List<Attachment> lstImgAttachment = new List<Attachment>();
        ServiceContract sc = [Select Id,Signature_du_client__c, Status from ServiceContract where Id=:serConId];
        system.debug('## sc :'+sc);
        //Check Signature
        List<Attachment> lstA = [Select Id,Name from Attachment where name ='SignatureClient.png' and ParentId=:serConId order by createdDate desc limit 1];
        system.debug('##lstA:'+lstA.size());
        //If Signature attachment not exists, create new one
        try{ 
            if(lstA.size()==0){
            // Insert Signature attachment if Signature_Client__c not empty
                if(sc.Signature_du_client__c!=null){
                    String imgClientBase64 = sc.Signature_du_client__c;

                    if(imgClientBase64.startsWith('data:image/jpeg;base64,')) {
                        imgClientBase64 = imgClientBase64.removeStart('data:image/jpeg;base64,');
                    }
                    lstImgAttachment.add(
                        new Attachment (
                            ParentId = serConId, 
                            ContentType = 'image/png',
                            body = EncodingUtil.base64Decode(imgClientBase64),
                            Name = 'SignatureClient.png'
                        )
                    );       
                    system.debug('## lstImgAttachment :'+lstImgAttachment);
                    if(lstImgAttachment.size() > 0) {
                        insert lstImgAttachment;
                        mapOfResult.put('error',false);
                        mapOfResult.put('message', 'SUCESS!!!');
                    }
                }
            }
        }
		catch (Exception e) {
		    System.debug('error msg:' + e.getMessage());
	        mapOfResult.put('error',true);
	        mapOfResult.put('message',e.getMessage());
		}
    return mapOfResult;
    }
}