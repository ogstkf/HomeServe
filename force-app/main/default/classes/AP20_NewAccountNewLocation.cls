/**
 * @File Name          : AP20_NewAccountNewLocation.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 25/11/2019, 10:53:41
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    15/10/2019   AMO     Initial Version
**/
public with sharing class AP20_NewAccountNewLocation {

    static Schema.Location loc;
    
    
    public static void AccountLocation(List<Account> NewAccounts){
        
        Map<Id, Schema.Location> mapLocationtoAdd = new Map<Id, Schema.Location>();
        //List<Schema.Location> lstLocationtoAdd = new List<Schema.Location>();

        List<Account> lstAccSelected = [SELECT Id FROM Account WHERE Id in :NewAccounts];
        List<Account> lstAccUpdate = new List<Account>();

        for(Account acc : NewAccounts){
             loc = new Schema.Location(Name = acc.Name, LocationType = AP_Constant.virtuallocation);
             mapLocationtoAdd.put(acc.Id, loc);
             //lstLocationtoAdd.add(loc);
        }

        if(mapLocationtoAdd.values().size() > 0){
            Insert mapLocationtoAdd.values();
        }
        

        for(Account acc1 : lstAccSelected){
            acc1.Location__c = mapLocationtoAdd.get(acc1.id).id;
            lstAccUpdate.add(acc1);
        }

        if(lstAccUpdate.size() > 0){
            Update lstAccUpdate;
        }
        

    }
}