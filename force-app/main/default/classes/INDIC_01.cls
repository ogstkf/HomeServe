/**
 * Classe en cours de développement
 * Objectif : collecter les données permettant de maintenir l'indicateur extract_ratio_cts_cumul
 * qui comptabilise les visites hors contrat et les contrats consécutifs aux visites
 *
 * Usage :
 *      INDIC_O1 ind = new INDIC_O1();
 *      Map<String, Map<String, Integer>> donneesCumuleesFev2020 = ind.extractData(2019, 5); //(depuis le premier octobre 2019, 5 mois de cumul)
 *
 * @author Emmanuel Bruno <e.bruno@askelia.com>
 * @since 2020-02-19
 * @version 0.1
 */
public with sharing class INDIC_01 {

    /* Liste des visites pour lesquelles on n' a trouvé aucune ligne matchant l'équipement au jour de
        la visite, classées par agence */
    protected Map<String, List<ServiceAppointment>> saWithoutContract;

    //Liste des contrats démarrant après la visite, classés par agence
    protected Map<String,List<ContractLineItem>> newContracts;

    /**
     * Contructeur, initialisation des maps
     **/
    public INDIC_01() {
        saWithoutContract   = new Map<String, List<ServiceAppointment>>();
        newContracts        = new Map<String, List<ContractLineItem>>();
    }

    /**
     * La méthode principale qui renverra les données attendues
     * @param year  Integer Entier désignant au 01/10 de quelle année on démarre la sélection
     * @param month Integer Le nième mois de l'exercice : 1 pour octobre, 2 pour novembre, etc
     * @return Map<String, Map<String, Object>> Pour chaque numéro d'agence, le nombre de visites hors contrat et le nombre de contrats consécutifs.
     */
    public Map<String, Map<String, Object>> extractData(Integer year, Integer month){

        DateTime start = Date.newInstance(Year,10,01);  //on démarre au 01/10 dernier
        DateTime eend = start.addMonths(month); //on considère un an max de données
        /* Les mêmes en DateTime*/
        Date st = Date.valueOf(start);
        Date nd = Date.valueOf(eend);

        /*la requete va chercher les rendez-vous, les infos du work_order et les infos sur l'agence */
        /* Impossible to finish, Client works to be planned : est-ce une intervention */
        List<ServiceAppointment> lsa = [
        select
            ServiceTerritory.Agency_Code__c,
            ServiceTerritory.TerritorySector__c,
            ServiceTerritory.Name,
            Work_Order__r.AssetId, 
            ActualStartTime
        from ServiceAppointment
        Where
            (
                Status = 'Done OK' OR
                Status = 'Impossible to finish' OR
                Status = 'Done KO' OR
                Status = 'Client works to be planned' OR
                Status = 'Done client absent' OR
                Status = 'In Progress' OR
                Status = 'On Hold'
            )
            and ActualStartTime >= :start
            and ActualEndTime < :eend
            and ServiceTerritory.Agency_Code__c != null
            order by ActualStartTime
            ];

        //Set de tous les équipements concernés par des rdv dans l'année
        Set<Id> assets = new Set<Id>();
        for (ServiceAppointment sa : lsa){
            assets.add(sa.Work_Order__r.AssetId);
        }

        /*Liste de toutes les lignes de contrats (collectifs et particuliers) liées aux équipements pour
        lesquels il y a eu une visite dans l'année
        ici se pose vraiment la question du status en attente de paiement. Est-ce une visite sous ou hors
        contrat ?  si l'objectif est de savoir si une visite hors contrat est transformée en contrat pour
        mesurer l'efficacité commerciale il parait logique de considerer un en attente de paiement comme
        sous contrat : Je ne risque pas de faire un nouveau contrat, juste faire payer celui qui existe
        déjà */
        List<ContractLineItem> lcli = [
            select AssetId, EndDate, StartDate
            from ContractLineItem
            Where StartDate <= :nd And EndDate >=:st AND AssetId in :assets
            And isBundle__c = true
            ];

        /*On peut avoir plusieurs lignes de contrat pour un même asset car on a une
        nouvelle ligne tous les ans, on fait donc
        un Map avec Asset => liste de contrats*/
        Map<Id, List<ContractLineItem>> mpLines = new Map<Id, List<ContractLineItem>>();
        for (ContractLineItem li : lcli){
            if (!mpLines.containsKey(li.AssetId)) mpLines.put(li.AssetId, new List<ContractLineItem>());
            mpLines.get(li.AssetId).add(li);
        }

        /*on itère sur les SAs, on vérifie si il y a un contrat ou une ligne pour cet équipement, si oui on
         compare les dates*/
        for (ServiceAppointment sa: lsa){

            /*pas de ligne pour cet équipement*/
            if ( ! mpLines.containsKey(sa.Work_Order__r.AssetId) ) {
                addSaWithoutCt(sa);//on ajoute le SA à la liste des hors contrat
                continue;
            }

            /*des lignes existent pour l'équipement, on initialise des variables pour cet équipement : */
            Boolean validForVisit = false; /* on n'a pas de contrat le jour de la visite */
            ContractLineItem contractAfterVisit = null; /* on n'a pas trouvé de contrat consécutif à la visite */

            /* examinons les lignes trouvées pour cet asset */
            for (ContractLineItem cli : mpLines.get(sa.Work_Order__r.AssetId)){
                /* cas où le contrat finit avant la visite :
                    rien à faire, validForVisit et contractAfterVisit ne changent pas, on passe au contrat
                    suivant */
                /* cas où on avait un contrat valide la veille et le jour de la visite */
                if ( cli.StartDate < Date.valueOf(sa.ActualStartTime) && cli.EndDate >= Date.valueOf(sa.ActualStartTime) ){
                    validForVisit = true;
                    /* pas besoin d'examiner les éventuelles autres lignes, le contrat est valide lors de la
                    visite donc la visite n'est pas la cause du contrat suivant */
                    continue;
                }
                /* cas où le contrat démarre le jour de la visite ou après */
                if ( Date.valueOf(sa.ActualStartTime) <= cli.StartDate ){
                    contractAfterVisit = cli; /* ici dans le cas improbable où on aurait plusieurs contrats
                    ultérieurs à la visite, on n'en gardera qu'un seul (le dernier écrasera le précédent).
                    Ce n'est pas grave car on veut juste savoir qu'il y a eu un contrat, j'ai fait une liste de
                    CLI mais j'aurais aussi bien pu faire un simple Integer compteur*/
                }
            }
            /* Si le contrat était valide le jour de la visite, on ne s'occupe pas du contrat ultérieur, on
                ne fait rien car on ne s'intéresse qu'aux visites hors contrat
                Si par contre le contrat n'était pas valide, on ajoute le SA à la liste des visites hors
                contrat, et si en plus il y a un contrat après la visite, on l'ajoute à la liste des nouveaux
                contrats */
            if (!validForVisit){
                addSaWithoutCt(sa);
                System.debug(sa); //pour vérification
                if (contractAfterVisit != null){
                    addNewCt(sa, contractAfterVisit);
                    System.debug(contractAfterVisit); //pour vérification
                }
            }
        }

        /**Ici on a toutes les données, on prépare la réponse
         *
         * Un map avec key : numéro d'agence => value :
         *      Un map avec key : interv | contracts => value : nombre d'interventions hors contrats pour cette agence | nombre de contrats consécutifs pour cette agence
         **/
        Map<String, Map<String, Object>> response = new Map<String, Map<String, Object>>();
        for (String agence : saWithoutContract.keySet()){
            if (!response.containsKey(agence)){
                response.put(agence, new Map<String, Object>());
                response.get(agence).put('contracts', 0);
                response.get(agence).put('ville', ville(saWithoutContract.get(agence)[0].ServiceTerritory.Name, saWithoutContract.get(agence)[0].ServiceTerritory.TerritorySector__c));
                response.get(agence).put('region', sectorCode(saWithoutContract.get(agence)[0].ServiceTerritory.TerritorySector__c));
                
            }
            /* Dédoublonnage des visites : on n'en compte qu'une par équipement car on veut une
            efficacité commerciale à 100% si deux visites ont entrainé un contrat */
            Set<Id> eqs = new Set<Id>();
            for (ServiceAppointment sApp : saWithoutContract.get(agence))
            	eqs.add(sApp.Work_Order__r.AssetId);

            response.get(agence).put('interv', eqs.size());
        }
        for (String agence : newContracts.keySet()){
            if (!response.containsKey(agence)){
                response.put(agence, new Map<String, Object>());
                response.get(agence).put('interv', 0);
                response.get(agence).put('ville', ville(saWithoutContract.get(agence)[0].ServiceTerritory.Name, saWithoutContract.get(agence)[0].ServiceTerritory.TerritorySector__c));
                response.get(agence).put('region', sectorCode(saWithoutContract.get(agence)[0].ServiceTerritory.TerritorySector__c));
            }
            response.get(agence).put('contracts', newContracts.get(agence).size());
        }
        return response;
    }

    /**
     * Ajoute le SA dans la bonne liste en fonction du numéro d'agence
     * @param sa ServiceAppointment Le service appointment hors contrat qu'on veut retenir
     * @return void
    **/
    private void addSaWithoutCt(ServiceAppointment sa){
        if (!saWithoutContract.containsKey(sa.ServiceTerritory.Agency_Code__c)){
            saWithoutContract.put(sa.ServiceTerritory.Agency_Code__c, new List<ServiceAppointment>());
        }
        saWithoutContract.get(sa.ServiceTerritory.Agency_Code__c).add(sa);
    }

    /**
     * Ajoute la ligne de contrat dans la liste des contrats consécutifs
     * à une visite hors contrat.
     * @param sa ServiceAppointment La visite hors contrat précédant un contrat
     * @param cli ContractLineItem La ligne de contrat créé après la visite hors contrat
     * @return void
     **/
    private void addNewCt(ServiceAppointment sa, ContractLineItem cli){
        if (!newContracts.containsKey(sa.ServiceTerritory.Agency_Code__c)){
            newContracts.put(sa.ServiceTerritory.Agency_Code__c, new List<ContractLineItem>());
        }
        newContracts.get(sa.ServiceTerritory.Agency_Code__c).add(cli);
    }

    /**
    * Renvoie le code secteur tel qu'on le trouve dans les fichiers excel
    */
    public String sectorCode(String sector){
    	if (sector == 'National') return sector;
    	return sector.replaceAll('(\\w)[^- ]*[- ]*', '$1').toUpperCase();
    }

    public String ville(String agence, String sector){
    	if (sector == 'National') return agence;
    	return agence.replaceFirst('^Cham\\s', '').toUpperCase('fr');
    }
}