/**
 * @File Name          : LC23_AddContractLine.cls
 * @Description        : 
 * @Author             : Spoon Consulting
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 14-12-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/12/2019   RRJ     Initial Version
**/
public class LC23_AddContractLine {
    @AuraEnabled
    public static ConWrapper fetchContractDetails(String srvConId){ 
        return new ConWrapper([SELECT Id, Agency__c, RecordTypeId, Pricebook2Id, Pricebook2.Name, Asset__r.Equipment_type_auto__c, StartDate, EndDate, Contrat_1ere_annee__c FROM ServiceContract WHERE Id = :srvConId]);
    }

    //DMU 20201029 - Added condition on agency__c tec-202
    @AuraEnabled
    public static List<Pricebook2> fetchPb(ServiceContract srvCon){
        system.debug('*** srvCon: '+srvCon); 
        return [SELECT Id, Name FROM Pricebook2 WHERE IsActive = true AND RecordType.DeveloperName = 'Vente' AND Ref_Agence__c = true AND IsActive = true AND Agence__c =:srvCon.Agency__c];
    }

    @AuraEnabled
    public static List<PricebookEntry> fetchPriceBookEntries(String prcBkId, String eqpType, String searchText, ServiceContract servCon){

        ServiceContract sCon = [SELECT Id, Agency__c, RecordTypeId FROM ServiceContract WHERE Id = :servCon.Id];

        String strQuery = 'SELECT Id, Name, Product2.ProductCode, UnitPrice, Product2.Description, Product2.Family, Product2Id FROM PricebookEntry WHERE Pricebook2Id = :prcBkId AND Product2.IsActive = true AND IsActive = true AND Product2.Ferme_souscription__c = false ';
        if(String.isNotBlank(searchText)){
            strQuery += ' AND (Name LIKE \'%'+String.escapeSingleQuotes(searchText)+'%\' ';
            strQuery += ' OR Product2.ProductCode LIKE \'%'+String.escapeSingleQuotes(searchText)+'%\' )';
        }
        if(sCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId()){
            strQuery += '  AND Product2.IsBundle__c = true AND Product2.Family = \'Offre d’entretien individuelle\'' ;
        }else if(sCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_collectifs_publics').getRecordTypeId() ||
        sCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId()){
            //strQuery += ' AND Product2.Family = \'Offre d\\\'entretien collective\'';
            strQuery += ' AND Product2.RecordType.DeveloperName = \'Cham_offer\'';
        }
        strQuery += ' ORDER BY Name ASC ';
        List<PricebookEntry> lstPbe = Database.query(strQuery);
        System.debug('List of pricebookentries: ' + lstPbe);

        return lstPbe;
    }

    @AuraEnabled
    public static List<ContractLineItem> fetchCLI(String conId, String pbkId){
        return [SELECT Id, Product2.Name, UnitPrice, Quantity, PricebookEntryId, Product2Id, PricebookEntry.Pricebook2Id, PricebookEntry.UnitPrice, PricebookEntry.Name, PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Description, PricebookEntry.Product2.Family, ServiceContractId FROM ContractLineItem WHERE ServiceContractId = :conId and PricebookEntry.Pricebook2Id = :pbkId];
    }

    @AuraEnabled
    public static Object populateCLI(List<String> lstPbeId, String conId, String pbId){
        ServiceContract srvCon = [SELECT Id, Pricebook2Id, AccountId, Pricebook2.Name, Asset__r.Equipment_type_auto__c, StartDate, EndDate, Asset__c, Agency__c, Tax__c, RecordTypeId FROM ServiceContract WHERE Id = :conId LIMIT 1];

        List<PricebookEntry> lstBundlePBE = [SELECT Id, Pricebook2Id, Product2Id, UnitPrice, Name, Product2.Name, Product2.ProductCode, Product2.Description, Product2.Family FROM PricebookEntry WHERE Id IN :lstPbeId ORDER BY Name];

        Set<Id> setBundleProdId = new Set<Id>();
        for(PricebookEntry pbe: lstBundlePBE){
            setBundleProdId.add(pbe.Product2Id);
        }

        List<Product_Bundle__c> lstProdBundleParent = [SELECT Id, Bundle__c, Price__c, Optional_Item__c, Product__c FROM Product_Bundle__c WHERE Product__c IN :setBundleProdId];

        Set<Id> setBundleId = new Set<Id>();
        for(Product_Bundle__c prodBun : lstProdBundleParent){
            setBundleId.add(prodBun.Bundle__c);
        }

        List<Product_Bundle__c> lstProdBundleChild = [SELECT Id, Bundle__c, Price__c, Optional_Item__c, Product__c FROM Product_Bundle__c WHERE Bundle__c IN :setBundleId];

        List<Tarifs_Unifies_Locaux__c> lstTUL = [SELECT Cap_d_augmentation_max__c,Bundle_du_contrat__c,Agence__c,Tarif_Unifie_Local__c FROM Tarifs_Unifies_Locaux__c WHERE Agence__c = :srvCon.Agency__c AND Bundle_du_contrat__c IN:setBundleId AND Actuellement_actif_pour_creation__c = TRUE LIMIT 1];

        Set<Id> setProdId = new Set<Id>();
        Map<Id, Product_Bundle__c> mapProdProdBun = new Map<Id, Product_Bundle__c>();
        for(Product_Bundle__c prodBun : lstProdBundleChild){
            setProdId.add(prodBun.Product__c);
            mapProdProdBun.put(prodBun.Product__c, prodBun);
        }
        System.debug('### setProdId: '+setProdId.size());

        List<PricebookEntry> lstPBE = [SELECT Id, Pricebook2Id, Product2Id, UnitPrice, Name, Product2.Name, Product2.ProductCode, Product2.Description, Product2.Family, Product2.IsBundle__c FROM PricebookEntry WHERE Product2Id IN :setProdId AND Pricebook2Id = :pbId ORDER BY Name];

        // CT-1401
        // List<PricebookEntry> lstPbeFilterProdFamily = new List<PricebookEntry>();
        // for(PricebookEntry pbe : lstPbe){
        //     if(srvCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId()){
        //         if(pbe.Product2.Family == 'Offre d’entretien individuelle'){
        //             lstPbeFilterProdFamily.add(pbe);
        //         }
        //     }else if(srvCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_collectifs_publics').getRecordTypeId() ||
        //     srvCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId()){
        //         if(pbe.Product2.Family == 'Offre d\'entretien collective'){
        //             lstPbeFilterProdFamily.add(pbe);
        //         }
        //     }
        // }

        List<ContractLineItem> lstCLI = [SELECT Id, Product2.Name, UnitPrice, Quantity, Discount, AssetId, PricebookEntryId, Product2Id, PricebookEntry.Pricebook2Id, PricebookEntry.UnitPrice, PricebookEntry.Name, PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Description, PricebookEntry.Product2.Family, StartDate, EndDate,VE_Status__c FROM ContractLineItem WHERE ServiceContractId = :srvCon.Id AND PricebookEntry.Pricebook2Id = :pbId];

		Map<String, Integer> mapProductQte = new Map<String, Integer>();

		if (
			(srvCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_collectifs_publics').getRecordTypeId() ||
			srvCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId())
		) {
			for (ContractLineItem cli : lstCLI) {
				if (mapProductQte.containsKey(cli.Product2.Name)) {
					integer count = mapProductQte.get(cli.Product2.Name) + 1;
					mapProductQte.put(cli.Product2.Name, count);
				} else {
					mapProductQte.put(cli.Product2.Name, 1);
				}
			}
		}

        Map<Id, ContractLineItem> mapPbeCLI = new Map<Id, ContractLineItem>();
        for(ContractLineItem cli : lstCLI){
            mapPbeCLI.put(cli.PricebookEntryId, cli);
        }

        List<CliWrapper> lstNewCliWrap = new List<CliWrapper>();
        for(PricebookEntry pbe: lstPbe){
            CliWrapper cliWrap = new CliWrapper();
            cliWrap.isOption = false;
			cliWrap.prodId = pbe.Product2Id;
            if(mapPbeCLI.containsKey(pbe.Id)){
                cliWrap.cli = mapPbeCLI.get(pbe.Id);
				cliWrap.assetId = mapPbeCLI.get(pbe.Id).AssetId;
                cliWrap.isExisting = true;
            }else{
                ContractLineItem cli = new ContractLineItem();
                cli.PricebookEntryId = pbe.Id;
                cli.UnitPrice = pbe.UnitPrice;
                // cli.Product2Id = pbe.Product2Id;
                cli.ServiceContractId = srvCon.Id;
                cli.StartDate = srvCon.StartDate;
                cli.EndDate = srvCon.EndDate;
                cli.Quantity = 1;
                cli.Discount = null;
                cliWrap.cli = cli;
                cliWrap.isExisting = false;
            }
            if(mapProdProdBun.containsKey(pbe.Product2Id)){
                Product_Bundle__c pb = mapProdProdBun.get(pbe.Product2Id);
                cliWrap.isOption = pb.Optional_Item__c;
                if(pb.Optional_Item__c == true && !mapPbeCLI.containsKey(pbe.Id)){
                    cliWrap.cli.Quantity = null;
                    cliWrap.cli.UnitPrice = pb.Price__c;
                }
                if(pb.Optional_Item__c == false && (pb.Price__c == 0 || pb.Price__c == null)){
                    cliWrap.isGuaranty = true;
                    cliWrap.cli.UnitPrice = 0;
                    cliWrap.cli.Quantity = 1;
                }
            }

            if(pbe.Product2.IsBundle__c && lstTUL.size()>0){
                if(lstTUL[0].Tarif_Unifie_Local__c >= pbe.UnitPrice){
                    cliWrap.cli.UnitPrice = lstTUL[0].Tarif_Unifie_Local__c;
                }else{
                    // cliWrap.cli.UnitPrice = lstTUL[0].Tarif_Unifie_Local__c;
                    Decimal remise = ((pbe.UnitPrice - lstTUL[0].Tarif_Unifie_Local__c)/pbe.UnitPrice) * 100;
                    remise = remise.setScale(2);
                    cliWrap.cli.Discount = remise;
                }
            }
            cliWrap.name = pbe.Product2.Name;
            cliWrap.isBundle = pbe.Product2.IsBundle__c;
            cliWrap.cli.AssetId = srvCon.Asset__c;
            cliWrap.prixHt = 0;
            if(cliWrap.cli.Quantity != null && cliWrap.cli.UnitPrice != null){
                cliWrap.prixHt = cliWrap.cli.Quantity * cliWrap.cli.UnitPrice;
            }
            if(cliWrap.cli.Discount != null){
                cliWrap.prixHt = cliWrap.prixHt * (1 - (cliWrap.cli.Discount/100));
            }

            cliWrap.tva = srvCon.Tax__c == null? 0 : Decimal.valueOf(srvCon.Tax__c);
            cliWrap.prixTTC = cliWrap.prixHt;
            if(cliWrap.tva != null){
               cliWrap.prixTTC = cliWrap.prixHt * (1 + (cliWrap.tva/100));
            }

            
			if (
				(srvCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_collectifs_publics').getRecordTypeId() ||
				srvCon.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId())
			) {
				System.debug('@@ Set Collective and is Active True');
				cliWrap.cli.Collective_Account__c = srvCon.AccountId;
				cliWrap.cli.IsActive__c = true;
				cliWrap.cli.StartDate = srvCon.StartDate;
				cliWrap.cli.EndDate = srvCon.EndDate;
				cliWrap.QteContratCollectif = mapProductQte.get(pbe.Product2.Name);
			}

            lstNewCliWrap.add(cliWrap);
        }

/*
		for (CliWrapper cliWrap : lstNewCliWrap) {
			if (cliWrap.isBundle) {
				for (Product_Bundle__c pbun : lstProdBundleParent) {
					if (pbun.Product__c == cliWrap.prodId) {
						cliWrap.bundleId = pbun.Bundle__c;
					}
				}
			}
			if (cliWrap.isGuaranty || cliWrap.isOption) {
				for (Product_Bundle__c prodBun : lstProdBundleChild) {
					if (prodBun.Product__c == cliWrap.prodId) {
						cliWrap.bundleId += prodBun.Bundle__c + ';';
					}
				}
			}
		}*/

        lstNewCliWrap.sort();
        return lstNewCliWrap;
        
    }

    @AuraEnabled
    public static Object saveCLI(String JSONlstWrap, String conWrapper, Pricebook2 pb, Boolean isContrat1ere){
        
        conWrapper conWrap = (conWrapper) JSON.deserialize(conWrapper, conWrapper.class);
        ServiceContract con = conWrap.con;
        Savepoint sp = Database.setSavepoint();
        try{
            List<CliWrapper> lstCLIwrap = (List<CliWrapper>)JSON.deserialize(JSONlstWrap, List<CliWrapper>.class);
            System.debug('##### lstCLIwrap: '+lstCLIwrap.size());
            System.debug('##### lstCLIwrap: '+lstCLIwrap);

            List<ContractLineItem> lstCLItoSave = new List<ContractLineItem>();
            List<ContractLineItem> lstCLItoDelete = new List<ContractLineItem>();

            for(CliWrapper cliWrap : lstCLIwrap){
                if (conWrap.type == 'collectif') {
                    if (cliWrap.action == 'upsert') {
                        cliWrap.cli.TECH_DoNotPopulateBundle__c = true;
                        cliWrap.cli.IsActive__c = true;
						lstCLItoSave.add(cliWrap.cli);
					} else if (cliWrap.action == 'delete') {
						cliWrap.cli.isActive__c = false;
						cliWrap.cli.UnitPrice = 0;
						lstCLItoDelete.add(cliWrap.cli);
					}
                }
                else {
                    if(cliWrap.action == 'upsert' && cliWrap.cli.Quantity != null && cliWrap.cli.Quantity != 0){
                        // System.debug('##### cliWrap.cli: '+cliWrap.cli.Quantity);
                        if(cliWrap.isBundle){
                            cliWrap.cli.TECH_DoNotPopulateBundle__c = true;
                            if (isContrat1ere != con.Contrat_1ere_annee__c) {
                                if (isContrat1ere)
                                    cliWrap.cli.VE_Status__c = 'VE non nécessaire';
                                else
                                    cliWrap.cli.VE_Status__c = 'Not Planned';
                            }
                        }
                        lstCLItoSave.add(cliWrap.cli);
                    }else if(cliWrap.action == 'delete' || (cliWrap.cli.Id != null && (cliWrap.cli.Quantity == null || cliWrap.cli.Quantity == 0))){
                        lstCLItoDelete.add(cliWrap.cli);
                    }
                }
            }

            if (conWrap.type != 'collectif' && (isContrat1ere != con.Contrat_1ere_annee__c || con.Pricebook2Id == null)) {
                if (isContrat1ere != con.Contrat_1ere_annee__c)
                    con.Contrat_1ere_annee__c = isContrat1ere;
                    
                if(con.Pricebook2Id == null)
                    con.Pricebook2Id = pb.Id;
                
                update con;
            }


            // System.debug('####### lstCLItoDelete: '+lstCLItoDelete.size());
            System.debug('####### lstCLItoDelete: '+lstCLItoDelete);
            if(lstCLItoDelete.size()>0){
                delete lstCLItoDelete;
            }
            // System.debug('####### lstCLItoSave: '+lstCLItoSave.size());
            System.debug('####### lstCLItoSave: '+lstCLItoSave);
            if(lstCLItoSave.size()>0){
                upsert lstCLItoSave;
            }
        }catch(Exception e){
            System.debug('#### Save CLI ERROR: '+e.getMessage());
            Database.rollback(sp);
            throw new AuraHandledException(e.getMessage());
        }
        // System.debug('#### done LC23');
        return con.Id;
    }

    
	@AuraEnabled
	public static List<AstWrapper> buildCliCollectif(String conId, String JSONlstWrap) {
		List<CliWrapper> lstCLIwrap = (List<CliWrapper>) JSON.deserialize(JSONlstWrap, List<CliWrapper>.class);
		// lstCLIwrap.sort();
		System.debug('##### lstCLIwrap: ' + lstCLIwrap.size());

		// Recuperer le lot rataché au contrat
		ServiceContract con = [SELECT Id, Logement__c, Lot__c, AccountId FROM ServiceContract WHERE Id = :conId];
		if (con.Lot__c == null) {
			throw new AP_Constant.CustomException('Le contrat n\'est pas rattaché à un lot');
		}
		System.debug('### con: ' + con);
		// Recuperer les logements rataché au lot
		List<Logement__c> lstLog = [SELECT Id FROM Logement__c WHERE Sous_Lot__r.Lot__c = :con.Lot__c];
		System.debug('### lstLog: ' + lstLog);
		// recuperer les équipements rattaché au logements
		List<Asset> lstAst = [
			SELECT
				Id,
				Name,
				IDEquipmenta__c,
				Logement__c,
				Logement__r.Name,
				Logement__r.Address__c,
				Logement__r.Imm_Res__c,
				Logement__r.Owner_Address__c,
				Logement__r.Door__c,
				Logement__r.Floor__c,
				Logement__r.Sous_Lot__c,
				Logement__r.Sous_Lot__r.Name,
				Logement__r.Owner__r.LastName,
				Logement__r.Owner__r.FirstName,
				Logement__r.Inhabitant__r.FirstName,
				Logement__r.Inhabitant__r.LastName,
				Logement__r.Inhabitant__c
			FROM Asset
			WHERE Logement__c IN :lstLog AND Status = 'Actif' // IMN-409 ARM:27/07/2021
			ORDER BY Logement__r.Sous_Lot__c
		];
		System.debug('### lstAst: ' + lstAst);
		// Recuperer les lignes de contrats existants sur les équipements.
		List<ContractLineItem> lstCLI = [
			SELECT
				Id,
				IsBundle__c,
				Product2.Name,
				AssetId,
				Asset.Name,
				UnitPrice,
				IsActive__c,
				Quantity,
				Discount,
				PricebookEntryId,
				Product2Id,
				PricebookEntry.Pricebook2Id,
				PricebookEntry.UnitPrice,
				PricebookEntry.Name,
				PricebookEntry.Product2.ProductCode,
				PricebookEntry.Product2.Description,
				PricebookEntry.Product2.Family,
				StartDate,
				EndDate,
				TVA__c,
				ServiceContractId,
				ServiceContract.Tax__c
			FROM ContractLineItem
			WHERE ServiceContractId = :conId AND AssetId IN :lstAst
		];
		Map<String, ContractLineItem> mapAstProdCli = new Map<String, ContractLineItem>();
		for (ContractLineItem cli : lstCLI) {
			mapAstProdCli.put(cli.AssetId + '-' + cli.Product2Id, cli);
		}
		System.debug(mapAstProdCli);
		// build list of asset wrapper
		List<AstWrapper> lstAstWrap = new List<AstWrapper>();
		List<Decimal> lstTotalLigne = new List<Decimal>(lstCLIwrap.size());
		for (Asset ast : lstAst) {
			AstWrapper astWrap = new AstWrapper();
			astWrap.ast = ast;
			List<CliWrapper> lstAstCliWrap = new List<CliWrapper>();
			Decimal prixTotalColonne = 0;
			integer i = 0;
			for (CliWrapper cliWrap : lstCLIwrap) {
				if (cliWrap.prixHt <> -1) {
					CliWrapper astCliWrap = new CliWrapper(cliWrap);

					astCliWrap.assetId = ast.Id;
					lstTotalLigne[i] = lstTotalLigne[i] == null ? 0 : lstTotalLigne[i];
					System.debug((ast.Id + '-' + cliWrap.prodId));
					if (mapAstProdCli.containsKey(ast.Id + '-' + cliWrap.prodId)) {
						astCliWrap.cli = mapAstProdCli.get(ast.Id + '-' + cliWrap.prodId);
						astCliWrap.cli.Quantity = 1;
						astCliWrap.isSelected = mapAstProdCli.get(ast.Id + '-' + cliWrap.prodId).isActive__c;
						astCliWrap.cli.UnitPrice = astCliWrap.cli.UnitPrice == 0 ? cliWrap.cli.UnitPrice : astCliWrap.cli.UnitPrice;
						astCliWrap.isExisting = true;
						astCliWrap.isBundle = astCliWrap.cli.IsBundle__c;
						if (astCliWrap.isBundle) {
							astWrap.tva = astCliWrap.cli.TVA__c;
						}
						prixTotalColonne += cliWrap.prixHt;
						lstTotalLigne[i] += cliWrap.prixHt;
					} else {
						astCliWrap.cli = new ContractLineItem(
							PricebookEntryId = cliWrap.cli.PricebookEntryId,
							UnitPrice = cliWrap.cli.UnitPrice,
							ServiceContractId = cliWrap.cli.ServiceContractId,
							StartDate = cliWrap.cli.StartDate,
							EndDate = cliWrap.cli.EndDate,
							Quantity = 0,
							Discount = cliWrap.cli.Discount
						);
					}
					astCliWrap.cli.Collective_Account__c = ast.Logement__r.Inhabitant__c; //IMN-409 : ARM 27/07/2021
					lstAstCliWrap.add(astCliWrap);
					i++;
				}
			}
			// astWrap.GlobalTva = '0';
			astWrap.IdLigne = ast.Id;
			astWrap.lstCliWrap = lstAstCliWrap;
			astWrap.totalColone = prixTotalColonne;
			astWrap.lstTotalLigne = lstTotalLigne;
			lstAstWrap.add(astWrap);
		}
		SYstem.debug(lstAstWrap);
		return lstAstWrap;
	}

	public class ConWrapper {
		@AuraEnabled
		public ServiceContract con;
		@AuraEnabled
		public String type;

		public ConWrapper() {
		}

		public ConWrapper(ServiceContract con) {
			this.con = con;
			if (
				(con.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_collectifs_publics').getRecordTypeId() ||
				con.RecordTypeId == Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId())
			) {
				this.type = 'collectif';
			} else {
				this.type = 'individuel';
			}
		}
	}

    public class CliWrapper implements Comparable{
        @AuraEnabled public ContractLineItem cli;
        @AuraEnabled public String name;
        @AuraEnabled public Boolean isOption;
        @AuraEnabled public Boolean isBundle;
        @AuraEnabled public Boolean isExisting;
        @AuraEnabled public Boolean isGuaranty;
		@AuraEnabled public Boolean isSelected;
		@AuraEnabled public Boolean isDisabled;
        @AuraEnabled public Decimal prixHt;
        @AuraEnabled public Decimal prixTTC;
        @AuraEnabled public Decimal tva;
        @AuraEnabled public String action;
		@AuraEnabled public String bundleId;
		@AuraEnabled public String prodId;
		@AuraEnabled public String assetId;
		@AuraEnabled public Integer QteContratCollectif;

		public CliWrapper() {
			this.cli = new ContractLineItem();
			this.isBundle = false;
		}

		public CliWrapper(ContractLineItem cli) {
			this.cli = cli;
		}
        
		public CliWrapper(CliWrapper cliWrap) {
			this.name = cliWrap.name;
			this.isOption = cliWrap.isOption;
			this.isGuaranty = cliWrap.isGuaranty;
			this.isBundle = cliWrap.isBundle;
			this.isExisting = false;
			this.isSelected = false;
			this.isDisabled = false;
			this.prixHt = cliWrap.prixHt;
			this.prixTTC = cliWrap.prixTTC;
			this.tva = cliWrap.tva;
			this.prodId = cliWrap.prodId;
		}

        public Integer compareTo(Object compareTo) {
            CliWrapper wrapLine = (CliWrapper)compareTo;
            if((wrapLine.isBundle == true && isGuaranty == true) || (wrapLine.isGuaranty == true && isOption == true)){
                return 1;
            }
            if((wrapLine.isGuaranty == true && isGuaranty == true) || (wrapLine.isBundle == true && isBundle == true) || (wrapLine.isOption == true && isOption == true )){
                return 0;
            }
            if((wrapLine.isGuaranty == true && isBundle == true) || (wrapLine.isOption == true && isGuaranty == true)){
                return -1;
            }
            return -1;
        }
    }

    
	public class AstWrapper {
		@AuraEnabled
		public List<CliWrapper> lstCliWrap;
		@AuraEnabled
		public Asset ast;
		@AuraEnabled
		public String tva;
		@AuraEnabled
		public string IdLigne;
		@AuraEnabled
		public Decimal totalColone;
		@AuraEnabled
		public List<Decimal> lstTotalLigne;

		public AstWrapper() {
			this.totalColone = 0;
		}
	}

}