/**
 * @File Name          : DocumentDownloadManager.cls
 * @Description        : 
 * @Author             : Irfaan Coonjbeeharry
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 09/04/2020, 16:38:29
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    09/04/2020   ChangeMeIn@UserSettingsUnder.SFDoc     0.1
**/

@RestResource(urlMapping='/v2/records/*/file/attached')
//-----------------------------------------------------------------------------------------------------------------
// Téléchargement du document
// GET
//
//Le paramètre recordId est l'identifiant du document recherché,
//Il correspond au champ STD_RECORD_ID dans les métadonnées du document.
//-----------------------------------------------------------------------------------------------------------------

global with sharing class WS01_RECORD_FILE_DOWNLOAD {
  @HttpGet
    global static Document  getRecords(){
      RestRequest req = RestContext.request;
      RestResponse res = RestContext.response;
      res.addHeader('Content-Type', 'application/octet-stream');
      Blob body;

      List<String> urlParams = req.requestURI.split('/');
      String recordId = req.requestURI.substringBetween('v2/records/', '/file/attached');
   
    List<Document> documents = [select Body from Document where Id=:recordId];
    if(documents.size()==0) return null;
   
  return documents[0];
}
}