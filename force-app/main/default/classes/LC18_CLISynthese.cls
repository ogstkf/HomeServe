/**
 * @File Name          : LC18_CLISynthese
 * @Description        : Controller for LC18_CLISynthese.cmp
 * @Author             : Spoon Consulting (KRO)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 31/01/2020, 11:11:20
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         20-01-2020     		KRO         Initial Version
 * 1.1         22-01-2020     		DMU         Added logic filter query
 * 1.2         04-06-2020           DMU         Commented class due to HomeServe Project 
**/

public class LC18_CLISynthese {
/*    
    @AuraEnabled
    public static List<ContractLineItemWrapper> getContractLineItem(integer index,integer currYear){
        system.debug('**** in LC18_CLISynthese getContractLineItem stype:  ');

        user userConnected = [SELECT id, companyName FROM User WHERE id=: UserInfo.getUserId()];
        string companyName = userConnected.companyName;
        string strCompanyName = '%'+companyName+'%';
        Date dtoday = system.today();

        FSL_SearchVesPlanning_BA fx = new FSL_SearchVesPlanning_BA();
        Date minDateMplus1 = Date.newInstance(currYear, index , 01);    
        Date maxDateMplus1 = Date.newInstance(minDateMplus1.year(), minDateMplus1.month(), fx.getLastDayOfMonth(minDateMplus1.month()));
        System.debug('Min Date of Month plus 1: ' + minDateMplus1);
        System.debug('Max Date of Month plus 1: ' + maxDateMplus1);


        Date minDateMplus2 = minDateMplus1.addMonths(1);
        Date maxDateMplus2 = Date.newInstance(minDateMplus2.year(), minDateMplus2.month() ,fx.getLastDayOfMonth(minDateMplus2.month()));
        System.debug('Min Date of Month plus 2: ' + minDateMplus2);
        System.debug('Max Date of Month plus 2: ' + maxDateMplus2);

        Date dM = minDateMplus1.addMonths(-1);
        Date maxDatedM = Date.newInstance(dM.year(), dM.month(), fx.getLastDayOfMonth(dM.month()));
        System.debug('Min Date of Month M: ' + dM);
        System.debug('Max Date of Month M: ' + maxDatedM);

        List<ContractLineItem> items = [SELECT Id, 
                                        ServiceContract.Logement__c,
                                        ServiceContract.Name, 
                                        ServiceContract.Type__c, 
                                        ServiceContract.Logement__r.Name, 
                                        ServiceContract.Logement__r.TECH_HomeName_External_Id__c, 
                                        ServiceContract.Logement__r.Nom_du_Collectif__c, 
                                        ServiceContract.Account.AccountNumber, 
                                        ServiceContract.Account.ClientNumber__c, 
                                        ServiceContract.Account.Name,
                                        VE_Max_Date__c, 
                                        VE_Min_DAte__c 
                                        FROM ContractLineItem 
                                        WHERE ServiceContract.Type__c = 'Collective' 
                                        AND (ServiceContract.Contract_Status__c = 'Active' OR ServiceContract.Contract_Status__c = 'Pending Payment' OR ServiceContract.Contract_Status__c = 'Pending first visit' OR ServiceContract.Contract_Status__c = 'En attente de renouvellement' OR ServiceContract.Contract_Status__c = 'Actif - en retard de paiement')
                                        //AND ServiceContract.Contract_Renewed__c = true
                                        AND (VE_Status__c = 'Not Planned' OR VE_Status__c = 'On Going')
                                        AND Customer_Absence_Range__c = true
                                        AND VE__c = true                                        
                                        AND ServiceContract.Agency__r.Name like  :strCompanyName
                                        AND TECH_IsVECounterLessThanVETarget__c = true
                                        AND Product2.IsBundle__c = true
                                        AND EndDate > :dtoday
                                        AND (
                                                VE_Max_Date__c < :maxDatedM 
                                                OR (CALENDAR_MONTH(VE_Max_Date__c) =:maxDateMplus1.month() AND CALENDAR_YEAR(VE_Max_Date__c) = :maxDateMplus1.year())
                                                OR (CALENDAR_MONTH(VE_Max_Date__c) = :maxDateMplus2.month() AND CALENDAR_YEAR(VE_Max_Date__c) = :maxDateMplus2.year())
                                                OR (CALENDAR_MONTH(VE_Max_Date__c) > :maxDateMplus2.month() AND CALENDAR_YEAR(VE_Max_Date__c) > :maxDateMplus2.year() AND VE_Min_DAte__c <= :maxDateMplus1)
                                            )
                                        	ORDER BY VE_Max_Date__c, ServiceContract.Type__c, ServiceContract.Logement__c ASC
                                        ];
        
        
        
        List<SObject> aggr = [SELECT count(id), ServiceContract.Logement__c FROM ContractLineItem GROUP BY ServiceContract.Logement__c ];
        Map<Id,Integer> clis = new Map<Id,Integer>();
        for(SObject o : aggr){
            Integer count = (Integer)o.get('expr0');
            Id logement = (Id)o.get('Logement__c');
            clis.put(logement, count);
        }
        
        Map<Id,ContractLineItemWrapper> result = new Map<Id,ContractLineItemWrapper>();
        if(Test.isRunningTest()){
            items = [SELECT Id, ServiceContract.Logement__c,ServiceContract.Name, ServiceContract.Type__c, ServiceContract.Logement__r.Name, ServiceContract.Logement__r.TECH_HomeName_External_Id__c,  ServiceContract.Logement__r.Nom_du_Collectif__c, 
                                        ServiceContract.Account.AccountNumber, 
                                        ServiceContract.Account.ClientNumber__c, 
                                        ServiceContract.Account.Name,
                                        VE_Max_Date__c, 
                                        VE_Min_DAte__c 
                                        FROM ContractLineItem ];
        }
        for(ContractLineItem item : items){
            Id logement = item.ServiceContract.Logement__c;
            if(!result.containsKey(logement)){
                ContractLineItemWrapper wrapper = new ContractLineItemWrapper();
                wrapper.item = item;
                wrapper.cli = clis.get(logement);
                result.put(logement,wrapper);
            }
            
        }
        return result.values(); 
        
    }    
    
    public class ContractLineItemWrapper{
        @AuraEnabled
        public ContractLineItem item;
        @AuraEnabled
        public Integer cli;
    }

    @AuraEnabled
    public static List<ServiceTerritoryMember> getResource(integer index,integer currYear){
        system.debug('** in getResource');
        system.debug('** index: '+index);
        system.debug('** currYear: '+currYear);

        FSL_SearchVesPlanning_BA fx = new FSL_SearchVesPlanning_BA();

        //index = index + 1;
        // Date dStart = Date.newInstance(currYear, index , 01);
        // Date dEnd = Date.newInstance(currYear, index , fx.getLastDayOfMonth(01));

        user userConnected = [SELECT id, companyName FROM User WHERE id=: UserInfo.getUserId()];
        string companyName = userConnected.companyName;
        string likeQueryCN = '%' + companyName + '%' ;
        system.debug('*** likeQueryCN: '+likeQueryCN);

        List<ServiceTerritoryMember> lstSTM = [SELECT id, ServiceResource.Name, ServiceTerritory.Name,ServiceResource.IsActive
                                                FROM ServiceTerritoryMember 
                                                WHERE ServiceTerritory.Name like :likeQueryCN
                                                AND ServiceResource.IsActive=true 
                                                AND ServiceTerritory.FSL__TerritoryLevel__c = 0
                                                //AND EffectiveStartDate <= :dStart
                                                //AND EffectiveEndDate >= :dEnd                                             
                                              ];
                                              
        system.debug('** lstSTM size: '+lstSTM.size());
        return lstSTM;
    }

    @AuraEnabled
    public  static Map<String, Object> saveVE (String JSONlstWrap){
        system.debug('** in generateVE JSONlstWrap: '+ JSONlstWrap);

        List<cliWrapper> lstCLIWrap = (List<cliWrapper>)JSON.deserialize(JSONlstWrap, List<cliWrapper>.class);
        System.debug('JSON.deserialize()'+lstCLIWrap );
        System.debug('ServiceContract.Name: '+lstCLIWrap[0].ServiceContract.Name );
        System.debug('Logement__c: '+lstCLIWrap[0].ServiceContract.Logement__c );
        System.debug('res1Id: '+lstCLIWrap[0].res1Id );
        System.debug('startDate: '+lstCLIWrap[0].startDate );
        System.debug('endDate: '+lstCLIWrap[0].endDate );

        Map<String, object> mapRes = new Map<String, object>();
        String error; 
        Map<String, cliWrapper> mapLogementIdCliWrp = new Map<String, cliWrapper>(); //map of LogementId, ccliWrapperli
        Map<String, String> mapIDLogementDate = new Map<String, String>();
        Map<String, list<String>> mapIDLogementServiceReslst= new Map<String, list<String>>();

        for(cliWrapper wr : lstCLIWrap){
            mapLogementIdCliWrp.put(wr.ServiceContract.Logement__c, wr);
            mapIDLogementDate.put(wr.ServiceContract.Logement__c, wr.startDate+'#'+wr.endDate);

            list<String> lstIdResources = new list<String>();            
            if(wr.res1Id!=null){
                lstIdResources.add(wr.res1Id);
            }
            if(wr.res2Id!=null){
                lstIdResources.add(wr.res2Id);
            }
            if(wr.res3Id!=null){
                lstIdResources.add(wr.res3Id);
            }
            mapIDLogementServiceReslst.put(wr.ServiceContract.Logement__c, lstIdResources);
        }
        system.debug('**mapLogementIdCliWrp:'+ mapLogementIdCliWrp.size());
        system.debug('**mapIDLogementDate:'+ mapIDLogementDate.size());
        system.debug('**mapIDLogementServiceReslst:'+ mapIDLogementServiceReslst.size());

        list<ContractLineItem> lstCLI = [SELECT id FROM ContractLineItem WHERE ServiceContract.Logement__c in:mapLogementIdCliWrp.keySet() ];
        if(Test.isRunningTest()){
            lstCLI = [SELECT id FROM ContractLineItem];
        }
        system.debug('**lstCLI:'+ lstCLI.size());
        list<Id> lstIds = new list<Id>();
        if(lstCLI.size()>0){
            for(ContractLineItem cli : lstCLI){
                lstIds.add(cli.Id);
            }
        }
        if(lstIds.size()>0){
            try{
                // Id batchJobId = '';
                Id batchJobId = Database.executeBatch(new FSL_SearchVesPlanning_BA(lstIds,'collectif', mapIDLogementDate, mapIDLogementServiceReslst), 200);
                System.debug('## Batch Id: '+ batchJobId);
                
                mapRes.put('error', false);
                mapRes.put('batch_id' , batchJobId);
                mapRes.put('msg' , 'Batch lancé avec succès');
            }catch(Exception e){
                error = e.getMessage();
                System.debug('## error : ' + error);
                mapRes.put('error', true);
                mapRes.put('msg', error);
            }
            mapRes.put('msg' , 'Lancement de la creation des Requêtes');
        }else{
            mapRes.put('msg' , 'Merci de selectioner une ligne pour lancer la creation des Requêtes');
        }            
        
        return mapRes;
    }
    
    public class cliWrapper {
        @AuraEnabled public String Id;
        @AuraEnabled public String ServiceContractId;
        @AuraEnabled public String res1Id;
        @AuraEnabled public String res2Id;
        @AuraEnabled public String res3Id;
        @AuraEnabled public ServiceContract ServiceContract;
        @AuraEnabled public Date startDate;
        @AuraEnabled public Date endDate;        
    }

       public static Map<Id, Decimal> calculateCAPTheorique (Integer mois, Integer Yr, Set<Id> ServiceTerritoryIds){
        system.debug('*** in calculateCAPTheorique');

        FSL_SearchVesPlanning_BA fx = new FSL_SearchVesPlanning_BA();

        Date startMonth = Date.newInstance(Yr, mois, 01);         
        Date endMonth = Date.newInstance(Yr, mois, fx.getLastDayOfMonth(mois)); 

        // Set<Id> ServiceTerritoryIds = new set<Id>{'0Hh6E0000008h0ISAQ'};
        Map<Id,Id> serviceMemberMap  = fx.getOperatingHours(ServiceTerritoryIds); 
        System.debug('## serviceMember map: ' + serviceMemberMap);
        Map<Id,Integer> operatingHoursMonthMap = new Map<Id,Integer>();
        Map<Id, Decimal> mapMinutesInMonth = new Map<Id, decimal>();
        Map<Id, Integer> mapAvgMinsPerDay = new Map<Id, Integer>();
        Map<Id, Integer> mapDaysInMonth = new Map<Id, Integer>();
        //get Time Slot to Operating Hours
        List<OperatingHours> operatingHours = [SELECT Id,Name, (SELECT DayOfWeek,StartTime, EndTime FROM TimeSlots) 
                                              FROM OperatingHours 
                                              WHERE Id In: serviceMemberMap.values()];
        System.debug('##operating hours retrieved: ' + operatingHours);
        
        for(OperatingHours op: operatingHours) {
            List <TimeSlot> daysList = new List <TimeSlot>();
            Integer count = 0;
            for(TimeSlot ts: op.TimeSlots){
               count = count + (fx.getTimeDif(ts.EndTime , ts.StartTime));
            }
            system.debug('***in count: '+count);
            operatingHoursMonthMap.put(op.Id,count);  
        }
        System.debug('## operating hours month map: ' + operatingHoursMonthMap);
        


        // get number of weeks in month
        Decimal weeksInMonth = weeksInMonth(mois, Yr); 

        //minutes per month 
        for(Id opId: operatingHoursMonthMap.keySet()){
            mapMinutesInMonth.put(
                opId, 
                operatingHoursMonthMap.get(opId) * weeksInMonth
            );

            mapAvgMinsPerDay.put(
                opId,
                operatingHoursMonthMap.get(opId) / 5
            );
			Integer dividend = mapAvgMinsPerDay.get(opId);
            if(Test.isRunningTest()){
                dividend = 1;
            }
            mapDaysInMonth.put(
                opId, 
                Integer.valueOf(mapMinutesInMonth.get(opId) / dividend)
            );
        }
        
        System.debug('## mapMinutesInMonth: ' + mapMinutesInMonth);
        System.debug('## mapDaysInMonth: ' + mapDaysInMonth);
        //get average per week

        return mapDaysInMonth; 
    }

    public static Decimal weeksInMonth(Integer mois, Integer Yr){
        return Date.daysInMonth(Yr, mois) / 7.0;
    }

    public static Map<Id, Decimal> calculateCapDejaAlloue(Integer mois, Integer Year, set<Id> serviceTerritoryIds){
        List<AggregateResult> lstAssRes = [SELECT ServiceResource.Id, SUM(ServiceAppointment.DurationInMinutes) 
        FROM AssignedResource 
        where ServiceAppointment.ServiceTerritoryId IN :serviceTerritoryIds 
        AND CALENDAR_YEAR(ServiceAppointment.SchedStartTime)=2019  
        AND CALENDAR_MONTH(ServiceAppointment.SchedStartTime)=11   
        AND CALENDAR_YEAR(ServiceAppointment.SchedEndTime)=2019  
        AND CALENDAR_MONTH(ServiceAppointment.SchedEndTime)=11 
        GROUP BY ServiceResource.Id];

        System.debug('## lstAss: ' + lstAssRes);
        


        Map<Id, Decimal> mapAvgMins = getAverageMinsPerDayAgency(serviceTerritoryIds); 
        System.debug('##lstAssRes @ ' + lstAssRes);
        
        //assume only 1 angency in set: 
        Id serviceTerrId = new List<Id>(serviceTerritoryIds)[0];

        Map<Id, Decimal> mapResourceIdToAvg = new Map<Id, Decimal>();
        for (AggregateResult ar : lstAssRes)  {
            System.debug('Campaign ID' + ar.get('Id'));
            System.debug('Average amount' + ar.get('expr0'));
            mapResourceIdToAvg.put((Id)ar.get('Id'), (Decimal)ar.get('expr0')/mapAvgMins.get(serviceTerrId) );

        }

        System.debug('## mapResourceIdToAvg: ' + mapResourceIdToAvg);

        return mapResourceIdToAvg;
    }

    public static Map<Id, Decimal> calculateAbsencesPrev(Integer mois, Integer year, set<Id> ServiceTerritoryIds){
        System.debug('## calculateAbsencePrev start');
        // get list of resources: 
        List<ServiceTerritoryMember> lstStm = [SELECT ServiceResource.Name, ServiceResource.Id
                                                FROM ServiceTerritoryMember 
                                                WHERE ServiceTerritory.Id IN :ServiceTerritoryIds
                                                AND ServiceResource.IsActive=true];
        Set<Id> setServiceResId = new Set<Id>();
        for(ServiceTerritoryMember stm: lstStm){
            System.debug('## stm: ' + stm);
            setServiceResId.add(stm.ServiceResource.Id); 
        }

        System.debug('## serviceResoues: ' + setServiceResId);  

        // Select and calculate total absences for period: 
        Datetime startDate = Datetime.newInstanceGMT(year, mois, 01, 0, 0, 0);
        Datetime endDate = Datetime.newInstanceGMT(year, mois, Date.daysInMonth(year, mois), 0, 0, 0);

        List<ResourceAbsence> lstResAbs = [SELECT Id, Resource.Id, Resource.Name, Type, Start, End, FSL__Duration_In_Minutes__c FROM ResourceAbsence 
                                            WHERE Resource.Id in :setServiceResId AND Start <= :endDate  AND End>= :startDate];
        System.debug('## lstResAbs' + lstResAbs);
        System.debug('## calculateAbsencePrev end');

        Map<Id, Decimal> mapTotalAbsPerRes = new Map<Id, Decimal>();
        for(ResourceAbsence res : lstResAbs){
            if(mapTotalAbsPerRes.containsKey(res.Resource.Id)){
                mapTotalAbsPerRes.put(
                    res.Resource.Id, 
                    (Decimal) res.FSL__Duration_In_Minutes__c + mapTotalAbsPerRes.get(res.Resource.Id)
                );
            }else{
                 mapTotalAbsPerRes.put(
                    res.Resource.Id, 
                    (Decimal) res.FSL__Duration_In_Minutes__c
                );
            }
        }
        // end calculate total absences

        // calculate in number of days
        Map<Id, Decimal> mapAvgMins = getAverageMinsPerDayAgency(serviceTerritoryIds); 
        System.debug('##mapAvgMins @ ' + mapAvgMins);
        
        // assume only 1 angency in set: 
        Id serviceTerrId = new List<Id>(serviceTerritoryIds)[0];
        Decimal avgMins =  mapAvgMins.get(serviceTerrId);

        if(avgMins != null){
            for(Id resId : mapTotalAbsPerRes.keySet()){
                mapTotalAbsPerRes.put(resId, mapTotalAbsPerRes.get(resId) / 1440);
            }
        }

        System.debug('## mapTotlAbsPerRes : ' + mapTotalAbsPerRes);
        // end calculate absences in number of days

        return mapTotalAbsPerRes;
        
    }

    public static Map<Id, Decimal> getAverageMinsPerDayAgency(Set<Id> ServiceTerritoryIds){

        List<ServiceTerritory> lstSerTer = [SELECT Id, OperatingHoursId from ServiceTerritory WHERE ID IN: ServiceTerritoryIds];

        //to return a map of ServiceTerritory To average per day
        FSL_SearchVesPlanning_BA fx = new FSL_SearchVesPlanning_BA();
        Map<Id,Id> serviceMemberMap  = fx.getOperatingHours(ServiceTerritoryIds); 

        List<OperatingHours> operatingHours = [SELECT Id,Name, (SELECT DayOfWeek,StartTime, EndTime FROM TimeSlots) 
                                              FROM OperatingHours 
                                              WHERE Id In: serviceMemberMap.values()];

        Map<String, Decimal> mapDayToMins = new Map<String, Decimal>();
        Map<String, Decimal> mapOperatingToAvgMin = new Map<String, Decimal>();

        for(OperatingHours op: operatingHours) {
            List <TimeSlot> daysList = new List <TimeSlot>();

            for(TimeSlot ts: op.TimeSlots){
                if(!mapDayToMins.containsKey(ts.DayOfWeek)){
                    mapDayToMins.put(ts.DayOfWeek, fx.getTimeDif(ts.EndTime , ts.StartTime));
                }else{
                    mapDayToMins.put(ts.DayOfWeek, mapDayToMins.get(ts.dayOfWeek) + (fx.getTimeDif(ts.EndTime , ts.StartTime)) );
                }   
            }
            
            System.debug('## mapDayToMins: ' + mapDayToMins);

            if(!mapDayToMins.isEmpty()){
                Integer numDays = mapDayToMins.keySet().size();
                Decimal mins = 0.0;
                for(String theDay : mapDayToMins.keySet()){
                    mins += mapDayToMins.get(theDay);
                }

                System.debug('## numDays / mins: ' + mins + ' '+ numDays);
                if(numDays != null && mins != null){
                    mapOperatingToAvgMin.put(op.Id, mins / (Decimal)numDays);
                }
            }

            System.debug('## mapOperatingToAvgMin + ' + mapOperatingToAvgMin);

        }

        Map<Id, decimal> mapServiceTerrToAvgMins = new Map<Id, Decimal>();
        for(ServiceTerritory s : lstSerTer){
            if(s.OperatingHoursId != null && mapOperatingToAvgMin.containsKey(s.OperatingHoursId)){
                mapServiceTerrToAvgMins.put(s.Id, mapOperatingToAvgMin.get(s.OperatingHoursId));
            }
        }

        System.debug('## result returned in method: ' + mapServiceTerrToAvgMins);

        return mapServiceTerrToAvgMins;

    }

    public class syntheseWrapper{
        @AuraEnabled public String resourceName;
        @AuraEnabled public Integer capTheorique;
        @AuraEnabled public Integer capDejaAloue; 
        @AuraEnabled public Integer absencesPrevue; 
        @AuraEnabled public Integer CapDispo; 
    }


    //retrieve resource caps for aura elements
    @AuraEnabled
    public static Map<String, Object> retrieve(Integer year, Integer month){
        
        //get Service Territory Id: 
        List<ServiceTerritoryMember> lstStm = getCurrentTerritoryMembers();
        Set<Id> setTerr = new Set<Id>();
        Map<String, Object> mapRes = new Map<String, Object>();

        if(!lstStm.isEmpty())
        {
            setTerr.add(lstStm[0].ServiceTerritory.Id);

            //retrieve capacité theorique 
            Map<Id, Decimal> mapCapTheorique = calculateCAPTheorique(month, year, setTerr);
            Map<Id, Decimal> capAlreadyAllocated = calculateCapDejaAlloue(month, Year, setTerr);
            Map<Id, Decimal> mapAbsences = calculateAbsencesPrev(month, year, setTerr);

            System.debug('## maps Restrieved: '+ mapCapTheorique);
            System.debug('## maps capAlreadyAllocated: '+ capAlreadyAllocated);
            System.debug('## maps mapAbsences: '+ mapAbsences);

            List<LC18_CLISynthese.syntheseWrapper> lstWrapper = new List<LC18_CLISynthese.syntheseWrapper>();

            for(ServiceTerritoryMember stm : lstStm){
                System.debug('##stm: ' + stm);
                LC18_CLISynthese.syntheseWrapper wrapper = new LC18_CLISynthese.syntheseWrapper();
                wrapper.resourceName = stm.ServiceResource.Name;
                wrapper.capTheorique = mapCapTheorique.containsKey(stm.ServiceTerritory.OperatingHoursId)  ? (Integer) mapCapTheorique.get(stm.ServiceTerritory.OperatingHoursId) : 0;
                wrapper.capDejaAloue = capAlreadyAllocated.containsKey(stm.ServiceResourceId) ? (Integer) capAlreadyAllocated.get(stm.ServiceResourceId) : 0;
                wrapper.absencesPrevue = mapAbsences.containsKey(stm.ServiceResourceId) ? (Integer) mapAbsences.get(stm.ServiceResourceId) : 0;
                lstWrapper.add(wrapper);
            }

            //calculate 
            
            for(Integer i = 0 ; i < lstWrapper.size(); i++){
                lstWrapper[i].CapDispo = lstWrapper[i].capTheorique - lstWrapper[i].capDejaAloue - lstWrapper[i].absencesPrevue;
            }

            System.debug('## lstWrapper: ' + lstWrapper); 
            System.debug('## lstWrapper: ' + lstWrapper); 
            mapRes.put('result', lstWrapper);
            return mapRes; 

        }else{
            mapRes.put('result', 'empty');
            return mapRes;  
        }

        
    }

    public static List<ServiceTerritoryMember> getCurrentTerritoryMembers(){
        user userConnected = [SELECT id, companyName FROM User WHERE id=: UserInfo.getUserId()];
        string companyName = userConnected.companyName;
        string likeQueryCN = '%' + companyName + '%' ;
        system.debug('*** likeQueryCN: '+likeQueryCN);   

        List<ServiceTerritoryMember> lstSTM = [SELECT id, ServiceResource.Id, ServiceTerritory.OperatingHoursId, ServiceTerritory.Id, ServiceResource.Name, ServiceTerritory.Name,ServiceResource.IsActive
                                                FROM ServiceTerritoryMember 
                                                WHERE ServiceTerritory.Name like :likeQueryCN
                                                AND ServiceResource.IsActive=true 
                                                AND ServiceTerritory.FSL__TerritoryLevel__c = 0
                                                //AND EffectiveStartDate <= :dStart
                                                //AND EffectiveEndDate >= :dEnd                                             
                                              ];
                                              
        if(Test.isRunningTest()){
            lstSTM = [SELECT id, ServiceResource.Id, ServiceTerritory.OperatingHoursId, ServiceTerritory.Id, ServiceResource.Name, ServiceTerritory.Name,ServiceResource.IsActive
                                                FROM ServiceTerritoryMember ];
        }
        
        system.debug('** lstSTM size: '+lstSTM.size());
        return lstSTM;

    }
*/
}