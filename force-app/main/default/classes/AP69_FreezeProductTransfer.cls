/**
 * @File Name          : AP69_FreezeProductTransfer.cls
 * @Description        : 
 * @Author             : MDO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 24/04/2020, 10:28:56
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    10/03/2020         MDO                    Initial Version - CT-1417: Gestion des inventaires - freeze des mouvements de stocks pendant la période d'inventaire
**/
public with sharing class AP69_FreezeProductTransfer {
    
    public static void checkProductTransfer(list<ProductTransfer> lstProductTransfer, set<Id> setIdLocation){
        system.debug('### AP69_FreezeProductTransfer - start checkProductTransfer');
        //variables
        map<Id, Id> mapLocaIdAgenceId = new map<Id,Id>(); 
        map<Id, boolean> mapAgenceIdInvenEnCours = new map<Id, boolean>();
        Id locationId;

        //get Agence from locations (destination and source)
        for (Schema.Location loc1: [select Id, Agence__c from Location where Id IN :setIdLocation]){
            if (loc1.Agence__c != null)
                mapLocaIdAgenceId.put(loc1.Id, loc1.Agence__c);
        }

        //check field 'inventaire en cours' from Agence of Location of productTransfer
        for (ServiceTerritory st: [select Id, Inventaire_en_cours__c from ServiceTerritory where Id IN :mapLocaIdAgenceId.values()]){
            mapAgenceIdInvenEnCours.put(st.Id, st.Inventaire_en_cours__c);
        }

        system.debug('### mapLocaIdAgenceId:'+ mapLocaIdAgenceId);
        system.debug('### mapAgenceIdInvenEnCours:'+ mapAgenceIdInvenEnCours);

        for (ProductTransfer pt: lstProductTransfer){
            system.debug('## pt.DestinationLocationId:'+pt.DestinationLocationId);
            if (pt.DestinationLocationId != null){
                locationId = pt.DestinationLocationId;

                if (checkInventaire(locationId, mapLocaIdAgenceId, mapAgenceIdInvenEnCours)){
                    System.debug('Update fields');
                    pt.IsReceived = false;
                    pt.Inventaire_en_cours__c = true;
                    continue;
                }
            }

            if (pt.SourceLocationId != null){
                locationId = pt.SourceLocationId;
                system.debug('### locationId:'+ locationId);
                if (checkInventaire(locationId, mapLocaIdAgenceId, mapAgenceIdInvenEnCours)){
                    pt.IsReceived = false;
                    pt.Inventaire_en_cours__c = true;
                    continue;
                }
            }
        }
        system.debug('### AP69_FreezeProductTransfer - end checkProductTransfer');
    }

    public static boolean checkInventaire(Id locationId, map<Id, Id> mapLocaIdAgenceId, map<Id, boolean> mapAgenceIdInvenEnCours){
        system.debug('### AP69_FreezeProductTransfer - start checkInventaire');

        Id agenceId;
        Boolean invenEnCours=false;

        if (mapLocaIdAgenceId.containsKey(locationId)){
            agenceId = mapLocaIdAgenceId.get(locationId);
        }

        system.debug('### agenceId:'+ agenceId);

        if (mapAgenceIdInvenEnCours.containsKey(agenceId)){
            invenEnCours = mapAgenceIdInvenEnCours.get(agenceId);
        }

        system.debug('### invenEnCours:'+ invenEnCours);

        system.debug('### AP69_FreezeProductTransfer - end checkInventaire');
        return invenEnCours;
    }
    

    public static void setPIQuantityInventaire(set<Id> setAgenceId2){
        system.debug('### AP69_FreezeProductTransfer - start setPIInventaireQuantity');
        system.debug('### setAgenceId2:'+setAgenceId2);
        
        //get product items from locations for which agency starts the inventory
        List<ProductItem> PIlst = new List <ProductItem>();

        for (ProductItem pi: [SELECT Id, QuantityOnHand, Quantite_inventaire__c FROM ProductItem WHERE Location.Agence__c in :setAgenceId2] ){
            pi.Quantite_inventaire__c = pi.QuantityOnHand;
            //BCH Update 08/04 reset de la quantityonhand au début de l'inventaire et mettre le statut à 'à compter'
            pi.Quantite_comptee__c=null;
            pi.Statut_inventaire__c = 'A compter';
            PIlst.add(pi);
        }    
        system.debug('PI Object:'+PIlst[0]);                
        system.debug('PI LIst size:'+PIlst.size());
                       
        if (PIlst.size()>0){            
            update(PIlst);
        }
        system.debug('### AP69_FreezeProductTransfer - end setPIInventaireQuantity');       
    }
    
    public static void setPIQuantityFinInventaire(set<Id> setAgenceId){
        system.debug('### AP69_FreezeProductTransfer - start setPIFinInventaireQuantity');
        system.debug('### setAgenceId:'+setAgenceId);
        
        //get product items from locations for which agency starts the inventory
        List<ProductItem> PIlst = new List <ProductItem>();

        for (ProductItem pi: [SELECT Id, QuantityOnHand, Quantite_inventaire__c, Quantite_comptee__c, Statut_inventaire__c FROM ProductItem WHERE Location.Agence__c in :setAgenceId] ){           
            if (pi.Quantite_comptee__c != null && (pi.Statut_inventaire__c == 'Compté' || pi.Statut_inventaire__c == 'Recompté suite à écart')){
                pi.QuantityOnHand = pi.Quantite_comptee__c;
                PIlst.add(pi);
            }                         
            
        }                      
        if (PIlst.size()>0){            
            update(PIlst);
        }
        system.debug('### AP69_FreezeProductTransfer - end setPIFinInventaireQuantity');       
    }         
    
}