global without sharing class LightningSelfRegisterController {

    private static final String accountType = 'Particulier';
    private static final String accountProType = 'Entreprise';
    private static final String accountStatus = 'Prospect';
    private static final String accountSource = 'Site Cham Digital';
    private static final String timezone = 'Europe/Paris';

   @TestVisible 
    private static boolean isValidPassword(String password, String confirmPassword) {
      return password == confirmPassword;
    }
    
    @TestVisible 
    private static boolean siteAsContainerEnabled(String communityUrl) {
      Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(communityUrl,'');
      return authConfig.isCommunityUsingSiteAsContainer();
    }
    
    @TestVisible 
    private static void validatePassword(User u, String password, String confirmPassword) {
      if(!Test.isRunningTest()) {
          Site.validatePassword(u, password, confirmPassword);
      }
      return;
    }
    
    @AuraEnabled
    public static Boolean isUserConnected() {
       return (UserInfo.getProfileId().equals(System.Label.GuestProfileId))? false : true;
    }
    
    @AuraEnabled
    public static String selfRegister(String salutation, String firstname, String lastname, String email, String confirmEmail, String password, String confirmPassword, String startUrl, String token, String accType) {
      System.debug('--- selfRegister Start !');
      System.debug('--- selfRegister accType: '+accType);

      String result;

      try {
        String username = email;

        if(String.isBlank(salutation) && accType=='particulier'){
          result = System.Label.SalutationRequired;
        }else if(String.isBlank(lastname)){
          result = System.Label.LastNameRequired;
        }else if(String.isBlank(firstname)){
          result = System.Label.FirstNameRequired;
        }else if(String.isBlank(email)){
          result = System.Label.EmailRequired;
        }else if (String.isBlank(confirmEmail)){
          result = System.Label.ConfirmationEmailRequired;
        }else if(String.isBlank(password)){
          result = System.Label.PasswordRequired;
        }else if(String.isBlank(confirmPassword)){
          result = System.Label.PasswordConfirmRequired;
        }else if(String.isBlank(token)){
          result = System.Label.AreYouARobot;
        }else{
          User anUser = new User(
            Username = username,
            Email = email,
            FirstName = accType=='professionnel' ? null : firstname,
            LastName = lastname,
            ProfileId  = System.Label.CustomerCommunityProfileId,
            TimeZoneSidKey = LightningSelfRegisterController.timezone
          );
          
          String networkId = Network.getNetworkId();

          // If using site to host the community the user should not hit s1 after logging in from mobile.
          if(networkId != null && siteAsContainerEnabled(Network.getLoginUrl(networkId))) {
            anUser.put('UserPreferencesHideS1BrowserUI',true);
          }
          
          String nickname = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
          nickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
          anUser.put('CommunityNickname', nickname);
                   
          if (!isValidPassword(password, confirmPassword)) {
            return Label.site.passwords_dont_match;
          }
          
          validatePassword(anUser, password, confirmPassword);

          System.debug('--- anUser : ' + anUser);

          Boolean resultCaptcha = LightningSelfRegisterController.checkCaptcha(token, System.Label.CaptchaSecretKey);
          System.debug('--- resultCaptcha : ' + resultCaptcha);

          if(resultCaptcha){
            System.debug('--- in captcha');
            // lastName is a required field on user, but if it isn't specified, we'll default it to the username
            String userId = '';
            if(accType=='professionnel'){
              System.debug('--- in professionnel: ');              
              User adminCham = [select id from User where Name like '%Admin Cham%'];
              list <RecordType> rtBusinessAcclst = [select id from RecordType where DeveloperName like '%BusinessAccount%']; //This query was due to issues with Schema.SObjectType
              
              //CLA 25 MAY 2020: CT-1790 - Added accout Pro Type = 'Entreprise'
              Account accBusiness = new Account(Name=LastName, 
                                                RecordTypeId = rtBusinessAcclst[0].Id, 
                                                OwnerId = adminCham.Id,
                                                Type = LightningSelfRegisterController.accountProType,
                                                Status__c = LightningSelfRegisterController.accountStatus,
                                                AccountSource = LightningSelfRegisterController.accountSource,
                                                Raison_sociale__c = LastName
                                                ,Siret__c = Decimal.valueOf(FirstName)
                                    );
              try{                
                insert accBusiness;
              }catch(Exception e){
                system.debug('--- err msg: '+ e.getMessage());
              }
                           
              userId = Site.createExternalUser(anUser, accBusiness.Id, password, false); 
              
            }else{
              userId = Site.createExternalUser(anUser, NULL, password, false);
            }            
            
            // create a fake userId for test.
            if (Test.isRunningTest()) {
              userId = 'fakeUserId';           
            }

            if (userId != null) { 
              
              map<String, String> userMap = Utils.getAccountIdFromCommunityUser(new List<String>{userId});             
              
              if(userMap.containsKey(userId) && accType=='particulier'){
                Account accountToUpdate = new Account(
                  Id = userMap.get(userId),              
                  Salutation = salutation,   
                  Type = LightningSelfRegisterController.accountType,
                  Status__c = LightningSelfRegisterController.accountStatus,
                  AccountSource = LightningSelfRegisterController.accountSource
                );               
                
                update accountToUpdate;
              }

              if (password != null && password.length() > 1) {
                ApexPages.PageReference lgn = Site.login(username, password, startUrl);
                
                if(!Test.isRunningTest()) {
                  aura.redirect(lgn);
                }
              }
              else 
              {
                  return 'password problem';
              }
            }
            else
            {
                return 'userid is null';
            }
          }
          else
          {
              return 'captcha validation error';
          }

          result = 'OK';                
        }
      }catch (Exception e) {
        System.debug('--- Error : ' + e.getMessage());
        System.debug('--- Errorrrrr : ' + e);
        result = e.getMessage();         
      }

      System.debug('--- result : ' + result);

      System.debug('--- selfRegister End !');
      return result;
    }
    
    // Appel le WS de Google pour vérifier le captcha
    public static Boolean checkCaptcha(String token, String secretAPI){
      System.debug('--- checkCaptcha Start !');
      
      Boolean result;
      
      if(!String.isBlank(token) && !String.isBlank(secretAPI)){
        String endpoint = System.Label.EndPointCaptcha;
        String body = 'secret=' + secretAPI + '&response=' + token;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setbody(body);
        Http http = new Http();
        HTTPResponse response = http.send(req);
        String resultWS = response.getBody();
        System.debug('--- resultWS : ' + resultWS);
        
        JSONParser parser = JSON.createParser(response.getBody());
        while (parser.nextToken() != null) {
          if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'success')) {
            parser.nextToken();
            result = parser.getBooleanValue();

            break;
          }
        }
      }       
      
      System.debug('--- result : ' + result);
      System.debug('--- checkCaptcha End !');
      
      return result;
    }
}