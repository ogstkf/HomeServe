/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 11-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   10-03-2022   MNA   Initial Version
**/
public with sharing class LC80_CBRevokeMandate {
    @AuraEnabled
    public static Map<String, String> revokeMandat(String CBId) {
        sofactoapp__Coordonnees_bancaires__c CB = [SELECT Id, RUM__c, sofactoapp__Compte__c, sofactoapp__Compte__r.Agence__c,  sofactoapp__Raison_sociale__c, sofactoapp__Raison_sociale__r.sofactoapp_Agence__c FROM sofactoapp__Coordonnees_bancaires__c WHERE Id = : CBId];
        ServiceTerritory agency;
        if (CB.sofactoapp__Compte__c != null && CB.sofactoapp__Compte__r.Agence__c != null) {
            agency = [SELECT Id, Name, Slimpay_Creditor_Name__c FROM ServiceTerritory WHERE Id =:CB.sofactoapp__Compte__r.Agence__c];
        }
        else if (CB.sofactoapp__Raison_sociale__c != null && CB.sofactoapp__Raison_sociale__r.sofactoapp_Agence__c != null) {
            agency = [SELECT Id, Name, Slimpay_Creditor_Name__c FROM ServiceTerritory WHERE Id =:CB.sofactoapp__Compte__r.Agence__c];
        }

        String idmandate = WS16_GestionDesPrelevements.getMandate(agency, CB.RUM__c);
        return WS16_GestionDesPrelevements.revokeMandate(agency, idmandate);
    }
}