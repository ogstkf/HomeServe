/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 28-10-2021
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   10-28-2020   ZJO   Initial Version
**/
public with sharing class AP57_CompletionDateOnCli {
    
    public static void updateCliCompletedStatus(List<WorkOrder> lstNew, set<Id> setClis){
        // system.debug(' dmu updateCliCompletedStatus ');
        // //1. Retrieve all Contract line items for the WorkOrders
        //     // WorkOrder -> Case -> CLI
        // //2. CLI.Completion date => WorkOrder.Completion date,
        // //   CLI.VE_counter number => VE_Counter number +1
        // //   CLI.Status => "Complete"

        // List<ContractLineItem> lstCliToUpdate = new List<ContractLineItem>(); 

        // Map<Id, ContractLineItem> mapIdToCli = new Map<Id, ContractLineItem>([SELECT Desired_VE_Date__c ,Completion_Date__c, VE_Number_Counter__c, VE_Number_Target__c, VE_Status__c, TECH_ServiceContractMonths__c 
        //                                 FROM ContractLineItem 
        //                                 WHERE Id IN :setClis]);

        // for(WorkOrder WO: lstNew){

        //     //get CLI: 
        //     ContractLineItem i = mapIdToCli.get(WO.TECH_CaseContractLineItem__c);
        //     system.debug(' i.VE_Number_Target__c:  '+i.VE_Number_Target__c);
        //     system.debug(' i.VE_Number_Counter__c:  '+i.VE_Number_Counter__c);

        //     // if(i.VE_Number_Target__c == i.VE_Number_Counter__c){
        //         i.Completion_Date__c = WO.Completion_Date__c;
        //         i.VE_Status__c = AP_Constant.cliStatusComplete; 
        //         // if(i.VE_Number_Counter__c != null){
        //         //     i.VE_Number_Counter__c += 1;
        //         // }else{
        //         //     i.VE_Number_Counter__c = 1;
        //         // } 

        //         if(i.Desired_VE_Date__c == null && i.TECH_ServiceContractMonths__c == null){
        //             i.Desired_VE_Date__c =WO.Completion_Date__c;
        //         }

        //         lstCliToUpdate.add(i);
        //     // }
        // }

        // if(!lstCliToUpdate.isEmpty()){
        //     update lstCliToUpdate;
        // }
        
    }

    public static void updateCustomerAbsence(List<WorkOrder> lstNew, set<Id> setClis){
        system.debug('** enter func updateCustomerAbsence');
        system.debug('*** lstNewWOs:' + lstNew);
        Set<ID> setWOId = (new Map<Id, WorkOrder>(lstNew)).KeySet();
        String query = 'SELECT id';
        Map<String, Schema.SObjectField> mapMesField =  Schema.getGlobalDescribe().get('WorkOrder').getDescribe().fields.getMap();
        for (String key : mapMesField.keySet()) {
            if (key != 'id' && key != 'productservicecampaignid' && key != 'returnorderid'
                && key != 'maintenanceworkruleid'
                && key != 'productservicecampaignitemid' && key != 'returnorderlineitemid' && key != 'assetwarrantyid'
                && key != 'businesshoursid' && key != 'servicereporttemplateid' && key != 'maintenanceworkruleid'
               && key != 'worktypeid' )
            	query += ', ' + key;
        }
        query += ' FROM WorkOrder WHERE Id IN :setWOId';
        lstNew = Database.query(query);
        
        Map<Id, ContractLineItem> mapIdToCli = new Map<Id, ContractLineItem>([SELECT Customer_Absences__c, Customer_Allowed_Absences__c, Completion_Date__c, VE_Number_Counter__c, VE_Status__c
                                        FROM ContractLineItem 
                                        WHERE Id IN :setClis]);

        List<ContractLineItem> lstCliToUpdate = new List<ContractLineItem>(); 

        Map<Id, ServiceAppointment> mapWoToSa = new Map<Id, ServiceAppointment>();
        for(ServiceAppointment sa : [SELECT Id, Work_Order__c, Status FROM ServiceAppointment WHERE Work_Order__c IN:lstNew]){
            system.debug('*** in SA loop');
            mapWoToSa.put(sa.Work_Order__c, sa);
        }

        system.debug('*** mapWOToSA:' + mapWoToSa);
        
        List<WorkOrder> lstWOsToInsert = new List<WorkOrder>();
        
        system.debug('** before loop');
        for(WorkOrder wo: lstNew){
            ContractLineItem i = mapIdToCli.get(WO.TECH_CaseContractLineItem__c);
            if(i.Customer_Absences__c != null){
                i.Customer_Absences__c += 1;
            }else{
                i.Customer_Absences__c = 1;
            }

            //TEC-209
            if(mapWoToSa.containsKey(wo.Id) && mapWoToSa.get(wo.Id).Status == 'Done client absent'){
                system.debug('test');
                system.debug('###### i.Customer_Absences__c'+ i.Customer_Absences__c);
                system.debug('###### i.Customer_Allowed_Absences__c'+ i.Customer_Allowed_Absences__c);
                if(i.Customer_Absences__c <= i.Customer_Allowed_Absences__c && wo.MaintenancePlanId != null){
                    system.debug('*** before clone');
                    WorkOrder newWo = wo.clone(false, false, false, false);
                    system.debug('*** after clone');
                    newWo.CaseId = wo.CaseId;
                    newWo.MaintenancePlanId = wo.MaintenancePlanId;
                    newWo.Status = 'New';
                    newWo.Customer_Absences__c = 0;
                    newWo.LocationId = null;
                    newWo.Acknowledged_On__c = DateTime.now();
                    newWo.Clientnoncontacte__c = true;
                    
                    lstWOsToInsert.add(newWo);
                }
            }
            lstCliToUpdate.add(i);
        }

        if(!lstWOsToInsert.isEmpty()){
            insert lstWOsToInsert;
        }

        if(!lstCliToUpdate.isEmpty()){
            update lstCliToUpdate;
        }
    }
}