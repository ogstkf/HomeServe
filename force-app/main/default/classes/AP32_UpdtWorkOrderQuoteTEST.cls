/**
 * @File Name          : AP32_UpdtWorkOrderQuoteTEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    28/10/2019   AMO     Initial Version
**/
@isTest
public with sharing class AP32_UpdtWorkOrderQuoteTEST {

    static User mainUser;
    static Account testAcc = new Account();
    static List<Quote> lstQuote = new List<Quote>();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<WorkOrder> lstWorkOrder = new List<WorkOrder>();
    //bypass
    static Bypass__c bp = new Bypass__c();

    static{

        mainUser = TestFactory.createAdminUser('AP32_UpdtWorkOrderQuoteTEST@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;


        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //Create WorkOrder
            lstWorkOrder = new List<WorkOrder>{
                new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal),
                new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal)
            };

            insert lstWorkOrder;

            //create opportunity
            lstOpp =  new List<Opportunity>{ 
                TestFactory.createOpportunity('Test1',testAcc.Id) ,
                TestFactory.createOpportunity('Test2',testAcc.Id) 
            };

            lstOpp[0].Ordre_d_execution__c = lstWorkOrder[0].Id;
            lstOpp[1].Ordre_d_execution__c = lstWorkOrder[1].Id;

            insert lstOpp;
            System.debug('Lst opp ' + lstOpp);

            //Create Quote
            lstQuote =  new List<Quote>{    
                new Quote(Name ='Test1',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[0].Id) ,                                   
                new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[0].Id)
            };


        }
    }

    @isTest
    public static void testInsert(){
        System.runAs(mainUser){

            Test.startTest();  
                Insert lstQuote;            
            Test.stopTest();

            Quote qu = [SELECT Id, Ordre_d_execution__c FROM Quote WHERE Id IN :lstQuote LIMIT 1];

            System.assertEquals(qu.Ordre_d_execution__c, lstOpp[0].Ordre_d_execution__c);
            
        }
    }

    @isTest
    public static void testUpdate(){
        System.runAs(mainUser){
            Test.startTest();
            Insert lstQuote;
            lstOpp[0].Ordre_d_execution__c = lstWorkOrder[1].Id;
            Update lstOpp;
            Update lstQuote;
            Test.stopTest();
            
            List<Quote> lstQu = [SELECT Id, Ordre_d_execution__c FROM Quote WHERE Id IN :lstQuote];
            // System.assertEquals(lstQu[0].Ordre_d_execution__c, lstOpp[0].Ordre_d_execution__c);
            // System.assertEquals(lstQu[1].Ordre_d_execution__c, lstOpp[0].Ordre_d_execution__c);

        }
    }
    
}