/**
 * @File Name          : LC56_MesEquipments.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/8/2020, 2:53:05 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/03/2020  ZJO     Initial Version
 * 1.1    19/06/2020   DMU     Commented class due to homeserve project
**/

public without sharing class LC56_MesEquipments {
 /*
  @AuraEnabled
    public static Object fetchAssetDetails(){
        
        //Get current user Id
        String userId = UserInfo.getUserId();

        //Query accounts for current user   
        User currentUser = [SELECT AccountId FROM User WHERE Id= :userId];
        String accId = currentUser.AccountId;
        Boolean isActive = true;

        List<DigitalPictoMapping__c> lstCustomSetting = new List<DigitalPictoMapping__c>();
        lstCustomSetting = [SELECT Name, Path__c from DigitalPictoMapping__c];
        Map<String, String> mapIcon = new Map<String, String>();
        for(DigitalPictoMapping__c mapping : lstCustomSetting){
            mapIcon.put(mapping.Name, mapping.Path__c);
        }

        //Query Assets for current user account
        List<Asset> lstAssets = [SELECT Id, SerialNumber, Product2.Brand__c, Product2.Model__c, Product2.Equipment_type1__c, Logement__r.Street__c, Logement__r.Adress_complement__c , Logement__r.Postal_Code__c , Logement__r.City__c  FROM Asset WHERE Active__c = :isActive AND AccountId= :accId];
        Map<Id, Asset> mapAst = new Map<Id, Asset>(lstAssets);


        Map<Id,ContractLineItem> mapAstToCliActive = new Map<Id,ContractLineItem>();
        Map<Id,ContractLineItem> mapAstToCliRenew = new Map<Id,ContractLineItem>();
        Map<Id, ContractLineItem> mapContractPDF = new Map<Id, ContractLineItem>();
        // Map<Id, List<ContractLineItem>> mapAstToClisActive = new Map<Id, List<ContractLineItem>>();
        // Map<Id, List<ContractLineItem>> mapAstToClisRenewed = new Map<Id, List<ContractLineItem>>();

        for (ContractLineItem cli : [SELECT ServiceContract.TECH_NumberOfDaysPriorEndDate__c, ServiceContract.Asset__r.AccountId, Id, ServiceContract.Asset__c, ServiceContract.GrandTotal, ServiceContract.EndDate, ServiceContract.StartDate, ServiceContract.Contrat_Cham_Digital__c, ServiceContract.Contract_Renewed__c, toLabel(ServiceContract.Contract_Status__c), ServiceContractId, IsActive__c, Product2Id, Product2.Name, ServiceContract.Asset__r.Product2.Equipment_family__c, ServiceContract.TECH_ContractPDF__c, UnitPrice, ServiceContract.Tax__c, ServiceContract.SubTotal, AssetId, ServiceContract.TECH_ContractActive__c
            FROM ContractLineItem 
            WHERE ServiceContract.Asset__c IN: mapAst.keySet() 
                AND IsBundle__c = true
                AND ServiceContract.Asset__c != null
                // AND ServiceContract.Contract_Renewed__c = false
                AND ServiceContract.Contract_Status__c IN ('Active', 'Pending first visit', 'Actif - en retard de paiement', 'Expired')
            ORDER BY ServiceContract.Contrat_Cham_Digital__c, ServiceContract.StartDate  ASC
        ]){
            if(!mapAstToCliActive.containsKey(cli.ServiceContract.Asset__c) && cli.ServiceContract.TECH_ContractActive__c){
                mapAstToCliActive.put(cli.ServiceContract.Asset__c, cli);
            }
            if(String.isNotBlank(cli.ServiceContract.TECH_ContractPDF__c)){
                if(mapContractPDF.containsKey(cli.ServiceContract.Asset__c)){
                    if(cli.ServiceContract.StartDate < mapContractPDF.get(cli.ServiceContract.Asset__c).ServiceContract.StartDate){
                        mapContractPDF.put(cli.ServiceContract.Asset__c, cli);
                    }
                }else{
                    mapContractPDF.put(cli.ServiceContract.Asset__c, cli);
                }
            }
            
        }

        for (ContractLineItem cli : [SELECT Id, ServiceContract.Asset__c, ServiceContract.TECH_NumberOfDaysPriorEndDate__c, ServiceContract.Asset__r.AccountId, ServiceContract.GrandTotal, ServiceContract.EndDate, ServiceContract.StartDate, ServiceContract.Contrat_Cham_Digital__c, ServiceContract.Contract_Renewed__c, toLabel(ServiceContract.Contract_Status__c), ServiceContractId, IsActive__c, Product2Id, Product2.Name, ServiceContract.Asset__r.Product2.Equipment_family__c, ServiceContract.TECH_ContractPDF__c, UnitPrice, ServiceContract.Tax__c, ServiceContract.SubTotal, AssetId 
            FROM ContractLineItem 
            WHERE ServiceContract.Asset__c IN: mapAst.keySet()
                AND IsBundle__c = true 
                AND ServiceContract.Asset__c != null
                AND ServiceContract.Contract_Renewed__c = true
                AND ServiceContract.TECH_RenewalPaid__c = false
                AND ServiceContract.Contract_Status__c IN ('Pending Payment', 'Actif - en retard de paiement')
                AND ServiceContract.StartDate <= NEXT_N_DAYS:60
                AND ServiceContract.StartDate >= LAST_N_DAYS:60
            ORDER BY ServiceContract.StartDate, ServiceContract.Contrat_Cham_Digital__c  ASC
        ]){
            if(!mapAstToCliRenew.containsKey(cli.ServiceContract.Asset__c)){
                mapAstToCliRenew.put(cli.ServiceContract.Asset__c, cli);
            }
        }

        Integer rows = Math.max(mapAstToCliRenew.size(), mapAstToCliActive.size());
        Integer count = 0;

        List<MesEquipmentsWrapper> lstMesEquipments = new List<MesEquipmentsWrapper>();

        System.debug('map asset Id:' + mapAst);
        System.debug('map asset to cli:' + mapAstToCliActive);

        
        for (Id astId : mapAst.keySet()){
            MesEquipmentsWrapper contents = new MesEquipmentsWrapper();
            contents.cli = null;
            if(mapAstToCliActive.containsKey(astId)){
                contents.asset = mapAst.get(astId);
                contents.cli = mapAstToCliActive.get(astId);
                contents.iconPath = mapIcon.get(mapAst.get(astId).Product2.Equipment_type1__c);
                contents.isActive = true;
                if(mapContractPDF.containsKey(astId)){
                    contents.contractPdfUrl = mapContractPDF.get(astId).ServiceContract.TECH_ContractPDF__c;
                }
            }
            if(mapAstToCliRenew.containsKey(astId)){
                contents.asset = mapAst.get(astId);
                contents.isRenew = true;
                contents.renewCli =mapAstToCliRenew.get(astId);
                contents.renewCliStart =mapAstToCliRenew.get(astId).ServiceContract.StartDate.addDays(-1);
                contents.renewCliEnd =mapAstToCliRenew.get(astId).ServiceContract.StartDate;
                contents.iconPath =  mapIcon.containsKey(mapAst.get(astId).Product2.Equipment_type1__c) ? mapIcon.get(mapAst.get(astId).Product2.Equipment_type1__c) : 'chaudiere-gaz.svg';
                if(!mapAstToCliActive.containsKey(astId)){
                    contents.cli = mapAstToCliRenew.get(astId);
                }
            }

            if(contents.asset != null){
                lstMesEquipments.add(contents);
            }

        }

        // Map<String, Object> mapTest = new Map<String, Object>();
        // mapTest.put('active: ', mapAstToCliActive);
        // mapTest.put('renew: ', mapAstToCliRenew);
    //  return mapAst;
        return lstMesEquipments;
    }

    public class MesEquipmentsWrapper{
        @AuraEnabled public Asset asset;
        @AuraEnabled public ContractLineItem cli;
        @AuraEnabled public ContractLineItem renewCli;
        @AuraEnabled public Date renewCliStart;
        @AuraEnabled public Date renewCliEnd;
        @AuraEnabled public String iconPath;
        @AuraEnabled public String contractPdfUrl;
        @AuraEnabled public Boolean isRenew;
        @AuraEnabled public Boolean isActive;

        public MesEquipmentsWrapper(){
            this.isRenew = false;
            this.isActive = false;
        }
    }
    */
}