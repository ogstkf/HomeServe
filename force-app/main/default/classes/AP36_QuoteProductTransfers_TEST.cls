/**
 * @File Name          : AP36_QuoteProductTransfers_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/03/2020   AMO     Initial Version
**/
@isTest
public with sharing class AP36_QuoteProductTransfers_TEST {
    
    static User mainUser;
    static Schema.Location loc = new Schema.Location();
    static Schema.Location loc2 = new Schema.Location();
    static List<QuoteLineItem> lstQuoteLineItem;
	static ProductItem pi = new ProductItem(); 
    static ProductItem prodi = new ProductItem(); 
    static Account testAcc = new Account();
    static Opportunity opp = new Opportunity();
    static WorkOrder wrkOrd = new WorkOrder();
    static list<Product2> lstProd = new list<Product2>();
    static OperatingHours oh = new OperatingHours();
    static sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c();
	static ServiceTerritory st = new ServiceTerritory();
	static ServiceTerritoryLocation stl = new ServiceTerritoryLocation();  
    static ServiceTerritoryLocation stlCommander = new ServiceTerritoryLocation();  

    static Quote quo = new Quote();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static list<QuoteLineItem> lstQli = new list<QuoteLineItem>();
    static List<ProductRequestLineItem> lstPRLI = new List<ProductRequestLineItem>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    //bypass
    static Bypass__c bp = new Bypass__c();
    
    static{
        
        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;


        System.runAs(mainUser){
			
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;
            
            sofactoapp__Compte_auxiliaire__c aux0 = createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
			update testAcc;
            
            
            //work order
            wrkOrd = TestFactory.createWorkOrder();
            insert wrkOrd;
             System.debug('Create wo:' + wrkOrd);
            //create Products
            lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                    
                
                    
                new Product2(Name='Prod3', 
                             Famille_d_articles__c='Consommables',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
                    
                // new Product2(Name='Prod4', 
                //              Famille_d_articles__c='Pièces détachées',
                //              RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
            };
               
            insert lstProd;
            
            //create Location
            loc = new Schema.Location(Name='Test1',
                                      LocationType='Entrepôt',
                                      ParentLocationId=NULL,
                                      IsInventoryLocation=true);
            insert loc;

            //create Location
            loc2 = new Schema.Location(Name='Test1',
                                      LocationType='Entrepôt',
                                      ParentLocationId=NULL,
                                      IsInventoryLocation=true);
            insert loc2;

			//create Operating Hours
			oh = new OperatingHours(Name='Test1');
			insert oh;

			//create Corporate Name
			sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(Name='Test1',
                                                                                  sofactoapp__Credit_prefix__c='Test1',
                                                                                  sofactoapp__Invoice_prefix__c='Test1');  
            insert srs;
            
            //create Service Territory
            st = new ServiceTerritory(Name='Test1',
                                      OperatingHoursId=oh.Id,
                                      Sofactoapp_Raison_Social__c=srs.Id);
            insert st;
            
            //create Service Territory Location
            stl = new ServiceTerritoryLocation(ServiceTerritoryId=st.Id,
                                               LocationId=loc.Id);
            insert stl;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            //creating raison sociale
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            //create service territory
            srvTerr = TestFactory.createServiceTerritory('test Territory',opHrs.Id, raisonSocial.Id);
            insert srvTerr;

            stlCommander = new ServiceTerritoryLocation(ServiceTerritoryId=srvTerr.Id,
                                               LocationId=loc2.Id);
            insert stlCommander;
            
            
           
            
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            insert PrcBkEnt;

            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);
            opp.Agency__c = srvTerr.Id;        
            insert opp;
            
            //create Quote
            quo = new Quote(Name ='Test1',
                            OpportunityId=opp.Id,
                            Agency__c=srvTerr.Id,
                            Pricebook2Id = lstPrcBk[0].Id,
                            Ordre_d_execution__c = wrkOrd.Id,
                            IsSync__c = false
                           );
            
            insert quo;  

            //create QuoteLineItem
            lstQuoteLineItem =  new List<QuoteLineItem>{
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstProd[1].Id,
                                    QuoteId = quo.Id, 
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today(),
                                    PriceBookEntryId = PrcBkEnt.Id
                                 )
                };
            insert lstQuoteLineItem;

            prodi = new ProductItem(Product2Id=lstProd[1].Id,
                                 LocationId=loc2.Id,
                                 QuantityOnHand=decimal.valueOf(2));
            insert prodi;

           // wrkOrd.Quote__c = quo.Id;
           // update wrkOrd;
                       
            
            ProductRequest pr = new ProductRequest();
          //  pr.A_preparer__c =1;
            pr.AccountId = testAcc.Id;
            pr.Description = 'description';
          //  pr.Nb_articles__c = 3;
            pr.NeedByDate = DateTime.now();
            pr.WorkOrderId = wrkOrd.Id;
            insert pr;
            
            
             pi = new ProductItem(Product2Id=lstProd[0].Id,
                                 LocationId=loc.Id,
                                 QuantityOnHand=decimal.valueOf(2));
            insert pi;
            
            quo.Ordre_d_execution__c = wrkOrd.Id;
            update quo;
            //create Quote Line Items
            lstQli = new list<QuoteLineItem>{
            	new QuoteLineItem(QuoteId=quo.Id,
                                  Product2Id=lstProd[0].Id, 
                                  Quantity=decimal.valueOf(2), 
                                  UnitPrice=decimal.valueOf(145),
                                  PriceBookEntryID=PrcBkEnt.Id)             
            };
            insert lstQli;


            lstPRLI = new List<ProductRequestLineItem > {
                //Order0 0-2
                new ProductRequestLineItem( Product2Id = lstProd[0].Id
                                            ,QuantityRequested = 1
                                            ,ParentId = pr.Id
                                            ,Status = null
                                            ,Quote__c = quo.Id
                                            ,Agence__c = srvTerr.Id,
                                            DestinationLocationId = null),
                new ProductRequestLineItem( Product2Id = lstProd[1].Id
                                            ,QuantityRequested = 1
                                            ,ParentId = pr.Id
                                            //,Status = 'Transféré'
                                            ,Status = null
                                            ,Quote__c = quo.Id
                                            ,Agence__c = srvTerr.Id)
                // new ProductRequestLineItem( Product2Id = prod.Id
                //                             ,QuantityRequested = 1
                //                             ,ParentId = lstProdRequest[0].Id
                //                             //,Status = 'Réservé à préparer'
                //                             ,Status = 'Réservé à préparer'
                //                             ,Commande__c = lstOrder[0].Id
                //                             ,Quote__c = quo.Id),
                // //Order1 3-5
                // new ProductRequestLineItem( Product2Id = prod.Id
                //                             ,QuantityRequested = 1
                //                             ,ParentId = lstProdRequest[0].Id
                //                             //,Status = 'Réservé à préparer'
                //                             ,Status = 'Réservé à préparer	'
                //                             ,Commande__c = lstOrder[1].Id
                //                             ,Quote__c = quo.Id),
                // new ProductRequestLineItem( Product2Id = prod.Id
                //                             ,QuantityRequested = 1
                //                             ,ParentId = lstProdRequest[0].Id
                //                             //,Status = 'Réservé à préparer'
                //                             ,Status = 'Réservé à préparer	'
                //                             ,Commande__c = lstOrder[1].Id
                //                             ,Quote__c = quo.Id),
                // new ProductRequestLineItem( Product2Id = prod.Id
                //                             ,QuantityRequested = 1
                //                             ,ParentId = lstProdRequest[0].Id
                //                             //,Status = 'Réservé à préparer'
                //                             ,Status = 'Réservé à préparer	'
                //                             ,Commande__c = lstOrder[1].Id
                //                             ,Quote__c = quo.Id), 
                // //Order2 6-8
                // new ProductRequestLineItem( Product2Id = prod.Id
                //                             ,QuantityRequested = 1
                //                             ,ParentId = lstProdRequest[0].Id
                //                             //,Status = 'En cours de commande'
                //                             ,Status = 'Réservé à préparer	'
                //                             ,Commande__c = lstOrder[2].Id
                //                             ,Quote__c = quo.Id),
                // new ProductRequestLineItem( Product2Id = prod.Id
                //                             ,QuantityRequested = 1
                //                             ,ParentId = lstProdRequest[0].Id
                //                             //,Status = 'Transféré'
                //                             ,Status = 'Réservé à préparer	'
                //                             ,Commande__c = lstOrder[2].Id
                //                             ,Quote__c = quo.Id),
                // new ProductRequestLineItem( Product2Id = prod.Id
                //                             ,QuantityRequested = 1
                //                             ,ParentId = lstProdRequest[0].Id
                //                             //,Status = 'Réservé à préparer'
                //                             ,Status = 'Réservé à préparer	'
                //                             ,Commande__c = lstOrder[2].Id
                //                             ,Quote__c = quo.Id)                                                                                    
            };                   
            insert lstPRLI;

            
           
            
            
            
        }
        
    }  

    @isTest
    public static void testResPrepareElse(){

        System.runAs(mainUser){
                
        Test.startTest();                    
            // lstPRLI[1].Status = 'Réservé à préparer'; //DMU 20200914 - Changed picklist value on test class due to homeserve not having 'Réservé préparé'
            lstPRLI[1].Status = 'Brouillon';
        	update lstPRLI[1];
        Test.stopTest();

        // ProductRequestLineItem pr = [SELECT Id, DestinationLocationId FROM ProductRequestLineItem WHERE Id = :lstPRLI[0].Id];

        // System.assertEquals(pr.DestinationLocationId, stlCommander.LocationId);
        }


    } 

    @isTest
    public static void testPrliACommander(){

        System.runAs(mainUser){
                
        Test.startTest();                    
            lstPRLI[0].Status = 'A commander';
        	update lstPRLI;
        Test.stopTest();

        ProductRequestLineItem pr = [SELECT Id, DestinationLocationId FROM ProductRequestLineItem WHERE Id = :lstPRLI[0].Id];

        System.assertEquals(pr.DestinationLocationId, stlCommander.LocationId);
        }


    }    
    
    
    
    public static sofactoapp__Plan_comptable__c createPlanComptable(){
        sofactoapp__Plan_comptable__c plan = new sofactoapp__Plan_comptable__c();
        plan.Name = 'sofactoapp__Plan_comptable__c';
        insert plan;
        return plan;
    }
    
    public static sofactoapp__Compte_comptable__c createCompteComptable(){
        sofactoapp__Plan_comptable__c chartAccount = createPlanComptable();
        sofactoapp__Compte_comptable__c compte = new sofactoapp__Compte_comptable__c();
        compte.Name = 'compte comptable';
        compte.sofactoapp__Plan_comptable__c = chartAccount.Id;
        compte.sofactoapp__Libelle__c = 'sofactoapp__Libelle__c';
        insert compte;
        return compte;
            
    }
    
    public static sofactoapp__Compte_auxiliaire__c createCompteAuxilaire(Id accountId){
        sofactoapp__Compte_comptable__c comptable = createCompteComptable();
        sofactoapp__Compte_auxiliaire__c compte = new sofactoapp__Compte_auxiliaire__c();
        compte.sofactoapp__Compte__c = accountId;
        compte.sofactoapp__Compte_comptable__c = comptable.Id;
        compte.Compte_comptable_Prelevement__c = comptable.Id;
        compte.sofactoapp__Libelle__c = 'sofactoapp__Libelle__c';
        insert compte;
        return compte;
        
    }
    
    @isTest
    public static void testCreateProductRequestLineItem(){
        System.runAs(mainUser){
            
        }
    }
   
    @isTest
    public static void testQuoteProductTransfers(){
        
    	System.runAs(mainUser){
                
        quo.Status=AP_Constant.quoteStatusValideSigne;
        quo.Devis_signe_par_le_client__c = true; // SH - 2019-01-22 - Due to new Validation rule
        quo.Date_de_debut_des_travaux__c = System.today(); // SH - 2019-01-22 - Due to new Validation rule
                
        Test.startTest();                    
        	update quo;
        Test.stopTest();
                
        //List<ProductRequest> lstPr = [SELECT Id, WorkOrderId, NeedByDate, AccountId FROM ProductRequest 
          //                                    WHERE WorkOrderId =: quo.Ordre_d_execution__c];
                
      //  System.assertEquals(lstPr.size(),1);
                
        //ProductRequestLineItem prli = [SELECT Id, Status FROM ProductRequestLineItem WHERE Product2Id= :lstQli[0].Product2Id];
        //System.assertEquals(prli.Status, 'Réservé à préparer');                               
        //Date needByDate = AP36_QuoteProductTransfers.getNeedByDate(System.today());
        }
    }

    @isTest
    public static void testResPrepare(){        
    	System.runAs(mainUser){                
        Test.startTest();                    
        	AP36_QuoteProductTransfers.ResPrepareElse(new Set<Id> {lstPRLI[0].Id,lstPRLI[1].Id}, new Set<Id> {st.Id, srvTerr.Id});
        Test.stopTest();
        }
    }

    @isTest
    public static void testPrliReservePrepare(){        
    	System.runAs(mainUser){                
        Test.startTest();                    
        	AP36_QuoteProductTransfers.PrliReservePrepare(new Set<Id> {lstPRLI[0].Id,lstPRLI[1].Id}, new Set<Id> {st.Id, srvTerr.Id});
        Test.stopTest();
        }
    }


}