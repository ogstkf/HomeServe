/**
 * @File Name          : LC12_QuoteTypeControllerTest.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 21-07-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    06/02/2020   AMO     Initial Version
**/
@isTest
public class LC12_QuoteTypeControllerTest {
    
    static List<Account> accountList;
    static List<User> userList;
    static List<Quote> quotes = new List<Quote>();
    static List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();
    static List<OperatingHours> operatingHourList; 
    static List<ServiceTerritory> serviceTerritoryList;
    static List<Product2> productList;
    static List<Opportunity> opportunityList;
    static List<Agency_Catalogue__c> agencyCatalogList;
    static List<Agency_intervention_zones__c> agencyInterventionList;
    static void createAllData(){
        String assetTpe = 'Pompe à chaleur air/eau';
        
        operatingHourList = DataTST.createOperatigHours(1);
        serviceTerritoryList = DataTST.createServiceTerritories(5, operatingHourList[0]);
        
        accountList = DataTST.createAccounts(1);

        for(Integer i=0; i<accountList.size(); i++){
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(accountList.get(i).Id);
            accountList.get(i).sofactoapp__Compte_auxiliaire__c = aux0.Id;
        }

        update accountList;


        userList = DataTST.createUsers(1, accountList);
        productList = DataTST.createProductsAndPriceBookEntries(1);
        agencyInterventionList = DataTST.createAgencyInterventionZones(serviceTerritoryList);
        opportunityList = DataTST.createOpportunities(1, accountList[0], productList[0], agencyInterventionList[0]);

        Quote q = new Quote();
        q.isSync__c = false;
        q.Name = 'quote';
        q.Description = 'description';
        q.Agency__c = serviceTerritoryList.get(0).Id;
        q.OpportunityId = opportunityList.get(0).Id;
        insert q;
        quotes.add(q);
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Product2Id =: productList.get(0).Id LIMIT 1];
        
        
        QuoteLineItem line = new QuoteLineItem();
        line.Remise_en100__c = 20;
        line.Product2Id = productList.get(0).Id;
        line.Quantity = 1;
        line.QuoteId = q.Id;
        line.UnitPrice = 20;
        line.PricebookEntryId = pbe.Id;
        insert line;
        
        quoteLineItems.add(line);
    }
    
    static testMethod void testQuoteType(){
        createAllData();
        Test.startTest();
        Devis_type__c devis = LC12_QuoteTypeController.getNewDevisType(quotes.get(0).Id);
        insert devis;
        LC12_QuoteTypeController.createLigneDevisType(devis.Id, quotes.get(0).Id);
        Test.stopTest();
    }
}