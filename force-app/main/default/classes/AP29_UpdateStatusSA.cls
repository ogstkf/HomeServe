/**
 * @File Name          : AP29_UpdateStatusSA.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 17/02/2020, 14:49:16
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/02/2020   AMO     Initial Version
**/
public with sharing class AP29_UpdateStatusSA {
/**
 * @File Name          : AP29_UpdateStatusSA.cls
 * @Description        : 
 * @Author             : Spoon Consulting(KZE)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 17/02/2020, 14:49:16
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         21-10-2019               KZE         Initial Version
 * 1.1         28-01-2020               SH          Added WO StartDate & EndDate
**/

    public static void updateWorkOrderCase(set<id> setIdSA){
        System.debug('##starting method updateWorkOrderCase: ');

        List<WorkOrder> lstWOToUpdate = new List <WorkOrder>();
        List<Case> lstCaseToUpdate = new List <Case>();
        Set<Id> setIdCase = new Set <Id>();
        Set<Id> setIdWO = new Set <Id>();

        for(ServiceAppointment sa: [SELECT Id , Work_Order__r.Id , Work_Order__r.CaseId
                                    FROM ServiceAppointment WHERE Id IN :setIdSA]){
            System.debug('SA: ' +sa.Work_Order__r.Id + ' & ' + sa.Work_Order__r.CaseId);
            setIdWO.add(sa.Work_Order__r.Id);
            setIdCase.add(sa.Work_Order__r.CaseId);
        }

        for(WorkOrder wo: [SELECT Id, Status, StartDate, EndDate FROM WorkOrder WHERE Id in:setIdWO]){
            System.debug('ONE WO');
            wo.Status= 'Cancelled';
            wo.StartDate = System.now().addHours(-1); // SH - 2020-01-28
            wo.EndDate = System.now(); // SH - 2020-01-28
            lstWOToUpdate.add(wo);
        }

        for(Case c: [SELECT Id, Status FROM Case WHERE Id IN:setIdCase]){
            System.debug('ONE Case');
            c.Status= 'Pending Action from Cham';
            lstCaseToUpdate.add(c);
        }

        System.debug('## Size lstWOToUpdate: '+lstWOToUpdate.size());
        if(lstWOToUpdate.size() >0){
            update lstWOToUpdate;
        }
        System.debug('## Size lstCaseToUpdate: '+lstCaseToUpdate.size());
        if(lstCaseToUpdate.size() >0){
            update lstCaseToUpdate;
        }
    }
}