/**
 * @File Name          : FacturesTriggerHandler.cls
 * @Description        : 
 * @Author             : SSA
 * @Group              : 
 * @Last Modified By   : SSA
 * @Last Modified On   : 02/05/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/12/2019           SSA                  Initial Version
 * 1.1    02/05/2020      
**/
public with sharing class FacturesTriggerHandler 
{
    
    
	public void handlebeforedelete (List<sofactoapp__Factures_Client__c> lstOldfact)
    {
        system.debug('handele before delete facture');
        Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
        for (sofactoapp__Factures_Client__c  Fact : lstOldfact)
       	{
           
          if  (userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('FacturesTriggerHandler')){
              system.debug ('etat de la facture ' + FACT.sofactoapp__Etat__c);
            	if ( Fact.Sofacto_Total_reglements__c >0 && Fact.sofactoapp__Etat__c =='Brouillon'){
                    system.debug ('suppression de la facture ' + FACT);
                    Fact.addError(System.Label.sofacto_suppression_facture );
            	}
           }
        }
    }
    
    public void handleafterdelete (List<sofactoapp__Factures_Client__c> lstOldfact)
    {
        system.debug ('after delete trgigger hander');
        Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
        set <ID> quoteIds = new set<Id>();
        set <ID> contractIds= new set<Id>();
        for (sofactoapp__Factures_Client__c  Fact : lstOldfact)
        {
            if  (userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('Factureshandleafterdelete')){
                if (Fact.sofactoapp__Type_de_facture__c =='Facture')
                {
                quoteIds.add(Fact.sofacto_Devis__c);
                contractIds.add(Fact.Sofactoapp_Contrat_de_service__c);
                }
            }
        }
    // traitement des facture contrat

        // requetes des contrat plus laliste associé des factures
        if (!contractIds.isempty())
        {
            List<ServiceContract> contrat2update = [select id, (select id from Factures__r) from ServiceContract where id in : contractIds
                                                         and tech_facture_OK__c =true ];
            for (ServiceContract cts : contrat2update)
            {
             list <sofactoapp__Factures_Client__c> listAsso = cts.Factures__r;
            system.debug('update ongoing contrat '+ cts + 'associé avec '+ listAsso.size() + ' facture' );
            if (listAsso.size() < 1)
            
            cts.tech_facture_OK__c =false;
            }

            if (contrat2update.size() >0)
            {    
            update contrat2update;
            }   
        }

        // traitement des factures devis
        if (!quoteIds.isempty()){
            list<quote> quote2update = [select id, (select id from Factures__r) from quote where id in : quoteIds];
       
            for (quote qt : quote2update)
            {
            list <sofactoapp__Factures_Client__c> listAsso = qt.Factures__r;
            system.debug('update ongoing quote '+ qt + 'associé avec '+ listAsso.size() + ' facture' );
            if (listAsso.size() < 1)

            qt.tech_deja_facture__c= false;
            }
        
            if (quote2update.size() >0)
            { 
            update quote2update;
            }
         }
        
    }

   public void handlebeforeinsert (List<sofactoapp__Factures_Client__c> lstnewfact)
    {
      	system.debug('handele before inser facture');
        set <id> factureinit = new set <id>();
        for (sofactoapp__Factures_Client__c FACT: lstnewfact)
        {
            
         factureinit.add(Fact.sofactoapp__Facture_initiale__c);
        }
        Map <id, sofactoapp__Factures_Client__c> FactinitMAp = new map <id,sofactoapp__Factures_Client__c> ([select id,SofactoappType_Prestation__c,Beneficiaire_Prime__c,
                                                                                                             Sofactoapp_Contrat_de_service__c,sofacto_Devis__c
                                                                                                            from sofactoapp__Factures_Client__c where id in : factureinit] );

        For (sofactoapp__Factures_Client__c FACT: lstnewfact)
        {
            if (Fact.sofactoapp__Type_de_facture__c =='Avoir' && Fact.sofactoapp__Facture_initiale__c !=null)
            {
            FACT.SofactoappType_Prestation__c = FactinitMAp.get(FACT.sofactoapp__Facture_initiale__c).SofactoappType_Prestation__c;
            FACT.Beneficiaire_Prime__c = FactinitMAp.get(FACT.sofactoapp__Facture_initiale__c).Beneficiaire_Prime__c;
            FACT.Sofactoapp_Contrat_de_service__c = FactinitMAp.get(FACT.sofactoapp__Facture_initiale__c).Sofactoapp_Contrat_de_service__c;
            FACT.sofacto_Devis__c = FactinitMAp.get(FACT.sofactoapp__Facture_initiale__c).sofacto_Devis__c;
              
             system.debug('boucle handle befire insert facturre '+ FACT);
              
            }
        }

    } 

}