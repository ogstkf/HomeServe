/**
 * @File Name         : BAT18_DeleteDocumentIdData
 * @Description       : 
 * @Author            : Spoon Consutling (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 22-09-2021
 * Modifications Log 
 * ========================================================
 * Ver   Date               Author          Modification
 * 1.0   10-08-2021         MNA             Initial Version
**/
global with sharing class BAT18_DeleteDocumentIdData implements Database.Batchable <sObject>, Database.Stateful, Database.AllowsCallouts, Schedulable {
    public String query;

    global Database.QueryLocator start(Database.BatchableContext BC){
        query = 'SELECT Id FROM Editique_Document_ID__c';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Editique_Document_ID__c> lstEdiDoc) {
        delete lstEdiDoc;
    }

    global void finish(Database.BatchableContext BC) {
        
    }

    global void execute(SchedulableContext sc) {
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT18_DeleteDocumentIdData');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT18_DeleteDocumentIdData(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT18_DeleteDocumentIdData());
        }
    }
}