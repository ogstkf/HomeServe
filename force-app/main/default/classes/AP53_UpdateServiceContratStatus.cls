/**
 * @File Name          : AP53_UpdateServiceContratStatus.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-08-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/12/2019         LGO                    Initial Version
**/
public without sharing class AP53_UpdateServiceContratStatus {

    public static void updateStatusFromContract(List<ServiceContract> lstSrvCon){
        system.debug('***dmu 1');
        // ZJO CT-1818 18/06/2020 added a new condition (Type = 'indivdual')
        List<ContractLineItem> lstCLI = [SELECT Id, VE_Status__c, ServiceContractId FROM ContractLineItem WHERE ServiceContractId IN :lstSrvCon AND IsBundle__c = true AND ServiceContract.Type__c = 'Individual'];
        Map<Id, ContractLineItem> mapConToCli = new Map<Id, ContractLineItem>();
        for(ContractLineItem cli : lstCLI){
            mapConToCli.put(cli.ServiceContractId, cli);
        }
        System.debug('#### lstCLI:'+lstCLI);

        List<sofactoapp__R_glement__c> lstPaymt = [SELECT Id, statut_du_paiement__c, sofactoapp__En_attente__c, sofactoapp__Facture__r.SofactoappType_Prestation__c, sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c FROM sofactoapp__R_glement__c WHERE sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c IN :lstSrvCon AND  statut_du_paiement__c IN ('Collecté', 'Encaissé') ORDER BY CreatedDate DESC];
        Map<Id, sofactoapp__R_glement__c> mapConToPaymt = new Map<Id, sofactoapp__R_glement__c>();
        for(sofactoapp__R_glement__c paymt : lstPaymt){
            if(!mapConToPaymt.containsKey(paymt.sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c)){
                mapConToPaymt.put(paymt.sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c, paymt);
            }
        }
        System.debug('##### lstPaymt: '+lstPaymt);

        List<ServiceContract> lstSrvConToUpdt = new List<ServiceContract>();
        for(ServiceContract con : lstSrvCon){
            system.debug('***dmu 1');
            
            Boolean isPaymentOk = false;
            Boolean isRenewed = con.Contract_Renewed__c;

            if(mapConToPaymt.containsKey(con.Id) && ((mapConToPaymt.get(con.Id).statut_du_paiement__c == 'Encaissé'  || mapConToPaymt.get(con.Id).statut_du_paiement__c == 'Collecté')&& mapConToPaymt.get(con.Id).sofactoapp__Facture__r.SofactoappType_Prestation__c == 'Contrat individuel')){
                system.debug('***dmu 2');
                isPaymentOk = true;                
            }else if(con.Sofactoapp_Mode_de_paiement__c == 'Prélèvement' && con.sofactoapp_Rib_prelevement__c != null){
                system.debug('***dmu 3');
                isPaymentOk = true;
            }
            
            System.debug('##### isPaymentOk: '+isPaymentOk);
            
            ServiceContract conToUpdt = new ServiceContract(Id = con.Id);
            if(con.Sofactoapp_Mode_de_paiement__c == 'Prélèvement'){
                system.debug('***dmu 4');
                if(isRenewed){
                    if(con.StartDate >= Date.Today() && (con.Contract_Status__c == 'Actif - en retard de paiement' || con.Contract_Status__c == 'Pending Payment')){
                        con.Contract_Status__c = 'Active';
                    }
                    else if(con.Contract_Status__c == 'Pending Payment'){
                        con.Contract_Status__c = 'En attente de renouvellement';
                    }
                }
                else{
                    con.Contract_Status__c = 'Active';
                }
                
            }else{
                if(!isPaymentOk){
                    conToUpdt.Contract_Status__c = 'Actif - en retard de paiement';
                }
                else if(isPaymentOk){
                    conToUpdt.Contract_Status__c = 'Active';
                }
                else if(isRenewed && con.StartDate > Date.Today() && isPaymentOk){
                    conToUpdt.Contract_Status__c = 'En attente de renouvellement';
                }
            }
            lstSrvConToUpdt.add(conToUpdt);
        }

        System.debug('##### lstSrvConToUpdt: '+lstSrvConToUpdt);
        if(lstSrvConToUpdt.size()>0){
            update lstSrvConToUpdt;
        }
    }

    public static void updateStatusFromPayment(List<sofactoapp__R_glement__c> lstPymt){
        System.debug('### lstPymt: '+lstPymt);
        Set<Id> setInvoiceId = new Set<Id>();
        for(sofactoapp__R_glement__c pymt : lstPymt){
            setInvoiceId.add(pymt.sofactoapp__Facture__c);
        }

        List<sofactoapp__Factures_Client__c> lstInvoice = [SELECT Id, Sofactoapp_Contrat_de_service__c, Sofactoapp_Contrat_de_service__r.Contrat_signe_par_le_client__c FROM sofactoapp__Factures_Client__c WHERE Id IN :setInvoiceId];
        Set<Id> setContractId = new Set<Id>();
        for(sofactoapp__Factures_Client__c inv: lstInvoice){
            setContractId.add(inv.Sofactoapp_Contrat_de_service__c);
        }
        System.debug('#### lstInvoice: '+lstInvoice);
		//DMU - 20200617 - Added condition on Type__c	 = 'Individual' [CT-1818]
        List<ContractLineItem> lstCLI = [SELECT Id, VE_Status__c, ServiceContract.Contrat_signe_par_le_client__c, ServiceContract.Contract_Renewed__c, ServiceContract.StartDate FROM ContractLineItem WHERE ServiceContractId IN :setContractId AND IsBundle__c = true AND ServiceContract.Contract_Status__c != 'Cancelled' AND ServiceContract.Contract_Status__c != 'Résilié' AND ServiceContract.Contract_Status__c != 'Expired' AND ServiceContract.Type__c = 'Individual'];
        Map<Id, ContractLineItem> mapConToCli = new Map<Id, ContractLineItem>();
        for(ContractLineItem cli : lstCLI){
            mapConToCli.put(cli.ServiceContractId, cli);
        }
        System.debug('##### lstCLI: '+lstCLI);

        List<ServiceContract> lstConToUpdate = new List<ServiceContract>();
        for(Id conId: setContractId){
            ServiceContract con = new ServiceContract(Id = conId);
            // Boolean isVeOk = false;
            // Boolean isSignatureOk = false;
            if(mapConToCli.containsKey(conId)){                
                con.Contract_Status__c = 'Active';         
                lstConToUpdate.add(con);
            }
        }
        System.debug('#### lstConToUpdate: '+lstConToUpdate);
        if(lstConToUpdate.size()>0){
            update lstConToUpdate;
        }
    }

    // public static void updateStatusFromCLI(List<ContractLineItem> lstCLI){
        // Set<Id> setContractId = new Set<Id>();
        // for(ContractLineItem cli: lstCLI){
        //     setContractId.add(cli.ServiceContractId);
        // }
        // ZJO CT-1818 17/06/2020 added a new condition (Type = 'indivdual')
        // List<ServiceContract> lstCon = [SELECT Id, Contrat_signe_par_le_client__c, Sofactoapp_Mode_de_paiement__c, Contract_Renewed__c, sofactoapp_Rib_prelevement__c FROM ServiceContract WHERE Id IN: setContractId AND Contract_Status__c != 'Cancelled' AND Contract_Status__c != 'Résilié' AND Contract_Status__c != 'Expired' AND Type__c	 = 'Individual'];
        // System.debug('#### lstCon: '+lstCon);
        
        // List<sofactoapp__R_glement__c> lstPaymt = [SELECT Id, sofactoapp__En_attente__c, sofactoapp__Facture__r.SofactoappType_Prestation__c, sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c, sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__r.Contrat_signe_par_le_client__c, statut_du_paiement__c FROM sofactoapp__R_glement__c WHERE sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c IN :lstCon  AND statut_du_paiement__c IN ('Collecté', 'Encaissé') ORDER BY CreatedDate DESC];
        // System.debug('#### lstPaymt: '+lstPaymt);

        // Map<Id, sofactoapp__R_glement__c> mapConToPaymt = new Map<Id, sofactoapp__R_glement__c>();
        // for(sofactoapp__R_glement__c paymt : lstPaymt){
        //     if(!mapConToPaymt.containsKey(paymt.sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c)){
        //         mapConToPaymt.put(paymt.sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c, paymt);
        //     }
        // }

        // for(ServiceContract con : lstCon){
        //     Boolean isPaymentOk = false;
        //     Boolean isSignatureOk = con.Contrat_signe_par_le_client__c;
        //     Boolean isRenewed = con.Contract_Renewed__c;
            
        //     if(mapConToPaymt.containsKey(con.Id)){
        //         if((mapConToPaymt.get(con.Id).statut_du_paiement__c == 'Collecté' || mapConToPaymt.get(con.Id).statut_du_paiement__c == 'Encaissé') && mapConToPaymt.get(con.Id).sofactoapp__Facture__r.SofactoappType_Prestation__c == 'Contrat individuel'){
        //             isPaymentOk = true;
        //         }
        //     }else if(con.Sofactoapp_Mode_de_paiement__c == 'Prélèvement' && con.sofactoapp_Rib_prelevement__c != null){
        //         isPaymentOk = true;
        //     }
        //     System.debug('#### con.Sofactoapp_Mode_de_paiement__c: '+con.Sofactoapp_Mode_de_paiement__c);
        //     System.debug('#### con.sofactoapp_Rib_prelevement__c: '+con.sofactoapp_Rib_prelevement__c);
        //     System.debug('#### isPaymentOk: '+isPaymentOk);
        //     System.debug('#### isSignatureOk: '+isSignatureOk);

            // if(isSignatureOk && isPaymentOk){
            //     con.Contract_Status__c = 'Active';
            // }
            // else if(!isSignatureOk && isPaymentOk){
            //     con.Contract_Status__c = 'Active';
            // ZJO CT-1818 17/06/2020 added a new condition (!isSignatureOk && isRenewed)
            // }
            // else if((isSignatureOk || (!isSignatureOk && isRenewed)) && !isPaymentOk){
            //     con.Contract_Status__c = 'Actif - en retard de paiement';
            // }
            // else{
            //     con.Contract_Status__c = 'Pending Client Validation';
            // }
        // }

        // if(lstCon.size()>0){
        //     update lstCon;
        // }
    // }
    public static void updateStatusVE(List<ServiceContract> lstSrvCon){

        List<ContractLineItem> lstCLI = [SELECT Id, VE_Status__c, ServiceContractId FROM ContractLineItem WHERE ServiceContractId IN :lstSrvCon AND IsBundle__c = true];
        Map<Id, ContractLineItem> mapConToCli = new Map<Id, ContractLineItem>();
        list<ContractLineItem> lstCLIToUpd = new list <ContractLineItem>();
        for(ContractLineItem cli : lstCLI){
            mapConToCli.put(cli.ServiceContractId, cli);
        }
        system.debug('*** mapConToCli '+mapConToCli);

        for(ServiceContract sc : lstSrvCon){
            system.debug('@@scTest ' +sc);
            ContractLineItem cli = new ContractLineItem();
            cli = mapConToCli.get(sc.Id);
            if(sc.Statut_VE__c == 'VE effectuée'){
                cli.VE_Status__c = 'Complete';
                cli.Completion_Date__c = sc.StartDate;
                cli.Desired_VE_Date__c = sc.StartDate;
            }
            if(sc.Statut_VE__c == 'VE non nécessaire'){
                system.debug('*** 22222');
                cli.VE_Status__c = 'VE non nécessaire';
            }
            lstCLIToUpd.add(cli);
        }
        system.debug('*** lstCLIToUpd.size(): '+lstCLIToUpd);
        if(lstCLIToUpd.size()>0){
            update lstCLIToUpd;
        }        
    }
}