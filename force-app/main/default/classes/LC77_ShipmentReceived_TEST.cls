/**
 * @description       : 
 * @author            : Spoon Consulting
 * @group             : 
 * @last modified on  : 02-15-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * =========================================================================
 * Ver   Date         Author                               Modification
 * 1.0   15-02-2021   MNA                                  Initial Version
**/
@isTest
public with sharing class LC77_ShipmentReceived_TEST {
    static User mainUser;
    static List<Shipment> lstShip;
    static List<ProductTransfer> lstProT;
    static{
        mainUser = TestFactory.createAdminUser('LC77@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;
        System.runAs(mainUser){
            lstShip = new List<Shipment>{
                new Shipment(ShipToName = 'test1'),
                new Shipment(ShipToName = 'test2')
            };
            insert lstShip;
            lstProT = new List<ProductTransfer>{
                new ProductTransfer(QuantitySent = 2, ShipmentId = lstShip[0].Id, Description = 'Test1'),
                new ProductTransfer(QuantitySent = 2, quantity_lot_received__c = 1, ShipmentId = lstShip[0].Id, Description = 'Test2'),
                new ProductTransfer(QuantitySent = 2, ShipmentId = lstShip[1].Id, Description = 'Test3')
            };
            insert lstProT;
        }
    }
    @isTest static void testrecu(){
        Test.startTest();
        LC77_ShipmentReceived.recu(lstShip[0].Id);
        Test.stopTest();
        Map<String, ProductTransfer> mapProT = new Map<String, ProductTransfer>();
        for (ProductTransfer proT : [SELECT Description,quantity_lot_received__c,IsReceived FROM ProductTransfer]) {
            mapProT.put(proT.Description, proT);
        }
        System.assertEquals(0, mapProT.get('Test1').quantity_lot_received__c);
        System.assertEquals(1, mapProT.get('Test2').quantity_lot_received__c);
        System.assertEquals(true, mapProT.get('Test1').IsReceived);
        System.assertEquals(true, mapProT.get('Test2').IsReceived);
        System.assertEquals(false, mapProT.get('Test3').IsReceived);
    }
}