/**
 * @File Name          : VFC12_GenerateReport_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 29/03/2020, 20:34:23
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    29/03/2020   ZJO     Initial Version
**/
@isTest
public with sharing class VFC12_GenerateReport_TEST {
    static User mainUser;
    static Product2 prod = new Product2();
    static List<ProductRequestLineItem > lstPRLI = new List<ProductRequestLineItem >();
    static List<ProductRequest> lstProdRequest = new List<ProductRequest>();
    static  Bypass__c userBypass = new Bypass__c();
    
    static{
        mainUser = TestFactory.createAdminUser( 'VFC12@test.com', TestFactory.getProfileAdminId() );
        insert mainUser;
        userBypass.SetupOwnerId  = mainUser.Id;
        userBypass.BypassValidationRules__c = true;
        insert userBypass;

        System.runAs(mainUser){
            //Create your product
            prod = new Product2(
                Name = 'Product X',
                ProductCode = 'Pro-X',
                isActive = true,
                Equipment_family__c = 'Chaudière',
                Equipment_type__c = 'Chaudière gaz',
                RecordTypeId =  Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId(), 
                Famille_d_articles__c = AP_constant.productFamillePieceDetache
            );
            insert prod;
            //pricebook entry
            // PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            // insert PrcBkEnt;
            

            lstProdRequest = new List<ProductRequest> {
                new ProductRequest(),
                new ProductRequest(),
                new ProductRequest()
            };  
            insert lstProdRequest;          

            lstPRLI = new List<ProductRequestLineItem > {
                new ProductRequestLineItem( 
                                            Product2Id = prod.Id,
                                            QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id),
                                          
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id),
                //Order1 3-5
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id)
                                                                                               
            };                   
            insert lstPRLI;  

        }
    }

    @IsTest
    public static void selectedListTest(){
        System.runAs(mainUser){
			
			Test.startTest();

				List<ProductRequestLineItem> lstPRLI1 = new List<ProductRequestLineItem>([SELECT id 
															  FROM ProductRequestLineItem
															  WHERE id in: lstPRLI]);
				System.debug('mgr lstPRLI1 ' + lstPRLI1);

				Test.setCurrentPage(Page.VFP12_GenerateReport);
                ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstPRLI1);
                stdSetController.setSelected(lstPRLI1); 

                VFC12_GenerateReport ext = new VFC12_GenerateReport(stdSetController);
	        Test.stopTest();

		}
    }

}