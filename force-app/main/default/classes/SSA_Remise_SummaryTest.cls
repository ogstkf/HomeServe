/**
 * @File Name          : MakeInvoiceIssuedTest.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/02/2020   AMO     Initial Version
**/
@isTest
public class SSA_Remise_SummaryTest {
    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<ServiceContract> lstSerCon = new List<ServiceContract>();
    static List<sofactoapp__Factures_Client__c> lstFacturesClients = new List<sofactoapp__Factures_Client__c>();
    static List<sofactoapp__R_glement__c> lstRGlements = new List<sofactoapp__R_glement__c>();
    static list <Id> RemiseId = new list <Id>() ;

    static {
        adminUser = TestFactory.createAdminUser('testuser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser) {
            for (Integer i = 0; i < 5; i++) {
                lstTestAcc.add(TestFactory.createAccountBusiness('testAcc' + i));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'BusinessAccount');
                lstTestAcc[i].BillingPostalCode = '1234' + i;
                lstTestAcc[i].Rating__c = 4;
            }
            insert lstTestAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstTestAcc.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstTestAcc.get(2).Id);
            sofactoapp__Compte_auxiliaire__c aux3 = DataTST.createCompteAuxilaire(lstTestAcc.get(3).Id);
            sofactoapp__Compte_auxiliaire__c aux4 = DataTST.createCompteAuxilaire(lstTestAcc.get(4).Id);
			lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstTestAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstTestAcc.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            lstTestAcc.get(3).sofactoapp__Compte_auxiliaire__c = aux3.Id;
            lstTestAcc.get(4).sofactoapp__Compte_auxiliaire__c = aux4.Id;
            
            update lstTestAcc;

            for (Integer i = 0; i < 5; i++) {
                lstSerCon.add(TestFactory.createServiceContract('test' + i, lstTestAcc[i].Id));
                lstSerCon[i].Type__c = 'Individual';
                lstSerCon[i].Contract_Status__c = 'Cancelled';
                lstSerCon[i].StartDate =  Date.newInstance(2019, 11, 20);
                lstSerCon[i].EndDate  = Date.newInstance(2020, 1, 20);
                lstSerCon[i].Contrat_resilie__c = false;
            }
            lstSerCon[0].StartDate =  Date.newInstance(2019, 12, 20);
            lstSerCon[3].StartDate =  Date.newInstance(2018, 12, 20);
            lstSerCon[4].StartDate =  Date.newInstance(2018, 1, 12);
            insert lstSerCon;
            
            

            sofactoapp__Raison_Sociale__c RS = new sofactoapp__Raison_Sociale__c(Name= 'CHAM2', sofactoapp__Credit_prefix__c= '145', sofactoapp__Invoice_prefix__c='982');
            insert RS;
            sofactoapp_remise__c Rb = new sofactoapp_remise__c(Sofacto_Raison_sociale__c=RS.Id, Type_remise__c='Chèque',
                                                              Banque_de_depot__c='CDN');
            insert Rb;
            RemiseId.add(Rb.id);

            for (Integer i = 0; i < 4; i++) {
                lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                        sofactoapp__Compte__c = lstTestAcc[i].Id
                        , sofactoapp__emetteur_facture__c = RS.Id
                    	, sofactoapp__Etat__c ='brouillon'
                    	, sofactoapp__Date_de_facture__c = Date.newInstance(2018, 12, 20)
                ));
            }
            insert lstFacturesClients;

            for (Integer i = 0; i < 49; i++) {
                lstRGlements.add(new sofactoapp__R_glement__c(
                        sofactoapp__Facture__c = lstFacturesClients[2].Id
                        , sofactoapp__Montant__c = 22
                        , sofactoapp__Mode_de_paiement__c = 'Chèque'
                        , statut_du_paiement__c = 'Collecté'
                        , sofactoapp_numeroro_de_Cheque__c = String.valueOf(i+1)
                        , sofactoapp_Nom_de_la_banque__c = 'Société Générale'
                     	,sofactoapp__Date__c = Date.newInstance(2019, 12, 20)
                    	,sofactoapp_Remise__c = Rb.id
                ));
            }
            
            
            
            insert lstRGlements;
                      
        }
           
      
    	}

     	static testMethod  void SSA_Remise_Summary() {
       		 Test.startTest();
          	SSA_Remise_Summary.RollupRemise(RemiseId);
       		Test.stopTest();
                    }
    	
  
           


}