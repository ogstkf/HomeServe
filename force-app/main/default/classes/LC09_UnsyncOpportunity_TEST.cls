/**
 * @File Name          : LC09_UnsyncOpportunity_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 07-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class LC09_UnsyncOpportunity_TEST {
 /**
 * @File Name          : LC09_UnsyncOpportunity_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 07-15-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         19-10-2019     		LGO         Initial Version
**/
static User mainUser;
    static Account testAcc = new Account();
    static Opportunity testOpp= new Opportunity();
    static Opportunity testOpp1= new Opportunity();
    static OpportunityLineItem oli = new OpportunityLineItem();
    static Quote quo = new Quote();

    static{
        mainUser = TestFactory.createAdminUser('LC09_UnsyncOpportunity@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccount('AP22_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

           //opportunity
            testOpp = TestFactory.createOpportunity('test', testAcc.Id);
            insert testOpp;

           //opportunity
            testOpp1 = TestFactory.createOpportunity('test1', testAcc.Id);
            insert testOpp1;

            Id pricebookId = Test.getStandardPricebookId();
            //Create your product
            Product2 prod = new Product2(Name = 'Product X',
                                                                ProductCode = 'Pro-X',
                                                                isActive = true,
                                                                Equipment_family__c = 'Chaudière',
                                                                Equipment_type__c = 'Chaudière gaz',
                                                                Statut__c = 'Approuvée'
            );
            insert prod;

           //Create your pricebook entry
            PricebookEntry pbEntry = new PricebookEntry(Pricebook2Id = pricebookId,
                                                                                                Product2Id = prod.Id,
                                                                                                UnitPrice = 100.00,
                                                                                                IsActive = true
            );
            insert pbEntry;

             oli = new OpportunityLineItem(OpportunityId = testOpp.Id,
                                                                                                        Quantity = 5,
                                                                                                        PricebookEntryId = pbEntry.Id,
                                                                                                        TotalPrice = pbEntry.UnitPrice
            );


            //create Quote
            quo = new Quote(Name ='Test1',
                            Devis_signe_par_le_client__c = false,
                            OpportunityId = testOpp.Id);
            insert quo;

            testOpp.Tech_Synced_Devis__c = quo.id;
            update testOpp;


        }
    }

    @isTest
    public static void testHasDevis(){
        System.runAs(mainUser){
            String msg;

            Test.startTest();
                msg = LC09_UnsyncOpportunity.clearDevisOpp(testOpp.Id,false);
            Test.stopTest();

            Opportunity oppResult = [SELECT Id,
                                    Tech_Synced_Devis__c
                                    FROM Opportunity where Id =: testOpp.Id] ;
            

            System.assertEquals(oppResult.Tech_Synced_Devis__c,null);
            System.assertEquals(msg,'L\'opportunité a été désynchronisée avec succès');
        }
    }

    @isTest
    public static void testNoDevisFound(){
        System.runAs(mainUser){
            String msg;

            Test.startTest();
                msg = LC09_UnsyncOpportunity.clearDevisOpp(testOpp1.Id,false);
            Test.stopTest();


            System.assertEquals(msg,'Aucune devis n\'a été retrouvé pour la désynchronisation');
        }
    }

    @isTest
    public static void testDeleteOppLineItem(){
        System.runAs(mainUser){
            String msg;

            Test.startTest();
                msg = LC09_UnsyncOpportunity.clearDevisOpp(testOpp.Id,true);
            Test.stopTest();


            System.assertEquals(msg,'L\'opportunité a été désynchronisée avec succès et les lignes de l\'opportunité ont été supprimées avec succès');
        }
    }

}