/**
 * @File Name          : AP37_ProdReqstLineItem.cls
 * @Description        : 
 * @Author             : SHU
 * @Group              : 
 * @Last Modified By   : SHU
 * @Last Modified On   : 04/11/2019, 21:30:13
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0     04/11/2019         SHU                   Initial Version  (CT-1087)
**/
public with sharing class AP37_ProdReqstLineItem {
    public static void setStatusAcommander(Set<Id> setOrderIds){
        List<ProductRequestLineItem> lstPRLI = new List<ProductRequestLineItem>();
        // select and loop over eligible product request line items to set to A commander
        for(ProductRequestLineItem prli : [SELECT Id, Status FROM ProductRequestLineItem where Commande__c IN :setOrderIds ]){ 
            prli.Status = 'A commander'; //To use AP_Constants
            prli.Commande__c = null;
            lstPRLI.add(prli);
            
        }
        if(lstPRLI.size() > 0){
            update lstPRLI;
        }
    }
}