global with sharing class BAT05_getPBELastHour implements Database.Batchable <sObject>, Database.Stateful, Schedulable {  
    global String query;
    global set<id> setProductId = new set<id>();
    
    global BAT05_getPBELastHour(){
        DateTime dt = System.Now().addHours(-2);
        String formattedDt = dt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        
        query = 'SELECT  id, ' +
            'unitprice, ' +
            'pricebook2.recordtype.NAME, ' +
            'prix_achat_brut_prix_public__c, ' +
            'product2.famille_d_articles__c, ' +
            'product2.Sous_familles_d_articles__c, ' +
            'product2.brand__c, ' +
            'product2.lot__c, ' +
            'product2.id, ' +
            //'tech_concatenatedfields__c, ' +
            'TECH_ConditionMatcher__c, ' +
            'remise__c, ' +
            'pricebook2.compte__c, ' +
            'pricebook2.compte__r.Type_de_fournisseurs__c ' +
            'FROM    pricebookentry ' +
           'WHERE   pricebook2.recordtype.NAME = \'Achat\' ' +
                        'AND product2.recordtype.DeveloperName = \'Article\' ' +
                        'AND product2.famille_d_articles__c IN ( \'Pièces détachées\', \'Appareils\', \'Accessoires\', \'Consommables\', \'Outillage\' ) ' +
                        'AND product2.stock_non_gere__c = false ' +
                        'AND override__c = false ' 
           + 'AND LastModifiedDate >= ' + formattedDt 

           // + ' AND product2.id = \'01t6E0000065VT5QAM\' '
            ;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug(query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<PricebookEntry> scope) {
        system.debug('## execute method ');
        system.Debug('### query: ' + query);
        system.debug('### : ' + scope.size() );
        for(PricebookEntry pbe : scope){
            setProductId.add(pbe.product2id);
        }
    }
    
    
    global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new BAT05_UnitPricePBE(setProductId));
    }
    
    global static String scheduleBatch() {
        BAT05_getPBELastHour sc = new BAT05_getPBELastHour();
        return System.schedule('Batch BAT05_getPBELastHour:' + Datetime.now().format(),  '0 0 0 * * ?', sc);
    }
    
    global void execute(SchedulableContext sc){
        Database.executeBatch(new BAT05_getPBELastHour());
    }  
}