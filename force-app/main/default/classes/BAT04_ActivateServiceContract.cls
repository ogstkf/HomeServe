/**
 * @author  SC (DMU)
 * @version 1.0
 * @since   08/11/2019
 *
 * Description : For Jira ticket CT-1180
 * 
 *-- Maintenance History: 
 *--
 *-- Date         Name  Version  Remarks 
 *-- -----------  ----  -------  ------------------------
 *-- 08-11-2019   DMU    1.0     Initial version
 *-- 09-04-2020   RRJ    1.1     modif according to CT-1586
 *-------------------------------------------------------
 * 
*/
global with sharing class BAT04_ActivateServiceContract implements Database.Batchable <sObject>, Database.Stateful, Schedulable{
    // ZJO CT-1810 17/06/2020 add GrandTotal field in query
    global Database.QueryLocator start(Database.BatchableContext BC){     
        String query = 'SELECT Id, Name, StartDate, EndDate, Status, Contract_Status__c, Subtotal, GrandTotal, TECH_NumberOfDaysPriorEndDate__c, AccountId, contactId, Logement__c, Agency__c, Asset__c, Type__c ';
        query += ' FROM ServiceContract';
        query += ' WHERE Contrat_resilie__c = false ';
        query += ' AND Type__c = \'Individual\' ';
        query += ' AND Contract_Renewed__c = true ';
        query += ' AND (Contract_Status__c = \'Pending Payment\' OR Contract_Status__c = \'En attente de renouvellement\') ';
        query += ' AND StartDate = TODAY ';
       
        system.Debug('### : ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ServiceContract> lstSerCon) {
        List<ServiceContract> lstConToUpdt = new List<ServiceContract>();
        try{
            for(ServiceContract scon : lstSerCon){
                String oldStat = scon.Contract_Status__c;
                // ZJO CT-1810 17/06/2020 (GrandTotal > 0)
                if(scon.Contract_Status__c == 'Pending Payment' && scon.GrandTotal > 0){
                    scon.Contract_Status__c = 'Actif - en retard de paiement';
                // ZJO CT-1810 17/06/2020 ('Pending Payment && GranTotal == 0')
                }else if(scon.Contract_Status__c == 'En attente de renouvellement' || (scon.Contract_Status__c == 'Pending Payment' && scon.GrandTotal == 0)){
                    scon.Contract_Status__c = 'Active';
                }

                if (oldStat != scon.Contract_Status__c) {
                    lstConToUpdt.add(scon);
                }

                if(lstConToUpdt.size()>0){
                    update lstConToUpdt;
                }
            }
        }catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('in finish: ');
    }

    //used to schedule job 
    global static String scheduleBatch() {
        BAT04_ActivateServiceContract scheduler = new BAT04_ActivateServiceContract();
        //String scheduleTime = '0 0 00 * * ?';
        //return System.schedule('Batch:', scheduleTime, scheduler);
        return System.schedule('Batch ActivateServiceContract:' + Datetime.now().format(),  '0 0 0 * * ?', scheduler);
    }

    global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT04_ActivateServiceContract');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT04_ActivateServiceContract(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT04_ActivateServiceContract());
        }
    }
    
}