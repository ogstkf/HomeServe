/**
 * @File Name          : ReglementTriggerHandler.cls
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 31-08-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/12/2019           LGO                  Initial Version
 * 1.1    17/08/2020           DMU                  Added condition on sofactoapp__Mode_de_paiement__c - TEC-123
 * 1.2    31/08/2021           MNA                  Added function handleAfterInsert
**/
public with sharing class ReglementTriggerHandler {
    public void handleafterupdate(List<sofactoapp__R_glement__c> lstOldReg, List<sofactoapp__R_glement__c> lstNewReg){
        Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
        List<sofactoapp__R_glement__c> lstReg = new List<sofactoapp__R_glement__c>(); //AP53


        for(Integer i=0; i<lstNewReg.size() ; i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP53;')){
                System.debug('lstOldReg[i].statut_du_paiement__c = ' + lstOldReg[i].statut_du_paiement__c);
                System.debug('lstNewReg[i].statut_du_paiement__c = ' + lstNewReg[i].statut_du_paiement__c );
                //DMU - 20200817 - Added condition on sofactoapp__Mode_de_paiement__c - TEC-123
                if(lstOldReg[i].statut_du_paiement__c <> lstNewReg[i].statut_du_paiement__c 
                    && lstNewReg[i].TECH_TypePrestation__c == 'Contrat individuel'
                    && (lstNewReg[i].statut_du_paiement__c == 'Collecté' || lstNewReg[i].statut_du_paiement__c == 'Encaissé')
                    ){
                    lstReg.add(lstNewReg[i]);
                }
            }
        }
        if(lstReg.size() > 0){
           AP53_UpdateServiceContratStatus.updateStatusFromPayment(lstReg);
        }

    }

    //MNA 31/08/2021
    public void handleAfterInsert(List<sofactoapp__R_glement__c> lstReg) {
        Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
        List<sofactoapp__R_glement__c> lstRegToUpdate = new List<sofactoapp__R_glement__c>(); //AP53

        System.debug('####lstReg:'+ lstReg);
        
        for(sofactoapp__R_glement__c reg : lstReg) {
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP53;')) {
                if (reg.TECH_TypePrestation__c == 'Contrat individuel'
                   && (reg.statut_du_paiement__c == 'Collecté' || reg.statut_du_paiement__c == 'Encaissé')) {
                        lstRegToUpdate.add(reg);
                }
            }
        }

        if(lstRegToUpdate.size() > 0){
            AP53_UpdateServiceContratStatus.updateStatusFromPayment(lstRegToUpdate);
        }
    }

    // ajouté par souleymane pour gérer les deletes
    
      public void handlebeforedelete (List<sofactoapp__R_glement__c> lstOldReg)
    {
        Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
        for (sofactoapp__R_glement__c  reg : lstOldReg)
        {
            if (userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('ReglementTriggerHandler') ) 
            {
            	if ( !string.isEmpty(reg.statut_du_paiement__c) ){
                reg.addError('Merci de contacter votre responsable facturation pour compléter cette action');
            	}
           } 
        }
    }
    
}