/**
 * @File Name          : AP57_CompletionDateOnCli_TEST.cls
 * @Description        : test class
 * @Author             : DMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 05-11-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    3/2/2020   DMO     Initial Version
**/

@isTest
public with sharing class AP57_CompletionDateOnCli_TEST {
    static User mainUser;
    static Account testAcc = new Account();
    static List<MaintenancePlan> lstMP = new List<MaintenancePlan>();
    static List<WorkOrder> lstWrkOrders;
    static List<ContractLineItem> lstContractLineItems;
    static List<Case> lstCases;
    static set<Id> lstContractLineItemsIds;
    static List<ServiceAppointment> lstSAs = new List<ServiceAppointment>();
    //bypass
    static Bypass__c bp = new Bypass__c();

    static{
            mainUser = TestFactory.createAdminUser('AP57@test.COM', TestFactory.getProfileAdminId());
            insert mainUser;

            bp.SetupOwnerId  = mainUser.Id;
            bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
            bp.BypassValidationRules__c = true;
            bp.BypassWorkflows__c = true;
            insert bp;

            System.runAs(mainUser){
            //Insert Account
            testAcc = TestFactory.createAccount('AP57');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.Mode_de_transmission__c = 'EDI';
            testAcc.Mode_de_transmission_privil_gi__c = 'EDI';
            insert testAcc;   

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;   

            //Insert products
            Product2 prod = TestFactory.createProduct('testProd');
            prod.Lot__c=1000;
            insert prod;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(), 
                IsActive = true,
                RecordTypeId= Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId(),
                Compte__c = testAcc.Id
            );
            update standardPricebook;

            //pricebook entry
            PricebookEntry PrcBkEnt  = TestFactory.createPriceBookEntry(standardPricebook.Id, prod.Id, 150);
            insert PrcBkEnt;

            ServiceContract srvcon = TestFactory.createServiceContract('AP57',testAcc.Id);
            srvcon.PriceBook2Id = standardPricebook.Id;
            srvcon.EndDate = Date.Today().addYears(1);
            srvcon.StartDate = Date.Today();
            insert srvcon;

            lstContractLineItems = new List<ContractLineItem>{
                new ContractLineItem(
                    Customer_Absences__c = 2,
                    Customer_Allowed_Absences__c = 4,
                    Completion_Date__c = System.today(),
                    VE_Number_Counter__c = 20,
                    VE_Status__c = 'Not Planned',
                    ServiceContractId = srvcon.Id,
                    PricebookEntryId=PrcBkEnt.Id,
                    Quantity=10,
                    UnitPrice=100),
                new ContractLineItem(
                    Customer_Absences__c = 1,
                    Customer_Allowed_Absences__c = 4,
                    Completion_Date__c = System.today(),
                    VE_Number_Counter__c = 20,
                    VE_Status__c = 'Not Planned',
                    ServiceContractId = srvcon.Id,
                    PricebookEntryId=PrcBkEnt.Id,
                    Quantity=10,
                    UnitPrice=100)
            };
            insert lstContractLineItems;

            //Insert logement
			Logement__c lo = TestFactory.createLogement('Lo_Test', 'test01.sc@mauritius.com', 'test_lname');
            lo.Blue_Order_Id__c = '9711';             
            insert lo;

            //Insert Asset & Case
            Asset asst = TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lo.Id);
            asst.accountId = testAcc.Id;
            asst.Product2Id = prod.Id;
            insert asst;

            lstCases = new List<Case>();
            Case cse1 = TestFactory.createCase(testAcc.Id, 'Troubleshooting', asst.Id);
            //cse1.Reason__c = 'Total Failure';
            cse1.Contract_Line_Item__c = lstContractLineItems[0].Id;
            lstCases.add(cse1);
            Case cse2 = TestFactory.createCase(testAcc.Id, 'Troubleshooting', asst.Id);
            //cse2.Reason__c = 'Total Failure';
            cse2.Contract_Line_Item__c = lstContractLineItems[1].Id;
            lstCases.add(cse2);
            insert lstCases;  

            //Insert MPs
            lstMP = new List<MaintenancePlan>{
                new MaintenancePlan(
                    Frequency = 2,
                    FrequencyType = 'Weeks',
                    MaintenanceWindowStartDays = 1,
                    MaintenanceWindowEndDays = 5,
                    GenerationTimeframe = 5,
                    GenerationTimeframeType = 'Weeks',
                    NextSuggestedMaintenanceDate = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd')),
                    DoesAutoGenerateWorkOrders = true,
                    GenerationHorizon = 5,
                    StartDate =  Date.newInstance(2019, 11, 20),
                    EndDate  = Date.newInstance(2020, 1, 20)
                ),
                new MaintenancePlan(
                    Frequency = 2,
                    FrequencyType = 'Weeks',
                    MaintenanceWindowStartDays = 1,
                    MaintenanceWindowEndDays = 5,
                    GenerationTimeframe = 5,
                    GenerationTimeframeType = 'Weeks',
                    NextSuggestedMaintenanceDate = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd')),
                    DoesAutoGenerateWorkOrders = true,
                    GenerationHorizon = 5,
                    StartDate =  Date.newInstance(2019, 11, 20),
                    EndDate  = Date.newInstance(2020, 1, 20)
                )
            };
            insert lstMP;

            //Insert WorkOrder
            lstWrkOrders = new List<WorkOrder>();
            WorkOrder wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse1.Id;
            wrkOrd.Completion_Date__c = System.today().addDays(1);
            wrkOrd.Customer_Absences__c = 1;
            wrkOrd.SuggestedMaintenanceDate = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            wrkOrd.MaintenancePlanId = lstMP[0].Id;
            wrkOrd.Status ='Done client absent';
            lstWrkOrders.add(wrkOrd);
            WorkOrder wrkOrd1 = TestFactory.createWorkOrder();
            wrkOrd1.caseId = cse2.Id;
            wrkOrd1.Completion_Date__c = System.today().addDays(2);
            wrkOrd1.Customer_Absences__c = 1;
            wrkOrd1.SuggestedMaintenanceDate = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            wrkOrd1.MaintenancePlanId = lstMP[1].Id;
            lstWrkOrders.add(wrkOrd1);
            insert lstWrkOrders;

            //Insert SA
            ServiceAppointment SA1 = TestFactory.createServiceAppointment(lstWrkOrders[0].Id);
            SA1.Status = 'Done client absent';
            SA1.Work_Order__c = lstWrkOrders[0].Id;
            ServiceAppointment SA2 = TestFactory.createServiceAppointment(lstWrkOrders[1].Id);
            SA2.Status = 'Done client absent';
            SA2.Work_Order__c = lstWrkOrders[1].Id;

            lstSAs.add(SA1);
            lstSAs.add(SA2);

            insert lstSAs;          
        }
    }

    @isTest
    public static void testUpdateCustomerAbsence(){
        System.runAs(mainUser){
            Test.startTest();
            List<WorkOrder> lstWrkOrdersNew = [SELECT Id, TECH_CaseContractLineItem__c, Completion_Date__c, Customer_Absences__c, SuggestedMaintenanceDate, CaseId, MaintenancePlanId, Status, LocationId, Acknowledged_On__c FROM WorkOrder WHERE Id IN:lstWrkOrders];
            AP57_CompletionDateOnCli.updateCustomerAbsence(lstWrkOrdersNew, new Set<Id>{lstContractLineItems[0].Id, lstContractLineItems[1].Id});
            Test.stopTest();
        }
    }
    
}