/**
 * @File Name          : LC53_GenerateAvisDePassage_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/13/2020, 10:54:26 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/04/2020   ZJO     Initial Version
**/
@isTest
public with sharing class LC53_GenerateAvisDePassage_TEST {
    static User adminUser;
    static Account testAcc = new Account();
    static sofactoapp__Raison_Sociale__c testSofacto;
    static ServiceTerritory testAgence = new ServiceTerritory();
    static Logement__c testLogement = new Logement__c();
    static Product2 testProd = new Product2();
    static Asset testAsset = new Asset();
    static Case testCase = new Case();
    static WorkOrder testWO = new WorkOrder();
    static ServiceAppointment testServApp = new ServiceAppointment();
    static ContentVersion cv = new ContentVersion();
  

    static{

        adminUser = TestFactory.createAdminUser('LC53_AvisDePassage@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){

            //Create Account
            testAcc = TestFactory.createAccount('testAcc');
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            //Update Account
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //Create Sofacto
            testSofacto = new sofactoapp__Raison_Sociale__c(
                Name = 'test sofacto 1',
                sofactoapp__Credit_prefix__c = '1234',
                sofactoapp__Invoice_prefix__c = '2134'
            );
            insert testSofacto;

            //Create Operating Hour
            OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
            insert OprtHour;

            //Create Agence
            testAgence = new ServiceTerritory(
                Name = 'test agence 1',
                Agency_Code__c = '0001',
                IsActive = True,
                OperatingHoursId = OprtHour.Id,
                Sofactoapp_Raison_Social__c = testSofacto.Id
            );
            insert testAgence;

            //Create Logement
            testLogement = TestFactory.createLogement(testAcc.Id, testAgence.Id);
            insert testLogement;

            //Create Product
            testProd = TestFactory.createProductAsset('testProd');
            insert testProd;

            //Create Asset
            testAsset = TestFactory.createAsset('test Asset', AP_Constant.assetStatusActif, testLogement.Id);
            testAsset.AccountId = testAcc.Id;
            testAsset.Product2Id = testProd.Id;
            insert testAsset;

            //Create Case
            testCase = TestFactory.createCase(testAcc.Id, 'Maintenance',  testAsset.Id);
            testCase.Origin = 'Phone';
            insert testCase;

            //Create WorkOrder
            testWO = TestFactory.createWorkOrder();
            testWO.caseId = testCase.Id;
            insert testWO;

            //Create ServiceAppointment
            testServApp = new ServiceAppointment(
                Status = AP_Constant.servAppStatusNone,
                ParentRecordId = testWO.Id,
                Work_Order__c = testWO.Id,
                ServiceTerritoryId = testAgence.Id,
                Date_Visit_Notice_Generated__c = System.today().addDays(-2)
            );
            insert testServApp;

            //Create Content Version
            cv = new ContentVersion(
                Title = 'testFile.pdf', 
                Description = 'This is a test class file',
                OwnerId = adminUser.Id,
                PathOnClient = 'testFile.pdf',
                VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body')
            );
            insert cv;

        }
    }

    @isTest
    public static void testSaveAvisDePassage(){
        System.runAs(adminUser){

            Test.startTest();
                ContentDocumentLinkTriggerHandler.bypass = true;
                map<string,Object> mapOfResult = LC53_GenerateAvisDePassage.saveAvisDePassage(testServApp.Id);
            Test.stopTest();
        }
    }
}