/**
 * @File Name          : LC01_Acc360Equipements_TEST.cls
 * @Description        : Test class for LC01_Acc360Equipements
 * @Author             : ASO
 * @Group              : Spoon Consulting
 *==============================================================================
 * Ver         Date                     Author                 Modification
 *==============================================================================
 * 1.0        02/08/2019, 14:28:15       ASO                   Initial Version
**/
@isTest
public class LC01_Acc360Requete_TEST {
static User adminUser;
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<ContractLineItem> lstTestContLnItem = new List<ContractLineItem>();
    static List<ServiceContract> lstServiceContracts = new List<ServiceContract>();
    
    static {
        adminUser = TestFactory.createAdminUser('LC01_Acc360Requete@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating logement
            for(integer i =0; i<5; i++){
                Logement__c lgmt = TestFactory.createLogement('logementTest'+i, 'logement@test.com'+i, 'lgmt'+i);
                lgmt.Postal_Code__c = 'lgmtPC'+i;
                lgmt.City__c = 'lgmtCity'+i;
                lgmt.Account__c = testAcc.Id;
                lstTestLogement.add(lgmt);
            }
            insert lstTestLogement;

            //creating products
            lstTestProd.add(TestFactory.createProductAsset('Ramonage gaz'));
            lstTestProd.add(TestFactory.createProduct('Entretien gaz'));
            
            insert lstTestProd;

            // creating Assets
            for(Integer i = 0; i<5; i++){
                Asset eqp = TestFactory.createAccount('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[i].Id);
                eqp.Product2Id = lstTestProd[0].Id;
                eqp.Logement__c = lstTestLogement[i].Id;
                eqp.AccountId = testacc.Id;
                lstTestAssset.add(eqp);                
            }
            insert lstTestAssset;

            //create pricebook 
            PriceBook2 aPriceBook = new PriceBook2(
                Id = Test.getStandardPricebookId(),
                Active_online__c = TRUE,
                IsActive = TRUE,
                Active_from__c = date.today().addMonths(-1),
                Active_to__c = date.today().addMonths(1)
            );
            update aPriceBook;

            //create pricebookentry
            List<PricebookEntry> lstPriceBooks = new List<PricebookEntry>();
            lstPriceBooks.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstTestProd[0].Id, 500 ));
            insert lstPriceBooks;
        
            //create service contract
            lstServiceContracts.add(TestFactory.createServiceContract('test', testAcc.Id));
            lstServiceContracts[0].PriceBook2Id = aPriceBook.Id;
            insert lstServiceContracts;

            //insert contractline
            ContractLineItem ctrt = TestFactory.createContractLineItem( (String)lstServiceContracts[0].Id, (String)lstPriceBooks[0].Id, 5, 10);
            ctrt.VE__C = true;
            // ctrt.Status = 'Active';
            ctrt.StartDate = Date.today().addDays(-1);
            ctrt.EndDate = Date.today().addDays(10);
            ctrt.AssetId = lstTestAssset[0].Id;
            lstTestContLnItem.add(ctrt);
            insert lstTestContLnItem;

            // create case
            case cse = new case(AccountId = testacc.id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id);
            insert cse;
        }
    }
    @isTest
    static void testFetch(){
        Test.startTest();
        LC01_Acc360Requete.getAcountDetails(testAcc.Id);
        LC01_Acc360Requete.goToCase(testAcc.Id);
        Test.stopTest();
    }
}