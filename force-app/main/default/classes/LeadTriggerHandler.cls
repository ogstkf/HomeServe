public with sharing class LeadTriggerHandler {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : On creation/Update of Lead BLUE - callout to SAV
					 On creation of Lead MyChauffage - callout to SAV
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 28-JUIN-2019  DMU    1.0     Initial version
-- 19-JUIN-2020  DMU    1.1     Commented AP04_LeadConversion & AP08_LeadConversion_MyChauffage due to homeserve project
--------------------------------------------------------------------------------------
**************************************************************************************/

	public void handleAfterUpdate(List<Lead> lstOldLead, List<Lead> lstNewLead){
		//DMU 20200619 Commented AP04_LeadConversion & AP08_LeadConversion_MyChauffage due to homeserve project

		// List<Lead> lstLeadToConvertIZI = new List<Lead>();
		// List<Lead> lstLeadToConvertMychauffage = new List<Lead>();

		for(Integer i=0;i<lstNewLead.size();i++){

			// if(lstNewLead[i].To_Convert__c != lstOldLead[i].To_Convert__c 
			// 	&& lstNewLead[i].To_Convert__c 
			// 	&& lstNewLead[i].TECH_LeadRecordTypeDEVName__c=='Pistes_BLUE'){			

			// 	lstLeadToConvertIZI.add(lstNewLead[i]);
			// }
			// if(lstNewLead[i].To_Convert__c != lstOldLead[i].To_Convert__c 
			// 	&& lstNewLead[i].To_Convert__c 
			// 	&& lstNewLead[i].TECH_LeadRecordTypeDEVName__c=='MyChauffage'){
			//    	lstLeadToConvertMychauffage.add(lstNewLead[i]);
			// }
		}
		// System.debug('***lstLeadToConvertIZI ' + lstLeadToConvertIZI.size());
		// System.debug('***lstLeadToConvertMychauffage ' + lstLeadToConvertMychauffage.size());

		// if(!lstLeadToConvertIZI.isEmpty())
		// 	AP04_LeadConversion.convert(lstLeadToConvertIZI);	

		// if(!lstLeadToConvertMychauffage.isEmpty())
		// 	AP08_LeadConversion_MyChauffage.convert(lstLeadToConvertMychauffage);				
	}

	public void handleAfterInsert(List<Lead> lstNewLead){
		//DMU 20200619 Commented AP04_LeadConversion due to homeserve project

		// List<Lead> lstLeadToConvertIZI = new List<Lead>();

		// for(Integer i=0;i<lstNewLead.size();i++){
		// 	if(lstNewLead[i].To_Convert__c && lstNewLead[i].Offer_Code__c == 'SECUG'){
		// 		lstLeadToConvertIZI.add(lstNewLead[i]);
		// 	}
		// }
		// if(!lstLeadToConvertIZI.isEmpty())
		// 	AP04_LeadConversion.convert(lstLeadToConvertIZI);
	}
}