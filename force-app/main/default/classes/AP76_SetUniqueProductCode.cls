/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 11-05-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   11-05-2020   ZJO   Initial Version
**/
public with sharing class AP76_SetUniqueProductCode {

    public static void setProductCode(List<Product2> lstNewProd){

        for(Product2 prod : lstNewProd){
            prod.Unique_product_code__c = prod.ProductCode;
        }
    }
}