public class LC75_NewOrder {
    @AuraEnabled
    public static String getSTforUserconnected() {
        Map<String,Object> resultMap = new Map<String,Object>();
        String id = '';
        List <User> myUser = new List <User> ([SELECT Id, Name, Societe_Agence__c 
                                                FROM User
                                                WHERE Id =: userInfo.getUserId()]);
        List <ServiceTerritory> myServiceTerritory = new List <ServiceTerritory> ([SELECT Id, Name
                                                                                    FROM ServiceTerritory
                                                                                    WHERE Name =: myUser[0].Societe_Agence__c]);
        if(myServiceTerritory.size()>0){
            id = myServiceTerritory[0].Id;        
        }
        return id;
    }
}