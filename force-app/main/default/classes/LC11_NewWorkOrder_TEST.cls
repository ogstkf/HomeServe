/**
 * @File Name          : LC11_NewWorkOrder_TEST.cls
 * @Description        :
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/4/2020, 1:14:09 PM
 * @Modification Log   :
 * Ver       Date            Author                  Modification
 * 1.0    18/11/2019   RRJ     Initial Version
 * 1.1	  17/12/2019   SH	   Added code to take in account the code of the class 
 **/
@isTest
public with sharing class LC11_NewWorkOrder_TEST {

	static User adminUser;
	static Account testAcc;
	static List<Logement__c> lstTestLogement = new List<Logement__c>();
	static List<Asset> lstTestAssset = new List<Asset>();
	static List<Product2> lstTestProd = new List<Product2>();
	static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
	static List<WorkType> lstWrkTyp = new List<WorkType>();
	static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
	static List<ContractLineItem> lstTestContLnItem = new List<ContractLineItem>();
	static List<ServiceContract> lstServiceContracts = new List<ServiceContract>();
	static case cse;

	static {
		adminUser = TestFactory.createAdminUser('LC01_Acc360Equipements_TEST@test.com', TestFactory.getProfileAdminId());
		insert adminUser;

		System.runAs(adminUser) {

			//creating account
			testAcc = TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test');
			testAcc.BillingPostalCode = '1233456';
			testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

			insert testAcc;

			sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

			//creating logement
			for(integer i = 0; i < 5; i ++) {
				Logement__c lgmt = TestFactory.createLogement('logementTest' + i, 'logement@test.com' + i, 'lgmt' + i);
				lgmt.Postal_Code__c = 'lgmtPC' + i;
				lgmt.City__c = 'lgmtCity' + i;
				lgmt.Account__c = testAcc.Id;
				lstTestLogement.add(lgmt);
			}
			insert lstTestLogement;

			//creating products
			lstTestProd.add(TestFactory.createProductAsset('Ramonage gaz'));
			lstTestProd.add(TestFactory.createProductAsset('Entretien gaz'));

			insert lstTestProd;

			// creating Assets
			for(Integer i = 0; i < 5; i ++) {
				Asset eqp = TestFactory.createAccount('equipement' + i, AP_Constant.assetStatusActif, lstTestLogement [i].Id);
				eqp.Product2Id = lstTestProd [0].Id;
				eqp.Logement__c = lstTestLogement [i].Id;
				eqp.AccountId = testacc.Id;
				lstTestAssset.add(eqp);
			}
			insert lstTestAssset;

			//create pricebook
			// PriceBook2 aPriceBook = new PriceBook2(
			//     Id = Test.getStandardPricebookId(),
			//     Active_online__c = TRUE,
			//     IsActive = TRUE,
			//     Active_from__c = date.today().addMonths(-1),
			//     Active_to__c = date.today().addMonths(1)
			// );
			// update aPriceBook;

			//create pricebookentry
			List<PricebookEntry> lstPriceBooks = new List<PricebookEntry>();
			lstPriceBooks.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstTestProd [0].Id, 500));
			insert lstPriceBooks;

			//create service contract
			lstServiceContracts.add(TestFactory.createServiceContract('test', testAcc.Id));
			lstServiceContracts[0].PriceBook2Id = Test.getStandardPricebookId();
			insert lstServiceContracts;

			//insert contractline
			ContractLineItem ctrt = TestFactory.createContractLineItem((String) lstServiceContracts [0].Id, (String) lstPriceBooks [0].Id, 5, 10);
			ctrt.VE__C = true;
			// ctrt.Status = 'Active';
			ctrt.StartDate = Date.today().addDays(-1);
			ctrt.EndDate = Date.today().addDays(10);
			ctrt.AssetId = lstTestAssset [0].Id;
			lstTestContLnItem.add(ctrt);
			insert lstTestContLnItem;

			// create case
			cse = new case(AccountId = testacc.id
						   ,type = 'A1 - Logement'
						   ,AssetId = lstTestAssset [0].Id);
			insert cse;


			WorkType wrkType1 = TestFactory.createWorkType('wrkType1', 'Hours', 1);
			wrkType1.Type__c = AP_Constant.wrkTypeTypeMaintenance;
			WorkType wrkType2 = TestFactory.createWorkType('wrkType2', 'Hours', 2);
			wrkType2.Type__c = AP_Constant.wrkTypeTypeMaintenance;
			WorkType wrkType3 = TestFactory.createWorkType('wrkType3', 'Hours', 3);
			wrkType3.Type__c = AP_Constant.wrkTypeTypeMaintenance;
			
			lstWrkTyp = new list<WorkType>{ wrkType1, wrkType2, wrkType3 };
			insert lstWrkTyp;

			System.debug('lstWrkTyp ' + lstWrkTyp);

			// 2019-12-17 - Added by SH
			// Just to test when no WO is created before (so, it will take info from the case)
			// And then the 3 other inserts will take info from this WO just insered
			Object objet = LC11_NewWorkOrder.fetchDefaultValue(cse.Id);

			for(Integer i = 0; i < 3; i ++) {
				WorkOrder wrkOrd = TestFactory.createWorkOrder();
				wrkOrd.caseId = cse.id;
				wrkOrd.WorkTypeId = lstWrkTyp [0].Id;
				lstWrkOrd.add(wrkOrd);
			}
			insert lstWrkOrd;

			ServiceAppointment serApp1 = TestFactory.createServiceAppointment(lstWrkOrd [0].Id);
			ServiceAppointment serApp2 = TestFactory.createServiceAppointment(lstWrkOrd [1].Id);
			ServiceAppointment serApp3 = TestFactory.createServiceAppointment(lstWrkOrd [2].Id);

			lstServiceApp = new List<ServiceAppointment>{ serApp1, serApp2, serApp3 };
			insert lstServiceApp;
		}
	}

	@IsTest
	static void testFetch() {
		Test.startTest();
		Map<String, String> mapCas = (Map<String, String>) LC11_NewWorkOrder.fetchDefaultValue(cse.Id);
		Test.stopTest();

		System.assertEquals(cse.Id, mapCas.get('CaseId'));
		//System.assertEquals(lstTestAssset [0].Id, mapCas.get('AssetId'));
	}

}