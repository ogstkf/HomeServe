/**
 * @File Name         : AP79_AccordClient_TEST
 * @Description       : 
 * @Author            : Spoon Consutling (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 22-09-2021
 * Modifications Log 
 * =========================================================
 * Ver   Date               Author          Modification
 * 1.0   21-09-2021         MNA             Initial Version
**/
@isTest
public with sharing class AP79_AccordClient_TEST {
    static User adminUser;
    static Bypass__c bp = new Bypass__c();
    
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory agency = new ServiceTerritory();
    static sofactoapp__Raison_Sociale__c raisonSociale = new sofactoapp__Raison_Sociale__c();

    static ServiceContract sc = new ServiceContract();

    static List<Account> testLstAcc = new List<Account>();
    static List<Quote> testLstQuote = new List<Quote>();
    static List<Opportunity> testLstOpp = new List<Opportunity>();

    static {
        adminUser = TestFactory.createAdminUser('AP79', TestFactory.getProfileAdminId());
        insert adminUser;

        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP14,AP16,AP27,AP32,AP53,AP76,WorkOrderTrigger';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(adminUser) {
            

            //create accounts
            for (Integer i = 0; i < 2; i++) {
                testLstAcc.add(TestFactory.createAccount('testAcc'+i));
                testLstAcc[i].PersonOptInSMSCompany__pc = false;
                testLstAcc[i].PersonOptInEMailCompany__pc = false;
                testLstAcc[i].PersonOptInMailHSV__pc = false;
                testLstAcc[i].PersonOptInCallHSV__pc = false;
            }
            insert testLstAcc;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('Operating hours');
            insert opHrs;

            //create raison sociale
            raisonSociale = DataTST.createRaisonSocial();
            insert raisonSociale;

            //create agency
            agency = TestFactory.createServiceTerritory('Agence 1', opHrs.Id, raisonSociale.Id);
            insert agency;

            //create Service Contract
            sc = TestFactory.createServiceContract('test serv con 1', testLstAcc[0].Id);
            sc.Contrat_signe_par_le_client__c = false;
            sc.Contract_Renewed__c = false;
            sc.Payeur_du_contrat__c = testLstAcc[1].Id;
            insert sc;

            testLstOpp = new List<Opportunity>{
                TestFactory.createOpportunity('Test1',testLstAcc[0].Id) ,
                TestFactory.createOpportunity('Test2',testLstAcc[0].Id)
            };
            insert testLstOpp;

            testLstQuote = new List<Quote>{
                new Quote(Name ='Prestations 1',
                            Devis_signe_par_le_client__c = false,
                            Status = AP_Constant.quoteStatusValideSigne,
                            Date_de_debut_des_travaux__c = System.today(),
                            Agency__c = agency.Id,
                            Nom_du_payeur__c = testLstAcc[0].Id,
                            OpportunityId = testLstOpp[0].Id
                ),
                new Quote(Name ='Prestations 2',
                        Devis_signe_par_le_client__c = false,
                        Status = 'In Review',
                        Date_de_debut_des_travaux__c = System.today(),
                        Agency__c = agency.Id,
                        Nom_du_payeur__c = testLstAcc[1].Id,
                        OpportunityId = testLstOpp[1].Id
                )
            };
        }
    }

    @isTest
    static void testUpdtScAcc(){
        System.runAs(adminUser) {
            sc.Contrat_signe_par_le_client__c = true;
            Test.startTest();
            update sc;
            Test.stopTest();
            Account acc = [SELECT Id, PersonOptInSMSCompany__pc, PersonOptInEMailCompany__pc, PersonOptInMailHSV__pc, PersonOptInCallHSV__pc FROM Account WHERE ID = :testLstAcc[1].Id];
            System.assertEquals(true, acc.PersonOptInSMSCompany__pc);
            System.assertEquals(true, acc.PersonOptInEMailCompany__pc);
            System.assertEquals(true, acc.PersonOptInMailHSV__pc);
            System.assertEquals(true, acc.PersonOptInCallHSV__pc);
        }
    }

    @isTest
    static void testUpdtQuoAccInsertHandler() {
        System.runAs(adminUser) {
            Test.startTest();
            insert testLstQuote[0];
            Test.stopTest();
            Account acc = [SELECT Id, PersonOptInSMSCompany__pc, PersonOptInEMailCompany__pc FROM Account WHERE ID = :testLstAcc[0].Id];
            System.assertEquals(true, acc.PersonOptInSMSCompany__pc);
            System.assertEquals(true, acc.PersonOptInEMailCompany__pc);
        }
    }

    @isTest
    static void testUpdtQuoAccUpdateHandler() {
        System.runAs(adminUser) {
            insert testLstQuote[1];
            testLstQuote[1].Status = AP_Constant.quoteStatusValideSigne;
            Test.startTest();
            update testLstQuote[1];
            Test.stopTest();
            Account acc = [SELECT Id, PersonOptInSMSCompany__pc, PersonOptInEMailCompany__pc FROM Account WHERE ID = :testLstAcc[1].Id];
            System.assertEquals(true, acc.PersonOptInSMSCompany__pc);
            System.assertEquals(true, acc.PersonOptInEMailCompany__pc);
        }
    }
}