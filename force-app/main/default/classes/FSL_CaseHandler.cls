public with sharing class FSL_CaseHandler implements ITriggerHandleable{
    
    private Map<Id, Asset> assets;

    public void onBeforeInsert(List<Case> cases){
        setEntitlement(cases);
        entitlementSet(cases);
        updateName(Cases);
    }

    public void onBeforeUpdate(List<Sobject> newList, Map<Id, Sobject> oldMap){
        setEntitlement((List<case>)newList, (Map<Id,Case>)oldMap);
    }

    public void onAfterInsert(List<Case> cases){}
    public void onAfterUpdate(List<Sobject> newList, Map<Id, Sobject> oldMap){}
    public void onBeforeDelete(List<Case> cases){}
    public void onAfterDelete(List<Case> cases){}



    private void setEntitlement(List<Case> cases, Map<Id,Case> oldCases){
        List<Case> changedCases = new List<Case>();
        for(Case c : cases){
            if(TriggerUtility.hasChanged(c, oldCases.get(c.Id), 'Type') || TriggerUtility.hasChanged(c, oldCases.get(c.Id), 'Reason__c') || TriggerUtility.hasChanged(c, oldCases.get(c.Id), 'AssetId')){
                changedCases.add(c);
            }
        }
        if(!changedCases.isEmpty()){
            setEntitlement(changedCases);
        }
    }    

    private void setEntitlement(List<Case> cases){
        if(assets==null)
            assets = findCasesAssets(cases);
        for(Case c : cases){
            if(c.Type=='Troubleshooting'){
                if(c.Reason__c=='Total Failure'){
                    Id totalFailureEntitlementId = caseGetEntitlementValue(c,assets,'Type', new Set<String>{'Total failure'});
                    if(totalFailureEntitlementId != null){
                        c.EntitlementId = totalFailureEntitlementId;
                    }
                }
                else{
                    Id partialFailureEntitlementId = caseGetEntitlementValue(c,assets,'Type', new Set<String>{'Partial failure'});
                    if(partialFailureEntitlementId != null){
                        c.EntitlementId = partialFailureEntitlementId;
                    }
                }
            }
        }
    }

    private Id caseGetEntitlementValue(Case c, Map<Id, Asset> assets, string fieldName, set<String> requiredValues){
        if(c.assetId != null && assets.containsKey(c.assetId))
            return assetGetEntitlementValue(assets.get(c.assetId), fieldName, requiredValues);
        return null;
    }

    private boolean caseHasEntitlementValue(Case c, Map<Id, Asset> assets, string fieldName, set<String> requiredValues){
        if(c.assetId != null && assets.containsKey(c.assetId))
            return null != assetGetEntitlementValue(assets.get(c.assetId), fieldName, requiredValues);
        return false;
    }

    private Id assetGetEntitlementValue(Asset asset, string fieldName, set<String> requiredValues){
        for(Entitlement entitlement : asset.Entitlements){
            if(requiredValues.contains(entitlement.Type))
                return entitlement.Id;
        }
        return null;
    }

    private Map<Id, Asset> findCasesAssets(List<Case> cases){
        Set<Id> assetIds = new Set<Id>();
        for(Case c : cases){
            assetIds.add(c.AssetId);
        }
        return new Map<Id, Asset>([Select Id, (Select Id, type From Entitlements) From Asset Where id in :assetIds]);
    }

    //WHEN ENTITLEMENT IS SET
    private void entitlementSet(list<Case> cases){
        Map<String, BusinessHours> businessHoursMap = new Map<String, BusinessHours>();
        for(BusinessHours bh : [Select Id, Name From BusinessHours]){
            businessHoursMap.put(bh.Name, bh);
        }
        if(assets==null)
            assets = findCasesAssets(cases);
        for(Case c : cases){
            if(caseHasEntitlementValue(c,assets,'Type', new Set<String>{'24/7'}) && businessHoursMap.containsKey('24/7')){
                c.BusinessHoursId = businessHoursMap.get('24/7').Id;
            }
            else if(caseHasEntitlementValue(c,assets,'Type', new Set<String>{'8/7'}) && businessHoursMap.containsKey('8/7')){
                c.BusinessHoursId = businessHoursMap.get('8/7').Id;
            }
        }
    }

    private void entitlementSet(list<Case> cases, Map<Id,Case> oldCases){
        List<Case> toUpdate = new List<Case>();
        for(Case c : cases){
            if(TriggerUtility.hasChanged(c, oldCases.get(c.Id), 'AssetId')){
                toUpdate.add(c);
            }
        }
        if(!toUpdate.isEmpty()){
            entitlementSet(toUpdate);
        }
    }


    public String LabelOfPicklistType (string Test) {

      string type;
      Schema.DescribeFieldResult field = case.Type.getDescribe();
      for (Schema.PicklistEntry f : field.getPicklistValues()){
        if(f.getValue() == test){
           type = f.getLabel();
        }
      }
      if(type == null){
        type ='';
      }
       
    return Type;          

}


    public String LabelOfPicklistReason (string Test) {

      string type;
      Schema.DescribeFieldResult field = case.Reason__c.getDescribe();
      for (Schema.PicklistEntry f : field.getPicklistValues()){
        if(f.getValue() == test){
           type = f.getLabel();
        }
      }
      if(type == null){
        type ='';
      }
       
    return Type;          

}


    public void UpdateName(List<Case> cases){
   
        for(Case c : Cases){
            if(c.Subject == null ){
                if(c.Reason__c != null)
                c.Subject = LabelOfPicklistType(c.Type) +' ' +'-' + ' '+ LabelOfPicklistReason(c.Reason__c);
                else{
                c.Subject = LabelOfPicklistType(c.Type);
                }
            }

        }
    }


    //END WHEN ENTITLEMENT IS SET
}