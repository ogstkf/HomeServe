/**
 * @File Name         : AP79_AccordClient
 * @Description       : 
 * @Author            : Spoon Consutling (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 21-10-2021
 * Modifications Log 
 * ========================================================
 * Ver   Date               Author          Modification
 * 1.0   20-09-2021         MNA             Initial Version
**/
public with sharing class AP79_AccordClient {
    public static void updtScAcc(Set<Id> setAccId) {
        List<Account> lstAccToUpdt = new List<Account>();
        for (Account acc : [SELECT Id, PersonOptInSMSCompany__pc, PersonOptInEMailCompany__pc, PersonOptInMailHSV__pc, PersonOptInCallHSV__pc, PersonOptInMailCompany__pc, PersonOptInCallCompany__pc 
                                                FROM Account WHERE Id IN :setAccId AND RecordType.DeveloperName = 'PersonAccount']) {
            acc.PersonOptInSMSCompany__pc = true;
            acc.PersonOptInEMailCompany__pc = true;
            acc.PersonOptInMailHSV__pc = true;
            acc.PersonOptInCallHSV__pc = true;
            acc.PersonOptInMailCompany__pc = true;
            acc.PersonOptInCallCompany__pc = true;
            lstAccToUpdt.add(acc);
        }
        if (lstAccToUpdt.size() > 0) 
            update lstAccToUpdt;
    }
    
    public static void updtQuoAcc(Set<Id> setAccId) {
        List<Account> lstAccToUpdt = new List<Account>();
        for (Account acc : [SELECT Id, PersonOptInSMSCompany__pc, PersonOptInEMailCompany__pc, PersonOptInMailCompany__pc, PersonOptInCallCompany__pc FROM Account WHERE Id IN :setAccId AND RecordType.DeveloperName = 'PersonAccount']) {
            acc.PersonOptInSMSCompany__pc = true;
            acc.PersonOptInEMailCompany__pc = true;
            acc.PersonOptInMailCompany__pc = true;
            acc.PersonOptInCallCompany__pc = true;
            lstAccToUpdt.add(acc);
        }
        if (lstAccToUpdt.size() > 0) 
            update lstAccToUpdt;
    }
}