/**
 * @File Name          : AP46_QuoteManager_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class AP46_QuoteManager_TEST {
 /**
 * @File Name          : AP42_CreateProductTransfer.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         14-11-2019     		LGO         Initial Version
**/

    static User mainUser;
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
	static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
	static PricebookEntry PrcBkEnt = new PricebookEntry();
    static  Bypass__c userBypass;
    static List<Order> lstOrder = new List<Order>();
    static List<ProductRequestLineItem > lstPRLI = new List<ProductRequestLineItem >();
    static List<ProductRequest> lstProdRequest = new List<ProductRequest>();
    static Quote quo = new Quote();
    static opportunity opp = new opportunity();
    static Account FournisseurAcc;


    static{


        

        mainUser = TestFactory.createAdminUser('AP46@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){

             //create account
            testAcc = TestFactory.createAccount('AP46QuoteManager');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            FournisseurAcc= new Account(Name='ChamFournissuer');
            FournisseurAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            insert FournisseurAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;

            update testAcc;

            //create products
            prod = TestFactory.createProduct('Product X');
            prod.ProductCode = 'Pro-X';
            prod.Equipment_family__c = 'Chaudière';
            prod.Equipment_type__c = 'Chaudière gaz';
            insert prod;
            

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2( Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;
            


            //Create Orders
            lstOrder = new List<Order> {
                new Order( AccountId = FournisseurAcc.Id
                          ,Name = 'Test1'
                          ,EffectiveDate = System.today()
                          ,Status = 'Demande d\'achats' //To add to AP_Constants
                          ,Pricebook2Id = lstPrcBk[0].Id
                          ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId()),
                new Order( AccountId = FournisseurAcc.Id
                          ,Name = 'Test2'
                          ,EffectiveDate = System.today()
                          ,Status = 'Demande d\'achats' //To add to AP_Constants
                          ,Pricebook2Id = lstPrcBk[0].Id
                          ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId()),
                new Order( AccountId = FournisseurAcc.Id
                          ,Name = 'Test3'
                          ,EffectiveDate = System.today()
                          ,Status = 'Demande d\'achats' //To add to AP_Constants
                          ,Pricebook2Id = lstPrcBk[0].Id
                          ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId())
            };     
            insert lstOrder;

            lstProdRequest = new List<ProductRequest> {
                new ProductRequest(),
                new ProductRequest(),
                new ProductRequest()
            };  
            insert lstProdRequest;    
        
            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);
            insert opp;

            //create Quote
            quo = new Quote(Name ='Test1',
                            Devis_signe_par_le_client__c = false,
                            OpportunityId = opp.Id);
            insert quo;    

            lstPRLI = new List<ProductRequestLineItem > {
                //Order0 0-2
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[0].Id
                                            ,Quote__c = quo.Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Transféré'
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[0].Id
                                            ,Quote__c = quo.Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[0].Id
                                            ,Quote__c = quo.Id),
                //Order1 3-5
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[1].Id
                                            ,Quote__c = quo.Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[1].Id
                                            ,Quote__c = quo.Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[1].Id
                                            ,Quote__c = quo.Id), 
                //Order2 6-8
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'En cours de commande'
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[2].Id
                                            ,Quote__c = quo.Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Transféré'
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[2].Id
                                            ,Quote__c = quo.Id),
                new ProductRequestLineItem( Product2Id = prod.Id
                                            ,QuantityRequested = 1
                                            ,ParentId = lstProdRequest[0].Id
                                            //,Status = 'Réservé à préparer'
                                            ,Status = 'Brouillon'
                                            ,Commande__c = lstOrder[2].Id
                                            ,Quote__c = quo.Id)                                                                                    
            };                   
            insert lstPRLI;

        }
    }

    @isTest
    public static void testVerifyAPPtrue(){
        System.runAs(mainUser){
            // prItem.APP_Checked__c = false;
            // System.debug('##### TEST prItem: '+prItem);

            // lstPRLI[0].Status = 'Réservé préparé'; //DMU 20200914 - Changed picklist value on test class due to homeserve not having 'Réservé préparé'
            // lstPRLI[0].Status = 'Brouillon';
            Test.startTest();
                //  update lstPRLI[0];
                AP46_QuoteManager.updateQuote(new set<Id>{quo.Id});
            Test.stopTest();


            list<Quote> lstQuote = [SELECT Id,
                                    A_planifier__c
                                    FROM Quote
                                    WHERE Id =: quo.Id] ;
            

            // System.assertEquals(lstQuote[0].A_planifier__c, true);
        }
    }
  
}