/**
 * @File Name          : AP46_QuoteManager.CLS
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 21-01-2022
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0      15/11/2019       LGO                    Initial Version (CT-1191)
**/
public with sharing class AP46_QuoteManager {

    public static void updateQuote(Set<Id> setQuoteIds){

        list<Quote> lstupdQuote = new list<Quote>();   
        //Boolean isPlan = false;     
        Map<Id, Boolean> mapQuoteIsPlan = new Map<Id, Boolean>();
/*
        for (Quote q: [SELECT Id , A_planifier__c,
                            (SELECT Id ,Status
                            FROM Product_Request_Line_Items__r) 
                        FROM Quote 
                        WHERE Id IN: setQuoteIds
                        AND RecordType.DeveloperName = 'Devis_standard']){

            if(!q.Product_Request_Line_Items__r.isEmpty()){

                for(ProductRequestLineItem pr : q.Product_Request_Line_Items__r){

                    if(!(pr.Status == AP_Constant.PRLIStatutCommande || pr.Status == AP_Constant.PRLIStatutEnCourCommande || pr.Status == null )){
                        isPlan = true;
                        
                    }
                    else {
                        isPlan = false;
                        break;
                    }
                }
                if(isPlan)                lstupdQuote.add(New Quote(Id = q.Id, A_planifier__c = true));
                if(!isPlan && q.A_planifier__c == true)                lstupdQuote.add(New Quote(Id = q.Id, A_planifier__c = false));

            }
        }*/

        for (ProductRequestLineItem pr: [SELECT Id ,Status, Quote__c FROM ProductRequestLineItem WHERE Quote__c IN :setQuoteIds]) {
            if(!(pr.Status == AP_Constant.PRLIStatutCommande || pr.Status == AP_Constant.PRLIStatutEnCourCommande || pr.Status == null )) {
                if(!mapQuoteIsPlan.containsKey(pr.Quote__c) || (mapQuoteIsPlan.containsKey(pr.Quote__c) && !mapQuoteIsPlan.get(pr.Quote__c))) 
                    mapQuoteIsPlan.put(pr.Quote__c, true);
            }
            else {
                mapQuoteIsPlan.put(pr.Quote__c, false);
            }
        }

        for(Quote q: [SELECT Id , A_planifier__c FROM Quote WHERE Id IN :mapQuoteIsPlan.keySet()]) {
            if (mapQuoteIsPlan.get(q.Id))
                q.A_planifier__c = true;
            else 
                q.A_planifier__c = false;
        }

        system.debug('### lstupdQuote '+lstupdQuote);
        if(lstupdQuote.size() >0 ){
            update lstupdQuote;
        }
    }

}