/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 05-11-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   15-09-2021   MNA   Initial Version
**/
global without sharing class WS13_ChaineEditiquev3 {
    public class Address {
        public String streetName1 = '';
        public String streetName2 = '';
        public String zipcode = '';
        public String city = '';

        public Address(ServiceTerritory st) {
            if (st != null) {
                this.streetName1 = value(st.Corporate_Street__c);
                this.streetName2 = value(st.Corporate_Street2__c);
                this.zipcode = value(st.Corporate_ZipCode__c);
                this.city = value(st.Corporate_City__c);
            }
        }

    }

    public class AddressInvoice {        
        public String streetName1 = '';
        public String streetName2 = '';
        public String zipcode = '';
        public String city = '';
        public String building = '';
        public String floor = '';
        public String stairway = '';
        public String door = '';

        public AddressInvoice (Account acc) {
            if (acc != null) {
                this.streetName1 = value(acc.BillingStreet);
                this.streetName2 = value(acc.Adress_complement__c);
                this.building = value(acc.Imm_Res__c);
                this.floor = value(acc.Floor__c);
                this.stairway = value(acc.Stair__c);
                this.door = value(acc.Door__c);
                this.zipcode = value(acc.BillingPostalCode);
                this.city = value(acc.BillingCity);
            }
        }
    }

    public class AddressDwelling {
        public String displayName = '';
        public String firstName = '';
        public String lastName = '';
        public String salutation = '';
        public String streetNumber = '';
        public String streetName1 = '';
        public String streetName2 = '';
        public String building = '';
        public String floor = '';
        public String stairway = '';
        public String door = '';
        public String zipcode = '';
        public String city = '';

        public AddressDwelling (Logement__c logement) {
            if (logement != null) {
                if (logement.Inhabitant__c != null) {
                    this.displayName = value(logement.Inhabitant__r.Name);
                    this.firstName = value(logement.Inhabitant__r.FirstName);
                    this.lastName = value(logement.Inhabitant__r.LastName);
                    this.salutation = label(logement.Inhabitant__r.Salutation, 'Account', 'Salutation');
                }
                this.streetName1 = value(logement.Street__c);
                this.streetName2 = value(logement.Adress_complement__c);
                this.building = value(logement.Imm_Res__c);
                this.floor = value(logement.Floor__c);
                this.stairway = value(logement.Stair__c);
                this.door = value(logement.Door__c);
                this.zipcode = value(logement.Postal_code__c);
                this.city = value(logement.City__c);
            }
        }
    }

    public class Advance_payments {
        public Decimal amount;

        public String date_abc;
        public String method;
    }

    public class Aide {
        public Decimal amount;

        public String label;
        public String designation_complementaire;

        public Aide (QuoteLineItem qliAIDE, Decimal amount) {
            if (qliAIDE == null) {
                this.amount = amount;
                this.label = '';
                this.designation_complementaire = '';
            }
            else {
                this.amount = amount;
                if (qliAIDE.Product2Id == null) {
                    this.label = '';
                    this.designation_complementaire = '';
                }
                else {
                    this.label = value(qliAIDE.Product2.Name);
                    this.designation_complementaire = value(qliAIDE.Product2.Designation_complementaire__c);
                }
            }
        }
    }

    public class Anomaly {
        public String level1;
        public String level2;
        public String date_abc;
    }

    public class Anomaly_raised {
        public String level1;
        public String level2;
        public String date_abc;
        public String comment;
    }

    public class Body {
        public Root root;
    }

    public class Broadcast {
        public Boolean central_print;
        public Boolean local_print;
        public Boolean arch;
        public Boolean email;
        public Boolean sms;
        public Boolean watermark;
    }

    public class Company {
        public String code = '';
        public String contactEmail = '';
        public String website = '';
        public String legalMentions = '';
        public String IBAN = '';
        public String openingHours = '';
        public String onduty = '';

        public Address address;
        public PhoneNumbers_company phoneNumbers;

        public Company (ServiceTerritory st, String MentionsLegales, String IBAN){
            if (st != null) {
                this.code = value(st.Agency_Code__c);
                this.contactEmail = value(st.Email__c);
                this.website = value(st.site_web__c);
                this.legalMentions = value(MentionsLegales);
                this.IBAN = value(IBAN);
                this.openingHours = value(st.Libelle_horaires__c);
                this.onduty = value(st.horaires_astreinte__c);
            }

            this.address = new address(st);
            this.phoneNumbers = new PhoneNumbers_company(st);
        }
    }

    public class Content {
        public String type;
        public String emissionDate;
        public String creationDate;
        public String documentId;

        public Devis devis;
        public Facture facture;
        public Contrat contrat;
        public Report report;
        public Intervention_notice intervention_notice;
        public Renewal renewal;
        public Subrogation subrogation;
        public Facture_acompte acompte;
        public Formalnotice formalnotice;
        public Credit_note credit_note;
    }

    public class Contrat {
        public String description;
        public String product_family;
        public String product_type;
        public String start_date;
        public String end_date;
        public String occupancy_status;
        public String first_year_contract_description;
        public String signature_date;
        public String customer_signature;
        public String technician_signature;
        public String technician_id;
        public String conditions_version;
        public String contract_id;

        public Optins_optouts optins_optouts;
        public Occupant occupant;

        public List<Element_asset> elements;
    }

    public class Contract_bundle {
        public Decimal price_without_tax;
        public Decimal price_without_tax_after_rebate;
        public Decimal subtotal;
        public Decimal vat_rate;
        public Decimal vat_amount;
        public Decimal price_with_tax;

        public String name;

        public Recap recap;
        public List<Included_coverage> included_coverage;
        public List<Option> options;
    }

    public class Credit_note {
        public Boolean is_anonymized;

        public String title;
        public String equipment_type;
        public String description;
        public String credit_note_reason;
        public String credit_note_comment;
        public String credit_note_id;
        public String quote_id;
        public String invoice_id;
        public String contract_id;
        public String work_starting_date;

        public Summary summary;
        
        public List<Element> elements;
    }

    public class Details_ref {
        public Integer quantity;

        public String comment;
        public String description;
        public String codeArticle;
        public String details;
        public String unit;
        public String unit_price_amount;
        public String discount_percentage;
        public String discount_amount;
        public String total_wo_vat;
        public String vat_rate;
        public String total_w_vat;
        public String ecocontribution;

        public Details_ref(QuoteLineItem qli) {

        }

        public Details_ref(sofactoapp__Ligne_de_Facture__c lneFac) {
            
        }
    }

    public class Details_ref_acompte {
        public String description;
        public String total;
    }

    public class Devis {
        public Boolean is_anonymized;
        public Boolean keep_old_material;
        public Boolean account_pro;

        public Date validity_date;
        public Date customer_signature_date;
        public Date work_date;

        public Decimal advance_payment_amount;
        public Decimal advance_payment_a_livraison;
        public Decimal advance_1;
        public Decimal advance_2;

        public String title;
        public String equipment_type;
        public String description;
        public String contract_proposal;
        public String Propcontrat_comment;
        public String quote_id;
        public String customer_signature;
        public String aide_presta_cee;
        public String aide_presta_mpr;
        public String aide_presta_cdp;

        public List<Element> elements;
        public List<Element_asset> child_asset;
        public Summary summary;

        public Devis (Quote quo, Account acc, List<Asset> lstAsset, Boolean isAnonymise) {
            Decimal tva5 = 0;
            Decimal tva10 = 0;
            Decimal tva20 = 0;
            QuoteLineItem qtiAIDECDP;
            QuoteLineItem qtiAIDECEE;
            QuoteLineItem qtiAIDEMPR;
            QuoteLineItem qtiPropContrat;
            this.elements = new List<Element>();
            this.child_asset = new List<Element_asset>();

            this.title = value(quo.Name);
            this.equipment_type = (quo.Equipement__c == null || quo.Equipement__r.Status != 'Actif' ? '' : label(quo.EquipmentType__c, 'Product2', 'Equipment_type1__c'));
            this.description = value(quo.Description);
            this.is_anonymized = isAnonymise;
            this.validity_date = quo.ExpirationDate;
            this.keep_old_material = quo.Le_client_souhaite_conserver_les_pieces__c;
            this.advance_payment_amount = (!quo.Acompte_demande__c ? quo.Montant_de_l_acompte_demande__c : 0);
            this.advance_payment_a_livraison = (quo.Deuxieme_acompte__c ? quo.Montant_du_deuxieme_acompte__c : null);
            this.quote_id = value(quo.QuoteNumber);
            this.customer_signature = signature(quo.Signature_client__c);
            this.customer_signature_date = quo.Date_de_signature_du_client__c;
            this.work_date = quo.Date_de_debut_des_travaux__c;
            this.advance_1 = quo.Pourcentage_de_l_acompte__c;
            this.advance_2 = quo.Pourcentage_du_deuxieme_acompte__c;
            this.aide_presta_cee = (qtiAIDECEE == null || qtiAIDECEE.Product2Id == null ? '' : getPresta(qtiAIDECEE.Product2.ProductCode));
            this.aide_presta_mpr = (qtiAIDEMPR == null || qtiAIDEMPR.Product2Id == null ? '' : getPresta(qtiAIDEMPR.Product2.ProductCode));
            this.aide_presta_cdp = (qtiAIDECDP == null || qtiAIDECDP.Product2Id == null ? '' : getPresta(qtiAIDECDP.Product2.ProductCode));
            this.account_pro = (acc != null && acc.RecordTypeId != null && !(acc.RecordType.DeveloperName == 'PersonAccount'));

            if (qtiPropContrat == null) {
                this.contract_proposal = '';
                this.Propcontrat_comment = '';
            }      
            else {
                this.contract_proposal = value(qtiPropContrat.Product2.Designation_complementaire__c);
                this.Propcontrat_comment = value(qtiPropContrat.Description__c);
            }
            
            
            for (QuoteLineItem qli : quo.QuoteLineItems) {
                if (qli.Product2Id != null && qli.Product2.ProductCode != null
                    && (qli.TECH_Aide_CoupDePouce__c || qli.TECH_Aide_CEE__c || qli.TECH_Aide_MaPrimeRenov__c) || qli.Product2.ProductCode == 'PROPCONTRAT') {
                        if (qli.TECH_Aide_CoupDePouce__c)
                            qtiAIDECDP = qli;
                        else if (qli.TECH_Aide_CEE__c)
                            qtiAIDECEE = qli;
                        else if (qli.TECH_Aide_MaPrimeRenov__c)
                            qtiAIDEMPR = qli;
                        else if (qli.Product2.ProductCode == 'PROPCONTRAT' ) 
                            qtiPropContrat = qli;
                }
                else {
                    this.elements.add(new Element(qli));
                    if (qli.Taux_de_TVA__c == 5.5)
                        tva5 += (qli.Prix_de_vente_apres_remise__c * 0.055).divide(1, 2, System.RoundingMode.HALF_UP);
                    else if(qli.Taux_de_TVA__c == 10)
                        tva10 += (qli.Prix_de_vente_apres_remise__c * 0.1).divide(1, 2, System.RoundingMode.HALF_UP);
                    else if(qli.Taux_de_TVA__c == 20)
                        tva20 += (qli.Prix_de_vente_apres_remise__c * 0.2).divide(1, 2, System.RoundingMode.HALF_UP);
                }
            }

            this.summary = new Summary(quo, qtiAIDECDP, qtiAIDECEE, qtiAIDEMPR, tva5, tva10, tva20);

            for (Asset ast : lstAsset) {
                this.child_asset.add(new Element_asset(ast));
            }
        }
    }

    public class Element {
        public String reference;

        public Details_ref details_ref;

        public Element (QuoteLineItem qli) {
            this.reference = value(qli.Product2.ProductCode);
            this.details_ref = new Details_ref(qli);
        }

        public Element (sofactoapp__Ligne_de_Facture__c lneFac) {
            this.reference = value(lneFac.sofactoapp__Code_produit__c);
            this.details_ref = new Details_ref(lneFac);
        }
    }

    public class Element_acompte {
        public Details_ref_acompte details_ref;
    }

    public class Element_Appointment {
        public String brand;
        public String reference;
        public String serial_number;
    }

    public class Element_asset {
        public String brand;
        public String ref;
        public String serial_nbr;
        public String waranty_manufacturer_end_date;
        public String commisionning_date;
        public Element_asset (Asset ast) {
            if (ast == null) {
                this.brand = '';
                this.ref = '';
                this.serial_nbr = '';
                this.waranty_manufacturer_end_date = '';
                this.commisionning_date = '';
            }
            else {
                this.brand = value(ast.Brand_auto__c);
                this.ref = (ast.Product2Id == null ? '' : value(ast.Product2.Name));
                this.serial_nbr = value(ast.SerialNumber);
                this.waranty_manufacturer_end_date = value(ast.Manufacturer_s_end_of_warranty_auto__c);
                this.commisionning_date = value(ast.Date_de_mise_en_service__c);
            }
        }
    }

    public class Element_subrogation {
        public Decimal montant_prime;

        public String num_dossier;
        public String num_facture;
        public String num_client;
        public String nom_client;
    }
    
    public class Facture {
        public Boolean is_anonymized;
        public Boolean is_draft;

        public Decimal balance_amount;

        public String title;
        public String equipment_type;
        public String description;
        public String quote_id;
        public String invoice_id;
        public String contract_id;
        public String work_starting_date;
        public String comment;
        public String contract_proposal;
        public String invoice_totally_paid_at;

        public Summary summary;
        public GenericPayment solde_restant;
        
        public List<Element> elements;
        public List<Payment> payments;
        public List<Advance_payments> advance_payments;
        public List<GenericPayment> credit_notes;

        public Facture(Quote quo, sofactoapp__Factures_Client__c fac, List<sofactoapp__Ligne_de_Facture__c> lstLneFac, List<sofactoapp__R_glement__c> lstPay, ServiceContract sc, Boolean isAnonymise, List<sofactoapp__Factures_Client__c> lstAvoir) {

        }
    }

    public class Facture_acompte {
        public Boolean is_fully_paid;

        public String title;
        public String equipment_type;
        public String quote_id;
        public String invoice_id;

        public List<Element_acompte> elements;
        public Summary_Acompte summary;
    }

    public class Fee {
        public Decimal amount_wo_vat;
        public Decimal amount_vat;

        public String label;
    }

    public class Formalnotice {
        public String contract_name;
    }

    public class GenericPayment {
        public Decimal amount;

        public String date_abc;
    }

    public class Included_coverage {
        public Integer quantity;

        public String label;
    }

    public class Installation {
        public String equipment_type;
        public List<Element_Appointment> elements;
    }

    public class Intervention {
        public String id;
        public String contract_id;
        public String quote_id;
        public String date_abc;
        public String type;
        public String comment;
        public String diagnostic;
        public String service_appointment_id;

        public List<String> checklist;
        public List<String> checklist_secondary;

        public List<Anomaly_raised> anomaly_raised;
        public List<Anomaly> anomaly_cleared;
    }

    public class Intervention_notice {
        public String date_planned;
        public String slot;
        public String phone_number;
    }

    public class JsonEditique {
        public Boolean isPreview;

        public String templateId;
        public String callbackUrl;

        public Body body;
    }

    public class Meta {
        public Company company;
        public Company agency;
        public Recipient recipient;

        public Meta(Account acc, String IBAN, Logement__c logement, String MentionsLegales, Account recipient, ServiceTerritory st) {
            this.company = new Company(st, MentionsLegales, IBAN);
            this.agency = new Company(st, MentionsLegales, IBAN);
            this.recipient = new Recipient(acc, recipient, logement);
        }
    }

    public class Occupant {
        public String type;
        public String displayName;
        public String salutation;

        public Address address;
        public PhoneNumbers phoneNumbers;
    }

    public class Optins_optouts {
        public String email_sms_hsv;
        public String email_sms_company;
        public String mail_phone_company;
    }

    public class Option {
        public Integer quantity;
        
        public Decimal price_without_tax;
        public Decimal price_without_tax_after_rebate;
        public Decimal subtotal;

        public String name;
    }
    
    public class Recap {
        public Integer scheduled_interventions;

        public Decimal total_without_tax;
        public Decimal rebate;
        public Decimal total_without_tax_after_rebate_amount;
        public Decimal vat_amount;
        public Decimal total_with_tax;
    }

    public class Renewal {
        public Boolean is_advantage_contract;

        public Decimal price_with_tax;
        public Decimal price_without_tax;
        public Decimal vat_rate;

        public String type;
        public String start_date;
        public String end_date;
        public String contract_name;
        public String phone_number;
        public String equipment_type;
    }

    public class Report {
        public String technician_name;
        public String technician_signature;
        public String customer_signature;
        public String customer_not_present;
        public String customer_signature_refusal;

        public Installation installation;
        public Intervention intervention;
    }

    public class Representative{
        public String type;
        public String displayName;
        public String salutation;
        public String email;

        public Address address;
        public PhoneNumbers phoneNumbers;
    }

    public class Result {
        public String label;
        public String value;
    }

    public class Root {
        public Meta meta;
        public Broadcast broadcast;
        public Content content;
    }

    public class Payment {
        public Decimal amount;

        public String date_abc;
        public String payment_method;
    }
    
    public class PhoneNumbers {

    }

    public class PhoneNumbers_company {
        public String commercial = '';
        public String emergency = '';
        public PhoneNumbers_company (ServiceTerritory st) {
            if (st != null)
                this.commercial = value(st.Phone__c);
        }
    }

    public class PhoneNumbers_recipient {
        public String landline;
        public String mobile;

        public PhoneNumbers_recipient(Account acc) {
            if (acc == null) {
                this.landline = '';
                this.mobile = '';
            }
            else {
                if (acc.RecordType.DeveloperName == 'PersonAccount') {
                    this.landline = value(acc.PersonHomePhone);
                    this.mobile = value(acc.PersonMobilePhone);
                }
                else {
                    this.landline =  value(acc.Phone);
                    this.mobile = value(acc.PersonMobilePhone);
                }
            }
        }
    }
    
    public class Recipient {
        public String type = '';
        public String displayName = '';
        public String firstName = '';
        public String lastName = '';
        public String salutation = '';
        public String email = '';
        public String customerId = '';

        public AddressInvoice addressInvoice;
        public AddressDwelling addressDwelling;
        public PhoneNumbers_recipient phoneNumbers;

        public Recipient (Account acc, Account recipient, Logement__c logement) {
            if (recipient != null) {
                this.type = value(recipient.Type);
                this.displayName = value(recipient.Name);
                this.firstName = value(recipient.FirstName);
                this.lastName = value(recipient.LastName);
                this.salutation = label(recipient.Salutation, 'Account', 'Salutation');
            }
            
            if (acc != null) {
                this.email = value(acc.PersonEmail);
                this.customerId = value(acc.ClientNumber__c);
            }

            this.addressInvoice = new AddressInvoice(recipient);
            this.addressDwelling = new AddressDwelling(logement);

            this.phoneNumbers = new PhoneNumbers_recipient(acc);
        }
    }

    public class Subrogation {
        public Boolean is_draft;

        public String type;
        public String description;

        public List<Element_subrogation> elements;
        public List<Fee> fees;
        public List<Summary_subrogation> summary;
    }

    public class Summary {
        public Boolean reversal_vat;

        public Decimal total_ttc_before_prime;
        public Decimal total_ht;
        public Decimal total_ttc;

        public String prime_description;

        public List<Tva> tva;
        public List<Aide> aides;

        public Summary (Quote quo, QuoteLineItem qtiAIDECDP, QuoteLineItem qtiAIDECEE, QuoteLineItem qtiAIDEMPR, Decimal tva5, Decimal tva10, Decimal tva20) {
            this.total_ttc_before_prime = quo.Montant_total_TTC_avant_aides__c;
            this.total_ht = quo.Prix_HT_apres_remise__c;
            this.total_ttc = quo.Prix_TTC__c;

            this.prime_description = ((qtiAIDECEE == null || qtiAIDECEE.Product2Id == null) ? '' : value(qtiAIDECEE.Product2.Designation_complementaire__c));

            this.tva = new List<Tva>{
                new Tva(round(tva5), 'TVA (5,5%)'),
                new Tva(round(tva10), 'TVA (10%)'),
                new Tva(round(tva20), 'TVA (20%)')
            };
            
            this.aides = new List<Aide>{
                new Aide(qtiAIDEMPR, quo.MaPrimeRenov__c),
                new Aide(qtiAIDECEE, quo.Aide_CEE__c),
                new Aide(qtiAIDECDP, quo.Coup_de_Pouce__c)
            };
        }
    }

    public class Summary_Acompte {
        public Decimal total_ttc;
    }

    public class Summary_subrogation {
        public Decimal total_ht;
        public Decimal total_ttc;

        public List<Tva> tva;
    }

    public class Tva {
        public Decimal amount; 
        public String label;
        
        public Tva (Decimal amount, String label) {
            this.amount = amount;
            this.label = label;
        }
    }

    
    public static String getPresta(String ProductCode) {
        if (ProductCode == null)
            return '';

        String Presta = '';
        List<String> lstStrSplit = new List<String>();
        if (ProductCode != null) {
            lstStrSplit = ProductCode.split('-');
            for (Integer i = 2; i < lstStrSplit.size(); i++) {
                if (i > 2)
                    Presta += '-';
                Presta += lstStrSplit[i];
            }
        }
        
        if(Presta == '')
            return '';

        Presta = Presta.substring(0, 1).toUpperCase() + Presta.substring(1).toLowerCase();
        return Presta;
    }

    public static String label(String value, String ObjAPI, String FieldAPI) {
        Schema.DescribeFieldResult res = Schema.getGlobalDescribe().get(ObjAPI).getDescribe().fields.getMap().get(FieldAPI).getDescribe();
        List<Schema.PicklistEntry> ple = res.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            if (value == f.getValue()) {
                return f.getLabel();
            }
        }
        return '';
    }
    
    public static Decimal round(Decimal o) {
        if (o == null)
            return 0;
        return o.divide(1, 2, System.RoundingMode.HALF_UP);
    }

    public static String signature(String s) {
        if (s == null)
            return '';
        if (s.startsWith('data:image/jpeg;base64,')) 
            return s.split('data:image/jpeg;base64,')[1];
        else 
            return s;
    }

    public static String value(Decimal o) {
        if (o == null)
            return '0';
        return String.valueOf(o);
    }
    public static String value(Integer o){
        if (o == null)
            return '0';
        return String.valueOf(o);
    }
    public static String value(Long o){
        if (o == null)
            return '0';
        return String.valueOf(o);
    }
    public static String value(Double o){
        if (o == null)
            return '0';
        return String.valueOf(o);
    }
    public static String value(Date o){
        if (o == null)
            return '';
        return Datetime.newInstance(o.year(), o.month(), o.day()).format('yyyy-MM-dd');
    }
    public static String value(Datetime o){
        if (o == null)
            return '';
        return o.format('yyyy-MM-dd');
    }
    public static String value(String str) {
        if (str == null)
            return '';
        String res = '';
        List<String> lstStr = str.split('\n');
        for  (Integer i = 0 ; i < lstStr.size(); i++ ) {
            if (i == lstStr.size() - 1)
                res += lstStr[i];
            else {
                if (lstStr[i].length() > 0 && lstStr[i].charAt(lstStr[i].length() -1) == 13)
                    res += lstStr[i].substring(0, lstStr[i].length() -1) + '\\n';
                else
                    res += lstStr[i].substring(0, lstStr[i].length()) + '\\n';
            }
        }
        lstStr = res.split('"');
        res = '';
        for  (Integer i = 0 ; i < lstStr.size(); i++ ) {
            if (i == lstStr.size() - 1)
                res += lstStr[i];
            else {
                res += lstStr[i] + '\\"';
            }
        }
        lstStr = res.split('\t');
        res = '';
        for  (Integer i = 0 ; i < lstStr.size(); i++ ) {
            if (i == lstStr.size() - 1)
                res += lstStr[i];
            else {
                res += lstStr[i] + '\\t';
            }
        }
        return res;
    }
    public static String value(Object o) {
        if (o == null)
            return '';
        return String.valueOf(o);
    }
}