/**
 * @File Name          : TestFactory.cls
 * @Description        : Test Factory class
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-08-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0      24/01/2019, 16:33:01        DMU/MGR                  Initial Version
 * 1.1      12/12/2019, 11:05:01        LGO                      add custom setting SyncingQLI__c
 * 1.2      27/12/2019, 16:33:01        SH                       Added Record Type on "createLogement"
**/
@isTest
public with sharing class TestFactory {
        
    //Randomize a string
    public static String randomizeString(String name) {
        String charsForRandom = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < 6) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), charsForRandom.length());
            randStr += charsForRandom.substring(idx, idx + 1);
        }
        return name + randStr;
    }

    //get for admin Profile Name / ID
    public static id getProfileAdminId() {
        return ([Select Id From Profile
                Where name = 'Administrateur système'
                OR name = 'System Administrator'
                OR name = 'Amministratore del sistema'
                OR name = 'Systemadministrator'
                OR name = 'Systemadministratör'
                OR name = 'Administrador do sistema'
                OR name = 'Systeembeheerder'
                OR name = 'Systemadministrator'
        ].Id);
    }

    //get community user profile name
    public static id getProfileCommunityId() {
        return ([Select Id From Profile Where name = 'Standard User'].Id);
    }

    //get user role
    public static id getUserRoleId(){
        return ([Select Id From UserRole Where DeveloperName = 'HES_Groupe' LIMIT 1].Id);
    }

    //get for Profil basique
    public static id getProfileBasicId() {
        return ([Select Id From Profile
                Where name = 'Profil basique'].Id);
    }

    //create admin user
    public static User createAdminUser(String name, Id profId) {
        return new User(
             Username = TestFactory.randomizeString(Name) + '@test.com'
            ,LastName = 'test'
            ,FirstName = 'test' + TestFactory.randomizeString('')
            ,Email = 'test@test.com'
            ,Alias = 'test' 
            ,LanguageLocaleKey = 'en_US'
            ,TimeZoneSidKey = 'Europe/Paris'
            ,LocaleSidKey = 'en_US'
            ,EmailEncodingKey = 'UTF-8'
            ,ProfileId = profId
        );
    }

    //create sofacto admin
    public static User insertSofactoAdminUser(String name){
        User usr = new User(
            Username = TestFactory.randomizeString(name) + '@test.com'
            ,LastName = 'test'
            ,FirstName = 'test'
            ,Email = 'test@test.com'
            ,Alias = 'test'
            ,LanguageLocaleKey = 'en_US'
            ,TimeZoneSidKey = 'Europe/Paris'
            ,LocaleSidKey = 'en_US'
            ,EmailEncodingKey = 'UTF-8'
            ,ProfileId = getProfileAdminId()
            ,sofactoapp__isSofactoFinance__c = true
        );

        insert usr;

        PackageLicense pkgLicence = [Select Id from PackageLicense where NamespacePrefix= 'sofactoapp' LIMIT 1];
        UserPackageLicense usrPkgLis = new UserPackageLicense(
            UserId = usr.Id,
            PackageLicenseId = pkgLicence.Id
        );

        insert usrPkgLis;

        List<PermissionSetAssignment> lstPsa = new List<PermissionSetAssignment>();
        for(PermissionSet ps : [SELECT Id, Name FROM PermissionSet WHERE Name IN ('Responsable_facturation_Sofacto_FULL', 'Sofacto_FLS_WRITE', 'Sofacto_Finance_Fields', 'Sofacto_Finance_Objects', 'Sofacto_Sales_Fields', 'Sofacto_Sales_Objects', 'UserApi_facturation_Sofacto_FULL')]){
            lstPsa.add(new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id));
        }
        insert lstPsa;

        return usr;
    }

    // create community user
    public static User createCommunityUser(Account acc){
        return new User( 
            Email = acc.PersonEmail,
            ProfileId = getProfileCommunityId(),
            UserName = acc.FirstName + '.' + acc.LastName + '@cham.fr', 
            Alias = 'Test',
            TimeZoneSidKey = 'America/New_York',
            EmailEncodingKey = 'ISO-8859-1',
            LocaleSidKey = 'en_US', 
            LanguageLocaleKey = 'en_US',
            ContactId = acc.PersonContactId,
            FirstName = acc.FirstName,
            LastName = acc.LastName
        );
    }

    //create Lead
    public static Lead createLead(String name, String status, String phone, String email) {
        return new Lead(
            LastName = TestFactory.randomizeString(name)
            ,Status = status
            ,Company = TestFactory.randomizeString(name)
            ,Phone = phone
            ,Email = email
        );
    }

    //create ContentVersion
    public static ContentVersion createContentVersion(String title, String type) {
        return new ContentVersion(
            Title = title,
            PathOnClient = title +'.'+type,
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
    }

    // create ContentVersion with ContentDocumentLink
    public static ContentVersion createContentVersionWithLink(String parentId, String title, String type){
        ContentVersion cv = createContentVersion(title, type);
        insert cv;

        ContentVersion contentVersion = [SELECT ContentDocumentId, Title FROM ContentVersion WHERE Id =:cv.Id];
        ContentDocumentLink cDocLink = new ContentDocumentLink(
            ContentDocumentId = contentVersion.ContentDocumentId,
            LinkedEntityId = parentId,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        //ContentDocumentLinkTriggerHandler.bypass = true;
        Insert cDocLink;

        return contentVersion;
    }

    //create Account
    public static Account createAccount(String name) {
        return new Account(
            LastName = name,
            PersonEmail = 'test@test.com',
            BillingPostalCode = 'test',
            PersonMobilePhone = '12345',
            PersonHomePhone ='12345'            
        );
    }

    //create Account Business
    public static Account createAccountBusiness(String name) {
        return new Account(
            name = name,
            BusinessAccountEmail__c = 'test@test.com',
            BillingPostalCode = 'test'
        );
    }

    //create Logement
    public static Logement__c createLogement(String accountId, String agencyId) {
        return new Logement__c(
            Account__c = accountId,
            Agency__c = agencyId
        );
    }

    //create AccountLogement
    public static Compte_Logement__c createCompteLogement(String accountId, String logementId) {
        return new Compte_Logement__c(
            Account__c = accountId,
            Logement__c = logementId
        );
    }

    //create Asset // TO BE DEPRECATED !!!!
    public static Asset createAccount(String name, String status, String logementId) { // TO BE DEPRECATED !!!!
        return new Asset(
            Name = name,
            Status = status,
            Logement__c = logementId
        );
    }

    //create Asset
    public static Asset createAsset(String name, String status, String logementId) {
        return new Asset(
            Name = name,
            Status = status,
            Logement__c = logementId
        );
    }    

    //create Opportunity
    public static Opportunity createOpportunity(String name, String accountId) {
        return new Opportunity(
            Name = name,
            AccountId = accountId,
            CloseDate = System.today().addDays(1),
            StageName = 'Qualification',
            RecordTypeId =AP_Constant.getRecTypeId('Opportunity', 'Opportunite_standard')
        );
    }

    //create Contract
    public static Contract createContract(String name, String accountId) {
        return new Contract(
            Name = name,
            AccountId = accountId,
            StartDate = System.today(),
            ContractTerm = 1
        );
    }

    //create ServiceAppointment
    public static ServiceAppointment createServiceAppointment(String parentRecordId) {
        return new ServiceAppointment(
            Status = 'Planned',
            EarliestStartTime = System.today(),
            DueDate = System.today().addDays(1),
            ParentRecordId = parentRecordId
        );
    }

      //create ServiceAppointment with Agency
      public static ServiceAppointment createServiceAppointment(String parentRecordId, String nameAgency) {
        return new ServiceAppointment(
            Status = 'Planned',
            EarliestStartTime = System.today(),
            DueDate = System.today().addDays(1),
            ParentRecordId = parentRecordId,
            ServiceTerritoryId = nameAgency
        );
    }

    //create ServiceAppointment
    public static ServiceAppointment createServiceAppointment() {
        return new ServiceAppointment(
            EarliestStartTime = System.today(),
            DueDate = System.today().addDays(1)
        );
    }

    public static OperatingHours createOperatingHour(string opHour) {
        return new OperatingHours(Name=opHour,
                                    TimeZone ='Europe/Paris');
    }

    public static Agency_Fixed_Slots__c createAgencyFixedSlot(string name, String slotFor) {
        return new Agency_Fixed_Slots__c(Name = name,
                            Start_Time_of_the_slot__c = System.now(),
                            Slot_taken__c = false,
                            EndTime_of_the_slot__c = System.now().addHours(1),
                            Slot_for__c = slotFor);
    }

    public static Agency_Fixed_Slots__c createAgencyFixedSlot(string name, String slotFor, Id agencyId) {
        return new Agency_Fixed_Slots__c(Name = name,
                            Agence__c = agencyId,
                            Start_Time_of_the_slot__c = System.now(),
                            Slot_taken__c = false,
                            EndTime_of_the_slot__c = System.now().addHours(1),
                            Slot_for__c = slotFor);
    }

    public static TimeSlot createTimeSlot(string dayOfWeek, OperatingHours operatingHoursId) {
        return new TimeSlot(DayOfWeek=dayOfWeek,
                            StartTime = System.now().Time(),
                            Type = 'Normal',
                            EndTime = System.now().addHours(1).Time(),
                            OperatingHoursId = operatingHoursId.Id);
    }
    
    public static ServiceTerritory createServiceTerritory(string terName,string opHourId,string raison_socialeId) {
        return new ServiceTerritory(Name=terName,
                                    IsActive = true,
                                    OperatingHoursid=opHourId,
                                    Sofactoapp_Raison_Social__c = raison_socialeId
                                    );
    }

        public static ServiceTerritory createServiceTerritory(string terName,string opHourId) {
        return new ServiceTerritory(Name=terName,
                                    IsActive = true,
                                    OperatingHoursid=opHourId
                                    );
    }
    
    public static Sofactoapp__Raison_Sociale__c createSofactoapp_Raison_Sociale(string name) {
        return new Sofactoapp__Raison_Sociale__c(Name=name,
                                                Sofactoapp__Credit_prefix__c='creditNote', 
                                                Sofactoapp__Invoice_prefix__c='InvoiceNote');
    }

    public static ServiceTerritoryMember createServiceTerritoryMember(string ressourceId,string TerritoryId) {
        return new ServiceTerritoryMember(ServiceTerritoryid=TerritoryId,
                                            ServiceResourceid=ressourceId,
                                            TerritoryType = 'P',
                                            EffectiveStartDate=system.now()-3);
    }

    //public static SingleRequestMock blueAuth = new SingleRequestMock(200,'Success','{"token":"eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6W-HbnfPj7RHRrGD8iK5nOYmiqqKTlJ8XZg","user":{"id":116221,"username":"Toni","email":"cham@izi-by-edf.fr","roles":["ROLE_API_CHAM"],"anytimeCard":false}}',null);
    //public static SingleRequestMock blueWSCallout_Status = new SingleRequestMock(200,'Success','{"id":91193,"state":999,"customerContract":null,"proAttestation":null}',null);
    //DMU - 20190202: Added to increase coverage AP01_BlueWSCallout
    //public static SingleRequestMock blueWSCallout_pdf = new SingleRequestMock(200,'Success','{"id":9711,"state":999,"customerContract":null,"proAttestation":null}',null);

    public static Map<String, HttpCalloutMock> multiMockMap(String endpoint) {
        Map <String, HttpCalloutMock> mocksMap = new Map <String, HttpCalloutMock> ();
        /*
        mocksMap.put(endpoint + '/api/login_check', blueAuth);
        mocksMap.put(endpoint + '/api/v2.2/order-groups/9711', blueWSCallout_Status);
        //DMU - 20190202: Added to increase coverage AP01_BlueWSCallout
        mocksMap.put(endpoint + '/api/v2.2/order-groups/9711/files', blueWSCallout_pdf); 
        */
        return mocksMap;
    }
    
    //YGO - 
    public static HTTPResponse respond() {

        //create a fake response
        HttpResponse res = new HttpResponse();
        res.setBody('Le client C000004 a été créé.');
        res.setStatusCode(201);
        return res;
    } 

    //get recordTypeId
    public static Id getRecordTypeId(String objectType, String RecordTypeLabel){
        System.Debug('### getContactRecordType' + UserInfo.getUserName());        
        SObject obj; 
        
        // Describing Schema  
        Schema.SObjectType res = Schema.getGlobalDescribe().get(ObjectType);
        
        if (res != null){  
            obj = res.newSObject();
            
            // Describing Object 
            Schema.DescribeSObjectResult DesRes = obj.getSObjectType().getDescribe();
            if (DesRes != null){
                Map<string,schema.recordtypeinfo> recordTypeMap = DesRes.getRecordTypeInfosByName();

                if (RecordTypeMap != null) {
                    Schema.RecordTypeInfo RecordTypeRes = RecordTypeMap.get(RecordTypeLabel);

                    if (RecordTypeRes != null){
                        return RecordTypeRes.getRecordTypeId();
                    }
                }
            }
        }
        
        return null;  
    }

    //DMU 
    public static HTTPResponse respond_PDFFile() {

        //create a fake response
        HttpResponse res = new HttpResponse();
        res.setBody('{"id":9711,"state":999,"customerContract":null,"proAttestation":null}');
        res.setStatusCode(201);
        return res;
    } 

    //create Logement__c
    public static Logement__c createLogement(String name, string email, string lname) {
        // Id RecordTypeId_Logement =  Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Logement').getRecordTypeId();
        Id RecordTypeId_Lot =       Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Lot').getRecordTypeId();
        //Id RecordTypeId_Market =    Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Market').getRecordTypeId();
        // Id RecordTypeId_Souslot =    Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Sous_lot').getRecordTypeId();

        return new Logement__c(
            Owner_Name__c = TestFactory.randomizeString(name), 
            Owner_E_mail__c = email, 
            // un numéro de tel est obligatoire à la création du compte
            Owner_Mobile_Phone__c = '0',
            Owner_Surname__c = lname,
            RecordTypeId = RecordTypeId_Lot // Added SH - 2019-12-27 (Souslot was chosen because last on if statement, so more code coverage...)
        );
    }

    //create work order
    public static WorkOrder createWorkOrder(){
        return new WorkOrder(
            Status = AP_Constant.wrkOrderStatusNouveau,
            Priority = AP_Constant.wrkOrderPriorityNormal,
            //BCH Added for WorkType 
            Type__c = 'Various Services'
            // , Reason__c = 'Other'
        );
    }

    //create Product
    public static Product2 createProduct(String name){
        return new Product2(
            Name = name,
            isActive = true,
            IsBundle__c = true,
            Statut__c = AP_Constant.productStatutApprouvee,
            ProductCode = 'Test Product Code',
            RecordTypeId =  Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()
        );
    }
   //create Proudc
    
     //create ProductAsset
    public static Product2 createProductAsset(String name){
        return new Product2(
            Name = name,
            isActive = true,
            Statut__c = AP_Constant.productStatutApprouvee,
            RecordTypeId =  Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Product').getRecordTypeId()
        );
    }
    
    //create contract lime items
    public static ContractLineItem createContractLineItem(Double unitPrice, Double quantity){
        return new ContractLineItem(
            UnitPrice = unitPrice,
            Quantity = quantity
        );
    }

    //RRJ 20193007
    //create worktype
    public static WorkType createWorkType(String name, String durationType, Integer estDuration){
        return new WorkType(
            Name = name,
            DurationType = durationType,
            EstimatedDuration = estDuration
        );
    }

    //create bundle
    public static Bundle__c createBundle(String name, Integer price){
        return new Bundle__c(
            Name = name,
            Price__c = price
        );
    }

    // create bundle product
    public static Product_Bundle__c createBundleProduct(String bundleId, String prodId){
        return new Product_Bundle__c(
            Bundle__c = bundleId,
            Product__c = prodId
        );
    }

    public static PriceBookEntry createPriceBookEntry(String prcBkId, String prodId, Integer price){
        return new PriceBookEntry(
            PriceBook2Id = prcBkId,
            Product2Id = prodId,
            UnitPrice = price,
            IsActive = true
        );
    }

    public static ServiceContract createServiceContract(String name, String accId){
        return new ServiceContract(
            Name = name,
            AccountId = accId
        );
    }

    public static ContractLineItem createContractLineItem(Id servConId,String prcBkEnrtyId,Integer unitPrice,Integer quantity){
        return new ContractLineItem(
            ServiceContractId = servConId,
            PriceBookEntryId = prcBkEnrtyId,
            UnitPrice = unitPrice,
            Quantity = quantity
        );
    }

    public static ContractLineItem createContractLineItemSuspension(String servConId,String prcBkEnrtyId, Date debutSusp, Date FinSusp, Integer unitPrice,Integer quantity){
        return new ContractLineItem(
            ServiceContractId = servConId,
            PriceBookEntryId = prcBkEnrtyId,
            Debut_de_la_suspension__c = debutSusp,
            Fin_de_la_suspension__c = FinSusp,
            UnitPrice = unitPrice,
            Quantity = quantity
        );
    }

    public static Case createCase(String accId, String type, String assetId){
        return new Case(
            AccountId = accId,
            Type = type,
            AssetId = assetId
        );
    }

    public static ContentWorkspace createContentWorkSpace(String name){
        return new ContentWorkspace(
            Name = name
        );
    }
    
    public static void initQuoteSyncSetting(){
            List<SyncingQLI__c> lstQuoSycn =  new List<SyncingQLI__c>{
                //Opportunity-Quote
                new SyncingQLI__c( Name = 'AccountId',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'AccountId',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = false
                                 ),
                new SyncingQLI__c( Name = 'Agency__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Agency__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),

                new SyncingQLI__c( Name = 'Ordre_d_execution__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Ordre_d_execution__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = false
                                 ),
                new SyncingQLI__c( Name = 'CloseDate',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'ExpirationDate',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = false
                                 ),
                new SyncingQLI__c( Name = 'Pricebook2Id',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Pricebook2Id',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = false
                                 ),
                new SyncingQLI__c( Name = 'TECH_Titre_du_devis__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Objet_du_devis__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),
                new SyncingQLI__c( Name = 'TECH_Demande_d_acompte__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Acompte_demande__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),
                new SyncingQLI__c( Name = ' sofactoapp__Contact__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'ContactId',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),
                new SyncingQLI__c( Name = 'sofactoapp__Solde__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Montant_du_solde__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),
                new SyncingQLI__c( Name = 'Tech_Devis_sign_par_le_client__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Devis_signe_par_le_client__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),
                new SyncingQLI__c( Name = 'TECH_Signature_du_client__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Signature_du_client__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),
                new SyncingQLI__c( Name = 'Montant_de_l_acompte__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Montant_de_l_acompte_verse__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),
                new SyncingQLI__c( Name = 'TECH_Type_de_devis__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Type_de_devis__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),
                new SyncingQLI__c( Name = 'TECH_Mode_de_paiement_du_devis__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Mode_de_paiement_du_devis__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),       
                new SyncingQLI__c( Name = 'TECH_Mode_de_paiement_de_l_acompte__c',
                                    MappingType__c = 'Opportunity-Quote',
                                    QLI__c = 'Mode_de_paiement_de_l_acompte__c',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = true
                                 ),                                                                                                                                                              
                //OpportunityLineItem-QuoteLineItem
                new SyncingQLI__c( Name = 'Remise_en_100__c',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'Discount',
                                    ToCreateQLI__c = true,
                                    ToCreateQuote__c = false
                                 ),
                new SyncingQLI__c( Name = 'UnitPrice',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'UnitPrice',
                                    ToCreateQLI__c = true,
                                    ToCreateQuote__c = false
                                 ),
                new SyncingQLI__c( Name = 'Quantity',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'Quantity',
                                    ToCreateQLI__c = true,
                                    ToCreateQuote__c = false
                                 ),  
                new SyncingQLI__c( Name = 'Product2Id',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'Product2Id',
                                    ToCreateQLI__c = true,
                                    ToCreateQuote__c = false
                                 ),  
                new SyncingQLI__c( Name = 'Description',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'Description',
                                    ToCreateQLI__c = true,
                                    ToCreateQuote__c = false
                                 ),    
                new SyncingQLI__c( Name = 'ServiceDate',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'ServiceDate',
                                    ToCreateQLI__c = true,
                                    ToCreateQuote__c = false
                                 ),   
                new SyncingQLI__c( Name = 'Discount',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'Remise_en100__c',
                                    ToCreateQLI__c = true,
                                    ToCreateQuote__c = false
                                 ),   
                new SyncingQLI__c( Name = 'TECH_CorrespondingQLItemId__c',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'Id',
                                    ToCreateQLI__c = false,
                                    ToCreateQuote__c = false
                                 ),    
                new SyncingQLI__c( Name = 'sofactoapp__VAT_rate__c',
                                    MappingType__c = 'OpportunityLineItem-QuoteLineItem',
                                    QLI__c = 'Taux_de_TVA__c',
                                    ToCreateQLI__c = true,
                                    ToCreateQuote__c = false
                                 )                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                };
            insert lstQuoSycn;

    }
    public static ServiceTerritory createServiceTerritoryWithLevel(string terName,string opHourId,string raison_socialeId,decimal level) {
        return new ServiceTerritory(Name=terName,
                                    IsActive = true,
                                    OperatingHoursid=opHourId,
                                    Sofactoapp_Raison_Social__c = raison_socialeId,
                                    FSL__TerritoryLevel__c = level
                                    );
    }
    
    public static Agency_intervention_zones__c createAgencyInterventionZone(string zipCode, string agencyId)
    {
        return new Agency_intervention_zones__c(Zip_Code__c = zipCode,Agence__c=agencyId);
    }

    public static Tarifs_Unifies_Locaux__c createTarifsUnifiesLocaux(string name,Integer capAugmentationMax, string bundleId,string agendyId,Integer tarif)
    {
        return new Tarifs_Unifies_Locaux__c(Name=name,Cap_d_augmentation_max__c = capAugmentationMax,Bundle_du_contrat__c=bundleId,Agence__c=agendyId,Tarif_Unifie_Local__c=tarif,
                                            Date_de_debut_validite_pour_creation__c = System.today(),
                                            Date_de_fin_validite_pour_creation__c = System.today().addDays(30),
                                            Date_de_debut_validite_renouvellement__c=System.today(),
                                            Date_de_fin_validite_renouvellement__c = System.today().addDays(60),
                                            Augmentation_nationale_annuelle__c = 50.00
                                            );
    }

    public static Devis_type__c createDevisType(String name){
        return new Devis_type__c(
            Name = name
            , Global__c = true
            , Description__c = 'Devis Typw Test'
        );
    }

    public static Lignes_de_devis_type__c createLigneDevisType(String devTypeId, String prodId){
        return new Lignes_de_devis_type__c(
            Devis_type__c = devTypeId
            , Produit__c = prodId
            , Quantity__c = 1
            , Description__c = 'Ligne de devis Type test'
        );
    }

    public static OpportunityLineItem createOpportunityLine(String oppId, String pbeId){
        return new OpportunityLineItem(
            OpportunityId = oppId
            , PricebookEntryId = pbeId
            , Quantity = 1
            , UnitPrice = 10
        );
    }

}