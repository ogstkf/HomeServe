/**
 * @File Name          : AP63_ChangePITValues.cls
 * @Description        : 
 * @Author             : SH
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    22/01/2020         SH                    Initial Version
**/
public with sharing class AP63_ChangePITValues {
    public static void changeValues(List<ProductItemTransaction> lstPIT){
        Set<Id> piIdsSets = new Set<Id>();

        // Get Ids of all the ProductItemTransaction
        for (ProductItemTransaction pit : lstPIT) {
            if ((pit.TransactionType == 'Transferred' || pit.TransactionType == 'Consumed') && (pit.Quantity < 0)) {
                piIdsSets.add (pit.ProductItemId);
            }
        }

        // Get list of corresponding ProductItem
        List<ProductItem> lstPI = [
            SELECT Id, Classification__c, Classe_de_rotation__c
            FROM ProductItem
            WHERE
                Location.LocationType = 'Entrepôt' AND
                Id IN :piIdsSets AND
                Classification__c IN ('Stock mort', 'Inactif', null)
        ];

        for (ProductItem pi : lstPI) {
            pi.Classification__c = 'A stocker';
            pi.Classe_de_rotation__c = 'A';
        }
        update lstPI;
    }
}