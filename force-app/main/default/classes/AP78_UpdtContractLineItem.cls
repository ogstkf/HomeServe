/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 04-01-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-24-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class AP78_UpdtContractLineItem {
    public Set<Id> setSAIds; //store ids of Service appointment
    public Set<Id> setAssetIds;   //store ids of Asset
    public Map<Id,ContractLineItem> mapSCAsCLIIds; //store ids of Asset and associated CLI
    public Map<Id,ServiceAppointment> mapSAIds; //store asset Id and associated service appointment
    public Map<Id,ServiceAppointment> mapChildSAIds; //store child asset Id and associated service appointment
    public List<ContractLineItem> lstCLIUpd; //store list of CLI to update
    public Set<Id> setAccIds;   //store ids of Account
    public Set<Id> setSCIds; //store ids of Service contract
    public Set<Id> setWOAssetIds;   //store ids of Asset of work order
    public Set<Id> setMesAssetIds;   //store ids of Asset of Mesures équipements secondaires



	public AP78_UpdtContractLineItem() {
        setSAIds = new Set<Id>();
        setAssetIds = new Set<Id>();
        mapSCAsCLIIds = new Map<Id,ContractLineItem> (); 
        mapSAIds = new Map<Id,ServiceAppointment> (); 
        lstCLIUpd = new List<ContractLineItem>();
        setAccIds = new Set<Id>();
        setSCIds = new Set<Id>();
        setWOAssetIds = new Set<Id>();
        mapChildSAIds = new Map<Id,ServiceAppointment> ();  
        setMesAssetIds = new Set<Id>(); 
	}

    // Method to calculate Contract line Items of Associated Service Appointments

    public void calculateCLI(){
        system.debug('**calculateCLI**');
        //extract all Asset from service contracts of the Service appointment   
        for(ServiceAppointment sa : [SELECT Id,Status, AccountId,TECH_ServiceContractType__c,Service_Contract__c,Service_Contract__r.Asset__c
                                    ,Service_Contract__r.Asset__r.Name,ActualEndTime,Work_Order__c,Work_Order__r.AssetId,TECH_CountMesure__c
                                        FROM ServiceAppointment
                                        WHERE Id IN:setSAIds
                                        AND TECH_ServiceContractType__c =: 'Collective'
                                        AND Work_Order__r.AssetId != null
                                        AND  TECH_CountMesure__c >= 1
                                        AND Status IN ('Done OK','Done KO')]){
            mapSAIds.put(sa.Work_Order__r.AssetId,sa);
            setSCIds.add(sa.Service_Contract__c);
            setAccIds.add(sa.AccountId);

        } 
        
       // system.debug('**calculateCLI** mapSAIds '+mapSAIds);
        //system.debug('**calculateCLI** setSCIds '+setSCIds);
        //system.debug('**calculateCLI** setAccIds '+setAccIds);

        //extract all contract line items with same ServiceContract and same Service Appointment Account
        for(ContractLineItem cli : [SELECT Id,AssetId,Asset.Name ,Collective_Account__c,ServiceContractId,VE__c
                                    ,Completion_Date__c,TECH_Code_produit__c ,VE_Status__c
                                            FROM ContractLineItem
                                            WHERE ServiceContractId IN: setSCIds 
                                            AND Collective_Account__c IN: setAccIds 
                                            AND TECH_Code_produit__c LIKE '%VF%']){     
            mapSCAsCLIIds.put(cli.AssetId, cli);           
        }
        
        //retrieve asset link to Mesures équipements secondaires
        for(Mesures_quipements_secondaires__c msec: [SELECT Id, Mesures_quipements_secondaires__c,Rendez_vous_de_service__c 
                         FROM Mesures_quipements_secondaires__c
                        WHERE Rendez_vous_de_service__c IN: setSAIds]){
            setMesAssetIds.add(msec.Mesures_quipements_secondaires__c);
        }
        
		//system.debug('**calculateCLI** mapSCAsCLIIds '+mapSCAsCLIIds);
        
        //retrieve all child of the asset of work orders Service Appointment
        for(Asset as1 : [SELECT Id,Product2Id, ParentId FROM Asset 
                         WHERE ParentId IN:mapSAIds.KeySet()
                         AND Id IN:setMesAssetIds]){
                             
            mapChildSAIds.put(as1.Id, mapSAIds.get(as1.ParentId));
        }
		//system.debug('**calculateCLI** mapChildSAIds '+mapChildSAIds);
        //extract all asset that are common from the CLI and lstChildAsset
        for(Id aIdSA : mapChildSAIds.KeySet()){
            for(Id aIdCLI : mapSCAsCLIIds.KeySet()){      
                if(aIdSA == aIdCLI ){
                    ContractLineItem cliUpd = new ContractLineItem();                    
                    if(mapChildSAIds.get(aIdSA).ActualEndTime != null){
                        cliUpd.Completion_Date__c = (mapChildSAIds.get(aIdSA).ActualEndTime).date();
                    }

                    cliUpd.Id = mapSCAsCLIIds.get(aIdCLI).Id;
                    cliUpd.VE_Status__c = 'Complete';
                    lstCLIUpd.add(cliUpd);
                }
            }
        }

        //system.debug('**calculateCLI** lstCLIUpd '+lstCLIUpd);      
        if(!lstCLIUpd.isEmpty()){
            update lstCLIUpd;

        }
         
    }


}