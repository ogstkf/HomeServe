/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 24-12-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   23-12-2021   MNA   Initial Version
**/
public with sharing class AP80_UpdateEntitlementRelated {
    public static void update_entitlement (Set<Id> setCliId) {
        List<Entitlement> lstEtlToUpd = new List<Entitlement>();
        for (Entitlement etl : [SELECT Id, ContractLineItem.AssetId, ContractLineItem.StartDate, ContractLineItem.EndDate, ContractLineItem.ServiceContractId
                    FROM Entitlement WHERE ContractLineItemId IN: setCliId]) {

                        lstEtlToUpd.add(
                            new Entitlement(
                                Id = etl.Id,
                                AssetId = etl.AssetId,
                                StartDate = etl.StartDate,
                                EndDate = etl.EndDate,
                                ServiceContractId = etl.ServiceContractId
                            )
                        );
        }
        if (lstEtlToUpd.size() > 0)
            update lstEtlToUpd;
    }
}