/**
 * @File Name          : AP40_UpdtQuotePRLITEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/11/2019   AMO     Initial Version
**/
@isTest
public with sharing class AP40_UpdtQuotePRLITEST {
    static User adminUser;
    static Account testAcc = new Account();
    static sofactoapp__Compte_auxiliaire__c CA = new sofactoapp__Compte_auxiliaire__c (); // Sub-ledger Account
    static sofactoapp__Coordonnees_bancaires__c CB = new sofactoapp__Coordonnees_bancaires__c(); // Bank Details
    static sofactoapp__Compte_comptable__c CC = new sofactoapp__Compte_comptable__c (); // Accounting Account
    static List<sofactoapp__Plan_comptable__c> PC = new List<sofactoapp__Plan_comptable__c> (); // Chart of Accounts
    static List<Quote> lstQuote = new List<Quote>();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<WorkOrder> lstWorkOrder = new List<WorkOrder>();
    static List<ProductRequestLineItem> lstprli = new List<ProductRequestLineItem>();
    static List<ProductRequest> lstProdRequest = new List<ProductRequest>();
    static Product2 prod = new Product2();
    static List<ProductItem> lstProdItem = new List<ProductItem>();
    static Schema.Location loc = new Schema.Location();
    static list<QuoteLineItem> lstQli = new list<QuoteLineItem>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static Bypass__c bp = new Bypass__c();    

    static {
        adminUser = TestFactory.createAdminUser('AP40_UpdtQuotePRLITEST@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;

        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53,ProductRequestLineItemTrigger,AP36';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(adminUser){

            PC = new List<sofactoapp__Plan_comptable__c> {
                new sofactoapp__Plan_comptable__c(),
                new sofactoapp__Plan_comptable__c(),
                new sofactoapp__Plan_comptable__c()
            };  
            insert PC; 

            // create sofactoapp__Compte_comptable__c (Accounting Account)
            CC.Name = 'test CC';
            CC.sofactoapp__Libelle__c = 'test libellé';
            CC.sofactoapp__Plan_comptable__c = PC[0].Id;
            insert CC; 

            // create sofactoapp__Compte_auxiliaire__c (Sub-ledger Account)
            CA.Name = 'test account aux';
            CA.sofactoapp__Compte_comptable__c = CC.Id;
            CA.Compte_comptable_Prelevement__c = CC.Id;
            insert CA; 

            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;   

            lstOpp =  new List<Opportunity>{ 
                TestFactory.createOpportunity('Test1',testAcc.Id),
                TestFactory.createOpportunity('Test2',testAcc.Id),
                TestFactory.createOpportunity('Test2',testAcc.Id),
                TestFactory.createOpportunity('Test2',testAcc.Id),
                TestFactory.createOpportunity('Test2',testAcc.Id),
                TestFactory.createOpportunity('Test2',testAcc.Id),
                TestFactory.createOpportunity('Test2',testAcc.Id),
                TestFactory.createOpportunity('Test2',testAcc.Id) 
            };
            for (Opportunity opp : lstOpp) {
                opp.APP_intervention_immediate__c = true;
                opp.StageName = 'Denied';
            }
            insert lstOpp;

            //Create WorkOrder
            lstWorkOrder = new List<WorkOrder>{
                new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal)
            };
            insert lstWorkOrder;

            lstProdRequest = new List<ProductRequest> {
                new ProductRequest(),
                new ProductRequest(),
                new ProductRequest()
            };  
            insert lstProdRequest;    

              //create Location
            loc = new Schema.Location(Name='Test1',
            LocationType='Entrepôt',
            ParentLocationId=NULL,
            IsInventoryLocation=true);
            insert loc;

            //Create product
            Product2 prod = TestFactory.createProduct('Product X');
            prod.ProductCode = 'Pro-X';
            prod.Equipment_family__c = 'Chaudière';
            prod.Equipment_type__c = 'Chaudière gaz';
            
            insert prod;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //create Quotes
            lstQuote =  new List<Quote>{
                new Quote(Name ='Test1',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = lstOpp[0].Id, 
                    Status = 'Denied', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200, Pricebook2Id = lstPrcBk[0].Id ) ,
                // new Quote(Name ='Test2',  
                //     Devis_signe_par_le_client__c = false, 
                //     OpportunityId = lstOpp[1].Id, 
                //     Status = 'Validé,signé mais abandonné') ,
                // new Quote(Name ='Test2',  
                //     Devis_signe_par_le_client__c = false, 
                //     OpportunityId = lstOpp[2].Id, 
                //     Status = 'Validé, signé et terminé') ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = lstOpp[3].Id, 
                    Status = 'Needs Review', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200, Ordre_d_execution__c = lstWorkOrder[0].Id, Pricebook2Id = lstPrcBk[0].Id) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = lstOpp[4].Id, 
                    Status = 'In Review', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200,  Ordre_d_execution__c = lstWorkOrder[0].Id, Pricebook2Id = lstPrcBk[0].Id) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = lstOpp[5].Id, 
                    Status = 'Draft', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200,  Ordre_d_execution__c = lstWorkOrder[0].Id, Pricebook2Id = lstPrcBk[0].Id) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = lstOpp[6].Id, 
                    Status = 'Denied', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200,  Ordre_d_execution__c = lstWorkOrder[0].Id, Pricebook2Id = lstPrcBk[0].Id) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = lstOpp[7].Id, 
                    Status = 'Denied', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200,  Ordre_d_execution__c = lstWorkOrder[0].Id, Pricebook2Id = lstPrcBk[0].Id),
                //CT-1181
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = true, // SH 2020-01-27 - Was false
                    Date_de_debut_des_travaux__c = System.today(), // SH 2020-01-27 - Added
                    OpportunityId = lstOpp[6].Id, 
                    Status = 'Validé, signé - en attente d\'intervention', 
                    //Raisons_de_l_abandon_du_devis__c = 'Important',
                    Ordre_d_execution__c = lstWorkOrder[0].Id, tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200, recordtypeId = AP_Constant.getRecTypeId('Quote', 'Devis_standard'), Pricebook2Id = lstPrcBk[0].Id)  
                };
            insert lstQuote;

            lstprli = new List<ProductRequestLineItem>{
                new ProductRequestLineItem(                        
                    Product2Id = prod.Id,
                    QuantityRequested = 1,
                    ParentId = lstProdRequest[0].Id,
                    Status = 'Réservé à préparer',
                    // Commande__c = lstOrder[0].Id),
                    WorkOrderId = lstWorkOrder[0].Id,
                    DestinationLocationId = loc.Id,
                    Quote__c = lstQuote[0].Id
                ),
                new ProductRequestLineItem(                        
                    Product2Id = prod.Id,
                    QuantityRequested = 1,
                    ParentId = lstProdRequest[0].Id,
                    Status = 'Réservé à préparer',
                    // Commande__c = lstOrder[0].Id),
                    WorkOrderId = lstWorkOrder[0].Id,
                    DestinationLocationId = loc.Id,
                    Quote__c = lstQuote[0].Id
                )
            };
            insert lstprli;


            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;

            //create QuoteLineItem
            lstQli =  new List<QuoteLineItem>{
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = prod.Id,
                                    QuoteId = lstQuote[0].Id, 
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today(),
                                    PriceBookEntryId = PrcBkEnt.Id
                                 )
                };
            insert lstQli;
           
            //Create ProductItem
            lstProdItem = new List<ProductItem>{
                new ProductItem(  
                    Quantite_allouee__c = -12,
                    Product2Id = prod.Id,
                    LocationId = loc.Id,
                    QuantityOnHand = 5
                )
            };
            insert lstProdItem;

            lstQuote[0].Montant_de_l_acompte_verse__c = 300;
            lstQuote[1].Montant_de_l_acompte_verse__c = 300;
            lstQuote[2].Montant_de_l_acompte_verse__c= 300;
            lstQuote[3].Montant_de_l_acompte_verse__c= 300;
            lstQuote[4].Montant_de_l_acompte_verse__c= 300;
            lstQuote[5].Montant_de_l_acompte_verse__c= 300;
            lstQuote[6].Montant_de_l_acompte_verse__c= 300;
            //lstQuote[7].Montant_de_l_acompte_verse__c= 300;
            update lstQuote;
        }
    }

    @isTest
    static void testNegotiation(){
        System.runAs(adminUser){
            lstQuote[6].Status = 'Validé, signé et terminé';
            lstQuote[6].TECH_Devis_P3R__c = true;
            //lstQuote[6].Raisons_de_l_abandon_du_devis__c = null;
            //lstQuote[4].Devis_signe_par_le_client__c = true;// SH 2020-01-27 - Was false
            //lstQuote[4].Date_de_debut_des_travaux__c = System.today(); // SH 2020-01-27 - Added

            Test.startTest();
                update lstQuote[6];
                AP40_UpdtQuotePRLI.UpdtPRLI(lstQuote);
            Test.stopTest();

            System.assertEquals(lstQuote[6].Status, 'Validé, signé et terminé');
            
            ProductRequestLineItem prli1 = [SELECT Id, Status FROM ProductRequestLineItem WHERE Id = :lstprli[0].Id];
            // System.assertEquals(prli1.Status, 'Réservé à préparer');
            //ProductRequestLineItem prli2 = [SELECT Id, Status FROM ProductRequestLineItem WHERE Id = :lstprli[2].Id];
            //System.assertEquals(prli2.Status, 'Annulé');
        }
    }
}