/**
 * @File Name          : VFC39_OrderItemPDF.cls
 * @Description        : 
 * @Author             : Spoon
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 26/02/2020, 15:46:19
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0       10/22/2019                   KZE               Initial Version-CT 1203
 * 1.1       01/12/2019                   SHU               Attache document as Files instead of Atachments
**/
public with sharing class VFC39_OrderItemPDF {
    public Order theOrder {get;set;}
    public string orderid {get;set;}
    public List<OrderItem> lstOItems {get;set;}
    public List<Order> lstOrder {get;set;}
    public string noClient {get;set;}
    public Boolean hasNoClient{get;set;}


    public VFC39_OrderItemPDF(ApexPages.StandardController stdController) {
        System.debug('**** In constructor VFC39_OrderItemPDF ****');
        orderid = ApexPages.currentPage().getParameters().get('id');
        theOrder= new Order();
        lstOItems = new List<OrderItem>();

        if(orderid != null){
            lstOrder = new List<Order>([SELECT  Id, 
                                                Agence__c, 
                                                Agence__r.Name, 
                                                Agence__r.Corporate_Name__c,
                                                Agence__r.Street, 
                                                Agence__r.Street2__c,
                                                Agence__r.City, 
                                                Agence__r.PostalCode,
                                                Agence__r.Phone__c, 
                                                Agence__r.Fax__c, 
                                                Agence__r.Email__c,
                                                Agence__r.SIREN__c,
                                                Agence__r.SIRET__c,
                                                Agence__r.Intra_Community_VAT__c,
                                                Agence__r.Agency_Code__c,
                                        		Agence__r.Logo_Name_Agency__c,
                                                Type,
                                                OrderNumber,
                                                EffectiveDate,
                                                Mode_de_transmission__c, 
                                                Owner.Name,Owner.Email,
                                                Interlocuteur_principal__r.Email,  
                                                Interlocuteur_principal__r.Fax, 
                                                Account.Name, 
                                                Account.ShippingStreet, 
                                                Account.ShippingPostalCode, 
                                                Account.ShippingCity,
                                                Account.BillingStreet, 
                                                Account.BillingPostalCode, 
                                                Account.BillingCity,
                                                Account.Code_client_CHAM_chez_le_fournisseur__c,
                                                TotalAmount,
                                                Frais_de_port__c,
                                                Montant_total_de_la_commande__c,
                                                Description,
                                                (SELECT ID, 
                                                        OrderItemNumber, 
                                                        UnitPrice, 
                                                        Eco_contribution__c, 
                                                        TotalPrice,
                                                        Product2.Name, 
                                                        Quantity, 
                                                        PricebookEntry.reference_fournisseur__c,
                                                        Product2.ProductCode,
                                                        Product2.Reference_article__c , 
                                                        Product2.Designation_article__c, 
                                                        Product2.Lot__c,  
                                                        Product2.Famille_d_articles__c
                                                FROM OrderItems 
                                                ORDER BY Product2.Reference_article__c ASC)
                                        FROM Order
                                        WHERE Id = :orderid ]);

            if(lstOrder.size() >0){
                theOrder = lstOrder[0];
                lstOItems = theOrder.OrderItems;
                System.debug('**** theOrder ' + theOrder);
                System.debug('**** theOrder.OrderItems ' + lstOItems);
            } 

            // SBH 17012020 retrieve custom setting pour affichage No client CT-1203
            noClient = '';
            hasNoClient =false;
            //retrieve custom settings
            if(theOrder.AccountId != null && theOrder.Agence__c != null && theOrder.Agence__r.Agency_Code__c != null){
                List<MappingAgenceFourn__c> lstMappingAgence= [SELECT Code_Agence__c,CreatedById,CreatedDate,Id,ID_Fournisseur__c,IsDeleted,LastModifiedById,LastModifiedDate,Mapping__c,Name,SetupOwnerId,SystemModstamp 
                        FROM MappingAgenceFourn__c
                        WHERE ID_Fournisseur__c = :theOrder.AccountId
                        AND Code_Agence__c = :theOrder.Agence__r.Agency_Code__c
                ];

                System.debug('## listMappingAgence : ' + lstMappingAgence);

                if(!lstMappingAgence.isEmpty()){
                    noClient = lstMappingAgence[0].Mapping__c;
                    hasNoClient = true;
                }
            }

            System.debug('##no client: ' + noClient + ' ' + hasNoClient);
            

        }
    }  
    public static ContentDocumentLink saveVF(String oId) {
        system.debug('## starting method saveVF');
        PageReference pdf = Page.VFP39_OrderItemPDF;
        Blob body;
        Order o = [
            SELECT Id, OrderNumber 
            FROM Order 
            WHERE Id = :oId
        ];
        pdf.getParameters().put('id',oId);
        body = Test.isRunningTest() ? blob.valueOf('Unit.Test') : pdf.getContent(); //testMethods do not support getContent call
        //body =  pdf.getContent();
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
        conVer.PathOnClient = 'PDF Commande fournisseur.pdf'; // The files name, extension is very important here which will help the file in preview.
        
        //get current user time: 
        Datetime now = Datetime.now();
        Integer offset = UserInfo.getTimezone().getOffset(now);
        Datetime local = now.addSeconds(offset/1000);

        conVer.Title = o.OrderNumber + '_' + local +'.pdf';// Display name of the files
        conVer.VersionData = body; // File body in blob; else needs convertion using EncodingUtil.base64Decode(body);
        insert conVer;    //Insert ContentVersion

        // First get the Content Document Id from ContentVersion Object
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        //create ContentDocumentLink  record 
        ContentDocumentLink conDocLink = New ContentDocumentLink();
        conDocLink.LinkedEntityId = oId; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
        conDocLink.ContentDocumentId = conDoc;  //ContentDocumentId Id from ContentVersion
        conDocLink.shareType = 'V';
        insert conDocLink;
        return conDocLink;

    }
    @AuraEnabled
    public static map<string,Object> checkIfActive(String oId){
        system.debug('## starting method checkIfActive');

        map<String,Object> mapOfResult = new map<String,Object>{'error' => true};
        List<Order> lstOrders = [SELECT Id, StatusCode FROM Order WHERE ID = :oId LIMIT 1];
        System.debug('## lstOrders.size(): ' +lstOrders.size());
        
        if(lstOrders.size() >0 && lstOrders[0].StatusCode =='Activated'){            
            ContentDocumentLink cdl = saveVF(oId);
            mapOfResult.put('error', false);
            mapOfResult.put('content_doc_id', cdl.ContentDocumentId);  
        }  
        return  mapOfResult; 
    }
}