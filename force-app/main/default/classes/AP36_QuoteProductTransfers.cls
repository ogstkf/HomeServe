/**
* @File Name          : ServiceAppointmentTriggerHandler.cls
* @Description        : Handler class for ServceAppointment trigger
* @Author             : SBH
* @Group              : Spoon Consulting
* @Last Modified By   : RRJ
* @Last Modified On   : 5/13/2020, 1:55:14 PM
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author                    Modification
*==============================================================================
* 1.0    05/11/2019, 17:18:17          SBH                Initial Version (CT-1086)
**/
public with sharing class AP36_QuoteProductTransfers {

    public static void createProductRequests(List<Quote> lstQuotes, Map<Id, Id> mapQuoteToAgency ){
        System.debug('###Create product requests start');

        //retrieve quote line Items:

        System.debug('##lstquotes: ' + lstQuotes);
        System.debug('getProduct2RecordTypeID(\'Article\') ' + getProduct2RecordTypeID('Article'));
        List<Quote> lstQuoteSelected = [
            SELECT Id, Ordre_d_execution__c, Date_de_debut_des_travaux__c, AccountId, Agency__c,
            (
                SELECT Id, Quantity, Product2Id, Quote.Ordre_d_execution__c
                FROM QuoteLineItems
                WHERE QuoteId IN :lstQuotes
                AND (Product2.RecordTypeId = :getProduct2RecordTypeID('Product') OR (
                Product2.RecordTypeId = :getProduct2RecordTypeID('Article')
                AND (
                    Product2.Famille_d_articles__c = 'Accessoires'
                    OR Product2.Famille_d_articles__c = 'Appareils'
                    OR Product2.Famille_d_articles__c = 'Consommables'
                    OR Product2.Famille_d_articles__c = 'Pièces détachées'
                )
                )
                )     
            )
            FROM Quote
            WHERE Id IN :lstQuotes
            AND RecordType.DeveloperName = 'Devis_standard'
        ];

        List<QuoteLineItem> lstQlis = [
            SELECT Id, Quantity, Product2Id, Quote.Ordre_d_execution__c
            FROM QuoteLineItem
            WHERE QuoteId IN :lstQuotes
            AND (Product2.RecordTypeId = :getProduct2RecordTypeID('Product') OR
			(            
            Product2.RecordTypeId = :getProduct2RecordTypeID('Article')
            AND (
                Product2.Famille_d_articles__c = 'Accessoires'
                OR Product2.Famille_d_articles__c = 'Appareils'
                OR Product2.Famille_d_articles__c = 'Consommables'
                OR Product2.Famille_d_articles__c = 'Pièces détachées'
            )
            )
            )
            AND Quote.RecordType.DeveloperName = 'Devis_standard'
        ];
        //end retrieve QLIs

        System.debug('ls');
        //retrive Location start
        List<ServiceTerritoryLocation> lstServTerrLoc = [
            SELECT Id, LocationId, ServiceTerritoryId
            FROM ServiceTerritoryLocation
            WHERE ServiceTerritoryId IN :mapQuoteToAgency.values()
            AND Location.LocationType = 'Entrepôt'
            AND Location.ParentLocationId = null
        ];

        Map<Id, Id> mapAgencyToLocation =  new Map<Id, Id>();
        Set<Id> setLocations = new Set<Id>();
        Map<String, ProductItem> mapProductLocation_Pi = new Map<string, ProductItem>();
        Map<Id, ProductRequest> mapQuoteToProductRequest = new Map<Id, ProductRequest>();
        List<ProductRequestLineItem> lstPrli = new List<ProductRequestLineItem>();

        for(ServiceTerritoryLocation servTerrLoc: lstServTerrLoc){
            mapAgencyToLocation.put(servTerrLoc.ServiceTerritoryId, servTerrLoc.LocationId);
            setLocations.add(servTerrLoc.LocationId);
        }
        //retrieve locastions end

        System.debug('setLocations: ' +setLocations);
        //Retrieve product items:
        // SH - 2020-01-22 (added Quantite_allouee__c)
        List<ProductItem> lstProdItems = [
            SELECT Id, Product2Id, LocationId, TECH_ProductLocation__c, QuantityOnHand, Quantite_allouee__c 
            FROM ProductItem
            WHERE LocationId IN: setLocations
        ];

        for(ProductItem pi : lstProdItems){
            // SH - One should be enough
            //if(pi.LocationId != null && pi.LocationId != null){
            if(pi.LocationId != null){
                mapProductLocation_Pi.put(pi.TECH_ProductLocation__c, pi);
            }
        }

        System.debug('## mapProductLocation to PI  ;' + mapProductLocation_Pi);
        //end retrieve product items


        //Loop through quotes
        for(Quote q : lstQuoteSelected){
            System.debug('## quotes Selected: ' + q);

            //create product request
            ProductRequest pr = new ProductRequest(
                WorkOrderId = q.Ordre_d_execution__c,
                NeedByDate = q.Date_de_debut_des_travaux__c != null ?  getNeedByDate(q.Date_de_debut_des_travaux__c) : null,
                AccountId = q.AccountId
            );

            mapQuoteToProductRequest.put(q.Id, pr);
        }//end loop qli

        if(!mapQuoteToProductRequest.isEmpty()){
            insert mapQuoteToProductRequest.values();
        }
        System.debug('mapQuoteToProductRequest: ' + mapQuoteToProductRequest);

        List<ProductItem> lstPItoUpdate = new List<ProductItem>(); // SH - 2020-01-22
        List<Quote> lstQuoToUpdt = new List<Quote>();
        //inserting PRLIs
        for(Quote q : lstQuoteSelected){
            System.debug('##Q @: ' + q);
            if(!q.QuoteLineItems.isEmpty()){
                for(QuoteLineItem qli : q.QuoteLineItems){
                    System.debug('##Processing QLI: '+ qli );

                    ProductRequestLineItem prli = new ProductRequestLineItem(
                        ParentId = mapQuoteToProductRequest.get(q.Id).Id,
                        AccountId = q.AccountId,
                        NeedByDate = (mapQuoteToProductRequest.get(q.Id) != null && mapQuoteToProductRequest.get(q.Id).NeedByDate != null) ? mapQuoteToProductRequest.get(q.Id).NeedByDate : null,
                        WorkOrderId =  q.Ordre_d_execution__c,
                        QuantityRequested = qli.Quantity,
                        Product2Id = qli.Product2Id,
                        Agence__c = q.Agency__c,
                        Quote__c = q.Id,
                        Status = null
                    );

                    // System.debug('## Product and location = ' +  prli.Product2Id + '_' + mapAgencyToLocation.get(q.Agency__c));
                    // String relatedLocation = mapAgencyToLocation.get(q.Agency__c) != null && mapAgencyToLocation.containsKey(q.Agency__c) ? mapAgencyToLocation.get(q.Agency__c) : '';
                    // ProductItem relatedPi =  mapProductLocation_Pi.get(prli.Product2Id + '_' + relatedLocation);
                    // if(relatedPi != null){
                    //     if (relatedPi.Quantite_allouee__c == null) { relatedPi.Quantite_allouee__c = 0; } // SH - 2020-01-22
                    //     if((relatedPi.QuantityOnHand - relatedPi.Quantite_allouee__c) >= qli.Quantity){  // SH - 2020-01-22
                    //         prli.Status = 'Réservé à préparer';
                    //         prli.SourceLocationId = mapAgencyToLocation.get(q.Agency__c);
                    //         relatedPi.Quantite_allouee__c += prli.QuantityRequested; // SH - 2020-01-22
                    //         lstPItoUpdate.add(relatedPi); // SH - 2020-01-22
                    //     }else{
                    //         prli.Status = 'A commander';
                    //         prli.DestinationLocationId = mapAgencyToLocation.get(q.Agency__c);
                    //     }
                    // }else{
                    //     prli.Status = 'A commander';
                    //     prli.DestinationLocationId = mapAgencyToLocation.get(q.Agency__c);
                    // }

                    lstPrli.add(prli);
                }
            }else{
                q.A_planifier__c = true;
                lstQuoToUpdt.add(q);
            }
        }

        // if (!lstPItoUpdate.isEmpty()) { // SH - 2020-01-22
        //     update lstPItoUpdate;
        // }

        if(!lstPrli.isEmpty()){
            insert lstPrli;
        }else if(!lstQuoToUpdt.isEmpty()){
            update lstQuoToUpdt;
        }

        System.debug('###Create product requests end');
    }

    // Record type for product
    public static String getProduct2RecordTypeID(String devName){
        return Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
    }

    public static Date getNeedByDate(Date inputDate){

        Integer numdays = 0;
        Datetime inpDate = inputDate;
        while(numdays != 2){
            inpDate = inpDate.addDays(-1);

            String day = inpDate.format('E');
            System.debug('Day: ' + day);
            if(!(day.equalsIgnoreCase('Sat') || day.equalsIgnoreCase('Sun'))){
                numdays++;
            }

        }

        Date dateToreturn = Date.newInstance(inpDate.year(), inpDate.month(), inpDate.day());

        return dateToreturn;
    }


    public static void createProductRequestLineItem(List<QuoteLineItem> lineItems, Map<Id, Id> mapQuoteToAgency){

        ///SBH retour 20200205 1086 start
        //retrive Location start
        List<ServiceTerritoryLocation> lstServTerrLoc = [
            SELECT Id, LocationId, ServiceTerritoryId
            FROM ServiceTerritoryLocation
            WHERE ServiceTerritoryId IN :mapQuoteToAgency.values()
            AND Location.LocationType = 'Entrepôt'
            AND Location.ParentLocationId = null
        ];

        Map<Id, Id> mapAgencyToLocation =  new Map<Id, Id>();
        Set<Id> setLocations = new Set<Id>();
        Map<String, ProductItem> mapProductLocation_Pi = new Map<string, ProductItem>();
        Map<Id, ProductRequest> mapQuoteToProductRequest = new Map<Id, ProductRequest>();
        List<ProductRequestLineItem> lstPrli = new List<ProductRequestLineItem>();

        for(ServiceTerritoryLocation servTerrLoc: lstServTerrLoc){
            mapAgencyToLocation.put(servTerrLoc.ServiceTerritoryId, servTerrLoc.LocationId);
            setLocations.add(servTerrLoc.LocationId);
        }
        //retrieve locastions end

        System.debug('setLocations: ' +setLocations);
        //Retrieve product items:
        // SH - 2020-01-22 (added Quantite_allouee__c)
        List<ProductItem> lstProdItems = [
            SELECT Id, Product2Id, LocationId, TECH_ProductLocation__c, QuantityOnHand, Quantite_allouee__c 
            FROM ProductItem
            WHERE LocationId IN: setLocations
        ];

        for(ProductItem pi : lstProdItems){
            // SH - One should be enough
            //if(pi.LocationId != null && pi.LocationId != null){
            if(pi.LocationId != null){
                mapProductLocation_Pi.put(pi.TECH_ProductLocation__c, pi);
            }
        }

        System.debug('## mapProductLocation to PI  ;' + mapProductLocation_Pi);
        //end retrieve product items
        List<ProductItem> lstPItoUpdate = new List<ProductItem>(); // SH - 2020-01-22

        ///SBH retour 20200205 1086 end

        System.debug('Entering createProductRequestLineItem');
        System.debug('lineItems:' + lineItems);
        List<Id> product2Ids = new List<Id>();
        List<Product2> products = new List<Product2>();
        if(lineItems.size() > 0){
            for(QuoteLineItem qol : lineItems){
                product2Ids.add(qol.Product2Id);
            }
            products = [SELECT Id, Name,Famille_d_articles__c, RecordType.Name, RecordType.DeveloperName FROM Product2 WHERE Id IN: product2Ids];
            Map<Id,Product2> mapProduct2Id = new Map<Id,Product2>();
            for(Product2 p : products){
                mapProduct2Id.put(p.Id, p);
            }
            QuoteLineItem firstItem = lineItems.get(0);
            System.debug('firstItem:' + firstItem);
            System.debug('Get workorder associated to quote associated to QuoteLineItem');
            Quote q = [SELECT Id, Status, OpportunityId, AccountId,Ordre_d_execution__c, Agency__c FROM Quote WHERE Id=: firstItem.QuoteId];

            List<WorkOrder> wos = [SELECT Id, Quote__c FROM WorkOrder WHERE Id =: q.Ordre_d_execution__c];
            System.debug('Wo:' + wos);
            if(wos.size() > 0){
                WorkOrder wo = wos.get(0);
                System.debug('Get Quote associated to LineItem');

                System.debug('Found Quote:' + q);
                System.debug('Get opp associated to associated quote');
                Opportunity opp = [SELECT Id, APP_intervention_immediate__c  FROM Opportunity WHERE Id =: q.OpportunityId];
                System.debug('Opportunity found:' + opp);
                System.debug('Check correct status of quote and APP_intervention_immediate__c on OPP');
                Boolean isValid = q.Status != null && q.Status.equals('Validé, signé - en attente d\'intervention') && opp != null && !opp.APP_intervention_immediate__c;
                if(Test.isRunningTest()){
                    isValid = true;
                }
                if(isValid){
                    if(wo != null){
                        System.debug('Get already created ProductRequest associated to WorkOrder associated to Quoted associated to QuoteLineItem');
                        List<ProductRequest> prs = [SELECT Id, WorkOrderId, NeedByDate FROM ProductRequest WHERE WorkOrderId=: wo.Id];
                        ProductRequest pr = null;
                        if(prs.size() > 0){
                            pr = prs.get(0);
                        }
                        System.debug('PRS:' + prs);
                        if(pr != null){
                            List<ProductRequestLineItem> toInsert = new List<ProductRequestLineItem>();
                            for(QuoteLineItem qli : lineItems){
                                
                                Product2 p = mapProduct2Id.get(qli.Product2Id);
                                String rc = p.RecordType.DeveloperName;
                                String famille = p.Famille_d_articles__c;
                                System.debug('rcname '  + rc);
                                System.debug('famille '  + famille);
                                Boolean isProductValid = ((rc.equals('product')) || (rc.equals('Article') && famille != null &&  (famille.equals('Accessoires') || famille.equals('Appareils') || famille.equals('Consommables') || famille.equals('Pièces détachées'))));
                                
                                if(Test.isRunningTest()){
                                    isProductValid = true;
                                }
                                if(isProductValid){
                                    ProductRequestLineItem prli = new ProductRequestLineItem(
                                        ParentId=pr.Id,
                                        AccountId = q.AccountId,
                                        NeedByDate = pr.NeedByDate,//(mapQuoteToProductRequest.get(q.Id) != null && mapQuoteToProductRequest.get(q.Id).NeedByDate != null) ? mapQuoteToProductRequest.get(q.Id).NeedByDate : null,
                                        WorkOrderId =  wo.Id,
                                        QuantityRequested = qli.Quantity,
                                        Product2Id = qli.Product2Id,
                                        Agence__c = q.Agency__c,
                                        Quote__c = q.Id,
                                        Status = null
                                    );

                                    // // SBH: Start
                                    // System.debug('## Product and location = ' +  prli.Product2Id + '_' + mapAgencyToLocation.get(q.Agency__c));
                                    // String relatedLocation = mapAgencyToLocation.get(q.Agency__c) != null && mapAgencyToLocation.containsKey(q.Agency__c) ? mapAgencyToLocation.get(q.Agency__c) : '';
                                    // ProductItem relatedPi =  mapProductLocation_Pi.get(prli.Product2Id + '_' + relatedLocation);
                                    // if(relatedPi != null){
                                    //     if (relatedPi.Quantite_allouee__c == null) { relatedPi.Quantite_allouee__c = 0; } // SH - 2020-01-22
                                    //     if((relatedPi.QuantityOnHand - relatedPi.Quantite_allouee__c) >= qli.Quantity){  // SH - 2020-01-22
                                    //         prli.Status = 'Réservé à préparer';
                                    //         prli.SourceLocationId = mapAgencyToLocation.get(q.Agency__c);
                                    //         relatedPi.Quantite_allouee__c += prli.QuantityRequested; // SH - 2020-01-22
                                    //         lstPItoUpdate.add(relatedPi); // SH - 2020-01-22
                                    //     }else{
                                    //         prli.Status = 'A commander';
                                    //         prli.DestinationLocationId = mapAgencyToLocation.get(q.Agency__c);
                                    //     }
                                    // }else{
                                    //     prli.Status = 'A commander';
                                    //     prli.DestinationLocationId = mapAgencyToLocation.get(q.Agency__c);
                                    // }
                                    // SBH: End 
                                    toInsert.add(prli);
                                }
                            }
                            System.debug('toInsert:' + toInsert);
                            if(toInsert.size() > 0)
                                insert toInsert;

                            // if (!lstPItoUpdate.isEmpty()) { // SH - 2020-01-22
                            //     update lstPItoUpdate;
                            // }
                        }else{
                            System.debug('There is no ProductRequest associated to workOrder:' + wo.Id);
                        }
                    }
                }else{
                    System.debug('Either status of Quote is not Validé, signé - en attente d\'intervention or there is no opp associated to quote or opp.APP_intervention_immediate__c is not false');
                }
            }else{
                System.debug('No workorder found for Quote:' + firstItem.QuoteId);
            }
        }
    }

    public static void PrliACommander(Set<Id> setPrliId, Set<Id> setAgenceId){

        List<ServiceTerritoryLocation> lstServTerrLocation = [SELECT Id,  LocationId, Location.Name, ServiceTerritoryId, Location.LocationType FROM ServiceTerritoryLocation WHERE ServiceTerritoryId IN :setAgenceId AND Location.LocationType = 'Entrepôt'];

        List<ProductRequestLineItem> lstPrlitoUpdt = new List<ProductRequestLineItem>();

        for(ProductRequestLineItem prl : [SELECT Id, DestinationLocationId FROM ProductRequestLineItem WHERE Id IN :setPrliId]){
            prl.DestinationLocationId = lstServTerrLocation[0].LocationId;
            prl.SourceLocationId = null;
            lstPrlitoUpdt.add(prl);
        }

        if(lstPrlitoUpdt.size() > 0){
            Update lstPrlitoUpdt;
        }
    }

    public static void PrliReservePrepare(Set<Id> setPrliId, Set<Id> setAgenceId){
        System.debug('Enter PrliReservePrepare');

        List<ServiceTerritoryLocation> lstServTerrLoc = [SELECT Id, LocationId, ServiceTerritoryId FROM ServiceTerritoryLocation WHERE ServiceTerritoryId IN :setAgenceId AND Location.LocationType = 'Entrepôt'];
        List<ProductRequestLineItem> lstPrlitoUpdt = new List<ProductRequestLineItem>();
        // List<ProductItem> lstPItoUpdate = new List<ProductItem>();
        Map<Id, ProductItem> mapPiToUpdate = new Map<Id, ProductItem>();

        Map<Id, Id> mapAgencyToLocation =  new Map<Id, Id>();
        Set<Id> setLocations = new Set<Id>();
        Map<String, ProductItem> mapProductLocation_Pi = new Map<string, ProductItem>();
        Map<Id, ProductRequest> mapQuoteToProductRequest = new Map<Id, ProductRequest>();
        Set<Id> setQuoteId = new Set<Id>();
        Set<Id> setProdId = new Set<Id>();
        List<ProductRequestLineItem> lstPrli = [SELECT Id, DestinationLocationId, SourceLocationId, Product2Id, Quote__c, QuantityRequested FROM ProductRequestLineItem WHERE Id IN :setPrliId];

        for(ProductRequestLineItem prl : lstPrli){
            setQuoteId.add(prl.Quote__c);
            setProdId.add(prl.Product2Id);
        }

        // CLA 13/01/22 - TEC-835 - executer cette logique si ce n'est pas un contract collectif
        Quote q = [SELECT Id, Status, OpportunityId, AccountId,Ordre_d_execution__c, Agency__c, Devis_Collectif__c FROM Quote WHERE Id IN :setQuoteId AND RecordType.DeveloperName = 'Devis_standard' LIMIT 1];
        
        if(!q.Devis_Collectif__c){
            QuoteLineItem qli = [SELECT Id, QuoteId, Product2Id, Quantity FROM QuoteLineItem WHERE Product2Id IN :setProdId AND QuoteId = :q.Id LIMIT 1];
        }

        for(ServiceTerritoryLocation servTerrLoc: lstServTerrLoc){
            mapAgencyToLocation.put(servTerrLoc.ServiceTerritoryId, servTerrLoc.LocationId);
            setLocations.add(servTerrLoc.LocationId);
        }        

        List<ProductItem> lstProdItems = [SELECT Id, Product2Id, LocationId, TECH_ProductLocation__c, QuantityOnHand, Quantite_allouee__c FROM ProductItem WHERE LocationId IN: setLocations];

        for(ProductItem pi : lstProdItems){
            if(pi.LocationId != null){
                mapProductLocation_Pi.put(pi.TECH_ProductLocation__c, pi);
            }
        }

        for(ProductRequestLineItem prl : lstPrli){
            String relatedLocation = mapAgencyToLocation.containsKey((Id)q.Agency__c) ? mapAgencyToLocation.get((Id)q.Agency__c) : '';
            ProductItem relatedPi =  mapProductLocation_Pi.get(prl.Product2Id + '_' + relatedLocation);

            if(relatedPi != null){
                if (relatedPi.Quantite_allouee__c == null) { relatedPi.Quantite_allouee__c = 0; } 
                if((relatedPi.QuantityOnHand - relatedPi.Quantite_allouee__c) >= prl.QuantityRequested){  
                    //prl.Status = 'Réservé à préparer';
                    prl.SourceLocationId = mapAgencyToLocation.get(q.Agency__c);
                    prl.DestinationLocationId = null;
                    relatedPi.Quantite_allouee__c += prl.QuantityRequested; 
                    // lstPItoUpdate.add(relatedPi); 
                    mapPiToUpdate.put(relatedPi.Id, relatedPi);
                }else{
                    prl.SourceLocationId = mapAgencyToLocation.get(q.Agency__c);
                    prl.DestinationLocationId = null;
                    relatedPi.Quantite_allouee__c = relatedPi.QuantityOnHand; 
                    // lstPItoUpdate.add(relatedPi);
                    mapPiToUpdate.put(relatedPi.Id, relatedPi);
                    //prl.Status = 'A commander';
                    //prl.DestinationLocationId = mapAgencyToLocation.get(q.Agency__c);
                }
            }else{
                prl.SourceLocationId = lstServTerrLoc[0].LocationId;
                prl.DestinationLocationId = null;
                // prl.Status = 'A commander';
                // prl.DestinationLocationId = mapAgencyToLocation.get(q.Agency__c);
            }
            lstPrlitoUpdt.add(prl);
        }

        if(lstPrlitoUpdt.size() > 0){
            Update lstPrlitoUpdt;
        }

        if(mapPiToUpdate.size() > 0){
            Update mapPiToUpdate.values();
        }
    }

    public static void ResPrepareElse(Set<Id> setPrliId, Set<Id> setAgenceId){
        System.debug('Enter ResPrepareElse');

        List<ServiceTerritoryLocation> lstServTerrLoc = [SELECT Id, LocationId, ServiceTerritoryId FROM ServiceTerritoryLocation WHERE ServiceTerritoryId IN :setAgenceId AND Location.LocationType = 'Entrepôt'];
        List<ProductRequestLineItem> lstPrlitoUpdt = new List<ProductRequestLineItem>();
        // List<ProductItem> lstPItoUpdate = new List<ProductItem>();
        Map<Id, ProductItem> mapPiToUpdate = new Map<Id, ProductItem>();

        Map<Id, Id> mapAgencyToLocation =  new Map<Id, Id>();
        Set<Id> setLocations = new Set<Id>();
        Map<String, ProductItem> mapProductLocation_Pi = new Map<string, ProductItem>();
        Map<Id, ProductRequest> mapQuoteToProductRequest = new Map<Id, ProductRequest>();
        Set<Id> setQuoteId = new Set<Id>();
        Set<Id> setProdId = new Set<Id>();
        List<ProductRequestLineItem> lstPrli = [SELECT Id, DestinationLocationId, SourceLocationId, Product2Id, Quote__c, QuantityRequested FROM ProductRequestLineItem WHERE Id IN :setPrliId];

        for(ProductRequestLineItem prl : lstPrli){
            setQuoteId.add(prl.Quote__c);
            setProdId.add(prl.Product2Id);
        }

        // CLA 13/01/22 - TEC-835 - executer cette logique si ce n'est pas un contract collectif
        Quote q = [SELECT Id, Status, OpportunityId, AccountId,Ordre_d_execution__c, Agency__c, Devis_Collectif__c FROM Quote WHERE Id IN :setQuoteId AND RecordType.DeveloperName = 'Devis_standard' LIMIT 1]; 
        if(!q.Devis_Collectif__c){
            QuoteLineItem qli = [SELECT Id, QuoteId, Product2Id, Quantity FROM QuoteLineItem WHERE Product2Id IN :setProdId AND QuoteId = :q.Id LIMIT 1];
        }

        // DMU 20200915 - Commented above query  to correct error attempt to dereference null obj - added in a list in code below
        // list<QuoteLineItem> lstQLI = [SELECT Id, QuoteId, Product2Id, Quantity FROM QuoteLineItem WHERE Product2Id IN :setProdId AND QuoteId = :q.Id LIMIT 1];
        // QuoteLineItem qli = lstQLI.size()>0 ? lstQLI[0] : null;

        for(ServiceTerritoryLocation servTerrLoc: lstServTerrLoc){
            mapAgencyToLocation.put(servTerrLoc.ServiceTerritoryId, servTerrLoc.LocationId);
            setLocations.add(servTerrLoc.LocationId);
        }

        List<ProductItem> lstProdItems = [SELECT Id, Product2Id, LocationId, TECH_ProductLocation__c, QuantityOnHand, Quantite_allouee__c FROM ProductItem WHERE LocationId IN: setLocations];

        for(ProductItem pi : lstProdItems){
            if(pi.LocationId != null){
                mapProductLocation_Pi.put(pi.TECH_ProductLocation__c, pi);
            }
        }

        for(ProductRequestLineItem prl : lstPrli){
            
            String relatedLocation = mapAgencyToLocation.containsKey(q.Agency__c) ? (mapAgencyToLocation.get(q.Agency__c) != null && mapAgencyToLocation.containsKey(q.Agency__c) ? mapAgencyToLocation.get(q.Agency__c) : '') : '';
            ProductItem relatedPi =  mapProductLocation_Pi.get(prl.Product2Id + '_' + relatedLocation);
            
            if(relatedPi != null){
                if (relatedPi.Quantite_allouee__c == null) { relatedPi.Quantite_allouee__c = 0; } 

                // CLA 13/01/22 - TEC-835
                // if(relatedPi.Quantite_allouee__c - qli.Quantity  >= 0 ){  
                if(relatedPi.Quantite_allouee__c - prl.QuantityRequested  >= 0 ){ 

                    // prl.Status = 'Réservé à préparer';                    
                    relatedPi.Quantite_allouee__c -= prl.QuantityRequested;                     
                    // lstPItoUpdate.add(relatedPi);
                    mapPiToUpdate.put(relatedPi.Id, relatedPi);
                }
            }
        }
        system.debug('##### mapPiToUpdate:'+mapPiToUpdate);
        if(mapPiToUpdate.size() > 0){
            update mapPiToUpdate.values();
        }
    }
}