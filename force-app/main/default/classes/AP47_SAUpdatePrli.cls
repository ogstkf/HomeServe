/**
 * @File Name          : AP47_SAUpdatePrli.cls
 * @Description        : 
 * @Author             : SBH
 * @Group              : 
 * @Last Modified By   : SBH
 * @Last Modified On   : 18/11/2019, 10:28:30
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/11/2019         SBH                    Initial Version  (CT-1192)
**/
public with sharing class AP47_SAUpdatePrli {

    public static void updateNeedByDate(List<ServiceAppointment> lstSas, Map<Id, ServiceAppointment> mapWoToSa){

        System.debug('## AP47 start UpdateNeedByDate: ');
        //select PRLIs for the Work order;
        System.debug('## AP47 mapWoToSA: ' + mapWoToSa);
        List<ProductRequestLineItem> lstPrli = [
            SELECT Id, Status, WorkOrderId
            FROM ProductRequestLineItem 
            WHERE WorkOrderId = :mapWoToSa.keySet()
        ];

        List<ProductRequestLineItem> lstPrliToUpdate = new List<ProductRequestLineItem>();

        for(ProductRequestLineItem prli : lstPrli){
            System.debug('## Curr PRLI: ' + prli);
            if(prli.Status == AP_Constant.PRLIStatutReservePrep){
                if(mapWoToSa.get(prli.WorkOrderId).SchedStartTime != null){
                    prli.NeedByDate = mapWoToSa.get(prli.WorkOrderId).SchedStartTime;
                    lstPrliToUpdate.add(prli);
                }
            }
            System.debug('## End prli loop');
        }

        if(!lstPrliToUpdate.isEmpty()){
            update lstPrliToUpdate;
        }
        System.debug('## lstPrliToUpdate: ' + lstPrliToUpdate);
    }
}