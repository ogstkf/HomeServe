/**
 * @File Name          : AP15_AttachCasePdfToAttachment.cls
 * @Description        : 
 * @Author             : ANA
 * @Group              : 
 * @Last Modified By   : ANA
 * @Last Modified On   : 01/10/2019, 16:56:00
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    01/10/2019   ANA     Initial Version
**/
public with sharing class AP15_AttachCasePdfToAttachment {

    @future (callout=true)
    public static void AttachToCase(List<Id> lstCase){
        
        List<Attachment> lstAttachment = new List<Attachment>();

        for(Id casId : lstCase){
            //Generate content of PDF fron VF page
            PageReference template = new PageReference('/apex/VFP08_AnomalieA1A2');
            template.getParameters().put('id', casId);
            Blob attData = Blob.valueOf('');
            if(!Test.isRunningTest()) 
            attData = template.getContentAsPDF();

            //create attachments to insert with task
            Attachment att = new Attachment(
                Body = attData,
                Name = 'AnomalieA1A2'+casId,
                ParentId = casId,
                ContentType = 'application/pdf'
            );

            lstAttachment.add(att);
        }

        if(lstAttachment.size() > 0) {
            insert lstAttachment;
        }
    }
}