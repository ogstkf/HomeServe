/**
 * @File Name          : VFC10_Devis_TEST.cls
 * @Description        : TestClass
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 10-23-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         09-12-2019     		    DMG         Initial Version
**/
@isTest
public with sharing class VFC10_Devis_TEST {
 static User mainUser;
    static Account testAcc = new Account();
    static Opportunity opp = new Opportunity();
    static sofactoapp__Compte_auxiliaire__c sofCmptAux;
    static sofactoapp__Compte_comptable__c sofCoomptable;
    static WorkOrder wrkOrd = new WorkOrder();
    static list<Product2> lstProd = new list<Product2>();
    static Schema.Location loc = new Schema.Location();
    static OperatingHours oh = new OperatingHours();
    static sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c();
	static ServiceTerritory st = new ServiceTerritory();
	static ServiceTerritoryLocation stl = new ServiceTerritoryLocation();    
    static Quote quo = new Quote();
    static Quote quo1 = new Quote();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static list<QuoteLineItem> lstQli = new list<QuoteLineItem>();
	static ProductItem pi = new ProductItem(); 
    static ProductRequestLineItem prli = new ProductRequestLineItem();
    static List<AgencyAccreditation__c> lstAAccr = new List<AgencyAccreditation__c>();
    
    static{
        
        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){
            
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.PersonMobilePhone = '12345678';
           // testAcc.recordtypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Devis_RTC').getRecordTypeId();
            insert testAcc;

            //create sofactoapp__Compte_auxiliaire__c
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //create sofactoapp__Compte_comptable__c
          /* sofCoomptable = new sofactoapp__Compte_comptable__c(
                Name = 'comptable',
                sofactoapp__Libelle__c = 'test'
            );
            insert sofCoomptable;

            sofCoomptable.sofactoapp__Plan_comptable__c = sofCoomptable.Id;
            update sofCoomptable;

            
             sofCmptAux = new sofactoapp__Compte_auxiliaire__c (
            Name = 'test',
            Compte_comptable_Prelevement__c = sofCoomptable.Id
            );
            insert sofCmptAux;

            testAcc.sofactoapp__Compte_auxiliaire__c = sofCmptAux.Id;
            update testAcc;*/

            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);            
            insert opp;

            //opp.AccountId = testAcc.Id;
            //update testAcc;
            
            //work order
            wrkOrd = TestFactory.createWorkOrder();
            insert wrkOrd;
                      
            //create Products
            lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
                    
                /*new Product2(Name='Prod2',
                             Famille_d_articles__c='Appareils',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                    
                new Product2(Name='Prod3', 
                             Famille_d_articles__c='Consommables',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                    
                new Product2(Name='Prod4', 
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())*/
            };
            insert lstProd;
            
            //create Location
            loc = new Schema.Location(Name='Test1',
                                      LocationType='Entrepôt',
                                      ParentLocationId=NULL,
                                      IsInventoryLocation=true);
            insert loc;

			//create Operating Hours
			oh = new OperatingHours(Name='Test1');
			insert oh;

			//create Corporate Name
			sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(Name='Test1',
                                                                                  sofactoapp__Credit_prefix__c='Test1',
                                                                                  sofactoapp__Invoice_prefix__c='Test1');  
            insert srs;
            
            //create Service Territory
            st = new ServiceTerritory(Name='Test1',
                                      OperatingHoursId=oh.Id,
                                      Sofactoapp_Raison_Social__c=srs.Id);
            insert st;
            
            //create Service Territory Location
            stl = new ServiceTerritoryLocation(ServiceTerritoryId=st.Id,
                                               LocationId=loc.Id);
            insert stl;
            
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            insert PrcBkEnt;

            // creating Assets
            // Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, st.Id);
            // eqp.Product2Id = lstProd[0].Id;
            // eqp.Logement__c = st.Id;
            // eqp.AccountId = testAcc.Id;
            // insert eqp;
            
            //create Quote
            quo = new Quote(Name ='Test1',
                            OpportunityId=opp.Id,
                            Agency__c=st.Id,
                            Status = 'In Review',
                            tech_deja_facture__c = true,
                            Pricebook2Id = lstPrcBk[0].Id,
                            Devis_signe_par_le_client__c = true,
                            Date_de_debut_des_travaux__c = Date.today(),
                            Mode_de_paiement_de_l_acompte__c = 'CB',
                            Montant_de_l_acompte_verse__c = 100.3,
                            Ordre_d_execution__c = wrkOrd.Id);
            insert quo;  

           // quo.recordtypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Devis_standard').getRecordTypeId();      
            //update quo;   
            
            //create Quote Line Items
            lstQli = new list<QuoteLineItem>{
            	new QuoteLineItem(QuoteId=quo.Id, 
                                  Product2Id=lstProd[0].Id, 
                                  Quantity=decimal.valueOf(2), 
                                  UnitPrice=decimal.valueOf(145),
                                //   TotalPrice = Quantity * UnitPrice,
                                  PriceBookEntryID=PrcBkEnt.Id,
                                  Remise_en100__c = 10,
                                  Taux_de_TVA__c = 10)
            };
            upsert lstQli;
            
            pi = new ProductItem(Product2Id=lstProd[0].Id,
                                 LocationId=loc.Id,
                                 QuantityOnHand=decimal.valueOf(2));
            insert pi;  

            //create agency acrredation
            lstAAccr = new List<AgencyAccreditation__c>{
                new AgencyAccreditation__c(name = 'Acc1',
                                        Type__c= 'Qualisav',
                                        Equipement_couvert__c = 'Pompe à chaleur air/eau',                             
                                        Valid_until__c= Date.newInstance(2025, 12, 20),                                     
                                        Numero_SGS__c ='12345',
                                        Agency__c = st.Id,
                                        LocalAgency__c = st.Id,
                                        DelegatedAgency__c = null)
            };

            insert lstAAccr ;
        }
        
    }  


	@isTest
	public static void doGenerate(){

		System.runAs(mainUser){
			
			Test.startTest();

				PageReference pageRef = Page.VFP10_Devis; 
				pageRef.getParameters().put('id', quo.Id);
				Test.setCurrentPage(pageRef);

                VFC10_Devis devis = new VFC10_Devis();

	        Test.stopTest();

		}
	}

}