/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 11-04-2020
 * @last modified by  : AMO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   11-04-2020   AMO   Initial Version
**/
public with sharing class VFC13_CGV {
    public List<ServiceTerritory> lstServTerritory {get;set;}
    public ServiceTerritory actualServTerritory {get;set;}

    public VFC13_CGV(){
    System.debug('##Starting VFC13_CGV');

           try{
            string sTerrId = ApexPages.currentPage().getParameters().get('id');
                if(String.isNotBlank(sTerrId)){
                    lstServTerritory = new List<ServiceTerritory>([SELECT Id,
                                                        Name,
                                                        Legal_Form__c,
                                                        Capital_in_Eur__c,
                                                        RCS__c,
                                                        Siren__c,
                                                        street,
                                                        Street2__c,
                                                        PostalCode,
                                                        City,
                                                        Intra_Community_VAT__c,
                                                        Site_web__c,
                                                        Logo_Name_Agency__c
                                                    FROM ServiceTerritory 
                                                    WHERE Id = :sTerrId]);
                    if(lstServTerritory.size()>0){
                        actualServTerritory=lstServTerritory[0];
                    }
                }  
            }catch(exception e){
                system.debug('**** exception msg: '+ e.getMessage());
            }

    }
}