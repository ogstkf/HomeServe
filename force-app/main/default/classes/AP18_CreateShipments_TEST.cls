@isTest
public with sharing class AP18_CreateShipments_TEST {
        /**
 * @File Name          : AP18_CreateShipments_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   :  
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         15-10-2019           LGO         Initial Version
**/
    static User mainUser;
    static Order ord ;
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static  Bypass__c userBypass;
    static OrderItem ordItem = new OrderItem();

    static{
        mainUser = TestFactory.createAdminUser('AP18@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        // userBypass.BypassWorkflows__c = true;
        // insert userBypass;

        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccountBusiness('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            testAcc.Livraison_express_possible__c  = true;
            insert testAcc;

             //create products
            prod = TestFactory.createProduct('testProd');
            insert prod;


            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            // System.debug('##### TEST lstPrcBk: '+lstPrcBk);

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;


            //create Order
            ord = new Order(AccountId = testAcc.Id
                                    ,Name = 'Test1'
                                    ,EffectiveDate = System.today()
                                    ,Status = 'Demande d\'achats'
                                    ,Pricebook2Id = lstPrcBk[0].Id
                                    ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId());

            insert ord;

            ordItem = new OrderItem(
                            OrderId=ord.Id,
                            Quantity=decimal.valueof('1'),
                            PricebookEntryId=PrcBkEnt.Id,
                            UnitPrice = 145);
        
            insert ordItem;

            System.debug('##### TEST ord: '+ord);
        }
    }

    @isTest
    public static void testcreateShipmentsForOrders(){
        System.runAs(mainUser){
            // Order ord1 = [Select Id, Status FROM Order Where Id =: ord.Id];
            ord.Status = 'Accusé de réception fournisseurs';
            ord.Type_de_livraison_de_la_commande__c ='Standard';
            ord.Mode_de_transmission__c = 'Email';
            System.debug('##### TEST ord1: '+ord);
            Test.startTest();
                update ord;
            Test.stopTest();

            // list<Shipment> lstNewShip = [SELECT ShipToName,Id,DeliveredToId FROM Shipment WHERE Id !=:Ship.Id];

            // System.assertEquals(lstNewShip[0].ShipToName,Ship.ShipToName);
            // System.assertEquals(lstNewShip[0].DeliveredToId,Ship.DeliveredToId);

            // list<ProductTransfer> lstNewPT = [SELECT QuantitySent FROM ProductTransfer WHERE ShipmentId=:lstNewShip[0].Id];

            // System.assertEquals(lstNewPT.size(),2);
            // System.assertEquals(lstNewPT[0].QuantitySent,lstPT[0].QuantitySent-lstPT[0].QuantityReceived);
        }
    }
}