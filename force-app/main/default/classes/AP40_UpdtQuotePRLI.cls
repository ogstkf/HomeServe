/**
 * @File Name          : AP40_UpdtQuotePRLI.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 11/11/2019, 09:34:07
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/11/2019   AMO     Initial Version
**/
public with sharing class AP40_UpdtQuotePRLI {

    public static void UpdtPRLI(List<Quote> lstNewQuote){
        System.debug ('Enter AP to update PRLI');

        Map<String, String> mapQuoteWO = new Map<String, String>();
        Set<Id> setProductIds = new Set<Id>();
        Set<Id> setLocationIds = new Set<Id>();

        for(Quote qu: lstNewQuote){
            mapQuoteWO.put(qu.Id, qu.Ordre_d_execution__c);
        }
        //System.debug ('Work order of Quote : ' + mapQuoteWO);

        // SH - 2020-01-23 - Added "Product2Id, DestinationLocationId"
        List<ProductRequestLineItem> lstPrli = [
            SELECT Id, WorkOrderId, Status, Product2Id, DestinationLocationId, QuantityRequested
            FROM ProductRequestLineItem 
            WHERE 
                WorkOrderId IN :mapQuoteWO.values() AND 
                (Status = 'A commander' OR Status = 'Réservé à préparer')];

        for (ProductRequestLineItem prli : lstPrli) {
            setProductIds.add(prli.Product2Id);
            setLocationIds.add(prli.DestinationLocationId);
        }

        // SH - 2020-01-23 - Get ProductItem
        List<ProductItem> lstProductItems = [
            SELECT Id, Quantite_allouee__c, Product2Id, LocationId
            FROM ProductItem
            WHERE
                Product2Id IN :setProductIds AND
                LocationId IN :setLocationIds
        ];

        for (ProductRequestLineItem prli : lstPrli) {
            // SH - 2020-01-23 - BEGIN
            if (prli.Status == 'Réservé à préparer') {
                for (ProductItem pi : lstProductItems) {         
                    if ((prli.Product2Id == pi.Product2Id) && (prli.DestinationLocationId == pi.LocationId)) {
                        pi.Quantite_allouee__c -= prli.QuantityRequested;
                        if (pi.Quantite_allouee__c < 0) { pi.Quantite_allouee__c = 0; }
                    }
                }
            }
            // SH - END

            prli.Status = 'Annulé';
        }

        update lstPrli;
        update lstProductItems; // SH - 2020-01-23
    }
}