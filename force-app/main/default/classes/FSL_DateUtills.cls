public class FSL_DateUtills {
    
    public static String getNameOfMonth(Integer month){
        switch on month {
            when 1 {return 'January';}
            when 2 {return 'February';}
            when 3 {return 'March' ;}
            when 4 {return 'April'; }
            when 5 {return 'May';}
            when 6 {return 'June';}
            when 7 {return 'July';}
            when 8 {return 'August';}
            when 9 {return 'September';}
            when 10 {return 'October';}
            when 11 {return 'November';}
        }
        return 'December';
    }

    public static String dow(Date d){
        DateTime newDate = DateTime.newInstance(d.year(),d.month(),d.day());
        String f = (String)newDate.format('u');
        switch on f {
            when '1' {return 'Monday';}
            when '2' {return 'Tuesday';}
            when '3' {return 'Wednesday' ;}
            when '4' {return 'Thursday'; }
            when '5' {return 'Friday';}
            when '6' {return 'Saturday';}
        }
        return 'Sunday';
    }
}