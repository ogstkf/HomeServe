/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 11-20-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   11-17-2020   ZJO   Initial Version
**/
@isTest
public with sharing class AP76_SetUniqueProductCode_TEST{

    static User adminUser;
    static List<Product2> lstProd = new List<Product2>();

    static{

        adminUser = TestFactory.createAdminUser('TestUser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){
      
            //Create List of Products
            Product2 prod1 = TestFactory.createProduct('TestProd1');
            prod1.ProductCode = '001';
            Product2 prod2 = TestFactory.createProduct('TestProd2');
            prod2.ProductCode = '002';
            Product2 prod3 = TestFactory.createProduct('TestProd3');
            prod3.ProductCode = '003';

            lstProd.add(prod1);
            lstProd.add(prod2);
            lstProd.add(prod3);   
        }
    }

    @isTest
    public static void testInsertProd(){
        System.runAs(adminUser){
            Test.StartTest();
            insert lstProd;
            Test.StopTest();
        }
    }

    @isTest
    public static void testUpdateProd(){
        System.runAs(adminUser){
            insert lstProd;

            Test.StartTest();
            lstProd[0].ProductCode = '001Test';
            update lstProd[0];
            Test.StopTest();
        }
    }
    
    //for code coverage issues
    @isTest
    public static void testCoverageIssue(){
        System.runAs(adminUser){
            insert lstProd;

            Test.StartTest();
           		AP76_SetUniqueProductCode.setProductCode(lstProd);
            Test.StopTest();
        }
    }
}