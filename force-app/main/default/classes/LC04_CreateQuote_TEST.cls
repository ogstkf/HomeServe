/**
 * @File Name          : LC04_CreateQuote_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 22-10-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class LC04_CreateQuote_TEST {
    	/**
 * @File Name          : 
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 22-10-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         02-12-2019     		ANA         Initial Version
**/

    static User mainUser;
    static Case cse;
    static Account acc;
    static WorkOrder wrkOrd;
    static Contact cnt;
    static  List<Account> lstAcc;
    static List<ServiceContract> lstServCon;
    static List<ContractLineItem> lstTestLstCli;
    static List<PriceBookEntry> lstPrcBkEnt;
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static Bypass__c bp = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('LC04CreateQuotw', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
        
            bp.SetupOwnerId  = mainUser.Id;
            bp.BypassValidationRules__c = true;
            bp.BypassWorkflows__c = true;
            bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53,MobileNotificationTrigger,AssignedResourceTrigger,ServiceAppointmentTrigger,AP10,AP74';
            insert bp;

            cnt = new Contact(LastName='Johnny');
            insert cnt;

            acc = TestFactory.createAccount('TEST ANA');
            acc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert acc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(acc.Id);
			acc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update acc;

            lstAcc = [SELECT Name,PersonContactId,recordtypeId, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE Id =:acc.Id];

            Logement__c lgmt = new Logement__c(
                Account__c = acc.Id,
                Street__c = 'New Street'
                );
            insert lgmt;

            //Create your product
            Product2 prod = new Product2(Name = 'Ramonage gaz',
                ProductCode = 'Pro-X',
                isActive = true,
                Equipment_family__c = 'Chaudière',
                //Equipment_type__c = 'Chaudière gaz',
                Equipment_type1__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz,
                Statut__c = 'Approuvée',
                RecordTypeId = AP_Constant.getRecTypeId('Product2', 'Product')
            );
            
            insert prod;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            lstServCon = new List<ServiceContract>{
                            TestFactory.createServiceContract('TestAcc', acc.Id)
            };
            lstServCon[0].Contract_Status__c =  'Active';
            // lstServCon[0].Type__c = 'Collective';
            lstServCon[0].PriceBook2Id = lstPrcBk[0].Id; 
            insert lstServCon;

            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lgmt.Id);
            eqp.Product2Id = prod.Id;
            eqp.AccountId = acc.Id;

            insert eqp;
            

            cse = new case(AccountId = acc.id
                                ,type = 'Draft'
                                ,status='New'
                                ,AssetId = eqp.Id,
                                Service_Contract__c = lstServCon[0].Id);
            cse.recordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            insert cse;

            wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse.id;

            lstPrcBkEnt = new List<PricebookEntry>{
                TestFactory.createPriceBookEntry(standardPricebook.Id, prod.Id, 0)
            };
            insert lstPrcBkEnt;

            lstTestLstCli = new List<ContractLineItem>{
                TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1)
            };
            lstTestLstCli[0].IsActive__c = true;
            lstTestLstCli[0].AssetId = eqp.Id;
            insert lstTestLstCli;

        }
    }

    @isTest
    public static void createQuoteTEST(){
        System.runAs(mainUser){
            Test.startTest();
            System.debug('lstAcc ANA : '+lstAcc);
                LC04_CreateQuote.createQuote(cse,lstAcc[0],null);
                String getServcon =LC04_CreateQuote.getServiceContract(cse);
                Map<Id,String> mapWrkOrder = LC04_CreateQuote.fetchWo(cse.Id);
                Boolean chekEli = LC04_CreateQuote.checkEligibilityTocreateQuote(cse.Id);
            Test.stopTest();

            // List<Opportunity> lstOpp = [SELECT AccountId FROM Opportunity];
            // System.assertEquals(lstOpp[0].AccountId,acc.Id);

            // List<OpportunityContactRole> lstOPC = [SELECT OpportunityId FROM OpportunityContactRole];
            // System.assertEquals(lstOPC[0].OpportunityId,lstOpp[0].Id);

            // List<Quote> lstQuo = [SELECT Status FROM Quote WHERE OpportunityId =:lstOpp[0].Id];
            // System.assertEquals(lstQuo[0].Status,'Draft');

            // System.assertEquals(getServcon,null);

            System.assertEquals(mapWrkOrder.size(),0);

            System.assertEquals(chekEli,true);
        }
    }

    @isTest
    public static void createQuoteTEST2(){
        System.runAs(mainUser){
            Test.startTest();
            cse.type='Troubleshooting';
            update cse;
                LC04_CreateQuote.createQuote(cse,lstAcc[0],null);
                LC04_CreateQuote.createQuoteFromCase(cse.Id,null);
            Test.stopTest();

            // List<Opportunity> lstOpp = [SELECT AccountId FROM Opportunity];
            // System.assertEquals(lstOpp[0].AccountId,acc.Id);

            // List<Quote> lstQuo = [SELECT Status FROM Quote WHERE OpportunityId =:lstOpp[0].Id];
            // System.assertEquals(lstQuo[0].Status,'Draft');
        }
    }
    @isTest
    public static void createQuoteTEST3(){
        System.runAs(mainUser){
            Test.startTest();
            cse.type='Information Request';
            update cse;
                LC04_CreateQuote.createQuote(cse,acc,null);
                
            Test.stopTest();
        }
    }
    @isTest
    public static void createQuoteTEST4(){
        System.runAs(mainUser){
            Test.startTest();
            cse.type='Commissioning';
            update cse;
                LC04_CreateQuote.createQuote(cse,acc,null);
                
            Test.stopTest();
        }
    }
    @isTest
    public static void createQuoteTEST5(){
        System.runAs(mainUser){
            Test.startTest();
            cse.type='Installation';
            update cse;
                LC04_CreateQuote.createQuote(cse,acc,null);
                
            Test.stopTest();
        }
    }
    @isTest
    public static void createQuoteTEST6(){
        System.runAs(mainUser){
            Test.startTest();
            cse.type='Maintenance';
            update cse;
                LC04_CreateQuote.createQuote(cse,acc,null);
                
            Test.stopTest();
        }
    }
    @isTest
    public static void createQuoteTEST7(){
        System.runAs(mainUser){
            Test.startTest();
            cse.type='Various Services';
            update cse;
                LC04_CreateQuote.createQuote(cse,acc,null);
                
            Test.stopTest();
        }
    }
    @isTest
    public static void createQuoteTEST8(){
        System.runAs(mainUser){
            Test.startTest();
            cse.type='Administrative management';
            update cse;
                LC04_CreateQuote.createQuote(cse,acc,null);
                
            Test.stopTest();
        }
    }
    @isTest
    public static void createQuoteTEST9(){
        System.runAs(mainUser){
            Test.startTest();
            cse.type='Claim';
            update cse;
                LC04_CreateQuote.createQuote(cse,acc,null);
                
            Test.stopTest();
        }
    }
}