global  with sharing class AP71_WSDocaposteActualisationDocs {
/**
     * @auteur  RRA (MADA)
     * @version 1.0
     * @depuis  22/05/2020
     * @description Used to View/Add/Download Files 
     *
     * -- Historique de maintenance: 
     * --
     * -- Date         Nom   Version  Commentaire 
     * -- -----------  ----  -------  ------------------------
     * -- 10-06-2020   RRA   1.0      Version initiale
     * -------------------------------------------------------
    */

    @AuraEnabled
    public static GEDResponseWrapper viewFiles(String sObjectId){
        system.debug('MCU sObjectId ' + sObjectId);
        //Map<String, Object> result = new Map<String,Object>();   
        //Http m_http = new Http();
        HttpRequest req = new HttpRequest();
        GEDResponseWrapper request = new GEDResponseWrapper();
        try{
            String endPointContrat = System.Label.endPointSearch;
            HttpResponse response;
            String jSession = idSession();
            Http h = new http();
            HttpRequest reqAuth = new HttpRequest();
            reqAuth.setEndpoint(endPointContrat);
            reqAuth.setMethod('POST');
            //reqAuth.setHeader('Accept', 'application/json;charset=UTF-8');
           // req.setTimeout(20000);
            reqAuth.setHeader('Content-Type', 'application/json;charset=UTF-8');
            //Ici on passe dans les header parameters la valeur retournée précédemment
            reqAuth.setHeader('Cookie',jSession);
            String body = '{"typeCode":["AVR_COMPT","AVI_RENOUV", "RLNCE_1", "MISE_DEM","BP","BPS","PAIE_BIS","AVR","FAC","FAC_BIS","FRF","Paie","RH","FRF"],"requestItemDTOs": [{"fieldCode":"ID_TECH","operation":"EQUAL","value": ["3O000000CzZDQA0"]}]}';
            reqAuth.setBody(body);
            response = h.send(reqAuth);
            System.debug('## reponse status Body : '+response);
            System.debug('## reponse Body: '+response.getBody());
            
            request = (GEDResponseWrapper) JSON.deserialize(response.getBody(), GEDResponseWrapper.class);
            system.debug('## MCU request :' + request);

            
            
        }catch(Exception e){
           system.debug('MCU e.message ' + e.getMessage());
        }
        return request;
    }

      
    @AuraEnabled
    public static String DownloadFiless(String idSFDC, String idDoca, String fileName){
    //public static String DownloadFiless(String strURL){
        // GEDResponseWrapper idDoca = viewFiles (IdSFDC); 
        // for (integer i = 0; i <= idDoca.data.size(); i++) {
            // String strURL = 'https://recette.okoro.demat.pro/1B5CB2/cham/gif/mag/ws/records/'+ idDoca.data[0].STD_RECORD_ID +'/file/attached';
             HttpResponse response;
             String jSession = idSession();
             Http h = new http();
             HttpRequest reqAuth = new HttpRequest();
             reqAuth.setEndpoint('https://recette.okoro.demat.pro/1B5CB2/cham/gif/mag/ws/records/'+ idDoca + '/file/attached');
             reqAuth.setMethod('GET');
             reqAuth.setHeader('Content-Type', 'application/octet-stream;charset=UTF-8');
             reqAuth.setCompressed(true); 
             reqAuth.setHeader('Connection', 'keep-alive');
             reqAuth.setHeader('Cookie', jSession);
             response = h.send(reqAuth);
 
             Blob file = response.getBodyAsBlob();
            // String myBase64String = EncodingUtil.base64Encode(Blob.valueof(response.getBody()));
             System.debug('## reponse status: '+response);
             System.debug('## reponse Body: '+response.getBody());  

             System.debug('### file: ' + file);
             System.debug('### PDF: ' + EncodingUtil.base64Encode(file));

            return EncodingUtil.base64Encode(file);
       
     }
    // Renew Id session to connect WS
    public static String idSession()
    {
        String strURL = System.Label.endPointLogin;
        HttpResponse response;
        String respHeader = '';
        Http h = new http();
        HttpRequest reqAuth = new HttpRequest();
        reqAuth.setEndpoint(strURL);
        reqAuth.setMethod('POST');
        reqAuth.setHeader('Connection', 'keep-alive');
        response = h.send(reqAuth);
        respHeader = response.getHeader('Set-Cookie');
        System.debug('## response status: '+ response);
        System.debug('## responseheader: '+ respHeader);
        return respHeader;
    }

	global class GEDResponseWrapper {
        public Integer count{get;set;}
        @AuraEnabled
        public String searchId{get;set;}
        @AuraEnabled
        public List<columnsStruct> columnsStruct{get;set;}
        @AuraEnabled
        public List<data> data{get;set;}
  }

  global class data{
    public String STD_TORETURNDATE{get;set;}
    public Boolean STD_VITAL{get;set;}
    public String STD_TRANSFER_LABEL{get;set;}
    public String STD_CLASSIFICATION{get;set;}
    public String STD_ARCHIVE_ID{get;set;}
    @AuraEnabled
    public String End_date{get;set;}
    public List<String> STD_ACTIVITY_PATH{get;set;}
    @AuraEnabled
    public String num_contrat{get;set;}
    public String STD_LOGICALLYELIMINATEDDATE{get;set;}
    public String STD_CREATORNAME{get;set;}
    public List<String> STD_REVISIONS{get;set;}
    public String STD_ORGANISATION{get;set;}
    @AuraEnabled
    public String num_client{get;set;}
    public String TYP_AGENCE{get;set;}
    public Integer STD_ORGANISATION_DEPTH{get;set;}
    public String STD_TRANSFER_ID{get;set;}
    @AuraEnabled
    public String STD_FILENAME{get;set;}
    @AuraEnabled
    public String start_date{get;set;}
    public String STD_MODIFICATIONDATE{get;set;}
    public String STD_DEPOSIT_SYSTEM{get;set;}
    public String STD_DESCRIPTION{get;set;}
    public String STD_OWNER{get;set;}
    @AuraEnabled
    public String STD_RECORDTYPE_LABEL{get;set;}
    @AuraEnabled
    public String ID_TECH{get;set;}
    public String STD_ORGANISATION_CODE{get;set;}
    public Integer STD_ACTIVITY_DEPTH{get;set;}
    public String STD_CREATOR{get;set;}
    public Boolean STD_FROZEN{get;set;}
    public Integer STD_FILEWEIGHT{get;set;}
    @AuraEnabled
    public String STD_CREATIONDATE{get;set;}
    public String STD_ANOCODE{get;set;}
    @AuraEnabled
    public String STD_RECORD_ID{get;set;}
    public String STD_RETURNEDDATE{get;set;}
    @AuraEnabled
    public String STD_TITLE{get;set;}
    public List<String> STD_ORGANISATION_PATH{get;set;}
    public String STD_TOELIMINATEDATE{get;set;}
    public String STD_ACTIVITY_CODE{get;set;}
    @AuraEnabled
    public String STD_FILEFORMAT{get;set;}
    @AuraEnabled
    public String nom_produit{get;set;}
    public String STD_ARCHIVE_OWNER{get;set;}
    public String STD_LIFE_CYCLE_STATUS{get;set;}
    @AuraEnabled
    public String STD_RECORDTYPE_ID{get;set;}
    public String STD_ARCHIVER_ID{get;set;}
    public String STD_ACTIVITY{get;set;}
}
global class columnsStruct{
    public String id{get;set;}
    public String filterType{get;set;}
    public String fieldLabel{get;set;}
    public String fieldType{get;set;}
    public String fieldCode{get;set;}
    public String fieldFormat{get;set;}
    public String modificationDate{get;set;}
    public Boolean hidden{get;set;}
    public String creationDate{get;set;}
}


 /* @AuraEnabled
    public static void RetrievePDF (String idSFDC, String idDoca, String fileName){
        //GEDResponseWrapper idDoca = viewFiles (idSFDC); 
        String RecordId = idSFDC; 
        String fileContentType='application/pdf';
        String extFileURL='https://recette.okoro.demat.pro/1B5CB2/cham/gif/mag/ws/records/'+ idDoca + '/file/attached';
        String file = AP71_WSDocaposteActualisationDocs.DownloadFiless(extFileURL);
        Id attachmentId = AP71_WSDocaposteActualisationDocs.createAttachment(file, RecordId , fileContentType, fileName);
        System.debug('*****attachmentId:'+attachmentId);
        //return attachmentId;
    }*/

 /* @AuraEnabled
     public static Id createAttachment(String fileContent, String recordId, String fileType, String Name){
         //for pdf files content type should be pdf
         //for jpeg file content type should be image/jpeg
         Attachment attach = new Attachment(); 
         attach.ParentId = recordId; 
         attach.Name = Name; 
         attach.Body = EncodingUtil.base64Decode(fileContent); 
         attach.contentType = fileType; 
         insert attach; 
         return attach.id;
     }*/

}