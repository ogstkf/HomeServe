/**
 * @description       : 
 * @author            : RRJ
 * @group             : 
 * @last modified on  : 07-22-2020
 * @last modified by  : RRJ
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-21-2020   AMO   Initial Version
**/
public with sharing class AP73_PBSofactoFlag {

    public static Boolean isConnectedUserSofacto(){
        User currUser = [SELECT Id, Name, sofactoapp__isSofactoFinance__c, sofactoapp__IsSofactoAPI__c, sofactoapp__isSofactoAdmin__c FROM User WHERE Id = :userinfo.getuserId()];
        return currUser.sofactoapp__isSofactoFinance__c || currUser.sofactoapp__IsSofactoAPI__c || currUser.sofactoapp__isSofactoAdmin__c;
    }
}