/**
 * @File Name          : LC53_GenerateAvisDePassage.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 09-04-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/04/2020   ZJO     Initial Version
**/
public with sharing class LC53_GenerateAvisDePassage {

    @AuraEnabled
    public static map<string,Object> saveAvisDePassage(String servAppId){
        
        List<String> lstServAppId = new List<String>();
        List<ContentVersion> lstCV = new List<ContentVersion>();
        map<string,Object> mapOfResult = new map<string,Object>();

        lstServAppId.add(servAppId);
        ServiceAppointment sa = [Select Id, AppointmentNumber, Date_Visit_Notice_Generated__c From ServiceAppointment Where Id=:servAppId];
        
        Savepoint sp = Database.setSavepoint();
        try{

            PageReference template = new PageReference('/apex/VFP07_MassAvisDePassagePDF');
            Blob reportPDF;
            template.getParameters().put('lstIds',  JSON.serialize(lstServAppId));
            reportPDF = (Test.isRunningTest() ? Blob.valueOf('Unit.Test') : template.getContentAsPDF());
            ContentVersion CV = new ContentVersion();
            Datetime now = Datetime.now();
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);
            CV.PathOnClient = sa.AppointmentNumber + '_' + local + '.pdf';
            CV.Title = sa.AppointmentNumber + '_' + local + '.pdf';
            CV.VersionData = reportPDF;
            CV.IsMajorVersion = true;

            Insert CV;
            lstCV.add(CV);

            //Get Content Documents
            List<ContentVersion> docList = [SELECT ContentDocumentId FROM ContentVersion where Id=:lstCV];
            System.debug('## doc list:' + docList);
            Map<Id, Id> mapIdCVIdCDL = new Map<Id, Id>();
            for(ContentVersion contentV : docList){
                mapIdCVIdCDL.put(contentV.Id,contentV.ContentDocumentId);
            }

            //Create ContentDocumentLink 
            ContentDocumentLink cdl_ADP = New ContentDocumentLink();
            cdl_ADP.LinkedEntityId = servAppId;
            cdl_ADP.ContentDocumentId = mapIdCVIdCDL.get(cv.Id);
            cdl_ADP.shareType = 'V';
            Insert cdl_ADP;

            //Update Generated Date
            sa.Date_Visit_Notice_Generated__c = System.today();
            Update sa;
           
            mapOfResult.put('error',false);
	        mapOfResult.put('message', 'SUCESS!!!');
            mapOfResult.put('ADP',cdl_ADP.ContentDocumentId);

        }catch(Exception e){

            Database.rollback(sp);
            mapOfResult.put('error',true);
	        mapOfResult.put('message',e.getMessage()); 
            System.debug('Message error: ' + e.getMessage());  
            throw new AuraHandledException(e.getMessage());           
        }

        return mapOfResult;
    }
}