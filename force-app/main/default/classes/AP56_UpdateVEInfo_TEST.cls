/**
 * @File Name          : AP56_UpdateVEInfo_TEST.cls
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 08-06-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         10-01-2020     		     LGO        Initial Version
**/
@ isTest
public with sharing class AP56_UpdateVEInfo_TEST {
   
    
    static User mainUser;
    static List<Case> lstCase;
    static List<Account> lstAccount;
    static List<WorkOrder> lstWorkOrder;
    static List<ServiceAppointment> lstServiceAppointment;
    static PricebookEntry prcBkEnt = new PricebookEntry();
    static ServiceContract servCon = new ServiceContract();
    static List<ContractLineItem> lstCli = new List<ContractLineItem>();
    static List<Product2> lstProd = new List<Product2>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static Opportunity opp = new Opportunity();
    static  Bypass__c userBypass = new Bypass__c();


    static{
        mainUser = TestFactory.createAdminUser('AP56_UpdateVEInfo@test.COM', 
                                                TestFactory.getProfileAdminId());
		insert mainUser;

        userBypass.SetupOwnerId  = mainUser.Id;
        userBypass.BypassValidationRules__c = true;
        userBypass.BypassTrigger__c = 'AP14,AP16,WorkOrderTrigger,ContractLineItemTrigger,AP53';
        userBypass.BypassWorkflows__c = true;
        insert userBypass;

        system.runAs(mainUser){  

            // sofactoapp__Compte_auxiliaire__c cAux = new sofactoapp__Compte_auxiliaire__c (Name = 'test');
            // insert cAux;

            lstAccount = new List<Account>{
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='Kelly'),
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='Kelly Gir'),
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='Kelly Girally')
            };
			
            Insert lstAccount;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAccount.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstAccount.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstAccount.get(2).Id);
			lstAccount.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstAccount.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstAccount.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            update lstAccount;
            //Create Products
            lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                             Famille_d_articles__c='Pièces détachées',
                             ProductCode = 'COUV-DEPL-GAR-CHAM',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId(),
                             Equipment_family__c = 'Pompe à chaleur',
                             Equipment_type1__c = 'Pompe à chaleur piscine'
                             ),

                new Product2(Name='Prod2',
                             Famille_d_articles__c='Pièces détachées',
                             ProductCode = 'DEPL-ZONE',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
            };
            insert lstProd;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

                        //Create Pricebook Entry
            prcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 200);
            insert prcBkEnt;
            //create opportunity
            opp = TestFactory.createOpportunity('Prestation panneaux',lstAccount[0].Id);
            opp.Pricebook2Id = lstPrcBk[0].Id;
            insert opp; 
            Logement__c logement =  new Logement__c(
                Account__c = lstAccount.get(0).Id,
                
                Street__c = '15 Avenue des Champs-Elysées',
                City__c = 'Paris',
                Postal_Code__c = '75009'
                
            );
            
            
            insert logement;

            lstProd[0].IsActive = true;
            update lstProd;

            Asset asset = new Asset();
            asset.Name = 'asset';
            asset.AccountId = lstAccount[0].Id;
            asset.Logement__c = logement.Id;
            asset.Product2Id = lstProd[0].Id;
            insert asset;
            
            lstCase = new List<Case>{
                new Case(
                    AccountId=lstAccount[0].Id ,Type=AP_Constant.caseTypeInstallation,VE_Planning__c=false
                    ,Description='caseFirst',Subject='caseFirst',Origin = 'Cham Digital',Opportunity__c = opp.Id
                    ,Due_Date__c=Date.today().addDays(3),Start_Date__c=Date.today(), Status = AP_Constant.wrkOrderStatusInProgress,AssetId=asset.Id),

                new Case(RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Client case').getRecordTypeId(),
                    AccountId=lstAccount[1].Id ,Type=AP_Constant.caseTypeInstallation,VE_Planning__c=false
                    ,Description='caseFirst',Subject='caseFirst',Origin = 'Cham Digital', Opportunity__c = opp.Id
                    ,Due_Date__c=Date.today().addDays(3),Start_Date__c=Date.today(), Status = AP_Constant.wrkOrderStatusInProgress,AssetId=asset.Id)

            };

            insert lstCase;
			
            WorkType wt = new WorkType();
            wt.Description ='desc';
            wt.EstimatedDuration = 22;
            wt.Equipment_type__c = 'Chaudière gaz';
            wt.Type__c = 'Maintenance';
            wt.Reason__c = 'Visite sous contrat';
            wt.Name = 'worktype';
            wt.Type_de_client__c = 'Tous';
            wt.Agence__c = 'Toutes';
            insert wt;
            
            lstWorkOrder = new List<WorkOrder>{
                new WorkOrder(caseId = lstCase[0].Id, Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderStatusInProgress,
                    AccountId=lstAccount[0].Id,AssetId=asset.Id,WorkTypeId = wt.Id),
                new WorkOrder(caseId = lstCase[1].Id ,Status = AP_Constant.wrkOrderStatusNouveau, 
                              Priority = AP_Constant.wrkOrderStatusInProgress, AccountId=lstAccount[0].Id,AssetId=asset.Id,WorkTypeId = wt.Id)
            };
            
          
			//WorkOrder wok = lstWorkOrder.get(0);
           //wok.WorkTypeId
           // wok.AssetId = asset.Id;
            insert lstWorkOrder;
//ServiceAppointment sap = new ServiceAppointment();
			//sap.TECH_CaseOrigin__c
            lstServiceAppointment = new List<ServiceAppointment>{
                new ServiceAppointment(
                    // Status = AP_Constant.wrkOrderStatusInProgress,
                    Status = 'Done KO',
                   // Status = 'Cancelled',
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWorkOrder[0].Id,
                   
                   
                    
                    // Service_Resource_Level__c = AP_Constant.ServiceResourceLevelJunior,
                    Work_Order__c = lstWorkOrder[0].Id),

                new ServiceAppointment(
                   Status = 'Done OK',
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(2),
                    ParentRecordId = lstWorkOrder[1].Id,
                       
                   Service_Contract__c = servCon.Id,
                    // Service_Resource_Level__c = AP_Constant.ServiceResourceLevelJunior,
                    Work_Order__c = lstWorkOrder[1].Id),

                new ServiceAppointment(
                   Status = 'Done KO',
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(2),
                    ParentRecordId = lstWorkOrder[1].Id,
                      
                   
                    // Service_Resource_Level__c = AP_Constant.ServiceResourceLevelJunior,
                    Work_Order__c = lstWorkOrder[1].Id)  ,
                    
                new ServiceAppointment(
                   Status = 'Done client absent',
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(2),
                    ParentRecordId = lstWorkOrder[1].Id,
                      
                   
                    // Service_Resource_Level__c = AP_Constant.ServiceResourceLevelJunior,
                    Work_Order__c = lstWorkOrder[1].Id)  
            };

            
			lstServiceAppointment[1].Status = 'Done client absent';
			lstServiceAppointment[2].Status = 'Done OK';
			lstServiceAppointment[3].Status = 'Done KO';
            insert lstServiceAppointment;
            //Create Service Contract
            servCon = new ServiceContract(name = 'Service Con',
                                                  Pricebook2Id = lstPrcBk[0].Id,
                                                  AccountId = lstAccount[0].Id);
            insert servCon;

            //Create Contract Line Item
            lstCli = new list<ContractLineItem>{
                new ContractLineItem(ServiceContractId = servCon.Id,
                                     PricebookEntryId = prcBkEnt.Id,
                                     Quantity = 5,
                                     UnitPrice = 100,
                                     VE_Number_Counter__c =0),
                new ContractLineItem(ServiceContractId = servCon.Id,
                                    PricebookEntryId = prcBkEnt.Id,
                                    Quantity = 10,
                                    UnitPrice = 100,
                                    VE_Number_Counter__c =0)
            };
            insert lstCli;

            lstCase[0].Contract_Line_Item__c = lstCli[0].Id;
            lstCase[1].Contract_Line_Item__c = lstCli[1].Id;
            // lstCase[0].Opportunity__c = opp.Id;
            update lstCase;


        }


    }
    
    //@isTest
    @isTest(SeeAllData=true) 
    public static void testupdateCond1(){
        System.runAs(mainUser){
            Test.startTest();
            AP56_UpdateVEInfo.updateCond1(lstServiceAppointment);
            AP56_UpdateVEInfo.updateCond2(lstServiceAppointment);
            Test.stopTest();
        }
    }    
   
}