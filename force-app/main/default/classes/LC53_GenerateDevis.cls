/**
 * @File Name          : LC53_GenerateDevis.cls
 * @Description        : 
 * @Author             : DMG
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 09-11-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0         06/12/2019               DMG                       Initial Version
**/
public with sharing class LC53_GenerateDevis {

    // CT-1145
     @AuraEnabled
    public static Quote fetchQuoDetails(String quoId){ 
        return [SELECT Id, OpportunityId, Opportunity.Pricebook2.Name, Opportunity.Pricebook2Id, Pricebook2Id, Pricebook2.Name, Description, Status FROM Quote WHERE Id = :quoId];
    }
	
    @AuraEnabled
    public static map<string,Object> saveDevis(String quoteId,Boolean anonymise){
	    system.debug('## starting method saveDevis :'+quoteId);
        system.debug('## anonymise :'+anonymise);
	    map<string,Object> mapOfResult = new map<string,Object>();
		try {
            Quote q = [Select Id,QuoteNumber__c,Type_de_devis__c,Signature_Client__c, Status from Quote where Id=:quoteId];
            List<ContentVersion> lstCV = new List<ContentVersion>();


            //Create PDF Devis
		    PageReference pageRefDevis = null;
            if(anonymise == true){
                pageRefDevis = page.VFP10_Devis;
            }else{
                pageRefDevis = page.VFP10_DevisAnonymise;
            }
            String title;
            title = q.QuoteNumber__c; 
            Blob reportPDF;
            pageRefDevis.getParameters().put('id', quoteId);
            if(Test.isRunningTest()) { 
                reportPDF = blob.valueOf('Unit.Test');
            } else {
                reportPDF = pageRefDevis.getContentAsPDF();
            }
            ContentVersion cv = new ContentVersion();
                Datetime now = Datetime.now();
                Integer offset = UserInfo.getTimezone().getOffset(now);
                Datetime local = now.addSeconds(offset/1000);
            cv.Title = title + '_' + local +'.pdf';
            cv.PathOnClient = title + '.pdf';
            cv.VersionData = reportPDF;
            cv.IsMajorVersion = true;
            Insert cv;
            lstCV.add(cv);

            //Create PDF CGV 
		    // PageReference pageRef_CGV = null;
            // pageRef_CGV = (q.Type_de_devis__c == 'Visite forfaitaire' ? page.CGV_VisiteforfaitaireCG : page.CGV_PoseDepannageCG); 
            // Blob reportPDF_CGV;
            // if(Test.isRunningTest()) { 
            //     reportPDF_CGV = blob.valueOf('Unit.Test');
            // } else {
            //     reportPDF_CGV = pageRef_CGV.getContentAsPDF();
            // }
            // reportPDF_CGV = pageRef_CGV.getContentAsPDF();
            // ContentVersion cv_CGV = new ContentVersion();
            // cv_CGV.Title = title+'_CGV.pdf';
            // cv_CGV.PathOnClient = title+'_CGV.pdf';
            // cv_CGV.VersionData = reportPDF_CGV;
            // cv_CGV.IsMajorVersion = true;
            // Insert cv_CGV;
            // lstCV.add(cv_CGV);

            //Get Content Documents
            List<ContentVersion> docList = [SELECT ContentDocumentId FROM ContentVersion where Id=:lstCV];
            Map<Id, Id> mapIdCVIdCDL = new Map<Id, Id>();
            for(ContentVersion contentV : docList){
                mapIdCVIdCDL.put(contentV.Id,contentV.ContentDocumentId);
            }

            //Create ContentDocumentLink devis
            ContentDocumentLink cdl_Devis = New ContentDocumentLink();
            cdl_Devis.LinkedEntityId = quoteId;
            cdl_Devis.ContentDocumentId = mapIdCVIdCDL.get(cv.Id);
            cdl_Devis.shareType = 'V';
            Insert cdl_Devis;
            //Create ContentDocumentLink CGV
            // ContentDocumentLink cdl_CGV = New ContentDocumentLink();
            // cdl_CGV.LinkedEntityId = quoteId;
            // cdl_CGV.ContentDocumentId = mapIdCVIdCDL.get(cv_CGV.Id);
            // cdl_CGV.shareType = 'V';
            // Insert cdl_CGV;

		    mapOfResult.put('error',false);
	        mapOfResult.put('message', 'SUCESS!!!');
            mapOfResult.put('quoteNumber',cdl_Devis.ContentDocumentId);

		}
		catch (Exception e) {
		    System.debug('error msg:' + e.getMessage());
	        mapOfResult.put('error',true);
	        mapOfResult.put('message',e.getMessage());
		}

    return mapOfResult;
    }
    @AuraEnabled
    public static map<string,Object> insertImageAttachment(String quoteId) {
        system.debug('## starting method insertImageAttachment :'+quoteId);
        map<string,Object> mapOfResult = new map<string,Object>();
        List<Attachment> lstImgAttachment = new List<Attachment>();
        Quote q = [Select Id,QuoteNumber__c,Signature_Client__c, Status from Quote where Id=:quoteId];

        //Check Signature
        List<Attachment> lstA = [Select Id,Name from Attachment where name ='SignatureClient.png' and ParentId=:quoteId order by createdDate desc limit 1];
        system.debug('##lstA:'+lstA.size());
        //If Signature attachment not exists, create new one
        try{ 
            if(lstA.size()==0){
            // Insert Signature attachment if Signature_Client__c not empty
                if(q.Signature_Client__c!=null){
                    String imgClientBase64 = q.Signature_Client__c;

                    if(imgClientBase64.startsWith('data:image/jpeg;base64,')) {
                        imgClientBase64 = imgClientBase64.removeStart('data:image/jpeg;base64,');
                    }
                    lstImgAttachment.add(
                        new Attachment (
                            ParentId = quoteId, 
                            ContentType = 'image/png',
                            body = EncodingUtil.base64Decode(imgClientBase64),
                            Name = 'SignatureClient.png'
                        )
                    );       

                    if(lstImgAttachment.size() > 0) {
                        insert lstImgAttachment;
                        mapOfResult.put('error',false);
                        mapOfResult.put('message', 'SUCESS!!!');
                    }
                }
            }
        }
		catch (Exception e) {
		    System.debug('error msg:' + e.getMessage());
	        mapOfResult.put('error',true);
	        mapOfResult.put('message',e.getMessage());
		}
    return mapOfResult;
    }
}