/**
 * @File Name          : VFC40_LaunchBatchSetAgencySubSector.cls
 * @Description        : 
 * @Author             : DMU
 * @Group              : 
 * @Last Modified By   : DMU
 * @Last Modified On   : 
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/12/2020     DMU     Initial Version
 * 1.1    04/06/2020     DMU     Commented class due to HomeServe Project 
**/

public with sharing class VFC40_LaunchBatchSetAgencySubSector{
    
    private ApexPages.StandardSetController setCon;
    public List<serviceTerritory>lstST {get;set;}
    public String selectedRecords{get;set;}
    public String filterId{get;set;}

    public VFC40_LaunchBatchSetAgencySubSector(ApexPages.StandardSetController controller) {
        System.debug('##in VFC40_LaunchBatchSetAgencySubSector');
        setCon = controller;
        lstST = (List<serviceTerritory>) setCon.getSelected();
        filterId = setCon.getFilterId();
        System.debug('## FilterId : ' + filterId);
        selectedRecords = '';
        for(serviceTerritory st : lstST){
            System.debug('## st: ' + st);
            selectedRecords += st.Id + ',';
        }

        selectedRecords = selectedRecords.removeEnd(',');

        System.debug('## selected Records: ' + selectedRecords);
        System.debug('## lstST: ' + lstST);
        System.debug('##VFC40_LaunchBatchSetAgencySubSector end');
    }

}