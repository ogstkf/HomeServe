/**
 * @description       : 
 * @author            : RRJ
 * @group             : 
 * @last modified on  : 09-09-2020
 * @last modified by  : RRJ
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-14-2020   AMO   Initial Version
**/
global with sharing class BAT14_FlagSofacto implements Database.Batchable <sObject>, Database.Stateful, Schedulable{
    
    public static Boolean testBlnIsSofactoAdmin;
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'SELECT Id, Flag_PB_Sofacto__c, PBSofactoFlag__c FROM Quote WHERE';
        query += ' Flag_PB_Sofacto__c = true ';
        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext BC, List<Quote> lstQuotes) {
        System.debug('List of quotes: ' + lstQuotes);

        //Get current user Id
        String userId = UserInfo.getUserId();

        Boolean isSofactoAdmin = Test.isRunningTest() ? testBlnIsSofactoAdmin : AP73_PBSofactoFlag.isConnectedUserSofacto();

        if(isSofactoAdmin){
            System.debug('User has sofacto license');

            for(Quote qu : lstQuotes){
                qu.Flag_PB_Sofacto__c = false;
                qu.PBSofactoFlag__c = 'BatchJobProcessed';
            }

            if(lstQuotes.size() > 0){
                Update lstQuotes;
            }
        }

    }

    global void finish(Database.BatchableContext BC) {
        System.debug('**** BAT14_FlagSofacto finish *****');
    }

    global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT14_FlagSofacto');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT14_FlagSofacto(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT14_FlagSofacto());
        }
    }
}