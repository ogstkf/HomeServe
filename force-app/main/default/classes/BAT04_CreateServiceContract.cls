/**
* @author  SC (DMU)
* @version 1.0
* @since   08/11/2019
*
* Description : For Jira ticket CT-1179
* 
*-- Maintenance History: 
*--
*-- Date         Name  Version  Remarks 
*-- -----------  ----  -------  ------------------------
*-- 08-11-2019   DMU    1.0     Initial version
*-- 25-11-2019   ANA    1.0     Added query & condition on Bundle product to set Unit Price if TUL
*-- 16-12-2019   AMO    1.0     Added query & condition on Pricebook to set Unit Price if NO TUL
*-- 10-08-2020   DMU    1.0     TEC-126: Added additional field - homeserve
*-------------------------------------------------------
* 
*/
global with sharing class BAT04_CreateServiceContract implements Database.Batchable <sObject>, Database.Stateful, Schedulable{
    
    Integer numDaysPrior = Integer.valueOf(System.Label.NbDaysPriorRenewal);
    global String query;
    global String queryCustom = System.Label.BAT04_CreateServiceContractQuery;

    global Database.QueryLocator start(Database.BatchableContext BC){  

        this.query = 'SELECT Id, Name,Pricebook2Id, Term, Agency__r.Name, Tax__c, StartDate, Date_de_signature_client__c, EndDate, Status, RecordTypeId, Subtotal, Discount__c, TECH_NumberOfDaysPriorEndDate__c, AccountId, contactId, Logement__c,Sofacto_DateOfSignature__c,  Agency__c, Asset__c, Type__c, Echeancier_Paiement__c, Sofactoapp_Mode_de_paiement__c,'; //Promotional_Code__c
        this.query += ' Contract_Status__c, Stock__c, Payeur_du_contrat__c, ShippingStreet, ownerId, Home_Owner__c, sofactoapp_Rib_prelevement__c,BillingCity, BillingCountry, BillingPostalCode, BillingStreet, ShippingCity, ShippingCountry, ShippingPostalCode, ShippingState, Inhabitant__c, Acceptation_du_client_des_CP_et_CGV__c, Retractation_rightrenonciation__c, Contrat_Cham_Digital__c, Signature_client__c, Contrat_signe_par_le_client__c,';
        this.query += ' TECH_ContractPDF__c,Numero_du_contrat__c, TECH_Archivage__c, Duree_d_application_de_la_remise__c, Include_Exclude__c, Months__c, TECH_Date_d_edition__c, TECH_Date_de_depot_poste__c, TECH_Dematerialise__c, TECH_info_client_mis_a_jour__c, TECH_Lot_External_Id__c, TECH_Motif_PND__c, TECH_Type_de_document__c, Frequence_de_maintenance__c, Type_de_frequence__c, Debut_de_la_periode_de_maintenance__c, Fin_de_periode_de_maintenance__c, Periode_de_generation__c, Type_de_periode_de_generation__c, Generer_automatiquement_des_OE__c, Horizon_de_generation__c, Date_du_prochain_OE__c, ';
		this.query += ' Remise_permanente__c, ';
        										
        this.query += '(SELECT Id,Product2.Id, AssetId, PricebookEntryId, ServiceContractId, UnitPrice, Quantity, StartDate, EndDate, IsBundle__c, IsActive__c, VE__c, VE_Status__c, Desired_VE_Date__c, Completion_Date__c, VE_Min_Date__c, VE_Max_Date__c';
        this.query += ' FROM ContractLineItems)';
        this.query += ' FROM ServiceContract ';
        // this.query += ' WHERE id in (\'8100Q0000009GmOQAU\',\'8100Q0000009Ge9QAE\') ';  //for test purposes        
        
        if(queryCustom!='NONE' && !Test.isRunningTest()){
            this.query += queryCustom;
            System.debug('*** in custom query');
        }else{
            this.query += ' WHERE TECH_NumberOfDaysPriorEndDate__c = :numDaysPrior AND TECH_BypassBAT__c = false AND (Contract_Status__c = \'Pending first visit\' OR Contract_Status__c = \'Active\' OR Contract_Status__c = \'Actif - en retard de paiement\' ) AND Contrat_resilie__c = false AND Type__c = \'Individual\' AND Account.AccountSource != \'SOWEE\'';
            //this.query += ' WHERE Id = \'8100Q0000009GKbQAM\'';
        }     
        
        System.Debug('### query : ' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<ServiceContract> lstOldCon) { 
        
        // System.debug('#### lstOldCon: '+lstOldCon);
        List<ContractLineItem> lstOldCLI = [SELECT Id, PricebookEntry.Product2Id, PricebookEntry.Product2.IsBundle__c, Product2.Id, AssetId, PricebookEntryId, ServiceContractId, ServiceContract.Agency__c, UnitPrice, Quantity, StartDate, EndDate, IsBundle__c, IsActive__c, VE__c, VE_Status__c, Desired_VE_Date__c, Completion_Date__c, VE_Min_Date__c, VE_Max_Date__c, Discount FROM ContractLineItem WHERE ServiceContractId IN :lstOldCon ORDER BY ServiceContractId, IsBundle__c DESC NULLS LAST];
        
        Set<Id> setBundleProdId = new Set<Id>(); // used to get TULS
        Set<Id> setProdId = new Set<Id>();
        Map<Id, List<ContractLineItem>> mapOldConToCli = new Map<Id, List<ContractLineItem>>();
        Map<Id, Boolean> mapOldConToIsDiscount = new Map<Id,Boolean>();
        
        for(ContractLineItem cli : lstOldCLI){
            setProdId.add(cli.PricebookEntry.Product2Id);
            if(mapOldConToCli.containsKey(cli.ServiceContractId)){
                mapOldConToCli.get(cli.ServiceContractId).add(cli);
            }else{
                mapOldConToCli.put(cli.ServiceContractId, new List<ContractLineItem>{cli});
            }
            if(cli.PricebookEntry.Product2.IsBundle__c){
                setBundleProdId.add(cli.PricebookEntry.Product2Id);
            }
        }
        
        List<sofactoapp__R_glement__c> lstPaymt = [SELECT Id, sofactoapp__En_attente__c, sofactoapp__Facture__r.SofactoappType_Prestation__c, sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c FROM sofactoapp__R_glement__c WHERE sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c IN :lstOldCon AND (sofactoapp__En_attente__c = false OR statut_du_paiement__c = 'Collecté') ORDER BY CreatedDate DESC];
        Map<Id, sofactoapp__R_glement__c> mapConToPaymt = new Map<Id, sofactoapp__R_glement__c>();
        for(sofactoapp__R_glement__c paymt : lstPaymt){
            if(!mapConToPaymt.containsKey(paymt.sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c)){
                mapConToPaymt.put(paymt.sofactoapp__Facture__r.Sofactoapp_Contrat_de_service__c, paymt);
            }
        }
        // System.debug('#### mapConToPaymt: '+mapConToPaymt);
        
        Set<Id> setAgenceId = new Set<Id>(); // used to get TUL
        List<ServiceContract> lstNewCon = new List<ServiceContract>(); // list of new contracts to insert
        Map<Id, ServiceContract> mapOldConToNewCon = new Map<Id, ServiceContract>();

        
        list<id> lstPBId = new list<id>();
        for(ServiceContract oldCon : lstOldCon){            
            lstPBId.add(oldCon.Agency__c);
        }
        system.debug('** lstPBId: '+lstPBId); 

        //DMU 2021-07-06 - TEC-737 Add condition on soql pricebook 
        map<id, pricebook2>mapAgencePB = new map<id, pricebook2>();
        for(Pricebook2 pb : [SELECT id, Name, Agence__c, recordTypeId, Agence__r.name   
                                  FROM pricebook2 
                                  WHERE Agence__c in:lstPBId 
                                  AND recordTypeId = : Schema.SObjectType.pricebook2.getRecordTypeInfosByDeveloperName().get('Vente').getRecordTypeId()                                  
                                  AND IsActive = True
                                  AND Compte__c = null]){
            mapAgencePB.put(pb.Agence__c, pb);
        } 
        system.debug('**mapAgencePB: '+mapAgencePB);                        

        for(ServiceContract oldCon : lstOldCon){
            
            setAgenceId.add(oldCon.Agency__c);
            
            // create clone on contract
            ServiceContract newCon = oldCon.clone(false, false, false, false);
            newCon.Id = null;
            newCon.Contract_Renewed__c = true;
            newCon.TECH_OldContract__c = oldCon.Id; //DMU 20210902 Added due to erro handling 
            
            
            newCon.Type__c = oldCon.Type__c;
            newCon.Name = oldCon.Name;
            newCon.AccountId = oldCon.AccountId;
            newCon.contactId = oldCon.contactId;
            newCon.Logement__c = oldCon.Logement__c;
            newCon.Asset__c = oldCon.Asset__c;
            newCon.Agency__c = oldCon.Agency__c;
            newCon.Contract_Status__c =  AP_Constant.serviceContractStatutContratPendingPayment;
            newCon.StartDate = oldCon.EndDate.addDays(1);
            newCon.EndDate = newCon.StartDate.addYears(1).addDays(-1);
            newCon.Echeancier_Paiement__c = oldCon.Echeancier_Paiement__c;
            newCon.ownerId = oldCon.ownerId;
            newCon.Home_Owner__c = oldCon.Home_Owner__c;
            newCon.Inhabitant__c = oldCon.Inhabitant__c;
            newCon.Acceptation_du_client_des_CP_et_CGV__c = oldCon.Acceptation_du_client_des_CP_et_CGV__c;
            newCon.Retractation_rightrenonciation__c = oldCon.Retractation_rightrenonciation__c;
            newCon.Contrat_Cham_Digital__c = oldCon.Contrat_Cham_Digital__c;
            newCon.Signature_client__c = oldCon.Signature_client__c;
            newCon.Contrat_signe_par_le_client__c = oldCon.Contrat_signe_par_le_client__c;
            // newCon.TECH_ContractPDF__c = oldCon.TECH_ContractPDF__c;
            newCon.TECH_Archivage__c = oldCon.TECH_Archivage__c;
            // newCon.TECH_Date_d_edition__c = oldCon.TECH_Date_d_edition__c;
            // newCon.TECH_Date_de_depot_poste__c = oldCon.TECH_Date_de_depot_poste__c;
            newCon.TECH_Dematerialise__c = oldCon.TECH_Dematerialise__c;
            newCon.TECH_info_client_mis_a_jour__c = oldCon.TECH_info_client_mis_a_jour__c;
            newCon.TECH_Lot_External_Id__c = oldCon.TECH_Lot_External_Id__c;
            // newCon.TECH_Motif_PND__c = oldCon.TECH_Motif_PND__c;
            // newCon.TECH_Type_de_document__c = oldCon.TECH_Type_de_document__c;

            //DMU 20201126 - Pricebook2Id is taken from the ServiceContract.Agency__c = Pricebook2.Agence__c
            newCon.Pricebook2Id = mapAgencePB.containsKey(oldCon.Agency__c) ? mapAgencePB.get(oldCon.Agency__c).Id : null;
            newCon.Include_Exclude__c = oldCon.Include_Exclude__c;
            newCon.Months__c = oldCon.Months__c;
            newCon.Sofactoapp_Mode_de_paiement__c = oldCon.Sofactoapp_Mode_de_paiement__c;
            newCon.sofactoapp_Rib_prelevement__c = oldCon.sofactoapp_Rib_prelevement__c;
            newCon.Sofacto_DateOfSignature__c = oldCon.Sofacto_DateOfSignature__c;
            newCon.Term = oldCon.Term;
            newCon.Tax__c = oldCon.Tax__c;
            newCon.BillingCity = oldCon.BillingCity;
            newCon.BillingCountry = oldCon.BillingCountry;
            newCon.BillingPostalCode = oldCon.BillingPostalCode;
            newCon.BillingStreet = oldCon.BillingStreet;
            newCon.ShippingCity = oldCon.ShippingCity;
            newCon.ShippingCountry = oldCon.ShippingCountry;
            newCon.ShippingPostalCode = oldCon.ShippingPostalCode;
            newCon.ShippingState = oldCon.ShippingState;
            newCon.ShippingStreet = oldCon.ShippingStreet;
            newCon.Date_de_signature_client__c = oldCon.Date_de_signature_client__c;
            newCon.Numero_du_contrat__c = oldCon.Numero_du_contrat__c;
            newCon.Payeur_du_contrat__c = oldCon.Payeur_du_contrat__c;
            newCon.Stock__c = oldCon.Stock__c;
            // newCon.Promotional_Code__c = oldCon.Promotional_Code__c;
            newCon.RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId();
            
            //DMU - 10/08/2020 - TEC-126: Added additional field - homeserve
            newCon.Frequence_de_maintenance__c = oldCon.Frequence_de_maintenance__c;
            newCon.Type_de_frequence__c = oldCon.Type_de_frequence__c;
            newCon.Debut_de_la_periode_de_maintenance__c = oldCon.Debut_de_la_periode_de_maintenance__c;
            newCon.Fin_de_periode_de_maintenance__c = oldCon.Fin_de_periode_de_maintenance__c;
            newCon.Periode_de_generation__c = oldCon.Periode_de_generation__c;
            newCon.Type_de_periode_de_generation__c = oldCon.Type_de_periode_de_generation__c;
            newCon.Generer_automatiquement_des_OE__c = oldCon.Generer_automatiquement_des_OE__c;
            newCon.Horizon_de_generation__c = oldCon.Horizon_de_generation__c;
            //DMU 03/09/2021 added condition to resolve VR error "Attention la date du premier ordre d'exécution ne peut pas être antérieure à la date de début du contrat. Merci de corriger la date du 1er ordre d'exécution pour mettre la date de début du contrat ou une date ultérieure.".
            newCon.Date_du_prochain_OE__c = oldCon.Date_du_prochain_OE__c.addYears(1) < newCon.StartDate ? newCon.StartDate : oldCon.Date_du_prochain_OE__c.addYears(1);          
            
            Decimal newDureeAppRemise =  (oldCon.Duree_d_application_de_la_remise__c == null || oldCon.Duree_d_application_de_la_remise__c == 0 ) ? 0 : (oldCon.Duree_d_application_de_la_remise__c - 1);
                        
            //MNA - 19/10/2021 - TEC-831 
            if (oldCon.Remise_permanente__c || (!oldCon.Remise_permanente__c && newDureeAppRemise > 0)) {
                mapOldConToIsDiscount.put(oldCon.Id, true);
                if (oldCon.Remise_permanente__c) 
                    newCon.Duree_d_application_de_la_remise__c = oldCon.Duree_d_application_de_la_remise__c;
                else 
                    newCon.Duree_d_application_de_la_remise__c = newDureeAppRemise;
            }
            else {
                mapOldConToIsDiscount.put(oldCon.Id, false);
                newCon.Duree_d_application_de_la_remise__c = newDureeAppRemise;
            }
            

            if(newDureeAppRemise == 0){
                newCon.Discount__c = null;
            }else{
                newCon.Discount__c = oldCon.Discount__c;
            }
            

            if(newCon.Contract_Status__c != 'Cancelled' || newCon.Contract_Status__c != 'Résilié' || newCon.Contract_Status__c != 'Expired'){
                if(newCon.Echeancier_Paiement__c == 'Mensuel' || newCon.Echeancier_Paiement__c == 'Trimestriel'){
                    newCon.Contract_Status__c = 'En attente de renouvellement';
                }else if(newCon.Echeancier_Paiement__c == 'Comptant'){
                    if(mapConToPaymt.containsKey(oldCon.Id) && oldCon.Contract_Status__c == 'Pending Payment'){
                        newCon.Contract_Status__c = 'En attente de renouvellement';
                    }else if(mapConToPaymt.containsKey(oldCon.Id) && oldCon.Contract_Status__c == 'Actif - en retard de paiement'){
                        system.debug('*** inside 12');
                        newCon.Contract_Status__c = 'Active';
                    }else{
                        newCon.Contract_Status__c = 'Pending Payment';
                    }
                }
            }
            
            lstNewCon.add(newCon);
            mapOldConToNewCon.put(oldCon.Id, newCon);
            System.debug('### new Con: '+newCon);
        }
        
        //DMU 08/01/2021 - Added logic to check TECH_BypassBAT__c on oldContracts after successfull renewal
        List<ServiceContract> lstServConUpd = new List<ServiceContract>();
        
        if(mapOldConToNewCon.size()>0){
            System.debug('###### CONTRACTS: '+mapOldConToNewCon.values());
            System.debug('###### CONTRACTS: '+mapOldConToNewCon.size());
            //insert mapOldConToNewCon.values();
            //System.debug(mapOldConToNewCon.values()[0].Id);
            
            //DMU 02/02021 - Added Error Handling
                        list<ServiceContract> lstSCToIns = mapOldConToNewCon.values();
            list<Database.SaveResult> resultsCon = Database.insert(lstSCToIns, false);

            //Error Handling
            List<TransactionLog__c> lstTransLog = new List<TransactionLog__c>();

            for (Integer i = 0; i < lstSCToIns.size(); i++) {
                Database.SaveResult sr = resultsCon[i];

                if (!sr.isSuccess()) {
                    TransactionLog__c tr = new TransactionLog__c(
                        ProcessName__c = 'BAT04_CreateServiceContract',
                        RecordId__c = lstSCToIns[i].TECH_OldContract__c,
                        Message__c = sr.getErrors().get(0).getMessage()
                    );

                    lstTransLog.add(tr);
                } else {
                    lstServConUpd.add(
                        new ServiceContract(
                            Id = lstSCToIns[i].TECH_OldContract__c,
                            TECH_BypassBAT__c = true
                        )
                    );
                }
            }
            Database.insert(lstTransLog, false);

            Database.update(lstServConUpd, false);
        }
        
        
        // get TULS for contracts
        List<Product_Bundle__c> lstProdBundleParent = [SELECT Id, Bundle__c, Price__c, Optional_Item__c, Product__c FROM Product_Bundle__c WHERE Product__r.isBundle__c = true AND Product__c IN :setBundleProdId ORDER BY Bundle__c];
        
        Map<Id, List<Id>> mapProdToBundles = new Map<Id, List<Id>>();
        Set<Id> setBundleId = new Set<Id>();
        
        for(Product_Bundle__c prodBun : lstProdBundleParent){
            setBundleId.add(prodBun.Bundle__c);
            if(mapProdToBundles.containsKey(prodBun.Product__c)){
                mapProdToBundles.get(prodBun.Product__c).add(prodBun.Bundle__c);
            }else{
                mapProdToBundles.put(prodBun.Product__c, new List<Id>{prodBun.Bundle__c});
            }
        }

        System.debug('#### setBundleId: '+setBundleId);

        List<Product_Bundle__c> lstOptionBundles = [SELECT Id, Bundle__c, Price__c, Optional_Item__c, Product__c FROM Product_Bundle__c WHERE Optional_Item__c = true AND Bundle__c IN :setBundleId ORDER BY Bundle__c];
        System.debug('#### lstOptionBundles: '+lstOptionBundles.size());
        Map<Id, Set<Id>> mapBundleToOption = new Map<Id, Set<Id>>();
        for(Product_Bundle__c prodBun : lstOptionBundles){
            if(mapBundleToOption.containsKey(prodBun.Bundle__c)){
                mapBundleToOption.get(prodBun.Bundle__c).add(prodBun.Product__c);
            }else{
                mapBundleToOption.put(prodBun.Bundle__c, new Set<Id>{prodBun.Product__c});
            }
        }
        System.debug('### mapBundleToOption: '+mapBundleToOption);

        List<Tarifs_Unifies_Locaux__c> lstTul = [SELECT Id, name, Cap_d_augmentation_max__c,Bundle_du_contrat__c,Agence__c,Tarif_Unifie_Local__c, Augmentation_nationale_annuelle__c FROM Tarifs_Unifies_Locaux__c WHERE Agence__c IN :setAgenceId AND Bundle_du_contrat__c IN :setBundleId  AND Actuellement_actif_pour_renouvellement__c = TRUE];
        
        Map<String, Tarifs_Unifies_Locaux__c> mapTUL = new Map<String, Tarifs_Unifies_Locaux__c>();
        for(Tarifs_Unifies_Locaux__c tul : lstTul){
            mapTUL.put(tul.Agence__c+'-'+tul.Bundle_du_contrat__c, tul);
        }
        System.debug('#######>>>>>'+mapTUL);
        
        Map<String, PricebookEntry> mapAllPbe = new Map<String, PricebookEntry>();
        //BCH 19/02/2020 add condition on Recordtype to limit the number of rows queried to only CHAM Offer record types product, no need for Articles
        List<PricebookEntry> lstPBE = [SELECT Id, Product2Id, Pricebook2Id, UnitPrice, Pricebook2.Active_for_renewal__c, Pricebook2.IsActive 
                                        FROM PricebookEntry 
                                        WHERE Pricebook2.Active_for_renewal__c = true 
                                        AND Pricebook2.Ref_Agence__c = true 
                                        AND Product2.recordtype.developername='Cham_offer' 
                                        AND Product2Id IN :setProdId 
                                        AND IsActive = true];
        
        Map<Id, PricebookEntry> mapRenewalProdIdToPbe = new Map<Id, PricebookEntry>();
        for(PricebookEntry pbe: lstPBE){
            mapRenewalProdIdToPbe.put(pbe.Product2Id, pbe);
            mapAllPbe.put(pbe.Pricebook2Id+'-'+pbe.Product2Id, pbe);
        }
        
        List<PricebookEntry> lstExistingPBE = [SELECT Id, Product2Id, Pricebook2Id, UnitPrice, Pricebook2.Active_for_renewal__c, Pricebook2.IsActive 
                                                FROM PricebookEntry 
                                                WHERE  Product2Id IN: setProdId 
                                                AND Pricebook2.Ref_Agence__c = true 
                                                AND Pricebook2.IsActive = true 
                                                AND IsActive = true];
        
        Map<Id, PricebookEntry> mapNormalProdIdToPbe = new Map<Id, PricebookEntry>();
        for(PricebookEntry pbe : lstExistingPBE){
            mapNormalProdIdToPbe.put(pbe.Product2Id, pbe);
            mapAllPbe.put(pbe.Pricebook2Id+'-'+pbe.Product2Id, pbe);
        }
        
        Map<Id, ServiceContract> mapConToUpdt = new Map<Id, ServiceContract>();
        Map<Id, List<ContractLineItem>> mapNewConToCLI = new Map<Id, List<ContractLineItem>>();
        for(Id oldConId : mapOldConToNewCon.keySet()){
            System.debug('##### ConId: '+ oldConId+' '+mapOldConToNewCon.get(oldConId).Id+ ' '+mapOldConToNewCon.get(oldConId).Name);
            if(mapOldConToCli.containsKey(oldConId)){
                Boolean hasTUL = false;
                Tarifs_Unifies_Locaux__c tul;
                Decimal oldBundlePrc;
                Decimal newBundlePrc;
                Id parentBundleId;
                for(ContractLineItem oldCli: mapOldConToCli.get(oldConId)){
                    ServiceContract newCon = mapOldConToNewCon.get(oldConId);
                    
                    ContractLineItem newCli = oldCli.clone(false, false, false, false);
                    newCli.ServiceContractId = newCon.Id;
                    newCli.StartDate = newCon.StartDate;
                    newCli.EndDate = newCon.EndDate;
                    newCli.PricebookEntryId = oldCli.PricebookEntryId;
                    
                    newCli.Quantity = oldCli.Quantity;
                    newCli.IsActive__c = false;
                    newCli.VE__c = false;
                    newCli.VE_Status__c = oldCli.IsBundle__c == True ? 'Not Planned' : oldCli.VE_Status__c;
                    newCli.Completion_Date__c = null;
                    
                    if(oldCli.Desired_VE_Date__c != null){
                        newCli.Desired_VE_Date__c = oldCli.Desired_VE_Date__c.addYears(1);
                    }else if(oldCli.Completion_Date__c != null && oldCli.Desired_VE_Date__c == null){
                        newCli.Desired_VE_Date__c = oldCli.Completion_Date__c.addYears(1);
                    }else if(oldCli.Desired_VE_Date__c == null && oldCli.Completion_Date__c == null){
                        newCli.Desired_VE_Date__c = null;
                    }
                    
                    newCli.AssetId = oldCli.AssetId;
                    newCli.VE_Min_Date__c = oldCli.VE_Min_Date__c != null ? oldCli.VE_Min_Date__c.addYears(1) : null;
                    newCli.VE_Max_Date__c = oldCli.VE_Max_Date__c != null ? oldCli.VE_Max_Date__c.addYears(1) : null;
                    newCli.UnitPrice = oldCli.UnitPrice;

                    //MNA - 19/10/2021 - TEC-831
                    if (mapOldConToIsDiscount.get(oldConId)) {
                        newCli.Discount = oldCli.Discount;
                    }
                    else {
                        newCli.Discount = null;
                    }
                    
                    Boolean isBundle = oldCli.PricebookEntry.Product2.IsBundle__c;
                    Boolean isOption = false;
                    
                    //DMU 2021-07-06 - TEC-737 - Commented newCon.Pricebook2Id since Pricebook2Id of new contract is already set earlier
                    if(isBundle){
                        oldBundlePrc = oldCli.UnitPrice;
                        
                        if(mapProdToBundles.containsKey(oldCli.PricebookEntry.Product2Id)){
                            for(Id bundleId : mapProdToBundles.get(oldCli.PricebookEntry.Product2Id)){
                                parentBundleId = bundleId;
                                if(mapTUL.containsKey(oldCli.ServiceContract.Agency__c+'-'+bundleId)){
                                    tul = mapTUL.get(oldCli.ServiceContract.Agency__c+'-'+bundleId);
                                    hasTUL = true;
                                }
                            }
                        }
                        if(mapRenewalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                            newCli.PricebookEntryId = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                            newCli.UnitPrice = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                            //newCon.Pricebook2Id = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                            mapConToUpdt.put(newCon.Id, newCon);
                        }else if(mapNormalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                            newCli.PricebookEntryId = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                            newCli.UnitPrice = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                            //newCon.Pricebook2Id = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                            mapConToUpdt.put(newCon.Id, newCon);
                        }
                        newBundlePrc = newCli.UnitPrice;

                        if(hasTUL && tul != null){
                            if(oldCli.UnitPrice > tul.Tarif_Unifie_Local__c){
                                /*Decimal augmentation = tul.Augmentation_nationale_annuelle__c == null ? 0 :tul.Augmentation_nationale_annuelle__c/100;
                                newCli.UnitPrice = (oldCli.UnitPrice + (augmentation * oldCli.UnitPrice));// to chang 1179*/
                                newCli.UnitPrice = oldCli.UnitPrice;
                            }
                            else if(oldCli.UnitPrice <= tul.Tarif_Unifie_Local__c){
                                Double percentCapAug = tul.Cap_d_augmentation_max__c/100;
                                Double newUnitPrice = oldCli.UnitPrice + (percentCapAug * oldCli.UnitPrice);
                                
                                if(tul.Tarif_Unifie_Local__c >= newUnitPrice){
                                    newCli.UnitPrice = newUnitPrice;
                                }else{
                                    newCli.UnitPrice = tul.Tarif_Unifie_Local__c;
                                }
                            }
                        }
                    
                        newCli.VE__c = true;
                    }else{
                        if(mapBundleToOption.containsKey(parentBundleId)){
                            isOption = mapBundleToOption.get(parentBundleId).contains(oldCli.PricebookEntry.Product2Id);
                        }
                    }
                    System.debug('###hasTUL:'+hasTUL);
                    System.debug('###isOption:'+isOption);

                    if(isOption){
                        if(hasTUL && tul != null){
                            if(oldBundlePrc > tul.Tarif_Unifie_Local__c){
                                /*Decimal augmentation = tul.Augmentation_nationale_annuelle__c == null ? 0 :tul.Augmentation_nationale_annuelle__c/100;
                                newCli.UnitPrice = (oldCli.UnitPrice + (augmentation * oldCli.UnitPrice)); // to chang 1179*/
                                newCli.UnitPrice = oldCli.UnitPrice;
                            }
                            else if(oldBundlePrc <= tul.Tarif_Unifie_Local__c){
                                Double percentCapAug = tul.Cap_d_augmentation_max__c/100;
                                Double newUnitPrice = oldCli.UnitPrice + (percentCapAug * oldCli.UnitPrice);
                                newCli.UnitPrice = newUnitPrice;
                            }
                            mapConToUpdt.put(newCon.Id, newCon);
                        }else{
                            if(mapRenewalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                                newCli.PricebookEntryId = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                                newCli.UnitPrice = oldCli.UnitPrice;
                                //newCli.UnitPrice = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                                //newCon.Pricebook2Id = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                            }else if(mapNormalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                                newCli.PricebookEntryId = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                                newCli.UnitPrice = oldCli.UnitPrice;
                                //newCli.UnitPrice = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                                //newCon.Pricebook2Id = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id; 
                            }
                            mapConToUpdt.put(newCon.Id, newCon);
                        }
                    }
                    

                    if(mapNewConToCLI.containsKey(newCon.Id)){
                        mapNewConToCLI.get(newCon.Id).add(newCli);
                    }else{
                        mapNewConToCLI.put(newCon.Id, new List<ContractLineItem>{newCli});
                    }
                }
            }
        }
        
        List<ContractLineItem> lstNewCli = new List<ContractLineItem>();
        
        for (String key : mapAllPbe.keySet()) {
            System.debug(key+'========='+mapAllPbe.get(key));
        }
        
        for(ServiceContract con : mapConToUpdt.values()){
            System.debug(con.Pricebook2Id);
            for(ContractLineItem cli : mapNewConToCLI.get(con.Id)){
                SysteM.debug(cli);
                System.debug('is present: '+mapAllPbe.containsKey(con.Pricebook2Id+'-'+cli.Product2Id));
                if(mapAllPbe.containsKey(con.Pricebook2Id+'-'+cli.Product2Id)){
                    cli.PricebookEntryId = mapAllPbe.get(con.Pricebook2Id+'-'+cli.Product2Id).Id;
                    lstNewCli.add(cli);
                }
                System.debug('#### new CLIIIII: '+cli);
            }
        }
        if(mapConToUpdt.size()>0){
            AP48_GestionTVA.getTVAServContr(new List<Id>(mapConToUpdt.keySet()));
            //update mapConToUpdt.values();
            Database.update(mapConToUpdt.values(), false);
        }

        System.debug('##### mapConToUpdt: '+ mapConToUpdt.keySet());
        
        if(lstNewCli.size()>0){
            //insert lstNewCli;
            Database.insert(lstNewCli, false);
        }
        System.debug('###### lstNewCli: '+lstNewCli);
        
    }
    
    
    global void finish(Database.BatchableContext BC) {
        System.debug('in finish: ');
    }
    
    //used to schedule job 
    global static String scheduleBatch() {
        BAT04_CreateServiceContract scheduler = new BAT04_CreateServiceContract();
        return System.schedule('Batch CreateServiceContract:' + Datetime.now().format(),  '0 0 0 * * ?', scheduler);
    }
    
    global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT04_CreateServiceContract');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT04_CreateServiceContract(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT04_CreateServiceContract());
        }
    }
}