/**
 * @File Name          : AP68_updateWorkOrderQuotes_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/26/2020   RRJ     Initial Version
**/
@isTest
public with sharing class AP68_updateWorkOrderQuotes_TEST {
    static User mainUser;
    static Account testAcc = new Account();
    static Opportunity opp = new Opportunity();
    static WorkOrder wrkOrd = new WorkOrder();
    static list<Product2> lstProd = new list<Product2>();
    static Schema.Location loc = new Schema.Location();
    static OperatingHours oh = new OperatingHours();
    static sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c();
	static ServiceTerritory st = new ServiceTerritory();
	static ServiceTerritoryLocation stl = new ServiceTerritoryLocation();  

    static Quote quo = new Quote();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static list<QuoteLineItem> lstQli = new list<QuoteLineItem>();
	static ProductItem pi = new ProductItem(); 
    static ProductRequestLineItem prli = new ProductRequestLineItem();
    static case cse;
    static Bypass__c bp = new Bypass__c();
    
    static{
        
        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;


        System.runAs(mainUser){
			
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;
            
            sofactoapp__Compte_auxiliaire__c aux0 = createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
			update testAcc;
            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);            
            insert opp;


            
            // create case
			cse = new case(AccountId = testacc.id
						   ,type = 'Maintenance', Reason__c='Visite sous contrat', RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId());
			insert cse;


            
            //work order
            wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse.Id;
            insert wrkOrd;
             System.debug('Create wo:' + wrkOrd);
            //create Products
            lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                             Famille_d_articles__c='Consommables',
                             RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
                    
                
                    
                /*new Product2(Name='Prod3', 
                             Famille_d_articles__c='Consommables',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                    
                new Product2(Name='Prod4', 
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())*/
            };
               
            insert lstProd;
            
            //create Location
            loc = new Schema.Location(Name='Test1',
                                      LocationType='Entrepôt',
                                      ParentLocationId=NULL,
                                      IsInventoryLocation=true);
            insert loc;

			//create Operating Hours
			oh = new OperatingHours(Name='Test1');
			insert oh;

			//create Corporate Name
			sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(Name='Test1',
                                                                                  sofactoapp__Credit_prefix__c='Test1',
                                                                                  sofactoapp__Invoice_prefix__c='Test1');  
            insert srs;
            
            //create Service Territory
            st = new ServiceTerritory(Name='Test1',
                                      OperatingHoursId=oh.Id,
                                      Sofactoapp_Raison_Social__c=srs.Id);
            insert st;
            
            //create Service Territory Location
            stl = new ServiceTerritoryLocation(ServiceTerritoryId=st.Id,
                                               LocationId=loc.Id);
            insert stl;
            
            
           
            
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            insert PrcBkEnt;
            
            //create Quote
            quo = new Quote(Name ='Test1',
                            OpportunityId=opp.Id,
                            Agency__c=st.Id,
                            Pricebook2Id = lstPrcBk[0].Id,
                            Ordre_d_execution__c = wrkOrd.Id,
                            isSync__c = false,
                            A_planifier__c = false,  
                            Devis_signe_par_le_client__c = true, 
                            Date_de_debut_des_travaux__c = System.today(),
                            Salarie_primable__c = mainUser.Id
                           );
            
            insert quo;  
            System.debug('Insert quote:' + quo);
           // wrkOrd.Quote__c = quo.Id;
           // update wrkOrd;
                       
            
            ProductRequest pr = new ProductRequest();
          //  pr.A_preparer__c =1;
            pr.AccountId = testAcc.Id;
            pr.Description = 'description';
          //  pr.Nb_articles__c = 3;
            pr.NeedByDate = DateTime.now();
            pr.WorkOrderId = wrkOrd.Id;
            insert pr;
            
            
             pi = new ProductItem(Product2Id=lstProd[0].Id,
                                 LocationId=loc.Id,
                                 QuantityOnHand=decimal.valueOf(2));
            insert pi;
            quo.Ordre_d_execution__c = wrkOrd.Id;
            update quo;
            //create Quote Line Items
            lstQli = new list<QuoteLineItem>{
            	new QuoteLineItem(QuoteId=quo.Id, 
                                  Product2Id=lstProd[0].Id, 
                                  Quantity=decimal.valueOf(2), 
                                  UnitPrice=decimal.valueOf(145),
                                  PriceBookEntryID=PrcBkEnt.Id)
            };
            insert lstQli;
            
        }
        
    } 

    @isTest
    public static void updateWorkOrderQuote(){
        WorkOrder newWrkOrd = TestFactory.createWorkOrder();
        newWrkOrd.Quote__c = quo.Id;
        
        System.runAs(mainUser){
            Test.startTest();
                insert newWrkOrd; 
            Test.stopTest();
        }
        List<Quote> lstQuote = [SELECT Id,Ordre_d_execution__c from Quote WHERE Id = :quo.Id];
        System.assertEquals( newWrkOrd.Id, lstQuote[0].Ordre_d_execution__c);
    }

    @isTest
    public static void getQuotesTest(){

        quo.Status = AP_Constant.quoteStatusValideSigne; 
        update quo; 

        System.runAs(mainUser){
            Test.startTest();
                List<Quote> lstQuotes = LC11_NewWorkOrder.getQuotes(
                    cse.Id
                );
            Test.stopTest();

            System.assertEquals(1, lstQuotes.size());
        }
    }

    public static sofactoapp__Plan_comptable__c createPlanComptable(){
        sofactoapp__Plan_comptable__c plan = new sofactoapp__Plan_comptable__c();
        plan.Name = 'sofactoapp__Plan_comptable__c';
        insert plan;
        return plan;
    }
    
    public static sofactoapp__Compte_comptable__c createCompteComptable(){
        sofactoapp__Plan_comptable__c chartAccount = createPlanComptable();
        sofactoapp__Compte_comptable__c compte = new sofactoapp__Compte_comptable__c();
        compte.Name = 'compte comptable';
        compte.sofactoapp__Plan_comptable__c = chartAccount.Id;
        compte.sofactoapp__Libelle__c = 'sofactoapp__Libelle__c';
        insert compte;
        return compte;
            
    }
    
    public static sofactoapp__Compte_auxiliaire__c createCompteAuxilaire(Id accountId){
        sofactoapp__Compte_comptable__c comptable = createCompteComptable();
        sofactoapp__Compte_auxiliaire__c compte = new sofactoapp__Compte_auxiliaire__c();
        compte.sofactoapp__Compte__c = accountId;
        compte.sofactoapp__Compte_comptable__c = comptable.Id;
        compte.Compte_comptable_Prelevement__c = comptable.Id;
        compte.sofactoapp__Libelle__c = 'sofactoapp__Libelle__c';
        insert compte;
        return compte;
        
    } 
}