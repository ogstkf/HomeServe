/**
* @File Name          		: MobileNotificationTriggerHandler.cls
* @Description				  : Trigger Handler for Mobile_notifications
* @Author					  	:  KRO 
* @Group              			: Spoon Consulting
* @Last Modified By   	: KRO
* @Last Modified On   	: 11/11/2019
* @Modification Log   	: 
*==============================================================================
* Ver         Date                     Author                    Modification
*==============================================================================
* 1.0    11/11/2019          		KRO                 Initial Version
**/
public class MobileNotificationTriggerHandler {
    public void handleAfterUpdate(List<Mobile_Notifications__c> oldNotifs, List<Mobile_Notifications__c> newNotifs){
        List<Mobile_Notifications__c> toHandle = new List<Mobile_Notifications__c>();
        List<Id> agencies = new List<Id>();
        Map<Id,String> messages = new Map<Id,String>();
        for(Integer i =0; i < oldNotifs.size();i++){
            Mobile_Notifications__c oldNotif = oldNotifs.get(i);
            Mobile_Notifications__c newNotif = newNotifs.get(i);
            if(oldNotif.Status__c.equals('Waiting') && newNotif.Status__c.equals('Sent')){
                toHandle.add(newNotif);
                String message = newNotif.message__c;
                if(newNotif.all_ressources__c){
                    AP16_MobileNotifications.sendNotification('cham', message,newNotif.Id);
                }else{
                    ServiceTerritory territory = [SELECT Id, Name FROM ServiceTerritory WHERE Id =:newNotif.Agency__c];
                    AP16_MobileNotifications.sendNotification( territory.Name,message, newNotif.Id);
                }
            }
        }
        
        
        
    }
}