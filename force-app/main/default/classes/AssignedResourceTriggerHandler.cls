/**
 * @File Name          : AssignedResourceTriggerHandler.cls
 * @Description        : 
 * @Author             : SBH
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   :
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/10/2019         SBH                    Initial Version
**/

public with sharing class AssignedResourceTriggerHandler {

    public static Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());

    public AssignedResourceTriggerHandler(){
        //userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    //Handle after insert
    public void handleAfterInsert(List<AssignedResource> lstNew){        
        System.debug('## handleAfterInsert start ##');	    
        List<Id> lstIdAR = new List<Id>();	

        List<AssignedResource> lstARLocation = new List<AssignedResource>();
        Set<Id> setSAIds = new Set<Id>();
        Set<Id> setSRIds = new Set<Id>();
        
        //selecting fields from SA for conditions: 
        for(AssignedResource AR : [SELECT Id, ServiceResource.Id, ServiceAppointment.Status, ServiceAppointment.SchedStartTime, ServiceAppointment.SchedEndTime 
                        FROM AssignedResource WHERE Id IN :lstNew ORDER BY CreatedDate DESC]){
            Datetime startOfDay = DateTime.newInstance(System.now().year() , System.now().month(), System.now().day(), 0, 0, 0);
            Datetime endOfDay = DateTime.newInstance(System.now().year() , System.now().month(), System.now().day(), 23, 59, 59);
            
            if(AR.ServiceAppointment.Status!='In Progress' && AR.ServiceAppointment.SchedStartTime<= endOfDay && AR.ServiceAppointment.SchedEndTime>= startOfDay ){
                lstIdAR.add(AR.Id);
            }

            //KZE CT-1161 fill work order location
            lstARLocation.add(AR);                  
            setSAIds.add(AR.ServiceAppointmentId);
            setSRIds.add(AR.ServiceResourceId);      
        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP16;')){
            AP16_MobileNotifications.BeforeSendingNotiff(lstIdAR);
        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP33;')){
            AP33_ManageAssignedResources.onServiceResourceInserted(lstARLocation,setSAIds, setSRIds);
        }
        System.debug('## handleAfterInsert end ##');
    }

    public void handleBeforeDelete(List<AssignedResource> lstOld){
        System.debug('## handleBefore Delete start');

        List<Id> lstIdAR = new List<Id>();
        for(AssignedResource AR : [SELECT Id, ServiceAppointment.Status, ServiceAppointment.SchedStartTime, ServiceAppointment.SchedEndTime FROM AssignedResource WHERE Id IN :lstOld]){
            Datetime startOfDay = DateTime.newInstance(System.now().year() , System.now().month(), System.now().day(), 0, 0, 0);
            Datetime endOfDay = DateTime.newInstance(System.now().year() , System.now().month(), System.now().day(), 23, 59, 59);
            
            if(AR.ServiceAppointment.Status!='In Progress' && AR.ServiceAppointment.SchedStartTime<= endOfDay && AR.ServiceAppointment.SchedEndTime>= startOfDay ){
                lstIdAR.add(AR.Id);
            }
            
        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP16;')){
            AP16_MobileNotifications.BeforeSendingNotiff(lstIdAR);
        }
        System.debug('## handleBefore Delete end');
    }

    public void handleAfterDelete(List<AssignedResource> lstOld){
        List<AssignedResource> lstARLocation = new List<AssignedResource>();
        Set<Id> setSAIds = new Set<Id>();
        Set<Id> setSRIds = new Set<Id>();

        for(AssignedResource AR : lstOld){
            //KZE CT-1161 fill work order location
            lstARLocation.add(AR);      
            setSAIds.add(AR.ServiceAppointmentId);
            setSRIds.add(AR.ServiceResourceId);  
        }

        System.debug('## setSAIds: ' + setSAIds);
        System.debug('## setSRIds: ' + setSRIds);
        System.debug('## lstARLocation ' + lstARLocation);

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP33;')){
            // if(!AP33_ManageAssignedResources.hasRunAp33){
                AP33_ManageAssignedResources.onServiceResourceDeleted(lstARLocation,setSAIds, setSRIds);
            // }
            
        }
    }
    //Handle after update start
    public void handleAfterUpdate(List<AssignedResource> lstOld, List<AssignedResource> lstNew){
        System.debug('## Handle After Update start');
        
        List<AssignedResource> lstAp33Old = new List<AssignedResource>();
        List<AssignedResource> lstAp33New = new List<AssignedResource>();
        Set<Id> setServAppIds = new Set<Id>();
        Set<Id> setNewServiceResources = new Set<Id>();
        
        for(Integer i = 0 ; i < lstOld.size(); i++){

            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP33;')){
                
                //Service resource changed
                if(lstOld[i].ServiceResourceId != lstNew[i].ServiceResourceId){
                    lstAp33Old.add(lstOld[i]);
                    lstAp33New.add(lstNew[i]);
                    setServAppIds.add(lstNew[i].ServiceAppointmentId);
                    setNewServiceResources.add(lstNew[i].ServiceResourceId);
                }
            }
        }
        System.debug('## Handle After Update End ');

        // Call APs
        if(!lstAp33New.isEmpty()){
            AP33_ManageAssignedResources.onServiceResourceModified(lstAp33Old, lstAp33New, setServAppIds, setNewServiceResources);
        }
    }
    //handle after update end
}