/**
 * @File Name          : 
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 02-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         28-11-2019           ANA         Initial Version
**/
public without sharing class LC04_CreateContract {

    //CT-1247 function to return list of service contract
    @AuraEnabled
    public static List<ServiceContract> fetchServiceCon(Id assetId){
        List<ServiceContract> lstServCon = [SELECT Id, Contract_Status__c FROM ServiceContract WHERE Asset__c = :assetId];
        System.debug('Apex Service Contract: ' + lstServCon);
        return lstServCon;
    }

    @AuraEnabled
    public static Id CreateContract(Id equipId){

        try{
            Date todaydate = Date.newInstance(system.today().year(), system.today().month(), system.today().day()); //RRA - 20200717 - CT-64 - l'ajout du code campagne - reference sur la page confluence 
            Asset objAss = [SELECT Id, AccountId, Account.PersonContactId, Logement__c, Logement__r.Agency__c, Logement__r.Owner__c,Logement__r.Inhabitant__c,Logement__r.Street__c, Logement__r.City__c, Logement__r.Postal_Code__c ,Logement__r.Country__c, Account.Name, Account.BillingStreet, Account.BillingCity, Account.BillingCountry, Account.BillingPostalCode ,Account.BillingState, Account.Campagne__c, Account.Date_fin_retombees__c FROM Asset WHERE Id =:equipId]; //RRA - 20200717 - CT-64 - l'ajout du code campagne - reference sur la page confluence 
            List<Pricebook2> lstPb = [SELECT Id FROM Pricebook2 WHERE RecordType.DeveloperName = 'Vente' AND Ref_Agence__c = true AND IsActive = true AND Agence__c =:objAss.Logement__r.Agency__c ];
            Id pbId = lstPb.size()>0 ? lstPb[0].Id : null;
            ServiceContract svc = new ServiceContract(
                Name = 'Contrat -'+objAss.Account.Name,
                Type__c='Individual',
                Contract_Status__c ='Draft',
                StartDate = Date.Today(),
                Term = 12,
                EndDate = Date.Today().addMonths(12).addDays(-1),
                Home_Owner__c = objAss.Logement__r.Owner__c,
                Inhabitant__c = objAss.Logement__r.Inhabitant__c,
                BillingStreet = objAss.Account.BillingStreet,
                BillingCity = objAss.Account.BillingCity,
                BillingState = objAss.Account.BillingState,
                BillingPostalCode = objAss.Account.BillingPostalCode,
                BillingCountry = objAss.Account.BillingCountry,
                ShippingStreet = objAss.Logement__r.Street__c,
                ShippingCity = objAss.Logement__r.City__c,
                ShippingPostalCode = objAss.Logement__r.Postal_Code__c,
                ShippingCountry = objAss.Logement__r.Country__c,
                Agency__c = objAss.Logement__r.Agency__c,
                AccountId = objAss.AccountId,
                ContactId = objAss.Account.PersonContactId,
                Logement__c = objAss.Logement__c,
                Asset__c = objAss.Id,
                Pricebook2Id = pbId,
                Campagne__c = (todaydate < objAss.Account.Date_fin_retombees__c) ? objAss.Account.Campagne__c : null, //RRA - 20200717 - CT-64 - l'ajout du code campagne - reference sur la page confluence 
                Payeur_du_contrat__c = objAss.AccountId

                //Tax__c = null
            );

            insert svc;

            list<Id> lstServId = new List<Id>{svc.Id};

            Map<String,Decimal> mapTVA =AP48_GestionTVA.getTVAServContr(lstServId);

            ServiceContract svcNew = new ServiceContract(Id=svc.Id,Tax__c=String.valueOf(mapTVA.get(svc.Id)));
            update svcNew;

            System.debug('##### svcNew: '+svcNew);
            return svc.Id;
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }

    }


    // NOTA : This method has a specific test Class : LC01_Acc360EqpDetails_TEST.cls
    @AuraEnabled
    public static void removeLinkEquipementAndAccount(Id eqpId, Date dateDemenagement) {
        
        Id actualAccountIdBeforeMoving, actualLogementIdBeforeMoving;
        Id vacantAccountId;
        


        // Step 0 : On récupère le logement de l'équipement correspondant
        Asset equipement = [SELECT Id, AccountId, Logement__c, Logement__r.Agency__r.Compte_pour_VACANT__c FROM Asset WHERE Id = :eqpId];
        actualAccountIdBeforeMoving = equipement.AccountId;
        actualLogementIdBeforeMoving = equipement.Logement__c;

        //Update 01/07/2021
        //List<Asset> lstEquipement = [SELECT Id, AccountId, Logement__c, Logement__r.Agency__r.Compte_pour_VACANT__c FROM Asset WHERE Id = :eqpId AND ParentId = :eqpId ORDER BY CreatedDate ASC];
        
        if (equipement.Logement__r.Agency__r.Compte_pour_VACANT__c == null) {
            vacantAccountId = System.Label.Id_Compte_Vacant;
        } else {
            vacantAccountId = equipement.Logement__r.Agency__r.Compte_pour_VACANT__c;
        }

        // Step 1 : L'occupant du logement est mis à jour avec un compte "Vacant".
        Logement__c logement = [
            SELECT Id, Inhabitant__c
            FROM Logement__c
            WHERE Id = :actualLogementIdBeforeMoving
            LIMIT 1
        ];
        logement.Inhabitant__c = vacantAccountId;
        update logement;

        // Step 2 : La relation compte/logement est mise à jour pour indiquer la date de fin de la relation et le statut "inactif".
        List<Compte_Logement__c> compteLogements = [
            SELECT Id, Actif__c, End__c
            FROM Compte_Logement__c
            WHERE
                Account__c = :actualAccountIdBeforeMoving AND
                Logement__c = :actualLogementIdBeforeMoving
        ];
        for (Compte_Logement__c compteLogement : compteLogements) {
            compteLogement.Actif__c = false;
            compteLogement.End__c = Date.valueOf(dateDemenagement);
        }
        update compteLogements;

        // Step 3 : Les équipements de ce logement sont associés au nouvel occupant ("Vacant")
        List<Asset> equipements = [SELECT Id FROM Asset WHERE Logement__c = :actualLogementIdBeforeMoving];
        for (Asset eqp : equipements) {
            eqp.AccountId = vacantAccountId;
        }
        update equipements;

        // Step 4 : S'il y a des requêtes VE ouvertes associées au compte, les SA/WO associés sont annulés et les requête fermées.
        // Step 4a : Annulation des RDV de Service (ServiceAppointment)
        List<ServiceAppointment> listSAs = [
            SELECT Id, Status
            FROM ServiceAppointment
            WHERE
                AccountId = :actualAccountIdBeforeMoving AND
                Work_Order__r.AssetId = :eqpId AND
                Status <> 'Cancelled' AND // IN ('None', 'Scheduled') AND
                Work_Order__r.Status <> 'Canceled' AND // added
                Work_Order__r.Case.Type IN ('Maintenance', 'First Maintenance Visit (new contract)') AND
                Work_Order__r.Case.Status <> 'Closed'
        ];

        for (ServiceAppointment sa : listSAs) {
            sa.Status = 'Cancelled';
        }
        update listSAs;

        // Step 4b : Annulation des Ordres d'exécution (WorkOrder)
        List<WorkOrder> listWOs = [
            SELECT Id, Status
            FROM WorkOrder
            WHERE
                AccountId = :actualAccountIdBeforeMoving AND
                AssetId = :eqpId AND
                Status <> 'Cancelled' AND // added
                Case.Type IN ('Maintenance', 'First Maintenance Visit (new contract)') AND
                Case.Status <> 'Closed'
        ];

        for (WorkOrder wo : listWOs) {
            wo.Status = 'Cancelled';
        }
        update listWOs;

        // Step 4c : Fermeture des Requêtes / Cases
        List<Case> listCases = [
            SELECT Id, Status
            FROM Case
            WHERE
                AccountId = :actualAccountIdBeforeMoving AND
                AssetId = :eqpId AND
                Type IN ('Maintenance', 'First Maintenance Visit (new contract)') AND
                Status <> 'Closed'
        ];

        for (Case caseUniq : listCases) {
            caseUniq.Status = 'Closed';
        }
        update listCases;

        // Step 5 : Si le compte a des contrats actifs associés à ce logement, les contrats restent associés au compte et au logement
        // et le statut est mis à jour automatiquement en "A résilier".
        // Fermeture des Contrats
        List<ServiceContract> listContracts = [
            SELECT Id, Contract_Status__c, Motif_d_arr_t_r_siliation_du_contrat__c, Contrat_resilie__c
            FROM ServiceContract
            WHERE
                //Le contrat est arrêté si l'occupant ou propriétaire occupant est payeur du contrat
                Payeur_du_contrat__c = :actualAccountIdBeforeMoving AND
                AccountId = :actualAccountIdBeforeMoving AND
                Logement__c = :actualLogementIdBeforeMoving AND
                Contract_Status__c IN ('Active', 'Pending Payment', 'Draft', 'Pending Client Validation', 'Pending first visit', 'En attente de renouvellement', 'Actif - en retard de paiement')
        ];

        for (ServiceContract contract : listContracts) {
            contract.Contract_Status__c = 'Cancelled';
            contract.Motif_d_arr_t_r_siliation_du_contrat__c = 'Déménagement';
            contract.Contrat_resilie__c = true;
        }
        update listContracts;

    }

}