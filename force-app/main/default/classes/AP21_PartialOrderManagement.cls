/**
 * @File Name          : AP20_NewAccountNewLocation.cls
 * @Description        : 
 * @Author             : SBH
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 02-09-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    15/10/2019         SBH                    Initial Version  (CT-1114)
**/
public with sharing class AP21_PartialOrderManagement {
    
    public static void updateOrderItem(List<ProductTransfer> lstProdTransfers, Set<String> setOrderIds, Set<Id> setProductIds){
        System.debug('## updateOrderItem start');
        System.debug('## updateOrderItem lstProdTransfers '+lstProdTransfers);
        System.debug('## updateOrderItem setOrderIds '+setOrderIds);
        System.debug('## updateOrderItem setProductIds '+setProductIds);
        
        Map<String, OrderItem> mapOrderProductToOrderItem = new Map<String, OrderItem>();
        List<OrderItem> lstOrderItemsToUpdate = new List<OrderItem>();

        // Retrieve order line items 
        List<OrderItem> lstOrderItems = [
            SELECT Quantite_recue__c, Product2Id, OrderId, Quantity FROM OrderItem WHERE OrderId IN :setOrderIds AND Product2Id IN :setProductIds 
        ];

        for(OrderItem oi: lstOrderItems){
            if(oi.OrderId != null && oi.Product2Id != null){
                mapOrderProductToOrderItem.put(String.valueOf(oi.OrderId).substring(0, 15) + '_' + oi.Product2Id + '_' + String.valueOf(oi.Id).substring(0, 15), oi);
            }
            System.debug('## mapOrderProductToOrderItem ' + mapOrderProductToOrderItem);
        }
       
        // loop in product transfers
        for(ProductTransfer ptf: lstProdTransfers){
            for (orderItem oi2:mapOrderProductToOrderItem.Values()){
                System.debug('ptf: ' + ptf.TECH_OrderId__c + '_' + ptf.Product2Id);            
                System.debug('mapOrderProductToOrderItem: ' + mapOrderProductToOrderItem);            
                if(ptf.TECH_OrderId__c != null && ptf.Product2Id != null){
                    if(oi2.Product2Id == ptf.Product2Id){                        
                        System.debug('##Order Item being updated: ' + oi2);
                        if(ptf.Quantity_Lot_received__c != null){                        
                            if(oi2.Quantite_recue__c == null) oi2.Quantite_recue__c = 0;                        
                            if (oi2.Quantite_recue__c < oi2.Quantity) {
                                oi2.Quantite_recue__c += ptf.Quantity_Lot_received__c;                        
                                //BCH 09/02/2021 - decrement the quantity lot received if added to the order item, and add a check to not update the order if it is already exceeding the quantity expected                            
                                lstOrderItemsToUpdate.add(oi2);
                                //Remove the order item from the map as it shouldn't be updated anymore
                                mapOrderProductToOrderItem.keySet().remove(String.valueOf(oi2.OrderId).substring(0, 15) + '_' + oi2.Product2Id + '_' + String.valueOf(oi2.Id).substring(0, 15));
                                break;                            
                            }
                        }
                    }
                }
            }     
            
            
            
            /*
            System.debug('ptf: ' + ptf.TECH_OrderId__c + '_' + ptf.Product2Id);
            
            if(ptf.TECH_OrderId__c != null && ptf.Product2Id != null){
                String orderToProduct = ptf.TECH_OrderId__c + '_' + ptf.Product2Id;
                
                System.debug('## OrderToProduct: ' + orderToProduct );
                
                if(mapOrderProductToOrderItem.containsKey(orderToProduct)){
                    OrderItem oiToUpdate = mapOrderProductToOrderItem.get(orderToProduct);                    
                    System.debug('##Order Item being updated: ' + oiToUpdate);
                    if(ptf.Quantity_Lot_received__c != null){                        
                        if(oiToUpdate.Quantite_recue__c == null) oiToUpdate.Quantite_recue__c = 0;                    
                        
                        if (oiToUpdate.Quantite_recue__c < oiToUpdate.Quantity) {
                            oiToUpdate.Quantite_recue__c += ptf.Quantity_Lot_received__c;                        
                            //BCH 09/02/2021 - decrement the quantity lot received if added to the order item, and add a check to not update the order if it is already exceeding the quantity expected                            
                            lstOrderItemsToUpdate.add(oiToUpdate);                            
                        }
                    }
                }
            }*/
        }


        if(!lstOrderItemsToUpdate.isEmpty()){
            update lstOrderItemsToUpdate;
        }
        // Retrieve order item based on productId

        System.debug('## updateOrderItem end');
    }
}