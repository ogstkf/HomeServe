/**
 * @File Name          : AP17_QuoteSyncedOpportunityLineItem.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 15/10/2019, 12:23:23
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    14/10/2019   AMO     Initial Version
**/
public with sharing class AP17_QuoteSyncedOpportunityLineItem{
    /*
    public static void QuoteSyncedOLI(List<OpportunityLineItem> NewOLI){
        
        Map<String, OpportunityLineItem> mapOLI = new Map<String, OpportunityLineItem>();
        Set<String> setKeyset = new Set<String>();

        for(OpportunityLineItem oppLI : NewOLI){
            mapOLI.put(oppLI.Id, oppLI);
            setKeyset.add(oppLI.TECH_SyncedQuoteId__c);
        }
        system.debug('** ally Map Quote Id and OpportunityLineItem: '+ mapOLI);

        List<SyncingQLI__c> lstCustomSetting = SyncingQLI__c.getAll().values();
        Map<String, String> mapCustomSetting = new Map<String, String>();
        for(SyncingQLI__c q : lstCustomSetting){
            mapCustomSetting.put(q.QLI__c, q.Name);
        }

        String qLineFlds = String.join(new List<String>(mapCustomSetting.keySet()), ',');
        String qLineQuery = 'SELECT '+qLineFlds+ ' ,OpportunityLineItemId  FROM QuoteLineItem WHERE QuoteId IN :setKeyset';

        System.debug('######## ally: qLineQuery: '+qLineQuery);
        List<QuoteLineItem> lstquoteline = Database.query(qLineQuery);
        System.debug('** ally lstquoteline: '+ lstquoteline.size());


        List<QuoteLineItem> lstQLIupdt = new List<QuoteLineItem>();
        for(QuoteLineItem qu : lstquoteline){
            QuoteLineItem qli = new QuoteLineItem();
            for(String fld: mapCustomSetting.keySet()){
                qli.put(fld, getFld(mapOLI.get(qu.OpportunityLineItemId), mapCustomSetting.get(fld)));
            }
            qli.Id = qu.Id;
            lstQLIupdt.add(qli);
                    system.debug('** ally qline: '+ qu);
        }
        system.debug('** ally List to update: '+ lstQLIupdt);
        
        // try{
            // Update lstQLIupdt;
        // }
        // catch(Exception ex){
        //     system.debug('** ally fail to insert: '+ ex.getMessage());
        // }
    }


        public static void QuoteSyncedQLI(List<QuoteLineItem> NewQLI){

        Map<String, QuoteLineItem> mapQLI = new Map<String, QuoteLineItem>();

        for(QuoteLineItem quoteLI : NewQLI){
            mapQLI.put(quoteLI.OpportunityLineItemId, quoteLI);
        }
        system.debug('** ally Map Quote Id and OpportunityLineItem: '+ mapQLI);

        List<OpportunityLineItem> lstoppLI = [SELECT Id FROM OpportunityLineItem WHERE Id IN :mapQLI.KeySet()];
        List<OpportunityLineItem> oppItemList = new List<OpportunityLineItem>();

        List<SyncingQLI__c> lstCustomSetting = SyncingQLI__c.getAll().values();
        Map<String, String> mapCustomSetting = new Map<String, String>();
        for(SyncingQLI__c q : lstCustomSetting){
            mapCustomSetting.put(q.QLI__c, q.Name);
        } 

        for(OpportunityLineItem opp : lstoppLI){

            OpportunityLineItem oppline = new OpportunityLineItem();

            for(String fld: mapCustomSetting.keySet()){
                oppline.put(fld, getFld(oppline, mapCustomSetting.get(fld)));
            }

            oppItemList.add(oppline);
        }
        
        try{
            Update oppItemList;
        }
        catch(Exception ex){
            system.debug('** ally fail to insert: '+ ex.getMessage());
        }
    }

    public static Object getFld(sObject obj, String fld){
        String[] fldArr = fld.split('([.])');
        Object retVal;
        while(fldArr.size()>1){
            String fldStr = fldArr.remove(0);
            obj = obj.getSobject(fldStr);
        }
        retVal = obj.get(fldArr[0]);
        
        return retVal;
    }
    */

}