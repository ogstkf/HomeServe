/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 08-06-2021
 * @last modified by  : ARA
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-20-2020   ZJO   Initial Version
**/

@isTest
public with sharing class AP74_MaintenancePlan_TEST {

    static User adminUser;
    static Account testAcc = new Account();
    static sofactoapp__Raison_Sociale__c testSofacto;
    static ServiceTerritory testAgence = new ServiceTerritory();
    static Logement__c testLogement = new Logement__c();
    static Product2 testProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PriceBookEntry PrcBkEnt = new PriceBookEntry();
    static Asset testAsset = new Asset();
    static WorkType testWT = new WorkType();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstCli = new List<ContractLineItem>();
    

    static{
        adminUser = TestFactory.createAdminUser('TestUser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){

            //Create Account
            testAcc = TestFactory.CreateAccount('TestAcc');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
            insert testAcc;

            //Update Account
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //Create Sofacto
            testSofacto = new sofactoapp__Raison_Sociale__c(
                Name = 'test sofacto 1',
                sofactoapp__Credit_prefix__c = '1234',
                sofactoapp__Invoice_prefix__c = '2134'
            );
            insert testSofacto;

            //Create Operating Hour
            OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
            insert OprtHour;

            //Create Agence
            testAgence = new ServiceTerritory(
                Name = 'test agence 1',
                Agency_Code__c = '0001',
                IsActive = True,
                OperatingHoursId = OprtHour.Id,
                Sofactoapp_Raison_Social__c = testSofacto.Id
            );
            insert testAgence;

            //Create Logement
            testLogement = TestFactory.createLogement(testAcc.Id, testAgence.Id);
            insert testLogement;

            //Create Product
            testProd = TestFactory.createProduct('TestProd');
            testProd.IsBundle__c = true;
            testProd.Equipment_family__c = 'Pompe à chaleur';          
            testProd.Equipment_type1__c = 'Pompe à chaleur air/eau';
            testProd.ProductCode = 'VF-000';
            insert testProd;

            //Create Asset
            testAsset = new Asset(
                Name = 'Test Asset',
                Product2Id = testProd.Id,
                AccountId = testAcc.Id,
                Status = 'Actif',
                Logement__c = testLogement.Id
            );
            insert testAsset;

            //Create WorkType
            testWT = new WorkType(
                Name = 'Entretien Visite Sous Contrat Pompe à chaleur air/eau',
                Type__c = 'Maintenance',
                Reason__c = 'Visite sous contrat',
                Equipment_type__c = 'Pompe à chaleur air/eau',
                EstimatedDuration = 7,
                Type_de_client__c = 'Tous', 
                Agence__c = 'Toutes'
            );
            insert testWT;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create PricebookEntry
            PriceBookEntry PrcBkEnt = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, testProd.Id, 100);
            insert PrcBkEnt;

            //Create List of ServiceContracts
            lstServCon = new List<ServiceContract>{
                new ServiceContract(
                    Name = 'TestCon 3',
                    RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels'),
                    Contract_Renewed__c = false,
                    Contract_Status__c = 'Draft',
                    AccountId = testAcc.Id,
                    Pricebook2Id = lstPrcBk[0].Id,
                    Asset__c = testAsset.Id,
                    StartDate = Date.Today(),
                    EndDate = Date.Today().addYears(1),
                    Frequence_de_maintenance__c = 3,
                    Type_de_frequence__c = 'Days',
                    Debut_de_la_periode_de_maintenance__c = 1,
                    Fin_de_periode_de_maintenance__c = 6,
                    Periode_de_generation__c = 5,
                    Type_de_periode_de_generation__c = 'Days',
                    Date_du_prochain_OE__c = Date.Today().addDays(7),
                    Generer_automatiquement_des_OE__c = true,
                    Horizon_de_generation__c = 4
                ),
                new ServiceContract(
                    Name = 'TestCon 4',
                    RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives'),
                    Contract_Renewed__c = false,
                    Contract_Status__c = 'Draft',
                    AccountId = testAcc.Id,
                    Pricebook2Id = lstPrcBk[0].Id,
                    StartDate = Date.Today(),
                    EndDate = Date.Today().addYears(1),
                    Frequence_de_maintenance__c = 3,
                    Type_de_frequence__c = 'Days',
                    Debut_de_la_periode_de_maintenance__c = 1,
                    Fin_de_periode_de_maintenance__c = 6,
                    Periode_de_generation__c = 5,
                    Type_de_periode_de_generation__c = 'Days',
                    Date_du_prochain_OE__c = Date.Today().addDays(7),
                    Generer_automatiquement_des_OE__c = true,
                    Horizon_de_generation__c = 4
                ),
                new ServiceContract(
                    Name = 'TestCon 5',
                    RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_collectifs_publics'),
                    Contract_Renewed__c = false,
                    Contract_Status__c = 'Active',
                    AccountId = testAcc.Id,
                    Pricebook2Id = lstPrcBk[0].Id,
                    StartDate = Date.Today(),
                    EndDate = Date.Today().addYears(1),
                    Frequence_de_maintenance__c = 3,
                    Type_de_frequence__c = 'Days',
                    Debut_de_la_periode_de_maintenance__c = 1,
                    Fin_de_periode_de_maintenance__c = 6,
                    Periode_de_generation__c = 5,
                    Type_de_periode_de_generation__c = 'Days',
                    Date_du_prochain_OE__c = Date.Today().addDays(7),
                    Generer_automatiquement_des_OE__c = true,
                    Horizon_de_generation__c = 4
                ),
                new ServiceContract(
                    Name = 'TestCon 6',
                    RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives'),
                    Contract_Renewed__c = false,
                    Contract_Status__c = 'Active',
                    AccountId = testAcc.Id,
                    Pricebook2Id = lstPrcBk[0].Id,
                    StartDate = Date.Today(),
                    EndDate = Date.Today().addYears(1),
                    Frequence_de_maintenance__c = 3,
                    Type_de_frequence__c = 'Days',
                    Debut_de_la_periode_de_maintenance__c = 1,
                    Fin_de_periode_de_maintenance__c = 6,
                    Periode_de_generation__c = 5,
                    Type_de_periode_de_generation__c = 'Days',
                    Date_du_prochain_OE__c = Date.Today().addDays(7),
                    Generer_automatiquement_des_OE__c = true,
                    Horizon_de_generation__c = 4
                )                    
            };
            insert lstServCon;
            

            //Create List of CLIs
            ContractLineItem testCli1 = TestFactory.createContractLineItem(lstServCon[1].Id, PrcBkEnt.Id, 100, 25);
            testCli1.AssetId = testAsset.Id;
            testCli1.StartDate = Date.Today();
            testCli1.EndDate = Date.Today().addDays(5);
            testCli1.IsActive__c = true;


            ContractLineItem testCli2 = TestFactory.createContractLineItem(lstServCon[2].Id, PrcBkEnt.Id, 100, 25);
            testCli1.AssetId = testAsset.Id;
            testCli1.StartDate = Date.Today();
            testCli1.EndDate = Date.Today().addDays(5);
            testCli1.IsActive__c = true;

            ContractLineItem testCli3 = TestFactory.createContractLineItem(lstServCon[2].Id, PrcBkEnt.Id, 100, 25);
            testCli2.AssetId = testAsset.Id;
            testCli2.StartDate = Date.Today();
            testCli2.EndDate = Date.Today().addDays(10);
            testCli2.IsActive__c = false;

            ContractLineItem testCli4 = TestFactory.createContractLineItem(lstServCon[3].Id, PrcBkEnt.Id, 110, 25);
            testCli3.AssetId = testAsset.Id;
            testCli3.StartDate = Date.Today();
            testCli3.EndDate = Date.Today().addDays(15);
            testCli3.IsActive__c = true;

            lstCli.add(testCli1);
            lstCli.add(testCli2);
            lstCli.add(testCli3);
            lstCli.add(testCli4);

        }
    }

    @isTest
    public static void testInsertConIndividual(){
        System.runAs(adminUser){
            Test.StartTest();
            ServiceContract ServConIndividual = new ServiceContract(
                Name = 'TestCon 1',
                RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels'),     
                Contract_Renewed__c = false,
                Contract_Status__c = 'Active',
                AccountId = testAcc.Id,
                Pricebook2Id = lstPrcBk[0].Id,
                Asset__c = testAsset.Id,
                StartDate = Date.Today(),
                EndDate = Date.Today().addYears(1),
                Frequence_de_maintenance__c = 3,
                Type_de_frequence__c = 'Days',
                Debut_de_la_periode_de_maintenance__c = 1,
                Fin_de_periode_de_maintenance__c = 6,
                Periode_de_generation__c = 5,
                Type_de_periode_de_generation__c = 'Days',
                Date_du_prochain_OE__c = Date.Today().addDays(7),
                Generer_automatiquement_des_OE__c = true,
                Horizon_de_generation__c = 4
            );
            insert ServConIndividual;
            Test.StopTest();

            MaintenancePlan MpConInd = [SELECT Id, ServiceContractId FROM MaintenancePlan WHERE ServiceContractId =:ServConIndividual.Id];
            System.assertEquals(MpConInd.ServiceContractId, ServConIndividual.Id);
            MaintenanceAsset MaConInd = [SELECT Id, MaintenancePlanId FROM MaintenanceAsset WHERE MaintenancePlanId =: MpConInd.Id];
            System.assertEquals(MaConInd.MaintenancePlanId, MpConInd.Id);
        }

    }

    @isTest
    public static void testInsertConCollectif(){
        System.runAs(adminUser){
            Test.StartTest();
            ServiceContract ServConCollectif = new ServiceContract(
                Name = 'TestCon 2',
                RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_collectifs_publics'),
                Contract_Renewed__c = false,
                Contract_Status__c = 'Active',
                AccountId = testAcc.Id,
                Pricebook2Id = lstPrcBk[0].Id,
                StartDate = Date.Today(),
                EndDate = Date.Today().addYears(1),
                Frequence_de_maintenance__c = 3,
                Type_de_frequence__c = 'Days',
                Debut_de_la_periode_de_maintenance__c = 1,
                Fin_de_periode_de_maintenance__c = 6,
                Periode_de_generation__c = 5,
                Type_de_periode_de_generation__c = 'Days',
                Date_du_prochain_OE__c = Date.Today().addDays(7),
                Generer_automatiquement_des_OE__c = true,
                Horizon_de_generation__c = 4    
            );
            insert ServConCollectif;
            Test.StopTest();

            // MaintenancePlan MpConCollectif = [SELECT Id, ServiceContractId, WorkTypeId FROM MaintenancePlan WHERE ServiceContractId =:ServConCollectif.Id];
            // System.assertEquals(MpConCollectif.ServiceContractId, ServConCollectif.Id);
            // System.assertEquals(MpConCollectif.WorkTypeId, null);
        }
    }

    @isTest
    public static void testUpdateConIndividual(){
        System.runAs(adminUser){
            
            Test.StartTest();
            ServiceContract conIndividual = lstServCon[0];
            conIndividual.Contract_Status__c = 'Active';
            update conIndividual;
            Test.StopTest();

            MaintenancePlan MpInd = [SELECT Id, ServiceContractId FROM MaintenancePlan WHERE ServiceContractId =:conIndividual.Id];
            System.assertEquals(MpInd.ServiceContractId, conIndividual.Id);
            MaintenanceAsset MaInd = [SELECT Id, MaintenancePlanId FROM MaintenanceAsset WHERE MaintenancePlanId =:MpInd.Id];
            System.assertEquals(MaInd.MaintenancePlanId, MpInd.Id);
        }
    }

    @isTest
    public static void testUpdateConCollectif(){
        System.runAs(adminUser){
            
            ServiceContract conCollectif = lstServCon[1];
            Test.StartTest();
            insert lstCli[0];
            conCollectif.Contract_Status__c = 'Active';
            update conCollectif;
            Test.StopTest();
            System.debug(conCollectif);
            System.debug(conCollectif.Id);
            MaintenancePlan MpCollectif = [SELECT Id, ServiceContractId FROM MaintenancePlan WHERE ServiceContractId =:conCollectif.Id];
            System.assertEquals(MpCollectif.ServiceContractId, conCollectif.Id);     
        }
    }

    @isTest
    public static void testUpdateStartEndDate(){
        System.runAs(adminUser){
            
            ServiceContract servCon = lstServCon[0];
            servCon.Contract_Status__c = 'Active';
            update servCon;

            Test.StartTest();       
            servCon.StartDate = Date.Today().addDays(2);
            servCon.EndDate = Date.Today().addYears(2);
            update servCon;
            Test.StopTest();

            MaintenancePlan Mp = [SELECT Id, StartDate, EndDate FROM MaintenancePlan WHERE ServiceContractId =:servCon.Id];
            System.assertEquals(Mp.StartDate, servCon.StartDate);
            System.assertEquals(Mp.EndDate, servCon.EndDate);
        }
    }

    @isTest
    public static void testUpdateCaseSA(){
        System.runAs(adminUser){
            
            ServiceContract sc = lstServCon[1];
            // sc.Contract_Status__c = 'Active';
            // update sc;

            Test.StartTest();
            sc.Contract_Status__c = 'Cancelled';   
            update sc;
            Test.StopTest();      
        }
    }
    
    @isTest
    public static void testInsertCli(){
        System.runAs(adminUser){
            Test.StartTest();
            insert lstCli;
            // AP74_MaintenancePlan.createMaintenancePlan(lstServCon);
            Test.StopTest();

            // MaintenancePlan MP = [SELECT Id, ServiceContractId FROM MaintenancePlan WHERE ServiceContractId =:conLineItem.ServiceContractId];
            // System.assertEquals(MP.ServiceContractId, conLineItem.ServiceContractId);
            
            // MaintenanceAsset MA = [SELECT Id, MaintenancePlanId FROM MaintenanceAsset WHERE MaintenancePlanId =:MP.Id];
            // System.assertEquals(MA.MaintenancePlanId, MP.Id);
        }
    }

//     @isTest
//     public static void testUpdateCli(){
//         System.runAs(adminUser){
//             ContractLineItem conLineItem1 = lstCli[1];
//             insert conLineItem1;

//             Test.StartTest();
//             conLineItem1.IsActive__c = true;
//             update conLineItem1;
//             Test.StopTest();

//             // MaintenancePlan MP = [SELECT Id, ServiceContractId FROM MaintenancePlan WHERE ServiceContractId =:conLineItem1.ServiceContractId];
//             // System.assertEquals(MP.ServiceContractId, conLineItem1.ServiceContractId);
            
//             // MaintenanceAsset MA = [SELECT Id, MaintenancePlanId FROM MaintenanceAsset WHERE MaintenancePlanId =:MP.Id];
//             // System.assertEquals(MA.MaintenancePlanId, MP.Id);
//         }
//     }


}