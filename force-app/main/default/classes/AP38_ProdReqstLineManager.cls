/**
 * @File Name          : AP38_ProdReqstLineManager.cls
 * @Description        :
 * @Author             : Spoon Consulting (KJB)
 * @Group              :
 * @Last Modified By   : AMO
 * @Last Modified On   : 19/12/2019, 09:52:14
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                   Author              Modification
 *==============================================================================
 * 1.0       06/11/2019       Spoon Consulting (KJB)     Initial Version
**/
public with sharing class AP38_ProdReqstLineManager {

    public static void changePrliStatus(List<OrderItem> lstOrdI){
        Map<Id, Id> mapOrdProd = new Map<Id, Id>();
        Map<Id, OrderItem> mapOrdOrdItm = new Map<Id, OrderItem>();
        Map<Id, OrderItem> mapPrlOrditm = new Map<Id, OrderItem>();
        Set<Id> setprodId = new Set<Id>();
        Set<Id> setLocationIds = new Set<Id>(); // SH - 2020-01-22

        for(OrderItem ord : lstOrdI){
            mapOrdOrdItm.put(ord.OrderId, ord);
            setprodId.add(ord.Product2Id);
        }

        // SH - 2020-01-22 - Added "DestinationLocationId"
        List<ProductRequestLineItem> lstPrli = [
            SELECT Id, Commande__c, Product2Id, QuantityRequested, Status, DestinationLocationId,
            DestinationLocation.Agence__r.Inventaire_en_cours__c,
            DestinationLocation.Agence__c
            FROM ProductRequestLineItem
            WHERE
                Commande__c IN :mapOrdOrdItm.keyset() AND
                Product2Id IN :setprodId
        ];
        for(ProductRequestLineItem prl : lstPrli){
            mapPrlOrditm.put(prl.Commande__c, mapOrdOrdItm.get(prl.Commande__c));
            mapOrdProd.put(prl.Id, prl.Product2Id);
            setLocationIds.add(prl.DestinationLocationId); // SH - 2020-01-22
        }

        Map<Id, Product2> mapProd = new Map<Id, Product2>();
        for(Product2 prod : [SELECT Id, Lot__c FROM Product2 WHERE Id IN :mapOrdProd.values()]){
            mapProd.put(prod.Id, prod);
        }

        // SH - 2020-01-22 - Get ProductItem
        List<ProductItem> lstProductItems = [
            SELECT Id, Quantite_allouee__c, Product2Id, LocationId
            FROM ProductItem
            WHERE
                Product2Id IN :setprodId AND
                LocationId IN :setLocationIds
        ];

        List<ProductRequestLineItem> lstUpdatedPRLI = new List<ProductRequestLineItem>();

        for(ProductRequestLineItem prli : lstPRLI){
            // SH - Why not with ">=" ??
            if((mapPrlOrditm.get(prli.Commande__c).Quantite_recue__c * mapProd.get(prli.Product2Id).Lot__c) == prli.QuantityRequested){
               prli.Status = AP_Constant.prliReserveAPreparer;
            }
            else if((mapPrlOrditm.get(prli.Commande__c).Quantite_recue__c * mapProd.get(prli.Product2Id).Lot__c) > prli.QuantityRequested){
               prli.Status = AP_Constant.prliReserveAPreparer;
            }
            if((mapPrlOrditm.get(prli.Commande__c).Quantite_recue__c * mapProd.get(prli.Product2Id).Lot__c) >= prli.QuantityRequested){
                prli.Status = AP_Constant.prliReserveAPreparer;
            }
            lstUpdatedPRLI.add(prli);

            // SH - 2020-01-22 - BEGIN
            if ((prli.Status == 'Réservé à préparer') && (prli.Commande__c != null)) {
                Boolean found = false;
                for (ProductItem pi : lstProductItems) {         
                    if ((prli.Product2Id == pi.Product2Id) && (prli.DestinationLocationId == pi.LocationId)
                        && prli.DestinationLocation.Agence__c != null 
                        && prli.DestinationLocation.Agence__r.Inventaire_en_cours__c == false
                    ) {
                        if (pi.Quantite_allouee__c == null) { pi.Quantite_allouee__c = 0; }

                        pi.Quantite_allouee__c += prli.QuantityRequested;
                        found = true;
                        break;
                    }
                }

                if (!found && prli.DestinationLocation.Agence__c != null 
                        && prli.DestinationLocation.Agence__r.Inventaire_en_cours__c == false) {
                    // Create Product Item
                    ProductItem pi = new ProductItem();
                    pi.Product2Id = prli.Product2Id;
                    pi.LocationId = prli.DestinationLocationId;
                    pi.QuantityOnHand = 0;
                    pi.Quantite_allouee__c = prli.QuantityRequested;

                    lstProductItems.add(pi);
                }
            }
            // SH - 2020-01-22 - END
        }

        update lstUpdatedPRLI;
        System.debug('##lstProductItems: ' + lstProductItems);
        upsert lstProductItems; // SH - 2020-01-22
    }
}