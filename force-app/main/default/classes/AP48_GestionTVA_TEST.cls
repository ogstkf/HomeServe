/**
 * @File Name          : AP48_GestionTVA_TEST.cls
 * @Description        : 
 * @Author             : SHU
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         24-11-2019     		     SHU        Initial Version
**/
@ isTest
public with sharing class AP48_GestionTVA_TEST {
    static User mainUser;
    //static Account testAcc = new Account();
    static List<Account> lstAccount = new List<Account>();
    static Opportunity opp = new Opportunity();
    static List<Product2> lstProd = new List<Product2>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Quote> lstQuote = new List<Quote>();
    static List<Contract> lstContract = new List<Contract>();
    static List<ServiceContract> lstServiceContract = new List<ServiceContract>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();    
    static List<Logement__c> lstLogement = new List<Logement__c>();
    
    static{
        mainUser = TestFactory.createAdminUser('UserAP48@example.com', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            //Create account
            lstAccount = new List<Account> {
                new Account( Name ='Apple Inc'
                            //,Type = ''
                            //,recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId()
                            ),
                new Account( Name ='Google Inc'
                            ,Type = 'Entreprise'
                            //,recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId()
                            ),
                new Account( Name ='Microsoft Inc'
                            ,Type = 'Autre'
                            //,recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId()
                            )
            };     
            insert lstAccount; 
            
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAccount.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstAccount.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstAccount.get(2).Id);
            lstAccount.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstAccount.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstAccount.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            
            update lstAccount;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('Business Operating hours');
            insert opHrs;

            //create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'Sofacto', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa;

            //create service territory
            srvTerr = TestFactory.createServiceTerritory('Zone sud',opHrs.Id,sofa.Id);
            // srvTerr.Sofactoapp_Raison_Social__c = sofa.Id;
            insert srvTerr;

            //create opportunity
            opp = TestFactory.createOpportunity('Prestation panneaux',lstAccount[0].Id);
            insert opp; 

            //Create Logement
            lstLogement = new List<Logement__c> {
                TestFactory.createLogement(lstAccount[0].Id, srvTerr.Id),
                TestFactory.createLogement(lstAccount[0].Id, srvTerr.Id),
                TestFactory.createLogement(lstAccount[0].Id, srvTerr.Id)
            };
            lstLogement[0].Professional_Use__c = 'Oui (plus de 50% d\'usage professionnel)';
            lstLogement[0].Age__c = 'Plus_2ans';

            lstLogement[1].Professional_Use__c = 'Non (moins de 50% d\'usage professionnel)';
            lstLogement[1].Age__c = 'Plus_2ans';

            lstLogement[2].Professional_Use__c = 'Non (moins de 50% d\'usage professionnel)';
            lstLogement[2].Age__c = 'Moins_2ans';

            insert lstLogement;   

            //Creating products
            lstProd.add(TestFactory.createProductAsset('Chaudiere fioul'));
            lstProd.add(TestFactory.createProductAsset('Chaudiere gaz'));
            lstProd[0].Equipment_family__c = 'Chaudière';
            lstProd[0].Equipment_type1__c = 'Chaudière fioul';
            lstProd[1].Equipment_family__c = 'Chaudière';
            lstProd[1].Equipment_type1__c = 'Chaudière fioul';
            
            insert lstProd;

            //Create Assets
            lstAsset = new List<Asset> {
                TestFactory.createAsset('THEMA C 25 EV', AP_Constant.assetStatusActif, lstLogement[0].Id),
                TestFactory.createAsset('HOVAL TKO 30KW', AP_Constant.assetStatusActif, lstLogement[1].Id), // HEP > False
                TestFactory.createAsset('HOVAL TKO 10KW', AP_Constant.assetStatusActif, lstLogement[1].Id), //Contract HEP > False
                TestFactory.createAsset('HOVAL TKO 10KW', AP_Constant.assetStatusActif, lstLogement[1].Id) //Contract HEP > False
            };
            lstAsset[0].Product2Id = lstProd[0].Id;
            lstAsset[0].AccountId = lstAccount[0].Id;
            lstAsset[0].HEP_Equipment__c = true;

            lstAsset[1].Product2Id = lstProd[1].Id;
            lstAsset[1].AccountId = lstAccount[0].Id;
            lstAsset[1].HEP_Equipment__c = false;

            lstAsset[2].Product2Id = lstProd[1].Id;
            lstAsset[2].AccountId = lstAccount[0].Id;
            lstAsset[2].HEP_Equipment__c = true;   

            lstAsset[3].Product2Id = lstProd[0].Id;
            lstAsset[3].AccountId = lstAccount[0].Id;
            lstAsset[3].HEP_Equipment__c = true;                       

            insert lstAsset  ;                  

            //Create Quotes
            lstQuote = new List<Quote> {
                new Quote(  Name ='Prestations'
                            ,OpportunityId = opp.Id
                            ,Equipement__c = lstAsset[0].Id
                            ,Logement__c = lstLogement[0].Id), //Scenario 1 => 20%
                new Quote(  Name ='Travaux'
                            ,OpportunityId = opp.Id
                            ,Equipement__c = lstAsset[0].Id
                            ,Logement__c = lstLogement[1].Id), //Scenario 2 => 5.5%
                new Quote(  Name ='Remplacement'
                            ,OpportunityId = opp.Id
                            ,Equipement__c = lstAsset[1].Id
                            ,Logement__c = lstLogement[1].Id), //Scenario 3 => 10%
                new Quote(  Name ='Sous-traitance totale'
                            ,OpportunityId = opp.Id
                            ,Equipement__c = lstAsset[1].Id
                            ,Logement__c = lstLogement[2].Id), //Scenario 4  => 20%
                new Quote(  Name ='Sous-traitance totale'
                            ,OpportunityId = opp.Id
                            ,Equipement__c = lstAsset[1].Id
                            ,Logement__c = null), //Scenario 5  => null
                new Quote(  Name ='Sous-traitance totale'
                            ,OpportunityId = opp.Id
                            ,Equipement__c = lstAsset[1].Id
                            ,Logement__c = null
                            ,Type_de_devis__c = 'Vente de pièces/équipements au guichet') //Scenario 6  => 20%                            
            };     
            insert lstQuote;            

            opp.Tech_Synced_Devis__c = lstQuote[0].Id;
            update opp;

            //Create contracts
            lstContract = new List<Contract> {
                TestFactory.createContract('Contrat chaudière', lstAccount[1].Id), //scenario 1
                TestFactory.createContract('Contrat gaz', lstAccount[1].Id), // scenario 2
                TestFactory.createContract('Contrat Radiateur gaz', lstAccount[1].Id), // scenario 3
                TestFactory.createContract('Contrat Radiateur gaz', lstAccount[2].Id) // scenario 4 Account NOT IN Entre/parti/ collectifs privés et publics
            };
            lstContract[0].Covered_Equipment__c = lstAsset[3].Id; //asset 0 with product equipment type Chaudière fioul
            lstContract[1].Covered_Equipment__c = lstAsset[2].Id; //asset 0 with product equipment type Radiateur gaz & HEP_Equipment true
            lstContract[2].Covered_Equipment__c = lstAsset[1].Id; //asset 0 with product equipment type Radiateur gaz & HEP_Equipment false

            insert lstContract;          

            //Create Service contracts
            lstServiceContract = new List<ServiceContract> {
                TestFactory.createServiceContract('Contrat chaudière', lstAccount[1].Id), //scenario 1
                TestFactory.createServiceContract('Contrat gaz', lstAccount[1].Id), // scenario 2
                TestFactory.createServiceContract('Contrat Radiateur gaz', lstAccount[1].Id), // scenario 3
                TestFactory.createServiceContract('Contrat Radiateur gaz', lstAccount[2].Id) // scenario 4 Account NOT IN Entre/parti/ collectifs privés et publics
            };
            lstServiceContract[0].Asset__c = lstAsset[3].Id; //asset 0 with product equipment type Chaudière fioul
            lstServiceContract[1].Asset__c = lstAsset[2].Id; //asset 0 with product equipment type Radiateur gaz & HEP_Equipment true
            lstServiceContract[2].Asset__c = lstAsset[1].Id; //asset 0 with product equipment type Radiateur gaz & HEP_Equipment false

            insert lstServiceContract;                      
        }
    }

    @isTest
    public static void testTVADevis(){
        System.runAs(mainUser){
            Map<String, decimal> mapDevisId_TVA = new Map<String, decimal>();

            Test.startTest();
            mapDevisId_TVA = AP48_GestionTVA.getTVADevis(new list<Id> {lstQuote[0].Id,lstQuote[1].Id,lstQuote[2].Id,lstQuote[3].Id,lstQuote[4].Id,lstQuote[5].Id});
            Test.stopTest();

            system.assertEquals(20, mapDevisId_TVA.get(lstQuote[0].Id), 'Scenario 1');
            system.assertEquals(5.5, mapDevisId_TVA.get(lstQuote[1].Id), 'Scenario 2');
            system.assertEquals(10, mapDevisId_TVA.get(lstQuote[2].Id), 'Scenario 3');
            system.assertEquals(20, mapDevisId_TVA.get(lstQuote[3].Id), 'Scenario 4');
            system.assertEquals(null, mapDevisId_TVA.get(lstQuote[4].Id), 'Scenario 5');
            system.assertEquals(20, mapDevisId_TVA.get(lstQuote[5].Id), 'Scenario 6');
            //system.assertEquals(Expected, Actual, Msg);
        }
    }    

    @isTest
    public static void testTVAServContract(){
        System.runAs(mainUser){
            Map<String, decimal> mapServContractId_TVA = new Map<String, decimal>();

            Test.startTest();
            mapServContractId_TVA =  AP48_GestionTVA.getTVAServContr(new list<Id> {lstServiceContract[0].Id,lstServiceContract[1].Id, lstServiceContract[2].Id, lstServiceContract[3].Id});
            Test.stopTest();

            /*system.assertEquals(10, mapServContractId_TVA.get(lstServiceContract[0].Id), 'Scenario 1');
            system.assertEquals(5.5, mapServContractId_TVA.get(lstServiceContract[1].Id), 'Scenario 2');
            system.assertEquals(10, mapServContractId_TVA.get(lstServiceContract[2].Id), 'Scenario 3');
            system.assertEquals(20, mapServContractId_TVA.get(lstServiceContract[3].Id), 'Scenario 4');   */       
            //system.assertEquals(Expected, Actual, Msg);
        }
    }        

}