/**
 * @File Name          : AP14_SetCaseStatus_TEST.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11-05-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/02/2020   RRJ     Initial Version
**/
@isTest
public with sharing class AP14_SetCaseStatus_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Test Class for AP14_SetCaseStatus
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 10-Sept-2019  DMU    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  

    static User mainUser;
    static list <ServiceAppointment> sappLst;
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<Account> lstTestAcc = new List<Account>();
    static Product2 lstTestProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();    
    static List<Case> lstCase= new List<Case>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static sofactoapp__Compte_auxiliaire__c aux0;
    static Bypass__c bp = new Bypass__c();

    //Case status
    public static string caStatusInProg = 'In Progress';
    public static string caStatusPendingActFromCham = 'Pending Action from Cham';
    public static string caStatusPendingActFromClient = 'Pending Action from Client';

    //SA Status
    public static string saStatusDoneClientAbsent = 'Done client absent';

    static {
        mainUser = TestFactory.createAdminUser('AP14_SetCaseStatus@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP16,WorkOrderTrigger,AP76,AP53';
        insert bp;

        System.runAs(mainUser){

            lstTestAcc.add(TestFactory.createAccount('testAcc'+1));
            lstTestAcc[0].BillingPostalCode = '1233456';
            lstTestAcc[0].recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert lstTestAcc;

            // creating compte auxiliaire
            aux0 = DataTST.createCompteAuxilaire(lstTestAcc[0].Id);
            lstTestAcc[0].sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update lstTestAcc;

            //create products            
            // lstTestProd.add(TestFactory.createProduct('testProd'));            
            // insert lstTestProd;

                    //Create your product
        Product2 lstTestProd = new Product2(Name = 'Product X',
                                    ProductCode = 'Pro-X',
                                    Statut__c = 'Approuvée',
                                    isActive = true,
                                    Equipment_family__c = 'Chaudière',
                                    Equipment_type1__c = 'Chaudière gaz'
        );
        insert lstTestProd;
           
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
           
            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd.Id, 0));          
            insert lstPrcBkEnt;
            
            //service contract           
            lstServCon.add(TestFactory.createServiceContract('testServCon', lstTestAcc[0].Id));
            lstServCon[0].PriceBook2Id = lstPrcBk[0].Id;          
            insert lstServCon;   
                        
            //contract line item
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));      
            lstConLnItem[0].Customer_Absences__c = 1;
            lstConLnItem[0].Customer_Allowed_Absences__c = 3;
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));      
            insert lstConLnItem;            

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            //creating raison sociale
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            //create service territory
            srvTerr = TestFactory.createServiceTerritory('test Territory',opHrs.Id, raisonSocial.Id);
            insert srvTerr;

            //creating logement
            lstLogement.add(TestFactory.createLogement(lstTestAcc[0].Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = lstTestAcc[0].Id;
            insert lstLogement;

            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstTestProd.Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = lstTestAcc[0].Id;
            insert lstAsset;

             // create case
            lstCase.add(TestFactory.createCase(lstTestAcc[0].Id, 'A1 - Logement', lstAsset[0].Id));
            lstCase[0].Contract_Line_Item__c = lstConLnItem[0].Id;
            lstCase.add(TestFactory.createCase(lstTestAcc[0].Id, 'A1 - Logement', lstAsset[0].Id));
            lstCase[1].Contract_Line_Item__c = lstConLnItem[1].Id;
            insert lstCase;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Commissioning';

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].caseId = lstCase[0].Id;  
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].Type__c = 'Maintenance';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[1].caseId = lstCase[1].Id;  
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[1].Type__c = 'Installation';
            insert lstWrkOrd;         
        }
    }

    @IsTest(SeeAllData=true)
    public static void insertSAStatus(){
        System.runAs(mainUser){
            sappLst = new List<ServiceAppointment>{
                                    new ServiceAppointment(
                                            Status = saStatusDoneClientAbsent,                                            
                                            ParentRecordId = lstWrkOrd[0].Id,                                         
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Work_Order__c = lstWrkOrd[0].Id                       
                                    ),
                                    new ServiceAppointment(
                                            Status = saStatusDoneClientAbsent,                                            
                                            ParentRecordId = lstWrkOrd[1].Id,                                         
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Work_Order__c = lstWrkOrd[1].Id                       
                                    )
            };
            Test.startTest();
               insert sappLst;
            Test.stopTest();

            list <Case> CaseLstUpd = [select id, status from Case where id in: lstCase];
            system.assertEquals(caStatusInProg, CaseLstUpd[0].status);
            system.assertEquals(caStatusPendingActFromCham, CaseLstUpd[1].status);
        }
    }

    @IsTest(SeeAllData=true)
    public static void updateSAStatus(){
        System.runAs(mainUser){

            sappLst = new List<ServiceAppointment>{
                                    new ServiceAppointment(
                                            // Status = 'Scheduled',  
                                            Status = 'Done OK',                                      
                                            ParentRecordId = lstWrkOrd[0].Id,                                         
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Work_Order__c = lstWrkOrd[0].Id,
                                            // Service_Resource_Level__c = 'Junior',
                                            Category__c = 'Pose'
                                            ,Residence__c = lstLogement[0].Id
                                    ),
                                    new ServiceAppointment(
                                            // Status = 'Scheduled',    
                                            Status = 'Done OK',                                            
                                            ParentRecordId = lstWrkOrd[0].Id,                                         
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Work_Order__c = lstWrkOrd[1].Id,
                                            // Service_Resource_Level__c = 'Junior',
                                            Category__c = 'Pose'
                                            ,Residence__c = lstLogement[0].Id
                                    )

            };
            insert sappLst;

            Test.startTest();
                sappLst[0].Unattainable_installation__c = true;
                sappLst[1].Unattainable_installation__c = true;
                update sappLst;
            Test.stopTest();

            list <Case> CaseLstUpd = [select id, status from Case where id in: lstCase];
            // system.assertEquals(caStatusPendingActFromClient, CaseLstUpd[0].status);
            // system.assertEquals(caStatusPendingActFromClient, CaseLstUpd[1].status);
        }
    }

}