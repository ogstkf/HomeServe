/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 08-24-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-29-2020   ZJO   Initial Version
**/

@isTest
public with sharing class AP72_CodeCampagne_TEST {

    static User adminUser;
    static Campaign testCampaign  = new Campaign();
    static Account testAcc = new Account();
    static Product2 testProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();

    static{

        adminUser = TestFactory.createAdminUser('TestUser', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){

            //Create Campaign
            testCampaign = new Campaign(Name = 'TestCampaign');
            insert testCampaign;
            
            //Create Account
            testAcc = TestFactory.createAccount('TestAccount');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.Campagne__c = testCampaign.Id;
            testAcc.Date_fin_retombees__c = System.today().addDays(2);
            insert testAcc;

            //Update Account
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //Create Product
            testProd = TestFactory.createProduct('TestProd');
            insert testProd;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //List of SC
            lstServCon = new List<ServiceContract>{
                new ServiceContract(
                    name = 'servcon 1',
                    Contract_Status__c = 'Draft',
                    Pricebook2Id = lstPrcBk[0].Id,
                    AccountId = testAcc.Id
                ),
                new ServiceContract(
                    name = 'servcon 2',
                    Contract_Status__c = 'Draft',
                    Pricebook2Id = lstPrcBk[0].Id,
                    AccountId = testAcc.Id
                )
            };        
        }
    }

    @isTest
    public static void testCodeCampagne(){
        System.runAs(adminUser){
            Test.StartTest();
              insert lstServCon;
            Test.StopTest();

            System.assertEquals(lstServCon[0].Campagne__c, lstServCon[0].TECH_AccountCampagne__c);
        }
    }
}