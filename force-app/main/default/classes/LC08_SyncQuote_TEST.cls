/**
 * @File Name          : LC08_SyncQuote_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         28-10-2019     		LGO         Initial Version
 * 1.1         12-12-2019     		LGO         Optimisation + Custom setting SyncQLI
**/
@isTest
public without sharing class LC08_SyncQuote_TEST {

    static User mainUser;
    static List<Account> lstAccount = new List<Account>();
    static Opportunity opp = new Opportunity();
    static List<Product2> lstProd = new List<Product2>();
    static List<Quote> lstQuote = new List<Quote>();
    static List<QuoteLineItem> lstQuoteLineItem = new List<QuoteLineItem>();
    static List<OpportunityLineItem> lstOppLineItem = new List<OpportunityLineItem>();
    static List<PricebookEntry> lstPriceBookEntry =  new List<PricebookEntry>();
    static List <SyncingQLI__c> lstSyncOppQLI = new List<SyncingQLI__c>();
    //bypass
    static Bypass__c bp = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('UserAP48@example.com', TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        TestFactory.initQuoteSyncSetting(); //create customsetting Sync

        System.runAs(mainUser){

            //create CS
            /*lstSyncOppQLI  = new List<SyncingQLI__c> {
                new SyncingQLI__c( Name ='TECH_Garantie_Fournisseur__c', QLI__c = 'TECH_Garantie_Fournisseur__c', ToCreateQLI__c = true, MappingType__c = 'OpportunityLineItem-QuoteLineItem'),
                new SyncingQLI__c( Name ='Remise_en_euros__c', QLI__c = 'Remise_en_euros__c', ToCreateQLI__c = true, MappingType__c = 'OpportunityLineItem-QuoteLineItem'),
                new SyncingQLI__c( Name ='UnitPrice', QLI__c = 'UnitPrice', ToCreateQLI__c = true, MappingType__c = 'OpportunityLineItem-QuoteLineItem')
            };     
            insert lstSyncOppQLI; */

            //Create account
            lstAccount = new List<Account> {
                new Account( Name ='Apple Inc'),
                new Account( Name ='Google Inc',Type = 'Entreprise'),
                new Account( Name ='Microsoft Inc',Type = 'Autre')
            };     
            insert lstAccount; 

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAccount.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstAccount.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstAccount.get(2).Id);
			lstAccount.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstAccount.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstAccount.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            
            update lstAccount;

            //Create PriceBook
            Id pricebookId = Test.getStandardPricebookId();            

            //create opportunity
            opp = TestFactory.createOpportunity('Prestation panneaux',lstAccount[0].Id);
            opp.Pricebook2Id = pricebookId;
            opp.Aide_CEE__c = 200;
            insert opp; 

            //Creating products
            lstProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstProd.add(TestFactory.createProduct('Entretien gaz'));
            lstProd[0].Equipment_type1__c = 'Chaudière gaz';
            lstProd[0].Statut__c = 'Approuvée';
            lstProd[0].RecordtypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Product').getRecordTypeId();
            lstProd[1].Equipment_type1__c = 'Chaudière gaz';
            lstProd[1].Statut__c = 'Approuvée';
            lstProd[1].RecordtypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Product').getRecordTypeId();
            insert lstProd;


            //create PricebookEntry
            lstPriceBookEntry =  new List<PricebookEntry>{
                new PricebookEntry( Pricebook2Id = pricebookId,
                                    Product2Id = lstProd[0].Id,
                                    UnitPrice = 150.00,
                                    IsActive = true
                                  ),
                new PricebookEntry( Pricebook2Id = pricebookId,
                                    Product2Id = lstProd[1].Id,
                                    UnitPrice = 50.00,
                                    IsActive = true
                                  )
            };
            insert lstPriceBookEntry;                             

            //Create Quotes
            lstQuote = new List<Quote> {
                new Quote(  Name ='Prestations'
                            ,OpportunityId = opp.Id
                            ,Pricebook2Id = pricebookId
                            ,Date_de_debut_des_travaux__c = system.today()
                            ,isSync__c = false,
                            Aide_CEE__c = 200
                            ), 
                new Quote(  Name ='Travaux'
                            ,OpportunityId = opp.Id
                            ,Pricebook2Id = pricebookId
                            ,Date_de_debut_des_travaux__c = system.today()
                            ,isSync__c = false,
                            Aide_CEE__c = 200
                            ), 
                new Quote(  Name ='Remplacement'
                            ,OpportunityId = opp.Id
                            ,Pricebook2Id = pricebookId
                            ,Date_de_debut_des_travaux__c = system.today()
                            ,isSync__c = false,
                            Aide_CEE__c = 200
                            ),
                new Quote(  Name ='Sous-traitance totale'
                            ,OpportunityId = opp.Id
                            ,Pricebook2Id = pricebookId
                            ,Type_de_devis__c = 'Vente de pièces/équipements au guichet'    
                            ,Date_de_debut_des_travaux__c = system.today()
                            ,isSync__c = false,
                            Aide_CEE__c = 200
                         )
            };     
            insert lstQuote;      

            //create QuoteLineItem
            lstQuoteLineItem =  new List<QuoteLineItem>{
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstProd[0].Id,
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPriceBookEntry[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                 ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstProd[0].Id,
                                    QuoteId = lstQuote[1].Id, 
                                    PricebookEntryId = lstPriceBookEntry[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                 ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstProd[1].Id,
                                    QuoteId = lstQuote[1].Id, 
                                    PricebookEntryId = lstPriceBookEntry[1].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                 )
                };
            insert lstQuoteLineItem;
            lstQuoteLineItem = [SELECT Description,
                    Discount,
                    Product2Id,
                    QuoteId,
                    PricebookEntryId,
                    TECH_QuoteOpp__c,
                    Taux_de_TVA__c,
                    UnitPrice,
                    Quantity,
                    Prix_de_vente_apres_remise__c,
                    Prix_HT_avant_remise__c,
                    Prix_TTC__c,
                    Remise_en100__c,
                    Remise_en_euros__c,
                    ServiceDate
            FROM QuoteLineItem WHERE Id IN: lstQuoteLineItem];

            //create OpportunityLineItem
            lstOppLineItem =  new List<OpportunityLineItem>{
                new OpportunityLineItem(OpportunityId = opp.Id,
                                        Quantity = 1,
                                        PricebookEntryId = lstPriceBookEntry[1].Id,
                                        TotalPrice = lstPriceBookEntry[1].UnitPrice,
                                        TECH_CorrespondingQLItemId__c =lstQuoteLineItem[2].Id
                                        )
            };
            insert lstOppLineItem;                         

            opp.Tech_Synced_Devis__c = lstQuote[0].Id;
            update opp;
          
        }
  
    }

    // @isTest
    // public static void testSyncQuote(){
    //     System.runAs(mainUser){
    //         Test.startTest();
    //         LC08_SyncQuote.SyncQuote(lstQuote[0].Id);            
    //         Test.stopTest();          

    //          List<OpportunityLineItem> lstOLIdelted= [SELECT Id, 
    //          OpportunityId 
    //          FROM OpportunityLineItem 
    //          WHERE OpportunityId =: lstQuote[0].OpportunityId] ;         

    //         // System.assertEquals(lstOLIdelted.size(),1);
    //     }
    // }

    @isTest
    public static void testcreateOppLine(){
        System.runAs(mainUser){
            Test.startTest();
            LC08_SyncQuote.createOppLine(lstQuoteLineItem);          
            Test.stopTest();       

             List<OpportunityLineItem> lstOLI= [SELECT Id
                                     FROM OpportunityLineItem
                                     WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            System.assertEquals(lstOLI.size(),2);
        }
    }

    @isTest
    public static void testdeleteValueAideCEE(){
        System.runAs(mainUser){
            set<Id> qliSet = new set<Id>();
                qliSet.add(lstQuote[0].Id);
            Test.startTest();
            LC08_SyncQuote.deleteValueAideCEE(qliSet);          
            Test.stopTest();       

            List<Quote> lstOLI= [SELECT Id, Aide_CEE__c, OpportunityId
                                     FROM Quote
                                     WHERE Id =: lstQuote[0].Id] ;   
             List<Opportunity> lstOpp= [SELECT Id, Aide_CEE__c 
                                     FROM Opportunity
                                     WHERE Id =: lstOLI[0].OpportunityId] ; 
                                     
            System.assertEquals(lstOLI[0].Aide_CEE__c,null);
            System.assertEquals(lstOpp[0].Aide_CEE__c,null);
        }
    }

    @isTest
    public static void testdeleteRelateOppLineItem(){
        System.runAs(mainUser){
            set<Id> qliSet = new set<Id>();
            for(Integer i=0; i< lstQuoteLineItem.size();i++){
                qliSet.add(lstQuoteLineItem[i].Id);
            }
           
            Test.startTest();
            LC08_SyncQuote.deleteRelateOppLineItem(qliSet);            
            Test.stopTest();            

             List<OpportunityLineItem> lstOLI= [SELECT Id
                                     FROM OpportunityLineItem
                                     WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;        
            System.assertEquals(lstOLI.size(),0);
        }
    }

    @isTest
    public static void testupdateOppLine(){
        System.runAs(mainUser){
            Test.startTest();
            LC08_SyncQuote.updateOppLine(lstQuoteLineItem);            
            Test.stopTest();         

             List<OpportunityLineItem> lstOLI= [SELECT Id
                                     FROM OpportunityLineItem
                                     WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;      
            System.assertEquals(lstOLI.size(),1);
        }
    }

    @isTest
    public static void testmapCSCreateQLI(){
        System.runAs(mainUser){
            Test.startTest();
            LC08_SyncQuote.mapCSCreateQLI();                    
            Test.stopTest();         
        }
    }

    @isTest
    public static void testmapCSCreateQuote(){
        System.runAs(mainUser){
            Test.startTest();
            LC08_SyncQuote.mapCSCreateQuote();                   
            Test.stopTest();         
        }
    } 
    
     @isTest
    public static void testTriggerHandlerUpdate(){
        System.runAs(mainUser){
            Test.startTest();
            
            update lstQuoteLineItem[0];
            Test.stopTest();
        }
    }

    @isTest
    public static void testTriggerHandlerDelete(){
        System.runAs(mainUser){
            Test.startTest();
            delete lstQuoteLineItem[0];
            Test.stopTest();
        }
    }
  
}