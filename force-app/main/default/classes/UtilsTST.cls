/**
 * @File Name          : UtilsTST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 09-09-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/02/2020   AMO     Initial Version
**/
@isTest(SeeAllData=true)

private class UtilsTST{

    static final String personAccountRecordType = System.Label.PersonAccountLabel;
    static List<Account> accountList;
    static User mainUser;
    static Account testAcc;
    static List<User> userList = new List<User>();
    static List<OperatingHours> operatingHoursList;
    static List<ServiceTerritory> serviceTerritoryList;
    static List<Logement__c> logementList;
    static List<Asset> assetList;
    static List<Contract> contractList;
    static List<ContentDocumentLink> contentDocumentLinkListTest;

    static void createAllData(){
      accountList = DataTST.createAccounts(10);

      sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(accountList.get(0).Id);
      sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(accountList.get(1).Id);
      sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(accountList.get(2).Id);
      sofactoapp__Compte_auxiliaire__c aux3 = DataTST.createCompteAuxilaire(accountList.get(3).Id);
      sofactoapp__Compte_auxiliaire__c aux4 = DataTST.createCompteAuxilaire(accountList.get(4).Id);
      sofactoapp__Compte_auxiliaire__c aux5 = DataTST.createCompteAuxilaire(accountList.get(5).Id);
      sofactoapp__Compte_auxiliaire__c aux6 = DataTST.createCompteAuxilaire(accountList.get(6).Id);
      sofactoapp__Compte_auxiliaire__c aux7 = DataTST.createCompteAuxilaire(accountList.get(7).Id);
      sofactoapp__Compte_auxiliaire__c aux8 = DataTST.createCompteAuxilaire(accountList.get(8).Id);
      sofactoapp__Compte_auxiliaire__c aux9 = DataTST.createCompteAuxilaire(accountList.get(9).Id);
      accountList.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
      accountList.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
      accountList.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
      accountList.get(3).sofactoapp__Compte_auxiliaire__c = aux3.Id;
      accountList.get(4).sofactoapp__Compte_auxiliaire__c = aux4.Id;
      accountList.get(5).sofactoapp__Compte_auxiliaire__c = aux5.Id;
      accountList.get(6).sofactoapp__Compte_auxiliaire__c = aux6.Id;
      accountList.get(7).sofactoapp__Compte_auxiliaire__c = aux7.Id;
      accountList.get(8).sofactoapp__Compte_auxiliaire__c = aux8.Id;
      accountList.get(9).sofactoapp__Compte_auxiliaire__c = aux9.Id;

      Update accountList;

      // userList = DataTST.createUsers(1, accountList);
      operatingHoursList = DataTST.createOperatigHours(5);
      serviceTerritoryList = DataTST.createServiceTerritories(5, operatingHoursList[0]);
      logementList = DataTST.createLogements(3, new List<Account>{accountList[0], accountList[1]}, new List<ServiceTerritory>{serviceTerritoryList[0], serviceTerritoryList[1]});
      assetList = DataTST.createAssets(5, new List<Account>{accountList[0], accountList[1]}, new List<Logement__c>{logementList[0], logementList[5]});
      contractList = DataTST.createContracts(1, new List<Account>{accountList[0]}, new List<Asset>{assetList[0]}, new List<ServiceTerritory>{serviceTerritoryList[0]}, Date.today(), 3);
    
      list<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument where title = 'test_contentDocumentLink_NepasSupprimer'];
      ContentDocumentLink cdl = New ContentDocumentLink();
      cdl.LinkedEntityId = accountList[0].id;
      cdl.ContentDocumentId = documents[0].Id;
      cdl.shareType = 'V';
     
      //insert cdl;
	  contentDocumentLinkListTest = new list <ContentDocumentLink>{cdl};
      insert contentDocumentLinkListTest;
        
    }

    static {
      mainUser = TestFactory.createAdminUser('AP01_BlueWSCallout_TEST@test.COM', TestFactory.getProfileAdminId());
      mainUser.UserRoleId = TestFactory.getUserRoleId();
      insert mainUser;

      System.runAs(mainUser){
           //Create Account
       testAcc = TestFactory.createAccount('testAcc');
       testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
       insert testAcc;

       //Update Account
       sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
       testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
       update testAcc;

       testAcc = [SELECT Id, FirstName, LastName, PersonEmail,  PersonContactId
                   FROM Account
                   WHERE Id = :testAcc.Id];

      // User communityUser = TestFactory.createCommunityUser(testAcc);
      // userList.add(newUser);
      // insert userList;
      }  
    }

    static testMethod void getAccountRecordTypeIdTST(){
      RecordType result = [
        SELECT ID, Name
        FROM RecordType
        WHERE SObjectType = 'Account'
        AND DeveloperName = 'PersonAccount'
      ];
        
      System.assertEquals(result.Id, Utils.getAccountRecordTypeId(personAccountRecordType));
    }

    // static testMethod void getAccountIdFromCommunityUserTST(){
    //   System.runAs(mainUser){
    //     // userList = DataTST.createUserss(1);
    //     // accountList = DataTST.createAccounts(1);
    //     //  //createAllData();
    //     // System.debug('userList = ' + userList[0].Id);
    //     // System.debug('accList = ' + accountList[0].Id);
    //     Test.startTest();
         
    //     map<String, String> userMap = Utils.getAccountIdFromCommunityUser(new List<String>{userList[0].Id});
          
    //     // System.assertEquals(accountList[0].Id, userMap.get(userList[0].Id));
          
    //     Test.stopTest();
    //   }
      
    // }
    
    // static testMethod void getUserAssetTST(){
    //   // createAllData();
    //   System.runAs(mainUser){
    //     Test.startTest();
        
    //     List<Asset> usersAssets = Utils.getUserAsset(userList[0].Id);
          
    //     Test.stopTest();
          
    //     // System.assertEquals(10, usersAssets.size());
    //   }   
    // }

    // static testMethod void getPublicLinkMapTST(){
    //   // createAllData();
    //   System.runAs(mainUser){
       
    //     Test.startTest();
    //     Utils.isTest = true;        
    //     Utils.contentDocumentLinkListTest = contentDocumentLinkListTest;
        
    //     map<String, String> publicLinkMap = Utils.getPublicLinkMap(new List<String>{contractList[0].Id});
        
    //     Test.stopTest();

    //   }
    // }

    static testMethod void cleanValueTST(){
      System.runAs(mainUser){
        String value = 'Hello ';

        Test.startTest();
        
        String valueCleaned = Utils.cleanValue(value);
        
        Test.stopTest();
  
        System.assertEquals('Hello', valueCleaned);

      } 
    }

    static testMethod void fromUTCToGMTTST(){
      System.runAs(mainUser){
        DateTime theDate = DateTime.now();
        Test.startTest();   
        DateTime utcDateTime= Utils.fromUTCToGMT(theDate);    
        Test.stopTest();
      }
   
    }
}