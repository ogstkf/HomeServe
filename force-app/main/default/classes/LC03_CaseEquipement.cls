/**
 * @File Name          : LC03_CaseEquipement.cls
 * @Description        : Controller class for lightning component LC03_CaseEquipement
 * @Author             : DMU
 * @Group              : Spoon Consulting
 * @Last Modified By   : RRJ
 * @Last Modified On   : 31/07/2019, 14:09:24
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    09/07/2019, 10:30:00   		DMU                      Initial Version
**/
public class LC03_CaseEquipement {

    @AuraEnabled
    public static List<Asset> fetchEqp(Id caseId){
    	Case cse = [SELECT Id, AssetId from Case WHERE Id = :caseId LIMIT 1];

    	List<Asset> lstAsset = new List<Asset>();
    	if(String.isNotBlank(cse.AssetId)){
	        lstAsset = [SELECT Id, Name, Equipment_type__c, Product2.Name, Logement__r.Name, Logement__r.Postal_Code__c, Logement__r.City__c, Logement__r.Agency__r.Name, Logement__r.Agency__c, (SELECT Id, Customer_Absences__c, Customer_Allowed_Absences__c, ServiceContractId, ServiceContract.Name, VE_Status__c, VE_Min_Date__c, VE_Max_Date__c, Completion_Date__c, ServiceContract.Contract_Status__c FROM ContractLineItems ORDER BY StartDate DESC LIMIT 1) FROM Asset WHERE Id = :cse.AssetId];

			// System.debug('##### lstAsset: '+ lstAsset);
	        // system.debug('**lstAsset size: '+lstAsset[0].ContractLineItems.size());
	    }
        return lstAsset;
    }
}