/**
 * @File Name          : AP24_QuoteSyncedOpportunity_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/25/2020, 2:15:26 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/02/2020   AMO     Initial Version
**/ 
@isTest
public with sharing class AP24_QuoteSyncedOpportunity_TEST {
/**
 * @File Name          : AP24_QuoteSyncedOpportunity_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 6/25/2020, 2:15:26 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-10-2019     		LGO         Initial Version
**/
static User mainUser;
    static Order ord ;
    static Account testAcc = new Account();
    static Opportunity opp = new Opportunity();
    static Quote quo = new Quote();
    // static Shipment ship = new Shipment();
    // static ProductTransfer prodTrans = new ProductTransfer();
	// static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
	// static PricebookEntry PrcBkEnt = new PricebookEntry();
    // static OrderItem ordItem = new OrderItem();

    static{
        mainUser = TestFactory.createAdminUser('AP24@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);
            opp.RecordTypeId = AP_Constant.getRecTypeId('Opportunity', 'Opportunite_standard');
            
            insert opp;

            //create Quote
            quo = new Quote(Name ='Test1',
                            Devis_signe_par_le_client__c = true,
                            OpportunityId = opp.Id);
            insert quo;

            opp.Tech_Synced_Devis__c = quo.Id;
            update opp;
        }
    }

    @isTest
    public static void testcheckProductTransfer(){
        System.runAs(mainUser){
            opp.Tech_Devis_sign_par_le_client__c = true;
            System.debug('##### TEST opp: '+opp);
            Test.startTest();
                update opp;
            Test.stopTest();

            list<Quote> lstNewQuo = [ SELECT Id, Devis_signe_par_le_client__c, 
                                    (SELECT Id 
                                        FROM Opportunit_s__r 
                                        WHERE Id =: opp.Id) 
                                    FROM Quote] ;
            

            System.assertEquals(lstNewQuo[0].Devis_signe_par_le_client__c,true);
        }
    }

}