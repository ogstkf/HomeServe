/**
 * @File Name          : AP27_CaseCannotCloseTEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : MNA
 * @Last Modified On   : 12/02/2020, 14:33:42
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    24/10/2019   AMO     Initial Version
**/
@isTest
public with sharing class AP27_CaseCannotCloseTEST {

    
    static User mainUser;
    static List<Case> lstCase;
    static List<Account> lstAccount;
    static List<WorkOrder> lstWorkOrder;
    static List<ServiceAppointment> lstServiceAppointment;
    static Bypass__c bp = new Bypass__c();

        static{
            mainUser = TestFactory.createAdminUser('AP27_CaseCannotClose@test.COM', 
                                                TestFactory.getProfileAdminId());

            insert mainUser;
        
            bp.SetupOwnerId  = mainUser.Id;
            bp.BypassValidationRules__c = True;
            bp.BypassTrigger__c = 'AP16,WorkOrderTrigger';
            insert bp;

        system.runAs(mainUser){  

            lstAccount = new List<Account>{
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='Ally'),
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='Ally Mohamud'),
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='Ally Mohamudally')
            };

            Insert lstAccount;

            lstCase = new List<Case>{
                new Case(RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Client case').getRecordTypeId(),
                    AccountId=lstAccount[0].Id ,Type=AP_Constant.caseTypeInstallation,VE_Planning__c=false
                    ,Description='caseFirst',Subject='caseFirst'
                    ,Due_Date__c=Date.today().addDays(3),Start_Date__c=Date.today(), Status = AP_Constant.wrkOrderStatusInProgress),

                new Case(RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Client case').getRecordTypeId(),
                    AccountId=lstAccount[1].Id ,Type=AP_Constant.caseTypeInstallation,VE_Planning__c=false
                    ,Description='caseFirst',Subject='caseFirst'
                    ,Due_Date__c=Date.today().addDays(3),Start_Date__c=Date.today(), Status = AP_Constant.wrkOrderStatusInProgress)

            };

            insert lstCase;

            lstWorkOrder = new List<WorkOrder>{
                new WorkOrder(caseId = lstCase[0].Id, Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderStatusInProgress),
                new WorkOrder(caseId = lstCase[1].Id ,Status = AP_Constant.StatusClosed, Priority = AP_Constant.wrkOrderStatusInProgress)
            };

            insert lstWorkOrder;

            lstServiceAppointment = new List<ServiceAppointment>{
                new ServiceAppointment(
                    Status = AP_Constant.wrkOrderStatusInProgress,
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWorkOrder[0].Id,
                    // Service_Resource_Level__c = AP_Constant.ServiceResourceLevelJunior,
                    Work_Order__c = lstWorkOrder[0].Id),

                new ServiceAppointment(
                    Status = 'Cancelled',
                    EarliestStartTime = System.today(),
                    DueDate = System.today().addDays(2),
                    ParentRecordId = lstWorkOrder[1].Id,
                    // Service_Resource_Level__c = AP_Constant.ServiceResourceLevelJunior,
                    Work_Order__c = lstWorkOrder[1].Id)
            };

            insert lstServiceAppointment;

        }


    }

    @isTest
    public static void testUpdateInProgress(){
        System.runAs(mainUser){ 
            lstWorkOrder[0].Status = AP_Constant.StatusClosed;
            Update lstWorkOrder;
        try{
            Test.startTest();
            lstCase[0].Status = AP_Constant.StatusClosed;
            Update lstCase;
            Test.stopTest();
        }

        catch(Exception e){
            System.Assert(e.getMessage().contains('Vous ne pouvez pas clôturer cette requête tant qu’il y a un ordre d’exécution et un rendez-vous de service ouvert'));
        }
        }
    }

    @isTest
    public static void testUpdateAllClose(){
        System.runAs(mainUser){ 
        
            Test.startTest();
                lstCase[1].Status = AP_Constant.StatusClosed;
                Update lstCase[1];
            Test.stopTest();

            System.AssertEquals(lstCase[1].Status, AP_Constant.StatusClosed);

        }
    }

    @isTest
    public static void testCaseInProgress(){
        System.runAs(mainUser){ 

        try{
            Test.startTest();
                lstCase[0].Status = AP_Constant.StatusClosed;
                Update lstCase;
            Test.stopTest();
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('Vous ne pouvez pas clôturer cette requête tant qu’il y a un ordre d’exécution et un rendez-vous de service ouvert'));
        }

        }
    }
}