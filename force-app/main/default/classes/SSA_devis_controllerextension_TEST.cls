/**
 * @File Name          : SSA_devis_controllerextension_TEST.cls
 * @Description        : TestClass
 * @Author             : SSA
 * @Group              : 
 * @Last Modified By   : SSA
 * @Last Modified On   : 12-10-2020
 * @Modification Log   : 

**/
@isTest

public class SSA_devis_controllerextension_TEST {
 static User mainUser;
    static Account testAcc = new Account();
    static Opportunity opp = new Opportunity();
    static WorkOrder wrkOrd = new WorkOrder();
    static list<Product2> lstProd = new list<Product2>();
    static Schema.Location loc = new Schema.Location();
    static OperatingHours oh = new OperatingHours();
    static sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c();
	static ServiceTerritory st = new ServiceTerritory();
	static ServiceTerritoryLocation stl = new ServiceTerritoryLocation();    
    static Quote quo = new Quote();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static list<QuoteLineItem> lstQli = new list<QuoteLineItem>();
	static ProductItem pi = new ProductItem(); 
    static ProductRequestLineItem prli = new ProductRequestLineItem();
    static sofactoapp__Factures_Client__c FacturesClients = new sofactoapp__Factures_Client__c();
    static AgencyAccreditation__c AccredAgence = new AgencyAccreditation__c();
    
    static{
        
        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){
			
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;  

            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);            
            insert opp;
            
            //work order
            wrkOrd = TestFactory.createWorkOrder();
            insert wrkOrd;
                      
            //create Products
            lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                            //  Famille_d_articles__c='Accessoires',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
                    
                /*new Product2(Name='Prod2',
                             Famille_d_articles__c='Appareils',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                    
                new Product2(Name='Prod3', 
                             Famille_d_articles__c='Consommables',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                    
                new Product2(Name='Prod4', 
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())*/
            };
            insert lstProd;
            
            //create Location
            loc = new Schema.Location(Name='Test1',
                                      LocationType='Entrepôt',
                                      ParentLocationId=NULL,
                                      IsInventoryLocation=true);
            insert loc;

			//create Operating Hours
			oh = new OperatingHours(Name='Test1');
			insert oh;

			//create Corporate Name
			sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(Name='Test1',
                                                                                  sofactoapp__Credit_prefix__c='123',
                                                                                  sofactoapp__Invoice_prefix__c='123');  
            insert srs;
            
            //create Service Territory
            st = new ServiceTerritory(Name='Test1',
                                      OperatingHoursId=oh.Id,
                                      Sofactoapp_Raison_Social__c=srs.Id,
                                     Agency_Code__c='123');
            insert st;
            srs.sofactoapp_Agence__c= st.Id;
            update srs;
            //create Service Territory Location
            stl = new ServiceTerritoryLocation(ServiceTerritoryId=st.Id,
                                               LocationId=loc.Id);
            insert stl;
            
            //create agency acrredation
            //AccredAgence = new AgencyAccreditation__c(name = 'Acc1',
           // Type__c= 'Qualisav',
           // Agency__c=st.id,
           // Equipement_couvert__c = 'Pompe à chaleur hybride Fioul',                             
           //   Valid_until__c= Date.newInstance(2025, 12, 20),                                     
           // Numero_SGS__c ='12345',
          //  LocalAgency__c= st.id);
          //  insert AccredAgence ;
            
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            insert PrcBkEnt;
            
            //create Quote
            quo = new Quote(Name ='Test1',
                            OpportunityId=opp.Id,
                            Agency__c=st.Id,
                            Pricebook2Id = lstPrcBk[0].Id,
                            isSync__c = false,
                            Ordre_d_execution__c = wrkOrd.Id);
            insert quo;          
                       
            //create Quote Line Items
            lstQli = new list<QuoteLineItem>{
            	new QuoteLineItem(QuoteId=quo.Id, 
                                  Product2Id=lstProd[0].Id, 
                                  Quantity=decimal.valueOf(2), 
                                  UnitPrice=decimal.valueOf(145),
                                  PriceBookEntryID=PrcBkEnt.Id)
            };
            insert lstQli;
            
            pi = new ProductItem(Product2Id=lstProd[0].Id,
                                 LocationId=loc.Id,
                                 QuantityOnHand=decimal.valueOf(2));
            insert pi;
            
       
                FacturesClients= new sofactoapp__Factures_Client__c(
                        sofactoapp__Compte__c = TestAcc.Id
                        , sofactoapp__emetteur_facture__c = srs.Id
                    	, sofactoapp__Etat__c ='brouillon'
                    	, sofactoapp__Date_de_facture__c = Date.newInstance(2018, 12, 20)
                );
            
            insert FacturesClients;
          
       
            
            
        }
        
    }  


	@isTest
	public static void doGenerate(){

		System.runAs(mainUser){
			
			Test.startTest();
     	 PageReference pageRef = Page.FactureExemple ;
		 pageRef.getParameters().put('id', FacturesClients.Id);
            sofactoapp__Factures_Client__c factureValidate = [SELECT Id
												FROM sofactoapp__Factures_Client__c
												WHERE Id = :FacturesClients.Id
														LIMIT 1
		];
           
			
			
			
			Test.setCurrentPage(pageRef);

            // Instantiate a new controller with all parameters in the page
		ApexPages.StandardController con = new ApexPages.StandardController(factureValidate);
		SSA_devis_controllerextension controller = new SSA_devis_controllerextension(con);
	        Test.stopTest();

		}
	}

}