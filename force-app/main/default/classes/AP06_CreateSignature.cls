public with sharing class AP06_CreateSignature {

	/**************************************************************************************
	-- - Author        : Spoon Consulting
	-- - Description   : Class for creation of signature
	--
	-- Maintenance History: 
	--
	-- Date         Name  Version  Remarks 
	-- -----------  ----  -------  -------------------------------------------------------
	-- 14-03-2019   SBH	   1.0     Initial version
	--------------------------------------------------------------------------------------
	**************************************************************************************/

	public static String calculateSignature(Map<String, String> mapData){

		//1. Sort keys in map
		//2. Concatenate values
		//3. Concatenate values + key
		//4. Encode in base64

		CreateSignature__c keyCS = CreateSignature__c.getInstance('SecretKey');
		string key = keyCS.SecretKeyValue__c;

		String message = '';
		//Sort and concat START
		Set<String> setKeysSorted = mapData.keySet();
		System.debug('## setKeysSorted: ' + setKeysSorted);

		Set<String> setKeysVads = new set<string>();
        for(string str : setKeysSorted){
            if (str.startsWith('vads_')){
                setKeysVads.add(str);
            }
        }
        System.debug('## setKeysVads: ' + setKeysVads);

        List<String> lstKeys = new List<String>(setKeysVads);
		lstKeys.sort();
		
		for(String currentKey : lstKeys){			
			message += mapData.get(currentKey) + '+';			
		}
		
		System.debug('## Message: ' + message);
		message += key;
		System.debug('## Message+key: ' + message);
		String sig = generateSignature(message, key);
		return sig;
	}

	public static String generateSignature(String input, String key) {
	    String signature = generateHmacSHA256Signature(input, key);
	    System.debug('Signature is : '+signature);
	    return signature;
	}

	private static String generateHmacSHA256Signature(String saltValue, String secretKeyValue) {
	    String algorithmName = 'HmacSHA256';
	    Blob hmacData = Crypto.generateMac(algorithmName, Blob.valueOf(saltValue), Blob.valueOf(secretKeyValue));
	    return EncodingUtil.base64Encode(hmacData);
	}
}