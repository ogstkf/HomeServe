/**
 * @File Name          : LC13_AddProduct.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 29/11/2019, 09:38:14
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/11/2019   AMO     Initial Version
**/
public class LC13_AddProduct {
/**************************************************************************************
-- - Author        : SPOON
-- - Description   : Logic for lightning component which adds an Order line item to an order
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 10-NOV-2019  SBH    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/

    @auraEnabled public static List<String> getPicklistValues() { 
        List<String> lstOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = Product2.Family.getDescribe();
        List<Schema.PicklistEntry> lstPLE = fieldResult.getPicklistValues();
       
        for(Schema.PicklistEntry ple : lstPLE) {
            lstOptions.add(ple.getValue());
        }     

        return lstOptions;
    }

    //Ally
    @AuraEnabled
    public static Map<Id, OrderItem> fetchOrderLineItem(Id orderId){
        List<OrderItem> lstOrderItem = [SELECT Id, OrderId, Product2Id, Remise__c, Remise100__c FROM OrderItem WHERE OrderId = :orderId];

        Map<Id, OrderItem> mapOrderItem = new Map<Id, OrderItem>();

        for(OrderItem ordI : lstOrderItem){
            mapOrderItem.put(ordI.Product2Id, ordI);
        }

        return mapOrderItem;
    }

    //RRJ 20191031: adding method to get details on init
    @AuraEnabled
    public static Object fetchDetails(Id orderId){
        System.debug('##Id@ :' + orderId);
        Order ord = [SELECT Id, PriceBook__c, Pricebook2.Name, AccountId, Remise_mode__c FROM Order WHERE Id = :orderId LIMIT 1];
        System.debug('##Order remise: ' + ord.Remise_mode__c);


        return ord;
    
    }

    @auraEnabled public static List<wrapPricebkEntry> getAllPricebkEntry(Integer ofst, Id orderId) {
        Integer intOffset = Integer.valueof(ofst);

        system.debug('### orderId getAllPricebkEntry: '+orderId);
        Order ord = [select id, Pricebook2.Name, Pricebook2Id, Remise_mode__c, AccountId from Order where id=: orderId];
        //Opportunity opp = [select Pricebook2Id from opportunity where id=: oppId];

        List<PricebookEntry> setPBE =  Test.IsRunningTest() ?  new List<PricebookEntry>([   SELECT  Id,
                                                                            Product2.Family,                                                                            
                                                                            Name,
                                                                            Product2Id,
                                                                            UnitPrice,
                                                                            Prix_achat_brut_prix_public__c
                                                                    FROM    PricebookEntry
                                                                    WHERE   Pricebook2Id=:Test.getStandardPricebookId()
                                                                    AND     Product2.IsActive = true                                                                    
                                                                    ORDER BY Name ASC
                                                                    LIMIT   50
                                                                    OFFSET  :intOffset])

                                                                    : new List<PricebookEntry>([    SELECT  Id,
                                                                            Pricebook2Id,
                                                                            Product2Id,
                                                                            Product2.Family,
                                                                            Product2.Famille_d_articles__c, 
                                                                            Product2.Sous_familles_d_articles__c,
                                                                            Name,
                                                                            Product2.Description,
                                                                            UnitPrice,
                                                                            Product2.ProductCode, 
                                                                            Prix_achat_brut_prix_public__c  
                                                                    FROM    PricebookEntry
                                                                    // WHERE   Pricebook2.isStandard = true
                                                                    WHERE Pricebook2Id =: ord.Pricebook2Id
                                                                    AND     Product2.IsActive = true                                                                   
                                                                    ORDER BY Name ASC
                                                                    LIMIT   50
                                                                    OFFSET  :intOffset ]);
                                                                    
        system.debug('### setPBE in getAllPricebkEntry : '+setPBE);

        List<wrapPricebkEntry> lstWrapPBE = new List<wrapPricebkEntry>();
        Set<Id> setProducts = new Set<Id>();

        for(OrderItem OrdI : [SELECT Id, OrderId, Product2Id FROM OrderItem WHERE OrderId = :orderId]){
            setProducts.add(OrdI.Product2Id);
        }


        for(PricebookEntry pbe : setPBE) {
            if(setProducts.contains(pbe.Product2Id)){
                wrapPricebkEntry wrp = new wrapPricebkEntry(pbe);
                wrp.selected = true;
                wrp.alreadypresent = true;
                lstWrapPBE.add(wrp);

            }
            else{
                lstWrapPBE.add(new wrapPricebkEntry(pbe));
            }
        }

        return lstWrapPBE;
    }

    @auraEnabled public static List<wrapPricebkEntry> searchPricebkEntry(String searchText, Integer ofst, Id orderId) {
        Integer intOffset = Integer.valueof(ofst);

        system.debug('### orderId searchPricebkEntry: '+orderId);
        Order ord = [select id, AccountId, Pricebook2.Name, Remise_mode__c, Pricebook2Id from Order where id=: orderId];

       // Opportunity opp = [select Pricebook2Id from opportunity where id=: oppId];

        List<PricebookEntry> lstPBE = Test.IsRunningTest() ?  new List<PricebookEntry>([    SELECT  Id,
                                                                            Product2.Family,
                                                                            Name,
                                                                            UnitPrice
                                                                    FROM    PricebookEntry
                                                                    WHERE   Pricebook2Id=:Test.getStandardPricebookId()
                                                                    AND     Name LIKE :('%' + searchText + '%')
                                                                    AND     Product2.IsActive = true 
                                                                    ORDER BY Name ASC
                                                                    LIMIT   50
                                                                    OFFSET  :intOffset ])

                                                                    : new List<PricebookEntry>([    SELECT  Id,
                                                                            Product2.Family,
                                                                            Name,
                                                                            UnitPrice, 
                                                                            Prix_achat_brut_prix_public__c                                                                             
                                                                    FROM    PricebookEntry  
                                                                    // WHERE   Pricebook2.isStandard = true                                                                  
                                                                    WHERE Pricebook2Id =: ord.Pricebook2Id
                                                                    AND   Name LIKE :('%' + searchText + '%')
                                                                    AND     Product2.IsActive = true 
                                                                    ORDER BY Name ASC
                                                                    LIMIT   50
                                                                    OFFSET  :intOffset ]);

        List<wrapPricebkEntry> lstWrapPBE = new List<wrapPricebkEntry>();

        for(PricebookEntry pbe : lstPBE) {
            lstWrapPBE.add(new wrapPricebkEntry(pbe));
        }

        return lstWrapPBE;
    }

    @auraEnabled public static List<wrapPricebkEntry> filterPricebkEntry(String prodFam, Integer ofst, Id quoteId) {
        Integer intOffset = Integer.valueof(ofst);

        system.debug('### quoteId filterPricebkEntry: '+quoteId);
        Quote qu = [select id, opportunityId, opportunity.Pricebook2Id from quote where id=: quoteId];
        //Opportunity opp = [select Pricebook2Id from opportunity where id=: oppId];

        Boolean isStd = Test.IsRunningTest() ? false : true;

        List<PricebookEntry> lstPBE = Test.IsRunningTest() ? new List<PricebookEntry>([ SELECT  Id,
                                                                            Product2.Family,
                                                                            Name,
                                                                            UnitPrice, 
                                                                            Prix_achat_brut_prix_public__c                                                                                                                                                       
                                                                    FROM    PricebookEntry                                                                  
                                                                    WHERE   Pricebook2Id=:Test.getStandardPricebookId()
                                                                    AND     Product2.IsActive = true 
                                                                    ORDER BY Name ASC
                                                                    LIMIT   50
                                                                    OFFSET  :intOffset ])

                                                                    : new List<PricebookEntry>([    SELECT  Id,
                                                                            Product2.Family,
                                                                            Name,                                                                           
                                                                            UnitPrice, 
                                                                            Prix_achat_brut_prix_public__c                                                                             
                                                                    FROM    PricebookEntry
                                                                    WHERE   Pricebook2.isStandard = true                                                                    
                                                                    //WHERE Pricebook2Id =: qu.opportunity.Pricebook2Id
                                                                    AND     Product2.IsActive = true 
                                                                    ORDER BY Name ASC
                                                                    LIMIT   50
                                                                    OFFSET  :intOffset ]);

        List<wrapPricebkEntry> lstWrapPBE = new List<wrapPricebkEntry>();

        for(PricebookEntry pbe : lstPBE) {
            lstWrapPBE.add(new wrapPricebkEntry(pbe));
        }

        return lstWrapPBE;
    }

    @auraEnabled public static List<wrapPricebkEntry> searchFilterPricebkEntry(String searchText, String prodFam, Integer ofst, Id quoteId) {
        Integer intOffset = Integer.valueof(ofst);

        system.debug('### searchFilterPricebkEntry quoteId: '+quoteId);
        Quote qu = [select id, opportunityId, opportunity.Pricebook2Id from quote where id=: quoteId];
        //Opportunity opp = [select Pricebook2Id from opportunity where id=: oppId];

        List<PricebookEntry> lstPBE = Test.IsRunningTest() ? new List<PricebookEntry>([ SELECT  Id,
                                                                            Product2.Family,
                                                                            Name,                                                                           
                                                                            UnitPrice, 
                                                                            Prix_achat_brut_prix_public__c                                                                             
                                                                    FROM    PricebookEntry
                                                                    WHERE   Pricebook2Id=:Test.getStandardPricebookId()
                                                                    AND     Name LIKE :('%' + searchText + '%')
                                                                    AND     Product2.IsActive = true 
                                                                    ORDER BY Name ASC
                                                                    LIMIT   50
                                                                    OFFSET  :intOffset ])

                                                                    : new List<PricebookEntry>([    SELECT  Id,
                                                                            Product2.Family,
                                                                            Name,
                                                                            UnitPrice, 
                                                                            Prix_achat_brut_prix_public__c                                                                             
                                                                    FROM    PricebookEntry
                                                                    WHERE   Pricebook2.isStandard = true
                                                                    //WHERE Pricebook2Id =: qu.opportunity.Pricebook2Id
                                                                    AND     Name LIKE :('%' + searchText + '%')
                                                                    AND     Product2.IsActive = true 
                                                                    ORDER BY Name ASC
                                                                    LIMIT   50
                                                                    OFFSET  :intOffset
        ]);

        List<wrapPricebkEntry> lstWrapPBE = new List<wrapPricebkEntry>();

        for(PricebookEntry pbe : lstPBE) {
            lstWrapPBE.add(new wrapPricebkEntry(pbe));
        }

        return lstWrapPBE;
    }
                                                    
    @auraEnabled public static String saveOrderlineItem(string json, String orderId) {
        system.debug('** json: '+json);
    

        String message = '';

        Order ord = [select id, Pricebook2Id, Pricebook2.Name, Remise_mode__c, AccountId from Order where id=: orderId];

        List<OrderItem> lstOrderItem = [SELECT Id, Product2Id, OrderId FROM OrderItem WHERE OrderId = :orderId];
        Map<Id, OrderItem> mapOrdProd = new Map<Id, OrderItem>();

        for(OrderItem  ordI : lstOrderItem){
            mapOrdProd.put(ordI.Product2Id, ordI);
        }
        
        // Insert List
        list<OrderItem> lstOrderLineItems = new list <OrderItem>(); 
        //Update List
        List<OrderItem> lstOrderItemUpdt = new List<OrderItem>();

        list<OLIWrapper> lstOli = getListOLIvalues(json);
        try{
            for(Integer i=0;i< lstOli.size();i++){
                // Update
                if(mapOrdProd.keyset().contains(lstOli[i].Product2Id)){
                    OrderItem ordline = new OrderItem(
                    Id = mapOrdProd.get(lstOli[i].Product2Id).Id
                    ,UnitPrice = decimal.valueOf( lstOli[i].UnitPriceOrderItem)
                    ,Quantity = integer.valueOf( lstOli[i].Quantity)
                    ,Remise100__c = lstOli[i].Remise100 != null ? decimal.valueOf( lstOli[i].Remise100) : null
                    ,Remise__c = lstOli[i].Remise != null ? decimal.valueOf(lstOli[i].Remise) : null
                    ,OrderId = Ord.Id
                    ,Product2Id =  lstOli[i].Product2Id
                    //,Prix_TTC__c = getListOLIvalues[i].Prix_TTC__c
                    ,PriceBookentryId =  lstOli[i].Id
                    ,Description = lstOli[i].Description
                    ,Remise_mode_de_transmission__c = Ord.Remise_mode__c
                    );

                    lstOrderItemUpdt.add(ordline);
                    // Delete
                    mapOrdProd.remove(lstOli[i].Product2Id);
                }
    
                else{
                    // Insert
                    OrderItem olineItem = new OrderItem(
                        UnitPrice = decimal.valueOf( lstOli[i].UnitPriceOrderItem)
                        ,Quantity = integer.valueOf( lstOli[i].Quantity)
                        ,Remise100__c = lstOli[i].Remise100 != null ? decimal.valueOf( lstOli[i].Remise100) : null
                        ,Remise__c = lstOli[i].Remise != null ? decimal.valueOf(lstOli[i].Remise) : null
                        ,OrderId = Ord.Id
                        ,Product2Id =  lstOli[i].Product2Id
                        //,Prix_TTC__c = getListOLIvalues[i].Prix_TTC__c
                        ,PriceBookentryId =  lstOli[i].Id
                        ,Description = lstOli[i].Description
                        ,Remise_mode_de_transmission__c = Ord.Remise_mode__c
                    );
                    lstOrderLineItems.add(olineItem);
                } 
            }           
        
            system.debug('** in try insert');
            insert lstOrderLineItems;
            update lstOrderItemUpdt;
            delete mapOrdProd.values();
            System.debug('lstOrderLineItems:' + lstOrderLineItems);
            message = 'creation de lignes de commandes reussite';
            // return message;
        }
        catch(Exception e){
            system.debug('** e msg: '+e.getMessage());
            message = 'une erreur c \'est produite lors de la creation des lignes de commande: ' +e.getMessage();
            throw new AuraHandledException(e.getMessage());            
        }
        return message;
    }


    public class wrapPricebkEntry {
        @auraEnabled public Boolean selected {get; set;}
        @auraEnabled public Boolean alreadypresent {get; set;}
        @auraEnabled public PricebookEntry pbe {get; set;}

        public wrapPricebkEntry(PricebookEntry pb) {
            this.pbe = pb;
            selected = false;
            alreadypresent = false;
        }
    }



     public class OLIWrapper {
        @AuraEnabled public String Id;
        @AuraEnabled public String Product2Id;
        @AuraEnabled public Product2 Product2;
        @AuraEnabled public String Name;
        @AuraEnabled public String UnitPrice;
        @AuraEnabled public String ListPrice;
        @AuraEnabled public String Quantity;
        @AuraEnabled public String Eco_contribution;
        @AuraEnabled public String Remise100;
        @AuraEnabled public String Remise;
        @AuraEnabled public String Description;
        @AuraEnabled public String Pricebook2Id;
        @AuraEnabled public String Remise100Init;
        @AuraEnabled public String UnitPriceOrderItem;
    }

    public static list <OLIWrapper> getListOLIvalues(String jsonStr) {
        list<OLIWrapper> qlst =  (List<OLIWrapper>) json.deserialize(jsonStr, List<OLIWrapper>.class);
        system.debug('*** zzqlstz: '+ qlst);        
        return qlst;        
    }

    @AuraEnabled
    public static Map<String, Object> getRemises(String accountId, String json, Id OrderId){
        System.debug('## getRemises: ' );
        System.debug('## accountId : ' + accountId);
        System.debug('## OrderId : ' + OrderId);
        System.debug('## String json: ' + json);        
        System.debug('## get remises End: ');

        Set<Id> setProdId = new Set<Id>();

        // select ois 
        List<OrderItem> lstOrderItem = [SELECT Id, OrderId, Product2Id FROM OrderItem WHERE OrderId = :OrderId];
        System.debug('## lstOrderItem : ' + lstOrderItem);


        // productid set
        for(OrderItem ordI : lstOrderItem){
            setProdId.add(ordI.Product2Id);
        }

        list<OLIWrapper> lstOli = getListOLIvalues(json);

        Set<String> productFamilies = new Set<String>();
        Set<String> productSubfamilies = new Set<String>();
        Set<Id> setPbIds = new set<Id>();
        Map<String, Object> mapRes = new Map<String, Object>();


        Map<Id, Conditions__c> mapProdToCondition = new Map<Id, Conditions__c>();
        Map<String, Conditions__c> mapSouFamilleToConditions = new Map<String, Conditions__c>();
        Map<String, Conditions__c> mapFamilleToConditions = new Map<String, Conditions__c>();

        for(Integer i=0 ; i< lstOli.size(); i++){
            System.debug('##In for loop : ' + lstOli[i]);

            //retrieve conditions sets
            if(lstOli[i].Product2.Famille_d_articles__c != null) productFamilies.add(lstOli[i].Product2.Famille_d_articles__c);
            if(lstOli[i].Product2.Sous_familles_d_articles__c != null) productSubfamilies.add(lstOli[i].Product2.Sous_familles_d_articles__c);
            if(lstOli[i].Pricebook2Id != null) setPbIds.add(lstOli[i].Pricebook2Id);
        }

        System.debug('productFamilies : ' + productFamilies);
        System.debug('productSubfamilies : ' + productSubfamilies);

        System.debug('setPbIds : ' + setPbIds);
        //selecting conditions for pbes
        List<Conditions__c> lstConds = [
            SELECT Id, Remise__c, Famille_d_articles__c, Sous_famille_d_articles__c, Produit__c
            FROM Conditions__c
            WHERE Catalogue__c IN :setPbIds
            AND Compte__c = :accountId
            AND RecordTypeId = :getConditionRecordType(AP_Constant.conditionRtLigne)
        ];

        System.debug('##conditions retrieved: ' + lstConds);

        for(Conditions__c cond : lstConds){
            if(cond.Produit__c != null && cond.sous_famille_d_articles__c == null && cond.Famille_d_articles__c == null ) mapProdToCondition.put(cond.Produit__c, cond);
            if(cond.sous_famille_d_articles__c != null && cond.Famille_d_articles__c == null && cond.Produit__c == null) mapSouFamilleToConditions.put(cond.sous_famille_d_articles__c, cond);
            if(cond.Famille_d_articles__c != null && cond.Produit__c == null && cond.sous_famille_d_articles__c == null) mapFamilleToConditions.put(cond.Famille_d_articles__c, cond);
        }
        System.debug('## product mapProdToCondition: ' + mapProdToCondition);
        System.debug('## product mapSouFamilleToConditions: ' + mapSouFamilleToConditions);
        System.debug('## product mapFamilleToConditions: ' + mapFamilleToConditions);
        System.debug('## product lstOli: ' + lstOli);
        System.debug('## product setProdId: ' + setProdId);

        for(Integer i=0 ; i< lstOli.size(); i++){
            System.debug('## product lstOli[i].Product2Id: ' + lstOli[i].Product2Id);
            System.debug('## product setProdId.contains(lstOli[i].Product2Id) ' + setProdId.contains(lstOli[i].Product2Id));
            if(lstOli[i].Product2Id != null && (!setProdId.contains(lstOli[i].Product2Id))){
                //verify product
                System.debug('## product mapProdToCondition.get(lstOli[i].Product2Id): ' + mapProdToCondition.get(lstOli[i].Product2Id));
                if(mapProdToCondition.get(lstOli[i].Product2Id) != null){
                    System.debug('## product Condition found: ' + mapProdToCondition.get(lstOli[i].Product2Id));
                    lstOli[i].Remise100 = String.valueOf(mapProdToCondition.get(lstOli[i].Product2Id).Remise__c);
                    lstOli[i].Remise100Init = String.valueOf(mapProdToCondition.get(lstOli[i].Product2Id).Remise__c);
                }
                //verify sous-famille
                else if(mapSouFamilleToConditions.get(lstOli[i].Product2.Sous_familles_d_articles__c) != null){
                    System.debug('## Sous famille condition found: ' + mapSouFamilleToConditions.get(lstOli[i].Product2.Sous_familles_d_articles__c));
                    lstOli[i].Remise100 = String.valueOf(mapSouFamilleToConditions.get(lstOli[i].Product2.Sous_familles_d_articles__c).Remise__c);
                    lstOli[i].Remise100Init = String.valueOf(mapSouFamilleToConditions.get(lstOli[i].Product2.Sous_familles_d_articles__c).Remise__c);
                }
                //verify famille de produit
                else if(mapFamilleToConditions.get(lstOli[i].Product2.Famille_d_articles__c) != null){
                    System.debug('## mapFamilleProduit found: ' + mapFamilleToConditions.get(lstOli[i].Product2.Famille_d_articles__c) );
                    lstOli[i].Remise100 = String.valueOf(mapFamilleToConditions.get(lstOli[i].Product2.Famille_d_articles__c).Remise__c);
                    lstOli[i].Remise100Init = String.valueOf(mapFamilleToConditions.get(lstOli[i].Product2.Famille_d_articles__c).Remise__c);
                }
            }
            
        }

        mapRes.put('result' , lstOli);
        System.debug(mapRes);
        return mapRes;        
        // return null;
    }

    // DMU: 20190326 - query mode de gestion from object Product & make mode de gestion editable for saving
    @auraEnabled public static List<String> getPickListValueModeGestion(String prodId) {
        system.debug('### in method getPickListValueModeGestion prodId:' +prodId);

        // list<Product2> listprod = [select Mode_de_gestion__c from Product2 where id =: prodId];

        // string modeGestion = listprod.size()>0 ? (listprod[0].Mode_de_gestion__c == null ? '' : listprod[0].Mode_de_gestion__c) : '' ;

        // List<String> pickListValuesList = modeGestion.contains(';') ? modeGestion.split(';') : new List<String> {modeGestion};
        List<String> pickListValuesList = new list <String> {'a', 'b'};
        return pickListValuesList;
    }

    public static String getConditionRecordType(String devName){
        return Schema.SObjectType.Conditions__c.getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
    }   


}