@isTest
private class AP05_WSCalculateTVA_TEST{
	static User mainUser;

	static {
		mainUser = TestFactory.createAdminUser('AP01_BlueWSCallout_TEST@test.COM', 
												TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

        }
    }

    static testMethod void success_autre(){   
		
		system.runAs(mainUser){  
			Test.startTest();
				AP05_WSCalculateTVA.ResponseWrapper ap05 = LC05_CalculateTVA.CalculateTVA(AP_Constant.qu1_autr,AP_Constant.qu2_non);                
            Test.stopTest();
	       	map<String,String> x = ap05.CalculateTVAResponse[0];
	        list<string> lstTVA = ap05.CalculateTVAResponse[0].Values();
	        System.assert(lstTVA != null);
		}  
	}

	static testMethod void success_plus2ansOui(){   
		
		system.runAs(mainUser){  
			Test.startTest();
				AP05_WSCalculateTVA.ResponseWrapper ap05 = LC05_CalculateTVA.CalculateTVA(AP_Constant.qu1_plus2ans,AP_Constant.qu2_oui);                
            Test.stopTest();
	        list<string> lstTVA = ap05.CalculateTVAResponse[0].Values();
	        System.assert(lstTVA != null);   
		}  
	}

	static testMethod void success_plus2ansNon(){   
		  
		system.runAs(mainUser){  
			Test.startTest();
				AP05_WSCalculateTVA.ResponseWrapper ap05 = LC05_CalculateTVA.CalculateTVA(AP_Constant.qu1_plus2ans,AP_Constant.qu2_non);                
            Test.stopTest();
	        list<string> lstTVA = ap05.CalculateTVAResponse[0].Values();
	        System.assert(lstTVA != null);      
		}  
	}

	static testMethod void success_moins2ans(){   
		
		system.runAs(mainUser){  
			Test.startTest();
				AP05_WSCalculateTVA.ResponseWrapper ap05 = LC05_CalculateTVA.CalculateTVA(AP_Constant.qu1_moins2ans,AP_Constant.qu2_non);                
            Test.stopTest();
	        list<string> lstTVA = ap05.CalculateTVAResponse[0].Values();
	        System.assert(lstTVA != null);     
		}  
	}

	static testMethod void err_wrongParam(){   
		
		system.runAs(mainUser){  
			Test.startTest();
				AP05_WSCalculateTVA.ResponseWrapper ap05 = LC05_CalculateTVA.CalculateTVA('Particulier logement de PLUS de 2 ans xxx','Oui');                
            Test.stopTest();
	        list<string> lstErr = ap05.CalculateTVAResponse[0].Values();
	        System.assert(lstErr != null); 
	        System.AssertEquals(true,lstErr.contains(AP_Constant.errorMsg));    
		}  
	}

	static testMethod void err_wrongParamCatchExp(){   
		 
		system.runAs(mainUser){  
			Test.startTest();
				try{
					system.debug('### in try');
					AP05_WSCalculateTVA.ResponseWrapper ap05 = LC05_CalculateTVA.CalculateTVA('"""',''); 					
			        list<string> lstErr = ap05.CalculateTVAResponse[0].Values();
			        System.assert(lstErr != null);
				}catch(Exception e){
					system.debug('### enter catch TestClass: ' + e.getMessage()); 
                    System.AssertEquals(true,e.getMessage().contains('Unexpected character')); 
				}				            
            Test.stopTest();    
		}  
	}

	static testMethod void err_ParamBlank(){   
		  
		system.runAs(mainUser){  
			Test.startTest();
				AP05_WSCalculateTVA.ResponseWrapper ap05 = LC05_CalculateTVA.CalculateTVA('','');                
            Test.stopTest();
	        list<string> lstErr = ap05.CalculateTVAResponse[0].Values();
	        System.assert(lstErr != null);  
	        System.AssertEquals(true,lstErr.contains(AP_Constant.errorMsg));        
		}  
	}

}