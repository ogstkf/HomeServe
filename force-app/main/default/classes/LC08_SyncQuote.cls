/**
 * @File Name          : LC08_SyncQuote.cls
 * @Description        : synchronisation of Quote-Opp / quotelineItem-OpplineItem
 * @Author             : Spoon Consulting (DMU)
 * @Group              : 
 * @Last Modified By   : MNA
 * @Last Modified On   : 18-11-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver        Date          Author      Modification
 *==============================================================================
 * 1.0      19-10-2019     	 DMU         Initial Version
 * 1.1      22-10-2019     	 LGO         Added method used in OpplineItemTrigger
 * 1.1      05/12/2019       SHU         Initial Version  (CT-1062)
 * 1.2      11-02-2020       RRJ         Remise a niveau
 * 1.3      27-07-2020       DMU         CT-75 Added logic to map Aide CEE

**/

public class LC08_SyncQuote {
    public static Object getFld(sObject obj, String fld){
        String[] fldArr = fld.split('([.])');
        Object retVal;
        while(fldArr.size()>1){
            String fldStr = fldArr.remove(0);  
            obj = obj.getSobject(fldStr);
        }
        retVal = obj.get(fldArr[0]);
        
        return retVal;
    }

    public static void createOppLine(List<QuoteLineItem> lstNewQLI){

        for (QuoteLineItem qli : lstNewQLI){
            System.debug(qli.TECH_QuoteOpp__c);
        }
        List<OpportunityLineItem> lstOpportunityline = new List<OpportunityLineItem>();

        map <String,String> mapCustomSetSyncingQLI = mapCustomSetSyncingQLI();
        
        for(QuoteLineItem q : lstNewQLI){
            OpportunityLineItem oppline = new OpportunityLineItem();
            oppline.Quantity = q.Quantity;
            oppline.UnitPrice = q.UnitPrice;
            oppLine.PricebookEntryId = q.PricebookEntryId;
            //oppline.Product2Id = Test.getStandardPricebookId();
            oppline.OpportunityId = q.TECH_QuoteOpp__c;
            for(String fld: mapCustomSetSyncingQLI.keySet()){
                if(getFld(q, mapCustomSetSyncingQLI.get(fld)) != null){
                   oppline.put(fld, getFld(q, mapCustomSetSyncingQLI.get(fld)));
                }               
            }
            lstOpportunityline.add(oppline);
        }
        insert lstOpportunityline;
    }

    public static map<String,String> mapCustomSetSyncingQLI(){

        List<SyncingQLI__c> lstCustomSetting = new List<SyncingQLI__c>();
        lstCustomSetting = [select Name, QLI__c, ToCreateQLI__c, MappingType__c from SyncingQLI__c where MappingType__c='OpportunityLineItem-QuoteLineItem' ];

        Map<String, String> mapCustomSetting = new Map<String, String>();
        for(SyncingQLI__c q : lstCustomSetting){
            mapCustomSetting.put(q.Name, q.QLI__c);
        } 
        return mapCustomSetting;
    }

    public static map<String,String> mapCSCreateQLI(){

        List<SyncingQLI__c> lstCustomSetting = new List<SyncingQLI__c>();
        lstCustomSetting = [select Name, QLI__c, ToCreateQLI__c, MappingType__c from SyncingQLI__c where ToCreateQLI__c=true and MappingType__c='OpportunityLineItem-QuoteLineItem' ];

        
        Map<String, String> mapCustomSetting = new Map<String, String>();
        for(SyncingQLI__c q : lstCustomSetting){
            mapCustomSetting.put(q.QLI__c, q.Name);
        } 
        return mapCustomSetting;
    }    

    public static map<String,String> mapCSCreateQuote(){
        List<SyncingQLI__c> lstCustomSetting = new List<SyncingQLI__c>();
        lstCustomSetting = [select Name, QLI__c, ToCreateQuote__c, MappingType__c from SyncingQLI__c where ToCreateQuote__c=true and MappingType__c='Opportunity-Quote' ];
        
        Map<String, String> mapCustomSetting = new Map<String, String>();
        for(SyncingQLI__c q : lstCustomSetting){
            mapCustomSetting.put(q.QLI__c, q.Name);
        } 
        return mapCustomSetting;
    }  

    public static void deleteRelateOppLineItem(set<Id> setQuoId){
        //retrieve all opportunity line items that is associated with a quoteline item
        List<OpportunityLineItem> lstOli = [SELECT id, 
                                                TECH_CorrespondingQLItemId__c 
                                            FROM OpportunityLineItem 
                                            WHERE TECH_CorrespondingQLItemId__c  <> null
                                            AND TECH_CorrespondingQLItemId__c IN:setQuoId];
   
        if(lstOli.size() >0 ){
            system.debug('toDelete oppLineItem '+lstOli);
            delete lstOli;
        }
    }

    //lgo 22.10.19
    public static void updateOppLine(List<QuoteLineItem> lstQuoLi){
        system.debug('*** in updateOppLine');

        List<OpportunityLineItem> lstOpportunityline = new List<OpportunityLineItem>();
        Map<String, Id> MapQLOppIds = new Map<String, Id> ();

        Set<Id> setQuoId = new Set<Id>();

        for(QuoteLineItem quo : lstQuoLi)
            setQuoId.add(quo.Id);

        //retrieve all opportunity line items that is associated with a quoteline item
        for ( OpportunityLineItem varOli : [SELECT Id, TECH_CorrespondingQLItemId__c
                                            FROM OpportunityLineItem 
                                            WHERE TECH_CorrespondingQLItemId__c  <> null
                                            AND TECH_CorrespondingQLItemId__c IN: setQuoId]){
 
            MapQLOppIds.put(varOli.TECH_CorrespondingQLItemId__c, varOli.Id) ;                              

        }

        system.debug('*** MapQLOppIds: '+MapQLOppIds);
        map <String,String> mapCustomSetSyncingQLI = mapCustomSetSyncingQLI();

        for(QuoteLineItem q : lstQuoLi){ 
            
            for(Id quoId: MapQLOppIds.keySet()){
                if(quoId == q.Id){
                    OpportunityLineItem oppline = new OpportunityLineItem(Id = MapQLOppIds.get(quoId));
                    for(String fld: mapCustomSetSyncingQLI.keySet()){
                        if(fld <> 'Product2Id'){
                            oppline.put(fld, getFld(q, mapCustomSetSyncingQLI.get(fld)));
                        }
                    }
                    lstOpportunityline.add(oppline);
                }
            }
            

            
        }
        system.debug('## lstOpportunityline '+ lstOpportunityline);
        update lstOpportunityline;
    }
    
    public static map<String,String> mapCS_Opp_Quote(){

        List<SyncingQLI__c> lstCustomSetting = new List<SyncingQLI__c>();
        
        list<string> lstOppFieldName = system.label.OppFieldNameSynchro.split(';');
        lstCustomSetting = [select Name, QLI__c, ToCreateQLI__c, MappingType__c from SyncingQLI__c where MappingType__c='Opportunity-Quote' AND Name in:lstOppFieldName  ]; 

        Map<String, String> mapCustomSetting = new Map<String, String>();
        for(SyncingQLI__c q : lstCustomSetting){
            mapCustomSetting.put(q.Name, q.QLI__c);
        } 
        return mapCustomSetting;
    }

    //SHU20191211 : CT-1062 To integrate AP52_QuoteValide
	public static void SyncOppLineItems(Set<Id> setValideQuoteIds, set<Id> setValideOppIds){
        System.debug('### SyncOppLineItems  dmu*** !!');
        Set<String> setProId = new Set<String>();
        List<OpportunityLineItem> lstOpliTodel = new List<OpportunityLineItem>();
        List<QuoteLineItem> lstQLIToCreate = new List<QuoteLineItem>();
        List<QuoteLineItem> lstQLIToUpdate = new List<QuoteLineItem>();
        //Get quotelineitem fields to build select defined in custom setting mapping
        Map<String,String> mapCustomSetSyncingQLI = mapCustomSetSyncingQLI();
        Map<String,QuoteLineItem> mapQLIProductId = new Map<String,QuoteLineItem>();
        Map<String,OpportunityLineItem> mapOpLIProductId = new Map<String,OpportunityLineItem>();
    
        System.debug('### mapCustomSetSyncingQLI: '+mapCustomSetSyncingQLI);
        System.debug('### mapCustomSetSyncingQLI.size(): '+mapCustomSetSyncingQLI.size());

        if(mapCustomSetSyncingQLI.values().size() > 0){
            String qLineFlds = String.join(mapCustomSetSyncingQLI.values(), ','); 
            if(setValideQuoteIds.size() > 0){
                // MNA 06/05 TEC 600
                /*String qLineQuery = 'SELECT TECH_QuoteOpp__c, PriceBookEntryId, '+qLineFlds+' FROM QuoteLineItem WHERE QuoteId IN :setValideQuoteIds';
                
                
                lstQLIToCreate = Database.query(qLineQuery);   
                if(setValideOppIds.size() > 0){
                    lstOpliTodel = [select Id from OpportunityLineItem where OpportunityId IN :setValideOppIds]; 
            
                    if(lstOpliTodel.size() > 0){ //delete existing oppotunitylineitems
                       delete lstOpliTodel;
                    }
                    if(lstQLIToCreate.size() > 0){// recreate opportunitylineitems
                        createOppLine(lstQLIToCreate);
                    }
                }*/
                // MNA 18/05
                String qLineQuery = 'SELECT TECH_QuoteOpp__c, PriceBookEntryId, '+qLineFlds+' FROM QuoteLineItem WHERE QuoteId IN :setValideQuoteIds';
                
                
                if(setValideOppIds.size() > 0){
                    for (OpportunityLineItem opli : [select Id, Product2Id from OpportunityLineItem where OpportunityId IN :setValideOppIds]) {
                        setProId.add(opli.Product2Id);
                        mapOpLIProductId.put(opli.Product2Id, opli);
                    }
                    
                    for(QuoteLineItem qli : Database.query(qLineQuery)) {
                        setProId.add(qli.Product2Id);
                        mapQLIProductId.put(qli.Product2Id, qli);
                    }

                    for (String proId : setProId) {
                        if (mapQLIProductId.containsKey(proId) && mapOpLIProductId.containsKey(proId))
                            lstQLIToUpdate.add(mapQLIProductId.get(proId));
                        else if (mapQLIProductId.containsKey(proId) && !mapOpLIProductId.containsKey(proId))
                            lstQLIToCreate.add(mapQLIProductId.get(proId));
                        else if (!mapQLIProductId.containsKey(proId) && mapOpLIProductId.containsKey(proId))
                            lstOpliTodel.add(mapOpLIProductId.get(proId));
                    }
            
                    if (lstOpliTodel.size() > 0) {
                        delete lstOpliTodel;
                    }
                    if (lstQLIToCreate.size() > 0){  
                        createOppLine(lstQLIToCreate);
                    }
                    if (lstQLIToUpdate.size() > 0) {
                        updateOppLine(lstQLIToUpdate);
                    }
                }
            }
        }                
		
        //DMU 20200727 - CT-75 Added logic to map Aide CEE
        Map<String,String> mapCustomQuoteOpp = mapCS_Opp_Quote();
        map<String, String> mapOpp = new map<String, String>();

        if(mapCustomQuoteOpp.values().size() > 0){
            system.debug('*** enter mapCustomQuoteOpp');

            List<Opportunity> lstOpp = new List<Opportunity>();

            map <String,String> mapCS_Opp_Quote = mapCS_Opp_Quote();
            system.debug('*** enter mapCS_Opp_Quote: '+mapCS_Opp_Quote);

            String qLineFlds = String.join(mapCS_Opp_Quote.values(), ',');             
            String qLineQuery = 'SELECT id, OpportunityId, '+qLineFlds+' FROM Quote WHERE id IN: setValideQuoteIds';
            system.debug('***qLineQuery dmu:  '+qLineQuery);
            list<Quote> lstQuote = Database.query(qLineQuery);
            
            for(Quote q : lstQuote){
                Opportunity opp = new Opportunity();
                opp.Id = q.OpportunityId;                
                for(String fld: mapCS_Opp_Quote.keySet()){                    
                    if(getFld(q, mapCS_Opp_Quote.get(fld)) != null){                                            
                        opp.put(fld, getFld(q, mapCS_Opp_Quote.get(fld)));
                    }               
                }
                lstOpp.add(opp);
            }
            update lstOpp;
        }
    }

     //RRA 20200825- CT-143 Added logic remove value of field Aide_CEE__c in Quote and Opportunity 
     public static void deleteValueAideCEE (set<Id> setQuoId){
        System.debug('##### start operation delete value... ');
        List<Quote> lstValueQuoteAide = new List<Quote>(); 
        List<Opportunity> lstValueOppAide = new List<Opportunity>();
        for(Quote q : [SELECT id, OpportunityId, Aide_CEE__c FROM Quote WHERE id IN: setQuoId ]) { 
            System.debug('##### Aide_CEE__c ' + q.Aide_CEE__c);
            if(q.Aide_CEE__c != null) {
                q.Aide_CEE__c = null;
                lstValueQuoteAide.add(q);
                System.debug('#####toDeleteAideQuote== ' + lstValueQuoteAide);
            }
            for(Opportunity opp : [SELECT Id, Aide_CEE__c FROM Opportunity WHERE Id =: q.OpportunityId ]) {
                if(opp.Aide_CEE__c != null) {
                    opp.Aide_CEE__c = null;
                    lstValueOppAide.add(opp);
                    System.debug('#####toDeleteAIdeOpp== ' + lstValueOppAide);
                }
            }
        }
        update lstValueQuoteAide;
        update lstValueOppAide;
    }
}