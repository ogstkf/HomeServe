public with sharing class AP07_CalculAppointmentHour{

    // Récupère l'heure et les minutes du rendez-vous lors d'une création
    public static void setAppointmentHourInsert(List<ServiceAppointment> serviceAppointmentList){
    
        for(ServiceAppointment aServiceAppointment : serviceAppointmentList){
            if(aServiceAppointment.EarliestStartTime != NULL){
                aServiceAppointment.TECH_AppointmentHour__c = AP07_CalculAppointmentHour.calculHour(aServiceAppointment.EarliestStartTime);
            }
        }
    }
    
    // Récupère l'heure et les minutes du rendez-vous lors d'une mise à jour
    public static void setAppointmentHourUpdate(List<ServiceAppointment> serviceAppointmentList, map<Id, ServiceAppointment> oldMap){
    
        for(ServiceAppointment aServiceAppointment : serviceAppointmentList){
            
            if(oldMap.containsKey(aServiceAppointment.Id)){
                ServiceAppointment oldServiceAppointment = oldMap.get(aServiceAppointment.Id);

                // Si modification de l'heure de début du rendez-vous
                if(
                    (oldServiceAppointment.EarliestStartTime == NULL && aServiceAppointment.EarliestStartTime != NULL)
                    || (oldServiceAppointment.EarliestStartTime != aServiceAppointment.EarliestStartTime && aServiceAppointment.EarliestStartTime != NULL)
                ){
                    aServiceAppointment.TECH_AppointmentHour__c = AP07_CalculAppointmentHour.calculHour(aServiceAppointment.EarliestStartTime);
                }
                // Si rendez-vous nul
                else if(aServiceAppointment.EarliestStartTime == NULL){
                    aServiceAppointment.TECH_AppointmentHour__c = NULL;
                }
            }            
        }
    }
    
    public static String calculHour(DateTime theDateTime){
        theDateTime = Utils.fromUTCToGMT(theDateTime);
        
        return AP07_CalculAppointmentHour.getNumberWith2Digits(String.valueOf(theDateTime.hourGMT())) + ':' + AP07_CalculAppointmentHour.getNumberWith2Digits(String.valueOf(theDateTime.minuteGMT()));
    }
    
    public static String getNumberWith2Digits(String value){
        
        if(!String.isBlank(value)){
            while(value.length() < 2){
                value = '0' + value;
            }
        }
        
        return value;
    }
}