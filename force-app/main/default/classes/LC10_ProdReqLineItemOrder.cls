/**
* @File Name          : LC10_ProdReqLineItemOrder.cls
* @Description        : 
* @Author             : SBH
* @Group              : 
* @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
* @Last Modified On   : 03-12-2021
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    24/10/2019   SBH     Initial Version (CT-1154)
**/
public with sharing class LC10_ProdReqLineItemOrder {
    
    @AuraEnabled
    public static void init(){
        
    }

    //Ally
    @AuraEnabled
    public static Object getDataReport(String recordIds){
        List<Id> lstIds = recordIds.split(',');

        Map<String, Object> mapRes = new Map<String, Object>();
        if(String.isBlank(recordIds)){
            throw new AP_Constant.CustomException(Label.LC10_SelectionLigneUnique);
        }

        List<ProductRequestLineItem> lstPrli = [SELECT Id, Product2Id, Product2.Name,  Product2.Lot__c FROM ProductRequestLineItem WHERE Id IN :lstIds ORDER BY Product2.Name DESC];

        Set<String> setProdName = new Set<String>();
        for(ProductRequestLineItem prli : lstPrli){
            setProdName.add(prli.Product2.Name);
        }

        return setProdName;
    }

    @AuraEnabled
    public static String baseUrl(){

        String baseUrlFinal;
        

        String baseUrl = URL.getSalesforceBaseURL().toExternalForm().replace('https://', '').split('\\.')[0];
        baseUrl = baseUrl.removeEnd('--c');
        baseUrlFinal =  'https://' + baseUrl.toLowerCase() + '.lightning.force.com';
        System.debug('baseUrl: ' + baseUrlFinal);
        return baseUrlFinal;
    }
    
    //Rakshak
    @AuraEnabled
    public static Object getData(String recordIds){
        Map<String, Object> mapRes = new Map<String, Object>();
        if(String.isBlank(recordIds)){
            throw new AP_Constant.CustomException(Label.LC10_SelectionLigneUnique);
        }
        
        List<Id> lstIds = recordIds.split(',');
        List<Id> lstAccountId = new List<Id>();
        Set<Id> setAgences = new Set<Id>();
        List<ProductRequestLineItem> lstPrli = [SELECT Parent.WorkOrder.Account.Name ,Id, NeedByDate, Status, Agence__c, Parent.Status, Parent.Type__c, Product2Id, Product2.Name, Product2.ProductCode,Product2.Lot__c, ParentId, 
        Parent.ProductRequestNumber, QuantityRequested, ProductRequestLineItemNumber FROM ProductRequestLineItem WHERE Id IN :lstIds ORDER BY Product2.Name DESC];
        Map<Id, ProductRequestLineItem> mapPrli = new Map<Id, ProductRequestLineItem>(lstPrli);
        Map<Id, List<ProductRequestLineItem>> mapProdToPrli = new Map<Id, List<ProductRequestLineItem>>();
        Set<Id> setAgenceId = new Set<Id>();
   
        for(ProductRequestLineItem prli : lstPrli){
            setAgenceId.add(prli.Agence__c);
            if(mapProdToPrli.containsKey(prli.Product2Id)){
                mapProdToPrli.get(prli.Product2Id).add(prli);
            }else{
                mapProdToPrli.put(prli.Product2Id, new List<ProductRequestLineItem>{prli});
            }
        }
        if(setAgenceId.size()>1){
            throw new AP_Constant.CustomException(Label.LC10_MessageStatutBrouillon);
        }
        //CT-1411
        List<Fournisseur_agence__c> lstAgenceFournisseur = [SELECT Id, Agence__c, Fournisseur__c FROM Fournisseur_agence__c WHERE Agence__c IN :setAgenceId];
        Map<Id, Id> mapFA = new Map<Id, Id>();

        for(Fournisseur_agence__c fa : lstAgenceFournisseur){
            mapFA.put(fa.Id, fa.Fournisseur__c);
        }
        System.debug(setAgenceId);
        System.debug(mapProdToPrli.keySet());
        System.debug(mapFA.values());
        
        List<PriceBookEntry> lstPbe = [SELECT Id, Pricebook2Id, Pricebook2.Name, Pricebook2.Compte__c, UnitPrice, Product2Id, IsActive, Product2.Lot__c 
                                            FROM PriceBookEntry WHERE Product2Id IN :mapProdToPrli.keySet() AND Pricebook2.IsActive = true AND IsActive = true
                                                    AND Pricebook2.RecordType.DeveloperName = 'Achat' AND ((Pricebook2.Compte__c IN :mapFA.values()
                                                    AND Pricebook2.Agence__c IN: setAgenceId) OR Pricebook2.Agence__c = null)];
        Map<Id, List<PriceBookEntry>> mapPbIdToPbes = new Map<Id, List<PriceBookEntry>>();
        Map<Id, Pricebook2> mapPB = new Map<Id, Pricebook2>();
        Map<Id, Account> mapPBAcc = new Map<Id, Account>();
        Map<Id, Id> mapPbpbe = new Map<Id, Id>();

        for(PricebookEntry pbe : lstPbe){
            mapPbpbe.put(pbe.Id, pbe.Pricebook2Id);
        }
        System.debug('mapPbpbe: ' + mapPbpbe);
        Map<Id, Pricebook2> mapPricebook = new Map<Id, Pricebook2>();
        for(Pricebook2 pb : [SELECT Id, Name, Compte__c, Compte__r.Name, Compte__r.Mode_de_transmission_privil_gi__c, Compte__r.Type_de_fournisseurs__c
                            FROM Pricebook2 WHERE Id = :mapPbpbe.values() AND ((Compte__c IN :mapFA.values() AND Agence__c IN: setAgenceId) OR Pricebook2.Agence__c = null)]){
            mapPricebook.put(pb.Id, pb);
        }
        System.debug('mapPricebook: ' + mapPricebook);
        for(PricebookEntry pbe : lstPbe){
            if(mapPricebook.containskey(pbe.Pricebook2Id)){
                lstAccountId.add(mapPricebook.get(pbe.Pricebook2Id).Compte__c);
                // CT-1229
                mapPBAcc.put(mapPricebook.get(pbe.Pricebook2Id).Id, mapPricebook.get(pbe.Pricebook2Id).Compte__r);
            }
            
            if(mapPbIdToPbes.containsKey(pbe.Pricebook2Id)){
                mapPbIdToPbes.get(pbe.Pricebook2Id).add(pbe);
            }else{
                mapPbIdToPbes.put(pbe.Pricebook2Id, new List<PriceBookEntry>{pbe});
            }
            
            if(!mapPB.containsKey(pbe.Pricebook2Id)){
                mapPB.put(pbe.Pricebook2Id, new Pricebook2(Id = pbe.Pricebook2Id, Name = pbe.Pricebook2.Name));
            }
        }
        
        
        List<PbWrapper> lstData = new List<PbWrapper>();
        List<PbWrapper> lstConstructeur = new List<PbWrapper>();
        List<PbWrapper> lstNational = new List<PbWrapper>();
        List<PbWrapper> lstLocal = new List<PbWrapper>();
        List<PbWrapper> lstNone = new List<PbWrapper>();

        //Map Pricebook and conditions.
        Map<Id, List<Conditions__c>> mapPbCons = new Map<Id, List<Conditions__c>>();
        List<Conditions__c> lstConditions = [SELECT Id, Actif__c, Frais_de_port__c, Compte__c, Type__c, Mode__c, Min__c, Catalogue__c FROM Conditions__c WHERE Compte__c IN :lstAccountId AND Actif__c = True AND Frais_de_port__c = 0 AND Type__c = 'Standard' AND Conditions__c.RecordType.DeveloperName = 'Header'];
        Map<Id, Id> mapConAcc = new Map<Id, Id>();
        for(Conditions__c cons : lstConditions){
            mapConAcc.put(cons.Id, cons.Compte__c);

            if(mapPbCons.containsKey(cons.Catalogue__c)){
                mapPbCons.get(cons.Catalogue__c).add(cons);
            }else{
                mapPbCons.put(cons.Catalogue__c, new List<Conditions__c>{cons});
            }
        }

        Map<Id, Account> mapAccAcc = new Map<Id, Account>();
            
        for(Account acc : [SELECT Id, Mode_de_transmission_privil_gi__c, Mode_de_transmission__c FROM Account WHERE Id IN :mapConAcc.values()]){
            mapAccAcc.put(acc.Id, acc);
        }

        for(Id pbId: mapPbIdToPbes.keySet()){
            PbWrapper rowData = new PbWrapper();
            rowData.pb = mapPB.get(pbId);
            rowData.pricebookname = mapPB.get(pbId).Name;
            rowData.pricebook2Id =pbId;
            rowData.acc = mapPB.get(pbId).Compte__c;
            rowData.PriceBkId = pbId;

            List<Conditions__c> lstCondSel = new List<Conditions__c>();
            
            if(mapPbCons.containsKey(pbId)){
                for(Integer i = 0; i<mapPbCons.get(pbId).size(); i++){
                    Set<String> setModeTrans = new Set<String>();
                    Set<String> setModeTransPriv = new Set<String>();
                    List<String> lstCon = new List<String>();
                    
                    if(String.isNotBlank(mapPbCons.get(pbId)[i].Mode__c)){
                        lstCon = mapPbCons.get(pbId)[i].Mode__c.split(';');
                    }

                    if(String.isNotBlank(mapAccAcc.get(mapPbCons.get(pbId)[i].Compte__c).Mode_de_transmission__c)){
                        setModeTrans = new Set<String>(mapAccAcc.get(mapPbCons.get(pbId)[i].Compte__c).Mode_de_transmission__c.split(';'));
                    }

                    if(String.isNotBlank(mapAccAcc.get(mapPbCons.get(pbId)[i].Compte__c).Mode_de_transmission_privil_gi__c)){
                        setModeTransPriv = new Set<String>{mapAccAcc.get(mapPbCons.get(pbId)[i].Compte__c).Mode_de_transmission_privil_gi__c};
                    }

                    if(mapAccAcc.get(mapPbCons.get(pbId)[i].Compte__c).Mode_de_transmission_privil_gi__c != null) {
                        for(String str : lstCon){
                            if(setModeTransPriv.contains(str)){
                                lstCondSel.add(mapPbCons.get(pbId)[i]);
                            }
                        }
                    }
                    if(lstCondSel.size() == 0){
                        for(String str : lstCon){
                            if(setModeTrans.contains(str)){
                                lstCondSel.add(mapPbCons.get(pbId)[i]);
                            }
                        }
                    }

                }
            }

            if(lstCondSel.size() == 1){
                    rowData.francoport = lstCondSel[0].Min__c;
            }
            else{
                for(Integer i = 1; i<lstCondSel.size(); i++){
                    if(lstCondSel[i].Min__c > lstCondSel[i-1].Min__c){
                        rowData.francoport = lstCondSel[i].Min__c;
                    }else{
                        rowData.francoport = lstCondSel[i-1].Min__c;
                    }
                }
            }
            
            rowData.lstPrliWrap = new List<PrliWrapper>();
            for(PriceBookEntry pbe: mapPbIdToPbes.get(pbId)){
                rowData.productId = pbe.Product2Id;
                if(mapProdToPrli.containsKey(pbe.Product2Id)){
                    for(ProductRequestLineItem prli: mapProdToPrli.get(pbe.Product2Id)){
                        PrliWrapper prliWrap = new PrliWrapper(prli);
                        prliWrap.unitPrice = prli.Product2Id == pbe.Product2Id ? pbe.UnitPrice : 0;
                        prliWrap.prodLot = mapPrli.get(prli.Id).Product2.Lot__c; 
                        Decimal lot = pbe.Product2.Lot__c != null ? pbe.Product2.Lot__c : 1;
                        //prliWrap.QuantityRequested = prli.Product2Id == pbe.Product2Id ? Math.ceil(prli.QuantityRequested / lot) : 0;
                       
                        system.debug('Laxmi prli '+ prli);

                        
                        system.debug('Laxmi QuantityRequested '+ prli.QuantityRequested);
    
                        Integer productLot = Integer.valueOf(mapPrli.get(prli.Id).Product2.Lot__c);
                        Decimal quantityNonRounded  = 0;
                        Long quantityRounded = 0;
    
                       system.debug('Laxmi lot '+ productLot);
    
                        if(productLot == 1){
                            quantityNonRounded  = prli.QuantityRequested; 
                            quantityRounded = quantityNonRounded.round(System.RoundingMode.CEILING); 
                        }
                        else if (productLot > 1){
                            //Quantité demandée < lot et donc  : quantité = lot 
                            if(prli.QuantityRequested <= productLot){
                                quantityRounded = productLot;
                            }// Quantité demandée > lot  :  quantité demandée / lot : résultat (arrondi supérieur)
                            if(prli.QuantityRequested > productLot){
                                quantityNonRounded  = prli.QuantityRequested/productLot;
                                quantityRounded = quantityNonRounded.round(System.RoundingMode.CEILING)  * productLot; 
                            }   
                        }
                        else{
                            quantityRounded = 0;
                        }
                        system.debug('Laxmi quantityRounded '+ quantityRounded);                       
                       
                       
                        prliWrap.QuantityRequested = quantityRounded;
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                        prliWrap.totPrice = prliWrap.unitPrice*prliWrap.QuantityRequested;
                        prliWrap.agence = prli.Agence__c;
                        

                        if(prliWrap.prli.Product2Id == pbe.Product2Id){
                            prliWrap.pbeId = pbe.Id;
                        }
                        rowData.lstPrliWrap.add(prliWrap);
                    }
                }
            }
            
            if(mapPBAcc.get(pbId) == null || mapPBAcc.get(pbId).Type_de_fournisseurs__c == null){
                lstNone.add(rowData);
            }
            else if(mapPBAcc.get(pbId) != null && mapPBAcc.get(pbId).Type_de_fournisseurs__c != null){
                if(mapPBAcc.get(pbId).Type_de_fournisseurs__c == 'Constructeur'){
                    lstConstructeur.add(rowData);
                }
                else if(mapPBAcc.get(pbId).Type_de_fournisseurs__c == 'Distributeur National'){
                    lstNational.add(rowData);
                }
                else if(mapPBAcc.get(pbId).Type_de_fournisseurs__c == 'Distributeur Local'){
                    lstLocal.add(rowData);
                }
                else{
                    lstNone.add(rowData);
                }
                
            }

        }
        //Concatenate all lists.
        lstData.addAll(lstConstructeur);
        lstData.addAll(lstNational);
        lstData.addAll(lstLocal);
        lstData.addAll(lstNone);
        
        for(PbWrapper rowData: lstData){
            Set<Id> setCoveredPrliId = new Set<Id>();
            for(PrliWrapper prliWrap : rowData.lstPrliWrap){
                setCoveredPrliId.add(prliWrap.prli.Id);
            }
            rowData.lstPrliWrap.size();
            
            for(Id prliId: mapPrli.keySet()){
                if(!setCoveredPrliId.contains(prliId)){
                    PrliWrapper prliWrap = new PrliWrapper();
                    prliWrap.prodCode = mapPrli.get(prliId).Product2.ProductCode;
                    prliWrap.prodLot = mapPrli.get(prliId).Product2.Lot__c; 
                    prliWrap.prodName = mapPrli.get(prliId).Product2.Name;
                    prliWrap.unitPrice = null;
                    prliWrap.totPrice = null;
                    prliWrap.prli = new ProductRequestLineItem();
                    prliWrap.QuantityRequested = mapPrli.get(prliId).QuantityRequested;   
                    prliWrap.agence = mapPrli.get(prliId).Agence__c;                 
                    rowData.lstPrliWrap.add(prliWrap);
                }
                rowData.lstPrliWrap.sort();
            }
        }
        return lstData;
    }
    
    @AuraEnabled
    public static Object saveOrders(String JSONwrapper){
        
        // inserting records: 
        Savepoint sp = Database.setSavepoint();
        try{
            PbWrapper row = (PbWrapper)JSON.deserialize(JSONwrapper, PbWrapper.class);
            System.debug('Row' + row);
            
            List<Pricebook2> lstPb = [SELECT Id, Compte__c FROM Pricebook2  WHERE Id = :row.pricebook2Id];

            Order orderToinsert = new Order(
                RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get(AP_Constant.ordRtCommandeFournisseur).getRecordTypeId(),
                Pricebook2Id = row.pricebook2Id,
                EffectiveDate = System.today(),
                Status = AP_Constant.OrdStatusAchat,
                AccountId = lstPb[0].Compte__c,
                OwnerId = UserInfo.getUserId()
            );
            
            insert orderToinsert;
            
            List<OrderItem> lstOlToInsert = new List<OrderItem>();
            List<ProductRequestLineItem> lstPrliToUpdate = new List<ProductRequestLineItem>();
            
            for(PrliWrapper prli : row.lstPrliWrap){
                if(prli.unitPrice != null){
                    // Decimal quantityNonRounded  = prli.Product2.Lot__c != null ?  (prli.QuantityRequested / prli.Product2.Lot__c) : prli.QuantityRequested; 
                    // Long quantity = quantityNonRounded.round(System.RoundingMode.CEILING); 
                    // system.debug('Laxmi prli '+ prli);

                    // system.debug('Laxmi lot '+ prli.prodLot);
                    // system.debug('Laxmi QuantityRequested '+ prli.QuantityRequested);

                    // Integer productLot = Integer.valueOf(prli.prodLot);
                    // Decimal quantityNonRounded  = 0;
                    // Long quantityRounded = 0;

                   

                    // if(productLot == 1){
                    //     quantityNonRounded  = prli.QuantityRequested; 
                    //     quantityRounded = quantityNonRounded.round(System.RoundingMode.CEILING); 
                    // }
                    // else if (productLot > 1){
                    //     //Quantité demandée < lot et donc  : quantité = lot 
                    //     if(prli.QuantityRequested <= productLot){
                    //         quantityRounded = productLot;
                    //     }// Quantité demandée > lot  :  quantité demandée / lot : résultat (arrondi supérieur)
                    //     if(prli.QuantityRequested > productLot){
                    //         quantityNonRounded  = prli.QuantityRequested/productLot;
                    //         quantityRounded = quantityNonRounded.round(System.RoundingMode.CEILING)  * productLot; 
                    //     }   
                    // }
                    // else{
                    //     quantityRounded = 0;
                    // }
                    // system.debug('Laxmi quantityRounded '+ quantityRounded);


                    OrderItem ol = new OrderItem(
                        Product2Id = prli.productId,
                        Quantity = prli.QuantityRequested,
                        // Quantity = quantityRounded,
                        UnitPrice = prli.unitPrice,
                        PriceBookEntryId = prli.pbeId,
                        OrderId = orderToinsert.Id
                    );
                    
                    lstOlToInsert.add(ol);

                    if(prli.agence != null){
                        orderToinsert.Agence__c = prli.agence;
                    }
                    
                    lstPrliToUpdate.add(new ProductRequestLineItem(Id = prli.prli.Id, Commande__c = orderToinsert.Id, Status= AP_Constant.prliStatusEnCourDeDemande));
                }
            }

            if(!lstOlToInsert.isEmpty()){
                insert lstOlToInsert;
            }
System.debug(orderToinsert.pricebook2Id);
                    if(orderToinsert != null){
            update orderToinsert;
        }
System.debug(orderToinsert.pricebook2Id);
          
            if(!lstPrliToUpdate.isEmpty()){
                update lstPrliToUpdate;
            }
            
            return orderToinsert.id;
            
            }
            catch(NullPointerException e){
                throw new AuraHandledException('Sélectionner un fournisseur pour créer la commande');
            }
            catch(Exception e){
            Database.rollback(sp);
            System.debug('Message error: ' + e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    
    //Parent
    public class PbWrapper{
        @AuraEnabled public Pricebook2 pb;
        @AuraEnabled public Id PriceBkId;
        @AuraEnabled public String AccType;
        @AuraEnabled public String pricebookname;
        @AuraEnabled public Decimal francoport;
        @AuraEnabled public String pbeId {get; set;}
        @AuraEnabled public String pricebook2Id;
        @AuraEnabled public String productId;
        @AuraEnabled public Boolean isChecked;
        @AuraEnabled public Id acc;
        @AuraEnabled public List<PrliWrapper> lstPrliWrap;

        public PbWrapper(){}
    }
    
    //Child
    public class PrliWrapper implements Comparable{
        @AuraEnabled public ProductRequestLineItem prli;
        @AuraEnabled public String prodName;
        @AuraEnabled public String prodCode;
        @AuraEnabled public Decimal prodLot;
        @AuraEnabled public Decimal unitPrice;
        @AuraEnabled public Decimal totPrice;
        @AuraEnabled public String pbeId;
        @AuraEnabled public Id prlis;
        @AuraEnabled public Id productId;
        @AuraEnabled public String productName;
        @AuraEnabled public Id productRequestId;
        @AuraEnabled public String productRequestName;
        @AuraEnabled public String clientName;
        @AuraEnabled public Decimal QuantityRequested;     
        @AuraEnabled public String needByDate;
        @AuraEnabled public String agence;
        @AuraEnabled public String ProductRequestLineItemNumber;
        @auraEnabled public ProductRequestLineItem prli2;
        
        public PrliWrapper(){}
        public PrliWrapper(ProductRequestLineItem prli){
            this.prli = prli;
            this.prodName = prli.Product2.Name;
            this.prodCode = prli.Product2.ProductCode;
            this.prodLot = prli.Product2.Lot__c;

        }

        public Integer compareTo(Object obj){
            PrliWrapper prli = (PrliWrapper)(obj);

            if(this.prodName > prli.prodName){
                return 1;
            }

            if(this.prodName == prli.prodName){
                return 0;
            }

            return -1;
        }
    }
}