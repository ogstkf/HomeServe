global with sharing class LC05_CalculateTVA {
	
	//public LC05_CalculateTVA() {		
	//}

	@auraEnabled 
	global static AP05_WSCalculateTVA.ResponseWrapper CalculateTVA(string p1, string p2){
		
		return AP05_WSCalculateTVA.CalculateTVA(p1, p2);
	}
	
}