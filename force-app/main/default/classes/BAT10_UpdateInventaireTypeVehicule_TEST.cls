/**
 * @File Name          : BAT10_UpdateInventaireTypeVehicule_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 02/04/2020, 11:28:43
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/04/2020   ZJO     Initial Version
**/
@isTest
public with sharing class BAT10_UpdateInventaireTypeVehicule_TEST {
    static User adminUser;
    static List<sofactoapp__Raison_Sociale__c> lstSofactos;
    static List<ServiceTerritory> lstAgences;
    static Set<Id> setAgenceId = new Set<Id>();
    static List<Schema.Location> lstLocations;
    static List<ProductTransfer> lstProdTransfers;

    static{
        adminUser = TestFactory.createAdminUser('batch2@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){

            //List of Sofactos
            lstSofactos = new List<sofactoapp__Raison_Sociale__c>{
               new sofactoapp__Raison_Sociale__c(
                 Name = 'test sofacto 1',
                 sofactoapp__Credit_prefix__c = '1234',
                 sofactoapp__Invoice_prefix__c = '2134'
               ),
               new sofactoapp__Raison_Sociale__c(
                 Name = 'test sofacto 1',
                 sofactoapp__Credit_prefix__c = '1342',
                 sofactoapp__Invoice_prefix__c = '2431'
               ),
               new sofactoapp__Raison_Sociale__c(
                 Name = 'test sofacto 1',
                 sofactoapp__Credit_prefix__c = '1432',
                 sofactoapp__Invoice_prefix__c = '2341'
               )
            };

            insert lstSofactos;

            //Create Operating Hour
            OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
            insert OprtHour;

            //List of agences
            lstAgences = new List<ServiceTerritory>{
                new ServiceTerritory(
                    Name = 'test agence 1',
                    Agency_Code__c = '0001',
                    IsActive = True,
                    Inventaire_en_cours__c = false,
                    OperatingHoursId = OprtHour.Id,
                    Sofactoapp_Raison_Social__c = lstSofactos[0].Id
                ),
                new ServiceTerritory(
                    Name = 'test agence 2',
                    Agency_Code__c = '0002',
                    IsActive = True,
                    Inventaire_en_cours__c = false,
                    OperatingHoursId = OprtHour.Id,
                    Sofactoapp_Raison_Social__c = lstSofactos[1].Id
                ),
                new ServiceTerritory(
                    Name = 'test agence 3',
                    Agency_Code__c = '0003',
                    IsActive = True,
                    Inventaire_en_cours__c = false,
                    OperatingHoursId = OprtHour.Id,
                    Sofactoapp_Raison_Social__c = lstSofactos[2].Id
                )
            };

            insert lstAgences;

            //Populate Set Agence Id
            for (ServiceTerritory agence : lstAgences){
                setAgenceId.add(agence.Id);
            }

            //List of Locations
            lstLocations = new List<Schema.Location>{
                new Schema.Location(
                   LocationType = 'Véhicule',
                   Name = 'source test 1',
                   Agence__c = lstAgences[0].Id
                ),
                new Schema.Location(
                    LocationType = 'Entrepôt',
                    Name = 'source test 2',
                    Agence__c = lstAgences[0].Id
                ),
                new Schema.Location(
                    LocationType = 'Véhicule',
                    Name = 'destination test 1',
                    Agence__c = lstAgences[0].Id
                 ),
                 new Schema.Location(
                    LocationType = 'Entrepôt',
                    Name = 'destination test 2',
                    Agence__c = lstAgences[0].Id
                 )
            };

            insert lstLocations;

            //List of Product Transfers
            lstProdTransfers  = new List<ProductTransfer>{
               new ProductTransfer(
                SourceLocationId = lstLocations[0].Id,
                DestinationLocationId = lstLocations[2].Id,
                QuantitySent = 2,
                QuantityReceived = 2,
                Inventaire_en_cours__c = True
               ),
               new ProductTransfer(
                SourceLocationId = lstLocations[1].Id,
                DestinationLocationId = lstLocations[3].Id,
                QuantitySent = 2,
                QuantityReceived = 1,
                Inventaire_en_cours__c = True
               ),
               new ProductTransfer(
                SourceLocationId = lstLocations[0].Id,
                DestinationLocationId = lstLocations[3].Id,
                QuantitySent = 3,
                QuantityReceived = 2,
                Inventaire_en_cours__c = True
               ),
               new ProductTransfer(
                SourceLocationId = lstLocations[1].Id,
                DestinationLocationId = lstLocations[2].Id,
                QuantitySent = 1,
                QuantityReceived = 1,
                Inventaire_en_cours__c = True
               )          
            };
            insert lstProdTransfers;       
        }
    }

    @isTest
    public static void testBatch2(){
        System.runAs(adminUser){
            Test.startTest();
                BAT10_UpdateInventaireTypeVehicule batch = new BAT10_UpdateInventaireTypeVehicule(setAgenceId);
                Database.executeBatch(batch);
            Test.stopTest();

        List<ProductTransfer> lstProdTransfers = [SELECT Id, SourceLocation.Agence__c, IsReceived, Inventaire_en_cours__c FROM ProductTransfer Where SourceLocation.Agence__c IN: setAgenceId];
        system.assertEquals(lstProdTransfers[0].IsReceived, True);
        system.assertEquals(lstProdTransfers[0].Inventaire_en_cours__c, False);
        
        }  
    }
}