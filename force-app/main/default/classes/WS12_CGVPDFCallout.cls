/**
 * @File Name          : WS12_CGVPDFCallout.cls
 * @Description        : 
 * @Author             : Spoon Consulting (YGO)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 11-04-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         23-01-2019     		    YGO         Initial Version
**/
@RestResource(urlMapping='/WS12_CGVPDFCallout/*')
global with sharing class WS12_CGVPDFCallout {
        	
    @HttpPost
    global static Map<String, String> getPdfContent(String typePdf, String typeCGV) {
        Map<String, String> mapResult = new Map<String,String>();
        PageReference pageRef;
        String result;

        // if(typePdf == 'GAZ'){
        //     pageRef = Page.CGV_chauffage;
        // }
        // else if(typePdf == 'PAC'){
        //     pageRef = Page.CGV_NouveauxclientsAgencespilotes;
        // }
        // else if(typePdf == 'FIOUL'){
        //     pageRef = Page.CGV_NvxclientsAgencespilotes;
        // }
        // else if(typePdf == 'BOIS'){
        //     pageRef = Page.CGV_Nouveauxclientsautresagences;
        // }
        // else if(typePdf == 'SOLAIRE'){
        //     pageRef = Page.CGV_chauffage;
        // }
        // else if(typePdf == 'DEPANNAGE'){
        //     pageRef = Page.CGV_ContratSolaireNouveauxclients;
        // }
        // else if(typePdf == 'VISITE'){
        //     pageRef = Page.CGV_VisiteforfaitaireCG;
        // }
        // else {
        //     result = 'Not a valid type.';
        // }
        
        if(typeCGV=='devis'){
            pageRef = new PageReference('/apex/VFP13_CGV');
        }
        else if(typeCGV=='contrat'){
            pageRef = new PageReference('/apex/VFP13_CGVContrat');
        }
        pageRef.getParameters().put('id', typePdf);

        if(pageRef != null){
            Blob blobContent = (Test.isRunningTest() ? Blob.valueOf('test') : pageRef.getContentAsPDF());
            result = EncodingUtil.base64Encode(blobContent);
            System.debug(blobContent.size());
        }
        System.debug('Pdf:'+result);

        mapResult.put('Status:', 'SUCCESS');
        // mapResult.put('id:', agenceId);
        mapResult.put('TypePDF:', typePdf);
        mapResult.put('Message:', result);

        return mapResult;
    }
}