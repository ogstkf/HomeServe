/**
 * @File Name          : BAT04_ServContrEndStatus.cls
 * @Description        : 
 * @Author             : SHU
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 13/04/2020, 16:12:45
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * ---    -----------       -------           ------------------------ 
 * 1.0    27/11/2019          SHU             Initial Version (CT-1189)
 * ---    -----------       -------           ------------------------ 
**/
global with sharing class BAT04_ServContrEndStatus implements Database.Batchable <sObject>, Database.Stateful, Schedulable { 
    
    global String query;
    global Date fromDate;
    global Date toDate;    

    // To run batch for specific date : Database.executeBatch(new BAT04_ServContrEndStatus( Date.newInstance(2019,11,27) , null ));
    global BAT04_ServContrEndStatus(){
        toDate = System.Today().addDays(-1);
   
        query = 'SELECT Id, Contrat_resilie__c, Contract_Status__c, EndDate, Type__c ';
        query +=' FROM ServiceContract WHERE EndDate = :toDate '; // limiting records to before specificDate provided
        // query += fromDate != null? ' EndDate > :fromDate AND ' : ''; // Filter 'from date' if prevDate provided
        query +=' AND Contract_Status__c IN (\'Active\', \'Actif - en retard de paiement\', \'Pending first visit\', \'Draft\', \'Pending Client Validation\') ';
        query +=' AND Type__c = \'Individual\' ';
    }    

    global Database.QueryLocator start(Database.BatchableContext BC){
        system.Debug('### query: ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ServiceContract> lstServContr) {
        system.debug('### : ' + lstServContr.size() );
        list<ServiceContract> lstServCntEnded = new list<ServiceContract>();
        for(ServiceContract servContr : lstServContr){
            if((servContr.Contract_Status__c == 'Active' || servContr.Contract_Status__c == 'Pending first visit' || servContr.Contract_Status__c == 'Actif - en retard de paiement') && servContr.Contrat_resilie__c == true){
                servContr.Contract_Status__c = 'Résilié';
            }else if(servContr.Contrat_resilie__c == false){
                servContr.Contract_Status__c = 'Expired';
            }
            lstServCntEnded.add(servContr);
        }
        if(lstServCntEnded.size() >0){
            update lstServCntEnded;
        }
    }

    global void finish(Database.BatchableContext BC) {
    }

    global static String scheduleBatch() {
        BAT04_ServContrEndStatus sc = new BAT04_ServContrEndStatus();
        //String scheduleTime = '0 0 00 * * ?';
        //return System.schedule('Batch:', scheduleTime, scheduler);
        return System.schedule('Batch BAT04_ServContrEndStatus:' + Datetime.now().format(),  '0 0 0 * * ?', sc);
    }

    global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT04_ServContrEndStatus');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT04_ServContrEndStatus(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT04_ServContrEndStatus());
        }
    }    
}