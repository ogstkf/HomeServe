/**
 * @File Name          : CaseTriggerHandler.cls
 * @Description        :
 * @Author             : ANA
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 02-12-2021
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    01/10/2019   ANA     Initial Version
 * 1.1    30/06/2020   DMU     Commented AP16_CreateWoFromCase.generateWoRetrofit due to homeserve project
 * 1.2    24/07/2020   DMU     Added condition in method handleAfterInsert (type='Contrôle')
 **/
public with sharing class CaseTriggerHandler {

    Bypass__c userBypass;
    public static Boolean firstRun = true;

    public CaseTriggerHandler() {
        this.userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void handleAfterInsert(List<Case> lstCaseNew) {

        List<Id> lstId = new List<Id>();
        List<Case> lstCaseToWo = new List<Case>();
        List<Case> lstCaseWoVePlanning = new List<Case>();
        // List<Case> lstCaseWoRetroFit = new List<Case>();
        List<Case> lstCaseToCheck = new List<Case>();
        List<Case> lstCaseValid = new List<Case>();

        map<string, string> mapWOIdCaseId = new map<string, string>();

        map<string,string> mapCaIdSCId = new map<string,string>();
        map<string,string> mapCaIdSCIdPlanning = new map<string,string>();
        for(Case cas :lstCaseNew) {
            if(cas.Type.contains('DGI') || cas.Type.contains('A1') || cas.Type.contains('A2'))
                lstId.add(cas.Id);

            if((userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP16')) && firstRun) {
                //MNA 20210212: TEC 493
                lstCaseToCheck.add(cas);
            }
        }

        //MNA 20210212: Verifie si un WorkTYpe correspondant au case existe si ce n'est pas le cas alors on affiche un message d'erreur et retourne les cases valides
        if(lstCaseToCheck.size() > 0) {
            lstCaseValid = AP16_CreateWoFromCase.checkWorkType(lstCaseToCheck);
        }

        for(Case cas :lstCaseValid) {
            /*if(cas.Type.contains('DGI') || cas.Type.contains('A1') || cas.Type.contains('A2'))
                lstId.add(cas.Id);*/

            //RRJ: AP16
            //System.debug('#### cas.Type:'+cas.Type);
            //System.debug('#### cas.reason__c:'+cas.reason__c);
            if((userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP16')) && firstRun) {
                if(((cas.Type == 'Information Request'  && cas.reason__c !=null) || (cas.Type == 'Contrôle' && cas.reason__c!=null) || cas.Type == 'Installation' || cas.Type == 'Commissioning' || (cas.Type == 'Maintenance' && cas.reason__c!=null)  || cas.Type == 'Troubleshooting' || (cas.Type == 'Various Services' && cas.reason__c!=null)) 
                    && cas.AccountId != null && cas.AssetId != null && !cas.VE_Planning__c && cas.TECH_WO_FSL__c==null) 
                {
                    lstCaseToWo.add(cas);
                    mapCaIdSCId.put(cas.Id, cas.Service_Contract__c);
                }

                if(cas.AccountId != null && cas.AssetId != null && cas.Type == 'Maintenance' && cas.VE_Planning__c && cas.Optimized__c && cas.TECH_WO_FSL__c==null) {
                    lstCaseWoVePlanning.add(cas);
                    mapCaIdSCIdPlanning.put(cas.Id, cas.Service_Contract__c);
                }

                // if((cas.Type == 'Installation' || cas.Type == 'Commissioning' || cas.Type == 'First Maintenance Visit (new contract)' || cas.Type == 'Maintenance' || cas.Type == 'Troubleshooting') && cas.AccountId != null && cas.AssetId != null && !cas.VE_Planning__c && ((cas.Origin == 'IZI' && cas.TECH_Dateheure_intervention_Izi__c != null ) || (cas.Origin == 'Cham Digital' && cas.Agency_fixed_slot__c != null) || cas.Origin == 'MyChauffage')  != null && cas.TECH_Payment_ok__c) {
                //  lstCaseWoRetroFit.add(cas);
                //  System.debug('Handler CaseRetroFit');
                // }
                if(cas.TECH_WO_FSL__c!=null){
                    mapWOIdCaseId.put(cas.TECH_WO_FSL__c,cas.Id);
                }
            }
        }


        //RRJ 20191010: AP16 calls
        if(lstCaseToWo.size() > 0) {
            AP16_CreateWoFromCase.generateWo(lstCaseToWo, mapCaIdSCId);
        }
        if(lstCaseWoVePlanning.size() > 0) {
            AP16_CreateWoFromCase.generateWoVePlanning(lstCaseWoVePlanning, mapCaIdSCIdPlanning);
        }
        // DMU 20200630 -  Commented AP16_CreateWoFromCase.generateWoRetrofit due to homeserve project
        // if(lstCaseWoRetroFit.size() > 0) {
        //  AP16_CreateWoFromCase.generateWoRetrofit(lstCaseWoRetroFit);
        // }
        if(mapWOIdCaseId.size()>0){
            AP16_CreateWoFromCase.assigneCaseIdOnWO(mapWOIdCaseId);
        }

        if(lstId.size() > 0) {
            AP15_AttachCasePdfToAttachment.AttachToCase(lstId);
        }
        firstRun = false;
    }

    public void handleBeforeUpdate(List<Case> lstCaseOld, List<Case> lstCaseNew) {
        system.debug('**** in handleBeforeUpdate');
        System.debug('##### case old: ' + lstCaseOld [0].Status);
        System.debug('##### case new: ' + lstCaseNew [0].Status);

        List<Case> lstNewCases = new List<Case>();
        //Check if status is closed and not same as before it was changed.
        for(integer i = 0; i < lstCaseNew.size(); i ++) {
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP27')) {
                if(lstCaseNew [i].Status == AP_Constant.StatusClosed && (lstCaseNew [i].Status != lstCaseOld [i].Status)) {
                    lstNewCases.add(lstCaseNew [i]);
                }
            }
        }
        if(lstNewCases.size() > 0) {
            AP27_CaseCannotClose.CaseCannotClose(lstNewCases);
        }
    }

    public void handleAfterUpdate(List<Case> lstCaseOld, List<Case> lstCaseNew) {
        List<Case> lstCaseToWo = new List<Case>();
        List<Case> lstCaseWoVePlanning = new List<Case>();
        List<Case> lstCaseToCheck = new List<Case>();

        map<string,string> mapCaIdSCId = new map<string,string>();
        map<string,string> mapCaIdSCIdPlanning = new map<string,string>();
        // List<Case> lstCaseWoRetroFit = new List<Case>();

                    
        //MNA 20210212: TEC 493
        //#region
        for(Integer i = 0; i < lstCaseNew.size(); i ++) {
            if((userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP16')) && firstRun) {    
                if (lstCaseNew[i].Type != lstCaseOld[i].Type || lstCaseNew[i].Reason__c != lstCaseOld[i].Reason__c || lstCaseNew[i].AssetId != lstCaseOld[i].AssetId
                    || lstCaseNew[i].Type_de_collectif_WT__c != lstCaseOld[i].Type_de_collectif_WT__c) {
                        lstCaseToCheck.add(lstCaseNew[i]);
                }
            }
        }

        //MNA 20210212: Verifie si un WorkTYpe correspondant au case existe si ce n'est pas le cas alors on affiche un message d'erreur et retourne les cases valides
        if(lstCaseToCheck.size() > 0) {
            lstCaseNew = AP16_CreateWoFromCase.checkWorkType(lstCaseToCheck);       // assigne à lstCaseNew la liste des cases valides
        }

        //#endregion

        for(Integer i = 0; i < lstCaseNew.size(); i ++) {
            //System.debug('##### RRJ case size' + lstCaseNew.size());

            if((userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP16')) && firstRun) {
                if(
                    (lstCaseNew [i].Type == 'Information Request' && lstCaseOld [i].Type == 'Information Request' && lstCaseOld [i].Reason__c == null && lstCaseNew [i].Reason__c != null)                   
                    && lstCaseNew [i].AccountId != null 
                    && lstCaseNew [i].AssetId != null 
                    //&& lstCaseNew[i].VE_Planning__c != lstCaseOld [i].VE_Planning__c
                    && lstCaseNew[i].TECH_WO_FSL__c==null
                ) {
                    System.debug('###### met the condition');
                    lstCaseToWo.add(lstCaseNew [i]);
                    mapCaIdSCId.put(lstCaseNew [i].Id, lstCaseNew [i].Service_Contract__c);
                }

                if(lstCaseNew [i].AccountId != null && lstCaseNew [i].AssetId != null && lstCaseNew [i].Type != lstCaseOld [i].Type && lstCaseNew [i].VE_Planning__c != lstCaseOld [i].VE_Planning__c
                   && lstCaseNew [i].Optimized__c != lstCaseOld [i].Optimized__c && lstCaseNew[i].TECH_WO_FSL__c==null) {
                    lstCaseWoVePlanning.add(lstCaseNew [i]);
                    mapCaIdSCIdPlanning.put(lstCaseNew [i].Id, lstCaseNew [i].Service_Contract__c);
                }

                // if(lstCaseNew [i].Type != null && lstCaseNew [i].AccountId != null && lstCaseNew [i].AssetId != null && !lstCaseNew [i].VE_Planning__c
                //    && ((lstCaseNew [i].Origin == 'IZI' && lstCaseNew [i].TECH_Dateheure_intervention_Izi__c != null ) || (lstCaseNew [i].Origin == 'Cham Digital' && lstCaseNew [i].Agency_fixed_slot__c != null) || lstCaseNew [i].Origin == 'MyChauffage')  && lstCaseNew[i].TECH_Payment_ok__c != lstCaseOld[i].TECH_Payment_ok__c && lstCaseNew[i].TECH_Payment_ok__c) {
                //  lstCaseWoRetroFit.add(lstCaseNew [i]);
                // }

            }
        }
        //RRJ 20191010: AP16 calls
        if(lstCaseToWo.size() > 0) {
            AP16_CreateWoFromCase.generateWo(lstCaseToWo, mapCaIdSCId);
        }
        if(lstCaseWoVePlanning.size() > 0) {
            AP16_CreateWoFromCase.generateWoVePlanning(lstCaseWoVePlanning, mapCaIdSCIdPlanning);
        }
        //20200630 -  Commented AP16_CreateWoFromCase.generateWoRetrofit due to homeserve project
        // if(lstCaseWoRetroFit.size() > 0) {
        //  AP16_CreateWoFromCase.generateWoRetrofit(lstCaseWoRetroFit);
        // }
        firstRun = false;
    }
}