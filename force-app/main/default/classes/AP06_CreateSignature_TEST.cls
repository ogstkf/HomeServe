@IsTest
public with sharing class AP06_CreateSignature_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Test Class for AP06_CreateSignature
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 21-03-2019   MGR	   1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/

	static User mainUser;

	static {

		mainUser = TestFactory.createAdminUser('AP06_CreateSignature_TEST@test.COM', TestFactory.getProfileAdminId());
		insert mainUser;

		System.runAs(mainUser){
			CreateSignature__c cs = new CreateSignature__c();
	        cs.Name='SecretKey';
	       	cs.SecretKeyValue__c = 'test';
	     	insert cs;
		}
	}

	@IsTest static void testCalculateSignature(){
		System.runAs(mainUser){
			Test.startTest();
			Map<String, String> mapData = new Map<String, String>();
			mapData.put('vads_test', 'test');
			mapData.put('vads_test1', 'test1');
			String sig = AP06_CreateSignature.calculateSignature(mapData);
			Test.stopTest();
		}
	}

	@IsTest static void testGenerateSignature(){
		System.runAs(mainUser){
			Test.startTest();
				String sig = AP06_CreateSignature.generateSignature('test', 'test1');
				System.assertEquals('9orh5LT7WfaLbjCsUWGKqMXnJJc1UXq1NE+5AEhOKkc=', sig);
			Test.stopTest();
		}
	}

}