/**
 * @File Name          : VFC07_MassAvisDePassage.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    21/08/2019, 18:04:13   RRJ     Initial Version
**/
public with sharing class VFC73_MassContrat {
    public ApexPages.StandardSetController controller {get; set;}
    public List<ServiceContract> lstSelected {get; set;}
    public String lstId {get; set;}
    public Boolean isEmpty {get; set;}
    
    public VFC73_MassContrat(ApexPages.StandardSetController controller){
        System.debug('Controller');
        this.controller = controller;
        lstSelected = controller.getSelected();
        System.debug('lstSelected' + lstSelected);
        lstId = serializeIds();
        System.debug('lstId' + lstId);
        isEmpty = controller.getSelected().isEmpty();         
    }

    public String serializeIds(){
        List<String> lstStr = new List<String>();
        System.debug('lstSelected' + lstSelected);
        for(ServiceContract srvCon : lstSelected){
            lstStr.add(srvCon.Id);
        }
        System.debug('lstStr' + lstStr);
        return JSON.serialize(lstStr);
    }

    @RemoteAction
    public static String openPDF(List<String> lstServConId){
        System.debug('##### lstsrvCtId: '+ lstServConId);
        String resp = '';
        Savepoint sp = Database.setSavepoint();
        List<ContentDocumentLink> lstCDLId = new List<ContentDocumentLink>();
        Map<String, String> mapCVTitleAgencyName = new Map<String, String>(); 
        list<ContentVersion> contentVersionLst = new List<ContentVersion>();
        map<String, String> mapCWNAmeId = new map<String, String>();     
        Map<String, String> mapCVTitleSAId = new Map<String, String>(); 
        string userAlias = [SELECT Id, Alias FROM USER WHERE Id = :UserInfo.getUserId() LIMIT 1].Alias;

        try{

            for (ServiceContract sa : [SELECT Id, Agency__r.Name FROM ServiceContract WHERE Id IN : lstServConId]) {
                System.debug('##### sa Id: '+ JSON.serialize(sa.Id));
                PageReference template = new PageReference('/apex/VFP73_Contrat');
                template.getParameters().put('lstIds', '['+JSON.serialize(sa.Id)+']');
                Blob attData = Blob.valueOf('t');
                if(!Test.isRunningTest()) attData = template.getContentAsPDF();
                                
                //create attachments to insert
                ContentVersion conVer = new ContentVersion();
                conVer.ContentLocation = 'S';
                conVer.PathOnClient = generateName(userAlias);
                conVer.Title = generateName(userAlias);
                conVer.VersionData = attData;  
                mapCVTitleAgencyName.put(conVer.Title, sa.Agency__r.Name);
                mapCVTitleSAId.put(conVer.Title, sa.Id);
                contentVersionLst.add(conVer); 
                System.debug('*** nomAgence: ' + sa.Agency__r.Name);     
                System.debug('*** Title: ' + conVer.Title);                   
            }

            //Considering listViews will be filtered by agence, in list of SA below, all agence will be same.
            String agenceName = mapCVTitleAgencyName.size()>0 ? mapCVTitleAgencyName.values()[0] : '' ;
            System.debug('*** agenceName: ' + agenceName);    

            String foldertoUse = 'Avis de passage '+ agenceName;

            System.debug('*** mapCVTitleAgencyName222: ' + mapCVTitleAgencyName);
            //PDF qui englobe tous les pdf unitaire
            PageReference template1 = new PageReference('/apex/VFP73_Contrat'); 
            template1.getParameters().put('lstIds',  JSON.serialize(lstServConId));
            Blob attData1 = Blob.valueOf('t');
            if(!Test.isRunningTest()) attData1 = template1.getContentAsPDF();                                     
            //create attachments to insert
            ContentVersion conVer1 = new ContentVersion();
            conVer1.ContentLocation = 'S';
            conVer1.PathOnClient = generateName(userAlias);
            conVer1.Title = generateName(userAlias);
            conVer1.VersionData = attData1;  
            contentVersionLst.add(conVer1);

            if(contentVersionLst.size()>0) {
                insert contentVersionLst;
            }

            //Get Content Documents                               
            List<ContentVersion> docList = [SELECT Id, ContentDocumentId, Title FROM ContentVersion WHERE Id IN : contentVersionLst order by ContentSize desc];            
            System.debug('docList : ' + docList);

            if(docList.size()>0){
                ContentDocument conDoc = new ContentDocument(Id = docList[0].ContentDocumentId);
                System.debug('conDoc : ' + conDoc);
                Pagereference resp1 = new ApexPages.StandardController(conDoc).view();
                resp = resp1.getUrl();
            }

            for(ContentWorkspace cw: [SELECT Id, RootContentFolderId, Name FROM ContentWorkspace WHERE Name =: foldertoUse OR Name LIKE '%Avis De Passage%']){ 
                mapCWNAmeId.put(cw.Name,cw.Id);
            }
            System.debug('*** mapCWNAmeId: ' + mapCWNAmeId);
            System.debug('*** mapCVTitleAgencyName: ' + mapCVTitleAgencyName);

            for(ContentVersion cv : docList) {              //first element of docList is the consolidated pdf when more than 1 Sa selected
                ContentDocumentLink cdl = new ContentDocumentLink();                        
                cdl.ContentDocumentId =  cv.ContentDocumentId; 
                cdl.ShareType = 'I';
                cdl.Visibility = 'AllUsers';                
                cdl.LinkedEntityId = mapCVTitleSAId.containsKey(cv.Title) ? mapCVTitleSAId.get(cv.Title) : (mapCWNAmeId.containsKey(foldertoUse) ? mapCWNAmeId.get(foldertoUse) : mapCWNAmeId.get('Avis De Passage'));
                //mapCWNAmeId.containsKey(foldertoUse) ? mapCWNAmeId.get(foldertoUse) : mapCWNAmeId.get('Avis De Passage'); //mapCWNAmeId.containsKey(mapCVTitleAgencyName.get(cv.Title)) ? mapCWNAmeId.get(mapCVTitleAgencyName.get(cv.Title)) : !Test.isRunningTest() ?  mapCWNAmeId.get('Avis De Passage') : mapCWNAmeId.get('Avis De Passage test');
                lstCDLId.add(cdl);
            }
            
            if (lstCDLId.size() > 0){
                upsert lstCDLId;
            }
            
            List<ServiceContract> lstSaToUpdate = new List<ServiceContract>();
            for(String strSaId: lstServConId){
                ServiceContract servApp  = new ServiceContract(
                    Id = strSaId
                    // Visit_Notice_Generated_In_Mass__c = true,
                    // Date_Visit_Notice_Generated__c = system.today()
                );
                lstSaToUpdate.add(servApp);
            }
            update lstSaToUpdate;
                        
        } catch(Exception e){
            Database.rollback(sp);
            System.debug('##### error occured: '+e.getMessage()+'-'+e.getStackTraceString());
            throw new AP_Constant.CustomException(e.getMessage());
        }
        System.debug('resp' + resp);
        
        return resp;        
    }

    private static String generateName(String alias){ 
        system.debug('*** in generateName alias:  '+alias);        

        string randomUniqueIdentifier = randomizeStringVFC07('');
        system.debug('*** in generateName randomUniqueIdentifier:  '+randomUniqueIdentifier);

        String pdfName = '';
        DateTime datToday = DateTime.now();
        pdfName += datToday.day() < 10 ? '0'+datToday.day() : datToday.day().format();
        pdfName += datToday.month() < 10 ? '0'+datToday.month() : datToday.month().format();
        pdfName += datToday.year();
        pdfName += datToday.hour() < 10 ? '0'+datToday.hour() : datToday.hour().format();
        pdfName += datToday.minute() < 10 ? '0'+datToday.minute() : datToday.minute().format();
        pdfName += datToday.second() < 10 ? '0'+datToday.second() : datToday.second().format();
        pdfName += '-';
        pdfName += alias;
        pdfName += '-' + randomUniqueIdentifier;
        pdfName += '.pdf';
        return pdfName;
    }

    public static String randomizeStringVFC07(String name) {
        String charsForRandom = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < 6) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), charsForRandom.length());
            randStr += charsForRandom.substring(idx, idx + 1);
        }
        return name + randStr;
    }

}