@RestResource(urlMapping='/v2.0/CalculateTVA/*')
global with sharing class AP05_WSCalculateTVA {

	@HttpPost
    global static ResponseWrapper CalculateTVA(string p1, string p2){
    	system.debug('### In method CalculateTVA');

    	Map <String, String> mapResponse = new Map <String, String>();
		try{
			string strTVA = '';

			string strRequest = '{"qu1":"'+p1+'","qu2":"'+p2+'"}';			
			//String strRequest = RestContext.request.requestBody.toString();
			system.debug('###strRequest: '+strRequest);

			RequestWrapper request = (RequestWrapper) JSON.deserialize(strRequest, RequestWrapper.class);	        
			system.debug('###request: '+request);				

			string qu1_autr = AP_Constant.qu1_autr;
			string qu1_plus2ans = AP_Constant.qu1_plus2ans;
			string qu1_moins2ans = AP_Constant.qu1_moins2ans;
			string qu2_oui = AP_Constant.qu2_oui;
			string qu2_non = AP_Constant.qu2_non;
			string errorMsg = AP_Constant.errorMsg;

			if(string.IsBlank(request.qu1) || string.IsBlank(request.qu2)){
				mapResponse.put('ERREUR ', errorMsg);
			}
			if((!((request.qu1.equals(qu1_autr) || request.qu1.equals(qu1_moins2ans) || request.qu1.equals(qu1_plus2ans)))) || (! (request.qu2.equals(qu2_oui) || request.qu2.equals(qu2_non))))
			{
				mapResponse.put('ERREUR', errorMsg);
			}
			else{
				if(request.qu1 == qu1_autr){
					strTVA = '0.2';
				}
				if(request.qu1 == qu1_moins2ans){
					strTVA = '0.2';
				}
				if(request.qu1 == qu1_plus2ans && request.qu2.contains(qu2_oui)){
					strTVA = '0.055';
				}
				if(request.qu1 == qu1_plus2ans && request.qu2.contains(qu2_non)){
					strTVA = '0.1';
				}

				mapResponse.put('TVA ', strTVA); 
			}	     
	    }
	    catch(Exception ex){
            system.debug('###In catch FAIL');
            mapResponse.put('ERREUR', ex.getMessage());
        }        
        system.debug('###mapResponse: '+mapResponse);
        return new ResponseWrapper(mapResponse);       
    }

	public class RequestWrapper{
		string accType;
        string qu1;
        string qu2;       
    } 

    global class ResponseWrapper{
        global List <Map <String, String>> CalculateTVAResponse;
        global ResponseWrapper(Map <String, String> mapResponse){
            CalculateTVAResponse = new List <Map <String, String>>();
            CalculateTVAResponse.add(mapResponse);
        }
    }
}