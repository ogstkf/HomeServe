public class LC06_ViewPDF {
	@AuraEnabled
    public static map<string,Object> getSA(String woId){
		system.debug('## starting method getSA');

		map<String,Object> mapOfResult = new map<String,Object>();
        List<ServiceAppointment> lstSA = new List<ServiceAppointment>();
        lstSA=[SELECT Id  FROM ServiceAppointment where ParentRecordId= :woId order by lastmodifiedDate desc limit 1];
        system.debug('## lstSA' +lstSA);


		if(lstSA.size()==1){
			mapOfResult.put('serviceAppointementID',lstSA[0].Id);
            mapOfResult.put('error',false);
        }else{
            mapOfResult.put('error',true);
        }

        return  mapOfResult;                            
	}
}