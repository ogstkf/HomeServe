/**
 * @File Name          : AP27_OpportunityQLIs.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 02-12-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    18/10/2019   KZE     Initial Version
 * 1.1    04/02/2020   RRJ     Added GeneratePDFDevis Method.
**/
public with sharing class AP27_OpportunityQLIs {
    public static boolean hasRunAp27 = false;

    public static void createQLIs(set<Id> setOppIds){
        QuoteLineItemTriggerHandler.notRun = true;
        QuoteTriggerHandler.notRun = true;
        System.debug('starting method createQLIs');

        hasRunAp27 = true;
        Boolean createPDF = false;

        Map<Id,List<OpportunityLineItem>> mapOpptoOLIs = new Map<Id,List<OpportunityLineItem>>();
        Map<Id,Quote> mapOpptoQs = new Map<Id,Quote>();
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>();
        List<Quote> lstQsToInsert = new List<Quote>();
        List<Quote> lstQsToDelete = new List<Quote>();
        List<QuoteLineItem> lstQlisToInsert = new List<QuoteLineItem>();
        Set<Id> setQuoId = new Set<Id>();
        List<Attachment> lstImgAttachment = new List<Attachment>();
        List<Quote> lstQuoValideSigne = new List<Quote>();
        Set<Quote> setQuoIntervImm = new Set<Quote>();
        Map<Id, Quote> mapQuoValideSigne = new Map<Id, Quote>();
        Map<Id, Id> mapQuoAgency = new Map<Id, Id>();
        
        for(Opportunity o: [SELECT Id 
                                , Name 
                                , Pricebook2Id 
                                , Ordre_d_execution__c
                                , Ordre_d_execution__r.TECH_LogementId__c  
                                , Ordre_d_execution__r.AssetId
                                , Agency__c 
                                , Description 
                                , sofactoapp__Payment_mode__c 
                                , Total_Price__c 
                                , TECH_Demande_d_acompte__c
                                , CloseDate
                                //TEC-39
                                , CampaignId 
                                , sofactoapp__Solde__c 
                                , sofactoapp__Contact__c
                                , Account.IsPersonAccount 
                                , Account.PersonEmail
                                , Account.BusinessAccountEmail__c 
                                , Account.Phone
                                , Account.Fax 
                                , Account.Name 
                                , Account.BillingStreet
                                , Account.BillingCity
                                , Account.BillingCountry
                                , Account.BillingState
                                , Tech_Devis_sign_par_le_client__c
                                , TECH_Titre_du_devis__c
                                , TECH_Signature_du_client__c
                                , Signature_client__c
                                , Signature_Hash_Client__c
                                , Date_de_signature_du_client__c
                                , APP_intervention_immediate__c
                                , Montant_de_l_acompte__c 
                                , StageName 
                                , TECH_date_prevue_de_debut_des_travaux__c 
                                , TECH_Type_de_devis__c
                                , TECH_Duree_estimee_des_travaux__c 
                                , TECH_Mode_de_paiement_de_l_acompte__c 
                                , TECH_Mode_de_paiement_du_devis__c
                                , Contrat_collectif_pas_de_signature__c
                                , TECH_Commentaire_du_technicien__c ,
                                Besoin_de_financement__c ,
                                conserver_les_pieces__c ,
                                Attestation_TVA_completee__c ,
                                Aide_CEE__c ,
                                MaPrimeRenov__c,
                                Remise_exceptionnelle_TTC__c,
                                Coup_de_Pouce__c
                                , (SELECT Id                   
                                    , Description
                                    , Discount
                                    , Product2Id
                                    , Quantity
                                    , Remise_en_euros__c
                                    , Remise_en_100__c
                                    , ServiceDate
                                    , sofactoapp__VAT_rate__c
                                    , TECH_CorrespondingQLItemId__c
                                    , UnitPrice
                                    , PricebookEntryId             
                                    , TECH_Garantie_CHAM__c
                                    , TECH_Garantie_Fournisseur__c
                                    , Description__c
                                    , Sequence__c,
                                    TECH_Aide_de_letat__c,
                                    TECH_Remise_Exceptionnelle__c
                                    

                                FROM OpportunityLineItems), 
                                (SELECT Id, Counter__c, Status FROM Quotes WHERE isSync__c = true LIMIT 1)
                            From Opportunity
                            where id IN :setOppIds]){
            mapOpp.put(o.Id, o);
            
            //delete existing opps
            if(o.Quotes.size()>0){
                setQuoId.add(o.Quotes[0].Id);                
            }

            //get olis            
            List<OpportunityLineItem> lstOLIs = o.OpportunityLineItems;
            System.debug('lstOLIs.size(): '+ lstOLIs.size());
            if(lstOLIs.size() >0){
                mapOpptoOLIs.put(o.ID, lstOLIs);

                //create new quote
                Quote q= new Quote(Name= o.Name, OpportunityId= o.Id, Pricebook2Id = o.Pricebook2Id );
                if(o.Quotes.size()>0){
                    q.Id = o.Quotes[0].Id;
                    q.Status = o.Quotes[0].Status;
                }

                //get map Custom Setting
                map<String,String> mapCSCreateQuote = LC08_SyncQuote.mapCSCreateQuote();

                for(String fld: mapCSCreateQuote.keySet()){
                    system.debug('*** fld: '+fld);
                    q.put(fld, LC08_SyncQuote.getFld(o, mapCSCreateQuote.get(fld)));
                }
                
                if(o.Ordre_d_execution__c != null){
                    list <ServiceAppointment> lstSA = [select id, ArrivalWindowStartTime, EarliestStartTime from ServiceAppointment where Work_Order__c =: o.Ordre_d_execution__c order by createdDate]; 
                    q.Devis_etabli_le__c = lstSA.size()>0 && lstSA[0].ArrivalWindowStartTime != null ? lstSA[0].ArrivalWindowStartTime.date() : null;
                    q.Date_de_la_visite_technique__c = lstSA.size()>0 && lstSA[0].EarliestStartTime != null ? lstSA[0].EarliestStartTime.date() : null;
                }

                //Determine Status of quote base on StageName of Opp
                String oldStatus = q.Status;
                q.Status = o.StageName == AP_Constant.oppStageQua ? AP_Constant.quoteStatusDraft : 
                            (o.StageName == AP_Constant.oppStageProposal ? AP_Constant.quoteStatusInReview : 
                            (o.StageName == AP_Constant.oppStageDevisValideEnAttenteInter ? AP_Constant.quoteStatusValideSigne : 
                            (o.StageName == AP_Constant.oppStageClosedWon ? AP_Constant.quoteStatusValideSigneTermine: 
                            (o.StageName == AP_Constant.oppStageClosedLost ? AP_Constant.quoteStatusValideSigneAbandonne : 
                            //New Value in Opp and Quote : Autre option retenue AMO 04.09.20
                            (o.StageName == AP_Constant.oppStageAutreOptionRetenue ? AP_Constant.quoteStatusAutreOptionRetenue :
                            (null))))));
                // if(oldStatus != q.Status){
                    createPDF = true;
                // }
                

                q.Duree_de_validite_du_devis__c = '3 mois';
                q.ExpirationDate = q.Devis_etabli_le__c == null ? null : q.Devis_etabli_le__c.addMonths(3);                
                q.Logement__c = o.Ordre_d_execution__c == null ? null : o.Ordre_d_execution__r.TECH_LogementId__c;
                q.Equipement__c = o.Ordre_d_execution__c == null ? null : o.Ordre_d_execution__r.AssetId;       
                q.Description = o.Description;                       
                q.Pourcentage_de_l_acompte__c = 30;                
                // q.Mode_de_paiement_de_l_acompte__c = //TO check mapping with Sarah - check with Souleymane        
                // q.Mode_de_paiement_du_solde__c =     //TO check mapping with Sarah
                // q.Solde_regle__c =                   //TO check mapping with Sarah
                q.Email = o.Account.IsPersonAccount == true ? o.Account.PersonEmail : o.Account.BusinessAccountEmail__c;
                q.Phone = o.Account.Phone;
                q.Fax = o.Account.Fax;
                q.BillingName = o.Account.Name;
                q.BillingStreet = o.Account.BillingStreet;
                q.BillingCity = o.Account.BillingCity;
                q.BillingCountry = o.Account.BillingCountry;
                q.BillingState = o.Account.BillingState;
                q.Date_de_debut_des_travaux__c = o.TECH_date_prevue_de_debut_des_travaux__c;
                q.Duree_estimee_de_l_intervention__c = string.valueOf(o.TECH_Duree_estimee_des_travaux__c);
                q.Nom_du_payeur__c = q.Nom_du_payeur__c == null? o.AccountId : q.Nom_du_payeur__c;
                q.isSync__c = true;
                q.Agency__c = o.Agency__c;
                //TEC-39
                q.Campagne__c = o.CampaignId;
                q.Counter__c = q.Counter__c == null ? 1 : q.Counter__c + 1;

                if (o.Ordre_d_execution__c != null && o.Ordre_d_execution__r.TECH_LogementId__c != null){
                    list <Logement__c> lstLogement = [SELECT Id, Name, Country__c, City__c, Street__c, Inhabitant__c, Owner__c FROM Logement__c WHERE Id =: o.Ordre_d_execution__r.TECH_LogementId__c];
                    if(lstLogement.size()>0){
                        q.ShippingName = lstLogement[0].Name;
                        q.ShippingStreet = lstLogement[0].Street__c;
                        q.ShippingCity = lstLogement[0].City__c;
                        q.ShippingCountry = lstLogement[0].Country__c;
                        q.Inhabitant__c = lstLogement[0].Inhabitant__c;
                        q.Owner__c = lstLogement[0].Owner__c;
                        // q.ShippingState = 
                    }                   
                }

                if (o.Ordre_d_execution__c != null && o.Ordre_d_execution__r.AssetId != null){
                    List<ServiceContract> lstServCon = [SELECT Id FROM ServiceContract WHERE Asset__c = :o.Ordre_d_execution__r.AssetId AND Contract_Status__c IN ('Pending first visit', 'Actif - en retard de paiement', 'Active')];
                    if(lstServCon.size()>0){
                        q.Contrat_de_service__c = lstServCon[0].Id;
                    }
                }

                
                
                System.debug('###### RRJ signatur: '+q.Signature_client__c);
                mapOpptoQs.put(o.ID, q);
                lstQsToInsert.add(q);
                
            }
        }
        
        if(lstQsToInsert.size() >0){
            upsert lstQsToInsert;
        }
        
        List<QuoteLineItem> lstQliToDelete = [SELECT Id FROM QuoteLineItem WHERE QuoteId IN :setQuoId];
        if(lstQliToDelete.size()>0){
            delete lstQliToDelete;
        }

        for(Quote q : lstQsToInsert){
            if(q.Signature_Client__c != null){
                String imgClientBase64 = q.Signature_Client__c;

                if(imgClientBase64.startsWith('data:image/jpeg;base64,')) {
                    imgClientBase64 = imgClientBase64.removeStart('data:image/jpeg;base64,');
                }
                lstImgAttachment.add(
                    new Attachment (
                        ParentId = q.Id, 
                        ContentType = 'image/png',
                        body = EncodingUtil.base64Decode(imgClientBase64),
                        Name = 'SignatureClient.png'
                    )
                );
            }
        }

        if(lstImgAttachment.size() > 0) {
            insert lstImgAttachment;
        }

        System.debug('lstQsToInsert.size(): '+ lstQsToInsert.size());

        map<String,String> mapCSCreateQLI = LC08_SyncQuote.mapCSCreateQLI();
        System.debug('mapOpptoOLIs size: '+ mapOpptoOLIs.size());
        Map<Id, QuoteLineItem> mapOliToQli = new Map<Id, QuoteLineItem>();
        List<QuoteLineItem> lstQliPRLI = new List<QuoteLineItem>();
        for(ID oppId: mapOpptoOLIs.keyset() ){            

            for(OpportunityLineItem oli: mapOpptoOLIs.get(oppId)){

                QuoteLineItem qli = new QuoteLineItem();
                qli.QuoteId = mapOpptoQs.get(oppId).ID;
                qli.PriceBookentryId = oli.PricebookEntryId;
                qli.Taux_de_TVA__c = oli.sofactoapp__VAT_rate__c;
                qli.TECH_Counter__c = 1; // RRJ CT-1317

                // Dynamic mapping of fields with customSetting Syncing QLI
                for(String fld: mapCSCreateQLI.keySet()){
                    qli.put(fld, LC08_SyncQuote.getFld(oli, mapCSCreateQLI.get(fld)));
                }
                mapOliToQli.put(oli.id, qli);
                lstQlisToInsert.add(qli);
                if( mapOpp.get(oppId).APP_intervention_immediate__c == true){
                    setQuoIntervImm.add(mapOpptoQs.get(oppId));
                } else if(mapOpptoQs.get(oppId).Status == 'Validé, signé - en attente d\'intervention' ){
                    mapQuoValideSigne.put(mapOpptoQs.get(oppId).Id, mapOpptoQs.get(oppId));
                    mapQuoAgency.put(mapOpptoQs.get(oppId).Id, mapOpptoQs.get(oppId).Agency__c);
                }
                if(mapOpptoQs.get(oppId).Status == 'Validé, signé - en attente d\'intervention'){
                    lstQliPRLI.add(qli);
                }
                
            }
        }
            
        System.debug('lstQlisToInsert.size(): '+ mapOliToQli.values().size());
        if(mapOliToQli.values().size() >0 ){
            insert mapOliToQli.values();
        }
        if(lstQliPRLI.size()>0){
            AP30_QuoteProductManagement.createProductRequiredLineItem(lstQliPRLI);
        }
        if(mapQuoValideSigne.size()>0){
            AP36_QuoteProductTransfers.createProductRequests(mapQuoValideSigne.values(), mapQuoAgency);
        }
        if(setQuoIntervImm.size()>0){
            deleteProductRequests(setQuoIntervImm);
        }
        // System.debug('#### mapOliToQli.values(): '+mapOliToQli.values());

        if(createPDF){
            generatePDFDevis(lstQsToInsert);
        }
    }
    
    public static void generatePDFDevis(List<Quote> lstQte){
        List<Id> lstQteToGeneratePDF = new List<Id>();
        for(Quote qte: lstQte){
            if(qte.Status == AP_Constant.quoteStatusValideSigne){
                lstQteToGeneratePDF.add(qte.Id);
            }
        }
        if(lstQteToGeneratePDF.size()>0){
            AP64_GenerateDevisPdf.generatePdfDevis(lstQteToGeneratePDF);
        }
    }

    private static void deleteProductRequests(Set<Quote> setQte){
        Map<Id, ProductRequest> mapPrToDelete = new Map<Id, ProductRequest>();
        for(ProductRequestLineItem prli : [SELECT Id, ParentId, Quote__c FROM ProductRequestLineItem WHERE Quote__c IN :setQte]){
            mapPrToDelete.put(prli.ParentId, new ProductRequest(
                Id = prli.ParentId
            ));
        }

        if(mapPrToDelete.size()>0){
            delete mapPrToDelete.values();
        }
    }

    public static void setCampagne(list<Opportunity> lstOpp){
        for(Opportunity opp : lstOpp){
            opp.CampaignId = opp.TECH_AccountCampagne__c;
        }
    }
}