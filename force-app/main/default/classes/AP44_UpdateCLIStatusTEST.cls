/**
 * @File Name          : AP44_UpdateCLIStatusTEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-09-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    14/11/2019   AMO     Initial Version
**/
@isTest
public with sharing class AP44_UpdateCLIStatusTEST {
    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<Logement__c> lstTestLog = new List<Logement__c>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<Bundle__c> lstTestBundle = new List<Bundle__c>();
    static List<Product_Bundle__c> lstTestBundleProd = new List<Product_Bundle__c>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();
    static List<ContractLineItem> lstConLnItem1 = new List<ContractLineItem>();
    //bypass
    static Bypass__c bp = new Bypass__c();


    static {
        adminUser = TestFactory.createAdminUser('AP09_BundleContract_TEST@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;

        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(adminUser){
            Integer parentNum = 1;
            Integer childNum = 5;
            //create accounts
            for(Integer i=0; i<parentNum; i++){
                lstTestAcc.add(TestFactory.createAccount('testAcc'+1));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
                lstTestAcc[i].BillingPostalCode = '1234'+i;
            }

            insert lstTestAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
			lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update lstTestAcc;

            //create products
            for(Integer i=0; i<parentNum*childNum; i++){
                lstTestProd.add(TestFactory.createProduct('testProd'+i));
                if(Math.mod(i, 5)== 0){
                    lstTestProd[i].IsBundle__c = true;
                    lstTestProd[i].Name = 'testBundleProd'+i/5;
                }
            }

            insert lstTestProd;

            //create bundles
            for(Integer i=0; i<parentNum; i++){
                lstTestBundle.add(TestFactory.createBundle('testBundle'+i, 150));
            }

            insert lstTestBundle;

            //create bundle products related to bundle and product
            for(Integer i=0; i<parentNum*childNum; i++){
                lstTestBundleProd.add(TestFactory.createBundleProduct(lstTestBundle[i/5].Id, lstTestProd[i].Id));
            }

            insert lstTestBundleProd;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;


            //pricebook entry
            for(Integer i=0; i<parentNum*childNum; i++){
                lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[i].Id, 0));
                if(lstTestProd[i].IsBundle__c){
                    lstPrcBkEnt[i].UnitPrice = 150;
                }
            }

            insert lstPrcBkEnt;

            //service contract
            for(Integer i=0; i<parentNum; i++){
                lstServCon.add(TestFactory.createServiceContract('testServCon'+i, lstTestAcc[i].Id));
                lstServCon[i].PriceBook2Id = lstPrcBk[0].Id;
                lstServCon[i].Contract_Status__c = 'Cancelled';
                lstServCon[i].Type__c = 'Individual';
            }

            insert lstServCon;
            // System.debug('##### TEST lstServCon: '+lstServCon);
            
            //contract line item
            for(Integer i=0; i<parentNum*childNum; i++){
                lstConLnItem.add(TestFactory.createContractLineItemSuspension(lstServCon[i/childNum].Id, lstPrcBkEnt[i].Id, System.today(), System.today()+1,  150, 1));
            }

            lstConLnItem[0].ServiceContractId = lstServCon[0].Id;
            lstConLnItem[0].VE_Max_Date__c = Date.newInstance(2016, 12, 9);
            lstConLnItem[0].VE_Min_Date__c = Date.newInstance(2016, 12, 9);
            lstConLnItem[0].Debut_de_la_suspension__c  = System.today();
            lstConLnItem[0].Fin_de_la_suspension__c  = System.today() + 1;

            insert lstConLnItem;

             //contract line item suspension
             for(Integer i=0; i<parentNum*childNum; i++){
                lstConLnItem1.add(TestFactory.createContractLineItemSuspension(lstServCon[i/childNum].Id, lstPrcBkEnt[i].Id, System.today()+1, System.today()+2, 150, 1));
            }

            lstConLnItem1[0].ServiceContractId = lstServCon[0].Id;
            lstConLnItem1[0].VE_Max_Date__c = Date.newInstance(2016, 12, 9);
            lstConLnItem1[0].VE_Min_Date__c = Date.newInstance(2016, 12, 9);
           // lstConLnItem[0].Debut_de_la_suspension__c  = System.today();
            //lstConLnItem[0].Fin_de_la_suspension__c  = System.today() + 1;

            insert lstConLnItem1;
        }
    }

    @isTest
    static void testUpdateServiceCOntract(){
        System.runAs(adminUser){
            
            lstServCon[0].Contract_Status__c = 'Résilié';

            Test.startTest();
                Update lstServCon[0];
            Test.stopTest();

            ContractLineItem cli = [SELECT Id, Debut_de_la_suspension__c, Fin_de_la_suspension__c, IsActive__c, ServiceContractId FROM ContractLineItem WHERE Id = :lstConLnItem[0].Id];
            System.assertEquals(cli.IsActive__c, False);
        }
    }

    @isTest
    static void testUpdateServiceCOntractActive(){
        System.runAs(adminUser){    
            lstServCon[0].Contract_Status__c = 'Résilié';

            Test.startTest();
                //Update lstServCon[0];
                AP44_UpdateCLIStatus.UpdateStatusActive (lstServCon);
            Test.stopTest();

            ContractLineItem cli = [SELECT Id, Debut_de_la_suspension__c, Fin_de_la_suspension__c, IsActive__c, ServiceContractId FROM ContractLineItem WHERE Id = :lstConLnItem1[0].Id];
            System.debug('cli = ' + cli);
            System.assertEquals(cli.IsActive__c, true);
        }
    }


}