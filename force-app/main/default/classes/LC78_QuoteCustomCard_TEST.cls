/**
 * @File Name         : LC78_QuoteCustomCard_TEST.cls
 * @Description       : 
 * @Author            : Spoon Consulting (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 17-06-2021
 * Modifications Log 
 * ==============================================================
 * Ver   Date               Author              Modification
 * 1.0   17-06-2021         MNA                 Initial Version
**/
@isTest
public with sharing class LC78_QuoteCustomCard_TEST {
    static User mainUser;
    static Account testAcc;
    static List<Opportunity> testLstOpp;
    static List<Quote> testLstQuo;
    static{
        mainUser = TestFactory.createAdminUser('LC78@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser) {
            testAcc = TestFactory.createAccount('LC78_QuoteCustomCard_TEST');
            insert testAcc;

            testLstOpp =  new List<Opportunity>{ 
                TestFactory.createOpportunity('Test1',testAcc.Id)
            };
            insert testLstOpp;

            testLstQuo =  new List<Quote> {
                new Quote (Name = 'Prestation 1', Nom_du_payeur__c = testAcc.Id, Status = 'In Review', Devis_etabli_le__c = System.today(), OpportunityId = testLstOpp[0].Id),
                new Quote (Name = 'Prestation 2', Owner__c = testAcc.Id, Status = 'Needs Review', Devis_etabli_le__c = System.today(), OpportunityId = testLstOpp[0].Id),
                new Quote (Name = 'Prestation 3', Nom_du_payeur__c = testAcc.Id, Owner__c = testAcc.Id, Inhabitant__c = testAcc.Id, Status = 'Draft',
                                    Devis_etabli_le__c = System.today(), OpportunityId = testLstOpp[0].Id)
            };
            insert testLstQuo;
        }
    }

    @isTest
    static void testgetData() {
        Test.startTest();
        try {
            LC78_QuoteCustomCard.getData(testAcc.Id);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        Test.stopTest();
    }

    @isTest
    static void testdelQuote() {
        Test.startTest();
        String Name = LC78_QuoteCustomCard.delQuote(testLstQuo[0].Id);
        Test.stopTest();
        Integer size = [SELECT id FROM Quote].size();

        System.assertEquals('Prestation 1', Name);
        System.assertEquals(2, size);
    }
    
    @isTest
    static void teststatusPicklistValue() {
        Test.startTest();
        LC78_QuoteCustomCard.statusPicklistValue();
        Test.stopTest();
    }
}