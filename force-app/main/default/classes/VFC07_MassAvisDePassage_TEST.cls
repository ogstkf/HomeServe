/**
 * @File Name          : VFC07_MassAvisDePassage_TEST.cls
 * @Description        : Test class for VFC07_MassAvisDePassage
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 09-09-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    27/08/2019, 13:21:57   RRJ     Initial Version
**/
@IsTest
public with sharing class VFC07_MassAvisDePassage_TEST {
    static User adminUser;
    static Account testAcc;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static OperatingHours opHrs = new OperatingHours();
   // static ServiceTerritory srvTerr = new ServiceTerritory();
    static List<ServiceTerritory> lstSrvTerr = new List<ServiceTerritory>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Case> lstCase = new List<Case>();
    static List<Product2> lstProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<ContentWorkspace> lstContentWs = new List<ContentWorkspace>();
    static sofactoapp__Raison_Sociale__c raisonSocial;

    static {
        adminUser = TestFactory.createAdminUser('AP12_ServiceAppointmentRules_TEST@test.COM', TestFactory.getProfileAdminId());
		insert adminUser;

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('AP12_ServiceAppointmentRules_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon[0].Type__c = 'Individual';
            lstServCon.add(TestFactory.createServiceContract('test serv con 2', testAcc.Id));
            lstServCon[1].Type__c = 'Individual';

            insert lstServCon;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            //create service territory
            lstSrvTerr.add(TestFactory.createServiceTerritory('SBF Energies1',opHrs.Id, raisonSocial.Id));
            lstSrvTerr.add(TestFactory.createServiceTerritory('ID Energies1',opHrs.Id, raisonSocial.Id));
            lstSrvTerr.add(TestFactory.createServiceTerritory('Electrogaz1',opHrs.Id, raisonSocial.Id));
            insert lstSrvTerr;


            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[0].Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[1].Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[2].Id));
            lstLogement[2].Postal_Code__c = 'lgmtPC 3';
            lstLogement[2].City__c = 'lgmtCity 3';
            lstLogement[2].Account__c = testAcc.Id;
            insert lstLogement;

            //creating products
            lstProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstProd.add(TestFactory.createProduct('Entretien gaz'));
            
            insert lstProd;

            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstProd[0].Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstLogement[1].Id));
            lstAsset[1].Product2Id = lstProd[0].Id;
            lstAsset[1].Logement__c = lstLogement[1].Id;
            lstAsset[1].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 3', AP_Constant.assetStatusActif, lstLogement[2].Id));
            lstAsset[2].Product2Id = lstProd[0].Id;
            lstAsset[2].Logement__c = lstLogement[2].Id;
            lstAsset[2].AccountId = testAcc.Id;
            insert lstAsset;

            // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[0].Id));
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[1].Id));
            lstCase[1].Service_Contract__c = lstServCon[1].Id;
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[2].Id));
            lstCase[2].Service_Contract__c = lstServCon[1].Id;
            insert lstCase;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Commissioning';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType2', 'Hours', 1));
            lstWrkTyp[1].Type__c = 'Information Request';
            lstWrkTyp[1].Reason__c = 'Commercial Visit';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType3', 'Hours', 1));
            lstWrkTyp[2].Type__c = 'Information Request';
            lstWrkTyp[2].Reason__c = 'Commercial Visit';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType4', 'Hours', 1));
            lstWrkTyp[3].Type__c = 'Installation';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType5', 'Hours', 1));
            lstWrkTyp[4].Type__c = 'Maintenance';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType6', 'Hours', 1));
            lstWrkTyp[5].Type__c = 'Maintenance';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType7', 'Hours', 1));
            lstWrkTyp[6].Type__c = 'Troubleshooting';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType8', 'Hours', 1));
            lstWrkTyp[7].Type__c = 'Various Services';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType8', 'Hours', 1));
            lstWrkTyp[8].Type__c = 'Various Services';

            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[1].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[2].WorkTypeId = lstWrkTyp[2].Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[3].WorkTypeId = lstWrkTyp[3].Id;

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[4].WorkTypeId = lstWrkTyp[4].Id;
            lstWrkOrd[4].CaseId = lstCase[0].Id;

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[5].WorkTypeId = lstWrkTyp[5].Id;
            lstWrkOrd[5].CaseId = lstCase[1].Id;

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[6].WorkTypeId = lstWrkTyp[6].Id;
            lstWrkOrd[6].CaseId = lstCase[2].Id;

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[7].WorkTypeId = lstWrkTyp[7].Id;

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[8].WorkTypeId = lstWrkTyp[8].Id;
            insert lstWrkOrd;

            // creating service appointment
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[0].Id, lstSrvTerr[0].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[1].Id, lstSrvTerr[1].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[2].Id, lstSrvTerr[2].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[3].Id, lstSrvTerr[0].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[4].Id, lstSrvTerr[1].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[5].Id, lstSrvTerr[2].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[6].Id, lstSrvTerr[0].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[7].Id, lstSrvTerr[1].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[8].Id, lstSrvTerr[2].Id));          
            insert lstServiceApp;

            System.debug('lstServiceApp ' + lstServiceApp);


            // lstContentWs.add(TestFactory.createContentWorkSpace('Avis De Passage'));
            lstContentWs.add(TestFactory.createContentWorkSpace('Avis de passage SBF Energies1'));
            // lstContentWs.add(TestFactory.createContentWorkSpace('Avis De Passage ID Energies1'));
            // lstContentWs.add(TestFactory.createContentWorkSpace('Avis De Passage Electrogaz1'));
            insert lstContentWs;

            System.debug('lstContentWs ' + lstContentWs);
        }
    }

    @IsTest
    public static void selectedListTest(){
        System.runAs(adminUser){
			
			Test.startTest();

				List<ServiceAppointment> lstServiceAppTest = new List<ServiceAppointment>([SELECT id 
															  FROM ServiceAppointment
															  WHERE id in: lstServiceApp]);
				System.debug('mgr lstServiceAppTest ' + lstServiceAppTest);

				Test.setCurrentPage(Page.VFP07_MassAvisDePassage);
                ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstServiceAppTest);
                stdSetController.setSelected(lstServiceAppTest); 

                VFC07_MassAvisDePassage ext = new VFC07_MassAvisDePassage(stdSetController);
	        Test.stopTest();

		}
    }

    @IsTest
    public static void generatePdfTest(){
        System.runAs(adminUser){
            List<String> lstSAId = new List<String>();
            for(ServiceAppointment srvApp: lstServiceApp){
                lstSAId.add(srvApp.Id);
            }
            System.debug('##### lstSAId: '+lstSAId);
			Test.startTest();
            String response = VFC07_MassAvisDePassage.openPDF(lstSAId);
            System.debug('##### response: '+response);

            Test.stopTest();
        }
    }
}