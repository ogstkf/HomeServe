/**
 * @File Name          : AP12_ServiceAppointmentRules_TEST.cls
 * @Description        : test class for AP12_ServiceAppointmentRules
 * @Author             : AMO
 * @Group              : Spoon Consulting
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 04-11-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    22/08/2019, 16:27:46   RRJ     Initial Version
**/
@isTest
public class AP12_ServiceAppointmentRules_TEST {

    static User adminUser;
    static Account testAcc;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static OperatingHours opHrs = new OperatingHours();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Case> lstCase = new List<Case>();
    static Product2 lstProd = new Product2();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static list<ServiceTerritory> lstServTerritory;
    static Bypass__c bp = new Bypass__c();
    static ServiceAppointment sa1;

    static {
        /*adminUser = TestFactory.createAdminUser('AP12_ServiceAppointmentRules_TEST@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;*/
        adminUser = [SELECT ID FROM User WHERE Name = 'Dhovisha Munbodh'];

        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassValidationRules__c = True;
        bp.BypassTrigger__c = 'AP16,WorkOrderTrigger';
        insert bp;

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('AP12_ServiceAppointmentRules_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.AccountSource = 'Site Cham Digital';
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon[0].Type__c = 'Collective';
            lstServCon[0].RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId();
            lstServCon[0].Contract_Renewed__c = false;
            lstServCon[0].Contrat_Cham_Digital__c = false;
            
            lstServCon.add(TestFactory.createServiceContract('test serv con 2', testAcc.Id));
            lstServCon[1].Type__c = 'Individual';
            lstServCon[1].RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId();
            lstServCon[1].Contract_Renewed__c = false;
            lstServCon[1].Contrat_Cham_Digital__c = false;
            insert lstServCon;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            lstServTerritory = new list<ServiceTerritory>{
                TestFactory.createServiceTerritory('SGS - test Territory', opHrs.Id, raisonSocial.Id)
            };
            insert lstServTerritory;


            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstServTerritory[0].Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstServTerritory[0].Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc.Id;
            insert lstLogement;

            //creating products
            Product2 lstProd = new Product2(Name = 'Product X',
                                        ProductCode = 'Pro-X',
                                        isActive = true,
                                        Equipment_family__c = 'Chaudière',
                                        Equipment_type__c = '',
                                        Statut__c = 'Approuvée'
            );
            insert lstProd;


            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstProd.Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstLogement[1].Id));
            lstAsset[1].Product2Id = lstProd.Id;
            lstAsset[1].Logement__c = lstLogement[1].Id;
            lstAsset[1].AccountId = testAcc.Id;
            insert lstAsset;

            // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'Maintenance', lstAsset[0].Id));
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase[0].Origin = 'Cham Digital';
            lstCase[0].Reason__c = 'Visite sous contrat';
            lstCase[0].RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            lstCase.add(TestFactory.createCase(testAcc.Id, 'Maintenance', lstAsset[1].Id));
            lstCase[1].Service_Contract__c = lstServCon[1].Id;
            lstCase[1].Origin = 'Cham Digital';
            lstCase[1].Reason__c = 'Visite sous contrat';
            lstCase[1].RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            insert lstCase;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('Commissioning', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Commissioning';
            lstWrkTyp[0].Type_de_client__c = 'Tous';
            lstWrkTyp[0].Agence__c = 'Toutes';
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[1].Type__c = 'Maintenance';
            lstWrkTyp[1].Type_de_client__c = 'Tous';
            lstWrkTyp[1].Agence__c = 'Toutes';
            // lstWrkTyp[1].Reason__c = 'Flat-rate visit';
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[2].Type__c = 'Maintenance';
            lstWrkTyp[2].Type_de_client__c = 'Tous';
            lstWrkTyp[2].Agence__c = 'Toutes';
            // lstWrkTyp[2].Reason__c = 'First maintenance';
            lstWrkTyp.add(TestFactory.createWorkType('Installation', 'Hours', 1));
            lstWrkTyp[3].Type__c = 'Installation';
            lstWrkTyp[3].Type_de_client__c = 'Tous';
            lstWrkTyp[3].Agence__c = 'Toutes';
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[4].Type__c = 'Maintenance';
            lstWrkTyp[4].Type_de_client__c = 'Tous';
            lstWrkTyp[4].Agence__c = 'Toutes';
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[5].Type__c = 'Maintenance';
            lstWrkTyp[5].Type_de_client__c = 'Tous';
            lstWrkTyp[5].Agence__c = 'Toutes';
            lstWrkTyp.add(TestFactory.createWorkType('Troubleshooting', 'Hours', 1));
            lstWrkTyp[6].Type__c = 'Troubleshooting';
            lstWrkTyp[6].Type_de_client__c = 'Tous';
            lstWrkTyp[6].Agence__c = 'Toutes';
            lstWrkTyp.add(TestFactory.createWorkType('Various Services', 'Hours', 1));
            lstWrkTyp[7].Type__c = 'Various Services';
            lstWrkTyp[7].Type_de_client__c = 'Tous';
            lstWrkTyp[7].Agence__c = 'Toutes';

            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].caseId = lstCase[0].Id;
            lstWrkOrd[0].AssetId = lstAsset[0].Id;
            lstWrkOrd[0].Reason__c = '';
            lstWrkOrd[0].Clientnoncontacte__c = true;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[1].Id;
            lstWrkOrd[1].caseId = lstCase[0].Id;
            lstWrkOrd[1].AssetId = lstAsset[0].Id;
            lstWrkOrd[1].Reason__c = '';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[2].WorkTypeId = lstWrkTyp[2].Id;
            lstWrkOrd[2].caseId = lstCase[0].Id;
            lstWrkOrd[2].AssetId = lstAsset[0].Id;
            lstWrkOrd[2].Reason__c = '';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[3].WorkTypeId = lstWrkTyp[3].Id;
            lstWrkOrd[3].caseId = lstCase[0].Id;
            lstWrkOrd[3].AssetId = lstAsset[0].Id;
            lstWrkOrd[3].Reason__c = '';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[4].WorkTypeId = lstWrkTyp[4].Id;
            lstWrkOrd[4].CaseId = lstCase[0].Id;
            lstWrkOrd[4].AssetId = lstAsset[0].Id;
            lstWrkOrd[4].Reason__c = '';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[5].WorkTypeId = lstWrkTyp[5].Id;
            lstWrkOrd[5].CaseId = lstCase[1].Id;
            lstWrkOrd[5].AssetId = lstAsset[0].Id;
            lstWrkOrd[5].Reason__c = '';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[6].WorkTypeId = lstWrkTyp[6].Id;
            lstWrkOrd[6].caseId = lstCase[0].Id;
            lstWrkOrd[6].AssetId = lstAsset[0].Id;
            lstWrkOrd[6].Reason__c = '';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[7].WorkTypeId = lstWrkTyp[7].Id;
            lstWrkOrd[7].caseId = lstCase[0].Id;
            lstWrkOrd[7].AssetId = lstAsset[0].Id;
            lstWrkOrd[7].Reason__c = '';
            insert lstWrkOrd;
            
            // creating service appointment            
            sa1 = TestFactory.createServiceAppointment(lstWrkOrd[0].Id);
            sa1.TECH_Cham_Digital__c = false;
            sa1.Work_Order__c = null;
            sa1.Clientnoncontacte__c = true;
            insert sa1;            

            for(integer i=0; i<8; i++){
                ServiceAppointment sa = TestFactory.createServiceAppointment(lstWrkOrd[i].Id);
                sa.Category__c = 'VE Individuelle';
                sa.Status = 'None';                
                sa.Service_Contract__c = lstServCon[1].Id;
                lstServiceApp.add(sa);
            }          

        }
    }    
    
    @isTest
    public static void testBeforeUpd1(){
        System.runAs(adminUser){
            sa1.Work_Order__c = lstWrkOrd[0].Id;
            Test.startTest();
            update sa1;            
            Test.stopTest();
        }
    }

    @isTest
    public static void testInsert(){
        System.runAs(adminUser){
            lstServiceApp[0].Work_Order__c = lstWrkOrd[0].Id;
            lstServiceApp[1].Work_Order__c = lstWrkOrd[1].Id;
            lstServiceApp[2].Work_Order__c = lstWrkOrd[2].Id;
            lstServiceApp[3].Work_Order__c = lstWrkOrd[3].Id;
            lstServiceApp[4].Work_Order__c = lstWrkOrd[4].Id;
            lstServiceApp[5].Work_Order__c = lstWrkOrd[5].Id;
            lstServiceApp[6].Work_Order__c = lstWrkOrd[6].Id;
            lstServiceApp[7].Work_Order__c = lstWrkOrd[7].Id;

            lstServiceApp[0].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[1].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[2].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[3].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[4].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[5].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[6].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[7].Service_Contract__c = lstServCon[1].Id;
            Test.startTest();
            insert lstServiceApp;            
            Test.stopTest();
        }
    }

    @isTest
    public static void testUpdate(){
        System.runAs(adminUser){
            lstServiceApp[0].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[1].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[2].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[3].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[4].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[5].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[6].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[7].Service_Contract__c = lstServCon[1].Id;
            insert lstServiceApp;

            lstServiceApp[0].Work_Order__c = lstWrkOrd[0].Id;
            lstServiceApp[1].Work_Order__c = lstWrkOrd[1].Id;
            lstServiceApp[2].Work_Order__c = lstWrkOrd[2].Id;
            lstServiceApp[3].Work_Order__c = lstWrkOrd[3].Id;
            lstServiceApp[4].Work_Order__c = lstWrkOrd[4].Id;
            lstServiceApp[5].Work_Order__c = lstWrkOrd[5].Id;
            lstServiceApp[6].Work_Order__c = lstWrkOrd[6].Id;
            lstServiceApp[7].Work_Order__c = lstWrkOrd[7].Id;
            Test.startTest();
            update lstWrkOrd;
            Test.stopTest();
        }
    }

    @isTest
    public static void testupdateCLISA(){
        System.runAs(adminUser){
            lstServiceApp[0].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[1].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[2].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[3].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[4].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[5].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[6].Service_Contract__c = lstServCon[1].Id;
            lstServiceApp[7].Service_Contract__c = lstServCon[1].Id;
            insert lstServiceApp;

            Test.startTest();
            // AP12_ServiceAppointmentRules.updateCLISA(lstServiceApp);
            Test.stopTest();
        }
    }

    
    @isTest
    static void testUpdateWorktype() {
        System.runAs(adminUser) {
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[7].Id;
            Test.startTest();
            update lstWrkOrd[0];
            Test.stopTest();
        }
    }
}