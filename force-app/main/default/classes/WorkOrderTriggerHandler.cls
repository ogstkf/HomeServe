/**
 * @File Name          : WorkOrderTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 23-11-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    16/10/2019   AMO     Initial Version
 * 1.1    03/06/2020   DMU     Commented Code related to AP58_WoResourceManagement due to HomeServe Project
**/
public with sharing class WorkOrderTriggerHandler {
        
    Bypass__c userBypass; 

    public WorkOrderTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void handlebeforeInsert(List<WorkOrder> lstNew){
        List<WorkOrder> lstWOgenerateFrmMainPlan = new List<WorkOrder>();
        map<string,string> mapWOIdSCId = new map<string,string>();

        for(Integer i = 0; i < lstNew.size(); i++){
            if(lstNew[i].IsGeneratedFromMaintenancePlan && lstNew[i].MaintenancePlanId <> null){
                lstWOgenerateFrmMainPlan.add(lstNew[i]);
                mapWOIdSCId.put(lstNew[i].Id, lstNew[i].ServiceContractId);
            }
        }

        if(lstWOgenerateFrmMainPlan.size()>0){
            AP75_UpdateWorkOrdersFromMaintenancePlan.updWOGenerateFromMaintenancePlan(lstWOgenerateFrmMainPlan, mapWOIdSCId);
        } 
    }

    public void handleAfterInsert(List<WorkOrder> lstNew){
        
        List<WorkOrder> lstWOToUpdateCompletionDate = new List<WorkOrder>(); 
        List<WorkOrder> lstWOToUpdateClientAbsent = new List<WorkOrder>();
        Set<Id> setCliToUpdateCompletionDate = new set<Id>();
        Set<Id> setCliToUpdateAbsenceClient = new Set<Id>();
        Set<Id> setWOIdPopulateClientNonContact = new Set<Id>();
        List<WorkOrder> lstWOCLIVEStatus = new List<WorkOrder>(); //AP66

        //DMU - 3/6/20 - commented below variable due to HomeServe
        // List<WorkOrder> lstWOAgencySubSector = new List<WorkOrder>(); //AP60

        //SBH: FSL subAgency Resource preference
        //Map Id workorder to agency
        //DMU - 3/6/20 - commented below 2 variables due to HomeServe
        // Map<Id, Id> mapWoToSubAgency = new Map<Id, Id>();  
        // List<WorkOrder> lstWoToCreateResourcePref = new List<WorkOrder>();  

        //SBH: LC11 - CT-1409 
        List<Id> lstWorkOrdersToUpdateQuotes = new List<Id>();
        Map<Id,Id> mapQuoteToNewWo = new Map<Id,Id>();
        //END SBH:

        Map<Id, Id> mapCli = new Map<Id, Id>();

        for(Integer i = 0; i < lstNew.size(); i++){
            
            if(lstNew[i].Completion_date__c != null  
               && lstNew[i].TECH_CaseContractLineItem__c != null
            ){
                system.debug('*** first cond');
                lstWOToUpdateCompletionDate.add(lstNew[i]);
                setCliToUpdateCompletionDate.add(lstNew[i].TECH_CaseContractLineItem__c);
            }

            if(
                lstNew[i].Customer_Absences__c != null
                && lstNew[i].Customer_Absences__c == 1
                && lstNew[i].TECH_CaseContractLineItem__c != null
            ){
                system.debug('*** second cond');
                lstWOToUpdateClientAbsent.add(lstNew[i]);
                setCliToUpdateAbsenceClient.add(lstNew[i].TECH_CaseContractLineItem__c);
            }   
            
            //AP66 Update CLI VE_Status
            if(
                lstNew[i].Type__c == 'Maintenance' && lstNew[i].Reason__c == 'Visite sous contrat' 
                  && lstNew[i].Status == 'New' && lstNew[i].TECH_CaseContractLineItem__c != null
            ) {
                lstWOCLIVEStatus.add(lstNew[i]);
            }
            
            if (lstNew[i].Clientnoncontacte__c) {
                setWOIdPopulateClientNonContact.add(lstNew[i].Id);
            }

            //SBH : FSL Subagency resource preference
            //DMU - 3/6/20 - commented the if condition due to HomeServe
            // if(lstNew[i].AgencySubSector__c != null){
            //     mapWoToSubAgency.put(lstNew[i].Id, lstNew[i].AgencySubSector__c);
            //     lstWoToCreateResourcePref.add(lstNew[i]);

            //     //DMU 20200116 - Added logic to set agency Sub Sector on SA
            //     lstWOAgencySubSector.add(lstNew[i]);
            // }
            
            

            //SBH : CT-1409
            if(lstNew[i].Quote__c != null){
                lstWorkOrdersToUpdateQuotes.add(lstNew[i].Id);
                mapQuoteToNewWo.put(lstNew[i].Quote__c, lstNew[i].Id);
            }            
        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP57')){
            if(!lstWOToUpdateCompletionDate.isEmpty()){
                AP57_CompletionDateOnCli.updateCliCompletedStatus(lstWOToUpdateCompletionDate, setCliToUpdateCompletionDate);
            }

            if(!lstWOToUpdateCompletionDate.isEmpty()){
                AP57_CompletionDateOnCli.updateCustomerAbsence(lstWOToUpdateClientAbsent, setCliToUpdateAbsenceClient);
            }
        } 

        //DMU - 3/6/20 - commented the if condition due to HomeServe
        // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP58')){
        //     AP58_WoResourceManagement.createResourcePreference(mapWoToSubAgency, lstWoToCreateResourcePref);
        // }     

        //DMU - 3/6/20 - commented the if condition due to HomeServe
        // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP60')){
        //     if(lstWOAgencySubSector.size()>0){
        //         AP60_SetAgencySubSector.setAgencySubSectorSA(lstWOAgencySubSector);
        //     }
        // }  
        
        // AP66 Update CLI VE_Status
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP66')){
            if(!lstWOCLIVEStatus.isEmpty()){
                AP66_SA_UpdateCLIStatus.updateWOCLIVEStatus(lstWOCLIVEStatus);
            }  
        }       
                
        // SBH : CT-1409
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP68')){
            if(!lstWorkOrdersToUpdateQuotes.isEmpty()){
                AP68_updateWorkOrderQuotes.updateWorkOrderQuote(lstWorkOrdersToUpdateQuotes, mapQuoteToNewWo);
            }   
        } 
        
        if (userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP12')) {
            if (!setWOIdPopulateClientNonContact.isEmpty())
                AP12_ServiceAppointmentRules.populateClientNonContact(setWOIdPopulateClientNonContact);
        }
    }

    public void handleAfterUpdate(List<WorkOrder> lstOld, List<WorkOrder> lstNew){

        List<WorkOrder> lstWOToUpdateCompletionDate = new List<WorkOrder>(); 
        List<WorkOrder> lstWOToUpdateClientAbsent = new List<WorkOrder>();
        Set<Id> setCliToUpdateCompletionDate = new set<Id>();
        Set<Id> setCliToUpdateAbsenceClient = new Set<Id>();
        Set<Id> setWoIdToPopulateCategory = new Set<Id>();
        List<WorkOrder> lstWOCLIVEStatus = new List<WorkOrder>(); //AP66

        //DMU - 3/6/20 - commented below variable due to HomeServe
        // List<WorkOrder> lstWOAgencySubSector = new List<WorkOrder>(); //AP60   

        //AP58 variables init: 
        //DMU - 3/6/20 - commented AP58_WoResourceManagement 4 below variables due to HomeServe
        // List<String> lstWoIdToAgencyOld = new List<String>();
        // List<String> lstWoIdToAgencyNew = new List<String>();
        // Map<Id, Id> mapWoToSubAgency = new Map<Id, Id>();
        // List<WorkOrder> lstWoToCreateResourcePref = new List<WorkOrder>();

        Map<Id, Id> mapCli = new Map<Id, Id>();

        for(Integer i = 0; i < lstOld.size(); i++){

            system.debug('*** in wohandler call AP57 lstNew[i].TECH_CaseContractLineItem__c: '+lstNew[i].TECH_CaseContractLineItem__c);
            //system.debug('*** in wohandler call AP57 lstOld[i].TECH_WOCaseCLITarget__c: '+lstOld[i].TECH_WOCaseCLITarget__c);
            system.debug('*** in wohandler call AP57 lstOld[i].Customer_Absences__c: '+lstOld[i].Customer_Absences__c);
            system.debug('*** in wohandler call AP57 lstNew[i].Customer_Absences__c: '+lstNew[i].Customer_Absences__c);
            
            system.debug('*** in wohandler call AP57 lstOld[i].WorkTypeId: '+lstOld[i].WorkTypeId);
            system.debug('*** in wohandler call AP57 lstNew[i].WorkTypeId: '+lstNew[i].WorkTypeId);
            
            if(
               lstNew[i].Completion_date__c != null  
               && (lstOld[i].Completion_date__c != lstNew[i].Completion_date__c)
               && lstNew[i].TECH_CaseContractLineItem__c != null 
            //    && lstOld[i].TECH_WOCaseCLITarget__c == lstNew[i].TECH_WOCaseCLICounter__c
            //    && lstOld[i].TECH_WOCaseCLITarget__c != lstNew[i].TECH_WOCaseCLITarget__c
            ){
                system.debug('*** in wohandler call AP57');
                lstWOToUpdateCompletionDate.add(lstNew[i]);
                setCliToUpdateCompletionDate.add(lstNew[i].TECH_CaseContractLineItem__c);
            }

            if(
                lstOld[i].Customer_Absences__c != lstNew[i].Customer_Absences__c
                && lstNew[i].Customer_Absences__c == 1
                && lstNew[i].TECH_CaseContractLineItem__c != null
            ){
                system.debug('****call to AP57');
                lstWOToUpdateClientAbsent.add(lstNew[i]);
                setCliToUpdateAbsenceClient.add(lstNew[i].TECH_CaseContractLineItem__c);
            }   

            //AP66 Update CLI VE_Status
            if(
                lstNew[i].Type__c == 'Maintenance' && lstNew[i].Reason__c == 'Visite sous contrat' 
                  && lstNew[i].Status == 'New' && lstNew[i].TECH_CaseContractLineItem__c != null && lstOld[i].TECH_CaseContractLineItem__c == null
            ) {
                lstWOCLIVEStatus.add(lstNew[i]);
            }
            
            //TEC-925 AP12
            if (lstOld[i].WorkTypeId != lstNew[i].WorkTypeId) {
                setWoIdToPopulateCategory.add(lstNew[i].Id);
            }
            
            //AP58: FSL Agency
            //DMU - 3/6/20 - commented the if condition due to HomeServe
            // if(lstOld[i].AgencySubSector__c != lstNew[i].AgencySubSector__c
            //     && lstNew[i].Status == AP_Constant.wrkOrderStatusNouveau
            // ){

            //     if(lstNew[i].AgencySubSector__c != null){
            //         //todo in same format as in insert trigger
            //         mapWoToSubAgency.put(lstNew[i].Id, lstNew[i].AgencySubSector__c);
            //         lstWoToCreateResourcePref.add(lstNew[i]);                    
            //     }

            //     if(lstOld[i].AgencySubSector__c != null){
            //         lstWoIdToAgencyOld.add(lstOld[i].Id);
            //     }
            // }

            //DMU - 3/6/20 - commented the if condition due to HomeServe
            // if(lstOld[i].AgencySubSector__c != lstNew[i].AgencySubSector__c){
            //     //DMU 20200116 - Added logic to set agency Sub Sector on SA
            //     lstWOAgencySubSector.add(lstNew[i]);
            // }

        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP57')){
            if(!lstWOToUpdateCompletionDate.isEmpty()){
                AP57_CompletionDateOnCli.updateCliCompletedStatus(lstWOToUpdateCompletionDate, setCliToUpdateCompletionDate);
                
            }

            if(!lstWOToUpdateClientAbsent.isEmpty()){
                system.debug('*** not empty');
                AP57_CompletionDateOnCli.updateCustomerAbsence(lstWOToUpdateClientAbsent, setCliToUpdateAbsenceClient);
            }
        }
        
        // AP66 Update CLI VE_Status
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP66')){
            if(!lstWOCLIVEStatus.isEmpty()){
                AP66_SA_UpdateCLIStatus.updateWOCLIVEStatus(lstWOCLIVEStatus);
            }  
        }
        
        //AP12 Populate SA category
        
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP12')) {
            if (!setWoIdToPopulateCategory.isEmpty()) 
                AP12_ServiceAppointmentRules.populateCategory(setWoIdToPopulateCategory);
            
        }

        //AP58 Sub-agency on work orders: 
        //DMU - 3/6/20 - commented the if condition due to HomeServe
        // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP58')){
        //     if(!lstWoIdToAgencyOld.isEmpty()){
        //         //call delete
        //         AP58_WoResourceManagement.deleteResourcePrefs(lstWoIdToAgencyOld);
        //     }

        //     if(!lstWoToCreateResourcePref.isEmpty()){
        //         AP58_WoResourceManagement.createResourcePreference(mapWoToSubAgency, lstWoToCreateResourcePref);

        //     }
        // }

        //DMU - 3/6/20 - commented the if condition due to HomeServe
        // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP60')){
        //     if(lstWOAgencySubSector.size()>0){
        //         AP60_SetAgencySubSector.setAgencySubSectorSA(lstWOAgencySubSector);
        //     }
        // } 

    }
}