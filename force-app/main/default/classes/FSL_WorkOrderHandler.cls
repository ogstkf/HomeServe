/**
Name: FSL_WordOrderHandler
Created By: Tamar Shifman - Balink (tamars@balink.net)
Created on: 22.5.2019
Test Class: FSL_WorkOrderHandler_Test
FSL Handler for the WorkOrder Trigger
**/
public without sharing class FSL_WorkOrderHandler implements ITriggerHandleable {

    public static Boolean isCreateCA = false;

    //This method is call by the trigger. This is the main entry of the Before Insert action
    public void onBeforeInsert(List<WorkOrder> workOrders){
        //assignWorkType(workOrders);
        //setEntitlement(workOrders);        
    }

    public void onAfterInsert(List<workOrder> workOrders){
        assignWorkType(workOrders);
        setEntitlement(workOrders);
    }

    public void onBeforeUpdate(List<Sobject> newList, Map<Id, Sobject> oldMap){
        setEntitlement((List<WorkOrder>) newList, (Map<Id,WorkOrder>)oldMap);
    }

    /*Assign a work type for a work order- if the Work Order is the Main WO- the correct Work Type will be inserted  according  match filed
    between work type to work order case's,
    else the match filed take between work type to work order */
    private void assignWorkType(List<WorkOrder> workOrders){
        System.debug('Assign Work Type');
        // Set<Id> caseIds = getCasesIds(workOrders);
        // List<Case> cases = [
        //     SELECT Id, Type , Main_Work_Order_created__c, Reason__c, Asset.Product2.Equipment_type1__c
        //     FROM Case
        //     WHERE Id IN :caseIds];
        // Map<Id,Case> caseMap = new Map<Id,Case>(cases);
        // Map<Id,Case> mainWorkOrderMap = buildWorkOrdersMap(workOrders, caseMap);

        // if(!mainWorkOrderMap.isEmpty()){
        //     insertWorkTypeByCase(mainWorkOrderMap, cases, workOrders);
        // }

        // SH - 2020-01-23
        // WorkType on WorkOrder was filled by the Type/Reason on the Case
        // Now, it's filled by the Type/Reason on the WorkOrder
        Set<Id> woIds = (new Map<Id,WorkOrder>(workOrders)).keySet();
        Map<Id,String> workOrderEquipementTypes = new Map<Id,String>();
        Map<Id,String> mapScAgenceCodeToId = new Map<Id,String>();
        System.debug('#######wos:'+woIds);
        List<WorkOrder> wos = [
            SELECT Id, AssetId, Asset.Product2Id, Asset.Product2.Equipment_type1__c, Type__c, Reason__c, ServiceTerritoryId, ServiceTerritory.Agency_Code__c
                            FROM WorkOrder
                            WHERE Id IN :woIds
        ];
        for (WorkOrder wo : wos) {
            System.debug(wo.AssetId);
            System.debug(wo.Asset.Product2Id);
            workOrderEquipementTypes.put(wo.Id, wo.Asset.Product2.Equipment_type1__c);
            mapScAgenceCodeToId.put(wo.Id, wo.ServiceTerritory.Agency_Code__c);
        }
        //insertWorkTypeByWorkOrder(workOrders, workOrderEquipementTypes);
        insertWorkTypeByWorkOrder2(workOrders, workOrderEquipementTypes, mapScAgenceCodeToId);
    }

    //This is return the Ids for the cases related to the work orders
    @TestVisible
    private static Set<Id> getCasesIds(List<WorkOrder> workOrders){
        Set<Id> caseIds = new Set<Id>();
        for(WorkOrder wo : workOrders){
           caseIds.add(wo.CaseId);
        }
        return caseIds;
    }

    // SH - 2020-01-23 - All method is out because not used anymore
    //Method to build a map that related case to WorkOrder and check if main WO created
    // private Map<Id,Case> buildWorkOrdersMap(List<WorkOrder> workOrders, Map<Id,Case> caseMap){
    //     Map<Id,Case> mainWorkOrderMap = new Map<Id,Case>();
    //     List<WorkOrder> workOrderToWorkType = new List<WorkOrder>();
    //     Map<Id,String> workOrderEquipementTypes = new Map<Id,String>();
    //     for(WorkOrder wo : workOrders){
    //         if(wo.caseId != null){
    //             Case cs =  caseMap.get(wo.CaseId);
    //             boolean mainWoCreated = cs.Main_Work_Order_created__c;
    //             if(!mainWoCreated){
    //                 mainWorkOrderMap.put(wo.Id, caseMap.get(wo.CaseId));
    //             }
    //             else{
    //                 workOrderToWorkType.add(wo);
    //                 if(!workOrderEquipementTypes.containsKey(wo.Id))
    //                 workOrderEquipementTypes.put(wo.Id, cs.Asset.Product2.Equipment_type1__c);
    //             }
    //         }
    //     }
    //     if(!workOrderToWorkType.isEmpty()){
    //         insertWorkTypeByWorkOrder(workOrderToWorkType, workOrderEquipementTypes);
    //     }
    //     return mainWorkOrderMap;
    // }

    //Assign a work type for a work order By Work Order fields
    private void insertWorkTypeByWorkOrder(List<WorkOrder> workOrderToWorkType, Map<Id,String> workOrderEquipementTypes){
        /*Set<String> types = new Set<String>();
        Set<String> reasons = new Set<String>();
        Set<String> equipementTypes = new Set<String>();

        List<Id> lstAsset = new List<Id>();

        //DMU - create list of case
        map<string,string> mapWOIdSCId = new map<string,string>();
        map<string,string> mapScCLIId = new map<string,string>();
        for(WorkOrder wo : workOrderToWorkType){
            mapWOIdSCId.put(wo.Id, wo.ServiceContractId);
        }
        for(ContractLineItem cli : [SELECT id,ServiceContractId FROM ContractLineItem WHERE ServiceContractId IN: mapWOIdSCId.Values() AND IsBundle__c=true ]){
            mapScCLIId.put(cli.ServiceContractId, cli.Id);
        }

        list <case> lstCaseToIns = new list<case>();
        for(WorkOrder wo : workOrderToWorkType){
            System.debug('##### wo.IsGeneratedFromMaintenancePlan' + wo.IsGeneratedFromMaintenancePlan);
            System.debug('##### wo.MaintenancePlanId = ' + wo.MaintenancePlanId);
            if((wo.IsGeneratedFromMaintenancePlan && wo.MaintenancePlanId <> null) || (test.isRunningTest() && isCreateCA)){
                System.debug('##### IsGeneratedFromMaintenancePlan = ' + wo.IsGeneratedFromMaintenancePlan);
                System.debug('##### MaintenancePlanId = ' + wo.MaintenancePlanId);                
                case ca = new case();
                ca.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
                ca.AccountId = wo.AccountId;
                ca.ContactId = wo.ContactId;
                ca.Service_Contract__c = wo.ServiceContractId;
                ca.Contract_Line_Item__c = mapScCLIId.get(mapWOIdSCId.get(wo.id));
                // ca.logement = wo.Asset.Logement__c; //There's no logement field on case
                ca.AssetId = wo.AssetId;
                ca.Campagne__c = wo.Campagne__c;
                ca.Type = wo.Type__c;
                ca.Reason__c = wo.Reason__c;
                ca.Local_Agency__c = wo.ServiceTerritoryId;
                ca.Status = 'In Progress';
                ca.Subject = wo.Type__c;
                ca.TECH_WO_FSL__c = wo.Id;
                lstCaseToIns.add(ca);
                System.debug('##### AV_lstCaseToIns = ' + lstCaseToIns);
            }

        }  
        try{
            insert lstCaseToIns;
            System.debug('##### AP_lstCaseToIns = ' + lstCaseToIns);
        }  catch(exception e){
            system.debug('*** ins failed');
        }

        for(WorkOrder wo : workOrderToWorkType){
           types.add(wo.Type__c);
           reasons.add(wo.Reason__c);
           equipementTypes.add(workOrderEquipementTypes.get(wo.Id));
           lstAsset.add(wo.AssetId);
        }

        //Map Asset to prod
        Map<Id, Id> mapAssetProd = new Map<Id,Id>();
        for(Asset asst : [SELECT Id, Product2Id FROM Asset WHERE Id IN :lstAsset]){
            mapAssetProd.put(asst.Id, asst.Product2Id);
        }

        List<Product2> lstProd = [SELECT Id, Equipment_type1__c FROM Product2 WHERE Id IN :mapAssetProd.values()];
        Map<Id, Product2> mapProd = new Map<Id, Product2>();
        for(Product2 pro : lstProd){
            mapProd.put(pro.Id, pro);
        }
        System.debug('########workOrderEquipementTypes:'+workOrderEquipementTypes);
        Map<String,Map<String,Map<String, WorkType>>> workTypesMap = buildWorkTypesMap(types, reasons, equipementTypes );
        List<WorkOrder> toUpdate = new List<WorkOrder>();
        for(WorkOrder wo : workOrderToWorkType){
            WorkOrder w = new WorkOrder(Id=wo.Id);

            if((wo.Type__c == 'First Maintenance Visit (new contract)' || wo.Type__c == 'Maintenance') && (wo.Reason__c == 'Flat-rate visit' || wo.Reason__c == 'First maintenance')){
                WorkType wrkType = [SELECT Id, Type__c, Reason__c, Equipment_Type__c, EstimatedDuration, DurationType FROM WorkType WHERE Type__c IN ('First Maintenance Visit (new contract)', 'Maintenance') AND Reason__c = :wo.Reason__c AND Equipment_type__c = :mapProd.get(mapAssetProd.get(wo.AssetId)).Equipment_type1__c LIMIT 1];
                w.WorkTypeId = wrkType.Id;
                w.Duration = wrkType.EstimatedDuration;
                w.DurationType = wrkType.DurationType;
                toUpdate.add(w);
            }
            else if(workTypesMap.containsKey(wo.Type__c) && workTypesMap.get(wo.Type__c).containsKey(wo.Reason__c) && workTypesMap.get(wo.Type__c).get(wo.Reason__c).containsKey(workOrderEquipementTypes.get(wo.Id))){
                w.WorkTypeId = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).Id;
                w.Duration = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).EstimatedDuration;
                w.DurationType = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).DurationType;
                System.debug(w);
                toUpdate.add(w);
            }
        }
        if(!toUpdate.isEmpty())
            update toUpdate;

        */
    }

    // SH - 2020-01-23 - All method is out because not used anymore
    //Assign a work type for a work order by Case fields
    // private void insertWorkTypeByCase(Map<Id,Case> mainWorkOrderMap, List<Case> cases, List<WorkOrder> workOrders){
    //     Set<String> types = new Set<String>();
    //     Set<String> reasons = new Set<String>();
    //     Set<String> equipementTypes = new Set<String>();
    //     for(Case cs : cases){
    //         types.add(cs.Type);
    //         reasons.add(cs.Reason__c);
    //         equipementTypes.add(cs.Asset.Product2.Equipment_type1__c);
    //     }
    //     Map<String,Map<String,Map<String, WorkType>>> workTypesMap = buildWorkTypesMap(types,reasons,equipementTypes);
    //     List<WorkOrder> toUpdate = new List<WorkOrder>();
    //     for(WorkOrder wo : workOrders){
    //         if(!mainWorkOrderMap.containsKey(wo.Id))
    //             continue;
    //         if(workTypesMap.containsKey(mainWorkOrderMap.get(wo.Id).Type) && workTypesMap.get(mainWorkOrderMap.get(wo.Id).Type).containsKey(mainWorkOrderMap.get(wo.Id).Reason__c) && workTypesMap.get(mainWorkOrderMap.get(wo.Id).Type).get(mainWorkOrderMap.get(wo.Id).Reason__c).containsKey(mainWorkOrderMap.get(wo.Id).Asset.Product2.Equipment_type1__c)){
    //             WorkOrder w = new WorkOrder(Id=wo.Id);
    //             w.WorkTypeId = workTypesMap.get(mainWorkOrderMap.get(wo.Id).Type).get(mainWorkOrderMap.get(wo.Id).Reason__c).get(mainWorkOrderMap.get(wo.Id).Asset.Product2.Equipment_type1__c).Id;
    //             w.Duration = workTypesMap.get(mainWorkOrderMap.get(wo.Id).Type).get(mainWorkOrderMap.get(wo.Id).Reason__c).get(mainWorkOrderMap.get(wo.Id).Asset.Product2.Equipment_type1__c).EstimatedDuration;
    //             w.DurationType =  workTypesMap.get(mainWorkOrderMap.get(wo.Id).Type).get(mainWorkOrderMap.get(wo.Id).Reason__c).get(mainWorkOrderMap.get(wo.Id).Asset.Product2.Equipment_type1__c).DurationType;
    //             toUpdate.add(w);
    //         }
    //     }
    //     if(!toUpdate.isEmpty())
    //         update toUpdate;
    // }

    //Build a map that is returning a work type according to Type, Reason and Equipement Type by Case/Work Order
    public static Map<String,Map<String,Map<String, WorkType>>> buildWorkTypesMap(Set<String> types, Set<String> reasons, Set<String> equipementTypes){
        /*Map<String,Map<String,Map<String, WorkType>>> ret = new Map<String,Map<String,Map<String, WorkType>>>();
        List<WorkType> workTypes= [
            SELECT Id, Type__c, Reason__c, Equipment_Type__c, DurationType, EstimatedDuration
            FROM WorkType
            WHERE Type__c = :types OR Reason__c = : reasons OR Equipment_type__c = :equipementTypes];
        for(WorkType wt : workTypes){
            if(!ret.containsKey(wt.Type__c))
                ret.put(wt.Type__c, new Map<String,Map<String, WorkType>>());
            if(!ret.get(wt.Type__c).containsKey(wt.Reason__c))
                ret.get(wt.Type__c).put(wt.Reason__c, new Map<String, WorkType>());
            if(!ret.get(wt.Type__c).get(wt.Reason__c).containsKey(wt.Equipment_Type__c))
                ret.get(wt.Type__c).get(wt.Reason__c).put(wt.Equipment_Type__c, wt);
        }

        return ret;*/
        return null;
    }

    //#region MNA - 2021-01-26 TEC 464 Assignation du worktype en fonction du champ Type de client 
    // MNA 07/06/2021 TEC-672 Intégrer l'agence pour l'assignation du worktype
    public static void insertWorkTypeByWorkOrder2(List<WorkOrder> workOrderToWorkType, Map<Id,String> workOrderEquipementTypes, Map<Id,String> mapScAgenceCodeToId) {
        
        Set<String> types = new Set<String>();
        Set<String> reasons = new Set<String>();
        Set<String> equipementTypes = new Set<String>();

        List<Id> lstAsset = new List<Id>();

        //DMU - create list of case
        map<string,string> mapWOIdSCId = new map<string,string>();
        map<string,string> mapScCLIId = new map<string,string>();
        map<string, string> mapWoIdSCRecTypeName = new map<string,string>();    
        for(WorkOrder wo : workOrderToWorkType){
            mapWOIdSCId.put(wo.Id, wo.ServiceContractId);
        }
        for(ContractLineItem cli : [SELECT id,ServiceContractId FROM ContractLineItem WHERE ServiceContractId IN: mapWOIdSCId.Values() AND IsBundle__c=true ]){
            mapScCLIId.put(cli.ServiceContractId, cli.Id);
        }

        for(WorkOrder wo : [SELECT Id, ServiceContractId, CaseId, ServiceContract.recordType.DeveloperName, Case.Type_de_collectif_WT__c FROM WorkOrder WHERE Id IN :workOrderToWorkType]) {
            System.debug('##########'+wo.CaseId);
            if (wo.CaseId == null) {
                if(wo.ServiceContractId == null)
                    mapWoIdSCRecTypeName.put(wo.Id, '');
                else {
                    if (wo.ServiceContract.recordType.DeveloperName == 'Contrats_Collectifs_Prives')
                        mapWoIdSCRecTypeName.put(wo.Id, 'Collectif Privé');
                    else if (wo.ServiceContract.recordType.DeveloperName == 'Contrats_collectifs_publics')
                        mapWoIdSCRecTypeName.put(wo.Id, 'Collectif Public');
                    else
                        mapWoIdSCRecTypeName.put(wo.Id, '');
                }
            }
            else {
                mapWoIdSCRecTypeName.put(wo.Id, (wo.Case.Type_de_collectif_WT__c == null) ? '' : wo.Case.Type_de_collectif_WT__c);
            }
        }

        list <case> lstCaseToIns = new list<case>();
        for(WorkOrder wo : workOrderToWorkType){
            System.debug('##### wo.IsGeneratedFromMaintenancePlan' + wo.IsGeneratedFromMaintenancePlan);
            System.debug('##### wo.MaintenancePlanId = ' + wo.MaintenancePlanId);
            if((wo.IsGeneratedFromMaintenancePlan && wo.MaintenancePlanId <> null) || (test.isRunningTest() && isCreateCA)){
                System.debug('##### IsGeneratedFromMaintenancePlan = ' + wo.IsGeneratedFromMaintenancePlan);
                System.debug('##### MaintenancePlanId = ' + wo.MaintenancePlanId);                
                case ca = new case();
                ca.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
                ca.AccountId = wo.AccountId;
                ca.ContactId = wo.ContactId;
                ca.Service_Contract__c = wo.ServiceContractId;
                ca.Contract_Line_Item__c = mapScCLIId.get(mapWOIdSCId.get(wo.id));
                // ca.logement = wo.Asset.Logement__c; //There's no logement field on case
                ca.AssetId = wo.AssetId;
                ca.Campagne__c = wo.Campagne__c;
                ca.Type = wo.Type__c;
                ca.Reason__c = wo.Reason__c;
                ca.Local_Agency__c = wo.ServiceTerritoryId;
                ca.Status = 'In Progress';
                ca.Subject = wo.Type__c;
                ca.TECH_WO_FSL__c = wo.Id;
                lstCaseToIns.add(ca);
                System.debug('##### AV_lstCaseToIns = ' + lstCaseToIns);
            }

        }  
        try{
            insert lstCaseToIns;
            System.debug('##### AP_lstCaseToIns = ' + lstCaseToIns);
        }  catch(exception e){
            system.debug('*** ins failed');
        }

        

        for(WorkOrder wo : workOrderToWorkType){
           types.add(wo.Type__c);
           reasons.add(wo.Reason__c);
           equipementTypes.add(workOrderEquipementTypes.get(wo.Id));
           lstAsset.add(wo.AssetId);
        }

        //Map Asset to prod
        Map<Id, Id> mapAssetProd = new Map<Id,Id>();
        for(Asset asst : [SELECT Id, Product2Id FROM Asset WHERE Id IN :lstAsset]){
            mapAssetProd.put(asst.Id, asst.Product2Id);
        }

        List<Product2> lstProd = [SELECT Id, Equipment_type1__c FROM Product2 WHERE Id IN :mapAssetProd.values()];
        Map<Id, Product2> mapProd = new Map<Id, Product2>();
        for(Product2 pro : lstProd){
            mapProd.put(pro.Id, pro);
        }
        System.debug('########workOrderEquipementTypes:'+workOrderEquipementTypes);
        Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> workTypesMap = buildWorkTypesMap2(types, reasons, equipementTypes );
        List<WorkOrder> WoWithoutWT = new List<WorkOrder>();
        Map<Id, WorkOrder> mapWo = new Map<Id,WorkOrder>(workOrderToWorkType);
        List<WorkOrder> toUpdate = new List<WorkOrder>();
        for(WorkOrder wo : workOrderToWorkType){
            
            WorkOrder w = new WorkOrder(Id=wo.Id);
            WorkType wrkType = new WorkType();
            /*
            if((wo.Type__c == 'First Maintenance Visit (new contract)' || wo.Type__c == 'Maintenance') && (wo.Reason__c == 'Flat-rate visit' || wo.Reason__c == 'First maintenance')){
                Map<String, WorkType> mapWt = new Map<String, WorkType>();
                List<WorkType> lstWrkType = [SELECT Id, Type__c, Reason__c, Equipment_Type__c, EstimatedDuration, DurationType, Type_de_client__c FROM WorkType WHERE Type__c IN ('First Maintenance Visit (new contract)', 'Maintenance') AND Reason__c = :wo.Reason__c AND Equipment_type__c = :mapProd.get(mapAssetProd.get(wo.AssetId)).Equipment_type1__c];
                for (WorkType wt : lstWrkType) {
                    mapWt.put(wt.Type_de_client__c, wt);
                }
                
                if (mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Privé') {
                    wrkType = mapWt.containsKey('Copro-Syndic') ? mapWt.get('Copro-Syndic') : (mapWt.containsKey('Tous') ? mapWt.get('Tous') : null);
                }
                else if (mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Public') {
                    wrkType = mapWt.containsKey('Bailleur') ? mapWt.get('Bailleur') : (mapWt.containsKey('Tous') ? mapWt.get('Tous') : null);
                }
                else if (mapWoIdSCRecTypeName.get(wo.Id) == '') {
                    wrkType = mapWt.containsKey('Particulier') ? mapWt.get('Particulier')  : (mapWt.containsKey('Tous') ? mapWt.get('Tous') : null);
                }
                if (wrkType != null) {
                    w.WorkTypeId = wrkType.Id;
                    w.Duration = wrkType.EstimatedDuration;
                    w.DurationType = wrkType.DurationType;
                    toUpdate.add(w);
                }
            }
            else*/ 

            if(workTypesMap.containsKey(wo.Type__c) && workTypesMap.get(wo.Type__c).containsKey(wo.Reason__c) && workTypesMap.get(wo.Type__c).get(wo.Reason__c).containsKey(workOrderEquipementTypes.get(wo.Id))) {
                if (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).containsKey(mapScAgenceCodeToId.get(wo.Id))
                    && (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).containsKey('Tous')
                        || (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).containsKey('Copro-Syndic') && mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Privé')
                        || (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).containsKey('Bailleur') && mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Public')
                        || (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).containsKey('Particulier') && mapWoIdSCRecTypeName.get(wo.Id) == '')
                        )
                ) {
                    if (mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Privé') {
                        wrkType = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).containsKey('Copro-Syndic') 
                                        ? workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).get('Copro-Syndic')
                                        : workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).get('Tous');
                    }
                    else if (mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Public') {
                        wrkType = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).containsKey('Bailleur') 
                                        ? workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).get('Bailleur')
                                        : workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).get('Tous');
                    }
                    else if (mapWoIdSCRecTypeName.get(wo.Id) == '') {
                        wrkType = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).containsKey('Particulier') 
                                        ? workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).get('Particulier')
                                        : workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get(mapScAgenceCodeToId.get(wo.Id)).get('Tous');
                    }
                    w.WorkTypeId = wrkType.Id;
                    w.Duration = wrkType.EstimatedDuration;
                    w.DurationType = wrkType.DurationType;
                    toUpdate.add(w);               
                }
                else if (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).containsKey('Toutes')
                    && (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').containsKey('Tous')
                        || (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').containsKey('Copro-Syndic') && mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Privé')
                        || (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').containsKey('Bailleur') && mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Public')
                        || (workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').containsKey('Particulier') && mapWoIdSCRecTypeName.get(wo.Id) == '')
                        )
                ) {
                    if (mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Privé') {
                        wrkType = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').containsKey('Copro-Syndic') 
                                        ? workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').get('Copro-Syndic')
                                        : workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').get('Tous');
                    }
                    else if (mapWoIdSCRecTypeName.get(wo.Id) == 'Collectif Public') {
                        wrkType = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').containsKey('Bailleur') 
                                        ? workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').get('Bailleur')
                                        : workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').get('Tous');
                    }
                    else if (mapWoIdSCRecTypeName.get(wo.Id) == '') {
                        wrkType = workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').containsKey('Particulier') 
                                        ? workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').get('Particulier')
                                        : workTypesMap.get(wo.Type__c).get(wo.Reason__c).get(workOrderEquipementTypes.get(wo.Id)).get('Toutes').get('Tous');
                    }
                    w.WorkTypeId = wrkType.Id;
                    w.Duration = wrkType.EstimatedDuration;
                    w.DurationType = wrkType.DurationType;
                    toUpdate.add(w);               
                }
                else {
                    WoWithoutWT.add(w);
                }
            }
            else {
                WoWithoutWT.add(w);
            }               
        }
        if(!toUpdate.isEmpty())
            update toUpdate;
        if (!WoWithoutWT.isEmpty())
            WorkOrderBlockInsertion(WoWithoutWT, mapWo);        
        
    }

    public static Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> buildWorkTypesMap2(Set<String> types, Set<String> reasons, Set<String> equipementTypes){
        Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> ret = new Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>>();
        List<WorkType> workTypes= [
            SELECT Id, Type__c, Reason__c, Equipment_Type__c, DurationType, EstimatedDuration, Type_de_client__c, Agence__c 
            FROM WorkType
            WHERE Type__c = :types OR Reason__c = : reasons OR Equipment_type__c = :equipementTypes];
        for(WorkType wt : workTypes){
            if(!ret.containsKey(wt.Type__c))                                                                                                        //Type
                ret.put(wt.Type__c, new Map<String,Map<String,Map<String,Map<String, WorkType>>>>());
            if(!ret.get(wt.Type__c).containsKey(wt.Reason__c))                                                                                      //Reason
                ret.get(wt.Type__c).put(wt.Reason__c, new Map<String,Map<String,Map<String, WorkType>>>());
            if(!ret.get(wt.Type__c).get(wt.Reason__c).containsKey(wt.Equipment_Type__c))                                                            //Type d'Equipment
                ret.get(wt.Type__c).get(wt.Reason__c).put(wt.Equipment_Type__c, new Map<String,Map<String, WorkType>>());                           
            if (!ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).containsKey(wt.Agence__c))                                         //Code agence
                ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).put(wt.Agence__c, new Map<String, WorkType>());
            if(!ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).get(wt.Agence__c).containsKey(wt.Type_de_client__c))                //Type de Client
                ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).get(wt.Agence__c).put(wt.Type_de_client__c, wt);
            
        }

        return ret;
    }
    //#endregion fin TEC 464

    public static void WorkOrderBlockInsertion(List<WorkOrder> lstWo, Map<Id,WorkOrder> mapWo) {
        for (WorkOrder wo : lstWo) {
            mapWo.get(wo.Id).addError('Ce type d’intervention n’est pas configuré pour votre société sur ce type d\'équipement.'
                                +' Merci de choisir une combinaison de Type/motif différente ou de contacter votre administrateur pour créer le type d’intervention.');
        }
    }

    //ENTITLEMENTS LOGIC
    private void setEntitlement(List<WorkOrder> wos, Map<Id,WorkOrder> oldwos){
        List<WorkOrder> changedWos = new List<WorkOrder>();
        for(WorkOrder wo : wos){
            if(TriggerUtility.hasChanged(wo, oldWos.get(wo.Id), 'Type__c') || TriggerUtility.hasChanged(wo, oldWos.get(wo.Id), 'Reason__c') || TriggerUtility.hasChanged(wo, oldWos.get(wo.Id), 'Assetid')){
                changedWos.add(wo);
            }
        }
        if(!changedWos.isEmpty()){
            setEntitlement(changedWos);
        }
    }

    private void setEntitlement(List<WorkOrder> wos){
        /*Map<Id, Asset> assets = findWOsAssets(wos);
        for(WorkOrder wo : wos){
            if(wo.Type__c=='Troubleshooting'){
                if(wo.Reason__c=='Total Failure'){
                    Id totalFailureEntitlementId = woGetEntitlementValue(wo,assets,'Type', new Set<String>{'Total failure'});
                    if(totalFailureEntitlementId != null){
                        wo.EntitlementId = totalFailureEntitlementId;
                    }
                }
                else{
                    Id partialFailureEntitlementId = woGetEntitlementValue(wo,assets,'Type', new Set<String>{'Partial failure'});
                    if(partialFailureEntitlementId != null){
                        wo.EntitlementId = partialFailureEntitlementId;
                    }
                }
            }
        }*/
    }
    
    @TestVisible
    private static Id woGetEntitlementValue(WorkOrder wo, Map<Id, Asset> assets, string fieldName, set<String> requiredValues){
        if(wo.assetId != null && assets.containsKey(wo.assetId))
            return assetGetEntitlementValue(assets.get(wo.assetId), fieldName, requiredValues);
        return null;
    }
    
    @TestVisible
    private static Id assetGetEntitlementValue(Asset asset, string fieldName, set<String> requiredValues){
        for(Entitlement entitlement : asset.Entitlements){
            if(requiredValues.contains(entitlement.Type))
                return entitlement.Id;
        }
        return null;
    }

    private Map<Id, Asset> findWOsAssets(List<WorkOrder> wos){
        Set<Id> assetIds = new Set<Id>();
        for(WorkOrder wo : wos){
            assetIds.add(wo.AssetId);
        }
        return new Map<Id, Asset>([Select Id, (Select Id, type From Entitlements) From Asset Where id in :assetIds]);
    }
    //END ENTITLEMENTS LOGIC

    public void onAfterUpdate(List<Sobject> newList, Map<Id, Sobject> oldMap){}

    public void onBeforeDelete(List<Sobject> old){}

    public void onAfterDelete(List<Sobject> old){}

    
}