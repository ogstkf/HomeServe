/**
 * @File Name          : ProductItemTransactionTriggerHandler.cls
 * @Description        :
 * @Author             : sc
 * @Group              :
 * @Last Modified By   : sc
 * @Last Modified On   : 3/30/2020, 5:09:41 PM
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    01/10/2019           sc                   Initial Version
/ **/
public with sharing class ProductItemTransactionTriggerHandler {

    Bypass__c userBypass;
    public ProductItemTransactionTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void handleBeforeInsert(List<ProductItemTransaction> lstPIT){
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP63')){
            if(!lstPIT.isEmpty()){
                AP63_ChangePITValues.changeValues(lstPIT);
            }
        }
    }

    public void handleAfterInsert(List<ProductItemTransaction> lstPIT){
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP61')){
            if(!lstPIT.isEmpty()){
                AP61_PIUpdateMediumPrice.updatePrice(lstPIT);
            }
        }
    }

}