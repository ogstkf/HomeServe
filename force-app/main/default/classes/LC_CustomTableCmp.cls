/**
 * @File Name          : LC_CustomTableCmp.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 12/06/2020, 00:00:19
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    07/05/2020   ZJO     Initial Version
**/
public with sharing class LC_CustomTableCmp {

  @AuraEnabled
	public static Object getFieldSet(String sObjectName, String fieldSetName){

        SObjectType objToken = Schema.getGlobalDescribe().get(sObjectName);
        Schema.DescribeSObjectResult d = objToken.getDescribe();
        Map<String, Schema.FieldSet> FsMap = d.fieldSets.getMap();
        Map<String, Schema.SObjectField> objFields = d.fields.getMap() ;
                
        List<headerWrapper> lstHeaders = new List<headerWrapper>();   
        Map<String, List<picklistWrapper>> mapPicklistToOptions = new Map<String, List<picklistWrapper>>();
        tableWrapper myTableWrapper = new tableWrapper();
        Boolean isEditable;
            
        if(FsMap.containsKey(fieldSetName)){
            for(Schema.FieldSetMember f : FsMap.get(fieldSetName).getFields()){  
                String fieldType = String.valueOf(f.getType()).toLowerCase();
               
                if (fieldType == 'reference' || fieldType == 'formula' || f.getFieldPath() == 'LineItemNumber'){
                    isEditable = false;
                }else{
                    isEditable = true;
                } 
                
                headerWrapper header = new headerWrapper(String.valueOf(f.getLabel()), String.valueOf(f.getFieldPath()), String.valueOf(f.getType()), isEditable); 
                lstHeaders.add(header);

                //Get each field picklist values
                if (fieldType == 'picklist'){
                    Schema.DescribeFieldResult fieldResult = objFields.get(f.getFieldPath()).getDescribe();
                    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                    List<picklistWrapper> picklistOptions = new List<picklistWrapper>();

                    for (Schema.PicklistEntry pickListVal : ple){
                        picklistWrapper option = new picklistWrapper(pickListVal.getLabel(), pickListVal.getValue(), pickListVal.isDefaultValue());
                        picklistOptions.add(option);
                    }
                    mapPicklistToOptions.put(f.getFieldPath(), picklistOptions);
                }            
            }
        } 

        if(!(lstHeaders.isEmpty() && mapPicklistToOptions.isEmpty())){
            myTableWrapper.headerList = lstHeaders;
            myTableWrapper.picklistMap = mapPicklistToOptions;
        }

        return myTableWrapper;
    }

    public class headerWrapper{
        @AuraEnabled public String label;
        @AuraEnabled public String name;
        @AuraEnabled public String type;
        @AuraEnabled public Boolean editable;
         
        public headerWrapper(String label, String name, String type, Boolean editable){
            this.label = label;
            this.name = name;
            this.type = type; 
            this.editable = editable;  
        }
    }

    public class tableWrapper{
        @AuraEnabled 
        public List<headerWrapper> headerList;
        @AuraEnabled
        public Map<String, List<picklistWrapper>> picklistMap;

        public tableWrapper(){
            headerList = new List<headerWrapper>();
            picklistMap = new Map<String, List<picklistWrapper>>();
        }
    }

    @AuraEnabled
    public static Object getTableRecords(String parentRecordId, List<String> lstFieldNames){
        List<sObject> lstResult = new List<sObject>();
        String fieldsToQuery = String.join(lstFieldNames, ', ');
   
        String query = 'SELECT Id, ServiceContractId, UnitPrice, ' + fieldsToQuery + ' FROM ContractLineItem WHERE ServiceContractId = \'' + parentRecordId + '\' AND UnitPrice > 0';
        
        System.Debug('query:' + query);

        for(sObject s : Database.query(query)){
            lstResult.add(s);
        }
        return lstResult;    
    } 
    
    public class picklistWrapper {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public Boolean isDefault {get; set;}
        
        public picklistWrapper(String label, String val,Boolean isDefault) {
            this.label = label;
            this.value = val;
            this.isDefault = isDefault;
        }
    }

    @AuraEnabled    
    public static Boolean updateCliRecords(List<ContractLineItem> lstUpdatedClis) {            
       try{
         update lstUpdatedClis;

       }catch (Exception e){
         return false;
       }
       return true; 
    } 

    @AuraEnabled
    public static Object filterRecords(String parentRecordId, List<String> lstFieldNames, String filters){
       List<sObject> lstResult = new List<sObject>();
       String fieldsToQuery = String.join(lstFieldNames, ', ');

       List<Object> lstFilters = (List<Object>) JSON.deserializeUntyped(filters);
       system.debug('filter list:' + lstFilters);

       String searchQuery = '';

       for(Object filter : lstFilters){
         Map<String, Object> filterData = (Map<String, Object>)filter;

         String searchType = String.valueOf(filterData.get('searchType'));
         String searchField = String.valueOf(filterData.get('searchField'));

        if (searchType == 'STRING' || searchType == 'REFERENCE'){
            searchQuery += ' AND ' + searchField + ' LIKE \'%' + String.valueOf(filterData.get('searchVal')) + '%\'';
        }
        else if (searchType == 'DOUBLE' || searchType == 'PERCENT' || searchType == 'CURRENCY'){
            Map<String, Object> numberData = (Map<String, Object>)filterData.get('searchVal');
            String minVal = String.valueOf(numberData.get('min'));
            String maxVal = String.valueOf(numberData.get('max'));

            if (String.isBlank(minVal)){
                searchQuery += ' AND ' + searchField + ' <= ' + maxVal;
            }else if (String.isBlank(maxVal)){
                searchQuery += ' AND ' + searchField + ' >= ' + minVal;
            }else{
                searchQuery += ' AND ' + searchField + ' >= ' + minVal + ' AND ' + searchField + ' <= ' + maxVal;
            }
        }
        else if (searchType == 'DATE'){
            Map<String, Object> dateData = (Map<String, Object>)filterData.get('searchVal');
            String startDate = String.valueOf(dateData.get('start'));
            String endDate = String.valueOf(dateData.get('end'));

            if (String.isBlank(startDate)){
                searchQuery += ' AND ' + searchField + ' <= ' + endDate;
            }else if (String.isBlank(endDate)){
                searchQuery += ' AND ' + searchField + ' >= ' + startDate;
            }else{
                searchQuery += ' AND ' + searchField + ' >= ' + startDate + ' AND ' + searchField + ' <= ' + endDate; 
            }
        }
        else if (searchType == 'PICKLIST'){
            String strOptions = String.valueOf(filterData.get('searchVal'));
            
            if (!strOptions.contains(';')){
                searchQuery += ' AND ' + searchField + ' = \'' + strOptions + '\'';

            }else{

                List<String> lstOptions = strOptions.split(';');
                List<String> lstQuotedOptions = new List<String>();
    
                for (String option: lstOptions){
                    String quotedStr = '\'' + option + '\'';
                    lstQuotedOptions.add(quotedStr);           
                }
                searchQuery += ' AND ' + searchField + ' IN (' + String.join(lstQuotedOptions, ', ') + ')';
            }                  
        }
       }

       String finalQuery = 'SELECT Id, ServiceContractId, UnitPrice, ' + fieldsToQuery + ' FROM ContractLineItem WHERE ServiceContractId = \'' + parentRecordId + '\' AND UnitPrice > 0' + searchQuery;
       System.Debug('final query:' + finalQuery);

       for(sObject s : Database.query(finalQuery)){
        lstResult.add(s);
       }
       return lstResult;         
    }
}