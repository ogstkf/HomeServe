/**
 * @File Name          : AP25_UpdateAddressWhenLogementCreated.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 19/12/2019, 09:55:51
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/10/2019   AMO     Initial Version
**/

//CT-1102
public with sharing class AP25_UpdateAddressWhenLogementCreated {
    public Static Void UpdateOwner(List<Logement__c> lstLogement){
        System.debug('Handle Before Insert Trigger');

        // List<Logement__c> lstLgmntAdd = new List<Logement__c>();
        // Map<String, String> maplgmntAcc = new Map<String, String>();
        // Map<Id, Account> mapAcc = new Map<Id, Account>();

        // for(Logement__c lgm : lstLogement){
        //     maplgmntAcc.put(lgm.Id, lgm.Inhabitant__c);
        // }

        // for(Account acc1 : [SELECT Id, BillingStreet, BillingCity, BillingPostalCode, BillingCountry FROM Account WHERE Id IN :maplgmntAcc.values()]){
        //     mapAcc.put(acc1.Id, acc1);
        // }

        // for(Logement__c lgmnt : lstLogement){
        //     if(lgmnt.Owner__c == NULL){
        //         lgmnt.Owner__c = lgmnt.Inhabitant__c;
        //     }
        // }
    }

    public Static Void UpdateAddressLatest(List<Logement__c> lstLogement){
        Map<Id, Logement__c> mapAcc = new Map<Id, Logement__c>();
        List<Id> lstLgmId = new List<Id>();
        List<Account> updt = new List<Account>();

        for(Logement__c lgmnt : lstLogement){
            lstLgmId.add(lgmnt.Inhabitant__c);
            mapAcc.put(lgmnt.Inhabitant__c, lgmnt);
        }

        for(Account acc : [SELECT Id, BillingStreet, BillingCity, BillingPostalCode, BillingCountry FROM Account  WHERE Id IN :lstLgmId]){
            if(mapAcc.containsKey(acc.Id)){
                acc.BillingStreet = mapAcc.get(acc.Id).Street__c;
                acc.BillingCity = mapAcc.get(acc.Id).City__c;
                acc.BillingPostalCode = mapAcc.get(acc.Id).Postal_Code__c;
                acc.BillingCountry = mapAcc.get(acc.Id).Country__c;
                updt.add(acc);
            }           
        }

        Update updt;

    }
}