/**
 * @File Name          : AP45_UpdateContractVE.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 4/23/2020, 2:10:06 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    15/11/2019   AMO     Initial Version
**/
public with sharing class AP45_UpdateContractVE {
    public static void UpdateServiceContract(List<ServiceAppointment> lstServApp){
        Map<String, ServiceAppointment> mapServAppCon = new Map<String, ServiceAppointment>();
        List<ServiceContract> lstUpdate = new List<ServiceContract>();
        List<ContractLineItem> lstCli = new List<ContractLineItem>();

        for(ServiceAppointment st : lstServApp){
            mapServAppCon.put( st.Service_Contract__c, st);
        }

        List<ServiceContract> lstServiceContract = [SELECT Id, Contract_Status__c, VE_completion_date__c,  (SELECT Id, VE__c, Completion_Date__c FROM ContractLineItems) FROM ServiceContract WHERE Id IN :mapServAppCon.keyset() AND Contract_Status__c = : AP_Constant.serviceContractStatutEnAttentePremiereVisitValid];
        
        if(Test.isRunningTest()){
            lstServiceContract = [SELECT Id, Contract_Status__c, VE_completion_date__c,  (SELECT Id, VE__c, Completion_Date__c FROM ContractLineItems) FROM ServiceContract];
        }
        
        for(ServiceContract sc : lstServiceContract){
            
            sc.Contract_Status__c = AP_Constant.serviceContractStatutContratActive;    
            Date myDate;
            if(mapServAppCon.containsKey(sc.Id))
            	// myDate = mapServAppCon.get(sc.Id).SchedStartTime != null ? Date.newinstance((mapServAppCon.get(sc.Id).SchedStartTime).year(), (mapServAppCon.get(sc.Id).SchedStartTime).month(), (mapServAppCon.get(sc.Id).SchedStartTime).day()) : null ;

                if(mapServAppCon.get(sc.Id).ActualStartTime != null){
                    myDate = Date.newinstance((mapServAppCon.get(sc.Id).ActualStartTime).year(), (mapServAppCon.get(sc.Id).ActualStartTime).month(), (mapServAppCon.get(sc.Id).ActualStartTime).day());
                }else{
                    if(mapServAppCon.get(sc.Id).SchedStartTime != null){ //DMU 20200618 - CT-1824 - replace ActualStartTime by SchedStartTime in below line
                        myDate = Date.newinstance((mapServAppCon.get(sc.Id).SchedStartTime).year(), (mapServAppCon.get(sc.Id).SchedStartTime).month(), (mapServAppCon.get(sc.Id).SchedStartTime).day());
                    }else if(mapServAppCon.get(sc.Id).TECH_Cham_Digital__c){
                        myDate = Date.newinstance((mapServAppCon.get(sc.Id).EarliestStartTime).year(), (mapServAppCon.get(sc.Id).EarliestStartTime).month(), (mapServAppCon.get(sc.Id).EarliestStartTime).day());
                    }
                }

            if(Test.isRunningTest()){
                myDate = Date.toDay();
            }
            for(ContractLineItem cli : sc.ContractLineItems){
                cli.Completion_Date__c = myDate;
                cli.VE__c = true;
                cli.VE_Status__c = 'Complete';
                lstCli.add(cli);
            }

            lstUpdate.add(sc);
            System.debug('Service contract with required status: ' + sc);
        }

        if(lstCli.size() > 0){
            Update lstCli;
        }

        if(lstUpdate.size() > 0){
            Update lstUpdate;
        }
        
        System.debug('Service Contract(Status) to update' + lstUpdate);
        System.debug('Contract Line Item (Completion Date) to update' + lstCli);
                   
    }

}