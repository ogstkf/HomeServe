public with sharing class SSA_devis_controllerextension {
    
    public sofactoapp__Factures_Client__c Mafacture {get;set;}
    public list<sofactoapp__Factures_Client__c> FACT {get; set;}
    

    public SSA_devis_controllerextension (ApexPages.StandardController stdController) {
        this.Mafacture = (sofactoapp__Factures_Client__c)stdController.getRecord();
        string FactId = ApexPages.currentPage().getParameters().get('id');
        FACT = [SELECT id, sofactoapp__emetteur_facture__c,sofactoapp__emetteur_facture__r.sofactoapp_Agence__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Street,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Name,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Street2__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.PostalCode,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.City,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Phone__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Email__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Siret__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Intra_Community_VAT__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Agency_Manager__r.Name,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Agency_Manager_Phone__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Agency_Manager_E_mail__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Corporate_Name__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Legal_Form__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Capital_in_Eur__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.APE__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Site_web__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Corporate_Street__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Corporate_Street2__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Corporate_ZipCode__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Corporate_City__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Siren__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.RCS__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Professional_Indemnity_Insurance__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Decennal_Inurance__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Insurance_Cover__c,
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.IBAN__c,      
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.TECH_CapitalInEur__c,      
                                                        sofactoapp__emetteur_facture__r.sofactoapp_Agence__r.Logo_Name_Agency__c, 
                
                sofacto_Devis__r.Equipement__c
               from sofactoapp__Factures_Client__c
                where id = : FactID
               LIMIT 1];
        Mafacture = FACT [0];
       




    }

}