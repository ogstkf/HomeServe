@RestResource(urlMapping='/UpdateDeviceToken')
global with sharing class WS11_UpdateDeviceToken {
    	/**
 * @File Name          : WS11_UpdateDeviceToken.cls
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         08-10-2019     		    ANA         Initial Version
**/
    @HttpPatch
    global static Map <String, String> doPatch(Id UserId,String DeviceToken){

        Map <String, String> mapResponse = new Map <String, String>();

        try{
            User use = [SELECT Id,Android_Device_Token__c FROM User WHERE Id=:UserId];
            if(use!=null){
                use.Android_Device_Token__c = DeviceToken;
                update use;
                mapResponse.put('Status','200');
                mapResponse.put('Message','OK');
            }
        }catch(exception ex){
                system.debug('###In catch FAIL 111' + ex.getMessage());
                mapResponse.put('Status','500');
                mapResponse.put('ERROR', ex.getMessage());            
        }
        return mapResponse;
    }
}