public class HousingInformationWrapper{

    @AuraEnabled public Account theAccount{get;set;}
    @AuraEnabled public Opportunity theOpportunity{get;set;}
     
    public HousingInformationWrapper(Account theAccount, Opportunity theOpportunity){
        this.theAccount = theAccount;
        this.theOpportunity = theOpportunity;
    }
}