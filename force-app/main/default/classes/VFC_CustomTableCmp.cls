/**
 * @File Name          : VFC_CustomTableCmp.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08/05/2020, 21:11:32
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/05/2020   ZJO     Initial Version
**/
public class VFC_CustomTableCmp {

    public String contractId {get;set;}

    public VFC_CustomTableCmp(ApexPages.StandardSetController controller) {
        contractId  = ApexPages.CurrentPage().getparameters().get('id');       
    }
}