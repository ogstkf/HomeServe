/**
 * @File Name          : LC74_GenerateAvisDeRelance.cls
 * @Description        : Génération PDF unique d'un avis de relance contrat
 * @Author             : RRA
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 26/08/2020, 10:00:00
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    26/08/2019, 10:00:00   RRA     Initial Version */
public with sharing class LC74_GenerateAvisDeRelance {

    @AuraEnabled
    public static map<String,Object> saveAvisRelance(String servConId){

        List<ContentVersion> lstCV = new List<ContentVersion>();
        map<string,Object> mapOfResult = new map<string,Object>();
       // List<String> lstServContact = new List<String>();
        //lstServContact.add(servConId);
        ServiceContract servCon = [SELECT Id, TECH_Date_Edition_R1__c FROM ServiceContract WHERE Id =:servConId];

        Savepoint sp = Database.setSavepoint();
        try{

            PageReference AvisDeRelanceTemplate = new PageReference('/apex/VFP74_RelanceContrat');
            Blob docPDF;
            AvisDeRelanceTemplate.getParameters().put('lstIds', '[' + JSON.serialize(servConId) + ']');
            docPDF = (Test.isRunningTest() ? Blob.valueOf('Unit.Test') : AvisDeRelanceTemplate.getContentAsPDF());
            ContentVersion CV = new ContentVersion();
            Datetime now = Datetime.now();
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);
            CV.PathOnClient = 'Avis de relance_' + local + '.pdf';
            CV.Title = 'Avis de relance_' + local + '.pdf';
            CV.VersionData = docPDF;
            CV.IsMajorVersion = true;

            Insert CV;
            lstCV.add(CV);

            //Get Content Documents
            List<ContentVersion> docList = [SELECT ContentDocumentId FROM ContentVersion where Id=:lstCV];
            System.debug('## doc list:' + docList);
            Map<Id, Id> mapIdCVIdCDL = new Map<Id, Id>();
            for(ContentVersion contentV : docList){
                mapIdCVIdCDL.put(contentV.Id,contentV.ContentDocumentId);
            }

            //Create ContentDocumentLink 
            ContentDocumentLink conDocLink = New ContentDocumentLink();
            conDocLink.LinkedEntityId = servConId;
            conDocLink.ContentDocumentId = mapIdCVIdCDL.get(cv.Id);
            conDocLink.shareType = 'V';
            Insert conDocLink;

            //Update Last Modified Date
            servCon.TECH_Date_Edition_R1__c = System.today();
            Update servCon;

            mapOfResult.put('error',false);
	        mapOfResult.put('message', 'SUCCESS!!!');
            mapOfResult.put('AvisDeRelance', conDocLink.ContentDocumentId);

        }catch(Exception e){

            Database.rollback(sp);
            mapOfResult.put('error',true);
	        mapOfResult.put('message',e.getMessage());   
            throw new AuraHandledException(e.getMessage());           
        }

        return mapOfResult;        
    }

}