@RestResource(urlMapping='/WS05_GetDocument/*')
global without sharing class WS05_GetDocument {

    @HttpPost
    global static documentWrapper getDocument(){
        documentWrapper result = new documentWrapper(System.label.DocumentNotAvailable, new Map<String, String>());

        RestRequest req = RestContext.request;
        Map<String, Object> params =  (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());

        String docId = params.get('docId') != null ? String.valueOf(params.get('docId')) : null;
        if(docId != null && docId.length() == 18){
            SObjecttype sobjType;
            try{
                sobjType = Id.valueOf(docId).getsobjecttype();
                String sObjName = String.valueOf(sobjType);
                Map<String, String> docObj = new Map<String, String>();

                if(sObjName == 'ContentVersion'){
                    ContentVersion cv = getContentVersion(docId);
                    if(cv != null){
                        docObj.put('base64', EncodingUtil.base64Encode(cv.VersionData));
                        docObj.put('type', cv.FileExtension);
                        docObj.put('title', cv.Title);

                        result.doc = docObj;
                        result.msg = 'success';
                    }
                }
                else if(sObjName == 'Document'){
                    Document doc = getDocument(docId);
                    if(doc != null){
                        docObj.put('base64', EncodingUtil.base64Encode(doc.Body));
                        docObj.put('type', doc.Type);
                        docObj.put('title', doc.Name);

                        result.doc = docObj;
                        result.msg = 'success';
                    }
                }
                else if(sObjName == 'Attachment'){
                    Attachment doc = getAttachment(docId);
                    if(doc != null){
                        docObj.put('base64', EncodingUtil.base64Encode(doc.Body));
                        docObj.put('type', doc.ContentType.split('/')[1]);
                        docObj.put('title', doc.Name);

                        result.doc = docObj;
                        result.msg = 'success';
                    }
                }
            }
            catch(exception e){}
        }
        return result;
    }

    public static ContentVersion getContentVersion(String docId){
        return [SELECT ContentSize, FileExtension, Title, VersionData 
                FROM ContentVersion 
                WHERE id = :docId];
    }

    public static Document getDocument(String docId){
        return [SELECT Body,Name,Type
                FROM Document 
                WHERE id = :docId];
    }

    public static Attachment getAttachment(String docId){
        return [SELECT Body,ContentType,Name,ParentId
                FROM Attachment 
                WHERE id = :docId];
    }

    global class documentWrapper{
        @AuraEnabled global String msg {get; set;}
        @AuraEnabled global Map<String, String> doc {get;set;}

        global documentWrapper(String msg, Map<String, String> doc){
            this.msg = msg;
            this.doc = doc;
        }
    }
}