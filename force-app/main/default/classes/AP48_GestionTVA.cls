/**
 * @File Name          : AP48_GestionTVA.cls
 * @Description        : 
 * @Author             : SHU   
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/14/2020, 1:20:49 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * ---    -----------       -------           ------------------------ 
 * 1.0    24/11/2019         SHU              Initial Version  (CT-1172)
 * ---    -----------       -------           ------------------------ 
**/
public with sharing class AP48_GestionTVA {

    public static Map<String, decimal> getTVADevis(List<Id> lstDevisIds){
        Map<String, decimal> mapDevisId_TVA = new Map<String, decimal>();

        for(Quote qte : [SELECT id 
                                ,Logement__c 
                                ,Logement__r.Professional_Use__c 
                                ,Logement__r.Age__c 
                                ,Equipement__r.HEP_Equipment__c 
                                ,Type_de_devis__c
                                FROM Quote WHERE id IN :lstDevisIds
                                AND Quote.RecordType.DeveloperName = 'Devis_standard']){
            if(qte.Type_de_devis__c == 'Vente de pièces/équipements au guichet'){ // Add to AP_Constants // Vente guichet
                mapDevisId_TVA.put(qte.Id, 20); //TVA 20%
            }
            else if(qte.Logement__c != null){
                if(qte.Logement__r.Professional_Use__c == 'Oui (plus de 50% d\'usage professionnel)'){ //Add to AP_Constants
                    mapDevisId_TVA.put(qte.Id, 20); //TVA 20%
                }
                else if(qte.Logement__r.Professional_Use__c == 'Non (moins de 50% d\'usage professionnel)'){ //Add to AP_Constants
                    if(qte.Logement__r.Age__c == 'Moins_2ans'){ //Add to AP_Constants
                        mapDevisId_TVA.put(qte.Id, 20); //TVA 20%
                    }
                    else if(qte.Logement__r.Age__c == 'Plus_2ans' && qte.Equipement__r.HEP_Equipment__c){ //Add to AP_Constants
                        mapDevisId_TVA.put(qte.Id, 5.5); //TVA 5,5%
                    }
                    else if (qte.Logement__r.Age__c == 'Plus_2ans' && !qte.Equipement__r.HEP_Equipment__c){ //Add to AP_Constants
                        mapDevisId_TVA.put(qte.Id, 10); //TVA 10%
                    }
                }
            }
            else{//if no logement, return null as TVA as value should be entered by admin
                mapDevisId_TVA.put(qte.Id, null);
            }
        }
        return mapDevisId_TVA;
    }

    public static Map<String, decimal> getTVAServContr(List<Id> lstServContrIds){   
// Note : Logement est associé a l'asset (Asset__r ) et non pas au ServiceContrat!!
        Map<String, decimal> mapServContr_TVA = new Map<String, decimal>();
        Set<String> setBusinessAccType = new Set<String> {    'Entreprise'
                                                            , 'Particulier'
                                                            , 'Collectifs publics'
                                                            , 'Collectifs privés'
                                                        };//Add to AP_Constants 

        Set<String> setAssProdEquipType1 = new Set<String> {  'Chaudière fioul'
                                                            , 'Caisson VMC'
                                                            , 'Bouche VMC'
                                                            , 'Pompe à chaleur air/air et clim 1 unités'
                                                            , 'Pompe à chaleur air/air et clim 2 unités'
                                                            , 'Pompe à chaleur air/air et clim 3 unités'
                                                            , 'Pompe à chaleur air/air et clim 4 unités'
                                                            , 'Pompe à chaleur air/air et clim 5 + X unités'
                                                           };//Add to AP_Constants 
        for(ServiceContract ctr : [SELECT id 
                                ,Account.Type
                                ,Asset__c 
                                ,Asset__r.Product2.Equipment_type1__c
                                ,Asset__r.HEP_Equipment__c
                                ,Asset__r.Logement__c
                                ,Asset__r.Logement__r.Professional_Use__c
                                ,Asset__r.Logement__r.Age__c
                                FROM ServiceContract WHERE id IN :lstServContrIds]){

            System.debug('#### ctr: '+ctr.Asset__r.Product2.Equipment_type1__c);
            System.debug('#### ctr: '+ctr.Account.Type);
            System.debug('#### ctr: '+ctr.Asset__r.Logement__r.Professional_Use__c);
            System.debug('#### ctr: '+ctr.Asset__r.Logement__r.Age__c);

            if( setBusinessAccType.contains(ctr.Account.Type) 
            && ctr.Asset__r.Logement__r.Professional_Use__c == 'Non (moins de 50% d\'usage professionnel)' //Add to AP_Constants 
            && ctr.Asset__r.Logement__r.Age__c == 'Plus_2ans' ){ //Add to AP_Constants 

                if( setAssProdEquipType1.contains(ctr.Asset__r.Product2.Equipment_type1__c) ){
                    mapServContr_TVA.put(ctr.Id, 10); // TVA 10%
                }
                else if(ctr.Asset__r.HEP_Equipment__c){
                    mapServContr_TVA.put(ctr.Id, 5.5); // TVA 5,5%
                }
                else{ // Asset__r.HEP_Equipment__c is false
                    mapServContr_TVA.put(ctr.Id, 10); // TVA 10%
                }
            }
            else{
                mapServContr_TVA.put(ctr.Id, 20); // TVA 20%
            }
        }
        return mapServContr_TVA;
    }
}