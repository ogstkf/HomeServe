/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 21-12-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   21-12-2021   MNA   Initial Version
**/
global without sharing class BAT12_InsertClICollectif implements Database.Batchable<sObject>, Database.Stateful, Schedulable  {
    global ServiceContract sc;
    

    public static String query = 'SELECT Id, Product2Id, ServiceContractId, Description, AccountId__c,'
                                +'     IsBundle__c, StartDate, EndDate, Quantity, UnitPrice, Discount,'
                                +'     Collective_Account__c, AssetId, Raisons_de_la_suspension__c,'
                                +'     Fin_de_la_suspension__c, Debut_de_la_suspension__c,'
                                +'     VE_Number_Target__c, VE_Number_Counter__c,'
                                +'     Statut_facturation__c,'
                                +'     PricebookEntryId, PricebookEntry.Product2Id, '
                                +'     PricebookEntry.Product2.IsBundle__c,'
                                +'     ListPrice, TVA__c, Remplacement_effectue__c,'
                                +'     Desired_VE_Date__c, VE_Min_Date__c, VE_Max_Date__c,'
                                +'     Completion_Date__c'
                                +' FROM ContractLineItem '
                                +' WHERE ServiceContractId = :sc.Id '
                                +' AND ( IsActive__c = true '
                                +'         OR (IsActive__c = false '
                                +'             AND debut_de_la_suspension__c != null'
                                +'             AND fin_de_la_suspension__c != null)'
                                +')';

    global BAT12_InsertClICollectif() {

    }
    
    global BAT12_InsertClICollectif(ServiceContract sc) {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        
    }
    global void execute(Database.BatchableContext BC, List<ContractLineItem> lstCli) {
        if(mapOldConIdToOldClis.containsKey(oldConId)){
            Decimal oldBundlePrc;
            Id parentBundleId;

            for(ContractLineItem oldCli: mapOldConIdToOldClis.get(oldConId)){
                ServiceContract newCon = mapOldConIdToNewCon.get(oldConId);
                
                ContractLineItem newCli = oldCli.clone(false, false, false, false);
                newCli.ServiceContractId = newCon.Id;
                newCli.Discount = 0;
                newCli.VE_Number_Counter__c = 0;
                newCli.Statut_facturation__c = null;
                newCli.IsActive__c = false;

                if(oldCli.Desired_VE_Date__c != null){
                    newCli.Desired_VE_Date__c = oldCli.Desired_VE_Date__c.addYears(1);
                }

                if(oldCli.VE_Min_Date__c != null){
                    newCli.VE_Min_Date__c = oldCli.VE_Min_Date__c.addYears(1);
                }

                if(oldCli.VE_Max_Date__c != null){
                    newCli.VE_Max_Date__c = oldCli.VE_Max_Date__c.addYears(1);
                }
                
                newCli.TECH_CompletionDateAnnee_M_1__c = oldCli.Completion_Date__c;
                newCli.Completion_Date__c = null;
                
                Boolean isBundle = oldCli.PricebookEntry.Product2.IsBundle__c;
                Boolean isOption = false;
                if(isBundle){
                    oldBundlePrc = oldCli.UnitPrice;
                    
                    if(mapRenewalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                        newCli.PricebookEntryId = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                        // newCli.UnitPrice = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                        newCon.Pricebook2Id = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                        mapConToUpdt.put(newCon.Id, newCon);
                    }else if(mapNormalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                        newCli.PricebookEntryId = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                        // newCli.UnitPrice = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                        newCon.Pricebook2Id = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                        mapConToUpdt.put(newCon.Id, newCon);
                    }                    
                    newCli.VE__c = true;
                }else{
                    if(mapBundleToOption.containsKey(parentBundleId)){
                        isOption = mapBundleToOption.get(parentBundleId).contains(oldCli.PricebookEntry.Product2Id);
                    }
                }

                if(isOption){
                    if(mapRenewalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                        newCli.PricebookEntryId = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                        newCli.UnitPrice = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                        newCon.Pricebook2Id = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                    }else if(mapNormalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                        newCli.PricebookEntryId = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                        newCli.UnitPrice = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                        newCon.Pricebook2Id = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                    }
                    mapConToUpdt.put(newCon.Id, newCon);
                }

                // calculating new UnitPrice
                /*System.debug('### coeff key: '+ newCon.Id + '-' + newCli.PricebookEntry.Product2Id);
                Decimal coeff = mapContractIdProductToCoeff.get(newCon.Id + '-' + newCli.PricebookEntry.Product2Id);
                if(coeff != null){
                    Decimal newUnitPrice = oldCli.UnitPrice * coeff;
                    newCli.UnitPrice = newUnitPrice.setScale(2, RoundingMode.HALF_UP);
                }*/

                if(mapNewConIdToCLI.containsKey(newCon.Id)){
                    mapNewConIdToCLI.get(newCon.Id).add(newCli);
                }else{
                    mapNewConIdToCLI.put(newCon.Id, new List<ContractLineItem>{newCli});
                }
                
                // System.debug(newCli);
            }
        }
        System.debug('#### mapNewConIdToCLI: '+mapNewConIdToCLI);
        System.debug('#### mapConToUpdt: '+mapConToUpdt);

        List<ContractLineItem> lstNewCli = new List<ContractLineItem>();
        for(ServiceContract con : mapConToUpdt.values()){
            /*for(ContractLineItem cli : mapNewConIdToCLI.get(con.Id)){
                if(mapAllPbe.containsKey(con.Pricebook2Id+'-'+cli.Product2Id)){
                    cli.PricebookEntryId = mapAllPbe.get(con.Pricebook2Id+'-'+cli.Product2Id).Id;
                    lstNewCli.add(cli);
                }
            }*/
            for (Integer i = 0; i< mapNewConIdToCLI.get(con.Id).size(); i++) {
                if(mapAllPbe.containsKey(con.Pricebook2Id+'-'+mapNewConIdToCLI.get(con.Id)[i].Product2Id)){
                    mapNewConIdToCLI.get(con.Id)[i].PricebookEntryId = mapAllPbe.get(con.Pricebook2Id+'-'+mapNewConIdToCLI.get(con.Id)[i].Product2Id).Id;
                }
            }
        }
        if(mapConToUpdt.size()>0){
            // CLA 17/12/21 - AP48 ne sert a rien /////////////////////////////////////////////////////////////////////////////////////////////////////////
            // AP48_GestionTVA.getTVAServContr(new List<Id>(mapConToUpdt.keySet()));
            
            
            // update mapConToUpdt.values();
        }
        for (String strKey:mapNewConIdToCLI.KeySet()) {
            lstNewCli.addAll(mapNewConIdToCLI.get(strKey));
        }

        if(lstNewCli.size()>0){
            System.debug('Cpu time 6 : '+Limits.getCpuTime());
            insert lstNewCli;
            System.debug('Cpu time 7 : '+Limits.getCpuTime());
        }

    }
    global void finish(Database.BatchableContext BC) {

    }
    global void execute(SchedulableContext sc) {

    }
}
