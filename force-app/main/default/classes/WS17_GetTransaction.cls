/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 07-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   23-02-2022   MNA   Initial Version
**/
@RestResource(urlMapping='/transaction/*')
global with sharing class WS17_GetTransaction {
    @HttpGet
    global static slimPayObjectWrp.absData getMethod() {
        sofactoapp__External_Api__c trans;
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String transaction_id = request.requestURI.substringBetween('transaction/', '/');
        if  (transaction_id == null)
            transaction_id = request.requestURI.substringAfter('transaction/');

        try {
            trans = [SELECT Id, sofactoapp__Response__c FROM sofactoapp__External_Api__c WHERE transaction__c =: transaction_id];
        } catch (Exception ex ) {
            insert WebServiceLog.generateLog(System.now(), request.requestURI, '404', null, 'Veuillez vérifier l\'id de la transaction entré'
            );
            
            response.statusCode = 404;
            return new slimPayObjectWrp.errorData('Veuillez vérifier l\'id de la transaction entré');
        }

        insert WebServiceLog.generateLog(System.now(), request.requestURI, '200', null, trans.sofactoapp__Response__c);
        
        response.statusCode = 200;
        return (slimPayObjectWrp.responsePost) JSON.deserialize(trans.sofactoapp__Response__c, slimPayObjectWrp.responsePost.class);
    }
}