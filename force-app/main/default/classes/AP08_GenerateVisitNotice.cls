/**
 * @File Name          : AP08_GenerateVisitNotice.cls
 * @Description        : Class to generate a PDF and add as attachment to work order when service appointment is updated
 * @Author             : Spoon Consulting
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 21/08/2019, 17:48:39
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         14-JUN-2019              RRJ         Initial Version
**/
public class AP08_GenerateVisitNotice {

	public static void checkPDF(List<ServiceAppointment> lstSaAvisDePassage){
		for(ServiceAppointment servApp : lstSaAvisDePassage){
			servApp.VisitNoticeSent__c = true;
			servApp.Date_Visit_Notice_Generated__c = system.today();
		}
	}

	/**
	* @description Method to generate PDF and attach (create attachment) pdf to work order of service appointment. 
	* Method updates Visit Notice Sent flag on work order.
	* @author RRJ | 18/06/2019
	* @param List<Id> lstSrvAppId - list of service appointments ids to create pdf for
	* @return void
	*/
	// @future(callout=true)
	// public static void generatePDF(List<Id> lstSrvAppId){
	// 	List<ServiceAppointment> lstSrvApp = [SELECT Id, ParentRecordId, OwnerId, WorkTypeId, WorkType.Type__c, Account.ClientNumber__c FROM ServiceAppointment WHERE Id IN :lstSrvAppId AND WorkTypeId != null AND WorkType.Type__c = :AP_Constant.wrkTypeTypeMaintenance];
	// 	List<Attachment> lstAttToCreate = new List<Attachment>();
	// 	List<WorkOrder> lstWrkOrderToUpdate = new List<WorkOrder>();

	// 	for(ServiceAppointment srvApp : lstSrvApp){
	// 		//Generate content of PDF fron VF page
	// 		PageReference template = new PageReference('/apex/VFP05_VisitNoticePdf');
	// 		template.getParameters().put('id', srvApp.Id);
	// 		Blob attData = Blob.valueOf('');
	// 		if(!Test.isRunningTest()) attData = template.getContentAsPDF();

	// 		//create attachments to insert with task
	//        	Attachment att = new Attachment(
	// 			Body = attData,
	// 			Name = generateName(srvApp.Account.ClientNumber__c),
	// 			OwnerId = srvApp.OwnerId,
	// 			ParentId = srvApp.Id
	// 		);
	// 		lstAttToCreate.add(att);

	// 		//update VisitNoticeSent__c to 'true'
	// 		srvApp.VisitNoticeSent__c = true;

	// 		//DMU - 20190820 - Update Date_Visit_Notice_Generated__c (see jira ticket CT-895)
	// 		srvApp.Date_Visit_Notice_Generated__c = system.today();
	// 	}

	// 	//Creating Attachments;
	// 	if(lstAttToCreate.size() > 0){
	// 		insert lstAttToCreate;
	// 	}

	// 	//updating work orders
	// 	if(lstSrvApp.size() > 0){
	// 		update lstSrvApp;
	// 	}
	// }


	// private static String generateName(String custNum){
	// 	String pdfName = '';
	// 	pdfName += custNum != null || custNum != '' ? custNum + '-' : '';
	// 	pdfName += 'Avis de passage-';
	// 	Date datToday = Date.today();
	// 	pdfName += datToday.day() < 10 ? '0'+datToday.day() : datToday.day().format();
	// 	pdfName += datToday.month() < 10 ? '0'+datToday.month() : datToday.month().format();
	// 	pdfName += datToday.year();
	// 	pdfName += '.pdf';
	// 	return pdfName;
	// }
}