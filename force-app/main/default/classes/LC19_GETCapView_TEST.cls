/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 07-15-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-15-2020   ZJO   Initial Version
**/
@isTest
public with sharing class LC19_GETCapView_TEST {
            	/**
 * @File Name          : 
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         03-12-2019     		ANA         Initial Version
**/

    static User mainUser;
    static CAP__c cp;

    static{
            mainUser = TestFactory.createAdminUser('LC04CreateQuotw', TestFactory.getProfileAdminId());
            insert mainUser;

            System.runAs(mainUser){
                cp = new CAP__c(name='CP Name',Year__c='2019',Month__c=11);
                insert cp;
            }
    }

    @isTest
    public static void getCapsTEST(){
        System.runAs(mainUser){
            Test.startTest();
               List<CAP__c> lstCap = LC19_GETCapView.getCaps(10,2019);
            Test.stopTest();
           
        }
    }

}