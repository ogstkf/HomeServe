/**
 * @File Name          : BAT05_getPBELastHour_TEST.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 08/02/2020, 15:59:07
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/02/2020   RRJ     Initial Version
**/
@isTest
public class BAT05_getPBELastHour_TEST {

    static User mainUser;
    static Account testAcc= new Account();
    static List<Product2> lstTestProd = new List<Product2>();   
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();

    static{
        mainUser = TestFactory.createAdminUser('LC23', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            //creating account
            testAcc = TestFactory.createAccount('test Acc');
            testAcc.BillingPostalCode = '1233456';
            testAcc.Actif__c = true;
            testAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;   

            //create products    
            Product2 prod = TestFactory.createProduct('testProd');
            prod.ProductCode='1234';
            prod.Description='1234';
            prod.Family='Garantie';
            prod.famille_d_articles__c = 'Consommables';
            prod.stock_non_gere__c = false;
            prod.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId();
            prod.IsBundle__c = true;
            lstTestProd.add(prod); 

            Product2 prod1 = TestFactory.createProduct('testProd');
            prod1.ProductCode='12333334';
            prod1.Description='33';
            prod1.IsBundle__c = false;
            lstTestProd.add(prod1); 
         
            insert lstTestProd;

                //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true,
                RecordTypeId= Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId(),
                // Compte__c = testAcc.Id,
                Ref_Agence__c = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
        
            //pricebook entry
            PricebookEntry pbe0 = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[0].Id, 100);
            pbe0.override__c = false;
            lstPrcBkEnt.add(pbe0);                 
            PricebookEntry pbe1 = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[1].Id, 100);
            lstPrcBkEnt.add(pbe1);                 
            insert lstPrcBkEnt;

        }
    }


    @isTest
    public static void testBatch(){
        Test.startTest();
        BAT05_getPBELastHour batch = new BAT05_getPBELastHour();
        Id batchId = Database.executeBatch(batch);
        Test.stopTest();
    }

    @isTest
    public static void testSchedule(){
        Test.startTest();
        Id jobId = BAT05_getPBELastHour.scheduleBatch();
        Test.stopTest();
    }

}