/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-01-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-01-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class deleteApexLog{
    @future(callout=true)
    public static void del (){
        List <Apexlog> loglist = [Select Id from Apexlog limit 100];
        for(Apexlog al: loglist){
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(Url.getOrgDomainUrl().toExternalForm()
            + '/services/data/v44.0/sobjects/Apexlog/'+al.Id);
            req.setMethod('DELETE');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
            HttpResponse res = h.send(req);
            System.debug(res.getStatusCode());
        }
    }
}