/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 11-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   11-03-2022   MNA   Initial Version
**/
public with sharing class UtilJSON {
    public static Map<String, String> showToast(String title, String message, String type) {
        Map<String, String> returnValue = new Map<String, String>();
        returnValue.put('title', title);
        returnValue.put('message', message);
        returnValue.put('type', type);
        return returnValue;
    }
}