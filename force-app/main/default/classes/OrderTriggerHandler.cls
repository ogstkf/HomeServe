/**
 * @File Name          : ServiceAppointmentTriggerHandler.cls
 * @Description        : Handler class for ServceAppointment trigger
 * @Author             : SBH
 * @Group              : Spoon Consulting
 * @Last Modified By   : ANR
 * @Last Modified On   : 01-19-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    15/10/2019, 17:18:17          SBH                Initial Version
 * 1.1    04/11/2019, 18:56:17          SHU                CT-1087 : Mise a jour PRLI
 * 1.2    14/11/2019, 16:56:17          LGO                CT-1175 
 * 1.3    19/06/2020  13:08:00          DMU                Commented AP18_CreateShipments.createShipmentsForOrders, AP37_ProdReqstLineItem, AP41_OrderConditions due to homeserve project
 **/
public with sharing class OrderTriggerHandler {
    

    Bypass__c userBypass;

    public OrderTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void handleAfterInsert(List<Order> lstNewOrd){
         List<Order> lstOrdToUpd = new List<Order>();
         List<Order> lstOrdFD = new List<Order>();
         List<Order> lstAfterInstOrd = new List<Order>();

        for(integer i=0; i<lstNewOrd.size(); i++){
             if(lstNewOrd[i].RecordTypeId == getOrderRecordTypeID('Commandes_fournisseurs')){
                lstOrdToUpd.add(lstNewOrd[i]);
             }
             if(lstNewOrd[i].Status == 'Accusé de réception fournisseurs'){
                lstOrdFD.add(lstNewOrd[i]);
             }

             if(lstNewOrd[i].Type_de_livraison_de_la_commande__c != null && lstNewOrd[i].Mode_de_transmission__c != null && lstNewOrd[i].TotalAmount != null && lstNewOrd[i].Montant_total_de_la_commande__c != null ){
                 lstAfterInstOrd.add(lstNewOrd[i]);
             }
                
        }

        //DMU 20200619 - Commented AP41_OrderConditions due to homeserve project
         if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41')){
             //if(lstOrdToUpd.size()>0){
             //    AP41_OrderConditions.updateModeTransOnInit(lstOrdToUpd);
             //}

             //if(lstAfterInstOrd.size()>0){
             //    AP41_OrderConditions.updateModeTransOnInit(lstAfterInstOrd);
             //}

             if(lstOrdFD.size()>0){
                 AP41_OrderConditions.afterInsertOrderFD(lstOrdFD);
             }

        }
            
    }

    public void handleBeforeUpdate(List<Order> lstNew, List<Order> lstOld){
        //Initialisations
        List<Order> lstOrdersToCreateShipments = new List<Order>();
        Set<Id> setServiceTerritoryIds = new Set<Id>();
        List<Order> lstOrdersConditionsNEW = new List<Order>();
        List<Order> lstOrdersConditionsOLD = new List<Order>();
        List<Order> lstOrderFD = new List<Order>();
        List<Order> lstOrderPB = new List<Order>(); //AP77
        //Initialisations End
        
        //Filterings
        for(Integer i = 0 ; i < lstNew.size(); i++){
            
            //SBH: CT- 1083
            //System.debug('## Record Type check : ' + getOrderRecordTypeID('Commandes_fournisseurs') + ' -- ' + lstNew[i].RecordTypeId);
            //CT-1175

            if( (   (lstNew[i].Type_de_livraison_de_la_commande__c != lstOld[i].Type_de_livraison_de_la_commande__c
                    || lstNew[i].Mode_de_transmission__c != lstOld[i].Mode_de_transmission__c
                    || lstNew[i].TotalAmount != lstOld[i].TotalAmount
                    )
                    && lstNew[i].Montant_total_de_la_commande__c != null 
                )
                ||
                ( lstNew[i].Status != lstOld[i].Status && lstNew[i].Status == AP_Constant.ordStatusCommandeEmise )
            ){
                lstOrdersConditionsNEW.add(lstNew[i]);
                lstOrdersConditionsOLD.add(lstOld[i]);

            }

            if(lstNew[i].Status == 'Accusé de réception fournisseurs' && lstNew[i].Status != lstOld[i].Status){
            //     System.debug('Enter conditions before update for date de livraison');
                 lstOrderFD.add(lstNew[i]);
            }
            
            if(lstNew[i].AccountId != lstOld[i].AccountId || lstNew[i].Agence__c != lstOld[i].Agence__c){
                lstOrderPB.add(lstNew[i]);
            }
        }
        //Filterings END

        //DMU 20200619 - Commented AP41_OrderConditions due to homeserve project
        //Call APs Start
        
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41;')){
             //if(lstOrdersConditionsNEW.size() >0 ){
             //    AP41_OrderConditions.updateOrderConditionsORStatus(lstOrdersConditionsNEW);
             //}

             if(lstOrderFD.size() >0 ){
                 System.debug('Enter conditions before update for date de livraison Calling AP');
                 AP41_OrderConditions.beforeUpdtOrderFD(lstOrderFD);
             }
        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP18;')){
             AP18_CreateShipments.createShipmentsForOrders(lstOrdersToCreateShipments, setServiceTerritoryIds);
        }
        
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP77')){
            if(lstOrderPB.size() > 0){
                AP77_SetPriceBookOnOrder.setPriceBook(lstOrderPB);
            }            
        }
        
        //Call AP end

        System.debug('## Order before update end');
    }

    public void handleAfterUpdate(List<Order> lstNew, List<Order> lstOld){
        Set<Id> setOrderIds = new Set<Id>();
        List<Order> lstOrdersToCreateShipments = new List<Order>();
        Set<Id> setServiceTerritoryIds = new Set<Id>();
        List<Order> lstOrders = new List<Order>();
        Set<Id> setOrds = new Set<Id>();
        List<Id> lstOrderGeneratePDF = new List<Id>();

        //Filterings
        for(Integer i = 0 ; i < lstNew.size(); i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP37;')){
                if(lstNew[i].Status == 'Annulé' && lstNew[i].Status != lstOld[i].Status) { //To use AP_Constants
                    setOrderIds.add(lstNew[i].Id);
                }
             }

            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP18;')){
                 if(lstNew[i].RecordTypeId == getOrderRecordTypeID('Commandes_fournisseurs')
                     && lstNew[i].Status == AP_Constant.ordStatusCommandeEmise 
                     && lstOld[i].Status != AP_Constant.ordStatusCommandeEmise
                     ){
                     lstOrdersToCreateShipments.add(lstNew[i]);
                     setServiceTerritoryIds.add(lstNew[i].Agence__c);
                 }  
             }

             if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP37;')){
                 if(lstNew[i].Remise_mode__c != lstOld[i].Remise_mode__c && lstNew[i].Remise_mode__c != null) { 
                     lstOrders.add(lstNew[i]);
                 }
             }

             if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41;')){
                 if(lstNew[i].Status != lstOld[i].Status && lstNew[i].Status == 'Accusé de réception fournisseurs'){
                     lstOrderGeneratePDF.add(lstNew[i].Id);
                 }
             }

                
        }
        //Call APs Start

        //DMU 20200619 - commented AP41_OrderConditions due to homeserve project
        /*
        if(lstOrders.size() > 0){
             AP41_OrderConditions.updateOLIOrderChange(lstOrders);
        }*/
        //DMU 20200619 - commented AP18_CreateShipments due to homeserve project
        if(!lstOrdersToCreateShipments.isEmpty()){
             AP18_CreateShipments.createShipmentsForOrders(lstOrdersToCreateShipments, setServiceTerritoryIds);
        }

        //DMU 20200619 - commented AP37_ProdReqstLineItem due to homeserve project
        if(SetOrderIds.size() > 0){
             AP37_ProdReqstLineItem.setStatusAcommander(SetOrderIds);
        }

        if(lstOrderGeneratePDF.size() > 0){
            AP41_OrderConditions.generatePDF(lstOrderGeneratePDF);
        }
    } 
     
    public void handleBeforeInsert(List<Order> lstNewOrder){  

        List<Order> lstOrderPB = new List<Order>();

         //Filterings
        for(Integer i = 0 ; i < lstNewOrder.size(); i++){
           if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP77')){ 
                if(lstNewOrder[i].AccountId !=null && lstNewOrder[i].Agence__c !=null){
                    lstOrderPB.add(lstNewOrder[i]);
                }
           }
        }

        if(lstOrderPB.size() > 0)
            AP77_SetPriceBookOnOrder.setPriceBook(lstOrderPB);


    }

    public static String getOrderRecordTypeID(String devName){
      return Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
    }
}