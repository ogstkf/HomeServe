/**
 * @File Name          : AP38_ProdReqstLineManager_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (KJB)
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                   Author              Modification
 *==============================================================================
 * 1.0       06/11/2019       Spoon Consulting (KJB)     Initial Version
**/
@isTest
public with sharing class AP38_ProdReqstLineManager_TEST {

    static User mainUser;
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static List<ProductItem> lstProdItem;
    static Order ord;
    static OrderItem ordItem = new OrderItem();
    static list<ProductRequest> lstProdRequest = new list<ProductRequest>();
    static Quote quote;
    static ProductRequestLineItem prli = new ProductRequestLineItem();
    static Schema.Location loc = new Schema.Location(); // SH - 2020-01-22
    static Account FournisseurAcc;


    static{
        mainUser = TestFactory.createAdminUser('AP38@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccount('AP38');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            FournisseurAcc= new Account(Name='ChamFournissuer');
            FournisseurAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            insert FournisseurAcc;

            //Update Account
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //create products
            prod = TestFactory.createProduct('testProd');
            prod.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId(); // SH - 2020-01-22
            insert prod;

             //pricebook
            Pricebook2 standardPricebook = new Pricebook2( Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;

            //Create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1234',
                  sofactoapp__Invoice_prefix__c = '2134'
            );
            insert sofa;

            //Create operating hour
            OperatingHours newOperatingHour = TestFactory.createOperatingHour('Opt Hours test');
            insert newOperatingHour;

            //Create Agency
            ServiceTerritory agence1 = TestFactory.createServiceTerritory('agence 1', newOperatingHour.Id, sofa.Id);
            agence1.Inventaire_en_cours__c = false;
            insert agence1;
            
            // SH - 2020-01-22
            //create Location
            loc = new Schema.Location(
                Name='Test1',
                LocationType='Entrepôt',
                ParentLocationId=NULL,
                IsInventoryLocation=true,
                Agence__c = agence1.Id
            );
            insert loc;

            //Create Product Item
            lstProdItem = new List<ProductItem>{
                new ProductItem(
                    Quantite_allouee__c = 10,
                    Product2Id = prod.Id,
                    LocationId = loc.Id,
                    QuantityOnHand = 50
                )
            };

            insert lstProdItem;

            //create Order
            ord = new Order(AccountId = FournisseurAcc.Id,
                Name = 'TestOrder',
                EffectiveDate = System.today(),
                Status = AP_Constant.OrdStatusAchat,
                Pricebook2Id = lstPrcBk[0].Id,
                Emplacement_de_destination__c = loc.id, // SH - 2020-01-22
                RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId());
            insert ord;

            //Create Order Item
            ordItem = new OrderItem(
                OrderId=ord.Id,
                Product2Id=prod.Id,
                Quantity=decimal.valueof('3'),
                Quantite_recue__c=decimal.valueof('1'),
                PricebookEntryId=PrcBkEnt.Id,
                UnitPrice = 145);
            insert ordItem;

            //Create Product Request
            lstProdRequest = new List<ProductRequest> {
                new ProductRequest()
            };  
            insert lstProdRequest;

            //Create Product Request Line Item
            prli = new ProductRequestLineItem(
                Status = 'En cours de commande',
                Parent = lstProdRequest[0],
                ParentId = lstProdRequest[0].Id,
                Product2Id=prod.Id,
                Commande__c=ordItem.OrderId,
                QuantityRequested=decimal.valueof('3'),
                DestinationLocationId = loc.Id // SH - 2020-01-22
            ); 
            insert prli;
        }
    }

    @isTest
    public static void testPrliChangeStatus(){
        System.runAs(mainUser){
            
            Test.startTest();
                ordItem.Quantite_recue__c=decimal.valueof('3');
                update ordItem;
                ordItem.Quantite_recue__c=decimal.valueof('4'); // SH - 2020-01-22
                update ordItem; // SH - 2020-01-22
            Test.stopTest();

            ProductRequestLineItem lstNewPrli = [SELECT Status, Commande__c FROM ProductRequestLineItem WHERE Commande__c= :ordItem.OrderId];
            system.assertEquals(lstNewPrli.Status, AP_Constant.prliReserveAPreparer);

        }
    }
}