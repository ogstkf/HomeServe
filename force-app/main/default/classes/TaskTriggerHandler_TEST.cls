@isTest
public with sharing class TaskTriggerHandler_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Test Class for TaskTriggerHandler
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 02-Sept-2019  DMU    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  

    static User mainUser;
    static list <User> userlistNonAdmin = new list <user>();
    static list <Task> taskLst;

    static {
        mainUser = TestFactory.createAdminUser('TaskTriggerHandler@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            //Creating a Queue
            Group grpSales = new Group(Type='Queue', Name='SBF');
            insert grpSales;
                    
            Id profileIdNonAdmn = [select id from profile where Name like '%System Administrator%' limit 1].Id;

            userlistNonAdmin = new list <user>{TestFactory.createAdminUser('Tom',profileIdNonAdmn)
                                                ,TestFactory.createAdminUser('Jean',profileIdNonAdmn)
            };
            insert userlistNonAdmin;

            // Group grpSalesRetrieve = [select Id from Group where Name='SBF' AND Type = 'Queue'];

            list<GroupMember> grpmemberLst = new list<GroupMember>{
                new GroupMember(UserOrGroupId = userlistNonAdmin[0].Id, GroupId = grpSales.Id)
                ,new GroupMember(UserOrGroupId = userlistNonAdmin[1].Id, GroupId = grpSales.Id)
            };
            insert grpmemberLst;
       
            taskLst = new list <Task> {
                new Task(Subject = 'T1', Priority = 'Normal', Status = 'Open', TECH_Support_Group__c = String.valueOf(grpSales .Id))
                ,new Task(Subject = 'T2', Priority = 'Normal', Status = 'Open', TECH_Support_Group__c = String.valueOf(grpSales .Id))
            };
        }
    }

    @isTest
    public static void testAfterIns(){
        System.runAs(mainUser){            
            
            Test.startTest();
               insert taskLst;
            Test.stopTest();

            list<Task> tasktlist = [select id from Task where (Subject='T1' or Subject='T2')];
            system.debug('***** tasktlist size: '+ tasktlist.size());           
            system.assertEquals(4, tasktlist.size());
        }
    }

    @isTest
    public static void testAfterUpd_priseEnCharge(){
        list<Task> tasktlist = new list<Task>();
        System.runAs(mainUser){            
            insert taskLst;                 
        
            tasktlist = [select id, Status, Work_In_Progress_By__c, Work_In_Progress_By__r.Name from Task where (Subject='T1' or Subject='T2')];
            system.debug('*** tasktlist size 2: '+ tasktlist.size());
            system.debug('*** tasktlist Work_In_Progress_By__r name: '+ tasktlist[1].Work_In_Progress_By__r.Name); 

            // System.runAs(userlistNonAdmin[0]){
                Test.startTest();              
                        tasktlist[0].Work_In_Progress__c=true;
                        tasktlist[0].Status='Completed';
                        update tasktlist;                    
                Test.stopTest();
            // }

            list<Task> tasktlistUpd = [select id, Status, Work_In_Progress_By__c from Task where (Subject='T1' or Subject='T2')];
            system.debug('***** tasktlist size: '+ tasktlistUpd.size());           
            system.assertNotEquals(null, tasktlistUpd[1].Work_In_Progress_By__c);   
        }  
    }

    @isTest
    public static void testAfterUpd_modifiEStatut(){
        list<Task> tasktlist = new list<Task>();
        System.runAs(mainUser){            
            insert taskLst;                 
        
            tasktlist = [select id, Status, Work_In_Progress_By__c, Work_In_Progress_By__r.Name from Task where (Subject='T1' or Subject='T2')];
            system.debug('*** tasktlist size 2: '+ tasktlist.size());
            system.debug('*** tasktlist Work_In_Progress_By__r name: '+ tasktlist[1].Work_In_Progress_By__r.Name); 

            tasktlist[0].Work_In_Progress__c=true;
            update tasktlist;      
            
            Test.startTest();            
                tasktlist[0].Status='Completed';
                update tasktlist;                    
            Test.stopTest();
            
            list<Task> tasktlistUpd = [select id, Status, Work_In_Progress_By__c from Task where (Subject='T1' or Subject='T2')];
            system.debug('***** tasktlist size: '+ tasktlistUpd.size());           
            system.assertEquals('Completed', tasktlistUpd[1].Status);     
        }
    }

    @isTest
    public static void testAfterUpd_resetPriseEnChargeFalse(){
        list<Task> tasktlist = new list<Task>();
        System.runAs(mainUser){            
            insert taskLst;                 
        
            tasktlist = [select id, Status, Work_In_Progress_By__c, Work_In_Progress_By__r.Name from Task where (Subject='T1' or Subject='T2')];
            tasktlist[0].Work_In_Progress__c=true;
            update tasktlist;      
            
            Test.startTest();            
                tasktlist[0].Work_In_Progress__c=false;
                update tasktlist;                    
            Test.stopTest();
            
            list<Task> tasktlistUpd = [select id, Status, Work_In_Progress__c from Task where (Subject='T1' or Subject='T2')];
            system.debug('***** tasktlist size: '+ tasktlistUpd.size());           
            system.assertEquals(false, tasktlistUpd[1].Work_In_Progress__c);    
            system.assertEquals(false, tasktlistUpd[2].Work_In_Progress__c);     
            system.assertEquals(false, tasktlistUpd[3].Work_In_Progress__c);   
        }
    }
}