/**
 * @File Name          : BAT10_UpdateInventaireTypeVehicule.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 01/04/2020, 16:29:15
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/03/2020   AMO     Initial Version
**/
global with sharing class BAT10_UpdateInventaireTypeVehicule implements Database.Batchable<Sobject> {

    private List<ServiceTerritory> lstServiceTerritory;
    private Set<Id> setAgencyId;
    
    public BAT10_UpdateInventaireTypeVehicule(Set<Id> setAgenceId){
        System.debug('Enter batch 2');
        setAgencyId = setAgenceId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('BAT10_UpdateInventaireTypeVehicule');
        String query = 'SELECT Id, DestinationLocation.LocationType, SourceLocation.LocationType, SourceLocation.Agence__c, ShipmentId, Inventaire_en_cours__c, Agence_source__c FROM ProductTransfer';
        query += ' WHERE (SourceLocation.Agence__c in :setAgencyId OR SourceLocation.Agence__r.Inventaire_en_cours__c = false) AND';
        query += ' (DestinationLocation.Agence__c in :setAgencyId OR DestinationLocation.Agence__r.Inventaire_en_cours__c = false) AND';
        query += ' (SourceLocation.LocationType = \'Véhicule\' OR SourceLocation.LocationType = \'Entrepôt\') AND';
        query += ' (DestinationLocation.LocationType = \'Véhicule\'  OR DestinationLocation.LocationType  = \'Entrepôt\') AND Inventaire_en_cours__c = true ORDER BY CreatedDate ASC';
        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext BC, List<ProductTransfer> lstProdTrans) {

        System.debug('## : ' + lstProdTrans.size());
        System.debug('## List Product Transfer : ' + lstProdTrans);

        List<ProductTransfer> lstProductTrnsfrtoUpdate = new list<ProductTransfer>();
        
        for(ProductTransfer prdT : lstProdTrans){

            prdT.IsReceived = true;
            prdT.Inventaire_en_cours__c = false;

            lstProductTrnsfrtoUpdate.add(prdT);
        }
        System.debug('### AMO lstProductTrnsfrtoUpdate batch 2: ' + lstProductTrnsfrtoUpdate);

        //ByPass Trigger AP69
        //ProductTransferTriggerHandler.byPassTriggerAP69 = true;

        if(lstProductTrnsfrtoUpdate.size() > 0){
            Update lstProductTrnsfrtoUpdate;
        }
    }

    global void finish(Database.BatchableContext BC) {

        if(setAgencyId.size() > 0){
            System.debug('Call batch 3');
            BAT10_UpdateInventaireSA batSa = new BAT10_UpdateInventaireSA(setAgencyId);
            Database.executeBatch(batSa);
        }

        
    }
}