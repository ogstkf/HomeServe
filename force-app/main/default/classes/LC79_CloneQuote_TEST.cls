/**
 * @File Name         : LC79_CloneQuote_TEST.cls
 * @Description       : 
 * @Author            : Spoon Consulting (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 25-10-2021
 * Modifications Log 
 * ======================================================
 * Ver   Date           Author          Modification
 * =====================================================
 * 1.0   21-10-2021     MNA              Initial Version
**/
@isTest
public with sharing class LC79_CloneQuote_TEST {
    static User mainUser;
    static Account testAcc;
    static List<Opportunity> testLstOpp;
    static List<Quote> testLstQuo;
    static List<QuoteLineItem> lstQuoteLineItem;
    static List<OpportunityLineItem> lstOppLineItem;
    static List<Product2> lstTestProd;
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static Bypass__c bp = new Bypass__c();
    
    static {
        mainUser = TestFactory.createAdminUser('LC79@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser) {
            bp.SetupOwnerId  = mainUser.Id;
            bp.BypassValidationRules__c = true;
            bp.BypassWorkflows__c = true;
            bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53,MobileNotificationTrigger,AssignedResourceTrigger,ServiceAppointmentTrigger,AP10';
            insert bp;

            testAcc = TestFactory.createAccount('LC79_CloneQuote_TEST');
            insert testAcc;

            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstTestProd  = new List<Product2>{
                new Product2( Name = 'Product X1', ProductCode = 'MXZ-4F80VF3', isActive = true, Equipment_family__c = 'Pompe à chaleur', Statut__c   = 'Approuvée',
                                                Equipment_type__c = 'Pompe à chaleur air/eau', isBundle__c = true)
            };
            insert lstTestProd;
            
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd[0].Id, 0));
            insert lstPrcBkEnt;

            testLstOpp =  new List<Opportunity>{ 
                TestFactory.createOpportunity('Test1',testAcc.Id)
            };
            insert testLstOpp;

            testLstQuo =  new List<Quote> {
                new Quote (Name = 'Prestation 1', Nom_du_payeur__c = testAcc.Id, Status = 'In Review', Devis_etabli_le__c = System.today(),
                                OpportunityId = testLstOpp[0].Id, Pricebook2Id = standardPricebook.Id)
            };
            insert testLstQuo;

            lstQuoteLineItem = new List<QuoteLineItem>{
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[0].Id,
                                    QuoteId = testLstQuo[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today(),
                                    Taux_de_TVA__c = 5.5
                                ),             
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[0].Id,
                                    QuoteId = testLstQuo[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 20,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                )
            };

            insert lstQuoteLineItem;
            
            lstOppLineItem = new List<OpportunityLineItem>{
                TestFactory.createOpportunityLine(testLstOpp[0].Id, lstPrcBkEnt[0].Id),
                TestFactory.createOpportunityLine(testLstOpp[0].Id, lstPrcBkEnt[0].Id)
            };

            lstOppLineItem[0].UnitPrice = 10;
            lstOppLineItem[1].UnitPrice = 20;

            insert lstOppLineItem;
        }
    }
    
    @isTest
    static void testClone() {
        Quote quo;
        Opportunity opp;
        List<QuoteLineItem> lstQli;
        List<OpportunityLineItem> lstOli;
        Test.startTest();
        String quoId = LC79_CloneQuote.clone(testLstQuo[0].Id, 'Test Clone');
        Test.stopTest();
        quo = [SELECT Name, Status, Devis_signe_par_le_client__c, Date_de_signature_du_client__c, Acompte_verse__c, Sofacto_Collectedbytech__c, Facturation_post_intervention__c,
                            Mode_de_paiement_du_devis__c, Mode_de_paiement_de_l_acompte__c, Montant_de_l_acompte_verse__c, Date_du_paiement_de_l_acompte__c, Deuxieme_acompte__c, 
                            Pourcentage_du_deuxieme_acompte__c, OpportunityId FROM Quote WHERE Id =:quoId];
        if (quo != null) {
            System.assertEquals('Test Clone', quo.Name);
            System.assertEquals('Draft', quo.Status);
            System.assertEquals(false, quo.Devis_signe_par_le_client__c);
            System.assertEquals(null, quo.Date_de_signature_du_client__c);
            System.assertEquals(false, quo.Acompte_verse__c);
            System.assertEquals(false, quo.Sofacto_Collectedbytech__c);
            System.assertEquals(false, quo.Facturation_post_intervention__c);
            System.assertEquals(null, quo.Mode_de_paiement_du_devis__c);
            System.assertEquals(null, quo.Mode_de_paiement_de_l_acompte__c);
            System.assertEquals(null, quo.Montant_de_l_acompte_verse__c);
            System.assertEquals(null, quo.Date_du_paiement_de_l_acompte__c);
            System.assertEquals(false, quo.Deuxieme_acompte__c);
            System.assertEquals(null, quo.Pourcentage_du_deuxieme_acompte__c);
        }
    }
}