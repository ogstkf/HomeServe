public with sharing class AP16_MobileNotifications {
    
    public static void deleteExpiredLogs(){
        DateTime dd = DateTime.now();
        dd = dd.addDays(-7);
        List<mobile_notifications_read_status__c> items = [SELECT Id FROM mobile_notifications_read_status__c WHERE CreatedDate < : dd];
        if(items.size() > 0){
            delete items;
        }
    }
    
    
    @future(callout=true)
    public static void sendNotification(String topic, String message, Id id){
        if(topic != null){
         
            string body =message;
            string title ='Notification';
            string obj ='Notification';
           // string ids =String.valueOf(IdServApp);
            string foregrounds = 'false';
            string clickaction ='OPEN_NOTIF';
            /*String deviceToken = '[';
            
            for(String tok : devTokens){
                deviceToken = deviceToken + '"' + tok + '",';
            }
            
            deviceToken = deviceToken.removeEnd(',') + ']';*/
            
            Http http = new Http(); 
            HttpRequest request = new HttpRequest();
            request.setEndpoint('https://fcm.googleapis.com/fcm/send');
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            //AIzaSyCLtqa-ie3AUvQgVeHU8KDSpWUdf2gVEuU
            request.setHeader('Authorization', 'Key='+label.firebase_key); //legacy server key
          //  request.setHeader('Authorization', 'Key='+'AAAAknGcYp8:APA91bELmQ10UJazTdIA0UKVhR-DcZM9oeISTdePG77wGo7fyB5Ue9-5Dngl3dn5enzx4bbGXDx4Pq4sU1KTO_WBS03b73SFNPYm19kq2QpjmY3yE4j_8fZWnDh-0yUtoT9K7zgRtk25');
           System.debug('label.firebase_key:'+ label.firebase_key);
            
           
            topic = topic.toLowerCase();
            topic = topic.replaceAll(' ', '_');
            String reqbody = '{"to":"/topics/'+topic+'", "data" : {"id":"'+id+'",  "body":"'+body+'", "title":"'+title+'", "object":"'+obj+'", "foreground":"'+foregrounds+'", "click_action":"'+clickaction+'"}}';
            System.debug('reqbody : '+reqbody);
            // Set the body as a JSON object
            request.setBody(reqbody);
            HttpResponse response = null;
            
            
            
            Integer statusCode = 200;
            String rbody = 'The response';
            if(!Test.isRunningTest()){
            	response = http.send(request);
                statusCode = response.getStatusCode();
            	rbody = response.getBody();
            }
            // Parse the JSON response
            if (statusCode != 200) {
                System.debug('The status code returned was not expected: ' +
                             statusCode + ' ' + rbody);
            }else {
                System.debug('## Status code 200');
                System.debug(statusCode);
                System.debug(rbody);
            }
        }
    }

    //Method ran when SA is assigned
    @future(callout=true)
    public static void onServiceAppointmentAssigned(Id IdServApp, String devToken){
        
        if(IdServApp != null && devToken != null){
            string deviceToken =  devToken; //your device token
            string body ='Votre planning a été mis à jour';
            string title ='TechEasy';
            string obj ='ServiceAppointement';
            string ids =String.valueOf(IdServApp);
            string foregrounds = 'false';
            string clickaction ='UPDATE_DATA';

            Http http = new Http(); 
            HttpRequest request = new HttpRequest();
            request.setEndpoint('https://fcm.googleapis.com/fcm/send');
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            request.setHeader('Authorization', 'Key='+label.firebase_key); //legacy server key

            String reqbody = '{"to":"'+deviceToken+'", "data" : { "body":"'+body+'", "title":"'+title+'", "object":"'+obj+'", "id":"'+ids+'", "foreground":"'+foregrounds+'", "click_action":"'+clickaction+'"}}';
            System.debug('reqbody : '+reqbody);
            // Set the body as a JSON object
            request.setBody(reqbody);
            HttpResponse response = test.IsRunningTest() ? new HttpResponse() : http.send(request);

            // Parse the JSON response
            if (response.getStatusCode() != 200) {
                System.debug('The status code returned was not expected: ' +
                response.getStatusCode() + ' ' + response.getStatus());
            }else {
                System.debug('## Status code 200');
                System.debug(response.getStatusCode());
                System.debug(response.getBody());
            }
        }


    }
        
       
    public static void onSaChanged(List<ServiceAppointment> lstSas){
       
        System.debug('## service appointment changed START');
        Set<Id> setSAIds = new Set<Id>();
        for(ServiceAppointment SA : lstSas){
            
            //Retrieve service resources for SAs
            setSAIds.add(SA.id);
        }
        
        List<AssignedResource> lstAssignedResources = [SELECT ServiceAppointmentId, ServiceResourceId, ServiceResource.RelatedRecordId, ServiceResource.RelatedRecord.Android_Device_Token__c FROM AssignedResource WHERE ServiceAppointmentId IN :setSAIds];
        System.debug('lstAssignedResources: ' + lstAssignedResources);
        for(AssignedResource AR: lstAssignedResources) {
        	if(AR.ServiceAppointmentId != null && 
               AR.ServiceResourceId != null &&
               AR.ServiceResource.RelatedRecordId != null &&
               AR.ServiceResource.RelatedRecord.Android_Device_Token__c != null &&
               AR.ServiceResource.RelatedRecordId != Userinfo.getUserId()
              ){
                  System.debug('## SENding an notification: ');
                ServiceAppointmentTriggerHandler.firstRun = false;
                onServiceAppointmentAssigned(AR.ServiceAppointmentId, AR.ServiceResource.RelatedRecord.Android_Device_Token__c);
            }
            
        }        
        System.debug('## service appointment changed END');
    }

    public static void BeforeSendingNotiff(List<Id> lstAssignedResource){

        if(!lstAssignedResource.isEmpty()){
            List<AssignedResource> lstAss = [SELECT ServiceAppointmentId, ServiceResourceId, ServiceResource.RelatedRecordId, ServiceResource.RelatedRecord.Android_Device_Token__c FROM AssignedResource WHERE Id =:lstAssignedResource[0]];
            System.debug('LstAss : '+lstAss);
            
            if(lstAss[0].ServiceAppointmentId != null && 
               lstAss[0].ServiceResource.RelatedRecordId != null &&
               lstAss[0].ServiceResource.RelatedRecord.Android_Device_Token__c != null &&
               lstAss[0].ServiceResource.RelatedRecordId != Userinfo.getUserId()
              ){
                  System.debug('## SENding an notification: ');
                onServiceAppointmentAssigned(lstAss[0].ServiceAppointmentId, lstAss[0].ServiceResource.RelatedRecord.Android_Device_Token__c);
            }
            
        }
    }
}