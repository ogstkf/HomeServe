/**
 * @File Name          : AP19_CreateShipmentOrPT_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 03/02/2020, 09:40:40
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    03/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class AP19_CreateShipmentOrPT_TEST {
    
    static User mainUser;
    static list<Schema.Location> lstloc;
    static User u;
    static list<Shipment> lstShip;
    static list<ProductTransfer> lstPT;
    static Product2 prod = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static Order ord;
    static Account testAcc;
    static OrderItem ordItem;
    static{
        mainUser = TestFactory.createAdminUser('AP19@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            User u = new User(
                LastName = 'last',
                Email = 'puser000@amamama.com',
                Username = 'puser000@amamama.com' + System.currentTimeMillis(),
                CompanyName = 'TEST',
                Title = 'title',
                Alias = 'alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId=TestFactory.getProfileAdminId()
            );

            insert u;

            //create account
            testAcc = TestFactory.createAccountBusiness('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();

            insert testAcc;


            //create products
            prod = TestFactory.createProduct('testProd');
            insert prod;


            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;


            //create Order
            ord = new Order(AccountId = testAcc.Id
                                    ,Name = 'Test1'
                                    ,EffectiveDate = System.today()
                                    ,Status = 'Demande d\'achats'
                                    ,Pricebook2Id = lstPrcBk[0].Id
                                    ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId());

            insert ord;

            ordItem = new OrderItem(OrderId=ord.Id
                                    ,UnitPrice=2
                                    ,Quantity=2
                                    ,PricebookEntryId=PrcBkEnt.Id);
            insert ordItem;



            lstShip = new list<Shipment>{
                new Shipment(/*SourceLocationId=lstloc[0].Id
                                ,DestinationLocationId=lstloc[1].Id
                                ,*/ShipToName='Ship Test'
                                ,DeliveredToId=u.Id
                                ,RecordTypeId=Schema.SObjectType.Shipment.getRecordTypeInfosByName().get('Reception Fournisseur').getRecordTypeId()
                                ,Order__c = ord.Id
                                ,ExpectedDeliveryDate = Date.today()
                                ,Satisfaction_qualit_de_la_commande__c='Moyenne'
                                ),
                
                new Shipment(/*SourceLocationId=lstloc[0].Id
                                ,*/ShipToName='Ship Test Null'
                                ,DeliveredToId=u.Id
                                ,RecordTypeId=Schema.SObjectType.Shipment.getRecordTypeInfosByName().get('Reception Fournisseur').getRecordTypeId()
                                ,Order__c = ord.Id
                                ,ExpectedDeliveryDate = Date.today()
                                ,Satisfaction_qualit_de_la_commande__c='Moyenne'
                                )
            };
            insert lstShip;

            lstPT = new list<ProductTransfer>{
                new ProductTransfer(ShipmentId=lstShip[1].Id
                                    ,QuantitySent=20),

                new ProductTransfer(ShipmentId=lstShip[0].Id
                                    ,QuantityReceived=10
                                    ,QuantitySent=20
                                    ,Quantity_Lot_received__c=10
                                    ,Quantity_lot_sent__c=20),

                new ProductTransfer(ShipmentId=lstShip[0].Id
                                    ,QuantityReceived=40
                                    ,QuantitySent=40
                                    ,Quantity_Lot_received__c=40
                                    ,Quantity_lot_sent__c=40),

                new ProductTransfer(ShipmentId=lstShip[0].Id
                                    ,QuantityReceived=50
                                    ,QuantitySent=100
                                    ,Quantity_Lot_received__c=50
                                    ,Quantity_lot_sent__c=100)
            };

            insert lstPT;
        }
    }

    @isTest
    public static void testInsertShipmentAndPT(){
        System.runAs(mainUser){
            lstShip[0].Status=AP_Constant.ShipStatusLivre;
            Test.startTest();
            try{
                update lstShip[0];
            }
            catch(Exception e){
                System.debug('testInsertShipmentAndPT : ');
                System.debug('e.getMessage() : '+e.getMessage());
            }
            Test.stopTest();
        }
    }

    @isTest
    public static void QuantityReceivedNullTEST(){
        System.runAs(mainUser){
            lstShip[1].Status=AP_Constant.ShipStatusLivre;
            Test.startTest();
            try{
                update lstShip[1];
            }
            catch(Exception e){
                System.debug('QuantityReceivedNullTEST');
                System.debug('e.getMessage() : '+e.getMessage());
                Boolean expectedExceptionThrown =  (e.getMessage().contains(label.QuantityReceivedNull)) ? true : false; 
                System.AssertEquals(true, expectedExceptionThrown, e.getMessage());
            }
            Test.stopTest();
        }
    }
}