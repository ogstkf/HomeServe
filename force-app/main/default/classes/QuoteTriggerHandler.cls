/**
 * @File Name          : QuoteTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 26-10-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         22-10-2019     		    SBH         Initial Version (ct-1088)
 * 1.1         04-11-2019               SBH         CT-1086
 * 1.2         04-12-2019               SHU         CT-1062
**/
public with sharing class QuoteTriggerHandler {

    Bypass__c userBypass;
    public static Boolean notRun = false;
    public static Boolean firstRun = true;
    private static String standardRecTypeId = AP_Constant.getRecTypeId('Quote', 'Devis_standard');

    public QuoteTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void handleAfterInsert(List<Quote> lstNewQuotes){
        //MNA: TEC-826
        Set<Id> payeurAccordClient = new Set<Id>();
        
        //SBH: CT-1086 start
        List<Quote> lstQuotesToCreateProdRequest = new List<Quote>();
        Map<Id, Id> mapQuoteToServiceTerritory = new Map<Id, Id>();
        //SBH: CT-1086 END
        
        for(Integer i = 0 ; i < lstNewQuotes.size(); i++){
            //MNA TEC-826
            payeurAccordClient.add(lstNewQuotes[i].Nom_du_payeur__c);
            
            if(lstNewQuotes[i].Status == AP_Constant.quoteStatusValideSigne){

                if(lstNewQuotes[i].TECH_OpportunityInterventionImmediate__c == false && lstNewQuotes[i].Ordre_d_execution__c != null
                    && !notRun //RRJ 20200505 CT-1717
                    && lstNewQuotes[i].RecordTypeId == standardRecTypeId //RRJ 20200512 CT-1718
                ){
                    //SBH: CT-1086
                    lstQuotesToCreateProdRequest.add(lstNewQuotes[i]);
                    mapQuoteToServiceTerritory.put(lstNewQuotes[i].Id, lstNewQuotes[i].Agency__c);
                    //END CT-1086
                }
            }
        }


        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP36')){
            if(!lstQuotesToCreateProdRequest.isEmpty()){
                AP36_QuoteProductTransfers.createProductRequests(lstQuotesToCreateProdRequest, mapQuoteToServiceTerritory);
            }
        }

        //MNA TEC-826
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP79')){
            if(payeurAccordClient.size() > 0)
                AP79_AccordClient.updtQuoAcc(payeurAccordClient);
        }

    }
    //Handle after update start
    public void handleAfterUpdate(List<Quote> lstOldQuotes, List<Quote> lstNewQuotes){
        System.debug('##QuoteTriggerHandler  handleAfterUpdate  start');
        Set<Quote> setQuo = new Set<Quote>();
        Set<Id> payeurAccordClient = new Set<Id>(); //MNA TEC-826
        List<Quote> lstQuoteUpdt = new List<Quote>();
        List<Quote> lstQuotesToCreateProdRequest = new List<Quote>(); //SBH: CT-1086
        Map<Id, Id> mapQuoteToServiceTerritory = new Map<Id, Id>(); //SBH: CT-1086
        List<Quote> lstQuotePRLI = new List<Quote>(); //CT-1181
        Set<Id> setValideQuoteIds = new Set<Id>(); // CT-1062
        Set<Id> setValideOppIds = new Set<Id>(); // CT-1062
        List<Id> lstQuoIdToGenPdf = new List<Id>(); // RRJ 20200204 - CT-1301
        List<Quote> lstQuoP3r = new List<Quote>(); // RRJ20200615 CT-1281
        

        for(Integer i = 0 ; i < lstOldQuotes.size(); i++){   
            //SBH CT1088
            if(lstOldQuotes[i].Status != AP_Constant.quoteStatusValideSigne && lstNewQuotes[i].Status == AP_Constant.quoteStatusValideSigne && !notRun) {
                
                payeurAccordClient.add(lstNewQuotes[i].Nom_du_payeur__c);   //MNA TEC-826

                if(lstOldQuotes[i].Status != lstNewQuotes[i].Status && !notRun // RRJ 20200505 CT-1717
                && lstNewQuotes[i].RecordTypeId == standardRecTypeId //RRJ 20200512 CT-1718
                ){
                    System.debug('## checking intervention immediate');
                    if(lstNewQuotes[i].TECH_OpportunityInterventionImmediate__c == false && lstNewQuotes[i].Ordre_d_execution__c != null
                        && !notRun // RRJ 20200505 CT-1717
                    ){
                        System.debug('## Intervention immediate set');
                        //SBH: CT-1086
                        lstQuotesToCreateProdRequest.add(lstNewQuotes[i]);
                        mapQuoteToServiceTerritory.put(lstNewQuotes[i].Id, lstNewQuotes[i].Agency__c);
                        //END CT-1086
                    }
                }
            }    
            
            //SHU20191205 CT-1062
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('LC08')){
                if(lstNewQuotes[i].IsSync__c && lstNewQuotes[i].Counter__c == lstOldQuotes[i].Counter__c && !notRun
                && lstNewQuotes[i].RecordTypeId == standardRecTypeId //RRJ 20200512 CT-1718
                ){  
                    system.debug('*** in quote 12 dmu');
                    setValideQuoteIds.add(lstNewQuotes[i].Id);
                    setValideOppIds.add(lstNewQuotes[i].OpportunityId);
                    // System.debug('###### In quote trigger for SYNC'+ notRun);
                }
                // else{
                //     System.debug('###### NOT In quote trigger for SYNC'+notRun);
                // }
            }

            //KZE CT-1123
            if(lstNewQuotes[i].A_planifier__c != lstOldQuotes[i].A_planifier__c && lstNewQuotes[i].A_planifier__c && lstNewQuotes[i].Status == AP_Constant.quoteStatusValideSigne && lstNewQuotes[i].TECH_OpportunityInterventionImmediate__c == false
            && lstNewQuotes[i].RecordTypeId == standardRecTypeId //RRJ 20200512 CT-1718
            && firstRun
            ){
                if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP31')){
                    setQuo.add(lstNewQuotes[i]);
                }
            }

            // RRJ 20200204 - CT-1301
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP64')){
                if(lstNewQuotes[i].Status != lstOldQuotes[i].Status && lstNewQuotes[i].Status == 'Validé, signé - en attente d\'intervention' && !notRun && lstNewQuotes[i].Counter__c == lstOldQuotes[i].Counter__c
                && lstNewQuotes[i].RecordTypeId == standardRecTypeId //RRJ 20200512 CT-1718
                ){
                    lstQuoIdToGenPdf.add(lstNewQuotes[i].Id);
                }
            }
        }

        for(Integer i = 0 ; i < lstNewQuotes.size(); i++){
            if(lstNewQuotes[i].Status != lstOldQuotes[i].Status
                && lstNewQuotes[i].RecordTypeId == standardRecTypeId //RRJ 20200512 CT-1718
            ){
                    lstQuoteUpdt.add(lstNewQuotes[i]);
            }

            if(lstOldQuotes[i].Status == 'Validé, signé - en attente d\'intervention' && lstNewQuotes[i].Status == 'Validé,signé mais abandonné'
                && lstNewQuotes[i].RecordTypeId == standardRecTypeId //RRJ 20200512 CT-1718
            ){
                lstQuotePRLI.add(lstNewQuotes[i]);
            }

            if(lstNewQuotes[i].Status != lstOldQuotes[i].Status && lstNewQuotes[i].Status == 'Validé, signé et terminé' && lstNewQuotes[i].TECH_Devis_P3R__c ){
                lstQuoP3r.add(lstNewQuotes[i]);
            }
        }

        //call APs
        //KZE CT-1123
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP31')){
            if(!setQuo.isEmpty()){
                firstRun = false;
                System.debug('#### rrj call AP31');
                AP31_ValideDevisIntervention.createWorkORder(setQuo);
            }
        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP36')){
            if(!lstQuotesToCreateProdRequest.isEmpty()){
                AP36_QuoteProductTransfers.createProductRequests(lstQuotesToCreateProdRequest, mapQuoteToServiceTerritory);
            }
        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP39')){
            if(!lstQuoteUpdt.isEmpty()){
                AP39_UpdtQUoteStatus.updateQuote(lstQuoteUpdt);
            }
        }

        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP40')){
            if(!lstQuotePRLI.isEmpty()){
                AP40_UpdtQuotePRLI.UpdtPRLI(lstQuotePRLI);
            }
        }

        //MNA TEC-826
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP79')){
            if(payeurAccordClient.size() > 0)
                AP79_AccordClient.updtQuoAcc(payeurAccordClient);
        }

        //SHU20191205 CT-1062
        if(setValideQuoteIds.size() > 0){
            System.debug(notRun);
            LC08_SyncQuote.SyncOppLineItems(setValideQuoteIds, setValideOppIds);
            System.debug(notRun);
        }

        if(lstQuoIdToGenPdf.size()>0){
            AP64_GenerateDevisPdf.generatePdfDevis(lstQuoIdToGenPdf);
        }
        if(lstQuoP3r.size()>0){
            AP28_StatusSA.updateCliP3r(lstQuoP3r);
        }
    }

    public void handleBeforeUpdate(List<Quote> lstOldQuotes, List<Quote> lstNewQuotes){
        Boolean isSofactoAdmin = AP73_PBSofactoFlag.isConnectedUserSofacto(); //CT-1840

        for(Integer i = 0 ; i < lstNewQuotes.size(); i++){
            if(lstOldQuotes[i].Status != AP_Constant.quoteStatusValideSigne && lstNewQuotes[i].Status == AP_Constant.quoteStatusValideSigne
                && lstNewQuotes[i].RecordTypeId == standardRecTypeId //RRJ 20200512 CT-1718
            ){
                if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('LC08')){ //CT-1062
                    lstNewQuotes[i].IsSync__c = true;
                }
            }

            //CT-1840
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP73')){
                if(!isSofactoAdmin){
                    
                    String strFlag = String.isBlank(lstNewQuotes[i].PBSofactoFlag__c) ? '' : lstNewQuotes[i].PBSofactoFlag__c;
                    String strFlagOld = String.isBlank(lstNewQuotes[i].PBSofactoFlag__c) ? '' : lstNewQuotes[i].PBSofactoFlag__c;
                    if(lstNewQuotes[i].Status != lstOldQuotes[i].Status && lstOldQuotes[i].Status == 'Validé, signé - en attente d\'intervention' && !lstNewQuotes[i].TECH_OppIsWon__c && lstNewQuotes[i].tech_deja_facture__c && lstNewQuotes[i].RecordType.DeveloperName != 'Devis_RTC'){
                        strFlag = 'Devis abondonné';
                    }
                    if(lstNewQuotes[i].TECH_OppIsWon__c && strFlag.contains('Devis abondonné')){
                        strFlag = strFlag.remove('Devis abondonné');
                    }

                    if(lstNewQuotes[i].Montant_de_l_acompte_verse__c != lstOldQuotes[i].Montant_de_l_acompte_verse__c && lstNewQuotes[i].tech_deja_facture__c && lstNewQuotes[i].RecordType.DeveloperName != 'Devis_RTC' && !strFlag.contains('Devis abondonné')){
                        strFlag += String.isBlank(strFlag) ? 'MAJ Acompte' : ',MAJ Acompte';
                    }

                    if((lstNewQuotes[i].Mode_de_paiement_de_l_acompte__c != lstOldQuotes[i].Mode_de_paiement_de_l_acompte__c || lstNewQuotes[i].Montant_de_l_acompte_verse__c != lstOldQuotes[i].Montant_de_l_acompte_verse__c) && 
                    lstNewQuotes[i].RecordType.DeveloperName != 'Devis_RTC' && lstOldQuotes[i].Montant_de_l_acompte_verse__c == null  && !strFlag.contains('Devis abondonné')){
                        strFlag += String.isBlank(strFlag) ? 'Versement Acompte' : ',Versement Acompte';
                    }
                    if(strFlagOld != strFlag){
                        lstNewQuotes[i].Flag_PB_Sofacto__c = true;
                        lstNewQuotes[i].PBSofactoFlag__c = strFlag;
                    }
                }
            }
        }

    }

    public void handleBeforeInsert(List<Quote> lstNewQuote){
        Boolean isSofactoAdmin = AP73_PBSofactoFlag.isConnectedUserSofacto(); //CT-1840
        AP32_UpdtWorkOrderInQuote.createQuote(lstNewQuote);

        for(Integer i=0; i<lstNewQuote.size(); i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP73')){ //CT-1840
                if(!isSofactoAdmin){
                    if(lstNewQuote[i].Opportunity.TECH_CompteAuxiliaire__c == null && lstNewQuote[i].RecordType.DeveloperName != 'Devis_RTC' ){
                        lstNewQuote[i].Flag_PB_Sofacto__c = true;
                        lstNewQuote[i].PBSofactoFlag__c = 'Compte Aux Vide';
                    }
                }
            }
        }
    }
}