/**
 * @File Name          : LC01_Acc360EqpDetails_TEST.cls
 * @Description        : Test class for LC01_Acc360EqpDetails
 * @Author             : SH
 * @Group              : 
 * @Last Modified By   : SH
 * @Last Modified On   : 28-06-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    03/01/2020                     SH          Initial Version
**/
@isTest
public class LC01_Acc360EqpDetails_TEST {
    static User mainUser;
    static Account testAcc;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstContLnItem = new List<ContractLineItem>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Case> lstCase = new List<Case>();
    static List<PricebookEntry> lstPriceBkEntry = new List<PricebookEntry>();
    static List<Product2> lstProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<Compte_Logement__c> lstCompteLogement = new List<Compte_Logement__c>();

    static sofactoapp__Raison_Sociale__c sofa;
    static sofactoapp__Coordonnees_bancaires__c Iban = new sofactoapp__Coordonnees_bancaires__c ();
    static sofactoapp__Plan_comptable__c plan = new sofactoapp__Plan_comptable__c();
    static sofactoapp__Compte_comptable__c compte = new sofactoapp__Compte_comptable__c();
    static sofactoapp__Compte_auxiliaire__c compteAux = new sofactoapp__Compte_auxiliaire__c();

    static {
        mainUser = TestFactory.createAdminUser('LC04CreateContract', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser)
        {
            //creating account
            testAcc = TestFactory.createAccount('test Acc');
            testAcc.BillingPostalCode = '1233456';
            testAcc.sofactoapp__Compte_auxiliaire__c = testAcc.Id; 
            testAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            // Create Sofacto Plan Comptable
            plan.Name = 'sofactoapp__Plan_comptable__c';
            insert plan;

            // Create Sofacto Compte Comptable
            compte.Name = '41compte comptable'; // Must start by '41'
            compte.sofactoapp__Plan_comptable__c = plan.Id;
            compte.sofactoapp__Libelle__c = 'sofactoapp__Libelle__c';
            insert compte;

            compteAux.Name = 'test Compte Auxilière';
            compteAux.sofactoapp__Compte__c = testAcc.Id;
            compteAux.sofactoapp__Compte_comptable__c = compte.Id;
            insert compteAux;

            // Create Sofacto Raison Sociale
            sofa = TestFactory.createSofactoapp_Raison_Sociale('test Raison Sociale');
            insert sofa;            

            // Create Sofacto Coordonnées bancaires
            Iban.Name ='testiban';
            Iban.sofactoapp__IBAN__c = 'FR0330002056460000117256J51';
            Iban.sofactoapp__Compte__c = testAcc.Id;
            insert Iban;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            //create service territory
            srvTerr = TestFactory.createServiceTerritory('test Territory', opHrs.Id, sofa.id);
            insert srvTerr;

            // //creating service contract
            // lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            // lstServCon[0].RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId();
            // lstServCon[0].Type__c = 'Collective';
            // lstServCon[0].Sofactoapp_Mode_de_paiement__c = 'Prélèvement';
            // lstServCon[0].sofactoapp_Rib_prelevement__c = Iban.Id;
            // lstServCon[0].Sofacto_DateOfSignature__c    = System.Today();
            // lstServCon[0].Pricebook2Id = Test.getStandardPricebookId();
            // lstServCon[0].Contract_Renewed__c = true;
            // lstServCon[0].Contract_Status__c = 'Résilié';

            // lstServCon.add(TestFactory.createServiceContract('test serv con 2', testAcc.Id));
            // lstServCon[1].RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId();
            // lstServCon[1].Type__c = 'Individual';
            // lstServCon[1].Sofactoapp_Mode_de_paiement__c = 'Prélèvement';
            // lstServCon[1].sofactoapp_Rib_prelevement__c = Iban.Id;
            // lstServCon[1].Sofacto_DateOfSignature__c    = System.Today();
            // lstServCon[1].Pricebook2Id = Test.getStandardPricebookId();
            // lstServCon[1].Contract_Renewed__c = true;
            // lstServCon[1].Contract_Status__c = 'Résilié';
            // insert lstServCon;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc.Id;
            lstLogement[1].RecordTypeId = Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Logement').getRecordTypeId();
            insert lstLogement;

            //creating Account/Logement
            lstCompteLogement.add(TestFactory.createCompteLogement(testAcc.Id, lstLogement[1].Id));
            lstCompteLogement[0].Actif__c = true;
            lstCompteLogement[0].Start__c = System.today();
            lstCompteLogement[0].Role__c = 'Occupant';
            insert lstCompteLogement;

            //creating products
            lstProd.add(TestFactory.createProduct('Ramonage gaz'));
            //lstProd.add(TestFactory.createProduct('Entretien gaz'));
            lstProd[0].IsBundle__c	= True;
            //lstProd[1].IsBundle__c = False;

            insert lstProd;

            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstProd[0].Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstLogement[1].Id));
            lstAsset[1].Product2Id = lstProd[0].Id;
            lstAsset[1].Logement__c = lstLogement[1].Id;
            lstAsset[1].AccountId = testAcc.Id;
            insert lstAsset;

            // //create pricebookentry
            // List<PricebookEntry> lstPriceBkEntry = new List<PricebookEntry>();
            // lstPriceBkEntry.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstProd[0].Id, 500 ));
            // lstPriceBkEntry.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstProd[1].Id, 200 ));
            // insert lstPriceBkEntry;

            // //insert contractline
            // lstContLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPriceBkEntry[0].Id, 5, 10));
            // lstContLnItem[0].VE__C = true;
            // lstContLnItem[0].StartDate = Date.today().addDays(-1);
            // lstContLnItem[0].EndDate = Date.today().addDays(10);
            // lstContLnItem[0].AssetId = lstAsset[0].Id;

            // lstContLnItem.add(TestFactory.createContractLineItem(lstServCon[1].Id, lstPriceBkEntry[1].Id, 5, 10));
            // lstContLnItem[1].VE__C = true;
            // // lstContLnItem[1].TotalPrice = 100;
            // lstContLnItem[1].StartDate = Date.today().addDays(-1);
            // lstContLnItem[1].EndDate = Date.today().addDays(10);
            // lstContLnItem[1].AssetId = lstAsset[1].Id;
            // insert lstContLnItem;

            // // create case
            // lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[0].Id));
            // lstCase[0].Service_Contract__c = lstServCon[0].Id;
            // lstCase[0].Type = 'Maintenance';
            // lstCase[0].Status = 'In Progress';
            // lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[1].Id));
            // lstCase[1].Service_Contract__c = lstServCon[1].Id;
            // insert lstCase;

            // // creating work type
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            // lstWrkTyp[0].Type__c = 'Commissioning';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType2', 'Hours', 1));
            // lstWrkTyp[1].Type__c = 'First Maintenance Visit (new contract)';
            // lstWrkTyp[1].Reason__c = 'Flat-rate visit';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType3', 'Hours', 1));
            // lstWrkTyp[2].Type__c = 'First Maintenance Visit (new contract)';
            // lstWrkTyp[2].Reason__c = 'First maintenance';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType4', 'Hours', 1));
            // lstWrkTyp[3].Type__c = 'Installation';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType5', 'Hours', 1));
            // lstWrkTyp[4].Type__c = 'Maintenance';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType6', 'Hours', 1));
            // lstWrkTyp[5].Type__c = 'Maintenance';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType7', 'Hours', 1));
            // lstWrkTyp[6].Type__c = 'Troubleshooting';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType8', 'Hours', 1));
            // lstWrkTyp[7].Type__c = 'Various Services';

            // insert lstWrkTyp;

            // // creating work order
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            // lstWrkOrd[0].ServiceTerritoryId = srvTerr.Id;
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[1].WorkTypeId = lstWrkTyp[1].Id;
            // lstWrkOrd[1].ServiceTerritoryId = srvTerr.Id;
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[2].WorkTypeId = lstWrkTyp[2].Id;
            // lstWrkOrd[2].ServiceTerritoryId = srvTerr.Id;
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[3].WorkTypeId = lstWrkTyp[3].Id;
            // lstWrkOrd[3].ServiceTerritoryId = srvTerr.Id;
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[4].WorkTypeId = lstWrkTyp[4].Id;
            // lstWrkOrd[4].ServiceTerritoryId = srvTerr.Id;
            // lstWrkOrd[4].CaseId = lstCase[0].Id;
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[5].WorkTypeId = lstWrkTyp[5].Id;
            // lstWrkOrd[5].ServiceTerritoryId = srvTerr.Id;
            // lstWrkOrd[5].CaseId = lstCase[1].Id;
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[6].WorkTypeId = lstWrkTyp[6].Id;
            // lstWrkOrd[6].ServiceTerritoryId = srvTerr.Id;
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[7].WorkTypeId = lstWrkTyp[7].Id;
            // lstWrkOrd[7].ServiceTerritoryId = srvTerr.Id;
            // insert lstWrkOrd;

            // // creating service appointment
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[0].Id));
            // lstServiceApp[0].ServiceTerritoryId = srvTerr.Id;
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[1].Id));
            // lstServiceApp[1].ServiceTerritoryId = srvTerr.Id;
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[2].Id));
            // lstServiceApp[2].ServiceTerritoryId = srvTerr.Id;
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[3].Id));
            // lstServiceApp[3].ServiceTerritoryId = srvTerr.Id;
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[4].Id));
            // lstServiceApp[4].ServiceTerritoryId = srvTerr.Id;
            // lstServiceApp[4].Status = 'In Progress'; // SH
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[5].Id));
            // lstServiceApp[5].ServiceTerritoryId = srvTerr.Id;
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[6].Id));
            // lstServiceApp[6].ServiceTerritoryId = srvTerr.Id;
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[7].Id));
            // lstServiceApp[7].ServiceTerritoryId = srvTerr.Id;
            
            // insert lstServiceApp;

        }
    }
    
    @isTest
    static void testRemoveLinkEquipementAndAccount(){
        System.runAs(mainUser){
            LC04_CreateContract.removeLinkEquipementAndAccount(lstAsset[1].Id, date.valueOf('2020-01-01'));
        }
    }
}