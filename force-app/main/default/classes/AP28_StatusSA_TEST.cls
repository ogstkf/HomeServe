/**
* @File Name          : AP28_StatusSA_TEST.cls
* @Description        : 
* @Author             : AMO
* @Group              : 
* @Last Modified By   : ARA
* @Last Modified On   : 04-05-2021
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0         22-10-2019     		    DMG         Initial Version
**/
@isTest
public with sharing class AP28_StatusSA_TEST {
    static User mainUser;
    static list <ServiceAppointment> sappLst;
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<Account> lstTestAcc = new List<Account>();
    static Product2 lstTestProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();    
    static List<Case> lstCase= new List<Case>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static Quote qte = new Quote();
    static Opportunity opp = new Opportunity();
    //Case status
    public static string caStatusInProg = 'In Progress';
    public static string caStatusPendingActFromCham = 'Pending Action from Cham';
    public static string caStatusPendingActFromClient = 'Pending Action from Client';
    
    
    //SA Status
    public static string saStatusDoneClientAbsent = 'Done client absent';
    //bypass
    static Bypass__c bp = new Bypass__c();
    
    static {
        mainUser = TestFactory.createAdminUser('AP14_SetCaseStatus@test.COM', 
                                               TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;
        
        System.runAs(mainUser){
            
            lstTestAcc.add(TestFactory.createAccount('testAcc'+1));
            lstTestAcc[0].BillingPostalCode = '1233456';
            lstTestAcc[0].recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert lstTestAcc;
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
            lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update lstTestAcc;
            //create products            
            Product2 lstTestProd = new Product2(Name = 'Product X',
                                                ProductCode = 'Pro-X',
                                                isActive = true,
                                                Equipment_family__c = 'Chaudière',
                                                Equipment_type__c = 'Chaudière gaz',
                                                Statut__c = 'Approuvée',
                                                RecordTypeId = AP_Constant.getRecTypeId('Product2', 'Article')
                                               );
            
            Product2 prodAsset = TestFactory.createProductAsset('asset');
            Product2 prodProd = TestFactory.createProduct('product');
            prodProd.ProductCode = 'P3R';

            insert lstTestProd;
            insert prodAsset;
			insert prodProd;
            
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );
            
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            
            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd.Id, 0));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prodProd.Id, 0));
            insert lstPrcBkEnt;
            
            //service contract           
            lstServCon.add(TestFactory.createServiceContract('testServCon', lstTestAcc[0].Id));
            lstServCon[0].Contract_Status__c =  'Cancelled';
            lstServCon[0].PriceBook2Id = lstPrcBk[0].Id;        
            insert lstServCon;   
            
            //contract line item
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));      
            lstConLnItem[0].Customer_Absences__c = 1;
            lstConLnItem[0].Customer_Allowed_Absences__c = 3;
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));      
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[1].Id, 150, 1));
			lstConLnItem[1].Remplacement_effectue__c = false;
			
            insert lstConLnItem;            
            
            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;
            
            //create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa;
            
            //create service territory
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;
            srvTerr = TestFactory.createServiceTerritory('test Territory',opHrs.Id,raisonSocial.Id);
            srvTerr.Sofactoapp_Raison_Social__c =sofa.Id; 
            srvTerr.Agency_switched_on_SF__c = true;
            insert srvTerr;
            
            //creating logement
            lstLogement.add(TestFactory.createLogement(lstTestAcc[0].Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = lstTestAcc[0].Id;
            insert lstLogement;
            
            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = prodAsset.Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = lstTestAcc[0].Id;
            insert lstAsset;
            
            // create case
            lstCase.add(TestFactory.createCase(lstTestAcc[0].Id, 'Troubleshooting', lstAsset[0].Id));
            lstCase[0].Contract_Line_Item__c = lstConLnItem[0].Id;
            //  lstCase.get(0).Origin = 'Cham Digital';
            lstCase.add(TestFactory.createCase(lstTestAcc[0].Id, 'Troubleshooting', lstAsset[0].Id));
            lstCase[1].Contract_Line_Item__c = lstConLnItem[1].Id;
            //  lstCase.get(1).Origin = 'Cham Digital';
            insert lstCase;
            
            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Commissioning';
            
            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].caseId = lstCase[0].Id;  
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].Type__c = 'Maintenance';
            lstWrkOrd.get(0).AssetId = lstAsset.get(0).Id;
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[1].caseId = lstCase[1].Id;  
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[1].Type__c = 'Installation';
            lstWrkOrd.get(1).AssetId = lstAsset.get(0).Id;
            insert lstWrkOrd;    
            //  ServiceAppointment sapy= new ServiceAppointment();
            // sapy.TECH_CaseOrigin__c
            // ServiceTerritory.Agency_switched_on_SF__c
            // TEXT(Work_Order__r.Case.Origin)
            
            
            sappLst = new List<ServiceAppointment>{
                new ServiceAppointment(
                    Status = 'None',                                            
                    ParentRecordId = lstWrkOrd[0].Id,                                         
                    EarliestStartTime = System.now(),
                    DueDate = System.now() + 30,
                    Work_Order__c = lstWrkOrd[0].Id    ,
                    ServiceTerritoryId =srvTerr.Id
                ),
				new ServiceAppointment(
					Status = 'None',                                            
					ParentRecordId = lstWrkOrd[1].Id,                                         
					EarliestStartTime = System.now(),
					DueDate = System.now() + 30,
					Work_Order__c = lstWrkOrd[1].Id ,
					
					ServiceTerritoryId =srvTerr.Id
				)
            };
                        
           	insert sappLst;
            
            opp = TestFactory.createOpportunity('Prestation panneaux',lstTestAcc[0].Id);
            opp.Pricebook2Id = lstPrcBk[0].Id;
            insert opp;
            
            qte = new Quote(  Name ='Prestations'
                            ,OpportunityId = opp.Id
                            ,Pricebook2Id = lstPrcBk[0].Id
                            ,Date_de_debut_des_travaux__c = System.today()
                            ,isSync__c = false
							,Contrat_de_service__c = lstServCon[0].Id
							,Equipement__c = lstAsset[0].Id
							,Status='In Review'
							,TECH_Devis_P3R__c = true
							,Devis_signe_par_le_client__c = false
							,Date_de_signature_du_client__c = System.today()
							,Ordre_d_execution__c = lstWrkOrd[0].Id
                            ,tech_deja_facture__c = true
                            ,Mode_de_paiement_de_l_acompte__c = 'CB'
                            ,Montant_de_l_acompte_verse__c=500
                           );
            
            insert qte;
        }
        
    }
    
    @isTest
    public static void testUpdateSAClose(){
        System.runAs(mainUser){
            
            Test.startTest();
            Set<Id> st = new Set<Id>();
            st.add(sappLst.get(0).Id);
            st.add(sappLst.get(1).Id);
            qte.Status = 'Validé, signé - en attente d\'intervention';
            update qte;
            AP28_StatusSA.updateSADoneOk(st);
            // sappLst[0].Status =null;
            //update sappLst[0];
            // sappLst[0].Status ='Done OK';
            // update sappLst[0];
            
            Test.stopTest();
            
        }
    }
    
    @isTest
    public static void testUpdateP3r(){
		qte.Status = 'Validé, signé et terminé';
        Test.startTest();
		AP28_StatusSA.updateCliP3r(new List<Quote>{qte});
		Test.stopTest();
    }
}