/**
 * @File Name          : AP16_CreateWoFromCase.cls
 * @Description        : Apex class to create WO From cases
 * @Author             : RRJ
 * @Group              :
 * @Last Modified By   : MNA
 * @Last Modified On   : 22-06-2021
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    09/10/2019   RRJ     Initial Version
 * 1.1    03/06/2020   DMU     Add comments on AgencySubSector__c due to Homeserve Project
 * 1.2    30/06/2020   DMU     Commented AP16_CreateWoFromCase.generateWoRetrofit due to homeserve project
 * 1.3    27/07/2020   DMU     Remove condition on Type/Reason in method generateWo
 * 1.4    12/08/2020   DMU     set field MaintenancePlanId & MaintenancePlanId - TEC-117/120
 **/
public without sharing class AP16_CreateWoFromCase {


	public static void generateWo(List<Case> lstCas, map<string,string> mapCaIdSCId){
        
        List<WorkOrder> lstWo = new List<WorkOrder>();
		Set<Id> setAstId = new Set<Id>();		
		map<String, list<MaintenancePlan>>mapSCMPlst = getMapSCMPlst(mapCaIdSCId);
        system.debug('*** mapSCMPlst: '+ mapSCMPlst.size());

		for(Case cas : lstCas) {
			setAstId.add(cas.AssetId);
		}
		Map<Id, Asset> mapAsset = new Map<Id, Asset>([SELECT Id, Logement__r.City__c, Logement__r.Country__c, Logement__r.Postal_Code__c, Logement__r.Agency__c, Logement__r.Street__c, Equipment_type_auto__c, Logement__r.AgencySubSector__c FROM Asset WHERE Id IN :setAstId]);
							
		for(Case cas : lstCas){
            lstWo.add(
                new WorkOrder(
                    AccountId = cas.AccountId,
                    Acknowledged_On__c = cas.CreatedDate,
                    AssetId = cas.AssetId,
                    CaseId = cas.Id,
                    ContactId = cas.ContactId,
                    Description = cas.Description,
                    Reason__c = cas.Reason__c,
                    ServiceContractId = cas.Service_Contract__c,
                    Status = 'New',
                    Subject = cas.Subject,
                    Type__c = cas.Type,
                    City = mapAsset.get(cas.AssetId).Logement__r.City__c,
                    Country = mapAsset.get(cas.AssetId).Logement__r.Country__c,
                    PostalCode = mapAsset.get(cas.AssetId).Logement__r.Postal_Code__c,
                    ServiceTerritoryId = mapAsset.get(cas.AssetId).Logement__r.Agency__c,
                    Street = mapAsset.get(cas.AssetId).Logement__r.Street__c,
                    Campagne__c = cas.Campagne__c,
                    MaintenancePlanId = mapSCMPlst.containsKey(cas.Service_Contract__c) ? mapSCMPlst.get(cas.Service_Contract__c)[0].Id : null,
                    SuggestedMaintenanceDate = mapSCMPlst.containsKey(cas.Service_Contract__c) ? system.Today() : null
                    // AgencySubSector__c = mapAsset.get(cas.AssetId).Logement__r.AgencySubSector__c   //03/06/2020 - DMU - Add comments due to Homeserve Project
                )
            );		
		}
		
		if(lstWo.size()>0){
			insert lstWo;
		}
	}

	public static void generateWoVePlanning(List<Case> lstCas, map<string,string> mapCaIdSCId){
        
        List<WorkOrder> lstWo = new List<WorkOrder>();
		Set<Id> setAstId = new Set<Id>();
		map<String, list<MaintenancePlan>>mapSCMPlst = getMapSCMPlst(mapCaIdSCId);
        system.debug('*** mapSCMPlst: '+ mapSCMPlst.size());    
		
		for(Case cas : lstCas) {
			setAstId.add(cas.AssetId);
		}
		Map<Id, Asset> mapAsset = new Map<Id, Asset>([SELECT Id, Logement__r.City__c, Logement__r.Country__c, Logement__r.Postal_Code__c, Logement__r.Agency__c, Logement__r.Street__c, Logement__r.AgencySubSector__c FROM Asset WHERE Id IN :setAstId]);
				
		for(Case cas : lstCas){
			lstWo.add(
				new WorkOrder(
					AccountId = cas.AccountId,
					Acknowledged_On__c = cas.CreatedDate,
					AssetId = cas.AssetId,
					CaseId = cas.Id,
					Description = cas.Description,
					Reason__c = cas.Reason__c,
					ServiceContractId = cas.Service_Contract__c,
					StartDate = cas.Start_Date__c,
					Status = 'New',
					Subject = cas.Subject,
					Type__c = cas.Type,
					VE_Planning__c = true,
					City = mapAsset.get(cas.AssetId).Logement__r.City__c,
					Country = mapAsset.get(cas.AssetId).Logement__r.Country__c,
					PostalCode = mapAsset.get(cas.AssetId).Logement__r.Postal_Code__c,
					ServiceTerritoryId = mapAsset.get(cas.AssetId).Logement__r.Agency__c,
					Street = mapAsset.get(cas.AssetId).Logement__r.Street__c,
					EndDate = cas.Due_Date__c != null ? DateTime.newInstance(cas.Due_Date__c.year(), cas.Due_Date__c.month(), cas.Due_Date__c.day()).addHours(21) : null,
					// AgencySubSector__c = mapAsset.get(cas.AssetId).Logement__r.AgencySubSector__c 
					ContactId = cas.ContactId,
					Campagne__c = cas.Campagne__c,
					MaintenancePlanId = mapSCMPlst.containsKey(cas.Service_Contract__c) ? mapSCMPlst.get(cas.Service_Contract__c)[0].Id : null,
					SuggestedMaintenanceDate =  mapSCMPlst.containsKey(cas.Service_Contract__c) ? system.Today() : null
				)
			);
		}
		if(lstWo.size()>0){
			insert lstWo;
		}
	}
	
    //DMU - 20200812 - TEC-117 - Addded method to assign case on workOrder
	public static void assigneCaseIdOnWO(map<string, string> mapWOIdCaseId){
		system.debug('** in setCaseIdOnWO');
		list<WorkOrder> woLstExisting = [SELECT id, caseId FROM WorkOrder where id IN: mapWOIdCaseId.KeySet() ];
		list<WorkOrder> WoLstToUpd = new list <WorkOrder>();
		for(WorkOrder wo: woLstExisting){
			wo.caseId = mapWOIdCaseId.get(wo.Id);
			WoLstToUpd.add(wo);
		}
		update WoLstToUpd;
	}
    
    //DMU - 20200812 - TEC-120 - Addded method to check existing MaintenancePlan MaintenanceAsset
    public static map<String, list<MaintenancePlan>> getMapSCMPlst(map<string,string> mapCaIdSCId){
		map<String, list<MaintenancePlan>>mapSCMPlst = new map<String, list<MaintenancePlan>>();

		for(ServiceContract sc : [SELECT id, (SELECT id FROM MaintenancePlans ORDER BY CreatedDate DESC limit 1) FROM ServiceContract WHERE id IN:mapCaIdSCId.Values() ]){
			if(sc.MaintenancePlans.size()>0){ 

				list <MaintenanceAsset> lstMA = [SELECT id FROM MaintenanceAsset where MaintenancePlanId =: sc.MaintenancePlans[0].Id ];
                if(lstMA.size()>0){
                    mapSCMPlst.put(sc.Id, sc.MaintenancePlans);
                }				
			}			 
		}
		return mapSCMPlst;
	}

	//DMU 20200630 Commented AP16_CreateWoFromCase.generateWoRetrofit due to homeserve project
	/*
	public static void generateWoRetrofit(List<Case> lstCas){
					System.debug('Enter AP16');
		Set<Id> setAstId = new Set<Id>();
		Set<Id> setAgFxdSlotId = new Set<Id>();
		for(Case cas : lstCas) {
			setAstId.add(cas.AssetId);
			setAgFxdSlotId.add(cas.Agency_fixed_slot__c);
		}
		Map<Id, Asset> mapAsset = new Map<Id, Asset>([SELECT Id, Logement__r.City__c, Logement__r.Country__c, Logement__r.Postal_Code__c, Logement__r.Agency__c, Logement__r.Street__c, Logement__r.AgencySubSector__c FROM Asset WHERE Id IN :setAstId]);
	
		Map<Id, Agency_Fixed_Slots__c> mapAgFxdSlot = new Map<Id, Agency_Fixed_Slots__c>([SELECT Id, Start_Time_of_the_slot__c, EndTime_of_the_slot__c FROM Agency_Fixed_Slots__c WHERE Id IN :setAgFxdSlotId]);

		List<WorkOrder> lstWo = new List<WorkOrder>();
		for(Case cas : lstCas){
			lstWo.add(
				new WorkOrder(
					Status = 'New',
					AccountId = cas.AccountId,
					Acknowledged_On__c = cas.CreatedDate,
					AssetId = cas.AssetId,
					CaseId = cas.Id,
					ContactId = cas.ContactId,
					Description = cas.Description,
					Reason__c = cas.Reason__c,
					ServiceContractId = cas.Service_Contract__c,
					Subject = cas.Subject,
					Type__c = cas.Type,					
					City = mapAsset.containskey(cas.AssetId) ? mapAsset.get(cas.AssetId).Logement__r.City__c : null,
                    Country = mapAsset.containskey(cas.AssetId) ? mapAsset.get(cas.AssetId).Logement__r.Country__c : null,
                    PostalCode = mapAsset.containskey(cas.AssetId) ? mapAsset.get(cas.AssetId).Logement__r.Postal_Code__c : null,
                    ServiceTerritoryId = mapAsset.containskey(cas.AssetId) ? mapAsset.get(cas.AssetId).Logement__r.Agency__c : null,
                    Street = mapAsset.containskey(cas.AssetId) ? mapAsset.get(cas.AssetId).Logement__r.Street__c : null,
					// StartDate = mapAgFxdSlot.get(cas.Agency_fixed_slot__c).Start_Time_of_the_slot__c,
					// EndDate = mapAgFxdSlot.get(cas.Agency_fixed_slot__c).EndTime_of_the_slot__c
					EndDate = cas.Origin == 'Cham Digital' ? (mapAgFxdSlot.containskey(cas.Agency_fixed_slot__c) ? mapAgFxdSlot.get(cas.Agency_fixed_slot__c).EndTime_of_the_slot__c: null) : cas.Origin == 'IZI' && cas.TECH_Dateheure_intervention_Izi__c != null ? cas.TECH_Dateheure_intervention_Izi__c.addHours(2) : null,
                    StartDate = cas.Origin == 'Cham Digital' ? (mapAgFxdSlot.containskey(cas.Agency_fixed_slot__c) ? mapAgFxdSlot.get(cas.Agency_fixed_slot__c).Start_Time_of_the_slot__c : null) : cas.Origin == 'IZI' ? cas.TECH_Dateheure_intervention_Izi__c : null
					// AgencySubSector__c = mapAsset.containskey(cas.AssetId) ? mapAsset.get(cas.AssetId).Logement__r.AgencySubSector__c : null 
				)
			);
		}
		if(lstWo.size()>0){
			insert lstWo;
		}
	}
	*/

	// MNA 20200212 TEC 493
	//#region
	public static List<Case> checkWorkType(List<Case> lstCas) {
        Set<String> types = new Set<String>();
        Set<String> reasons = new Set<String>();
		Set<String> equipementTypes = new Set<String>();
		List<Case> CasWithoutWT = new List<Case>();
		List<Case> lstCasToReturn = new List<Case>();
        

		Map<Id,Case> mapCas = new Map<Id,Case>(lstCas);
		Map<Id, String> mapCasEquipment = new Map<Id, String>();
		Map<Id, String> mapCasRecTypeDevName = new Map<Id, String>();
		map<string, string> mapCasTypeCollectif = new map<string,string>();
		Map<String, String> mapAgencyCodeToCasId = new Map<String, String>();

		List<Case> lstCasWithEquipment = [SELECT Id, Type, Reason__c, Type_de_collectif_WT__c, AssetId, Asset.Product2Id, Asset.Product2.Equipment_type1__c,
											RecordType.DeveloperName, Local_Agency__c, Local_Agency__r.Agency_Code__c
												FROM Case WHERE Id IN :mapCas.keySet()];
		
        for(Case cas : lstCasWithEquipment) {
            mapCasRecTypeDevName.put(cas.Id, cas.RecordType.DeveloperName);
			mapCasTypeCollectif.put(cas.Id, (cas.Type_de_collectif_WT__c == null) ? '' : cas.Type_de_collectif_WT__c);
			types.add(cas.Type);
			reasons.add(cas.Reason__c);
			if (cas.AssetId != null && cas.Asset.Product2Id != null && cas.Asset.Product2.Equipment_type1__c != null) {
				equipementTypes.add(cas.Asset.Product2.Equipment_type1__c);
				mapCasEquipment.put(cas.Id, cas.Asset.Product2.Equipment_type1__c);
			}
			if (cas.Local_Agency__c == null)
				mapAgencyCodeToCasId.put(cas.Id, null);
			else
				mapAgencyCodeToCasId.put(cas.Id, cas.Local_Agency__r.Agency_Code__c);
		}

		Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> workTypesMap = buildWorkTypesMap(types, reasons, equipementTypes );

		for (Case cas : lstCas) {
			if(	workTypesMap.containsKey(cas.Type) && workTypesMap.get(cas.Type).containsKey(cas.Reason__c) 																																	//Verification du Type et de la Raison		
					&& workTypesMap.get(cas.Type).get(cas.Reason__c).containsKey(mapCasEquipment.get(cas.Id))																																	//Verification du type d'Equipment
					&& (
					(workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).containsKey(mapAgencyCodeToCasId.get(cas.Id))																							//Verification de l'agence code
						&& (workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).get(mapAgencyCodeToCasId.get(cas.Id)).containsKey('Tous')																		//Verification du type de client
							|| (workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).get(mapAgencyCodeToCasId.get(cas.Id)).containsKey('Copro-Syndic') && mapCasTypeCollectif.get(cas.Id) == 'Collectif Privé')
							|| (workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).get(mapAgencyCodeToCasId.get(cas.Id)).containsKey('Bailleur') && mapCasTypeCollectif.get(cas.Id) == 'Collectif Public')
							|| (workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).get(mapAgencyCodeToCasId.get(cas.Id)).containsKey('Particulier') && mapCasTypeCollectif.get(cas.Id) == '')
						) 
					) ||
					(workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).containsKey('Toutes')																						
						&& (workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).get('Toutes').containsKey('Tous')																								//Verification du type de client
							|| (workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).get('Toutes').containsKey('Copro-Syndic') && mapCasTypeCollectif.get(cas.Id) == 'Collectif Privé')
							|| (workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).get('Toutes').containsKey('Bailleur') && mapCasTypeCollectif.get(cas.Id) == 'Collectif Public')
							|| (workTypesMap.get(cas.Type).get(cas.Reason__c).get(mapCasEquipment.get(cas.Id)).get('Toutes').containsKey('Particulier') && mapCasTypeCollectif.get(cas.Id) == '')
						) 
					) 
				)
			) {
					lstCasToReturn.add(cas);
			}
			else {
                if (mapCasRecTypeDevName.get(cas.Id) == 'Client_case' && !(cas.Origin == 'E-mail' && cas.Type == 'Information Request' && cas.Reason__c == null)) {
                    mapCas.get(cas.Id).addError('Ce type d’intervention n’est pas configuré pour votre société sur ce type d\'équipement.'
                       		+' Merci de choisir une combinaison de Type/motif différente ou de contacter votre administrateur pour créer le type d’intervention.');
			
                }
            }
		}
		return lstCasToReturn;
	}
	
    public static Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> buildWorkTypesMap(Set<String> types, Set<String> reasons, Set<String> equipementTypes){
        Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> ret = new Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>>();
        List<WorkType> workTypes= [
            SELECT Id, Type__c, Reason__c, Equipment_Type__c, DurationType, EstimatedDuration, Type_de_client__c, Agence__c 
            FROM WorkType
            WHERE Type__c = :types OR Reason__c = : reasons OR Equipment_type__c = :equipementTypes];
        for(WorkType wt : workTypes){
            if(!ret.containsKey(wt.Type__c))                                                                                                        //Type
                ret.put(wt.Type__c, new Map<String,Map<String,Map<String,Map<String, WorkType>>>>());
            if(!ret.get(wt.Type__c).containsKey(wt.Reason__c))                                                                                      //Reason
                ret.get(wt.Type__c).put(wt.Reason__c, new Map<String,Map<String,Map<String, WorkType>>>());
            if(!ret.get(wt.Type__c).get(wt.Reason__c).containsKey(wt.Equipment_Type__c))                                                            //Type d'Equipment
                ret.get(wt.Type__c).get(wt.Reason__c).put(wt.Equipment_Type__c, new Map<String,Map<String, WorkType>>());                           
            if (!ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).containsKey(wt.Agence__c))                                         //Code agence
                ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).put(wt.Agence__c, new Map<String, WorkType>());
            if(!ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).get(wt.Agence__c).containsKey(wt.Type_de_client__c))                //Type de Client
                ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).get(wt.Agence__c).put(wt.Type_de_client__c, wt);
            
        }

        return ret;
	}
	//#endregion
}