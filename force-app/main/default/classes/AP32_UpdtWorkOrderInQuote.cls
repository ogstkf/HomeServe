/**
 * @File Name          : AP32_UpdtWorkOrderInQuote.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/13/2020, 1:38:41 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/10/2019   AMO     Initial Version
**/
public with sharing class AP32_UpdtWorkOrderInQuote {
    public static void createQuote(List<Quote> lstNewQuote){

        Map<String, Opportunity> mapOpp = new Map<String, Opportunity>();
        List<Opportunity> lstopp = new List<Opportunity>();
        Set<Id> setOpp = new Set<Id>();

        for(Quote qu : lstNewQuote){
            setOpp.add(qu.OpportunityId);
        }

        for(Opportunity opp : [SELECT Id, Ordre_d_execution__c FROM Opportunity WHERE Id IN :setOpp]){
            mapOpp.put(opp.Id, opp);
        }
        
        for(Quote quote : lstNewQuote){
            System.debug('After Insert mapOpp.get(quote.OpportunityId)' + mapOpp.get(quote.OpportunityId));
            quote.Ordre_d_execution__c = mapOpp.get(quote.OpportunityId).Ordre_d_execution__c;
        }

    }

    public static void updateOpportunity(List<Opportunity> lstOppNew){
        System.debug('Handle After Update 1');
        Map<String, List<Quote>> mapOppQuote = new Map<String, List<Quote>>();
        List<Quote> lstQuoteUpdt = new List<Quote>();

        for(Quote qu : [SELECT Id, OpportunityId, Ordre_d_execution__c FROM Quote WHERE OpportunityId IN :lstOppNew AND RecordType.DeveloperName = 'Devis_standard']){
            if(mapOppQuote.containsKey(qu.OpportunityId)){
                mapOppQuote.get(qu.OpportunityId).add(qu);
            } else{
                mapOppQuote.put(qu.OpportunityId, new List<Quote> {qu});
            }
        }
        system.debug('Ally opp and quote ' + mapOppQuote);

        for(Opportunity opp : lstOppNew){
            if(mapOppQuote.containsKey(opp.Id)){
                for(Quote qu : mapOppQuote.get(opp.Id)){
                    qu.Ordre_d_execution__c = opp.Ordre_d_execution__c;
                    lstQuoteUpdt.add(qu);
                }
            }
        }
        if(lstQuoteUpdt.size()>0){
            update lstQuoteUpdt;
        }
        System.debug('Ally list to update ' + lstQuoteUpdt);
    }
}