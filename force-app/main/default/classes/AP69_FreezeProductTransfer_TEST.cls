/**
 * @File Name          : AP69_FreezeProductTransfer_TEST.cls
 * @Description        : 
 * @Author             : MDO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 24/04/2020, 10:36:42
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/03/2020         MDO                    Initial Version - CT-1417: Gestion des inventaires - freeze des mouvements de stocks pendant la période d'inventaire
**/
@isTest
private class AP69_FreezeProductTransfer_TEST {

    static User adminUser;
    static Account testAcc;
    static List<sofactoapp__Raison_Sociale__c> lstSofactos;
    static List<ServiceTerritory> lstAgences;
    static List<Schema.Location> lstLocations;
    static Product2 prod;
    static ProductItem prodItem = new ProductItem();
    
    static{
        adminUser = TestFactory.createAdminUser('AP69@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){

            //Create Account
            testAcc = TestFactory.createAccount('testAcc');
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            //Update Account
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //List of Sofactos
            lstSofactos = new List<sofactoapp__Raison_Sociale__c>{
                new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1234',
                  sofactoapp__Invoice_prefix__c = '2134'
                ),
                new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 2',
                  sofactoapp__Credit_prefix__c = '1342',
                  sofactoapp__Invoice_prefix__c = '2431'
                )    
            };
 
             insert lstSofactos;
 
             //Create Operating Hour
             OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
             insert OprtHour;
 
             //List of agences
             lstAgences = new List<ServiceTerritory>{
                 new ServiceTerritory(
                     Name = 'test agence 1',
                     Agency_Code__c = '0001',
                     IsActive = True,
                     Inventaire_en_cours__c = true,
                     OperatingHoursId = OprtHour.Id,
                     Sofactoapp_Raison_Social__c = lstSofactos[0].Id
                 ),
                 new ServiceTerritory(
                     Name = 'test agence 2',
                     Agency_Code__c = '0002',
                     IsActive = True,
                     Inventaire_en_cours__c = false,
                     OperatingHoursId = OprtHour.Id,
                     Sofactoapp_Raison_Social__c = lstSofactos[1].Id
                 )
             };
 
             insert lstAgences;
    
            //List of Locations
            lstLocations = new List<Schema.Location>{
                new Schema.Location(
                   LocationType = 'Virtual',
                   Name = 'loc 1',
                   IsInventoryLocation = true,
                   Agence__c = lstAgences[0].Id
                ),
                new Schema.Location(
                    LocationType = 'Entrepôt',
                    Name = 'loc 2',
                    IsInventoryLocation = true,
                    Agence__c = lstAgences[1].Id
                )
            };

            insert lstLocations; 

            //Create Product
            prod = TestFactory.createProduct('testprod');
            insert prod;

            //Create Product Item
            prodItem = new ProductItem(
                Product2Id = prod.Id,
                LocationId = lstLocations[1].Id,
                QuantityOnHand = 100,
                Quantite_inventaire__c = 50
            );
            insert prodItem;
        }
    }

    @isTest
    public static void testInsertProdTrnsfr(){
        System.runAs(adminUser){  

            //Create Product Transfer
            ProductTransfer prodTrans = new ProductTransfer(
                    SourceLocationId = lstLocations[0].Id,
                    DestinationLocationId = lstLocations[1].Id,
                    QuantitySent = 2,
                    QuantityReceived = 2,
                    IsReceived = true,
                    Inventaire_en_cours__c = false       
            );
      
            Test.startTest();        
                insert prodTrans;
            Test.stopTest();

            System.assertEquals(prodTrans.IsReceived, true);
            System.assertEquals(prodTrans.Inventaire_en_cours__c, false);

        }
    }

    @isTest
    public static void testUpdateProdTrnsfr(){
        System.runAs(adminUser){

            //Insert Product Transfer
             ProductTransfer prodTrans1 = new ProductTransfer(
                    SourceLocationId = lstLocations[0].Id,
                    DestinationLocationId = lstLocations[1].Id,
                    QuantitySent = 2,
                    QuantityReceived = 2,
                    IsReceived = true,
                    Inventaire_en_cours__c = false     
            );
            insert prodTrans1;
            
            //Update Product Transfer
            prodTrans1.IsReceived = false;
            prodTrans1.Inventaire_en_cours__c = true;

            Test.startTest();        
                update prodTrans1;
            Test.stopTest();

            System.assertEquals(prodTrans1.IsReceived, false);
            System.assertEquals(prodTrans1.Inventaire_en_cours__c, true);

        }

    }	

    @isTest
    public static void testUpdateAgence(){
        System.runAs(adminUser){   
            lstAgences[1].Inventaire_en_cours__c = true;

            Test.startTest();  
                update lstAgences[1];  
            Test.stopTest();      
        }
    }
}