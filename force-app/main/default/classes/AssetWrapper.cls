public class AssetWrapper{

    @AuraEnabled public Asset theAsset{get;set;}
    @AuraEnabled public String sharingLink{get;set;}
     
    public AssetWrapper(Asset theAsset, String sharingLink){
        this.theAsset = theAsset;
        this.sharingLink = sharingLink;
    }
}