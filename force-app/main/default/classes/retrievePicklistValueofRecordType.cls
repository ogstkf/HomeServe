public class retrievePicklistValueofRecordType{
     public String objectName;
     public String recordTypeName;
     public String fieldName;
     public List<String> listofValue;
     public retrievePicklistValueofRecordType() { 
         objectName = 'case'; 
         recordTypeName ='Client_case'; 
         fieldName = 'Type'; 
         listofValue = new List<String>(); 
     }     
     public void retrieveValue(){       
         listofValue=MetadataServiceExamples.getDependentPicklistValuesByRecordType(objectName,recordTypeName,fieldName);
         system.debug('*** listofValue: '+listofValue);
     }
}