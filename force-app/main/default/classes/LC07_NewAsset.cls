/**
 * @File Name          : LC07_NewAsset.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 24/10/2019, 10:34:07
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/10/2019   AMO     Initial Version
**/
public with sharing class LC07_NewAsset {
        
    @AuraEnabled
    public static Object fetchDefaultValues(String logementId){
        Id logId = (Id)logementId;
        Map<String, String> mapLgmnt = new Map<String, String>();

        Logement__c lgmnt = [SELECT Id, Account__c, Inhabitant__c FROM Logement__c WHERE Id = :logId LIMIT 1];
        mapLgmnt.put('AccountId', lgmnt.Inhabitant__c);
        mapLgmnt.put('Logement__c', lgmnt.Id);
        
        return mapLgmnt;
    }
}