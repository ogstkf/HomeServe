/**
 * @File Name          : LC12_QuoteTypeController.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 21-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/01/2020   RRJ     Initial Version
 * 1.1    21/07/2021   MNA     Add Sequence fields
**/
public class LC12_QuoteTypeController {
    
    @AuraEnabled
    public static Devis_type__c getNewDevisType(Id quoteId){
        Quote quote = [SELECT Id, Name, Agency__c, Description FROM Quote WHERE Id =: quoteId ];
        Devis_type__c dv = new Devis_type__c();
        dv.Name = quote.Name;
        dv.Description__c = quote.Description;
        dv.Agence__c = quote.Agency__c;
        
        return dv;
        
    }
    
    
    @AuraEnabled
    public static List<Lignes_de_devis_type__c> createLigneDevisType(Id devisTypeId, Id quoteId){
        
        List<Lignes_de_devis_type__c> result = new List<Lignes_de_devis_type__c>();
        List<QuoteLineItem> lines = [SELECT Id, Remise_en100__c,Remise_en_euros__c,Product2Id,Quantity,Description__c, Sequence__c FROM QuoteLineItem WHERE QuoteId =: quoteId];
        if(lines.size() > 0){
           // Integer count = 0;
            for(QuoteLineItem line : lines){
                Lignes_de_devis_type__c nn = new Lignes_de_devis_type__c();
                //nn.Name = System.currentTimeMillis() + '' + count;
                nn.Devis_type__c = devisTypeId;
                nn.Produit__c = line.Product2Id;
                nn.Quantity__c = line.Quantity;
                nn.Remise_en100__c = line.Remise_en100__c;
                nn.Remise_en_euros__c = line.Remise_en_euros__c;
                nn.Description__c = line.Description__c;
                nn.Sequence__c = line.Sequence__c;
                result.add(nn);
               // count++;
            }
            
            insert result;
        }
        
        return result;
        
       // insert devisType;
       // return devisType;
    }

}