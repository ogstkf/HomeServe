public class MakeInvoiceIssued {
    
    @InvocableMethod
    public static void InvoiceIssued(List<ID> recordIds){
        
            List <sofactoapp__Factures_Client__c> Factures = new  List <sofactoapp__Factures_Client__c>() ;       
            For (sofactoapp__Factures_Client__c Fact : [select id, sofactoapp__Date_de_facture__c from sofactoapp__Factures_Client__c 
                          where (Sofactoapp_Contrat_de_service__c in :recordIds )
                          and sofactoapp__Etat__c  = 'Brouillon' and (Sofactoapp_Contrat_de_service__c != null) ]) {
                
                Fact.sofactoapp__Date_de_facture__c = system.today(); 
                Fact.sofactoapp__Etat__c ='Emise';
                Factures.add(Fact);
            }
            try{
            System.debug('### in upd');
            update Factures;
            }catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
            }
        
         
   }
    
     //
  //  ! @future 
 //   public static void InvoiceIssuedFuture(List<ID> recordIds){
    
  //      List <sofactoapp__Factures_Client__c> Factures = new  List <sofactoapp__Factures_Client__c>() ;       
  //      For (sofactoapp__Factures_Client__c Fact : [select id, sofactoapp__Date_de_facture__c from sofactoapp__Factures_Client__c 
   //                       where (Sofactoapp_Contrat_de_service__c in :recordIds or sofactoapp__Opportunit__c  in :recordIds)
  //                        and sofactoapp__Etat__c  = 'Brouillon' and (Sofactoapp_Contrat_de_service__c != null or sofactoapp__Opportunit__c != null) ]) {
  //              
   //             Fact.sofactoapp__Date_de_facture__c = system.today(); 
   //             Fact.sofactoapp__Etat__c ='Emise';
    //            Factures.add(Fact);
  //      }
  //        try{
  //          System.debug('### in future update');
  //          update Factures;
  //          }catch(Exception ex){
 //           System.debug('*** error message: '+ ex.getMessage());
  //          }
    //}
}