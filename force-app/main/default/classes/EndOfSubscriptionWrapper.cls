public class EndOfSubscriptionWrapper{

    //DMU - 6/8/19 - Remove ServiceAppointment as parameter due to Necia retrofitting

    @AuraEnabled public Account theAccount{get; set;}
    @AuraEnabled public ServiceContract theContract{get; set;}
    @AuraEnabled public Case theCase{get; set;}
    
    public EndOfSubscriptionWrapper(Account theAccount, ServiceContract theContract, Case theCase){
        this.theAccount = theAccount;
        this.theContract = theContract;
        this.theCase = theCase;
    }
}