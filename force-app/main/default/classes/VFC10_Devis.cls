/**
 * @File Name          : VFC10_Devis.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 09-02-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0         27/10/2019               DMG                       Initial Version
 * 1.1         03/01/2020               DMG                       Include CGV-
 * 1.2         27/01/2020               DMG                       Logo Limit AgencyAccreditation__c 
 * 1.3         03/02/2020               DMG                       Contrat Type Collective 
 * 1.4         18/02/2020               DMG                       showOwnerLogement
**/
public with sharing class VFC10_Devis {
    public List<Quote> lstQuote {get;set;}
    public List<QuoteLineItem> lstQuoteLineItems {get;set;}
    public List<QuoteLineItem> lstQuoteLineItemsMEO {get;set;}
    public List<QuoteLineItem> lstQuoteLineItemsEP {get;set;}
    public List<AgencyAccreditation__c> lstAgencyAccreditations {get; set;}
    public List<AgencyAccreditation__c> lstAgencyAccALL {get; set;}
    public Quote actualQuote {get;set;}
    public Boolean showAddFacturation {get;set;}
    public Double totaltva10 {get;set;}
    public Double totaltva20 {get;set;}
    public Double totaltva55 {get;set;}
    public string idSignature {get;set;}
    public string CGV {get;set;}
    public Boolean UsageExclusif {get;set;}
    public Boolean UsageExclusif2 {get;set;}
    public Boolean RemiseEuro {get;set;}
    public Boolean RemisePercent {get;set;}
    public Integer CountRemiseEuro {get;set;}
    public Integer CountRemisePercent {get;set;}
    public Boolean hasAgencyAccreditations {get; set;}
    public Boolean isCollective {get; set;}
    public Boolean showOwnerLogement {get; set;}
    public Boolean hasAgencyCertifQualiba {get; set;}
    public String allNoQualibat {get; set;}
    public String cmpAddress {get; set;}

    //Added DMU 6/8/20
    public Boolean RemisePercent1 {get;set;}
    public Boolean RemiseEuro1 {get;set;}

    public class WrapperQLIs{
        public Boolean RemisePourcent {get;set;}
        public Boolean RemiseEuro {get;set;}
    }

    public VFC10_Devis(){
    System.debug('##Starting VFC10_Devis');

           try{
            string qId = ApexPages.currentPage().getParameters().get('id');
            showAddFacturation = false;
            totaltva10 = 0.00;
            totaltva20 = 0.00;
            totaltva55 = 0.00;
                if(String.isNotBlank(qId)){
                    lstQuote = new List<Quote>([SELECT Id,
                                                        Name,
                                                        QuoteNumber__c,
                                                        Montant_total_TTC_avant_aides__c,
                                                        Aide_CEE__c,
                                                        Total_HT_avant_aides__c, 
                                                        Devis_etabli_le__c,
                                                        ExpirationDate,
                                                        Montant_de_l_acompte_verse__c,
                                                        Acompte_verse__c,
                                                        Date_du_paiement_de_l_acompte__c,
                                                        Date_de_la_visite_technique__c,
                                                        Duree_de_validite_du_devis__c,
                                                        Objet_du_devis__c,
                                                        Agency__c,
                                                        Agency__r.Street,
                                                        Agency__r.Name,
                                                        Agency__r.Street2__c,
                                                        Agency__r.PostalCode,
                                                        Agency__r.City,
                                                        Agency__r.Phone__c,
                                                        Agency__r.Email__c,
                                                        Agency__r.Siret__c,
                                                        Agency__r.Intra_Community_VAT__c,
                                                        Agency__r.Agency_Manager__r.Name,
                                                        Agency__r.Agency_Manager_Phone__c,
                                                        Agency__r.Agency_Manager_E_mail__c,
                                                        Agency__r.Corporate_Name__c,
                                                        Agency__r.Legal_Form__c,
                                                        Agency__r.Capital_in_Eur__c,
                                                        Agency__r.APE__c,
                                                        Agency__r.Site_web__c,
                                                        Agency__r.Corporate_Street__c,
                                                        Agency__r.Corporate_Street2__c,
                                                        Agency__r.Corporate_ZipCode__c,
                                                        Agency__r.Corporate_City__c,
                                                        Agency__r.Siren__c,
                                                        Agency__r.RCS__c,
                                                        Agency__r.Professional_Indemnity_Insurance__c,
                                                        Agency__r.Decennal_Inurance__c,
                                                        Agency__r.Insurance_Cover__c,
                                                        Agency__r.IBAN__c,      
                                                        Agency__r.TECH_CapitalInEur__c,      
                                                        Agency__r.Logo_Name_Agency__c,                                            
                                                        Contrat_de_service__c,
                                                        Contrat_de_service__r.Name,
                                                        Contrat_de_service__r.Agency__r.Name,
                                                        Contrat_de_service__r.ContactId,
                                                        Contrat_de_service__r.Contact.Name,
                                                        Contrat_de_service__r.BillingStreet,
                                                        Contrat_de_service__r.BillingPostalCode,
                                                        Contrat_de_service__r.BillingCity,
                                                        Contrat_de_service__r.ContractNumber,
                                                        Contrat_de_service__r.AccountId,
                                                        Contrat_de_service__r.Account.Name,
                                                        Contrat_de_service__r.Account.Salutation,
                                                        Contrat_de_service__r.Type__c,
                                                        Contrat_de_service__r.RootServiceContract.Name,
                                                        Contrat_de_service__r.RootServiceContract.ContractNumber,
                                                        Logement__c,
                                                        Logement__r.Name,
                                                        Logement__r.Inhabitant__c,
                                                        Logement__r.Inhabitant__r.Name,
                                                        Logement__r.Inhabitant__r.Salutation,
                                                        Logement__r.Inhabitant__r.PersonMobilePhone,
                                                        Logement__r.Inhabitant__r.PersonEmail,
                                                        Logement__r.Account__r.PersonMobilePhone,
                                                        Logement__r.Account__r.PersonEmail,
                                                        Logement__r.Age__c,
                                                        Logement__r.Primary_Residence__c,
                                                        Logement__r.Professional_Use__c,
                                                        Logement__r.Nom_du_Collectif__c,
                                                        Logement__r.Lot__c,
                                                        Logement__r.Lot__r.Nom_du_Collectif__c,
                                                        Logement__r.Owner__c,
                                                        Logement__r.Owner__r.Name,
                                                        Logement__r.Owner__r.PersonMobilePhone,
                                                        Logement__r.Owner__r.PersonEmail,
                                                        Logement__r.Street__c,
                                                        Logement__r.Postal_code__c,
                                                        Logement__r.City__c,
                                                        Logement__r.Adress_complement__c,
                                                        Owner__c,
                                                        AccountId,
                                                        Account.Name,
                                                        Account.IsPersonAccount,
                                                        Account.BillingStreet,
                                                        Account.BillingPostalCode,
                                                        Account.BillingCity,
                                                        Account.ClientNumber__c,
                                                        Account.Salutation,
                                                        Nom_du_payeur__c,
                                                        Nom_du_payeur__r.Raison_sociale__c,
                                                        Nom_du_payeur__r.IsPersonAccount,
                                                        Nom_du_payeur__r.Name,
                                                        Nom_du_payeur__r.BillingStreet,
                                                        Nom_du_payeur__r.BillingPostalCode,
                                                        Nom_du_payeur__r.BillingCity,
                                                        Nom_du_payeur__r.ClientNumber__c,
                                                        Nom_du_payeur__r.Salutation,
                                                        Nom_du_payeur__r.Adress_complement__c,
                                                        Nom_du_payeur__r.Imm_Res__c,
                                                        Nom_du_payeur__r.Door__c,
                                                        Nom_du_payeur__r.Floor__c,
                                                        Equipement__r.IDEquipmenta__c,
                                                        Equipement__r.Power_in_kw__c,
                                                        ShippingStreet,
                                                        ShippingPostalCode,
                                                        ShippingCity,
                                                        Description,
                                                        Date_de_debut_des_travaux__c,
                                                        Duree_estimee_de_l_intervention__c,
                                                        Sous_traitant_Intermediaire__c,
                                                        Sous_traitant_SIRET__c,
                                                        Sous_traitant_RGE__c,
                                                        Prix_HT_avant_remise__c,
                                                        Remise_en_euro__c,
                                                        Prix_HT_apres_remise__c,
                                                        GrandTotal,
                                                        Montant_de_l_acompte_demande__c,
                                                        Mode_de_paiement_de_l_acompte__c,
                                                        Mode_de_paiement_du_solde__c,
                                                        Montant_du_solde__c,
                                                        Date_de_signature_du_client__c,
                                                        Signature_client__c,
                                                        Le_client_souhaite_conserver_les_pieces__c,
                                                        Devis_avec_sous_traitance_intermediaire__c,
                                                        Acompte_demande__c,
                                                        Prix_TTC__c,
                                                        Type_de_devis__c ,
                                                        TECH_RemiseAccordee__c,
                                                        EquipmentType__c,
                                                        RecordType.Name,
                                                        RecordTypeId,
                                                        RecordType.DeveloperName


                                                    FROM Quote 
                                                    WHERE Id = :qId]);
                    System.debug('**** lstQuote ' + lstQuote);
                    
                    if(lstQuote.size()>0){
                        actualQuote=lstQuote[0];
                        // System.debug('**** actualQuote.Equipement__r.IDEquipmenta__c ' + actualQuote.Equipement__r.IDEquipmenta__c);
                        // System.debug('**** actualQuote.Equipement__r.Power_in_kw__c ' + actualQuote.Equipement__r.Power_in_kw__c);

                        System.debug('**** adresse : ' +  actualQuote.Account.BillingStreet +  actualQuote.Account.BillingPostalCode +  actualQuote.Account.BillingCity);
                        System.debug('**** adresse facturation : ' + actualQuote.Contrat_de_service__r.BillingStreet + actualQuote.Contrat_de_service__r.BillingPostalCode + actualQuote.Contrat_de_service__r.BillingCity);
                        //check if addresse facturation == addresse
                        if((actualQuote.Account.BillingStreet +  actualQuote.Account.BillingPostalCode +  actualQuote.Account.BillingCity) != (actualQuote.Contrat_de_service__r.BillingStreet + actualQuote.Contrat_de_service__r.BillingPostalCode + actualQuote.Contrat_de_service__r.BillingCity)){
                            if(actualQuote.Contrat_de_service__r.BillingStreet!=null || actualQuote.Contrat_de_service__r.BillingPostalCode !=null || actualQuote.Contrat_de_service__r.BillingCity !=null){
                                showAddFacturation  = true;
                            }
                            System.debug('**** showAddFacturation : ' + showAddFacturation);
                        }

                        //cmpAddress
                        cmpAddress = actualQuote.Nom_du_payeur__r.Adress_complement__c!='' ? actualQuote.Nom_du_payeur__r.Adress_complement__c : '';
                        //(actualQuote.Nom_du_payeur__r.BillingPostalCode + ' ' + actualQuote.Nom_du_payeur__r.BillingCity);

                        //DMG 03/01/2020 - Check CGV
                        CGV = '';
                        if(actualQuote.Type_de_devis__c == 'Visite forfaitaire'){
                            CGV = 'CGV_VisiteforfaitaireCG';
                        }else if(actualQuote.Type_de_devis__c == 'Vente déquipement / Pose' || actualQuote.Type_de_devis__c == 'Mise en service' || actualQuote.Type_de_devis__c == 'Prestations' ){
                            CGV = 'CGV_PoseDepannageCG';
                        }


                        //DMG 03/02/2020 - Check contrat Collective
                        isCollective = false;
                        if(actualQuote.Contrat_de_service__c != null && actualQuote.Contrat_de_service__r.Type__c == 'Collective'){
                            isCollective = true;
                        }
                        System.debug('**** isCollective : ' + isCollective);

                        //DMG 18/02/2020 - showOwnerLogement
                        showOwnerLogement = false;
                        if(actualQuote.Nom_du_payeur__c != null && actualQuote.Owner__c != null){
                            if(actualQuote.Nom_du_payeur__c != actualQuote.Owner__c){
                                showOwnerLogement = true;
                            }
                        }
                        System.debug('**** showOwnerLogement : ' + showOwnerLogement);

                        //DMG08/01/2020 - UsageExclusif Case à cocher
                        if(actualQuote.Logement__r.Professional_Use__c=='Non (moins de 50% d\'usage professionnel)'){
                            UsageExclusif = true;
                        }else if(actualQuote.Logement__r.Professional_Use__c=='Oui (plus de 50% d\'usage professionnel)'){
                            UsageExclusif2 = true;
                        }   
                        System.debug('**** UsageExclusif : ' + UsageExclusif);
                        lstQuoteLineItems = [Select Id,
                                                    Product2.Reference_anonymisee__c,
                                                    Product2.Nom_de_l_article__c,
                                                    Product2.Name,
                                                    Product2.ProductCode,
                                                    Product2.Description,
                                                    Product2.Designation_complementaire__c,
                                                    Product2.Eco_contribution__c,
                                                    Quantity,
                                                    ListPrice,
                                                    UnitPrice, //Modification 13/02 - ajout du UnitPrice en lieu et place du ListPrice
                                                    Remise_en100__c,
                                                    Remise_en_euros__c,
                                                    Prix_de_vente_apres_remise__c,
                                                    Taux_de_TVA__c,
                                                    Prix_TTC__c,
                                                    Description,
                                                    Prix_de_vente_force__c,
                                                    Sequence__c,
                                                    Description__c
                                                from QuoteLineItem  
                                                where QuoteId =: lstQuote[0].Id order by Sequence__c asc NULLS LAST, Prix_TTC__c desc
                                            ];
                         System.debug('**** lstQuoteLineItems ' + lstQuoteLineItems);

                         //added dmu 6/8/20
                         system.debug('***dmu RemiseEuro1: '+RemiseEuro1);
                         system.debug('***dmu RemisePercent1: '+RemisePercent1);
                         RemiseEuro1 = false;
                         RemisePercent1 = false;
                         list<decimal> lstRemisePercent = new list<decimal>();
                         list<decimal> lstRemiseEuro = new list<decimal>();
                         for(QuoteLineItem qu : lstQuoteLineItems){
                             if(qu.Remise_en100__c <> null && qu.Remise_en100__c <> 0.00 ){
                                 lstRemisePercent.add(qu.Remise_en100__c);
                             }
                             if(qu.Remise_en_euros__c <> null && qu.Remise_en_euros__c <> 0.00){
                                 lstRemiseEuro.add(qu.Remise_en_euros__c);
                             }
                         }
                         system.debug('***dmu lstRemisePercent:'+lstRemisePercent);
                         system.debug('***dmu lstRemiseEuro: '+lstRemiseEuro);
                         if(lstRemisePercent.size()>0){
                            system.debug('***dmu enter RemisePercent1');                            
                            RemisePercent1 = true;
                         }
                         if(lstRemiseEuro.size()>0){
                            system.debug('***dmu enter RemiseEuro1');
                            RemiseEuro1 = true;                            
                         }
                         system.debug('***dmu RemiseEuro1 2: '+RemiseEuro1);
                         system.debug('***dmu RemisePercent1 2: '+RemisePercent1);

                         List<String> lstProductCode = new List<String>();
                         Set<String> SetProductCode = new Set<String>();
                         lstQuoteLineItemsMEO = new List <QuoteLineItem>();
                         lstQuoteLineItemsEP = new List <QuoteLineItem>();
                         lstProductCode = System.label.VFC10_Devis_ProductCode_MEO.split(';');
                         for(String s : lstProductCode){
                            SetProductCode.add(s);
                         }
                         System.debug('**** SetProductCode ' + SetProductCode);
                         RemiseEuro = true;
                         RemisePercent = true;
                         CountRemiseEuro = 0;
                         CountRemisePercent = 0;
                         for(QuoteLineItem qli :lstQuoteLineItems){
                            if(qli.Taux_de_TVA__c == 10){
                                totaltva10 = totaltva10 + (qli.Prix_de_vente_apres_remise__c * 0.10);
                            }else if(qli.Taux_de_TVA__c == 20){
                                totaltva20 = totaltva20 + (qli.Prix_de_vente_apres_remise__c * 0.20);
                            }
                            else if(qli.Taux_de_TVA__c == 5.5){
                                totaltva55 = totaltva55 + (qli.Prix_de_vente_apres_remise__c * 0.055);
                            }
                            System.debug('**** qli.Product2.ProductCode ' + qli.Product2.ProductCode);
                            // separate Equipement(s) et produit(s) & Mise en œuvre
                            if(!SetProductCode.contains(qli.Product2.ProductCode)){
                                 lstQuoteLineItemsEP.add(qli);
                            }else{
                                lstQuoteLineItemsMEO.add(qli);
                            }
                            //DMG 10/01/2020 - Check If RemiseEuro   
                            if(qli.Remise_en_euros__c==null || qli.Remise_en_euros__c==0 ){
                                CountRemiseEuro = CountRemiseEuro + 1;
                            }
                            System.debug('**** qli.Product2.ProductCode ' + qli.Product2.ProductCode);
                            if(qli.Remise_en100__c==null || qli.Remise_en100__c==0 ){
                                 CountRemisePercent = CountRemisePercent + 1;
                            }
                            System.debug('**** qli.Remise_en100__c: ' + qli.Remise_en100__c);
                            System.debug('**** qli.Remise_en_euros__c: ' + qli.Remise_en_euros__c);
                         }
                        if(CountRemiseEuro ==lstQuoteLineItems.size() ){
                            RemiseEuro = false;
                        }
                        if(CountRemisePercent ==lstQuoteLineItems.size() ){
                            RemisePercent = false;
                        }

                         System.debug('**** totaltva10: ' + totaltva10);
                         System.debug('**** totaltva20: ' + totaltva20);
                         System.debug('**** totaltva55: ' + totaltva55);
                         System.debug('**** lstQuoteLineItemsMEO: ' + lstQuoteLineItemsMEO);
                         System.debug('**** lstQuoteLineItemsEP: ' + lstQuoteLineItemsEP);
                         System.debug('**** RemisePercent: ' + RemisePercent);
                         System.debug('**** RemiseEuro: ' + RemiseEuro);
                        List<Attachment> lstAttachmentClient = new List<Attachment>();
                        lstAttachmentClient = [select Id,Name from Attachment where ParentId=:actualQuote.Id  and  Name='SignatureClient.png' order by createdDate desc];
                        if(lstAttachmentClient.size() > 0){
                            idSignature = '/servlet/servlet.FileDownload?file='+lstAttachmentClient[0].id;
                        }else{
                            idSignature = 'null';
                        }
                        //YGO 16.01.2020
                        hasAgencyAccreditations = false;
                        lstAgencyAccALL = [SELECT Agency__c, 
                                                        Eligibility__c, 
                                                        Type__c,
                                                        Numero_SGS__c,
                                                        Equipement_couvert__c 
                                                    FROM AgencyAccreditation__c 
                                                    WHERE Agency__c = :actualQuote.Agency__c
                                                    AND (Eligibility__c = 'Oui propre' OR Eligibility__c = 'Oui déléguée')];
                        // if(lstAgencyAccALL.size()>0){
                        //     lstAgencyAccreditations =lstAgencyAccALL ;
                        //     hasAgencyAccreditations = true;
                        // }
                        //DMG 27/01/2020 - Limit AgencyAccreditation__c : new requirements
                        system.debug('**** actualQuote.EquipmentType__c '+ actualQuote.EquipmentType__c);
                        lstAgencyAccreditations = new List<AgencyAccreditation__c>();
                        for(AgencyAccreditation__c aa : lstAgencyAccALL){
                            system.debug('**** Has Equipement_couvert__c : '+aa.Equipement_couvert__c);
                            system.debug('**** Has actualQuote : '+actualQuote.EquipmentType__c);
                           if(aa.Equipement_couvert__c != null && actualQuote.EquipmentType__c!=null){
                                if(aa.Equipement_couvert__c.contains(actualQuote.EquipmentType__c)){
                                    lstAgencyAccreditations.add(aa);
                                    system.debug('**** Has Equipement_couvert__c : '+aa.Equipement_couvert__c);
                                }else{
                                    system.debug('**** Has Equipement_couvert__c But no Match : '+aa.Equipement_couvert__c);
                                }
                            }else{
                                system.debug('**** No Equipement_couvert__c');
                                lstAgencyAccreditations.add(aa);
                            }
                            
                        }
                        system.debug('**** lstAgencyAccreditations IN ' +lstAgencyAccreditations);

                        allNoQualibat ='';
                        if(lstAgencyAccreditations.size()>0){
                            hasAgencyAccreditations = true;
                            //DMG 18/02/2020 - Add Agence certifiée RGE Qualibat
                            for(AgencyAccreditation__c a :lstAgencyAccreditations){
                                 system.debug('**** a.Type__c ' +a.Type__c);
                                if(a.Type__c == 'RGE Chaudière' || a.Type__c == 'RGE PAC'  ){
                                    allNoQualibat = allNoQualibat + a.Numero_SGS__c + ', ';
                                    system.debug('**** allNoQualibat IN ' +allNoQualibat);
                                } 
                            }
                        }
                        if(allNoQualibat!= ''){
                             system.debug('**** allNoQualibat ' +allNoQualibat);
                            allNoQualibat = allNoQualibat.removeEnd(', ');
                            system.debug('**** allNoQualibat2 ' +allNoQualibat);
                            hasAgencyCertifQualiba = true;
                        }


                    }
                }  
            }catch(exception e){
                system.debug('**** exception msg: '+ e.getMessage());
            }

    }
    
    
}