/**
 * @File Name          : AP63_UpdateActifVE_TEST.cls
 * @Description        : 
 * @Author             : DMU
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 11-20-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/12/2019         DMU                    Initial Version
**/

@isTest
public class AP63_UpdateActifVE_TEST {

    static User mainUser;
    static Account testAcc = new Account();
    static Opportunity opp = new Opportunity();
    static list<Product2> lstProd = new list<Product2>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();
    static ServiceContract sc = new ServiceContract();
    

    static{

        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            //Create Account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            //Update Account
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //Create Opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);            
            insert opp;

            //Create Products
            lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                
                new Product2(Name='Prod2',
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
            };
            insert lstProd;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create Pricebook Entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            PrcBkEnt.Product2Id = lstProd[0].Id;
            insert PrcBkEnt;
            system.debug('**PrcBkEnt.Id: '+PrcBkEnt.Id);
            system.debug('** PrcBkEnt.Product2Id: '+ PrcBkEnt.Product2Id);
            
            sc = TestFactory.createServiceContract('testServCon', testAcc.Id);
            sc.Pricebook2Id = lstPrcBk[0].Id;
            insert sc;
            
            //contract line item
            for(Integer i=0; i<2; i++){
                lstConLnItem.add(TestFactory.createContractLineItem(sc.Id, PrcBkEnt.Id, 150, 1));
            }
            insert lstConLnItem;
            
            system.debug('**lstProd[0].Id: '+lstProd[0].Id);
            system.debug('**lstConLnItem[0].PricebookEntryId: '+lstConLnItem[0].PricebookEntryId);
            system.debug('**lstConLnItem[0].PricebookEntry.Product2Id: '+lstConLnItem[0].PricebookEntry.Product2Id);

        }

    }

    @isTest
    public static void checkPriceBookEntry_TEST(){
       System.runAs(mainUser){

            //List<Product2> lstProductTocreate = new List<Product2>();

            Test.StartTest();
               AP63_UpdateActifVE.updateActifVEFromCLI(lstProd); 
            Test.StopTest();

            //lstProductTocreate = [SELECT Id, Name FROM Product2 WHERE Id =: lstProd[1].Id];
            //System.assertEquals(lstProductTocreate.size(),1);

       } 
    } 

}