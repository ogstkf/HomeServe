/**
 * @File Name          : LC07_NewAssetTEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 04/02/2020, 21:41:22
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/10/2019   AMO     Initial Version
**/
@isTest
public with sharing class LC07_NewAssetTEST {

    static User mainUser;
    static list <Logement__c> lstlgmnt;
    static List<Account> lstAccount;
    static List<Asset> lstAsset;
    static List<ServiceTerritory> lstServTerritory;
    static OperatingHours opHrs = new OperatingHours();
    static sofactoapp__Raison_Sociale__c raisonSocial;

    static{
            mainUser = TestFactory.createAdminUser('LC07_NewAsset@test.COM', 
												TestFactory.getProfileAdminId());
            
            insert mainUser;

            System.runAs(mainUser){

            lstAccount = new List<Account>{
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='John')
            };

            insert lstAccount;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAccount.get(0).Id);
			lstAccount.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update lstAccount;

            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;

            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            lstServTerritory = new list<ServiceTerritory>{
                TestFactory.createServiceTerritory('SGS - test Territory', opHrs.Id, raisonSocial.Id)
            };
            insert lstServTerritory;

            lstlgmnt = new list<Logement__c>{
                new Logement__c(Account__c =lstAccount[0].Id, Inhabitant__c = lstAccount[0].Id)
            };

            lstlgmnt[0].City__c='Cityy1';
            lstlgmnt[0].Postal_Code__c='1234';
            lstlgmnt[0].Street__c='Royale Road';
            lstlgmnt[0].Agency__c=lstServTerritory[0].Id;
            
            insert lstlgmnt;

            }        
    }

    static testMethod void checkUpdatedAsset(){   
		
		system.runAs(mainUser){

            Test.startTest();
                LC07_NewAsset.fetchDefaultValues(lstlgmnt[0].Id);
            Test.stopTest();
        }
    }
}