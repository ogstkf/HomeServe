/**
 * @File Name          : AP34_CreateProductConsumed_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-05-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/04/2020   ZJO     Initial Version
**/

@isTest
public with sharing class AP34_CreateProductConsumed_TEST {
    static User adminUser;
    static List<Account> lstAccounts;
    static List<Product2> lstProducts;
    static List<ProductRequired> lstProdReq;
    static List<sofactoapp__Raison_Sociale__c> lstSofactos;
    static List<ServiceTerritory> lstAgences;
    static List<Schema.Location> lstLocations;
    static List<Logement__c> lstLogements;
    static List<Asset> lstAssets;
    static List<Case> lstCases;
    static List<WorkOrder> lstWorkOrders;
    static List<ServiceAppointment> lstSAs;
    static List<ProductItem> lstProdItems;
    //bypass
    static Bypass__c bp = new Bypass__c();
    

    static{
        adminUser = TestFactory.createAdminUser('AP34@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(adminUser){

            //List of Accounts
            lstAccounts = new List<Account>{
                TestFactory.createAccount('Acc 1'),
                TestFactory.createAccount('Acc 2'),
                TestFactory.createAccount('Acc 3')
            };
            insert lstAccounts;

            //List of Products
            lstProducts = new List<Product2>{
                TestFactory.createProduct('prod 1'),
                TestFactory.createProduct('prod 2'),
                TestFactory.createProduct('prod 3')
            };
            // lstProducts[1].ProductCode =
            insert lstProducts;

            //List of Sofactos
            lstSofactos = new List<sofactoapp__Raison_Sociale__c>{
                new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1234',
                  sofactoapp__Invoice_prefix__c = '2134'
                ),
                new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1342',
                  sofactoapp__Invoice_prefix__c = '2431'
                ),
                new sofactoapp__Raison_Sociale__c(
                  Name = 'test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1432',
                  sofactoapp__Invoice_prefix__c = '2341'
                )
             };
 
             insert lstSofactos;
 
             //Create Operating Hour
             OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
             insert OprtHour;
 
             //List of agences
             lstAgences = new List<ServiceTerritory>{
                 new ServiceTerritory(
                     Name = 'test agence 1',
                     Agency_Code__c = '0001',
                     IsActive = True,
                     Inventaire_en_cours__c = false,
                     OperatingHoursId = OprtHour.Id,
                     Sofactoapp_Raison_Social__c = lstSofactos[0].Id
                 ),
                 new ServiceTerritory(
                     Name = 'test agence 2',
                     Agency_Code__c = '0002',
                     IsActive = True,
                     Inventaire_en_cours__c = false,
                     OperatingHoursId = OprtHour.Id,
                     Sofactoapp_Raison_Social__c = lstSofactos[1].Id
                 ),
                 new ServiceTerritory(
                     Name = 'test agence 3',
                     Agency_Code__c = '0003',
                     IsActive = True,
                     Inventaire_en_cours__c = false,
                     OperatingHoursId = OprtHour.Id,
                     Sofactoapp_Raison_Social__c = lstSofactos[2].Id
                 )
             };
 
             insert lstAgences;

               //List of Locations
            lstLocations = new List<Schema.Location>{
                new Schema.Location(
                   LocationType = 'Véhicule',
                   Name = 'loc 1',
                   IsInventoryLocation = True,
                   Agence__c = lstAgences[0].Id
                ),
                new Schema.Location(
                    LocationType = 'Entrepôt',
                    Name = 'loc 2',
                    IsInventoryLocation = True,
                    Agence__c = lstAgences[1].Id
                ),
                new Schema.Location(
                    LocationType = 'Virtual',
                    Name = 'loc 3',
                    IsInventoryLocation = True,
                    Agence__c = lstAgences[2].Id
                 )
            };

            insert lstLocations;

            //List of Logements
            lstLogements = new List<Logement__c>{
                TestFactory.createLogement(lstAccounts[0].Id, lstAgences[0].Id),
                TestFactory.createLogement(lstAccounts[1].Id, lstAgences[1].Id),
                TestFactory.createLogement(lstAccounts[2].Id, lstAgences[2].Id)
           };

           insert lstLogements;

           //List of Assets
           lstAssets = new List<Asset>{
              new Asset (
                  Name = 'equipment 1',
                  Status = AP_Constant.assetStatusActif,
                  Logement__c = lstLogements[0].Id,
                  AccountId = lstAccounts[0].Id
              ),
              new Asset (
                  Name = 'equipment 2',
                  Status = AP_Constant.assetStatusActif,
                  Logement__c = lstLogements[1].Id,
                  AccountId = lstAccounts[1].Id
              ),
              new Asset (
                  Name = 'equipment 3',
                  Status = AP_Constant.assetStatusActif,
                  Logement__c = lstLogements[2].Id,
                  AccountId = lstAccounts[2].Id
              )   
           };

           insert lstAssets;

           //List of Cases
           lstCases = new List<Case>{
               TestFactory.createCase(lstAccounts[0].Id, 'Troubleshooting', lstAssets[0].Id),
               TestFactory.createCase(lstAccounts[0].Id, 'Troubleshooting', lstAssets[0].Id),
               TestFactory.createCase(lstAccounts[0].Id, 'Troubleshooting', lstAssets[0].Id)
           };

           insert lstCases;

             //List of WorkOrders
             lstWorkOrders = new List<WorkOrder>{
                new WorkOrder(
                   caseId = lstCases[0].Id,
                   LocationId = lstLocations[0].Id,
                   ServiceTerritoryId = lstAgences[0].Id,
                   Status = AP_Constant.wrkOrderStatusNouveau,
                   Priority = AP_Constant.wrkOrderStatusInProgress
                ),
                new WorkOrder(
                   caseId = lstCases[1].Id,
                   LocationId = lstLocations[1].Id,
                   ServiceTerritoryId = lstAgences[1].Id,
                   Status = AP_Constant.wrkOrderStatusNouveau,
                   Priority = AP_Constant.wrkOrderStatusInProgress
                ),
                new WorkOrder(
                   caseId = lstCases[2].Id,
                   LocationId = lstLocations[2].Id,
                   ServiceTerritoryId = lstAgences[2].Id,
                   Status = AP_Constant.StatusClosed,
                   Priority = AP_Constant.wrkOrderStatusInProgress
                )
           };

           insert lstWorkOrders;

           //List of Products Required
           lstProdReq = new List<ProductRequired>{
               new ProductRequired(
                ParentRecordId = lstWorkOrders[0].Id,
                Product2Id =  lstProducts[0].Id, 
                Quantite_utilisee__c = 2,
                APP_Checked__c = true
               ),
               new ProductRequired(
                ParentRecordId = lstWorkOrders[1].Id,
                Product2Id =  lstProducts[1].Id, 
                Quantite_utilisee__c = 3,
                APP_Checked__c = true
               ),
               new ProductRequired(
                ParentRecordId = lstWorkOrders[2].Id,
                Product2Id =  lstProducts[2].Id, 
                Quantite_utilisee__c = 1,
                APP_Checked__c = true
               )
            };
            insert lstProdReq;

            //List of Product Items
            lstProdItems = new List<ProductItem>{
                new ProductItem(
                    Product2Id = lstProducts[0].Id,
                    LocationId = lstLocations[0].Id,        
                    QuantityOnHand = decimal.valueOf(2)
                ),
                new ProductItem(
                    Product2Id = lstProducts[1].Id,
                    LocationId = lstLocations[1].Id,
                    QuantityOnHand = decimal.valueOf(2)
                ),
                new ProductItem(
                    Product2Id = lstProducts[2].Id,
                    LocationId = lstLocations[2].Id,
                    QuantityOnHand = decimal.valueOf(2)
                )
            };
            insert lstProdItems;

           //List of Service Appointments
           lstSAs = new List<ServiceAppointment>{
            new ServiceAppointment(
                Status = 'In Progress',
                ParentRecordId = lstWorkOrders[0].Id,
                Work_Order__c = lstWorkOrders[0].Id,
                ServiceTerritoryId = lstAgences[0].Id,
                Inventaire_en_cours__c = True
            ),
            new ServiceAppointment(
                Status = 'In Progress',
                ParentRecordId = lstWorkOrders[1].Id,
                Work_Order__c = lstWorkOrders[1].Id,
                ServiceTerritoryId = lstAgences[1].Id,
                Inventaire_en_cours__c = True
            ),
            new ServiceAppointment(
                Status = 'In Progress',
                ParentRecordId = lstWorkOrders[2].Id,
                Work_Order__c = lstWorkOrders[2].Id,
                ServiceTerritoryId = lstAgences[2].Id,
                Inventaire_en_cours__c = True
            )
        };

        insert lstSAs;
            
        }
    }

    @isTest 
    public static void testCreateProductConsumed(){

        System.runAs(adminUser){

            Set<Id> setWOIds = new Set<Id>();
            Map<Id, Set<Id>> mapWotoSas = new Map<Id, Set<Id>>();

            //Populate Set and Map with Values
            for (ServiceAppointment sa : lstSAs){
            setWOIds.add(sa.Work_Order__c);

                If (mapWotoSas.containsKey(sa.Work_Order__c)){
                    mapWotoSas.get(sa.Work_Order__c).add(sa.Id);
                }else{
                    mapWotoSas.put(sa.Work_Order__c, new Set<Id> {sa.Id});
                }
            }

            Test.startTest();
               AP34_CreateProductConsumed.createProdConsumed(lstSAs, setWOIds, mapWotoSas);
            Test.stopTest(); 

            List<ProductConsumed> lstProdConsumed = [SELECT Id, WorkOrderId, ProductItemId, QuantityConsumed From ProductConsumed WHERE WorkOrderId IN: setWOIds];
            system.assertEquals(lstProdConsumed.size(), 3);
        }

    }
}