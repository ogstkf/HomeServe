/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 07-05-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-05-2021   MNA   Initial Version
**/
public with sharing class WS13_EnqueueTest implements Queueable{
    String Id = '';
    public WS13_EnqueueTest(String Id) {
        this.Id = Id;
    }
    public void execute(QueueableContext qc){          
		WS13_ChaineEditiquev2.createJSON(Id, true, true, false, 'DEVIS_TCH', null, true);
    }
}
/**
 * List<ServiceContract> lstSc = [SELECT Id FROM ServiceContract ORDER BY CreatedDate ASC];
    for (Integer i = 250; i < 300 ; i++) {
        System.enqueueJob(new WS13_EnqueueTest(lstSc[i].Id));
    }
 */