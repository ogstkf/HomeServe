/**
* @File Name          : ContractLineItemTriggerHandler.cls
* @Description        : Trigger handler for contract line item
* @Author             : ZJO
* @Group              : Spoon Consulting
* @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
* @Last Modified On   : 24-12-2021
* @Modification Log   : 
*==============================================================================
* Ver         Date                     Author                 Modification
*==============================================================================
* 1.0    02/07/2019, 14:11:26          RRJ                    Initial Version
* 1.1    03/06/2020, 12:48:00          DMU                    Commented code related to AP67_updateCLIVEStatus due to HomeServe Project
* 1.2    17/08/2020                    AMO                    Commented AP53 - TEC-123
* 1.3    18/08/2020                    ZJO                    TEC-119
**/
public class ContractLineItemTriggerHandler {

    private Bypass__c userBypass;

    public ContractLineItemTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    /**
    * @description After insert method. Handles all after insert calls
    * @author RRJ | 02/07/2019
    * @param List<ContractLineItem> lstNewConLineItem
    * @return void
    */
    public void handleAfterInsert(List<ContractLineItem> lstNewConLineItem){
        
        Set<Id> setCliColIdAP80 = new Set<Id>();//Tec-888

        Map<Id, ContractLineItem> mapProdIdToConLineItem = new Map<Id, ContractLineItem>();
        list<ContractLineItem> lstCLI = new list <ContractLineItem>();
        List<ContractLineItem> lstCliVE2 = new List<ContractLineItem>(); 

        List<ContractLineItem> lstCliCollective = new List<ContractLineItem>();//Tec-119
        
        Map<Id, String> mapCliProCode = new Map<Id, String>();
        
        for(ContractLineItem cli : [SELECT Id, Product2.ProductCode FROM ContractLineItem WHERE Id IN :(new Map<Id,ContractLineItem>(lstNewConLineItem)).KeySet()]) {
            mapCliProCode.put(cli.Id, cli.Product2.ProductCode);
        }

        for(Integer i=0; i<lstNewConLineItem.size(); i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP09')){
                if(String.isNotBlank(lstNewConLineItem[i].Product2Id ) 
                    && !lstNewConLineItem[i].isClone() 
                    && !lstNewConLineItem[i].TECH_DoNotPopulateBundle__c // RRJ 23/12/2019 added to prevent trigger from launching when creating from wizard CT-1228
                ){
                    mapProdIdToConLineItem.put(lstNewConLineItem[i].Product2Id, lstNewConLineItem[i]);
                }
            }

            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP49')){
                if (lstNewConLineItem[i].StartDate!=null && lstNewConLineItem[i].EndDate!=null){
                    lstCLI.add(lstNewConLineItem[i]);
                }
            }

            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP74')){
                //Tec-119
                //system.debug('lstNewConLineItem[i]' +lstNewConLineItem[i]);
                if((lstNewConLineItem[i].TECH_ServiceContractRecordType__c == 'Contrats_collectifs_publics' || lstNewConLineItem[i].TECH_ServiceContractRecordType__c == 'Contrats_Collectifs_Prives') && (lstNewConLineItem[i].TECH_ServiceContractStatus__c == 'Active' || lstNewConLineItem[i].TECH_ServiceContractStatus__c == 'Pending Payment' || lstNewConLineItem[i].TECH_ServiceContractStatus__c == 'En attente de renouvellement')
                    && lstNewConLineItem[i].IsActive__c == true && lstNewConLineItem[i].AssetId != null  && lstNewConLineItem[i].EndDate > system.today() && mapCliProCode.get(lstNewConLineItem[i].Id).startsWith('VF-') ){
                        lstCliCollective.add(lstNewConLineItem[i]);
                }
            }
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP80')) {
                if (lstNewConLineItem[i].TECH_ServiceContractRecordType__c == 'Contrats_Individuels' && lstNewConLineItem[i].AssetId != null) {
                    setCliColIdAP80.add(lstNewConLineItem[i].Id);
                }
            }

        }

        if(lstCliCollective.size() > 0){
            AP74_MaintenancePlan.createMaintenancePlanCollective(lstCliCollective);
        }

        if(mapProdIdToConLineItem.size()>0){
            AP09_BundleContract.populateConLineItems(mapProdIdToConLineItem);
        }

        if (setCliColIdAP80.size()>0) {
            AP80_UpdateEntitlementRelated.update_entitlement(setCliColIdAP80);
        }

        if(lstCLI.size()>0){
            // AP49_SetCompletionDateOfPreviousYrCLI.populateConLineItems(lstCLI);
        }
        
        
    }

     public void handleBeforeInsert(List<ContractLineItem> lstNewConLineItem){
        Map<Id, ContractLineItem> mapProdIdToConLineItem = new Map<Id, ContractLineItem>();
        list<ContractLineItem> lstCLI = new list <ContractLineItem>();
        
        /*
        for(Integer i=0; i<lstNewConLineItem.size(); i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP49')){
                if (lstNewConLineItem[i].StartDate!=null && lstNewConLineItem[i].EndDate!=null){
                    lstCLI.add(lstNewConLineItem[i]);
                }
            }
        }

        if(lstCLI.size()>0){
            // AP49_SetCompletionDateOfPreviousYrCLI.populateConLineItems(lstCLI);
        }
        */
    }

    public void handleBeforeDelete(List<ContractLineItem> lstOldConLineItem){
        Set<Id> setChildToDelete = new Set<Id>();

        for(Integer i=0; i<lstOldConLineItem.size(); i++){
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP09')){
                if(String.isNotBlank(lstOldConLineItem[i].Product2Id)){
                    setChildToDelete.add(lstOldConLineItem[i].Id);
                }
            }
        }

        if(setChildToDelete.size()>0){
            if(!Test.isRunningTest())  AP09_BundleContract.deleteBundleConLineItems(setChildToDelete);
        }
    }

    public void handleAfterUpdate(List<ContractLineItem> lstOldCLI, List<ContractLineItem> lstNewCLI){
        System.debug('ContractLineItemTriggerHandled');
        
        Set<Id> setCliColIdAP80 = new Set<Id>();//Tec-888
        List<ContractLineItem> lstCliVE = new List<ContractLineItem>(); //1228

        List<ContractLineItem> lstCliCollective = new List<ContractLineItem>();//Tec-119

        Map<Id, String> mapCliProCode = new Map<Id, String>();
        
        for(ContractLineItem cli : [SELECT Id, Product2.ProductCode FROM ContractLineItem WHERE Id IN :(new Map<Id,ContractLineItem>(lstNewCLI)).KeySet()]) {
            mapCliProCode.put(cli.Id, cli.Product2.ProductCode);
        }
        System.debug('ContractLineItemTriggerHandled mapCliProCode '+mapCliProCode);


        for(Integer i=0; i<lstNewCLI.size(); i++){            
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP53')){
                if(lstNewCLI[i].VE_Status__c != lstOldCLI[i].VE_Status__c  && mapCliProCode.get(lstNewCLI[i].Id).startsWith('VF-') && lstNewCLI[i].VE_Status__c == 'Complete'){
                    lstCliVE.add(lstNewCLI[i]);
                }
            }
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP74')){
                //Tec-119
                if((lstNewCLI[i].TECH_ServiceContractRecordType__c == 'Contrats_collectifs_publics' || lstNewCLI[i].TECH_ServiceContractRecordType__c == 'Contrats_Collectifs_Prives') && (lstNewCLI[i].TECH_ServiceContractStatus__c == 'Active' || lstNewCLI[i].TECH_ServiceContractStatus__c == 'Pending Payment' || lstNewCLI[i].TECH_ServiceContractStatus__c == 'En attente de renouvellement')
                    && lstNewCLI[i].IsActive__c <> lstOldCLI[i].IsActive__c && lstOldCLI[i].IsActive__c == false && lstNewCLI[i].IsActive__c == true && lstNewCLI[i].AssetId != null && lstNewCLI[i].EndDate > system.today() && mapCliProCode.get(lstNewCLI[i].Id).startsWith('VF-')){
                        lstCliCollective.add(lstNewCLI[i]);
                }
            }
            
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP80')) {
                if (lstNewCLI[i].TECH_ServiceContractRecordType__c == 'Contrats_Individuels' && lstNewCLI[i].AssetId != null && lstNewCLI[i].AssetId != lstOldCLI[i].AssetId) {
                    setCliColIdAP80.add(lstNewCLI[i].Id);
                }
            }
        }

        if(lstCliCollective.size() > 0){
            AP74_MaintenancePlan.createMaintenancePlanCollective(lstCliCollective);
        }

        
        if (setCliColIdAP80.size()>0) {
            AP80_UpdateEntitlementRelated.update_entitlement(setCliColIdAP80);
        }
        
        //AMO 20200817 - Commented AP53 - TEC-123
        if(lstCliVE.size()>0){
            // AP53_UpdateServiceContratStatus.updateStatusFromCLI(lstCliVE);
        }  
    }

    public void handleBeforeUpdate(List<ContractLineItem> lstOldCLI, List<ContractLineItem> lstNewCLI){
        System.debug('handleBeforeUpdate');

        //DMU - 3/6/20 - commented AP67_updateCLIVEStatus below variable due to HomeServe
        // List<ContractLineItem> lstCliAP67 = new List<ContractLineItem>(); //AP67

        for(Integer i=0; i<lstOldCLI.size(); i++){

            //DMU - 3/6/20 - commented the if condition due to HomeServe
            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP67')){
            //     if(lstNewCLI[i].Customer_Absence_Range__c != lstOldCLI[i].Customer_Absence_Range__c && !lstNewCLI[i].Customer_Absence_Range__c){
            //         lstCliAP67.add(lstNewCLI[i]);
            //     }
            // }
        }
        //DMU - 3/6/20 - commented the if condition due to HomeServe
        // if(lstCliAP67.size()>0){
        //     AP67_updateCLIVEStatus.veStatusDoubleAbs(lstCliAP67);
        // }
    }
}