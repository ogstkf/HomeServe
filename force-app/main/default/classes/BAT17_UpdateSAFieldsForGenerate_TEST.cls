/**
 * @File Name         : BAT17_UpdateSAFieldsForGenerate_TEST
 * @Description       : 
 * @Author            : Spoon Consulting (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 04-11-2021
 * Modifications Log 
 * ===================================================
 * Ver   Date           Author      Modification
 * ===================================================
 * 1.0   04-11-2021     MNA         Initial Version
**/
@itest
public with sharing class BAT17_UpdateSAFieldsForGenerate_TEST {
    static User mainUser;
    
    static Bypass__c bp = new Bypass__c();
    static Account testAcc;
    static Contact testCon;
    static Account testBusinessAccount;
    static OperatingHours opHrs = new OperatingHours();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<ServiceTerritory> lstSrvTerr = new List<ServiceTerritory>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
}
