/**
 * @File Name          : LC59_CloneContractCollectif_TEST.cls
 * @Description        : 
 * @Author             : CLA
 * @Group              : 
 * @Last Modified By   : CLA
 * @Last Modified On   : 08/07/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/07/2020   CLA     Initial Version
**/
@isTest
public with sharing class LC59_CloneContractCollectif_TEST {
    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<ServiceContract> lstSerCon = new List<ServiceContract>();
    static List<ServiceContract> lstSerConParent = new List<ServiceContract>();
    static List<ContractLineItem> lstContLineItem = new List<ContractLineItem>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<Product_Bundle__c> lstProductBundle = new List<Product_Bundle__c>();
    static sofactoapp__Coordonnees_bancaires__c bnkDet;
    static List<Tarifs_Unifies_Locaux__c> lstTUL = new List<Tarifs_Unifies_Locaux__c>();
    static List<ServiceTerritory> lstServiceTerritory = new List<ServiceTerritory>();
    static Integer numDaysPrior = Integer.valueOf(System.Label.NbDaysPriorRenewalCollectifs);
    static Bypass__c bp = new Bypass__c();

    static {
        adminUser = TestFactory.createAdminUser('testuser', TestFactory.getProfileAdminId());
        insert adminUser;
        
        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP74';
        bp.BypassValidationRules__c = True;
        bp.BypassWorkflows__c = True;
        bp.Bypass_Process_Builder__c = True;
        insert bp;

        System.runAs(adminUser) {
            Id serviceContractRecordTypeId = TestFactory.getRecordTypeId('ServiceContract', 'Contrats Collectifs Privés');

            for (Integer i = 0; i < 5; i++) {
                lstTestAcc.add(TestFactory.createAccount(TestFactory.randomizeString('test Acc') + i));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
                lstTestAcc[i].BillingPostalCode = '1234' + i;
                lstTestAcc[i].PersonEmail = TestFactory.randomizeString('test_acc').toLowerCase() + i+'@test.com';
                lstTestAcc[i].Rating__c = 4;
            }
            insert lstTestAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstTestAcc.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstTestAcc.get(2).Id);
            sofactoapp__Compte_auxiliaire__c aux3 = DataTST.createCompteAuxilaire(lstTestAcc.get(3).Id);
            sofactoapp__Compte_auxiliaire__c aux4 = DataTST.createCompteAuxilaire(lstTestAcc.get(4).Id);
			lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstTestAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstTestAcc.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            lstTestAcc.get(3).sofactoapp__Compte_auxiliaire__c = aux3.Id;
            lstTestAcc.get(4).sofactoapp__Compte_auxiliaire__c = aux4.Id;
            
            update lstTestAcc;

            //create products
            for(Integer i=0; i<10; i++){
                lstTestProd.add(TestFactory.createProduct('testProd'+i));
                lstTestProd[i].ProductCode = 'Test Code' + i;											//MNA 27/08/2021
                if(Math.mod(i, 5)== 0){
                    lstTestProd[i].IsBundle__c = true;
                    lstTestProd[i].Name = 'testBundleProd'+i/5;
                }
            }

            insert lstTestProd;

            Bundle__c bundleParent = TestFactory.createBundle('testProdBundleParent',100);
            insert bundleParent;

            Product_Bundle__c productBundleParent0 = TestFactory.createBundleProduct(bundleParent.Id, lstTestProd[0].Id);
            lstProductBundle.add(productBundleParent0);
            Product_Bundle__c productBundleParent1 = TestFactory.createBundleProduct(bundleParent.Id, lstTestProd[1].Id);
            productBundleParent1.Price__c = 50;
            productBundleParent1.Optional_Item__c = true;
            lstProductBundle.add(productBundleParent1);
            insert lstProductBundle;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST2', sofactoapp__Credit_prefix__c= '244', sofactoapp__Invoice_prefix__c='422');
            insert sofa; 

            bnkDet = new sofactoapp__Coordonnees_bancaires__c(
                sofactoapp__Compte__c = lstTestAcc[0].Id
                // , sofactoapp__Compte_comptable__c = 
                , Sofacto_Actif__c = true
                , tech_Rib_Valid__c = true
                , sofactoapp__IBAN__c = 'FR14 2004 1010 0505 0001 3M02 606'
                , sofactoapp__Raison_sociale__c = sofa.Id
            );
            insert bnkDet;

            OperatingHours newOperatingHour = TestFactory.createOperatingHour('test1');
            insert newOperatingHour;

            lstServiceTerritory = new List<ServiceTerritory>{
                new ServiceTerritory(
                    Name = 'test',
                    Agency_Code__c = '0007',
                    OperatingHoursId = newOperatingHour.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa.Id
                )};
            insert lstServiceTerritory;

            lstTUL.add(TestFactory.createTarifsUnifiesLocaux('testTarif',20,bundleParent.Id,lstServiceTerritory[0].Id,230));
            lstTUL[0].Augmentation_nationale_annuelle__c = 20;
            insert lstTUL;

            for (Integer i = 0; i < 5; i++) {
                lstSerConParent.add(TestFactory.createServiceContract('test' + i, lstTestAcc[i].Id));
                lstSerConParent[i].Type__c = 'Collective';
                lstSerConParent[i].Agency__c = lstServiceTerritory[0].Id;
                lstSerConParent[i].Contract_Status__c = 'Active';
                lstSerConParent[i].StartDate =  System.today().addYears(-2);
                lstSerConParent[i].EndDate = System.today().addYears(2);
                lstSerConParent[i].Contrat_resilie__c = false;
                lstSerConParent[i].PriceBook2Id = lstPrcBk[0].Id;
                lstSerConParent[i].sofactoapp_Rib_prelevement__c = bnkDet.Id;
                lstSerConParent[i].Contrat_signe_par_le_client__c = false;
                lstSerConParent[i].tech_facture_OK__c = false;
                lstSerConParent[i].Tech_generate_digital_invoice__c = false;
                lstSerConParent[i].recordTypeId = serviceContractRecordTypeId;
            }
            insert lstSerConParent;

            for (Integer i = 0; i < 5; i++) {
                lstSerCon.add(TestFactory.createServiceContract('test' + i, lstTestAcc[i].Id));
                lstSerCon[i].Type__c = 'Collective';
                lstSerCon[i].Agency__c = lstServiceTerritory[0].Id;
                lstSerCon[i].Contract_Status__c = 'Active';
                lstSerCon[i].StartDate =  System.today().addYears(-1).addDays(numDaysPrior * -1);
                lstSerCon[i].EndDate = System.today().addDays(numDaysPrior * -1);
                lstSerCon[i].Contrat_resilie__c = false;
                lstSerCon[i].PriceBook2Id = lstPrcBk[0].Id;
                lstSerCon[i].sofactoapp_Rib_prelevement__c = bnkDet.Id;
                lstSerCon[i].Contrat_signe_par_le_client__c = false;
                lstSerCon[i].tech_facture_OK__c = false;
                lstSerCon[i].Tech_generate_digital_invoice__c = false;
                lstSerCon[i].Nombre_de_renouvellements_restants__c = 2;
                lstSerCon[i].ParentServiceContractId = lstSerConParent[i].Id;
                lstSerCon[i].recordTypeId = serviceContractRecordTypeId;
                lstSerCon[i].TECH_BypassBAT__c = false;
            }

            lstSerCon[0].StartDate =  System.today().addDays(Integer.valueOf(System.Label.NbDaysPriorRenewalCollectifs)).addYears(-1);
            lstSerCon[0].EndDate =  System.today().addDays(Integer.valueOf(System.Label.NbDaysPriorRenewalCollectifs)+10);
            lstSerCon[0].tech_facture_OK__c = true;

            lstSerCon[3].TECH_BypassBAT__c =  true;

            lstSerCon[4].Duree_d_application_de_la_remise__c = 2;
            lstSerCon[4].Echeancier_Paiement__c = 'Comptant';
            lstSerCon[4].Contract_Status__c = 'Résilié';
            insert lstSerCon;



            //pricebook entry
            for(Integer i=0; i<10; i++){
                lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[i].Id, 0));
                if(lstTestProd[i].IsBundle__c){
                    lstPrcBkEnt[i].UnitPrice = 150;
                }
            }

            insert lstPrcBkEnt;

            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[1].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[2].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[3].Id, 0, 1));
            lstContLineItem.add(TestFactory.createContractLineItem(lstSerCon[0].Id, lstPrcBkEnt[4].Id, 0, 1));

            lstContLineItem[0].ServiceContractId = lstSerCon[0].Id;
            insert lstContLineItem;
        }
    }

    static testMethod void testBatch() {
        System.runAs(adminUser) {
            Test.startTest();
                LC59_CloneContractCollectif.resultWrapper result1 = LC59_CloneContractCollectif.createNewChildContrat(lstSerCon[0].Id);
                System.assertEquals('Nouveau contrat ne peut etre créer car le TECH_NumberOfDaysPriorEndDate__c est plus grand que '+ numDaysPrior, result1.msg);
    
                LC59_CloneContractCollectif.resultWrapper result2 = LC59_CloneContractCollectif.createNewChildContrat(lstSerCon[1].Id);
                System.assertEquals('success', result2.msg);
                
                LC59_CloneContractCollectif.resultWrapper result3 = LC59_CloneContractCollectif.createNewChildContrat(lstSerCon[2].Id);
                System.assertEquals('success', result3.msg);
                
                LC59_CloneContractCollectif.resultWrapper result4 = LC59_CloneContractCollectif.createNewChildContrat(lstSerCon[3].Id);
                System.assertEquals('Nouveau contrat déjà créer', result4.msg);
                
                LC59_CloneContractCollectif.resultWrapper result5 = LC59_CloneContractCollectif.createNewChildContrat(lstSerCon[4].Id);
                System.assertEquals('Nouveau contrat ne peut etre créer', result5.msg);
    
            Test.stopTest();
        }
    }
}