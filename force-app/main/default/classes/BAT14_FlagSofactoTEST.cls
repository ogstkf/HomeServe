/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 08-03-2020
 * @last modified by  : AMO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-14-2020   AMO   Initial Version
**/
@isTest
public class BAT14_FlagSofactoTEST {

    static user adminUser;
    static Quote quo;
    static Account testAcc;
    static Opportunity opp;
    static List<Quote> lstQuotes;
    static user adminUsersofacto;
    static sofactoapp__Compte_comptable__c compte = new sofactoapp__Compte_comptable__c();
    static sofactoapp__Plan_comptable__c plan = new sofactoapp__Plan_comptable__c();
    static sofactoapp__Compte_auxiliaire__c compteAux = new sofactoapp__Compte_auxiliaire__c();
    static{

        adminUser = new User(
            Username = 'amo@test.com'
            ,LastName = 'test'
            ,FirstName = 'test'
            ,Email = 'test@test.com'
            ,Alias = 'test'
            ,LanguageLocaleKey = 'en_US'
            ,TimeZoneSidKey = 'Europe/Paris'
            ,LocaleSidKey = 'en_US'
            ,EmailEncodingKey = 'UTF-8'
            ,ProfileId = TestFactory.getProfileAdminId()
            ,sofactoapp__isSofactoAdmin__c = false
            ,sofactoapp__IsSofactoAPI__c = false
            ,sofactoapp__isSofactoFinance__c = false
        );

        insert adminUser;

        adminUsersofacto = TestFactory.insertSofactoAdminUser('TestUser');

        System.runAs(adminUser){

            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.sofactoapp__Compte_auxiliaire__c = null;
            testAcc.Type = 'Agency prospect';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            // Create Sofacto Plan Comptable
            plan.Name = 'sofactoapp__Plan_comptable__c';
            insert plan;

            // Create Sofacto Compte Comptable
            compte.Name = '41compte comptable'; // Must start by '41'
            compte.sofactoapp__Plan_comptable__c = plan.Id;
            compte.sofactoapp__Libelle__c = 'sofactoapp__Libelle__c';
            insert compte;

            compteAux.Name = 'test Compte Auxilière';
            compteAux.sofactoapp__Compte__c = testAcc.Id;
            compteAux.sofactoapp__Compte_comptable__c = compte.Id;
            insert compteAux;

            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);
            opp.RecordTypeId = AP_Constant.getRecTypeId('Opportunity', 'Opportunite_standard');
            
            insert opp;

            lstQuotes = new List<Quote>{new Quote(Name ='Test1',
            Devis_signe_par_le_client__c = false,
            OpportunityId = opp.Id,
            recordtypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Devis_standard').getRecordTypeId(),
            Ordre_d_execution__c = null
            )};

            insert lstQuotes;


        }

    }

    @IsTest
    public static void testBatch(){

        System.runAs(adminUser){
            Test.startTest();
            BAT14_FlagSofacto.testBlnIsSofactoAdmin = true;
            BAT14_FlagSofacto batch = new BAT14_FlagSofacto();
            Database.executeBatch(batch, 1);

            Test.stopTest();

        }
    }

    @isTest
    public static void myTestMethod() {        
        test.starttest();
        BAT14_FlagSofacto.testBlnIsSofactoAdmin = true;
        BAT14_FlagSofacto myClass = new BAT14_FlagSofacto ();   
        String chron = '0 0 23 * * ?';                
        System.schedule('Test Sched', chron, myClass);
        test.stopTest();
    }

    @isTest
    public static void IsConnectedUserSofacto() {        
         test.starttest();        
            Boolean isConnectedUserSofacto = AP73_PBSofactoFlag.isConnectedUserSofacto();
         test.stopTest();
    }

}