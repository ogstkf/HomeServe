/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 29-04-2021
 * Modifications Log 
 * =====================================================================
 * Ver   Date           Author                  Modification
 * 1.0   28-04-2021     MNA                     Initial Version
**/
@isTest
public with sharing class BAT17_ServConAutomatisme_TEST {
    static User mainUser;
    static Bypass__c bp = new Bypass__c();
    static Account testAcc;
    static Contact testCon;
    static Account testBusinessAccount;
    static OperatingHours opHrs = new OperatingHours();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceTerritory> lstSrvTerr = new List<ServiceTerritory>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    
    static{

        mainUser = TestFactory.createAdminUser('BAT17@test.com', TestFactory.getProfileAdminId());
        insert mainUser;
        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53,MobileNotificationTrigger,AssignedResourceTrigger,ServiceAppointmentTrigger,AP10';
        insert bp;

        System.runAs(mainUser){

              
            //creating account
            testAcc = TestFactory.createAccount('WS13_ChaineEditiquev2_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.PersonEmail = 'test@spoon.com';
            insert testAcc;

            testBusinessAccount = TestFactory.createAccountBusiness('WS13_ChaineEditiquev2_TEST2');
            insert testBusinessAccount;

            //Create PriceBook
            Id pricebookId = Test.getStandardPricebookId();  
            

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;
            
            //create service territory
            lstSrvTerr.add(TestFactory.createServiceTerritory('SBF Energies',opHrs.Id, raisonSocial.Id));

            lstSrvTerr[0].Agency_Code__c = '1234';
            lstSrvTerr[0].Corporate_Street__c = '12349';
            lstSrvTerr[0].Corporate_Street2__c = '12348';
            lstSrvTerr[0].Corporate_ZipCode__c = '12345';
            lstSrvTerr[0].Corporate_City__c = 'Paris';
            lstSrvTerr[0].Phone__c  = '123456789';
            lstSrvTerr[0].Email__c = 'contact@sbf-energies.com';
            lstSrvTerr[0].site_web__c = 'www.google.com';
            lstSrvTerr[0].IBAN__c = 'FR1023456213456';
            lstSrvTerr[0].Libelle_horaires__c = '1234';
            lstSrvTerr[0].horaires_astreinte__c = '1234';

            insert lstSrvTerr;

            raisonSocial.sofactoapp_Agence__c = lstSrvTerr[0].Id;
            raisonSocial.sofactoapp__Email__c = 'contact@sbf-energies.com';
            update raisonSocial;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[0].Id));
            lstLogement[0].Street__c = 'lgmtStreet 1';
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement[0].Inhabitant__c = testAcc.Id;
            
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[0].Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testBusinessAccount.Id;
            lstLogement[1].Inhabitant__c = testBusinessAccount.Id;

            insert lstLogement;


            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon.add(TestFactory.createServiceContract('test serv con 2', testBusinessAccount.Id));

            lstServCon[0].Type__c = 'Individual';
            lstServCon[0].Agency__c = lstSrvTerr[0].Id;
            lstServCon[0].PriceBook2Id = pricebookId;
            lstServCon[0].EndDate = Date.Today().addYears(1);
            lstServCon[0].StartDate = Date.Today().addMonths(1);
            lstServCon[0].RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels') ;
            lstServCon[0].Type__c = 'Individual';
            lstServCon[0].Payeur_du_contrat__c = testAcc.Id;
            lstServCon[0].TransactionId__c = '123456789';
            lstServCon[0].Contract_Status__c = 'En attente de renouvellement';

            insert lstServCon;
        }
    }

    @isTest
    static void testScheduleExecute(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            System.schedule('Batch BAT17' + Datetime.now().format(),  '0 0 0 * * ?',  new BAT17_ServConAutomatisme());
            Test.stopTest();
        }
    }
    
    @isTest
    static void testBatchExecute1(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            lstServCon[0].StartDate = Date.Today().addMonths(-2);
            lstServCon[0].Contract_Status__c = 'Actif - en retard de paiement';
            update lstServCon[0];
            Test.startTest();
            Database.executeBatch(new BAT17_ServConAutomatisme());
            Test.stopTest();
        }
    }

    @isTest
    static void testBatchExecute2(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            lstServCon[0].StartDate = Date.Today().addMonths(-6);
            lstServCon[0].Contract_Status__c = 'Actif - en retard de paiement';
            update lstServCon[0];
            Test.startTest();
            Database.executeBatch(new BAT17_ServConAutomatisme());
            Test.stopTest();
        }
    }
    
    @isTest
    static void testBatchExecute3(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            lstServCon[0].Payeur_du_contrat__c = testBusinessAccount.Id;
            update lstServCon[0];
            Test.startTest();
            Database.executeBatch(new BAT17_ServConAutomatisme());
            Test.stopTest();
        }
    }

    public class calloutStatusPrinted implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"printed"}');
            response.setStatusCode(200);
            return response; 
        }
    }
    
    public class calloutStatuserrorprinting implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"error-printing"}');
            response.setStatusCode(200);
            return response; 
        }
    }
}