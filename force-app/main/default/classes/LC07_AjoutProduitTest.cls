/**
 * @File Name          : 
 * @Description        : Test class for LC07_AjoutProduit.cls
 * @Author             : ANA (AzharNahoor)
 * @Group              : 
 * @Last Modified By   : LGO
 * @Last Modified On   : 15-03-2022
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    29/11/2019           ANA                 Initial Version
**/
@isTest
public with sharing class LC07_AjoutProduitTest {
    static User mainUser;
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<Lignes_de_devis_type__c> lstLDT = new List<Lignes_de_devis_type__c>();
    static Devis_type__c devisType;
    static Account testAcc;
    static QuoteLineItem qli;
    static WorkOrder wrkOrd;
    static Quote quo, quo2;
    static sofactoapp__Compte_auxiliaire__c aux;
    static List<Product_Bundle__c> BP = new List<Product_Bundle__c> (); // Bundle Product
    static List<Bundle__c> bundle = new List<Bundle__c> (); // Bundle Product
    static List<RTCRemiseVolume__c> rtcRemiseVolume= new List<RTCRemiseVolume__c> (); // RTCRemiseVolume__c
    static Bypass__c bp1 = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('testadmin', TestFactory.getProfileAdminId());
        insert mainUser;
        
        System.runAs(mainUser){
            bp1.SetupOwnerId  = mainUser.Id;
            bp1.BypassTrigger__c = 'AP16,WorkOrderTrigger';
            bp1.BypassValidationRules__c = True;
            insert bp1;

            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            aux = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux.Id;

            update testAcc;

            //Create products         
            Product2 prod = TestFactory.createProduct('testProd');
            prod.Produit_de_type_Remise_exceptionnelle__c = true;
            lstTestProd.add(prod);  
            insert lstTestProd;

            //Pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                Ref_Agence__c = true,
                isActive = true,
                recordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Vente').getRecordTypeId()
            ); 
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
        
            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[0].Id, 0));       
            // lstPrcBkEnt[0].name = 'test';
            insert lstPrcBkEnt;

            //Create Opportunity
            Opportunity testOpp = TestFactory.createOpportunity('test', testAcc.Id);
            insert testOpp;

            //Create WorkOrder
            wrkOrd = TestFactory.createWorkOrder();
            insert wrkOrd;

            //create Quote
            quo = new Quote(Name ='Test1',
                Devis_signe_par_le_client__c = false,
                OpportunityId = testOpp.Id,
                Ordre_d_execution__c = wrkOrd.Id,
                Pricebook2Id = lstPrcBk[0].Id,
                isSync__c = false,
                RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Devis_RTC').getRecordTypeId(),
                Date_de_debut_des_travaux__c = System.today()
            );
            insert quo;

            quo2 = new Quote(Name ='Test2',
                Devis_signe_par_le_client__c = false,
                OpportunityId = testOpp.Id,
                Ordre_d_execution__c = wrkOrd.Id,
                Pricebook2Id = lstPrcBk[0].Id,
                isSync__c = false,
                RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Devis_standard').getRecordTypeId(),
                Date_de_debut_des_travaux__c = System.today()
        );
        insert quo2;

            devisType = TestFactory.createDevisType('test Devis Type');
            insert devisType;

            lstLDT.add(TestFactory.createLigneDevisType(devisType.Id, lstTestProd[0].Id));
            insert lstLDT;
            //Create  Bundle
            bundle = new List<Bundle__c> {
                new Bundle__c(),
                new Bundle__c(),
                new Bundle__c()
            };  
            insert bundle;


            //Create product Bundle
            // Master detail relationship
            BP = new List<Product_Bundle__c> {
                new Product_Bundle__c(Bundle__c = bundle[0].Id, Price__c = 100, Optional_Item__c = true, Product__c =lstTestProd[0].Id)
            };  
            insert BP;

            //Create RTCRemiseVolume
            rtcRemiseVolume = new List<RTCRemiseVolume__c>{
                new RTCRemiseVolume__c(
                Name = 'testRtcRemiseVolume', 
                Date_debut__c = Date.valueOf(Datetime.newInstance(2020, 07, 28).format('yyyy-MM-dd')), 
                Date_fin__c =  Date.valueOf(Datetime.newInstance(2020, 07, 30).format('yyyy-MM-dd')),
                Product_Code__c = lstTestProd[0].ProductCode, 
                Qmax__c = 23, 
                Qmin__c = 1, 
                Remisevaleur__c = 25, 
                Remisepourcent__c = 56
                )};
            insert rtcRemiseVolume;
        }
    }

    @isTest
    public static void fetchQuoteDetails_Test(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, Object> mapRes = (Map<String, Object>)LC07_AjoutProduit.fetchQuoDetails(quo.Id);
                Boolean isAdmin = (Boolean) mapRes.get('isAdmin');
                Quote q = (Quote) mapRes.get('quote');
            Test.stopTest();

            System.assertEquals(isAdmin, true);
            System.assertEquals(q.Id, Quo.Id);
        }
    }

    @isTest
    public static void fetchPB_Test(){
        System.runAs(mainUser){
            Test.startTest();
                List<Pricebook2> pricebooks = new List<PriceBook2>();
                pricebooks = LC07_AjoutProduit.fetchPb(quo.Id);
            Test.stopTest();

            System.assertEquals(true, pricebooks.isEmpty());
        }
    }

    @isTest
    public static void calulateRemiseQLIs_Test(){
        System.runAs(mainUser){
            Test.startTest();
               // List<QliWrapper> qliWrapper = new List<QliWrapper>();
                Object lstNewQliWrap = LC07_AjoutProduit.populateQLI(new List<String>{lstPrcBkEnt[0].Id}, quo.Id, null);

                Object qliWrapper = LC07_AjoutProduit.calulateRemiseQLIs(JSON.serialize(lstNewQliWrap), quo.Id);
            Test.stopTest();
        }
    }

    @isTest
    public static void fetchPriceBookEntries_TestSOQL(){
        System.runAs(mainUser){
            Test.startTest();
                List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
                pricebookEntries = LC07_AjoutProduit.fetchPriceBookEntries(lstPrcBk[0].Id, 'a', quo.Id);
            Test.stopTest();

            System.assertEquals(true, pricebookEntries.isEmpty());
        }
    }

    @isTest
    public static void getRemiseRTC_Test(){
        System.runAs(mainUser){
            Test.startTest();
               // List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
                Object pricebookEntries = LC07_AjoutProduit.getRemiseRTC(new List<String>{lstPrcBkEnt[0].Id});
            Test.stopTest();
        }
    }

    @isTest
    public static void fetchPriceBookEntries_TestSOQL_devisStandard(){
        System.runAs(mainUser){
            Test.startTest();
                List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
                pricebookEntries = LC07_AjoutProduit.fetchPriceBookEntries(lstPrcBk[0].Id, null, quo2.Id);
            Test.stopTest();

            System.assertEquals(false, !pricebookEntries.isEmpty());
        }
    }

    @isTest
    public static void fetchPriceBookEntries_TestSOQL_serachTextNull(){
        System.runAs(mainUser){
            Test.startTest();
                List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
                pricebookEntries = LC07_AjoutProduit.fetchPriceBookEntries(lstPrcBk[0].Id, null , quo.Id);
            Test.stopTest();

            System.assertEquals(false, !pricebookEntries.isEmpty());
        }
    }

    @isTest
    public static void fetchPriceBookEntries_TestSOSL(){
        System.runAs(mainUser){
            Test.startTest();
                List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
                pricebookEntries = LC07_AjoutProduit.fetchPriceBookEntries(lstPrcBk[0].Id, 'abc', quo.Id);
            Test.stopTest();

            System.assertEquals(true, pricebookEntries.isEmpty());
        }
    }

    @isTest
    public static void fetchPriceBookEntriesWithQuery_Test(){
        System.runAs(mainUser){
            Test.startTest();
                List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
                //pricebookEntries = LC07_AjoutProduit.fetchPriceBookEntries(lstPrcBk[0].Id, 'xxxxxxxxxxxxxx');
            Test.stopTest();

            System.assertEquals(true, pricebookEntries.isEmpty());
        }
    }

    @isTest
    public static void fetchQLI_Test(){
        //create QuoteLineItem
        qli  = new QuoteLineItem(
            Quantity= 2,
            Product2Id =lstTestProd[0].Id,
            QuoteId = quo.Id, 
            PricebookEntryId = lstPrcBkEnt[0].Id,
            UnitPrice = 10
        );
        insert qli;

        System.runAs(mainUser){
            Test.startTest();
                List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();
                quoteLineItems = LC07_AjoutProduit.fetchQLI(quo.Id, lstPrcBk[0].Id);
            Test.stopTest();

            System.assertEquals(true, !quoteLineItems.isEmpty());
        }
    }

    @isTest
    public static void populateQLI_UpdateQLiTest(){
        //create QuoteLineItem
        qli  = new QuoteLineItem(
            Quantity= 2,
            Product2Id =lstTestProd[0].Id,
            QuoteId = quo.Id, 
            PricebookEntryId = lstPrcBkEnt[0].Id,
            UnitPrice = 10
        );
        insert qli;

        System.runAs(mainUser){
            Test.startTest();
                Object lstNewQliWrap = LC07_AjoutProduit.populateQLI(new List<String>{lstPrcBkEnt[0].Id}, quo.Id, null);
                system.debug('### QLI qrap' + lstNewQliWrap);
                system.debug(lstNewQliWrap);
                Object quoId = LC07_AjoutProduit.saveQLIs(JSON.serialize(lstNewQliWrap), quo, lstPrcBk[0]);
            Test.stopTest();
            // System.assert((Id) quoId, Quo.id);
        }
    }

 /*   @isTest
    public static void populateQLI_DeleteQLiTest(){
        //create QuoteLineItem
        qli  = new QuoteLineItem(
            Quantity= null,
            Product2Id =lstTestProd[0].Id,
            QuoteId = quo.Id, 
            PricebookEntryId = lstPrcBkEnt[0].Id,
            UnitPrice = 10
        );
        insert qli;

        System.runAs(mainUser){
            Test.startTest();
                Object lstNewQliWrap = LC07_AjoutProduit.populateQLI(new List<String>{lstPrcBkEnt[0].Id}, quo.Id, null);
                system.debug('### QLI qrap' + lstNewQliWrap);
                system.debug(lstNewQliWrap);
                Object quoId = LC07_AjoutProduit.saveQLIs(JSON.serialize(lstNewQliWrap), quo, lstPrcBk[0]);
            Test.stopTest();
            // System.assert((Id) quoId, Quo.id);
        }
    }*/

    @isTest
    public static void populateQLI_CreateQLITest(){
        System.runAs(mainUser){
            Test.startTest();
                Object lstNewQliWrap = LC07_AjoutProduit.populateQLI(new List<String>{lstPrcBkEnt[0].Id}, quo.Id, devisType.Id);
                Object quoId = LC07_AjoutProduit.saveQLIs(JSON.serialize(lstNewQliWrap), quo, lstPrcBk[0]);
            Test.stopTest();
            // System.assert((Id) quoId, Quo.id);
        }
    }

    @isTest
    public static void populateQLI_CreateQLITest_upsert(){
        System.runAs(mainUser){
                    //create QuoteLineItem
            qli  = new QuoteLineItem(
                Quantity= 2,
                Product2Id =lstTestProd[0].Id,
                QuoteId = quo.Id, 
                PricebookEntryId = lstPrcBkEnt[0].Id,
                UnitPrice = 10
            );
            insert qli;
            
            Test.startTest();
                List<LC07_AjoutProduit.QliWrapper> lstNewQliWrap = new List<LC07_AjoutProduit.QliWrapper>();
                
                lstNewQliWrap = (List<LC07_AjoutProduit.QliWrapper>) LC07_AjoutProduit.populateQLI(new List<String>{lstPrcBkEnt[0].Id}, quo.Id, null);
                
                lstNewQliWrap[0].action = 'upsert';
                Object quoId = LC07_AjoutProduit.saveQLIs(JSON.serialize(lstNewQliWrap), quo, lstPrcBk[0]);
            Test.stopTest();
        }
    }

    @isTest
    public static void getAllProductsQLIT_Test(){
        System.runAs(mainUser){
            Test.startTest();
            Object lstPbe = LC07_AjoutProduit.getAllProductsQLIT(devisType, lstPrcBk[0]);
            Test.stopTest();
            // System.assertEquals(1, lstPbe.size());
        }
    }

    @isTest
    public static void fetchLookUpValues_Test(){
        System.runAs(mainUser){
            Test.startTest();
            List<Devis_type__c> lstDevType = LC07_AjoutProduit.fetchLookUpValues('test', 'Devis_type__c', quo.Id);
            Test.stopTest();
            System.assertEquals(1, lstDevType.size());
        }
    }
}