/**
 * @File Name          : AP33_WorkOrderLocation_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    6/11/2020   KZE     Initial Version
**/
@istest (SeeAllData=true)
public with sharing class AP33_WorkOrderLocation_TEST {
static User mainUser;
    static Account testAcc = new Account();
    static sofactoapp__Compte_auxiliaire__c CA = new sofactoapp__Compte_auxiliaire__c (); // Sub-ledger Account
    // static sofactoapp__Coordonnees_bancaires__c CB = new sofactoapp__Coordonnees_bancaires__c(); // Bank Details
    static sofactoapp__Compte_comptable__c CC = new sofactoapp__Compte_comptable__c (); // Accounting Account
    static List<sofactoapp__Plan_comptable__c> PC = new List<sofactoapp__Plan_comptable__c> (); // Chart of Accounts
    static list <ServiceAppointment> sappLst;
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();   
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceResource> lstSR = new List<ServiceResource>();
    static List<Schema.Location> lstLocation = new List<Schema.Location>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static List<AssignedResource> lstASR = new List<AssignedResource>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();    
    static List<Case> lstCase= new List<Case>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<ServiceTerritoryMember> lstSTM = new List<ServiceTerritoryMember>();
    static List<User> lstUser = new List<User>();
    static Bypass__c bp = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('AP24@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = True;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassWorkflows__c = true;
        insert bp;
        
        System.runAs(mainUser){ 

            // Define relation Master Detail with object sofactoapp__Plan_comptable__c
            PC = new List<sofactoapp__Plan_comptable__c> {
                            new sofactoapp__Plan_comptable__c(),
                            new sofactoapp__Plan_comptable__c(),
                            new sofactoapp__Plan_comptable__c()
                        };  
            insert PC; 
            
            // create sofactoapp__Compte_comptable__c (Accounting Account)
            CC.Name = 'test CC';
            CC.sofactoapp__Libelle__c = 'test libellé';
            CC.sofactoapp__Plan_comptable__c = PC[0].Id;
            insert CC; 

            // create sofactoapp__Compte_auxiliaire__c (Sub-ledger Account)
            CA.Name = 'test account aux';
            CA.sofactoapp__Compte_comptable__c = CC.Id;
            CA.Compte_comptable_Prelevement__c = CC.Id;
            insert CA; 
            
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;  
            
            testAcc.sofactoapp__Compte_auxiliaire__c = CA.Id;
            update testAcc;


            //create products            
            lstTestProd.add(TestFactory.createProduct('testProd'));
            lstTestProd[0].RecordTypeId = AP_Constant.getRecTypeId('Product2', 'Product');
            insert lstTestProd;
           
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
           
            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[0].Id, 0));          
            insert lstPrcBkEnt;

            // create sofactoapp__Coordonnees_bancaires__c
            // CB.Name = 'Test bank';
            // CB.tech_Rib_Valid__c = false;
            // insert CB;
           
            
            //service contract           
            lstServCon.add(TestFactory.createServiceContract('testServCon', testAcc.Id));
            lstServCon[0].PriceBook2Id = lstPrcBk[0].Id;      
            //lstServCon[0].Subtotal = 10;    
            lstServCon[0].Contrat_signe_par_le_client__c = false;
            lstServCon[0].Contract_Renewed__c = true;
            lstServCon[0].Contract_Status__c = 'Cancelled';
            lstServCon[0].tech_facture_OK__c = true;
            lstServCon[0].Contrat_Cham_Digital__c = true;
            lstServCon[0].Echeancier_Paiement__c = 'Comptant';
            lstServCon[0].Sofactoapp_Mode_de_paiement__c = 'Virement';
            lstServCon[0].Tech_generate_digital_invoice__c = false;
            insert lstServCon;  
            
            // lstServCon[0].sofactoapp_Rib_prelevement__c = CB.Id;
            // update lstServCon;  
                        
            //contract line item
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));      
            lstConLnItem[0].Customer_Absences__c = 1;
            lstConLnItem[0].Customer_Allowed_Absences__c = 3;
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));      
            insert lstConLnItem;            

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;
            
            //create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST2', sofactoapp__Credit_prefix__c= '244', sofactoapp__Invoice_prefix__c='422');
            insert sofa;

            //create service territory
            srvTerr = TestFactory.createServiceTerritory('test Territory',opHrs.Id);
            srvTerr.Sofactoapp_Raison_Social__c =sofa.Id;            
            insert srvTerr;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            insert lstLogement;

            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstTestProd[0].Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = testAcc.Id;
            insert lstAsset;     

             // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[0].Id));
            lstCase[0].Contract_Line_Item__c = lstConLnItem[0].Id;            
            lstCase[0].Origin = 'Phone';
            lstCase[0].Status = 'In Progress';
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A2 - Logement', lstAsset[0].Id));
            lstCase[1].Contract_Line_Item__c = lstConLnItem[0].Id;            
            lstCase[1].Origin = 'Phone';
            lstCase[1].Status = 'In Progress';
            insert lstCase;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Commissioning';

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].caseId = lstCase[0].Id;  
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].Type__c = 'Maintenance';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[1].caseId = lstCase[1].Id;  
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[1].Type__c = 'Installation';
            insert lstWrkOrd;     

            sappLst = new List<ServiceAppointment>{
                                    new ServiceAppointment(
                                            Status = 'Planned',                                            
                                            ParentRecordId = lstWrkOrd[0].Id,                                         
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Work_Order__c = lstWrkOrd[0].Id ,
                                            SchedStartTime = Datetime.now().addYears(1),
                                            SchedEndTime = Datetime.now().addYears(2)                      
                                    ),
                                    new ServiceAppointment(
                                            Status = 'Planned',                                            
                                            ParentRecordId = lstWrkOrd[1].Id,                                         
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Work_Order__c = lstWrkOrd[1].Id ,
                                            SchedStartTime = Datetime.now().addYears(1),
                                            SchedEndTime = Datetime.now().addYears(2)                     
                                    )
            };
            insert sappLst;

            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 

            lstUser = new List<User>{
                			new User(Alias = 'standt', Email='standarduser@testorg.com', 
                                    EmailEncodingKey='UTF-8', LastName='Test User ANA', LanguageLocaleKey='en_US', 
                                    LocaleSidKey='en_US', ProfileId = p.Id, 
                                    TimeZoneSidKey='America/Los_Angeles', UserName='TestUserANA@testorg.com'),
                			new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                                    EmailEncodingKey='UTF-8', LastName='Test User YDO', LanguageLocaleKey='en_US', 
                                    LocaleSidKey='en_US', ProfileId = p.Id, 
                                    TimeZoneSidKey='America/Los_Angeles', UserName='TestUserYADO1@testorg.com'),
                			new User(Alias = 'standt', Email='standarduser1@testorg.com', 
                                    EmailEncodingKey='UTF-8', LastName='Test User KZE', LanguageLocaleKey='en_US', 
                                    LocaleSidKey='en_US', ProfileId = p.Id, 
                                    TimeZoneSidKey='America/Los_Angeles', UserName='TestUserANA1@testorg.com')
                                };

            insert lstUser;
            
            lstSR = new List<ServiceResource>{
                                    new ServiceResource(
                                        IsActive=true,
                                        Name='Test User KZE',
                                        RelatedRecordId=lstUser[0].Id),
                                    new ServiceResource(
                                        IsActive=true,
                                        Name='Test User KZE 2',
                                        RelatedRecordId=lstUser[1].Id) ,
                                    new ServiceResource(
                                        IsActive=true,
                                        Name='Test User KZE 3',
                                        RelatedRecordId=lstUser[2].Id)                  
            };
            insert lstSR;

            lstLocation = new List<Schema.Location>{
                                    new Schema.Location(
                                            LocationType = 'Véhicule',     
                                            Name='Véhicule 1',
                                            Service_Resource__c = lstSR[0].Id            
                                    ),
                                    new Schema.Location(
                                            LocationType = 'Véhicule',      
                                            Name='Véhicule 2',
                                            Service_Resource__c = lstSR[1].Id   
                                    ) ,
                                    new Schema.Location(
                                            LocationType = 'Véhicule',      
                                            Name='Véhicule 3',
                                            Service_Resource__c = lstSR[2].Id   
                                    )              
            };
            insert lstLocation;

            lstSTM = new List<ServiceTerritoryMember>{
                                    new ServiceTerritoryMember(
                                        EffectiveStartDate=Datetime.now().addYears(-1),
                                        ServiceTerritoryId=srvTerr.Id,
                                        ServiceResourceId=lstSR[0].Id
                                    ),       
                                    new ServiceTerritoryMember(
                                        EffectiveStartDate=Datetime.now().addYears(-1),
                                        ServiceTerritoryId=srvTerr.Id,
                                        ServiceResourceId=lstSR[1].Id
                                    ),       
                                    new ServiceTerritoryMember(
                                        EffectiveStartDate=Datetime.now().addYears(-1),
                                        ServiceTerritoryId=srvTerr.Id,
                                        ServiceResourceId=lstSR[2].Id
                                    )
            };
            insert lstSTM;
        }
    }

    @isTest
    public static void testInsertASR(){
        System.runAs(mainUser){
            Test.startTest();           
                lstASR = new List<AssignedResource>{
                                        new AssignedResource(
                                                ServiceAppointmentId = sappLst[0].Id,
                                                ServiceResourceId=lstSR[0].Id)
                                                ,
                                        new AssignedResource(
                                                ServiceAppointmentId = sappLst[0].Id,
                                                ServiceResourceId=lstSR[1].Id)             
                };
                insert lstASR;
                // AP33_WorkOrderLocation.updateLocation(lstASR);
            Test.stopTest();

            list<WorkOrder> lstNewWO = [ SELECT Id, LocationId FROM WorkOrder
                                            WHERE Id = :lstWrkOrd[0].Id ] ;       
            System.assertEquals(lstNewWO[0].LocationId , null);
        }
    }

    @isTest
    public static void testInsertASR2(){
        System.runAs(mainUser){
            Test.startTest();           
                lstASR = new List<AssignedResource>{
                                        new AssignedResource(
                                                ServiceAppointmentId = sappLst[0].Id,
                                                ServiceResourceId=lstSR[0].Id)          
                };
                insert lstASR;
            Test.stopTest();

            list<WorkOrder> lstNewWO = [ SELECT Id, LocationId FROM WorkOrder
                                            WHERE Id = :lstWrkOrd[0].Id ] ;       
            // System.assertEquals(lstNewWO[0].LocationId , lstLocation[0].Id);
        }
    }

    @isTest
    public static void testDeleteASR(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;

            Test.startTest();           
                delete lstASR;
            Test.stopTest();
            
            list<WorkOrder> lstNewWO = [ SELECT Id, LocationId FROM WorkOrder
                                            WHERE Id = :lstWrkOrd[0].Id ] ;       
            System.assertEquals(lstNewWO[0].LocationId , null);

        }
    }
    @isTest
    public static void testDeleteASR2(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;
            List<AssignedResource> lstASR1 = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[1].Id)             
            };
            insert lstASR1;

            Test.startTest();           
                delete lstASR[0];
            Test.stopTest();
            
            list<WorkOrder> lstNewWO = [ SELECT Id, LocationId FROM WorkOrder
                                            WHERE Id = :lstWrkOrd[0].Id ] ;           
            System.debug('lstASR[0].Id: ' + lstASR[0].Id);     
            System.debug('lstASR1[0].Id: ' + lstASR1[0].Id);             
            System.debug('lstLocation[0].Id: ' + lstLocation[0].Id);     
            System.debug('lstLocation[1].Id: ' + lstLocation[1].Id);     
            System.debug('lstLocation[2].Id: ' + lstLocation[2].Id);         
            // System.assertEquals(lstNewWO[0].LocationId , lstLocation[1].Id);

        }
    }
    @isTest
    public static void testUpdateASR(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;

            Test.startTest();           
                lstASR[0].ServiceResourceId = lstSR[2].Id;
                update lstASR[0];
            Test.stopTest();
            
            list<WorkOrder> lstNewWO = [ SELECT Id, LocationId FROM WorkOrder
                                            WHERE Id = :lstWrkOrd[0].Id ] ;                
            // System.assertEquals(lstNewWO[0].LocationId , lstLocation[2].Id);

        }
    }
    @isTest
    public static void onServiceResourceModified(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;

            set<Id> setSRId = new set<Id>();
            for(ServiceResource s : lstSR){
                setSRId.add(s.Id);
            }

            set<Id> setSAId = new set<Id>();
            for(ServiceAppointment s : sappLst){
                setSAId.add(s.Id);
            }

            Test.startTest();           
                lstASR[0].ServiceResourceId=lstSR[1].Id;
                update lstASR;
               AP33_ManageAssignedResources.onServiceResourceModified(lstASR, lstASR, setSAId ,setSRId);
            Test.stopTest();
        }
    }

    @isTest
    public static void onServiceResourceModified2(){
        System.runAs(mainUser){
            AP33_ManageAssignedResources.blnTest = true;
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;

            set<Id> setSRId = new set<Id>();
            for(ServiceResource s : lstSR){
                setSRId.add(s.Id);
            }

            set<Id> setSAId = new set<Id>();
            for(ServiceAppointment s : sappLst){
                setSAId.add(s.Id);
            }

            Test.startTest();           
                lstASR[0].ServiceResourceId=lstSR[1].Id;
                update lstASR;
               AP33_ManageAssignedResources.onServiceResourceModified(lstASR, lstASR, setSAId ,setSRId);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void onServiceResourceInserted(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;

            set<Id> setSRId = new set<Id>();
            for(ServiceResource s : lstSR){
                setSRId.add(s.Id);
            }

            set<Id> setSAId = new set<Id>();
            for(ServiceAppointment s : sappLst){
                setSAId.add(s.Id);
            }

            Test.startTest();           
                lstASR[0].ServiceResourceId=lstSR[1].Id;
                update lstASR;
               AP33_ManageAssignedResources.onServiceResourceInserted(lstASR, setSAId ,setSRId);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void onServiceResourceInserted2(){
        System.runAs(mainUser){
            AP33_ManageAssignedResources.blnTest = true;
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;

            set<Id> setSRId = new set<Id>();
            for(ServiceResource s : lstSR){
                setSRId.add(s.Id);
            }

            set<Id> setSAId = new set<Id>();
            for(ServiceAppointment s : sappLst){
                setSAId.add(s.Id);
            }

            Test.startTest();           
                lstASR[0].ServiceResourceId=lstSR[1].Id;
                update lstASR;
               AP33_ManageAssignedResources.onServiceResourceInserted(lstASR, setSAId ,setSRId);
            Test.stopTest();
        }
    }

    @isTest
    public static void onServiceResourceDeleted(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;

            set<Id> setSRId = new set<Id>();
            for(ServiceResource s : lstSR){
                setSRId.add(s.Id);
            }

            set<Id> setSAId = new set<Id>();
            for(ServiceAppointment s : sappLst){
                setSAId.add(s.Id);
            }

            Test.startTest();           
                lstASR[0].ServiceResourceId=lstSR[1].Id;
                update lstASR;
               AP33_ManageAssignedResources.onServiceResourceDeleted(lstASR, setSAId ,setSRId);
            Test.stopTest();
        }
    }

    @isTest
    public static void handleAfterUpdate(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;           

            AssignedResourceTriggerHandler handler = new AssignedResourceTriggerHandler();
            Test.startTest();                           
                handler.handleAfterUpdate(lstASR, lstASR);
            Test.stopTest();
        }
    }
    @isTest
    public static void handleAfterDelete(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;           
            AssignedResourceTriggerHandler handler = new AssignedResourceTriggerHandler();
            Test.startTest();             
            handler.handleAfterDelete(lstASR);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void handleBeforeDelete(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;           
            AssignedResourceTriggerHandler handler = new AssignedResourceTriggerHandler();
            Test.startTest();             
            handler.handleBeforeDelete(lstASR);
            Test.stopTest();
        }
    }

    @isTest
    public static void handleAfterInsert(){
        System.runAs(mainUser){
            lstASR = new List<AssignedResource>{
                                    new AssignedResource(
                                            ServiceAppointmentId = sappLst[0].Id,
                                            ServiceResourceId=lstSR[0].Id)          
            };
            insert lstASR;           

            AssignedResourceTriggerHandler handler = new AssignedResourceTriggerHandler();
            Test.startTest();                        
                handler.handleAfterInsert(lstASR);
            Test.stopTest();
        }
    }

}