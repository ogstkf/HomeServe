/**
 * @File Name          : ProductRequestLineItemTriggerHandler.CLS
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 16/03/2020, 16:39:36
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0      15/11/2019      LGO     Initial Version
**/
public with sharing class ProductRequestLineItemTriggerHandler {

    Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());

    public void handleAfterUpdate(List<ProductRequestLineItem> lstOldPRLI, List<ProductRequestLineItem> lstNewPRLI){
        
        Set<Id> setQuoteIds= new Set<Id>();
        Set<Id> setPrlIdCommander = new Set<Id>();
        Set<Id> setAgencedCommander = new Set<Id>();
        Set<Id> setPrlIdPreparer = new Set<Id>();
        Set<Id> setAgencedPreparer = new Set<Id>();
        Set<Id> setPrlIdElse = new Set<Id>();
        Set<Id> setAgencedElse = new Set<Id>();
        Boolean conOneThree = false;        

        for(integer i=0; i<lstNewPRLI.size(); i++){

            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP36')){
                if((lstOldPRLI[i].Status == null || lstOldPRLI[i].Status == 'Réservé à préparer'|| lstOldPRLI[i].Status == 'Réservé préparé'|| lstOldPRLI[i].Status == 'Annulé') && lstNewPRLI[i].Status == 'A commander'){
                    System.debug('Enter 2 Condition');
                    setAgencedCommander.add(lstNewPRLI[i].Agence__c);
                    setPrlIdCommander.add(lstNewPRLI[i].Id);
                }

                if((lstOldPRLI[i].Status == null || lstOldPRLI[i].Status == 'A commander' ) && (lstNewPRLI[i].Status == 'Réservé à préparer' || lstNewPRLI[i].Status == 'Réservé préparé')){
                    System.debug('Enter 1 Condition');
                    conOneThree = true;
                    setPrlIdPreparer.add(lstNewPRLI[i].Id);
                    setAgencedPreparer.add(lstNewPRLI[i].Agence__c);
                }


                if((lstOldPRLI[i].Status == 'Réservé à préparer' || lstOldPRLI[i].Status == 'Réservé préparé') && (lstNewPRLI[i].Status != 'Réservé à préparer' && lstNewPRLI[i].Status != 'Réservé préparé')){
                    System.debug('Enter 3 Condition');
                    setPrlIdElse.add(lstNewPRLI[i].Id);
                    setAgencedElse.add(lstNewPRLI[i].Agence__c);
                }
            }
            
            //if(lstNewPRLI[i].Status  <> lstOldPRLI[i].Status && lstNewPRLI[i].Status == AP_Constant.PRLIStatutReservePrep && lstNewPRLI[i].Quote__c  <> null){
            if(lstNewPRLI[i].Status  <> lstOldPRLI[i].Status && lstNewPRLI[i].Quote__c  <> null){
                System.debug('Enter 4 Condition');
                setQuoteIds.add(lstNewPRLI[i].Quote__c);
            }

        }

        
        if(setPrlIdCommander.size() > 0){
            AP36_QuoteProductTransfers.PrliACommander(setPrlIdCommander, setAgencedCommander);
        }
        
        if(setPrlIdPreparer.size() > 0){
            AP36_QuoteProductTransfers.PrliReservePrepare(setPrlIdPreparer, setAgencedPreparer);
        }
        
        if(setPrlIdElse.size() > 0){
            AP36_QuoteProductTransfers.ResPrepareElse(setPrlIdElse, setAgencedElse);
        }
        
        if(setQuoteIds.size() >0 ){
            AP46_QuoteManager.updateQuote(setQuoteIds);
        }
        
    }


     public void handleAfterInsert(List<ProductRequestLineItem> lstNewPRLI){
        
        Set<Id> setQuoteIds= new Set<Id>();
        Set<Id> setPrlIdCommander = new Set<Id>();
        Set<Id> setAgencedCommander = new Set<Id>();

        for(integer i=0; i<lstNewPRLI.size(); i++){
            if(lstNewPRLI[i].Status == AP_Constant.PRLIStatutReservePrep
             && lstNewPRLI[i].Quote__c  <> null){
                setQuoteIds.add(lstNewPRLI[i].Quote__c);
            }

            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP36')){
                if(lstNewPRLI[i].Status == 'A commander' && lstNewPRLI[i].Quote__c == null){                
                    setAgencedCommander.add(lstNewPRLI[i].Agence__c);
                    setPrlIdCommander.add(lstNewPRLI[i].Id);
                }
            }
            
        }
        system.debug(' setQuoteIds: '+setQuoteIds);

        if(setQuoteIds.size() >0 ){
            AP46_QuoteManager.updateQuote(setQuoteIds);
        }
          
        if(setPrlIdCommander.size() > 0){
            AP36_QuoteProductTransfers.PrliACommander(setPrlIdCommander, setAgencedCommander);
        }
        
        
    }



}