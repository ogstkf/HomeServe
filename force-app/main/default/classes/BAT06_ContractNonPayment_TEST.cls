/**
 * @File Name          : BAT06_ContractNonPayment_TEST.cls
 * @Description        : 
 * @Author             : KJB
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 04/02/2020, 16:49:38
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    13/12/2019         KJB                    Initial Version
**/

@isTest
public class BAT06_ContractNonPayment_TEST {

    static Account testAcc = new Account();
    static List<Product2> lstProd = new List<Product2>();
    static Pricebook2 standardPricebook = new Pricebook2();
    static List<ServiceContract> lstServContr =new List<ServiceContract>();

    @TestSetup
    static void setup(){  

        //Create Account
        testAcc = TestFactory.createAccount('Test1');
        testAcc.BillingPostalCode = '1233456';
        testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        insert testAcc;

        sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
        testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
        update testAcc;

        //Create Products
        lstProd = new list<Product2>{
        new Product2(Name='Prod1',
                     Famille_d_articles__c='Sous-traitance',
                     RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
        };
        insert lstProd;

        //Create Pricebook
        standardPricebook = new Pricebook2(Name = 'PriceBook', IsActive = true);
        insert standardPricebook;      

        //Inserting 5 records in lstServContr
        //2 records having status 'Actif, en retard de paiement' for more than 30 days
        //2 records having status 'Actif, en retard de paiement' for more than 60 days
        //1 reocrds having status 'Actif, en retard de paiement' for less than 30 days
        lstServContr = new list<ServiceContract>{
                new ServiceContract(Name='Service Contract 1',
                                    Pricebook2Id = standardPricebook.Id,
                                    AccountId = testAcc.Id,
                                    Contract_Renewed__c = true,
                                    Contract_Status__c = 'Actif - en retard de paiement',
                                    StartDate = System.Today().addMonths(-6),
                                    TECH_DateStatusActifEnRetard__c = Date.today().addDays(-42)),


                new ServiceContract(Name='Service Contract 2',
                                    Pricebook2Id = standardPricebook.Id,
                                    AccountId = testAcc.Id,
                                    Contract_Renewed__c = false,
                                    Contract_Status__c = 'Actif - en retard de paiement',
                                    TECH_DateStatusActifEnRetard__c = Date.today().addDays(-49)),

                new ServiceContract(Name='Service Contract 3',
                                    Pricebook2Id = standardPricebook.Id,
                                    AccountId = testAcc.Id,
                                    Contract_Renewed__c = true,
                                    Contract_Status__c = 'Actif - en retard de paiement',
                                    TECH_DateStatusActifEnRetard__c = Date.today().addDays(-73)),

                new ServiceContract(Name='Service Contract 4',
                                    Pricebook2Id = standardPricebook.Id,
                                    AccountId = testAcc.Id,
                                    Contract_Renewed__c = true,
                                    Contract_Status__c = 'Actif - en retard de paiement',
                                    TECH_DateStatusActifEnRetard__c = Date.today().addDays(-79)),

                new ServiceContract(Name='Service Contract 5',
                                    Pricebook2Id = standardPricebook.Id,
                                    AccountId = testAcc.Id,
                                    Contract_Renewed__c = false,
                                    Contract_Status__c = 'Actif - en retard de paiement',
                                    TECH_DateStatusActifEnRetard__c = Date.today().addDays(-10))                                    
        };
        insert lstServContr;
        system.debug('## lstServContr.Name :'  + lstServContr[0].Name);
        system.debug('## lstServContr.Status :'  + lstServContr[0].Contract_Status__c);
        system.debug('## lstServContr.Date :'  + lstServContr[0].TECH_DateStatusActifEnRetard__c);
    }

    @isTest
    public static void Test(){
        
        Test.startTest();
            BAT06_ContractNonPayment batch = new BAT06_ContractNonPayment();
            Id batchId = Database.executeBatch(batch);
        Test.stopTest();

        Date dte = Date.today().addDays(-6);

        //Records having status 'Actif, en retard de paiement' for more than 30 days
        System.assertEquals(0, [SELECT COUNT() FROM ServiceContract WHERE Contract_Renewed__c = false AND Contract_Status__c = 'Résiliation pour retard de paiement']);

        //Records having status 'Actif, en retard de paiement' for more than 60 days
        //System.assertEquals(0, [SELECT COUNT() FROM ServiceContract WHERE Contract_Renewed__c = true AND Contract_Status__c = 'Résiliation pour retard de paiement']);

        //Records having status 'Actif, en retard de paiement' for less than 30 days
        System.assertEquals(0, [SELECT COUNT() FROM ServiceContract WHERE Contract_Renewed__c = false AND TECH_DateStatusActifEnRetard__c =: dte]);
        

    }

    @isTest
    public static void myTestMethod() {        
        Test.startTest();
         BAT06_ContractNonPayment myClass = new BAT06_ContractNonPayment ();   
         String chron = '0 0 23 * * ?';                
         System.schedule('Test Sched', chron, myClass);
         Test.stopTest();
    }

}