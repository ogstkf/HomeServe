/**
 * @File Name          : LC01_Acc360RendezVous.cls
 * @Description        : Apex controller for lightning component LC01_Acc360RendezVous
 * @Author             : RRJ
 * @Group              : Spoon Consulting
 * @Last Modified By   : RRJ
 * @Last Modified On   : 29/07/2019, 16:32:33
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    21/06/2019, 15:45:58          RRJ                       Initial Version
**/
public class LC01_Acc360RendezVous {

    @AuraEnabled
    public static List<RdvDetail> fetchRDV(String accId){
        List<ServiceAppointment> lstSrvApp = [SELECT Id, AppointmentNumber, Status, SchedStartTime, SchedEndTime, WorkType.Name, Street, City, State, Country, PostalCode, ParentRecordId, EarliestStartTime, DueDate FROM ServiceAppointment WHERE AccountId = :accId AND (DueDate >= TODAY OR SchedStartTime >= TODAY ) AND (StatusCategory != :AP_Constant.servAppStatCatCancelled OR StatusCategory != :AP_Constant.servAppStatCatCompleted) AND (Status = 'None' OR Status = 'Scheduled' OR Status = 'Dispatched' OR Status = 'In Progress')];
        Set<Id> setWrkOrderId = new Set<Id>();
        for(ServiceAppointment srvApp : lstSrvApp){
            if(srvApp.ParentRecordId.getSObjectType().getDescribe().getName() == 'WorkOrder'){
                setWrkOrderId.add(srvApp.ParentRecordId);
            }
        }
        Map<Id, WorkOrder> mapWrkOrder = new Map<Id, WorkOrder>([SELECT Id, Case.AssetId, Case.Asset.Name FROM WorkOrder WHERE Id IN :setWrkOrderId]);
        List<RdvDetail> lstData = new List<RdvDetail>();
        for(ServiceAppointment srvApp : lstSrvApp){
            if(mapWrkOrder.containsKey(srvApp.ParentRecordId)){
                lstData.add(new RdvDetail(srvApp, mapWrkOrder.get(srvApp.ParentRecordId)));
            }else{
                lstData.add(new RdvDetail(srvApp));
            }
        }
        System.debug('#### lstData: '+lstData);
        return lstData;
    }

    public class RdvDetail{
        @AuraEnabled public ServiceAppointment srvApp;
        @AuraEnabled public WorkOrder wrkOrder;
        @AuraEnabled public String statusLabel;

        public RdvDetail(ServiceAppointment srvApp, WorkOrder wrkOrder){
            this.srvApp = srvApp;
            this.wrkOrder = wrkOrder;
            this.statusLabel = getPicklistLabel('ServiceAppointment', 'Status', srvApp.Status);
        }

        public RdvDetail(ServiceAppointment srvAp){
            this.srvApp = srvApp;
            this.statusLabel = getPicklistLabel('ServiceAppointment', 'Status', srvApp.Status);
        }
    }

    @AuraEnabled
    public static void annulerRdv(Id rdvId){
        try{
            ServiceAppointment servApp = new ServiceAppointment(
                Id = rdvId,
                Status = AP_Constant.servAppStatusCancelled
            );
            update servApp;
        }catch(Exception e){
            System.debug('##### LC01_Acc360RendezVous ERROR: '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static String getPicklistLabel(String strObj, String strFld, String strPickValue){
        for(Schema.PicklistEntry pickEntry : Schema.getGlobalDescribe().get('ServiceAppointment').getDescribe().fields.getMap().get('Status').getDescribe().getPicklistValues()){
            if(pickEntry.getValue() == strPickValue){
                return pickEntry.getLabel();
            }
        }
        return null;
    }
}