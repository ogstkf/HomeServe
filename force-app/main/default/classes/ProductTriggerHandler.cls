/**
 * @File Name          : ProductTriggerHandler.cls
 * @Description        : 
 * @Author             : DMG
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 11-05-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    05/12/2019         DMG                    Initial Version
 * 1.1    22/01/2020		 SH						Corrected problems on SOQL inside for loops and simplify get RecordTypeId
 * 1.2    22/01/2020         SH						Added Classification and Classe Rotation changes on ProductItem
 * 1.3    24/06/2020         DMU                    Commented AP62 due to homeserve project
 * 1.4    05/11/2020         ZJO                    Added before Insert method (Tec-218)
**/
public with sharing class ProductTriggerHandler {
	Bypass__c userBypass;

	public ProductTriggerHandler() {
		this.userBypass = Bypass__c.getInstance(UserInfo.getUserId());
	}

	public void handleBeforeInsert(List<Product2> lstNewProd){

		List<Product2> lstProd = new List<Product2>();

		for(Integer i=0; i < lstNewProd.size(); i++){
			if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP76')){
				if(lstNewProd[i].ProductCode <> null){
					lstProd.add(lstNewProd[i]);
				}
			}
		}

		if(lstProd.size() > 0){
			AP76_SetUniqueProductCode.setProductCode(lstProd);
		}		
	}

	public void handleBeforeUpdate(List<Product2> lstProOld, List<Product2> lstProNew){

		List<Product2> lstProdToUpdate = new List<Product2>();

		for(Integer i=0; i < lstProNew.size(); i++){
			if((userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP76')) && lstProOld[i].ProductCode <> lstProNew[i].ProductCode){
				lstProdToUpdate.add(lstProNew[i]);	
			}
		}

		if(lstProdToUpdate.size() > 0){
			AP76_SetUniqueProductCode.setProductCode(lstProdToUpdate);
		}		
	}


	public void handleAfterInsert(List<Product2> lstProNew) {
        System.debug('##### ProductTriggerHandler -> handleAfterInsert ' + lstProNew.size());
		List<Product2> lstProduits = new List<Product2>();
		List<Product2> lstProduits2 = new List<Product2>();
		
		set<string> setFamilledarticles = new set<string>();//a set of Famille_d_articles__c
		setFamilledarticles.add(Label.Produit_Famille_d_articles_1);
		setFamilledarticles.add(Label.Produit_Famille_d_articles_2);
		setFamilledarticles.add(Label.Produit_Famille_d_articles_3);
		setFamilledarticles.add(Label.Produit_Famille_d_articles_4);
		setFamilledarticles.add(Label.Produit_Famille_d_articles_5);

		// Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Product2.getRecordTypeInfosByName(); // SH 
		// Id RCT_Article = recordTypes.get('Référentiel Article').getRecordTypeId(); // SH 
		Id RCT_Article = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId(); // SH

		for(Integer i = 0; i < lstProNew.size(); i ++) {
			System.debug ('##### lstProNew Product size' + lstProNew.size());

			if(
				(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP51')) 
				&& lstProNew[i].IsActive == true 
				&& setFamilledarticles.contains(lstProNew[i].Famille_d_articles__c)
				&& lstProNew[i].recordTypeId == RCT_Article
				) 
			{
				lstProduits.add(lstProNew [i]);
				System.debug ('##### Product Added');
			}
			if(
				(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP63')) 
				&& lstProNew[i].IsBundle__c == true 
				) 
			{
				lstProduits2.add(lstProNew[i]);
			}			
	    }

		//DMG AP51 - > Create PriceBookEntry (standard)
		//SH this part of code was inside the for loop
		if(lstProduits.size() > 0) {
			AP51_AddPricebookEntryAuto.createPriceBookEntry(lstProduits);
		}
		//LGO AP63
		if(lstProduits2.size() > 0) {
			AP63_UpdateActifVE.updateActifVEFromCLI(lstProduits2);
		}		
    }

	public void handleAfterUpdate(List<Product2> lstProOld, List<Product2> lstProNew) {
         System.debug ('##### ProductTriggerHandler -> handleAfterUpdate ' + lstProNew.size());
		List<Product2> lstProduits = new List<Product2>();
		List<Product2> lstProduits2 = new List<Product2>();
		
		// List<Product2> lstProductsThatBecomeNePasStocker = new List<Product2>();
		// List<Product2> lstProductsThatBecomeAStocker = new List<Product2>();

		// Map<String, Schema.RecordTypeInfo> recordTypes = Schema.SObjectType.Product2.getRecordTypeInfosByName(); // SH 
		// Id RCT_Article = recordTypes.get('Référentiel Article').getRecordTypeId(); // SH 
		Id RCT_Article = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId(); // SH

		for(Integer i = 0; i < lstProNew.size(); i ++) {
			// If a product becomes Active, check that is has a PBE (and create if necessary)
			if((userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP51')) && 
				lstProNew[i].recordTypeId == RCT_Article && 
				lstProOld[i].IsActive == false && 
				lstProNew[i].IsActive == true) 
			{
				lstProduits.add(lstProNew [i]);
			}

			// If the Classification of a Product changes TO 'Ne_pas_stocker'
			// if((userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP62')) &&
			// 	lstProNew[i].recordTypeId == RCT_Article &&
			// 	lstProOld[i].Classification__c != 'Ne pas stocker' &&
			// 	lstProNew[i].Classification__c == 'Ne pas stocker') 
			// {
			// 	lstProductsThatBecomeNePasStocker.add(lstProNew [i]);
			// }

			// If the Classification of a Product changes FROM 'Ne_pas_stocker'
			// if((userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP62')) &&
			// 	lstProNew[i].recordTypeId == RCT_Article &&
			// 	lstProOld[i].Classification__c == 'Ne pas stocker' &&
			// 	lstProNew[i].Classification__c != 'Ne pas stocker') 
			// {
			// 	lstProductsThatBecomeAStocker.add(lstProNew [i]);
			// }
			if(
				(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP63')) 
				&& lstProNew[i].IsBundle__c == true 
				&& lstProOld[i].IsBundle__c == false
				) 
			{
				lstProduits2.add(lstProNew[i]);
			}
		}

		//DMG AP51 - Check is PriceBookEntry exist else Create PriceBookEntry (standard)
		//SH this part of code was inside the for loop
		if(lstProduits.size() > 0) {
			AP51_AddPricebookEntryAuto.checkPriceBookEntry(lstProduits);
		}

		//DMU 20200624 - Commented AP62 due to homeserve project
		// SH AP62 - Change "Classification" to "Ne pas stocker" AND "Classe de Rotation" to "D"
		// if(lstProductsThatBecomeNePasStocker.size() > 0) {
		// 	AP62_ChangeProducItemValues.changeValues(lstProductsThatBecomeNePasStocker, 'Ne pas stocker', 'D');
		// }

		// // SH AP62 - Change "Classification" to "A stocker" AND "Classe de Rotation" to "D"
		// if(lstProductsThatBecomeAStocker.size() > 0) {
		// 	AP62_ChangeProducItemValues.changeValues(lstProductsThatBecomeAStocker, 'A stocker', 'A');
		// }
	
		//LGO AP63
		if(lstProduits2.size() > 0) {
			AP63_UpdateActifVE.updateActifVEFromCLI(lstProduits2);
		}
	}
}