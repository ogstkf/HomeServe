/**
 * @File Name          : LC53_GenerateDevis_TEST.cls
 * @Description        : TestClass
 * @Author             : Spoon Consulting
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         09-12-2019     		    DMG         Initial Version
**/
@isTest
public with sharing class LC53_GenerateDevis_TEST {
static User mainUser;
    static Account testAcc = new Account();
    static Opportunity opp = new Opportunity();
    static WorkOrder wrkOrd = new WorkOrder();
    static list<Product2> lstProd = new list<Product2>();
    static Schema.Location loc = new Schema.Location();
    static OperatingHours oh = new OperatingHours();
    static sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c();
	static ServiceTerritory st = new ServiceTerritory();
	static ServiceTerritoryLocation stl = new ServiceTerritoryLocation();    
    static Quote quo = new Quote();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static list<QuoteLineItem> lstQli = new list<QuoteLineItem>();
	static ProductItem pi = new ProductItem(); 
    static ProductRequestLineItem prli = new ProductRequestLineItem();
    
    static{
        
        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){
			
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);            
            insert opp;
            
            //work order
            wrkOrd = TestFactory.createWorkOrder();
            insert wrkOrd;
                      
            //create Products
            lstProd = new list<Product2>{ TestFactory.createProduct('Prod1')
                             
                /*new Product2(Name='Prod2',
                             Famille_d_articles__c='Appareils',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                    
                new Product2(Name='Prod3', 
                             Famille_d_articles__c='Consommables',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                    
                new Product2(Name='Prod4', 
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())*/
            };
            // lstProd[0].Famille_d_articles__c='Accessoires';
            insert lstProd;
            
            //create Location
            loc = new Schema.Location(Name='Test1',
                                      LocationType='Entrepôt',
                                      ParentLocationId=NULL,
                                      IsInventoryLocation=true);
            insert loc;

			//create Operating Hours
			oh = new OperatingHours(Name='Test1');
			insert oh;

			//create Corporate Name
			sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(Name='Test1',
                                                                                  sofactoapp__Credit_prefix__c='Test1',
                                                                                  sofactoapp__Invoice_prefix__c='Test1');  
            insert srs;
            
            //create Service Territory
            st = new ServiceTerritory(Name='Test1',
                                      OperatingHoursId=oh.Id,
                                      Sofactoapp_Raison_Social__c=srs.Id);
            insert st;
            
            //create Service Territory Location
            stl = new ServiceTerritoryLocation(ServiceTerritoryId=st.Id,
                                               LocationId=loc.Id);
            insert stl;
            
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            insert PrcBkEnt;
            
            //create Quote
            quo = new Quote(Name ='Test1',
                            OpportunityId=opp.Id,
                            Agency__c=st.Id,
                            Pricebook2Id = lstPrcBk[0].Id,
                            Ordre_d_execution__c = wrkOrd.Id,
                            Signature_Client__c = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAB7AXwDASIAAhEBAxEB/8QAHAABAQACAwEBAAAAAAAAAAAAAAYFBwECAwQJ/8QAPRAAAQQBAgMFBAcFCQEAAAAAAAECAwQFBhEHEiExQVFhgRMUMnEVIkJicpGhFiMzY7EkJTQ1Q0VSU4Ki/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAIDBAUBBv/EADgRAAIBAwICBwUHAwUAAAAAAAABAgMEERIhQVEFExUxYXGBFCKRobEyQlJyosHRI5LwYpOy4fH/2gAMAwEAAhEDEQA/AP1TAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMFqfV+M0wyGGZslvI3FVlLH105p7Lvup3NTvcvREK6lWFGLnUeEi2jQqXE1TpLLf8An/r4GVvX6OMqyXsjbirV4k3fLK9GtanmqkoutMxqJVh0HhveYd1auSuosVZPNifFJ6bIcUNE289di1DxAkjuWWKj6uNYu9Sl6f6j/F6+iIhZMY2NqMY1GtamyIibIiGVdfc7v3I/qf7Ly3fimb37JZbJdbP9C8uMvPKXg1uRkmlOIVyL+18SXV5N1XapjY0anluq7qcJV4qYLdYMljNRwNToyxH7rOv/AKbu1V+aFsD32GC3jKSfPVJ/Jtr5HnatV+7OEHHloivnFKXwZF0OJ2PZcbi9V4q5p249eViXERYJF+7Kn1V9diyY9sjUexyOa5N0VF3RUPG9Qo5Ku6pkKkViF6bOZKxHIv5kbNozPaVkW/w/ya+7t+s/C3Hq6u9PCN3bEvy3TyPNVzbfb9+PNbSXp3P0w+SZLRZ3m1N9VPk3mD8n3x9crnJF0Ce07rTHZ2Z2MsRSY3Lwt3mx1r6srfNvc9v3m7p8ihNVKrCtHXTeUYK9vUtp9XVWH/m65rk1swACwpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABK6v4l6S0U+OnlL7p8lY6V8ZTYs9udV7EbE3r18V2TzKq1enbw6yrJRXNl9ta1ryoqVvByk+CWSqMDqTW+mdKKyLL5JqWpv4NOFqy2ZvwRMRXL+WxOVXcT9bbS2o26LxD0/hNVs+Tlb5uVPZwfJEc5PFDJMweiuGOFyOpnVUb7tC6xbvWHrLZm2T7Uj1VzlVeiJv2qY5XVWrBzpLTFfekmtuajs/jp9TpQsKFCoqdxLXNvChTae74Oe8f7dXLYmdWcWtYY2pXfg9APjnyEqQY+HKWUZYtPX/jBHuqIidXK5zeVO0ab4SaoTI2NVaq1/dXNZFGrP7jFGxsLdl/cxvc1XtYir9lU323VD34TY7K6ofJxX1hArcjlWq3GVnJ9WhR33Y1qL2Od8Tl7+ngbOMVpaPpBK6upSku+Ce2Fzajjd8nnC278nS6Q6QXRDlYWMIxktqkktWX+FOWraLW7WNT37sEdX4a1YmK2zq/VtlV7Vfm52/o1yIHcK9Myv57FzOTr/Ny9h/9XFiDpdn2uMOmvqcXte+zlVZLyePoR0/CjSU7Gt/vOJW9josjMx35o7c7pw9fW3+jNbamqpt0R+QdYRPkkvMU1zIUMcxJcher1mKuyOmlaxFX5qp1qZTGX/8AA5GrZ7/3MzX/ANFI+x2ilhRSfhs/luT7S6QcNUpycfHdfPKJiXBcSqf1sZrqpcRvVGZLGMXm6diuhWPb5nRuqtdYhUZqTQrrcadtrC2GzJt4rFJyuT5IrlLQEvY3HelUkvXV/wAs/LBFdIqaxXpQkvLS/wBGn55IHL5XhtrpsVDJ5NMdk4156q2eajeryJ9qP2iNdunlunzQ+nFaizGl70WndcztmhnckeOzTW8kdhV6JFN3Ry+C9Gv7tl6FTksRisxXdUyuNrXIX/FHPEj2r6KRuU4PYOWnPT0/kr2HhsNVktNj/b0ZGqmytdWl5o0TbvajVTuUyVqF3Tl11JJy8Pdz4NPKfg8pry2e+3urCtT9mrylGHBPEtL5xksNeK0yT474avgagx+odb8GqcWO4gsk1HgXTtr08vQYqzVUcuzWWI5HKqt3VER/O5U7F33Q2HgtbaW1HM6nisxC65H/ABaUqLDai/HDIiSN9Wmi26QpV31c/cqcYvv9Oa8VlGS96Hr2qdWn/UpcJx3i148YvmpYaM4ADeckAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE/q7XmlNC0vftT5mCm12/so1XmllXwYxN3OX5IUBibOlNN3M7Dqa3hKk+UrxJDFakiR0kbN1XZqr2dVXqnXqU3HXOGKGNXjnC9F3+WV5mm0dsqqd2pOHKOE34ZeceeHjkzXX0pxb4qNVmDqyaF05L0W9aYjsnYZ/Lj7IkVO9eveilfojhhpHQLJJcNRdNkLHW1krb1mt2HL2q+R3Xr4JsnkVgMlDo6nTmq9ZupU/FLh+Vd0fRZ5tm+66Zq1aTtraKpUn92PH88n7035vC4JAkOJ+irevdOxYSrcrwoy5DZljssc6KwxiqqxP5VReVenZ4FeDXcUIXNKVGosxezOfaXVWyrxuKLxKLyvMjkw3FGWvHB+2OnMe1iIm1TBSvcjU7kdJZVP/k7Jo3Vc7ZEvcV9QJz/AGadPHwtangnNXe715ivBT7DS4uX98v5wae1K/CMP9un9dOSNXhqkkKQ2de6ym27X/SyxOX1ja39DsvCvS0sbY79rUV9Grvtb1HkJWqvmxZuVfyLADs+14wT89/rkdr3y+zVcfJ6fpgkq/CXhhWc6RNAYGWRzudZbFCOeRV8eeRFd+p7z8MOGtlFSxw+03Jv2q7FQKv58pTAkrG1SwqcceS/gi+lL5vU608/mf8AJFrwg0LFyuxNC5hpGb+zfisjYqcm/g2N6NVPJUVPI7x6Y11h/wDJNfuvxN+GvnKTJ+ngksPsn+rudfmWII9n28d6cdP5cx+mM+pN9LXk9qs9a/1pT+GpPHpgjZdV61w3XUGgZbULfisYO0ltOztWJ6RyejWu9TJYLX2k9RTOqY7MRtuR9JKdhrq9ln4oZEa9PVCgMXndL6c1NEyHP4SnfSNd43TRI50a+LHdrV80VFHVXNPenPV4SX7rGPVMKvZVtqtJwfODfzjJvPpKJ9l+hSy1GfHZCtHZq2Y1jliem7XsVNlRUNcy6ewkGRq6H17jI8lReu2Ay1jf27FTr7s6ZF52ytT4XIqK9qdVVyKq5z9gL+LZtpHWuXxrW/BWtOS9XRO5Npf3u3kkiIYHUlfinaxk+Hz+kcRqKpL1Sxirq1bESou7Xtjl7Houyps/uMN7Jyjqq0nqXhqi/B4y8cm47Pfmn1OjIxhLRQuFpfN9XOL4STk1HK4pTeVtyazTMBrfSac2mc67PUG/7bmJN52t8IrSdfSVHfiQyWD13hsvd+hrbJ8Vl2pu7HX2pHMqeLOqtkb95iqhBab4418BVTA8TsZmsXlaqLyyzY+R/vMKLs2VfZo5qL2c3XbfsUzOQ17wX19UTG5HU+Le5r94Vln9hPFJ3PjcuzmuTxQqo39tpTtqyT/BN93hv70X8VyRfc9FXupq9t5SXCrTjlNc9lpmuPCT4y4GyAa6rauuaLiYmay0eewHZHloXtdPA3u9uxvxIn/Y1PmidpsGvYhtQR2a0jZIpWo9j2rujmr2Kh1re6hcZS2ku9cf+14rY+fu7GraYlLeL7pLuf7prinho9AAaTEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATOtdO2MnDXzWGVI81iHLPTf3PT7cTvFrk6fkoo4fSmscLBkclpKg73piukhtUmK+N+6o5F3TfdFRepTAzO1g6jm1lPvWNm+fw28duRtjfVY0Y002nF7NNppcV5Z3XJ55kPDwS4VV51sQaKose7fflV6Iu/lzbfoWkEENWFlevE2OKJqMYxqbI1E7ERD0BKja0LbPUwUc8kl9CFzfXV7j2mrKeO7VJvHxYABeZQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/9k=');
            insert quo;          
                       
            //create Quote Line Items
            lstQli = new list<QuoteLineItem>{
            	new QuoteLineItem(QuoteId=quo.Id, 
                                  Product2Id=lstProd[0].Id, 
                                  Quantity=decimal.valueOf(2), 
                                  UnitPrice=decimal.valueOf(145),
                                  PriceBookEntryID=PrcBkEnt.Id)
            };
            insert lstQli;
            
            pi = new ProductItem(Product2Id=lstProd[0].Id,
                                 LocationId=loc.Id,
                                 QuantityOnHand=decimal.valueOf(2));
            insert pi;
            
            
        }
        
    }  




    @isTest
    public static void testSaveDevis(){
        System.runAs(mainUser){
            Test.startTest();
				PageReference pageRef = Page.VFP10_Devis; 
				pageRef.getParameters().put('id', quo.Id);
				Test.setCurrentPage(pageRef);

                VFC10_Devis devis = new VFC10_Devis();
                map<string,Object> mapOfResult =  LC53_GenerateDevis.saveDevis(quo.Id,true);
            Test.stopTest();

        }
    }
    @isTest
    public static void testinsertAtt(){
        System.runAs(mainUser){
            Test.startTest();
                map<string,Object> mapOfResult = LC53_GenerateDevis.insertImageAttachment(quo.Id);
            Test.stopTest();

        }
    }

    @isTest
    public static void testfetchQuoDetails(){
        System.runAs(mainUser){
            Test.startTest();
                Quote quo = LC53_GenerateDevis.fetchQuoDetails(quo.Id);
            Test.stopTest();

        }
    }
}