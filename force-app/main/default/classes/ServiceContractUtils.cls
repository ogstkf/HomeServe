public with sharing class ServiceContractUtils {
     /**
    *@description Set de la map IDindice -> indice
    *@return indexes Map<Id, Valeur_indice__c> 
    */
    public static Map<Id, sofactoapp__Valeur_indice__c> indexes {
        get {
            if (indexes == null) {
                indexes = new Map<Id, sofactoapp__Valeur_indice__c>(
                    [SELECT Id, sofactoapp__Coefficient__c, sofactoapp__Debut__c FROM sofactoapp__Valeur_indice__c LIMIT 10000]
                );
            }

            return indexes;
        }
        private set;
    } 
    
    // 
    /**
    *@description Recherche d'un indice de révision pour une liste de contrats
    *@param p_subIds : Set D'id de contrats
    */
    public static void findIndex (Set<Id> p_subIds) {
        findIndex (p_subIds, null);
    }
    
     /**
    *@description Recherche d'un indice de révision pour une liste de contrats en excluant certaines valeurs d'indice
    *@param p_subIds : Set contrats 
    *@param p_excludedValues : Set contrats a exclure
    */
    public static void findIndex (Set<Id> p_subIds, Set<Id> p_excludedValues) {
 
        List<ServiceContract> lst_subForIndexCalculation = 
            [SELECT Id, Indice_1__c, 
            Date_pour_indice_de_reference_1__c, Valeur_d_indice_reference1__c, Coefficient_indice_de_reference__c
            FROM ServiceContract
            WHERE Id IN :p_subIds
            
            AND Indice_1__c != null
            AND Indice_1__r.sofactoapp__Actif__c  = true
            AND Date_pour_indice_de_reference_1__c != null];

        Date lv_startDateForIndexComputing = null;
        Set<Id> set_indexeIds = new Set<Id>();

        for (ServiceContract lv_sub : lst_subForIndexCalculation) {
            set_indexeIds.add(lv_sub.Indice_1__c);
            if (lv_startDateForIndexComputing == null || 
                    lv_sub.Date_pour_indice_de_reference_1__c < lv_startDateForIndexComputing) 
            {
                lv_startDateForIndexComputing = lv_sub.Date_pour_indice_de_reference_1__c;
            }
        }
        
        findIndex(lst_subForIndexCalculation, set_indexeIds, lv_startDateForIndexComputing, true, p_excludedValues);
    }

    /**
    *@description Recherche d'un indice de révision pour une liste de contrats, sans transaction en base de données car méthode appelée en pré-insert/update
    *@param p_subs : List de contrats
    */
    public static void findIndex (List<ServiceContract> p_subs) {
        
        Date lv_startDateForIndexComputing = null;
        Set<Id> set_indexeIds = new Set<Id>();

        for (ServiceContract lv_sub : p_subs) {    
            set_indexeIds.add(lv_sub.Indice_1__c);
            if (lv_startDateForIndexComputing == null || 
                    lv_sub.Date_pour_indice_de_reference_1__c < lv_startDateForIndexComputing) 
            {
                lv_startDateForIndexComputing = lv_sub.Date_pour_indice_de_reference_1__c;
            }
        }
        
        findIndex(p_subs, set_indexeIds, lv_startDateForIndexComputing, false);
    }

    /**
    *@description Recherche des valeurs d'indice sur abonnement, à partir d'une date donnée
    *@param p_startDateForIndexComputing : Date de départ pour la recherche
    */
    public static void findIndex (Map<Id, Date> p_startDateForIndexComputing) {
        
        
        if (p_startDateForIndexComputing != null) {
           
            // Récupération des factures elligibles à une mise à jour de leur taux de conversion
            String lv_query = 'SELECT Id, Date_indice_reference__c, Valeur_indice_reference__c, Coefficient_indice_reference__c, Indice__c';
            lv_query += ' FROM ServiceContract WHERE';
            Boolean lv_OrContraintAdded = false;
            for (String lv_index : p_startDateForIndexComputing.keySet()) {
                if (!lv_OrContraintAdded) {
                    lv_query += ' (';
                    lv_OrContraintAdded = true;
                }
                lv_query += ' ( Indice_1__c = ';
                lv_query += '\'' + lv_index + '\'';
                lv_query += ' AND Date_indice_reference__c >= ';
                Date lv_date = p_startDateForIndexComputing.get(lv_index);
                DateTime lv_dateConverted = Datetime.newInstance(lv_date.year(),lv_date.month(),lv_date.day(),0,0,0);
                lv_query += lv_dateConverted.format('yyyy-MM-dd');
                lv_query += ')';
                lv_query += ' OR ';
            }

            // On enlève le dernier OR
            lv_query = lv_query.substringBeforeLast('OR');

            // Ajout des conditions sur l'état de la facture
            if (lv_OrContraintAdded) {
                lv_query += ') AND ';
            }
            
            lv_query += ' IsInterrupted__c = false';
            lv_query += ' ORDER BY Date_indice_reference__c ASC';

            System.debug('*** ABO FIND INDEX STARTDATE > CPU CHECK #2 *** ' + Limits.getCpuTime());

            //System.debug('AbonnementUtils.findIndex[458] > lv_query > ' + lv_query);

            // On vérifie 
            List<ServiceContract> lst_subs = 
                (List<ServiceContract>)Database.query(lv_query);
            System.debug('*** ABO FIND INDEX STARTDATE > CPU CHECK #3 *** ' + Limits.getCpuTime());

            if (!lst_subs.isEmpty()) {
                
                String lv_queryOnIndexes = 'SELECT Id, Indice_1__c, sofactoapp__Debut__c, Coefficient__c FROM Valeur_Indice_1__c WHERE';
                
                for (String lv_index : p_startDateForIndexComputing.keySet()) {
                    lv_queryOnIndexes += ' ( Indice_1__c = ';
                    lv_queryOnIndexes += '\'' + lv_index + '\'';
                    lv_queryOnIndexes += ' AND sofactoapp__Debut__c >= ';
                    Date lv_date = p_startDateForIndexComputing.get(lv_index);
                    DateTime lv_dateConverted = Datetime.newInstance(lv_date.year(),lv_date.month(),lv_date.day(),0,0,0);
                    lv_queryOnIndexes += lv_dateConverted.format('yyyy-MM-dd');
                    lv_queryOnIndexes += ')';
                    lv_queryOnIndexes += ' OR ';
                }

                // On enlève le dernier OR
                lv_queryOnIndexes = lv_queryOnIndexes.substringBeforeLast('OR');
                lv_queryOnIndexes += ' ORDER BY sofactoapp__Debut__c ASC';

                //System.debug('AbonnementUtils.findIndex[439] > lv_queryOnIndexes > ' + lv_queryOnIndexes);
                System.debug('*** ABO FIND INDEX STARTDATE > CPU CHECK #4 *** ' + Limits.getCpuTime());
                List<sofactoapp__Valeur_indice__c> lst_indexes = Database.query(lv_queryOnIndexes);
                System.debug('*** ABO FIND INDEX STARTDATE > CPU CHECK #5 *** ' + Limits.getCpuTime());
                Map<Id, List<sofactoapp__Valeur_indice__c>> map_indexes = new Map<Id, List<sofactoapp__Valeur_indice__c>>();

                // Récupération des coefficients en fonction des dates, pour chaque indice
                for (sofactoapp__Valeur_indice__c lv_value : lst_indexes) {
                    if (map_indexes.get(lv_value.sofactoapp__Indice__c) == null) {
                        map_indexes.put(lv_value.sofactoapp__Indice__c, new List<sofactoapp__Valeur_indice__c>());
                    }
                    map_indexes.get(lv_value.sofactoapp__Indice__c).add(lv_value);
                }

                // Enfin, on détermine le taux applicable pour chaque facture
                for (ServiceContract lv_sub : lst_subs) {
                    lv_sub.Valeur_d_indice_reference1__c = null;
                    lv_sub.Coefficient_indice_de_reference__c = null;

                    Date lv_indexDate = null;
                    for (sofactoapp__Valeur_indice__c lv_value : map_indexes.get(lv_sub.Indice_1__c)) {
                        if (lv_value.sofactoapp__Debut__c <= lv_sub.Date_pour_indice_de_reference_1__c) {
                            lv_sub.Valeur_d_indice_reference1__c = lv_value.Id;
                            lv_sub.Coefficient_indice_de_reference__c = lv_value.sofactoapp__Coefficient__c;
                        } else {
                            break;
                        }
                    }
                }

                System.debug('*** ABO FIND INDEX STARTDATE > CPU CHECK #6 *** ' + Limits.getCpuTime());
                //FacturesClientUtils.byPassVatCalculation = true;
                List<Database.SaveResult> lst_results = Database.update(lst_subs, false);
                //FacturesClientUtils.byPassVatCalculation = false;
                System.debug('*** ABO FIND INDEX STARTDATE > CPU CHECK #7 *** ' + Limits.getCpuTime());

                for (Integer i=0; i<lst_results.size(); i++) {
                    Database.SaveResult lv_result = lst_results.get(i);
                    if (!lv_result.isSuccess()) {
                        // Operation failed, so get all errors 
                        String lv_msg = 'ServiceContractUtils.findIndex[517] Error while updating index for invoice ';
                        lv_msg += lst_subs.get(i).Name;
                        lv_msg += ' : '; 
                        for (Database.Error lv_err : lv_result.getErrors()) {
                            lv_msg += '\n';
                            lv_msg += lv_err.getMessage();
                        }
                        System.debug(lv_msg);
                    }
                }
            }  
        } 
    }
/**
    *@description Recherche d'un indice de révision pour une liste de facture
    *@param p_subs : List de contrats 
    *@param p_indexeIds : Set d'index ID
    *@param p_startDateForIndexComputing : Date de départ de recherche 
    *@param p_commit : commit en base
    */
    public static void findIndex (List<ServiceContract> p_subs, Set<Id> p_indexeIds, Date p_startDateForIndexComputing, Boolean p_commit) {
        findIndex (p_subs, p_indexeIds, p_startDateForIndexComputing, p_commit, null);
    }
        /**
    *@description Recherche d'un indice de révision pour une liste de facture
    *@param p_subs : List de contra
    *@param p_indexeIds : Set d'index ID
    *@param p_startDateForIndexComputing : Date de départ de recherche 
    *@param p_commit : commit en base
    *@param p_excludedValues : Set d'ID a exclure
    */
    public static void findIndex (List<ServiceContract> p_subs, Set<Id> p_indexeIds, Date p_startDateForIndexComputing, Boolean p_commit, Set<Id> p_excludedValues) {
        

        if (p_startDateForIndexComputing != null && p_subs != null) {

            //Map<Id, Indice__c> map_indexesMIN = new Map<Id, Indice__c>(
            //    [SELECT Id, 
            //    (
            //        SELECT Id, Debut__c, Coefficient__c
            //        FROM Valeurs__r
            //        WHERE Debut__c <= :p_startDateForIndexComputing
            //        AND IsDeleted = false
            //        AND Id NOT IN :p_excludedValues
            //        ORDER BY Debut__c DESC
            //        LIMIT 1
            //    )
            //    FROM Indice__c
            //    WHERE Id IN :p_indexeIds
            //    AND Actif__c = true]
            //);

            Map<Id, List<sofactoapp__Valeur_indice__c>> map_indexes = new Map<Id, List<sofactoapp__Valeur_indice__c>>();
            
            for (sofactoapp__Valeur_indice__c lv_value : 
                    [SELECT Id, sofactoapp__Debut__c, sofactoapp__Coefficient__c, sofactoapp__Indice__c
                    FROM sofactoapp__Valeur_indice__c
                    WHERE sofactoapp__Debut__c <= :p_startDateForIndexComputing
                    AND IsDeleted = false
                    AND Id NOT IN :p_excludedValues
                    AND sofactoapp__Indice__c IN :p_indexeIds
                    AND sofactoapp__Indice__r.sofactoapp__Actif__c = true
                    ORDER BY sofactoapp__Debut__c DESC]) 
            {
                if (map_indexes.get(lv_value.sofactoapp__Indice__c) == null) {
                    map_indexes.put(lv_value.sofactoapp__Indice__c, new List<sofactoapp__Valeur_indice__c>());    
                    map_indexes.get(lv_value.sofactoapp__Indice__c).add(lv_value);
                }
                
                //if (map_indexesMIN.get(lv_index).Valeurs__r.isEmpty() == false) {
                //    map_indexes.get(lv_index).add(map_indexesMIN.get(lv_index).Valeurs__r.get(0));
                //}
            }

            //for (Indice__c lv_index : 
            //        [SELECT Id, 
            //    (
            //        SELECT Id, Debut__c, Coefficient__c
            //        FROM Valeurs__r
            //        WHERE Debut__c > :p_startDateForIndexComputing
            //        AND IsDeleted = false
            //        AND Id NOT IN :p_excludedValues
            //        ORDER BY Debut__c ASC
            //    )
            //    FROM Indice__c
            //    WHERE Id IN :p_indexeIds
            //    AND Actif__c = true]) 
            for (sofactoapp__Valeur_indice__c lv_value : 
                    [SELECT Id, sofactoapp__Debut__c, sofactoapp__Coefficient__c , sofactoapp__Indice__c
                    FROM sofactoapp__Valeur_indice__c
                    WHERE sofactoapp__Debut__c > :p_startDateForIndexComputing
                    AND IsDeleted = false
                    AND Id NOT IN :p_excludedValues
                    AND sofactoapp__Indice__c IN :p_indexeIds
                    AND sofactoapp__Indice__r.sofactoapp__Actif__c = true
                    ORDER BY sofactoapp__Debut__c ASC]) 
            {
                if (map_indexes.get(lv_value.sofactoapp__Indice__c) == null) {
                    map_indexes.put(lv_value.sofactoapp__Indice__c, new List<sofactoapp__Valeur_indice__c>());
                }
                map_indexes.get(lv_value.sofactoapp__Indice__c).add(lv_value);
            }

            // Enfin, on détermine le taux applicable pour chaque facture
            if (map_indexes.isEmpty() == false) {
                for (ServiceContract lv_sub : p_subs) {

                    lv_sub.Valeur_d_indice_reference1__c = null;
                    lv_sub.Coefficient_indice_de_reference__c = null; 

                    if (map_indexes.get(lv_sub.Indice_1__c) != null) {

                        Date lv_indexDate = null;
                        for (sofactoapp__Valeur_indice__c lv_value : map_indexes.get(lv_sub.Indice_1__c)) {
                            if (lv_value.sofactoapp__Debut__c <= lv_sub.Date_pour_indice_de_reference_1__c) {
                                lv_sub.Valeur_d_indice_reference1__c = lv_value.Id;
                                lv_sub.Coefficient_indice_de_reference__c = lv_value.sofactoapp__Coefficient__c;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }

            if (p_commit) {
                List<Database.SaveResult> lst_results = Database.update(p_subs, false);
                for (Integer i=0; i<lst_results.size(); i++) {
                    Database.SaveResult lv_result = lst_results.get(i);
                    if (!lv_result.isSuccess()) {
                        // Operation failed, so get all errors 
                        String lv_msg = 'AbonnementUtils[367] Error while updating index subscription ';
                        lv_msg += p_subs.get(i).Name;
                        lv_msg += ' : '; 
                        for (Database.Error lv_err : lv_result.getErrors()) {
                            lv_msg += '\n';
                            lv_msg += lv_err.getMessage();
                        }
                        System.debug(lv_msg);
                    }
                }
            }
        }
    }

}