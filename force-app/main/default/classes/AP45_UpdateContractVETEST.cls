/**
 * @File Name          : AP45_UpdateContractVETEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 04-05-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    22/11/2019   AMO     Initial Version
**/
@isTest
public with sharing class AP45_UpdateContractVETEST {

    static User mainUser;
    static List<ServiceAppointment> lstServiceApp;
    static List<Account> lstAcc;
    static List<Opportunity> lstOpportunity = new List<Opportunity>();
    static List<Logement__c> lstLogement;
    static List<ServiceTerritory> lstAgence;
    static List<Contract> lstContract = new List<Contract>();
    static List<OperatingHours> lstOpHrs;
	static List<Product2> lstTestProd = new List<Product2>();   
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstContractLineItems = new List<ContractLineItem>();
    static Bypass__c bp = new Bypass__c();
    
    static{
        mainUser = TestFactory.createAdminUser('AP45_UpdateContractVE@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;


        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP16,WorkOrderTrigger,AP53';
        bp.BypassValidationRules__c = True;
        insert bp;        

        lstAcc = new List<Account>{
                            new Account(Name='test', 
                                        ClientCreeSurSAV__c = true,
                                        Client_doesnt_have_mail__c=true)
        };
        insert lstAcc;

        sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAcc.get(0).Id);
        lstAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
        
        update lstAcc;

        lstOpHrs = new List<OperatingHours>{
                            new OperatingHours(Name='op')
        };
        insert lstOpHrs;

        lstContract.add(TestFactory.createContract('test', lstAcc[0].Id));
        insert lstContract;

        lstOpportunity.add(TestFactory.createOpportunity('test', lstAcc[0].Id));
        insert lstOpportunity;

        Id pricebookId = Test.getStandardPricebookId();

        //Create your product
        Product2 prod = new Product2(Name = 'Product X',
                                    ProductCode = 'Pro-X',
                                    isActive = true,
                                    Equipment_family__c = 'Chaudière',
                                    // Equipment_type__c = 'Chaudière gaz',
                                    Statut__c = 'Approuvée'
        );
        insert prod;

        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(Pricebook2Id = pricebookId,
                                                    Product2Id = prod.Id,
                                                    UnitPrice = 100.00,
                                                    IsActive = true
        );
        insert pbEntry;

        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = lstOpportunity[0].Id,
                                                            Quantity = 5,
                                                            PricebookEntryId = pbEntry.Id,
                                                            TotalPrice = pbEntry.UnitPrice
        );
        insert oli;

                    sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa;

        lstAgence = new List<ServiceTerritory>{
                                new ServiceTerritory(Name='test', OperatingHoursId = lstOpHrs[0].Id, Sofactoapp_Raison_Social__c =sofa.Id)
        };
        insert lstAgence;
        
        lstLogement = new List<Logement__c>{
                                new Logement__c(Street__c='rue test, stpierre, spoon, testing moka helvetiaaqwdweftgryjutjujybgryheyje',
                                                Account__c = lstAcc[0].Id,
                                                Postal_Code__c = '75009',
                                                City__c = 'Paris',
                                                Agency__c = lstAgence[0].Id)
        };
        insert lstLogement;

        lstServiceApp = new List<ServiceAppointment>{
                                    new ServiceAppointment(
                                            Status = AP_Constant.servAppStatusClientAbsent,
                                            Subject = 'this is a test subject',
                                            ParentRecordId = lstAcc[0].Id,
                                            TECH_SA_payed__c = false,
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Opportunity__c = lstOpportunity[0].Id,
                                            Contract__c = lstContract[0].Id,
                                            Residence__c = lstLogement[0].Id,
                                            Category__c = AP_Constant.ServAppCategoryVisiteEntr
                                    )
                };
        insert lstServiceApp;
        
        
        
               Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true,
                RecordTypeId= Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Vente').getRecordTypeId(),
                // Compte__c = testAcc.Id,
                Ref_Agence__c = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;


            //service contract           
            lstServCon.add(TestFactory.createServiceContract('testServCon', lstAcc.get(0).Id));
            lstServCon[0].Contract_Status__c = 'Cancelled';
            lstServCon[0].Contract_Renewed__c = true;
            lstServCon[0].PriceBook2Id = lstPrcBk[0].Id;    
            lstServCon[0].Agency__c = lstAgence[0].Id;
          //  lstServCon.add(TestFactory.createServiceContract('testServCon1', testAcc.Id));
            insert lstServCon;   

            lstContractLineItems = new List<ContractLineItem>{
                new ContractLineItem(
                    Customer_Absences__c = 2,
                    Completion_Date__c = System.today(),
                    VE_Number_Counter__c = 20,
                    VE_Status__c = 'Not Planned',
                    ServiceContractId = lstServCon[0].Id,
                    PricebookEntryId=pbEntry.Id,
                    Quantity=10,
                    UnitPrice=100),
                new ContractLineItem(
                    Customer_Absences__c = 1,
                    Completion_Date__c = System.today(),
                    VE_Number_Counter__c = 20,
                    VE_Status__c = 'Not Planned',
                    ServiceContractId = lstServCon[0].Id,
                    PricebookEntryId=pbEntry.Id,
                    Quantity=10,
                    UnitPrice=100)
            };
            insert lstContractLineItems;
    }

     @isTest
    public static void createServiceAppointment2(){
        System.runAs(mainUser){

            // lstServiceApp[0].Status = AP_Constant.servAppStatusEfectueConc;
            // lstServiceApp[0].TECH_WorkType_Type__c = AP_Constant.servAppStatusChaudiereGaz;

            Test.startTest();
            AP45_UpdateContractVE.UpdateServiceContract(lstServiceApp);
                //update lstServiceApp[0];
            Test.stopTest();

           // ServiceAppointment sa = [SELECT Id, Status, TECH_WorkType_Type__c FROM ServiceAppointment WHERE Id = :lstServiceApp[0].Id];
            // System.assertEquals(sa.Status, AP_Constant.servAppStatusEfectueConc);
    }
}
}