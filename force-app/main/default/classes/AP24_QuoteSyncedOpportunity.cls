/**
 * @File Name          : AP24_QuoteSyncedOpportunity.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/13/2020, 1:26:54 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/13/2020   RRJ     Initial Version
**/
public with sharing class AP24_QuoteSyncedOpportunity {
/**
 * @File Name          : AP24_QuoteSyncedOpportunity.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-10-2019     		LGO         Initial Version (CT-1122)
**/
    public static void updateOppDevis(Set<Id> setQuoId){
        System.debug('start updateOppDevis');
   

        list<Quote> lstupdDevis = new list<Quote>();        
  

        for(Quote quo: [SELECT Id, Devis_signe_par_le_client__c
                        FROM Quote
                        WHERE Id IN: setQuoId
                        AND RecordType.DeveloperName = 'Devis_standard'])
        {
             //update Devis               
                    Quote updQuo = new Quote (Id = quo.Id, Devis_signe_par_le_client__c = true, status ='Validé, signé - en attente d\'intervention');
                    lstupdDevis.add(updQuo);            

        }
            
        System.debug('updateOppDevis lstupdDevis '+lstupdDevis.size());
        if(lstupdDevis.size() >0 ){
            update lstupdDevis;
        }
    }
}