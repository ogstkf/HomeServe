public with sharing class LC01_Acc360EqpDetails {

	@AuraEnabled
	public static void removeLinkEquipementAndAccount(Id eqpId, Date dateDemenagement) {
        Id actualAccountIdBeforeMoving, actualLogementIdBeforeMoving;
        Id vacantAccountId;

        // Step 0 : On récupère le logement de l'équipement correspondant
		Asset equipement = [SELECT Id, AccountId, Logement__c, Logement__r.Agency__r.Compte_pour_VACANT__c FROM Asset WHERE Id = :eqpId];
        actualAccountIdBeforeMoving = equipement.AccountId;
        actualLogementIdBeforeMoving = equipement.Logement__c;
        if (equipement.Logement__r.Agency__r.Compte_pour_VACANT__c == null) {
            vacantAccountId = System.Label.Id_Compte_Vacant;
        } else {
            vacantAccountId = equipement.Logement__r.Agency__r.Compte_pour_VACANT__c;
        }

		// Step 1 : L'occupant du logement est mis à jour avec un compte "Vacant".
        Logement__c logement = [
            SELECT Id, Inhabitant__c
            FROM Logement__c
            WHERE Id = :actualLogementIdBeforeMoving
            LIMIT 1
        ];
        logement.Inhabitant__c = vacantAccountId;
        update logement;

        // Step 2 : La relation compte/logement est mise à jour pour indiquer la date de fin de la relation et le statut "inactif".
        List<Compte_Logement__c> compteLogements = [
            SELECT Id, Actif__c, End__c
            FROM Compte_Logement__c
            WHERE
                Account__c = :actualAccountIdBeforeMoving AND
                Logement__c = :actualLogementIdBeforeMoving
        ];
        for (Compte_Logement__c compteLogement : compteLogements) {
            compteLogement.Actif__c = false;
            compteLogement.End__c = Date.valueOf(dateDemenagement);
        }
        update compteLogements;

        // Step 3 : Les équipements de ce logement sont associés au nouvel occupant ("Vacant")
        List<Asset> equipements = [SELECT Id FROM Asset WHERE Logement__c = :actualLogementIdBeforeMoving];
		for (Asset eqp : equipements) {
            eqp.AccountId = vacantAccountId;
        }
		update equipements;

        // Step 4 : S'il y a des requêtes VE ouvertes associées au compte, les SA/WO associés sont annulés et les requête fermées.
        // Step 4a : Annulation des RDV de Service (ServiceAppointment)
        List<ServiceAppointment> listSAs = [
            SELECT Id, Status
            FROM ServiceAppointment
            WHERE
                AccountId = :actualAccountIdBeforeMoving AND
                Work_Order__r.AssetId = :eqpId AND
                Status <> 'Cancelled' AND // IN ('None', 'Scheduled') AND
                Work_Order__r.Status <> 'Canceled' AND // added
                Work_Order__r.Case.Type IN ('Maintenance', 'First Maintenance Visit (new contract)') AND
                Work_Order__r.Case.Status <> 'Closed'
        ];

        for (ServiceAppointment sa : listSAs) {
            sa.Status = 'Cancelled';
        }
        update listSAs;

        // Step 4b : Annulation des Ordres d'exécution (WorkOrder)
        List<WorkOrder> listWOs = [
            SELECT Id, Status
            FROM WorkOrder
            WHERE
                AccountId = :actualAccountIdBeforeMoving AND
                AssetId = :eqpId AND
                Status <> 'Cancelled' AND // added
                Case.Type IN ('Maintenance', 'First Maintenance Visit (new contract)') AND
                Case.Status <> 'Closed'
        ];

        for (WorkOrder wo : listWOs) {
            wo.Status = 'Cancelled';
        }
        update listWOs;

        // Step 4c : Fermeture des Requêtes / Cases
        List<Case> listCases = [
            SELECT Id, Status
            FROM Case
            WHERE
                AccountId = :actualAccountIdBeforeMoving AND
                AssetId = :eqpId AND
                Type IN ('Maintenance', 'First Maintenance Visit (new contract)') AND
                Status <> 'Closed'
        ];

        for (Case caseUniq : listCases) {
            caseUniq.Status = 'Closed';
        }
        update listCases;

        // Step 5 : Si le compte a des contrats actifs associés à ce logement, les contrats restent associés au compte et au logement
        // et le statut est mis à jour automatiquement en "A résilier".
        // Fermeture des Contrats
        List<ServiceContract> listContracts = [
            SELECT Id, Contract_Status__c
            FROM ServiceContract
            WHERE
                AccountId = :actualAccountIdBeforeMoving AND
                Logement__c = :actualLogementIdBeforeMoving AND
                Contract_Status__c IN ('Active', 'Pending Payment', 'Draft', 'Pending Client Validation', 'Pending first visit', 'En attente de renouvellement', 'Actif - en retard de paiement')
        ];

        for (ServiceContract contract : listContracts) {
            contract.Contract_Status__c = 'Cancelled';
        }
        update listContracts;
	}
}