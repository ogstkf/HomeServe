public without sharing class MetadataServiceExamples {

    public List<String> listofValue;

    public MetadataServiceExamples() { 
        listofValue = new List<String>(); 
    }

    public static List<String> getDependentPicklistValuesByRecordType(String objectName,String recordTypeName,String fieldName){
        List<String> listofavailablevalue = new List<String>();
        String objectRecordType = objectName + '.' + recordTypeName;
        // Create service
        MetadataService.MetadataPort service = createService();        
        if(!Test.isRunningTest()){
            MetadataService.RecordType recordType = (MetadataService.RecordType) service.readMetadata('RecordType', new String[] { objectRecordType }).getRecords()[0];
            for ( MetadataService.RecordTypePicklistValue rpk : recordType.picklistValues ) {
                system.debug('*** rpk : '+rpk);
                if ( rpk.picklist == fieldName ) {
                    for ( MetadataService.PicklistValue pk : rpk.values ) {
                        listofavailablevalue.add(String.valueof(pk.fullName));
                    }
                }
            }
        }
        return listofavailablevalue;
    }
    
    public static MetadataService.MetadataPort createService(){
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = getSessionId();//UserInfo.getSessionId();
        return service;
    }
    
     public static String getSessionId() {
        String result;
        Boolean isAsync = ((System.isBatch() || System.isFuture() || System.isQueueable() || System.isScheduled()) ? true : false);
        Boolean isLX = (UserInfo.getUiThemeDisplayed().contains('Theme4') ? true : false);
        // hack for asynchronous calls and LX
        if (isLX || isAsync || Test.isRunningTest()) {
            String content = (Test.isRunningTest() ? 'Start_Of_Session_Id0123456789End_Of_Session_Id' : Page.SfSession.getContent().toString());
            Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length();
            Integer e = content.indexOf('End_Of_Session_Id');
            result = content.substring(s, e);
        }
        else {
            result = UserInfo.getSessionId();
        }
        return result;
    }

    public void retrieveValue(string objectName,string recordTypeName,string fieldName){ 
        listofValue=MetadataServiceExamples.getDependentPicklistValuesByRecordType(objectName,recordTypeName,fieldName);
        system.debug('*** retrieveValue listofValue: '+listofValue); 
    }
}