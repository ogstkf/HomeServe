/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 03-11-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   11-10-2021   MNA   Initial Version
**/
public with sharing class LC76_ServiceAppointmentCustomListView {
    @AuraEnabled
    public static List<ServiceAppointment> getData(String query){
        query += ' ';
        query = String.join(query.split('"'), '\'');
        return Database.query(query);
    }
}