/**
 * @File Name          : AP49_SetCompletionDateOfPreviousYrCLI.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 01/08/2019, 17:30:25
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    02/07/2019, 15:50:51   RRJ     Initial Version
**/
public class AP49_SetCompletionDateOfPreviousYrCLI{

    public static void populateConLineItems(list<ContractLineItem> lstCLI){
        system.debug('**populateConLineItems: ');

        list<Id> lstAssetId = new list<Id>();
        list<Id> lstAccId = new list<Id>();
        list<ContractLineItem> lstCLIToUpd = new list<ContractLineItem>();

        for(ContractLineItem cli : lstCLI){
            system.debug('**cli.TECH_ServiceContractAsset__c: '+cli.TECH_ServiceContractAsset__c);
            system.debug('**cli.serviceContract.Asset__c: '+cli.serviceContract.Asset__c);
            lstAssetId.add(cli.TECH_ServiceContractAsset__c);
            lstAccId.add(cli.TECH_ServiceContractAccount__c);
        }

        map<Integer, ContractLineItem> mapCLIExisting = new map<Integer, ContractLineItem>();
        for(ContractLineItem cliExisting : [SELECT id, Completion_Date__c, StartDate, EndDate from ContractLineItem 
                                            // WHERE TECH_ServiceContractAsset__c in: lstAssetId AND TECH_ServiceContractAccount__c in:lstAccId  
                                            ]){
            mapCLIExisting.put(cliExisting.StartDate.year(), cliExisting);
        }
        system.debug('**mapCLIExisting: '+mapCLIExisting.size());

        for(ContractLineItem cli : lstCLI){
            if(mapCLIExisting.containsKey(cli.StartDate.year()-1)){
                cli.TECH_CompletionDateAnnee_M_1__c = mapCLIExisting.get(cli.StartDate.year()-1).Completion_Date__c;
            }
            lstCLIToUpd.add(cli);
        }
        // if(lstCLIToUpd.size()>0){
        //     update lstCLIToUpd;
        // }
    }

}