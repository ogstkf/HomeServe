/**
 * @File Name          : BAT16_WSExternalApi.cls
 * @description       : 
 * @author            : MNA
 * @group             : 
 * @last modified on  : 20-09-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   22/03/2021   MNA                                  Initial Version
**/
global with sharing abstract class BAT16_WSExternalApi implements Database.Batchable <sObject>, Database.Stateful, Database.AllowsCallouts, Schedulable  {
    
    global String query;
    global String queryCondition;

    global BAT16_WSExternalApi(String queryCondition) {
        this.queryCondition = queryCondition;
    }

    global BAT16_WSExternalApi(){
        
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        query = 'SELECT Id, Name, analytique_client__c, analytique_departement__c, analytique_etablissement__c, analytique_produit__c, code_client__c'
                +   ', Comptabilise__c, compte_general__c, date__c, date_echeance__c, devise__c, etablissement__c, Journal__c, libelle__c, mode_reglement__c'
                +   ', montant__c, nom__c, num_contrat__c, num_equipement__c, num_facture__c, numero_piece__c, sofactoapp__Plateforme__c, prenom__c'
                +   ', profil_tva__c, raison_sociale__c, sofactoapp__Response__c, sens__c, SIRET__c, Societe__c, sofactoapp__Status_Code__c'
                +   ', sofactoapp__TechError__c, tiers__c, type_ligne__c, Type_piece__c, URL__c, Type_Ecriture__c, Analytique_categorie_produit__c'
                +   ', Libelle_remise__c, DateEnvoi__c'
                +   ' FROM sofactoapp__External_Api__c WHERE'
                //+   ' Sage_Actif__c = True AND Comptabilise__c = false'
                //+   ' Id IN (' + queryCondition + ')'
                +   queryCondition
                +   ' ORDER BY numero_piece__c desc, compte_general__c desc';
                
        System.debug('**query: ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sofactoapp__External_Api__c> lstExtApi) {
        WS14_APISage.appelWS(lstExtApi);
    }

    global void finish(Database.BatchableContext bc) {

    }

    global void execute(SchedulableContext sc) {
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT16');
        Integer batchSize = ((batchConfig != null && batchConfig.BatchSize__c != null)
                                    ? Integer.valueof(batchConfig.BatchSize__c)
                                    : 200);

        this.manualExecution(batchSize);
    }

    protected List<String> join(Integer batchSize) {
        List<String> lstQueryCondition = new List<String>();
        
        List<sofactoapp__External_Api__c> lstExtApi = [SELECT Id, numero_piece__c FROM sofactoapp__External_Api__c
                                                                    WHERE Sage_Actif__c = True AND Comptabilise__c = false
                                                                    ORDER BY numero_piece__c desc, compte_general__c desc];
        
        //Update 06-07-2021 TEC 748 
        // Step 1 : Rassembler les enregistrements ayant le même numero_piece__c dans un même lot du batch
        Integer totalSize = 0;
        Integer count = 0;
        String oldNumPiece = null;
        
        Map<String, String> mapExtApiIdByNumPiece = new Map<String, String>();
        for (sofactoapp__External_Api__c extApi: lstExtApi) {
            if (mapExtApiIdByNumPiece.containsKey(extApi.numero_piece__c))
                mapExtApiIdByNumPiece.put(extApi.numero_piece__c, mapExtApiIdByNumPiece.get(extApi.numero_piece__c) + ',\'' + extApi.Id + '\'');
            else
                mapExtApiIdByNumPiece.put(extApi.numero_piece__c, '\'' + extApi.Id + '\'');

        }
        for (String strKey: mapExtApiIdByNumPiece.keySet()) {
            Integer size = mapExtApiIdByNumPiece.get(strKey).split(',').size();
            if (totalSize == 0) {
                lstQueryCondition.add(mapExtApiIdByNumPiece.get(strKey));
            }
            else {
                if (totalSize + size <= batchSize) {
                    lstQueryCondition[count] += ',' + mapExtApiIdByNumPiece.get(strKey);
                }
                else {
                    totalSize = 0;
                    lstQueryCondition.add(mapExtApiIdByNumPiece.get(strKey));
                    count++;
                }
            }
            totalSize += size;
        }
        
        // Step 2 : Appeler le batch
        for (String queryCondition: lstQueryCondition) {
            //Database.executeBatch(new BAT16_WSExternalApi(queryCondition), batchSize);
        }
        return lstQueryCondition;
    }

    global abstract void manualExecution(Integer batchSize);
}