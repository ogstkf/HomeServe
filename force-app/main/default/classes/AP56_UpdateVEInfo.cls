/**
 * @File Name          : AP56_UpdateVEInfo.cls
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 4/22/2020, 4:32:24 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    07/01/2020         LGO                    Initial Version
 * 1.1    03/03/2020         DMU                    Added condition to use SchedStartTime if ActualStartTime is null
 * 1.2    22/04/2020         RRJ                    Added condition to use Earliest start time is ChamDigital
**/
public with sharing class AP56_UpdateVEInfo {


    //1. If completed and VE
    public static void updateCond1(List<ServiceAppointment> lstSA){
        system.debug('dmu updateCond1 ');

        Set<Id> setWorkOrderId = new Set<Id>();
        List<ContractLineItem> lstContractLineId = new List<ContractLineItem>();
        Map<Id, Datetime> mapIdDate = new Map<Id, Datetime> ();
        List<WorkOrder> LstWorkOrders = new List<WorkOrder>();
        Map<Id, ContractLineItem> mapIdContractLine = new Map<Id, ContractLineItem>();
        Map<Id, WorkOrder> mapsaIdWO = new Map<Id, WorkOrder> (); //DMU 20200109 - Added condition on counter < target

        //1. Update CLI Completion
        List<ServiceAppointment> lstServApps = [SELECT Id, ActualStartTime, SchedStartTime, ParentRecordId, Work_Order__c, Work_Order__r.TECH_WOCaseCLITarget__c, Work_Order__r.TECH_WOCaseCLICounter__c, EarliestStartTime FROM ServiceAppointment WHERE ParentRecordType='WorkOrder' AND Id IN :lstSA];
        for(ServiceAppointment sa: lstServApps){
            if(sa.ParentRecordId != null){               
               //DMU: 20200203 - added condition to use SchedStartTime if ActualStartTime is null - requirement on mail 31/01/2020 from Laure Fessart with Subject 'Re: [CHAM] - NECIA - Planification - Tests Dates'
                if(sa.ActualStartTime != null){
                    mapIdDate.put(sa.ParentRecordId, sa.ActualStartTime);
                }else{
                    if(sa.SchedStartTime != null){
                        mapIdDate.put(sa.ParentRecordId, sa.SchedStartTime);
                    }else {
                        mapIdDate.put(sa.ParentRecordId, sa.EarliestStartTime);
                    }
                }
                mapsaIdWO.put(sa.ParentRecordId, sa.Work_Order__r);
            }
        }
        System.debug('#######mapsaIdWO :'+mapsaIdWO);
		System.debug('#######mapIdDate :'+mapIdDate);
        if(!mapIdDate.isEmpty()){
            for(Id woId : mapIdDate.KeySet()){
                DateTime dT = mapIdDate.get(woId);
                Date saDate = mapIdDate.get(woId) != null ? date.newinstance(dT.year(), dT.month(), dT.day()) : null;
                System.debug('#######mapsaIdWO.get(woId).TECH_WOCaseCLICounter__c :'+mapsaIdWO.get(woId).TECH_WOCaseCLICounter__c);
                System.debug('#######mapsaIdWO.get(woId).TECH_WOCaseCLITarget__c :'+mapsaIdWO.get(woId).TECH_WOCaseCLITarget__c);
                //if(mapsaIdWO.get(woId).TECH_WOCaseCLICounter__c < mapsaIdWO.get(woId).TECH_WOCaseCLITarget__c){  //DMU 20200109 - Added condition on counter < target
                   system.debug('in dmu mapsaIdWO ');
                    LstWorkOrders.add(new WorkOrder(Id = woId, Completion_Date__c= saDate));
                //}
            }
        }
        System.debug('#######LstWorkOrders :'+LstWorkOrders);
        if(!LstWorkOrders.isEmpty()){
            update LstWorkOrders;
        }     

        //2. Upd CLI VE_Number_Counter__c
        for (WorkOrder wo : [SELECT Id,caseid,Case.Contract_Line_Item__c, Case.Contract_Line_Item__r.VE_Number_Counter__c 
                                FROM WorkOrder WHERE Id IN: mapIdDate.KeySet()]){                        
            System.debug('#######wo.Case.Contract_Line_Item__r.VE_Number_Counter__c :'+wo.Case.Contract_Line_Item__r.VE_Number_Counter__c);
            Decimal count = wo.Case.Contract_Line_Item__r.VE_Number_Counter__c == null ? 1 : wo.Case.Contract_Line_Item__r.VE_Number_Counter__c + 1;
            System.debug('#######counter :'+count);
            if(!mapIdContractLine.containsKey(wo.Case.Contract_Line_Item__c))                
                if(mapsaIdWO.get(wo.Id).TECH_WOCaseCLICounter__c < mapsaIdWO.get(wo.Id).TECH_WOCaseCLITarget__c){
                    DateTime dT = mapIdDate.get(wo.Id);
                    Date saDate = mapIdDate.get(wo.Id) != null ? date.newinstance(dT.year(), dT.month(), dT.day()) : null;
                    mapIdContractLine.put(wo.Case.Contract_Line_Item__c, new ContractLineItem(Id = wo.Case.Contract_Line_Item__c , VE_Number_Counter__c = count, Completion_Date__c=saDate, VE_Status__c = AP_Constant.cliStatusComplete ));
                }
        }
        System.debug('#######mapIdContractLine :'+mapIdContractLine);
        if(!mapIdContractLine.isEmpty()){
            system.debug('updateCond1 '+mapIdContractLine.values());
            update mapIdContractLine.values();
        }   
    }

    //2. If Customer Absent
    public static void updateCond2(List<ServiceAppointment> lstSA){
        Set<Id> setWorkOrderId = new Set<Id>();
        List<WorkOrder> LstWorkOrders = new List<WorkOrder>();
        //1. Update parent WO
        List<ServiceAppointment> lstServApps = [SELECT Id, ActualStartTime, ParentRecordId FROM  ServiceAppointment WHERE ParentRecordType='WorkOrder' AND Id IN :lstSA];
        for(ServiceAppointment sa: lstServApps){
            if(sa.ParentRecordId != null){
               setWorkOrderId.add(sa.ParentRecordId);
            }
        }

        if(!setWorkOrderId.isEmpty()){
            for(Id woId : setWorkOrderId){
                LstWorkOrders.add(new WorkOrder(Id = woId, Customer_Absences__c= 1));
            }
        }
        if(!LstWorkOrders.isEmpty()){
            update LstWorkOrders;
        } 
    }        
  
    //3. Is Done Ok and origin is Cham Digital and has WO 
    //4. Is Done Ok and origin is Cham Digital and has Opp
    public static void updateCond3_4(List<ServiceAppointment> lstSA){
        system.debug('LGO updateCon3_4'+ lstSA);
        List<Account> LstAccounts = new List<Account>();
        Set<Id> setWorkOrderId = new Set<Id>();
        List<Opportunity> LstOpps = new List<Opportunity>();
        Map<Id, DateTime> mapIdDate = new Map<Id, DateTime>();
        List<WorkOrder> lstWoUpdt = new List<WorkOrder>();

        //1. Update Account      
        List<ServiceAppointment> lstServApps = [SELECT Id, AccountId, Account.Status__c, ParentRecordId, EarliestStartTime, ActualStartTime, SchedStartTime, TECH_Cham_Digital__c FROM  ServiceAppointment WHERE ParentRecordType='WorkOrder' AND Id IN :lstSA];
        for(ServiceAppointment sa: lstServApps){
            if(sa.ParentRecordId != null){
               setWorkOrderId.add(sa.ParentRecordId);
               LstAccounts.add(new Account(Id = sa.AccountId, Status__c= 'Client'));

                if(sa.ActualStartTime != null){
                    mapIdDate.put(sa.ParentRecordId, sa.ActualStartTime);
                }else{
                    if(sa.SchedStartTime != null){
                        mapIdDate.put(sa.ParentRecordId, sa.SchedStartTime);
                    }else if(sa.TECH_Cham_Digital__c){
                        mapIdDate.put(sa.ParentRecordId, sa.EarliestStartTime);
                    }
                }
            }
        }

        if(!LstAccounts.isEmpty()){
            update LstAccounts;
        }     
        //2. Update Opp
        for (WorkOrder wo : [SELECT Id,Caseid,Case.Opportunity__c FROM WorkOrder WHERE Id IN: setWorkOrderId]){
            if(wo.Case.Opportunity__c != null){
                LstOpps.add(new Opportunity(Id = wo.Case.Opportunity__c , StageName = 'Closed Won'));
            }
            if(mapIdDate.containsKey(wo.Id)){
                wo.Completion_Date__c = Date.newinstance(mapIdDate.get(wo.Id).year(), mapIdDate.get(wo.Id).month(), mapIdDate.get(wo.Id).day());
                lstWoUpdt.add(wo);
            }
        }
        if(lstWoUpdt.size()>0){
            update lstWoUpdt;
        }
        if(!LstOpps.isEmpty()){
            update LstOpps;
        }         
 
    }         
        
      
    //5. Is Done Ok and origin is Cham Digital and has Service Contract
    public static void updateCond5(List<ServiceAppointment> lstSA){
        List<ServiceContract> LstServiceCons = new List<ServiceContract>();

        //1. Update Service Contract     
        List<ServiceAppointment> lstServApps = [SELECT Id, Service_Contract__c, Service_Contract__r.Contract_Status__c FROM  ServiceAppointment WHERE Id IN :lstSA];
        for(ServiceAppointment sa: lstServApps){
               LstServiceCons.add(new ServiceContract(Id = sa.Service_Contract__c, Contract_Status__c= 'Active'));
        }

        if(!LstServiceCons.isEmpty()){
            update LstServiceCons;
        }           
 
    }          

    //6. Is Done KO and origin is Cham Digital and has Opp
    public static void updateCond6(List<ServiceAppointment> lstSA){
        set<Account> setAccounts = new set<Account>();
        Set<Id> setWorkOrderId = new Set<Id>();
        List<Opportunity> LstOpps = new List<Opportunity>();


        //1. Update Account      
        List<ServiceAppointment> lstServApps = [SELECT Id, AccountId, Account.Status__c, ParentRecordId FROM  ServiceAppointment WHERE ParentRecordType='WorkOrder' AND Id IN :lstSA];
        for(ServiceAppointment sa: lstServApps){
            if(sa.ParentRecordId != null){
               setWorkOrderId.add(sa.ParentRecordId);
               setAccounts.add(new Account(Id = sa.AccountId, Status__c= 'Prospect'));
            }
        }

        if(!setAccounts.isEmpty()){  
            // update setAccounts;
            List<Account> lstAccounts = new List<Account>();
            lstAccounts.addAll(setAccounts);
            update lstAccounts;
        }     
        //2. Update Opp
        for (WorkOrder wo : [SELECT Id,Caseid,Case.Opportunity__c FROM WorkOrder WHERE Id IN: setWorkOrderId]){
            LstOpps.add(new Opportunity(Id = wo.Case.Opportunity__c , StageName = 'Closed Lost'));
        }
        if(!LstOpps.isEmpty()){
            if(!Test.isRunningTest())
            	update LstOpps;
        }         
 
    }          
 
    //7. Is Done KO and origin is Cham Digital and has Service Contract
    public static void updateCond7(List<ServiceAppointment> lstSA){
        List<ServiceContract> LstServiceCons = new List<ServiceContract>();

        //1. Update Service Contract     
        List<ServiceAppointment> lstServApps = [SELECT Id, Service_Contract__c, Service_Contract__r.Contract_Status__c FROM  ServiceAppointment WHERE Id IN :lstSA];
        for(ServiceAppointment sa: lstServApps){
               LstServiceCons.add(new ServiceContract(Id = sa.Service_Contract__c, Contract_Status__c= 'Draft'));
        }

        if(!LstServiceCons.isEmpty()){
            update LstServiceCons;
        }           
 
    } 

}