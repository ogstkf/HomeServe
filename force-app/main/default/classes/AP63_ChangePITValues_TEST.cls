@isTest
public with sharing class AP63_ChangePITValues_TEST {

    static User mainUser;
    static User u = new User();
    static Account testAcc;
    static Schema.Location loc = new Schema.Location();
    static Schema.Location loc2 = new Schema.Location();
    static Product2 prod = new Product2();
    static PricebookEntry pbe;
    static Order ord = new Order();
    static OrderItem oi = new OrderItem();
    static Shipment ship = new Shipment();
    static ProductItem pi = new ProductItem();
    static ProductItem pi2 = new ProductItem();
    static ProductTransfer pt = new ProductTransfer();
    static ProductItemTransaction pit = new ProductItemTransaction();


    static{
        mainUser = TestFactory.createAdminUser('AP62_ChangeProducItemValues@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        // Create user;
        u.LastName = 'last';
        u.Email = 'puser000@amamama.com';
        u.Username = 'puser000@amamama.com' + System.currentTimeMillis();
        u.CompanyName = 'TEST';
        u.Title = 'title';
        u.Alias = 'alias';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.ProfileId=TestFactory.getProfileAdminId();
        insert u;

        // Create Account
        testAcc = TestFactory.createAccountBusiness('AP10_GenerateCompteRenduPDF_Test');
        testAcc.BillingPostalCode = '1233456';
        testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
        insert testAcc;

        // Create Location
        loc.Name='Test Bagnols';
        loc.LocationType='Entrepôt';
        loc.IsInventoryLocation=True;
        insert loc;

        // Create Location
        loc2.Name='Test Bagnols2';
        loc2.LocationType='Entrepôt';
        loc2.IsInventoryLocation=True;
        insert loc2;

        // Create Products
        prod = TestFactory.createProduct('testProd');
        insert prod;

        // Create Product Item
        pi.Product2Id=prod.Id;
        pi.LocationId=loc.Id;
        pi.QuantityOnHand=100;
        pi.Classification__c = 'Stock mort';
        insert pi;

        // Create Product Item
        pi2.Product2Id=prod.Id;
        pi2.LocationId=loc2.Id;
        pi2.QuantityOnHand=100;
        pi2.Classification__c = null;
        insert pi2;

        // Get Standard PriceBookId
        // Create Pricebook
        Id standardPricebookId = Test.getStandardPricebookId();

        // Create Pricebook entry
        pbe  = TestFactory.createPriceBookEntry(standardPricebookId, prod.Id, 150);
        insert pbe;

        // Create Order
        ord.AccountId = testAcc.Id;
        ord.Name = 'Test1';
        ord.EffectiveDate = System.today();
        ord.Status = 'Demande d\'achats';
        ord.Pricebook2Id = standardPricebookId;
        ord.RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId();
        insert ord;

        // Create Order Item
        oi.OrderId=ord.Id;
        oi.UnitPrice=2;
        oi.Quantity=50;
        oi.PricebookEntryId=pbe.Id;
        insert oi;

        // Create Shipment
        ship.ShipToName='Ship Test';
        ship.DeliveredToId=u.Id;
        ship.RecordTypeId=Schema.SObjectType.Shipment.getRecordTypeInfosByName().get('Reception Fournisseur').getRecordTypeId();
        ship.Order__c = ord.Id;
        ship.ExpectedDeliveryDate = System.today();
        ship.Date_du_bon_de_livraison__c = System.today();
        ship.Satisfaction_qualit_de_la_commande__c='Moyenne';
        ship.N_de_bon_de_livraison__c ='3';
        ship.Status = 'Livré';
        insert ship;

        // Create Product Transfer
        pt.ShipmentId=ship.Id;
        pt.Product2Id = prod.Id;
        pt.QuantityReceived=10;
        pt.QuantitySent=20;
        pt.Quantity_Lot_received__c=10;
        pt.Quantity_lot_sent__c=20;
        pt.IsReceived = true;
        pt.Commande__c = ord.id;
        pt.SourceLocationId = loc2.id;
        pt.DestinationLocationId = loc.id;
        insert pt;
    }

    @isTest
    public static void ChangeValues_Test(){
        System.runAs(mainUser) {
            Test.startTest();
                List<Shipment> lstShip = [SELECT Id FROM Shipment];
            Test.stopTest();
        }
    }
}