/**
 * @File Name          : AP67_updateCLIVEStatusTEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 04-05-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    09/03/2020   AMO     Initial Version
**/

@isTest
public with sharing class AP66_SA_UpdateCLIStatus_TEST {

    static User adminUser;
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static Product2 lstTestProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();
    static List<PriceBookEntry> lstPrcBkEnt = new List<PriceBookEntry>();
    static List<Case> lstCase = new List<Case>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static Bypass__c bp = new Bypass__c();

    static {

            Integer parentNum = 1;
            Integer childNum = 5;

        adminUser = TestFactory.createAdminUser('AP66_SA_UpdateCLIStatus_TEST@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;
        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP16,WorkOrderTrigger,AP53';
        bp.BypassValidationRules__c = True;
        insert bp;

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);

            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //creating logement
            for(integer i =0; i<5; i++){
                Logement__c lgmt = TestFactory.createLogement('logementTest'+i, 'logement@test.com'+i, 'lgmt'+i);
                lgmt.Postal_Code__c = 'lgmtPC'+i;
                lgmt.City__c = 'lgmtCity'+i;
                lgmt.Account__c = testAcc.Id;
                lstTestLogement.add(lgmt);
            }
            insert lstTestLogement;

            Product2 lstTestProd = new Product2(Name = 'Product X1',
                ProductCode = 'Pro-X1',
                isActive = true,
                Equipment_family__c = 'Pompe à chaleur',
                Statut__c   = 'Approuvée',
                Equipment_type__c = 'Pompe à chaleur air/eau'
            );
            insert lstTestProd;

            Product2 lstTestProds = new Product2(Name = 'Product X2',
                ProductCode = 'Pro-X2',
                isActive = true,
                Statut__c   = 'Approuvée',
                Equipment_family__c = 'Pompe à chaleur',
                Equipment_type__c = 'Pompe à chaleur air/eau'
            );
            insert lstTestProds;

            // creating Assets
            for(Integer i = 0; i<5; i++){
                Asset eqp = TestFactory.createAccount('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[i].Id);
                // eqp.Product2Id = lstTestProd.Id;
                eqp.Logement__c = lstTestLogement[i].Id;
                eqp.AccountId = testacc.Id;
                lstTestAssset.add(eqp);
            }
            insert lstTestAssset;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd.Id, 0));

            insert lstPrcBkEnt;

            //service contract
            for(Integer i=0; i<2; i++){
                lstServCon.add(TestFactory.createServiceContract('testServCon'+i, testAcc.Id));
                lstServCon[i].PriceBook2Id = lstPrcBk[0].Id;
                lstServCon[i].EndDate = Date.Today().addYears(1);
                lstServCon[i].StartDate = Date.Today();
            }

            insert lstServCon;
            
            //contract line item
            for(Integer i=0; i<2; i++){
                lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[i].Id, lstPrcBkEnt[0].Id, 150, 1));
            }
            // lstConLnItem[0].Customer_Absences__c = 1;
            // lstConLnItem[0].Customer_Allowed_Absences__c = 2;

            insert lstConLnItem;

            // create case
            // case cse = new case(AccountId = testacc.id
            //                     ,RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId()
            //                     ,AssetId = lstTestAssset[0].Id
            //                     ,Contract_Line_Item__c = lstConLnItem[0].Id);
            // insert cse;

            for(Integer i=0; i<2; i++){
                lstCase.add(new case(AccountId = testacc.id
                                ,RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId()
                                ,AssetId = lstTestAssset[0].Id
                                ,Contract_Line_Item__c = lstConLnItem[i].Id)                              
                          );
            }
            insert lstCase;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Maintenance';
            lstWrkTyp[0].Type_de_client__c = 'Tous';
            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());            
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].CaseId = lstCase[0].Id;
            lstWrkOrd[1].CaseId = lstCase[1].Id;
            insert lstWrkOrd;

             // creating service appointment
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[0].Id));
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[1].Id));
            // insert lstServiceApp;   
        }        
    }

    @isTest
    public static void testUpdate(){
        System.runAs(adminUser){

            lstServiceApp[0].Work_Order__c = lstWrkOrd[0].Id;
            lstServiceApp[0].ParentRecordId = testAcc.Id;
            lstServiceApp[1].Work_Order__c = lstWrkOrd[1].Id;
            lstServiceApp[1].ParentRecordId = testAcc.Id;
            insert lstServiceApp;

            list <ServiceAppointment> lstSA = [SELECT Id, status, TECH_WOCaseCLI__c FROM ServiceAppointment WHERE Id IN: lstServiceApp];
            
            Test.startTest();            
                AP66_SA_UpdateCLIStatus.updateCLIVEStatus(lstSA);
            Test.stopTest();

            list <ContractLineItem> lstCLI = [SELECT Id, VE_Status__c FROM ContractLineItem WHERE Id IN: lstConLnItem];
            System.assertEquals(lstCLI[0].VE_Status__c, 'On Going');
            System.assertEquals(lstCLI[1].VE_Status__c, 'On Going');
        }
    }

    @isTest
    public static void testupdateInterventionImpossible(){
        System.runAs(adminUser){

            lstServiceApp[0].Work_Order__c = lstWrkOrd[0].Id;
            lstServiceApp[0].ParentRecordId = testAcc.Id;
            lstServiceApp[1].Work_Order__c = lstWrkOrd[1].Id;
            lstServiceApp[1].ParentRecordId = testAcc.Id;
            insert lstServiceApp;

            list <ServiceAppointment> lstSA = [SELECT Id, status, TECH_WOCaseCLI__c FROM ServiceAppointment WHERE Id IN: lstServiceApp];
            
            Test.startTest();            
                AP66_SA_UpdateCLIStatus.updateInterventionImpossible(lstSA);
            Test.stopTest();

            list <ContractLineItem> lstCLI = [SELECT Id, VE_Status__c FROM ContractLineItem WHERE Id IN: lstConLnItem];
            System.assertEquals(lstCLI[0].VE_Status__c, 'Intervention impossible');
        }
    }

    @isTest
    public static void testupdateVEStatusWhenSAStatusCancel(){
        System.runAs(adminUser){

            lstServiceApp[0].Work_Order__c = lstWrkOrd[0].Id;
            lstServiceApp[0].ParentRecordId = testAcc.Id;
            lstServiceApp[1].Work_Order__c = lstWrkOrd[1].Id;
            lstServiceApp[1].ParentRecordId = testAcc.Id;
            insert lstServiceApp;

            list <ServiceAppointment> lstSA = [SELECT Id, status, TECH_WOCaseCLI__c FROM ServiceAppointment WHERE Id IN: lstServiceApp];
            
            Test.startTest();            
                AP66_SA_UpdateCLIStatus.updateVEStatusWhenSAStatusCancel(lstSA);
            Test.stopTest();

            list <ContractLineItem> lstCLI = [SELECT Id, VE_Status__c FROM ContractLineItem WHERE Id IN: lstConLnItem];
            System.assertEquals(lstCLI[0].VE_Status__c, 'On Going');
            // System.assertEquals(lstCLI[1].VE_Status__c, 'Not Planned');
        }
    }

    @isTest
    public static void updateWOCLIVEStatus(){
        System.runAs(adminUser){
            Test.startTest();            
                AP66_SA_UpdateCLIStatus.updateWOCLIVEStatus(lstWrkOrd);
            Test.stopTest();
        }
    }

}