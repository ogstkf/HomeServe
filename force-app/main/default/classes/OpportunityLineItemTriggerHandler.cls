/**
 * @File Name          : OpportunityLineItemTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 15/10/2019, 14:49:10
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/10/2019   AMO     Initial Version
**/
public with sharing class OpportunityLineItemTriggerHandler {

    // public void handleAfterUpdate(List<OpportunityLineItem> lstOldOppLine, List<OpportunityLineItem> lstNewOppLine){
        
    //     List<OpportunityLineItem> NewlstNewOppLine = new List<OpportunityLineItem>();

    //     for(integer i=0; i<lstNewOppLine.size(); i++){
    //         if(lstNewOppLine[i].TECH_SyncedQuoteId__c != null){
    //             NewlstNewOppLine.add(lstNewOppLine[i]);
    //         }
    //     }
    //     system.debug('ally NewlstNewOppLine: '+NewlstNewOppLine);
    //     // AP17_QuoteSyncedOpportunityLineItem.QuoteSyncedOLI(NewlstNewOppLine);
    // }

    // public static void sync(List<OpportunityLineItem> newLineItems)
    // {
    //     // get quote ids we need to query for
    //     Set<Id> oppIds = new Set<Id>();
    //     for (OpportunityLineItem oli : newLineItems)
    //     {
    //         if (oli.OpportunityId != null)
    //         {
    //             oppIds.add(oli.OpportunityId);
    //         }
    //     }
 
    //     // Linking quote line item with Opportunity Line Items
    //     Map<Id, Id> mapOpportunityLineItemSortOrder = new Map<Id, Id>();
    //     mapOpportunityLineItemSortOrder = returnDefaultLinking(oppIds);
 
    //     //Fetch opportunity line item for sync
    //     Map<Id, QuoteLineItem> mapQuoteLineItems=new Map<Id, QuoteLineItem>();
    //     for(QuoteLineItem qli:[select id, Remise_en100__c from QuoteLineItem where QuoteLineItem.OpportunityLineItemId in :oppIds])
    //     {
    //         mapQuoteLineItems.put(qli.id,qli);
    //     }
 
    //     List<QuoteLineItem> lstQuoteToUpdate = new List<QuoteLineItem>();
    //     for (OpportunityLineItem oli : newLineItems) {
    //         QuoteLineItem qli = mapQuoteLineItems.get(mapOpportunityLineItemSortOrder.get(oli.Id));
    //         if (qli != null ) {
    //             qli.Remise_en100__c = oli.Discount;
    //             //update more fields….
 
    //             lstQuoteToUpdate.add(qli);
    //         }
    //     }
    //     update lstQuoteToUpdate;
    // }

    // private static Map<id,id> returnDefaultLinking(Set<Id> poIds)
    // {
    //     Map<Id, Id> mapSortOrder= new Map<Id, Id>();
    //     String query='select id, name,(select id, Opportunity_Line_Item_ID__c from OpportunityProduct) from Quote where id in :poIds';
    //     List<Quote> lstQuotesWithLineItems=Database.query(query);
    //     for(Quote q: lstQuotesWithLineItems)
    //     {
    //         if(q.QuoteLineItems !=null)
    //         {
    //             for(QuoteLineitem qli : q.QuoteLineItems)
    //             {
    //                 if(qli.Opportunity_Line_Item_ID__c!=null)
    //                 {
    //                     //map quote line item id with respective opportunity line item id
    //                      mapSortOrder.put(qli.Id,ID.valueOF(qli.Opportunity_Line_Item_ID__c));
    //                 }
    //             }
    //         }
    //     }
    //     return mapSortOrder;
    // }
}