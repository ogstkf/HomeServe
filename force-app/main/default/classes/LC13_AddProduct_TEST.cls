/**
 * @File Name          : LC13_AddProduct_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-01-2020    		LGO         Initial Version
**/
@isTest
public with sharing class LC13_AddProduct_TEST {
    static User adminUser;
    static Account Acc = new Account();
    static List<Order> lstOrder = new List<Order>();
    static List<OrderItem> lstOrderItem = new List<OrderItem>();
    static Product2 prod = new Product2();
	static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
	static PricebookEntry PrcBkEnt = new PricebookEntry();
    static List<Quote> lstQuote = new List<Quote>();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<Product2> lstprod = new List<Product2>();
    static List<Conditions__c> lstConds = new List<Conditions__c>();

        static {
            adminUser = TestFactory.createAdminUser('AP39_UpdtQUoteStatus@test.COM', TestFactory.getProfileAdminId());
		    insert adminUser;

            System.runAs(adminUser){

                Acc = new Account(
                    Name='test', 
                    ClientCreeSurSAV__c = true,
                    Client_doesnt_have_mail__c=true,
                    RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId()
                );

                Insert Acc;
				sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(Acc.Id);
           		 Acc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
                update Acc;
                //create products
                prod = TestFactory.createProduct('testProd');
                Insert prod;

                lstprod = new List<Product2>{
                    TestFactory.createProduct('testProd1'),
                    TestFactory.createProduct('testProd2')

                };
                Insert lstprod;

                //pricebook
                Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
                );

                lstPrcBk.add(standardPricebook);
                Update lstPrcBk;

                //pricebook entry
                PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
                Insert PrcBkEnt;

                lstOrder = new List<Order>{
                    new Order (
                        RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get(AP_Constant.orderRtCommandeFournisseur).getRecordTypeId(),
                        AccountId = Acc.Id,
                        EffectiveDate = System.today(),
                        Status = 'Demande d\'achats',
                        Pricebook2Id = lstPrcBk[0].Id
                        ),
                    new Order (
                        RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get(AP_Constant.orderRtCommandeFournisseur).getRecordTypeId(),
                        AccountId = Acc.Id,
                        EffectiveDate = System.today(),
                        Status = 'Demande d\'achats',
                        Pricebook2Id = lstPrcBk[0].Id
                        )                        
                };

                Insert lstOrder;
                System.debug('List of orders: ' + lstOrder);

                lstOrderItem = new List<OrderItem>{
                    new OrderItem (
				            OrderId = lstOrder[0].Id,
				            Quantity = decimal.valueof('2'),
				            PricebookEntryId = PrcBkEnt.Id,
                            Product2Id = prod.Id,
				            UnitPrice = 145,
                            Remise__c = 50,
                            Remise100__c = 20
                    ),
                    new OrderItem (
                        	OrderId = lstOrder[0].Id,
				            Quantity = decimal.valueof('3'),
				            PricebookEntryId = PrcBkEnt.Id,
                            Product2Id = lstprod[0].Id,
				            UnitPrice = 145
                    ),
                    new OrderItem (
                            OrderId = lstOrder[0].Id,
				            Quantity = decimal.valueof('4'),
				            PricebookEntryId = PrcBkEnt.Id,
				            UnitPrice = 145,
                            Product2Id = lstprod[1].Id
                    ),
                    new OrderItem (
                            OrderId = lstOrder[0].Id,
				            Quantity = decimal.valueof('3'),
				            PricebookEntryId = PrcBkEnt.Id,
				            UnitPrice = 145
                    )
                };

                Insert lstOrderItem;
                System.debug('List of order items: ' + lstOrderItem);

            }
            //create opportunity
            lstOpp =  new List<Opportunity>{ 
                TestFactory.createOpportunity('Test1',Acc.Id) ,
                TestFactory.createOpportunity('Test2',Acc.Id) 
            };


            insert lstOpp;
            System.debug('Lst opp ' + lstOpp);

            //Create Quote
            lstQuote =  new List<Quote>{    
                new Quote(Name ='Test1',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[0].Id) ,                                   
                new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[0].Id)
            };   
            insert lstQuote;   

            //Create Conditions
            lstConds = new list<Conditions__c>{
                new Conditions__c(Compte__c = Acc.Id,
                                Produit__c = prod.Id,
                                //Famille_d_articles__c='Pièces détachées',
                                //Sous_famille_d_articles__c = 'Autre',
                                marque__c = 'INCONNU',
                                Debut__c = Date.today().addDays(-10),
                                Fin__c = Date.today(),
                                Catalogue__c = lstPrcBk[0].Id),

                new Conditions__c(Compte__c = Acc.Id,
                                Produit__c = lstprod[0].Id,
                                Famille_d_articles__c='Pièces détachées',
                                Sous_famille_d_articles__c = 'Autre',
                                marque__c = 'INCONNU',
                                Debut__c = Date.today().addDays(-10),
                                Fin__c = Date.today(),
                                Catalogue__c = lstPrcBk[0].Id)
            };
            insert lstConds;                  
    }


    @isTest
    public static void testfetchOrderLineItem(){
        System.runAs(adminUser){
            Test.startTest();
            LC13_AddProduct.fetchOrderLineItem(lstOrder[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    }
    @isTest
    public static void testfetchDetails(){
        System.runAs(adminUser){
            Test.startTest();
            LC13_AddProduct.fetchDetails(lstOrder[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    }
    @isTest
    public static void testgetAllPricebkEntry(){
        System.runAs(adminUser){
            Test.startTest();
            LC13_AddProduct.getAllPricebkEntry(0,lstOrder[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    }   
    @isTest
    public static void testsearchPricebkEntry(){
        System.runAs(adminUser){
            Test.startTest();
            LC13_AddProduct.searchPricebkEntry('Ord',0,lstOrder[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    }   
    @isTest
    public static void testfilterPricebkEntry(){
        System.runAs(adminUser){
            Test.startTest();
            LC13_AddProduct.filterPricebkEntry('Ord',0,lstQuote[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    }    
    @isTest
    public static void testsearchFilterPricebkEntry(){
        System.runAs(adminUser){
            Test.startTest();
            LC13_AddProduct.searchFilterPricebkEntry('Ord','Ord',0,lstQuote[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    }  
    @isTest
    public static void testsaveOrderlineItem(){
        System.runAs(adminUser){
            // String j = '[{"Id":"01u6E000008EQDRQA4","Name":"ATLANTIC - OPTIMA UNIT 4130 BV","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","ProductCode":"L646","Family":"Appareils","Id":"01t1t000001le1wAAA"},"Product2Id":"01t1t000001le1wAAA","UnitPrice":"1","UnitPriceOrderItem":1,"Quantity":"1"},{"Id":"01u6E000008LCukQAG","Name":"JOINT SILICONE","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Sous_familles_d_articles__c":"Travaux (cuivre, raccords)","ProductCode":"JS33388","Id":"01t6E000005cXm0QAE","Famille_d_articles__c":"Consommables"},"Product2Id":"01t6E000005cXm0QAE","UnitPrice":"5","UnitPriceOrderItem":5,"Quantity":"1"}]';
           String j ='[{"Id":"01u6E000008LCukQAG","Name":"JOINT SILICONE","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Sous_familles_d_articles__c":"Travaux (cuivre, raccords)","ProductCode":"JS33388","Id":"01t6E000005cXm0QAE","Famille_d_articles__c":"Consommables"},"Product2Id":"01t6E000005cXm0QAE","UnitPrice":"5","UnitPriceOrderItem":5,"Quantity":"1"}]';
            
            LC13_AddProduct.OLIWrapper oliWrap = new LC13_AddProduct.OLIWrapper();
            oliWrap.Id = PrcBkEnt.Id;
            oliWrap.Product2Id= prod.Id;
            oliWrap.Product2= prod;
            oliWrap.Name= '';
            oliWrap.UnitPrice= '10';
            oliWrap.ListPrice= '10';
            oliWrap.Quantity= '10';
            oliWrap.Eco_contribution= '0';
            oliWrap.Remise100= '10';
            oliWrap.Remise= '10';
            oliWrap.Description= '';
            oliWrap.Pricebook2Id= lstPrcBk[0].Id;
            oliWrap.Remise100Init= '10';
            oliWrap.UnitPriceOrderItem= '10';


            String inputJson = JSON.serialize(oliWrap);
            
            Test.startTest();
            LC13_AddProduct.saveOrderlineItem('['+inputJson+']',lstOrder[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    }   
    @isTest
    public static void testsaveOrderlineItem1(){
        System.runAs(adminUser){
            // String j = '[{"Id":"01u6E000008EQDRQA4","Name":"ATLANTIC - OPTIMA UNIT 4130 BV","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","ProductCode":"L646","Family":"Appareils","Id":"01t1t000001le1wAAA"},"Product2Id":"01t1t000001le1wAAA","UnitPrice":"1","UnitPriceOrderItem":1,"Quantity":"1"},{"Id":"01u6E000008LCukQAG","Name":"JOINT SILICONE","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Sous_familles_d_articles__c":"Travaux (cuivre, raccords)","ProductCode":"JS33388","Id":"01t6E000005cXm0QAE","Famille_d_articles__c":"Consommables"},"Product2Id":"01t6E000005cXm0QAE","UnitPrice":"5","UnitPriceOrderItem":5,"Quantity":"1"}]';
           String j ='[{"Id":"01u6E000008LCukQAG","Name":"JOINT SILICONE","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Sous_familles_d_articles__c":"Travaux (cuivre, raccords)","ProductCode":"JS33388","Id":"01t6E000005cXm0QAE","Famille_d_articles__c":"Consommables"},"Product2Id":"01t6E000005cXm0QAE","UnitPrice":"5","UnitPriceOrderItem":5,"Quantity":"1"}]';
            
            LC13_AddProduct.OLIWrapper oliWrap = new LC13_AddProduct.OLIWrapper();
            oliWrap.Id = PrcBkEnt.Id;
            oliWrap.Product2Id= lstprod[1].Id;
            oliWrap.Product2= lstprod[1];
            oliWrap.Name= '';
            oliWrap.UnitPrice= '10';
            oliWrap.ListPrice= '10';
            oliWrap.Quantity= '10';
            oliWrap.Eco_contribution= '0';
            oliWrap.Remise100= '10';
            oliWrap.Remise= '10';
            oliWrap.Description= '';
            oliWrap.Pricebook2Id=  lstPrcBk[0].Id;
            oliWrap.Remise100Init= '10';
            oliWrap.UnitPriceOrderItem= '10';           

            String inputJson = JSON.serialize(oliWrap);
            
            Test.startTest();
            LC13_AddProduct.saveOrderlineItem('['+inputJson+']',lstOrder[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    }          
    @isTest
    public static void testgetRemises(){
        System.runAs(adminUser){
            // String j = '[{"Id":"01u6E000008EQDRQA4","Name":"ATLANTIC - OPTIMA UNIT 4130 BV","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","ProductCode":"L646","Family":"Appareils","Id":"01t1t000001le1wAAA"},"Product2Id":"01t1t000001le1wAAA","UnitPrice":"1","UnitPriceOrderItem":1,"Quantity":"1"},{"Id":"01u6E000008LCukQAG","Name":"JOINT SILICONE","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Sous_familles_d_articles__c":"Travaux (cuivre, raccords)","ProductCode":"JS33388","Id":"01t6E000005cXm0QAE","Famille_d_articles__c":"Consommables"},"Product2Id":"01t6E000005cXm0QAE","UnitPrice":"5","UnitPriceOrderItem":5,"Quantity":"1"}]';
           String j ='[{"Id":"01u6E000008LCukQAG","Name":"JOINT SILICONE","Pricebook2Id":"01s6E000000wzXHQAY","Product2":{"Sous_familles_d_articles__c":"Travaux (cuivre, raccords)","ProductCode":"JS33388","Id":"01t6E000005cXm0QAE","Famille_d_articles__c":"Consommables"},"Product2Id":"01t6E000005cXm0QAE","UnitPrice":"5","UnitPriceOrderItem":5,"Quantity":"1"}]';
            
            LC13_AddProduct.OLIWrapper oliWrap = new LC13_AddProduct.OLIWrapper();
            oliWrap.Id = PrcBkEnt.Id;
            oliWrap.Product2Id= lstprod[0].Id;
            oliWrap.Product2= lstprod[0];
            oliWrap.Name= '';
            oliWrap.UnitPrice= '10';
            oliWrap.ListPrice= '10';
            oliWrap.Quantity= '10';
            oliWrap.Eco_contribution= '0';
            oliWrap.Remise100= '10';
            oliWrap.Remise= '10';
            oliWrap.Description= '';
            oliWrap.Pricebook2Id= lstPrcBk[0].Id;
            oliWrap.Remise100Init= '10';
            oliWrap.UnitPriceOrderItem= '10';

            LC13_AddProduct.OLIWrapper oliWrap1 = new LC13_AddProduct.OLIWrapper();
            oliWrap1.Id = PrcBkEnt.Id;
            oliWrap1.Product2Id= lstprod[1].Id;
            oliWrap1.Product2= lstprod[1];
            oliWrap1.Name= '';
            oliWrap1.UnitPrice= '10';
            oliWrap1.ListPrice= '10';
            oliWrap1.Quantity= '10';
            oliWrap1.Eco_contribution= '0';
            oliWrap1.Remise100= '10';
            oliWrap1.Remise= '10';
            oliWrap1.Description= '';
            oliWrap1.Pricebook2Id= lstPrcBk[0].Id;
            oliWrap1.Remise100Init= '10';
            oliWrap1.UnitPriceOrderItem= '10';            

            String inputJson = JSON.serialize(oliWrap);
            String inputJson1 = JSON.serialize(oliWrap1);
            
            Test.startTest();
            LC13_AddProduct.getRemises(Acc.Id,'['+inputJson+','+inputJson1+']',lstOrder[0].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }


    }     
    @isTest
    public static void testgetPickListValueModeGestion(){
        System.runAs(adminUser){
            Test.startTest();
            LC13_AddProduct.getPickListValueModeGestion(lstprod[1].Id);          
            Test.stopTest();       

            //  List<OpportunityLineItem> lstOLI= [SELECT Id
            //                          FROM OpportunityLineItem
            //                          WHERE TECH_CorrespondingQLItemId__c =: lstQuoteLineItem[2].Id] ;   
            // System.assertEquals(lstOLI.size(),2);
        }
    } 
          
}