/**
 * @author  SC (YGO)
 * @version 1.0
 * @since   30/01/2019
 *
 * Description : 
 * 
 *-- Maintenance History: 
 *--
 *-- Date         Name  Version  Remarks 
 *-- -----------  ----  -------  ------------------------
 *-- 30-01-2019   YGO    1.0     Initial version
 *-- 18-03-2019   MGR    1.1     Display date/time according to User locale
 *-------------------------------------------------------
 * 
*/
global with sharing class BAT01_SendPisteCSV implements Database.Batchable <sObject>, Database.Stateful, Schedulable {
    
    global final String query;
    global final String header = 'Code Agence, Date de création , Civilité, Nom, Prénom, Adresse de l\'intervention - Rue, Adresse de l\'intervention - Complément, Adresse de l\'intervention - CP, Adresse de l\'intervention - Ville, Téléphone, Téléphone mobile, Type d\'équipement , Code de l\'offre choisie, Taux de TVA, Numéro de piste,URL de la piste, Date de l\'intervention  \n';
    global String finalStr = header;
    global String recordType;

    global BAT01_SendPisteCSV(){

        string SELECT_part_query = ' SELECT Agency_Code__c, id, TECH_AccountSource__c, Salutation, FirstName, LastName, Phone, MobilePhone, Type_dequipement__c, Additional_Intervention_Address__c, Intervention_Postal_Code__c, Intervention_City__c, Offer_Code__c, Taux_de_TVA__c, Payment__c, LeadNumber__c, Lead_URL__c, Intervention_Street__c, Intervention_date__c, CreatedDate ';
        string FROM_part_query = ' FROM Lead ';
        string WHERE_part_query = ' WHERE CreatedDate = YESTERDAY AND Payment__c = true AND TECH_AccountSource__c != \'SOWEE\'';
        String ORDER_part_query = ' ORDER BY Agency_Code__c ';

        if(Test.isRunningTest()){
            query = 'SELECT Agency_Code__c, id, Salutation, FirstName, LastName, Phone, MobilePhone, Type_dequipement__c, Additional_Intervention_Address__c, Intervention_Postal_Code__c, Intervention_City__c, Offer_Code__c, Taux_de_TVA__c, Payment__c, LeadNumber__c, Lead_URL__c, Intervention_Street__c, Intervention_date__c, CreatedDate FROM Lead WHERE Payment__c = true';
        }
        else{
            query = SELECT_part_query + FROM_part_query + WHERE_part_query;
        }
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        system.Debug('### : ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Lead> lstLead) {
        System.debug('### executeLead' + lstLead);
        
        for(Lead l : lstLead){
            String s1 = l.Lead_URL__c;
            String s2 = s1.substringAfter('\"').substringBefore('\"');
            
            //MGR 18-03-2019:START
            
            //Commented:START
            //Datetime dt = l.CreatedDate;
            //String dCreated = dt.formatGMT('dd-MM-yyyy \' \'HH:mm:ss');
            //System.debug('mgr test3 '+ dCreated);
            
            //Datetime d = l.Intervention_date__c;
            //String dIntervention = d.formatGMT('dd-MM-yyyy \' \'HH:mm:ss');
            //Commented:END

            String dCreated = l.CreatedDate.format('dd-MM-yyyy \' \'HH:mm:ss');

            String dIntervention='';
            if(l.Intervention_date__c != null)
            dIntervention = l.Intervention_date__c.format('dd-MM-yyyy \' \'HH:mm:ss');
            //MGR 18-03-2019:END

            String recordString = '"'+(String.isBlank(l.Agency_Code__c) ? '':l.Agency_Code__c) 
                                + '","' + dCreated
                                + '","' +(String.isBlank(l.Salutation) ? '':l.Salutation)
                                + '","' +(String.isBlank(l.FirstName) ? '':l.FirstName)
                                + '","' +(String.isBlank(l.LastName) ? '':l.LastName) 
                                + '","' +(String.isBlank(l.Intervention_Street__c) ? '':l.Intervention_Street__c) 
                                + '","' +(String.isBlank(l.Additional_Intervention_Address__c) ? '':l.Additional_Intervention_Address__c)
                                + '","' +(String.isBlank(l.Intervention_Postal_Code__c) ? '':l.Intervention_Postal_Code__c)
                                + '","' +(String.isBlank(l.Intervention_City__c) ? '':l.Intervention_City__c)
                                + '","' +(String.isBlank(l.Phone) ? '': l.Phone)
                                + '","' +(String.isBlank(l.MobilePhone) ? '': l.MobilePhone)
                                + '","' +(String.isBlank(l.Type_dequipement__c) ? '':l.Type_dequipement__c)
                                + '","' +(String.isBlank(l.Offer_Code__c) ? '':l.Offer_Code__c)
                                + '","' +(l.Taux_de_TVA__c == null? 0:l.Taux_de_TVA__c)
                                + '","' +(String.isBlank(l.LeadNumber__c) ? '':l.LeadNumber__c) 
                                + '","' + s2
                                + '","' + dIntervention + '"\n';
            finalstr =+ finalstr + recordString;
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('finalstr: ' + finalStr);

        Set<String> setUserName = new Set<String>();
        //add the user to a list of string
        for(EmailAddress__c em : [SELECT Name FROM EmailAddress__c]){
            setUserName.add(em.Name);
        }

        Blob csvBlob = Blob.valueOf(finalstr);
        String filename= 'Piste.csv';

        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();    
        csvAttc.setFileName(filename);
        csvAttc.setBody(csvBlob);

        Messaging.SingleEmailMessage[] lstRes = new List<Messaging.SingleEmailMessage> ();
        String subject ='CSV des pistes créées hier';

        for(User usr : [SELECT Id, Name FROM User WHERE Name IN :setUserName]){
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage(); 
            
            email.setSubject(subject);
            email.setTargetObjectId(usr.Id);
            email.setHtmlBody(System.label.EmailBody);
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
            email.saveAsActivity = false;
            lstRes.add(email);
        }

        try{
            Messaging.SendEmailResult[] results = Messaging.sendEmail(lstRes);
            System.debug('### mail sent' + results);
        }
        catch(Exception e){
            System.debug('error' + e.getMessage());
        }
    }

    //used to schedule job 
    global static String scheduleBatch() {
        BAT01_SendPisteCSV scheduler = new BAT01_SendPisteCSV();
        //String scheduleTime = '0 0 00 * * ?';
        //return System.schedule('Batch:', scheduleTime, scheduler);
        return System.schedule('Batch SendPisteCSV:' + Datetime.now().format(),  '0 0 0 * * ?', scheduler);
    }

    global void execute(SchedulableContext sc){
     Database.executeBatch(new BAT01_SendPisteCSV());
    }
}