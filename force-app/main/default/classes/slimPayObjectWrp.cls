/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 09-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-03-2022   MNA   Initial Version
**/
global class slimPayObjectWrp {
	public class PaymentWrp {
        public String scheme;
        public String cur;
        public String label;
        public String reference;
        public Decimal amount;
        public refObj creditor;
        public refObj mandate;

        public PaymentWrp (sofactoapp__R_glement__c payment, ServiceTerritory agency) {
            this.scheme = 'SEPA.DIRECT_DEBIT.CORE';
            this.cur = 'EUR';
            this.amount = payment.sofactoapp__Montant__c;
            this.creditor = new refObj(agency.Slimpay_Creditor_Name__c);
            if (payment.Sofacto_Service_Contract__c != null) {
                this.label = 'Contrat ' + payment.Sofacto_Service_Contract__r.Numero_du_contrat__c;
                if (payment.Sofacto_Service_Contract__r.sofactoapp_Rib_prelevement__c != null)
                    this.mandate = new refObj(payment.Sofacto_Service_Contract__r.sofactoapp_Rib_prelevement__r.RUM__c);
            }
        }
    }

    public class MandateWrp {
        String dateSigned;
        String createSequenceType;
        refObj creditor;
        refObj subscriber;
        Signatory signatory;
        public MandateWrp (Account acc, ServiceTerritory agency, ServiceContract sc) {
            Datetime dt = sc.sofactoapp_Rib_prelevement__r.Date_signature_du_mandat__c;
            this.dateSigned = dt.formatGmt('yyyy-MM-dd') + 'T' + dt.formatGmt('hh:mm:ss.SSS') + '+0000';
            this.createSequenceType = 'FRST';
            this.creditor = new refObj(agency.Slimpay_Creditor_Name__c);
            this.subscriber = new refObj(acc.ClientNumber__c);
            this.signatory = new Signatory(acc, sc.sofactoapp_IBAN__c);
        }
    }
    
    public class OrderWrp {
        public Boolean started;
        public String paymentScheme;
        public refObj creditor;
        public refObj subscriber;
        public List<Item> items;
        public OrderWrp (Account acc, ServiceTerritory agency, String iban, String scheme, String itmRef) {
            this.started = true;
            this.paymentScheme = scheme;
            this.creditor = new refObj(agency.Slimpay_Creditor_Name__c);
            this.subscriber = new refObj(acc.ClientNumber__c);
            this.items = new List<Item>{new Item(acc, iban, itmRef)};
        }
    }
    
    public class refObj {
        public String reference;
        public refObj (String reference) {
            this.reference = reference;
        }
    }
    
    public class Item {
        public String type;
        public SignatureApproval signatureApproval;
        public ItemMandate mandate;
        public Item (Account acc, String iban, String reference) {
            this.type = 'signMandate';
            this.mandate = new ItemMandate(acc, iban, reference);
            this.signatureApproval = new SignatureApproval();
        }
    }
    
    public class SignatureApproval {
        public String paymentProcessor;
        public Method method;
        public SignatureApproval() {
            this.paymentProcessor = 'slimpay';
            this.method = new Method();
        }
    }
    
    public class ItemMandate {
        public String reference;
        public Signatory signatory;
        public ItemMandate (Account acc, String iban, String reference) {
            this.reference = reference;
            this.signatory = new Signatory(acc, iban);
        }
    } 
    
    public class Method {
        public String type;
        public Method() {
            this.type = 'otp';
        }
    }
      
    public class Signatory {
        //public String honorificPrefix;
        public String familyName;
        public String givenName;
        public String telephone;
        public String email;
        public BillingAddress billingAddress;
        public BankAccount bankAccount;
        public Signatory (Account acc, String iban) {
            
            //this.honorificPrefix = acc.Salutation;
            this.familyName = acc.LastName;
            this.givenName = acc.FirstName;
            this.telephone = acc.PersonMobilePhone;
            this.email = acc.PersonEmail;
            this.billingAddress = new BillingAddress(acc);
            this.bankAccount = new BankAccount(iban);
        }
    }
    
    public class BillingAddress {
        public String street1;
        public String street2;
        public String postalCode;
        public String city;
        public String country;
        public BillingAddress (Account acc) {
            this.street1 = acc.BillingStreet;
            this.street2 = acc.Adress_complement__c;
            this.postalCode = acc.BillingPostalCode;
            this.city = acc.BillingCity;
            this.country = acc.BillingCountry;
            if (this.country == 'France') {
                this.country = 'FR';
            }
        }
    }
    
    public class BankAccount {
        public String iban;
        public BankAccount(String iban) {
            this.iban = iban;
        }
    }
    
    public class responseOrder {
        public String Id {get;set;}
        public String reference {get;set;}
        public String rum {get;set;}
        public String state {get;set;}
        public String standard {get;set;}
        public String dateCreated {get;set;}
        public String dateStarted {get;set;}
        public String dateClosed {get;set;}
        public Link links {get;set;}
    }
    
    public class responsePost extends absData {
        public String transaction_id {get;set;}
        public String status {get;set;}
        public Payload payload {get;set;}
    }

    public class Payload {
        public String customer_id {get;set;}
        public String contract_id {get;set;}
        public String redirect_user_url {get;set;}
    }
    
    public class Link {
        public UrlToGet urlToGet {get;set;}
    }

    public class UrlToGet {
        public String href {get;set;}
    }
    
    public class dataReceiving {
        public String customer_id {get;set;}
        public String contract_id {get;set;}
        public String iban {get;set;}
        public String frequency {get;set;}
        public Boolean override_iban {get;set;}
        public String bic {get;set;}
        public String nom_debiteur {get;set;}
    }

    global abstract class absData {

    }
    
    global class errorData extends absData {
        String message;
        String errorCode;
        public errorData(Integer statusCode, String msgError) {
            this.message = msgError;
            switch on statusCode {
                when 403 {
                    errorCode = 'FORBIDDEN';
                }
                when 404 {
                    errorCode = 'NOT_FOUND';
                }
                when 409 {
                    errorCode = 'CONFLICT';
                }
            }
        }
    }

    global class dataAccScWrp extends absData {
        accWrp customer;
        scWrp contract;
        public dataAccScWrp(Account acc, ServiceContract sc) {
            this.customer = new accWrp(acc);
            this.contract = new scWrp(sc);
        }
        public dataAccScWrp() {
            
        }
    }

    public class accWrp {
        String first_name;
        String last_name;
        addressWrp address;
        public accWrp(Account acc) {
            this.first_name = acc.FirstName;
            this.last_name = acc.LastName;
            this.address = new addressWrp(acc);
        }
    }

    public class scWrp {
        Decimal amount_due;

        String name;
        String iban;
        public scWrp(ServiceContract sc) {
            this.name = sc.Name;
            this.amount_due = sc.GrandTotal;
            this.iban = sc.sofactoapp_IBAN__c;
        }
    }

    public class addressWrp {
        String street_number;
        String street_name;
        String city;
        String zipcode;
        public addressWrp (Account acc) {
            this.street_name = (acc.Adress_complement__c == null ? acc.BillingStreet : acc.BillingStreet + ' ' + acc.Adress_complement__c);
            this.city = acc.BillingCity;
            this.zipcode = acc.BillingPostalCode;
        }
    }

    public static Blob getErrorApex(Integer statusCode, String msgError) {
        List<slimPayObjectWrp.errorData> lstErr = new List<slimPayObjectWrp.errorData>{
            new slimPayObjectWrp.errorData(statusCode, msgError)
        };
        return Blob.valueOf(JSON.serialize(lstErr));
    }
}