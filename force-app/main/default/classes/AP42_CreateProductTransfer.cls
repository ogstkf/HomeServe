/**
* @File Name          : AP42_CreateProductTransfer.cls
* @Description        :
* @Author             : Spoon Consulting (LGO)
* @Group              :
* @Last Modified By   : ZJO
* @Last Modified On   : 23/04/2020, 14:56:11
* @Modification Log   :
*==============================================================================
* Ver         Date                     Author      Modification
*==============================================================================
* 1.0         13-11-2019                LGO         Initial Version (CT-1182)
**/
public with sharing class AP42_CreateProductTransfer {

    public static void createProdTransfer(List<ProductRequired> lstProdReq){
        Set<Id> setParentId = new Set<Id>();
        Map<Id, List<ProductRequired>> mapWoToProdReq = new Map<Id, List<ProductRequired>>();

        for(ProductRequired pr : lstProdReq){
            setParentId.add(pr.ParentRecordId);
            if(mapWoToProdReq.containsKey(pr.ParentRecordId)){
                mapWoToProdReq.get(pr.ParentRecordId).add(pr);
            }else{
                mapWoToProdReq.put(pr.ParentRecordId, new List<ProductRequired>{pr});
            }
        }

        List<WorkOrder> lstWO = [SELECT Location.Agence__c, Location.Agence__r.Inventaire_en_cours__c, Id, LocationId, ServiceTerritoryId FROM WorkOrder WHERE Id IN: setParentId AND LocationId != null AND ServiceTerritoryId != null];
        Set<Id> setSrvTerrId = new Set<Id>();
        Set<Id> setLocationId = new Set<Id>();
        for(WorkOrder wo: lstWO){
            setSrvTerrId.add(wo.ServiceTerritoryId);
            setLocationId.add(wo.LocationId);
        }

        // SH - 2020-01-24 - Added DestinationLocationId
        Map<Id, List<ProductRequestLineItem>> mapWoToPrlis = new Map<Id, List<ProductRequestLineItem>>();
        for(ProductRequestLineItem prli : [SELECT Id, Product2Id, WorkOrderId, DestinationLocationId FROM ProductRequestLineItem WHERE WorkOrderId IN: lstWO]){
            if(mapWoToPrlis.containsKey(prli.WorkOrderId)){
                mapWoToPrlis.get(prli.WorkOrderId).add(prli);
            }else{
                mapWoToPrlis.put(prli.WorkOrderId, new List<ProductRequestLineItem>{prli});
            }
        }

        List<ServiceTerritoryLocation> lstSrvLoc = [SELECT Location.Agence__c, Location.Agence__r.Inventaire_en_cours__c, Id, ServiceTerritoryId, LocationId FROM ServiceTerritoryLocation WHERE ServiceTerritoryId IN: setSrvTerrId AND ServiceTerritory.ParentTerritoryId = null AND Location.LocationType =: 'Entrepôt'];

        Map<Id, Id> mapAgenceLoc = new Map<Id, Id>();
        
        //SBH: CT-1417
        Map<Id, Boolean> mapAgenceLocation = new Map<Id, Boolean>();
        Map<Id, Boolean> mapLocationToInventaireEncours = new Map<Id, Boolean>();
        //END SBH
        for(ServiceTerritoryLocation loc : lstSrvLoc){
            setLocationId.add(loc.LocationId);
            mapAgenceLoc.put(loc.ServiceTerritoryId, loc.LocationId);
            
            //SBH CT1417
            if(loc.Location != null && loc.Location.Agence__c != null && loc.Location.Agence__r.Inventaire_en_cours__c){
                mapAgenceLocation.put(loc.ServiceTerritoryId, loc.Location.Agence__r.Inventaire_en_cours__c);
                mapLocationToInventaireEncours.put(loc.Id, loc.Location.Agence__r.Inventaire_en_cours__c);
            }
            
        }

        // SH - 2020-01-24 - Added Quantite_allouee__c
        List<ProductItem> lstProdItm = [SELECT Id, QuantityOnHand, LocationId, Product2Id, Quantite_allouee__c FROM ProductItem WHERE LocationId IN: setLocationId];
        Map<Id, List<ProductItem>> mapLocToProdItms = new Map<Id, List<ProductItem>>();
        for(ProductItem prdItm : lstProdItm){
            if(mapLocToProdItms.containsKey(prdItm.LocationId)){
                mapLocToProdItms.get(prdItm.LocationId).add(prdItm);
            }else{
                mapLocToProdItms.put(prdItm.LocationId, new List<ProductItem>{prdItm});
            }
        }

        List<ProductTransfer> lstProdTrToCreate = new List<ProductTransfer>();
        for(WorkOrder wo: lstWO){
            if(mapWoToProdReq.containsKey(wo.Id)){
                for(ProductRequired prodReq :mapWoToProdReq.get(wo.Id)){
                    ProductTransfer prodTr = new ProductTransfer();
                    prodTr.IsReceived = true;
                    prodTr.Product2Id = prodReq.Product2Id;
                    if(mapWoToPrlis.containsKey(wo.Id)){
                        for(ProductRequestLineItem prli: mapWoToPrlis.get(wo.Id)){
                            if(prli.Product2Id == prodReq.Product2Id){
                                prodTr.ProductRequestLineItemId  = prli.Id;
                            }
                        }
                    }
                    if(prodReq.APP_Checked__c){
                        prodTr.SourceLocationId = mapAgenceLoc.get(wo.ServiceTerritoryId);
                        prodTr.DestinationLocationId = wo.LocationId;

                        //SBH Start CT-1417
                        if(
                           ( 
                               mapAgenceLocation.containsKey(wo.ServiceTerritoryId) 
                                && mapAgenceLocation.get(wo.ServiceTerritoryId) == true)
                            || (
                                wo.Location.Agence__c != null && wo.Location.Agence__r.Inventaire_en_cours__c == true
                            )
                        ){
                            prodTr.Inventaire_en_cours__c = true;
                            prodTr.IsReceived = false;
                        }else{
                         //SBH END CT-1417

                            // SH - 2020-01-24 - BEGIN
                            for (ProductItem pi : lstProdItm) {
                                for(ProductRequestLineItem prli: mapWoToPrlis.get(wo.Id)){
                                    if((prli.Product2Id == pi.Product2Id) && (prli.DestinationLocationId == pi.LocationId)) {
                                        pi.Quantite_allouee__c -= prodReq.QuantityRequired;
                                        if (pi.Quantite_allouee__c < 0) { pi.Quantite_allouee__c = 0; }
                                    }
                                }
                            }
                            // SH - 2020-01-24 - END
                        }
                    }else{
                        prodTr.SourceLocationId = wo.LocationId;
                        prodTr.DestinationLocationId = mapAgenceLoc.get(wo.ServiceTerritoryId);
                        
                        //SBH Start CT-1417
                        if(
                           ( 
                               mapAgenceLocation.containsKey(wo.ServiceTerritoryId) 
                                && mapAgenceLocation.get(wo.ServiceTerritoryId) == true)
                            || (
                                wo.Location.Agence__c != null && wo.Location.Agence__r.Inventaire_en_cours__c == true
                            )
                        ){
                            prodTr.Inventaire_en_cours__c = true;
                            prodTr.IsReceived = false;
                        }else{
                        //SBH End CT-1417
                            // SH - 2020-01-24 - BEGIN
                            for (ProductItem pi : lstProdItm) {
                                for(ProductRequestLineItem prli: mapWoToPrlis.get(wo.Id)){
                                    if((prli.Product2Id == pi.Product2Id) && (prli.DestinationLocationId == pi.LocationId)) {
                                        pi.Quantite_allouee__c += prodReq.QuantityRequired;
                                    }
                                }
                            }
                            // SH - 2020-01-24 - END
                        }
                        
                    }
                    Decimal qtyOnHand = 0;
                    if(mapLocToProdItms.containsKey(prodTr.SourceLocationId)){
                        for(ProductItem prodItm :mapLocToProdItms.get(prodTr.SourceLocationId)){
                            if(prodItm.Product2Id == prodTr.Product2Id){
                                qtyOnHand = prodItm.QuantityOnHand;
                            }
                        }
                    }    
                    System.debug('qty on hand:' + qtyOnHand);                      
                    if(qtyOnHand < prodReq.QuantityRequired && mapLocationToInventaireEncours.containsKey(prodTr.SourceLocationId) && mapLocationToInventaireEncours.get(prodTr.SourceLocationId) == false ){                        
                        prodTr.QuantitySent = qtyOnHand;
                        prodTr.QuantityReceived = qtyOnHand;
                    }else{
                        if(qtyOnHand < prodReq.QuantityRequired){                            
                            prodTr.QuantitySent = qtyOnHand;
                            prodTr.QuantityReceived = qtyOnHand;
                        } else {                            
                            prodTr.QuantitySent = prodReq.QuantityRequired;
                            prodTr.QuantityReceived = prodReq.QuantityRequired;
                        }
                       
                    }                    
                    lstProdTrToCreate.add(prodTr);
                }
            }
        }

        if (lstProdItm.size()>0) { update lstProdItm; } // SH - 2020-01-24
        
        if (lstProdTrToCreate.size()>0) { insert lstProdTrToCreate; }
        

        // System.debug('###### RRJ: '+lstProdTrToCreate);
        // System.debug('###### RRJ: '+lstProdTrToCreate[0].Id);

    }
}