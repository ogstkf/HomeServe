/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : 
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 28-JAN-2019  YGO    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  
public with sharing class AP03_ManageLeadFiles {
	
	public static void callWSBlue(Map<String, ContentDocumentLink> mapLeadContentDocLink ){
		System.debug('### AP03_ManageLeadFiles-callWSBlue')
/*
		//DMU 20190411 - Lift & paste logic from LeadTriggerHandler to lauch WS AP01_BlueWSCallout when ServApp.Status is modified (MAJ commande et ficheir PDF) 
		//Requirement on mail "Transfert des flux Blue sur l'objet RDV de service" 20190410
		
		//Map<String, Lead> mapContentDocIdLeadId = new Map<String, Lead>();
		Map<String, ServiceAppointment> mapContentDocIdLeadId = new Map<String, ServiceAppointment>();

		for(ServiceAppointment sApp : [SELECT Id, Status, IZI_Order_Id__c FROM ServiceAppointment WHERE Id IN :mapLeadContentDocLink.keySet()]){		
			if((sApp.Status != AP_Constant.servAppStatusNone) || (sApp.Status != AP_Constant.servAppStatusPlanned)) {
				if(mapLeadContentDocLink.containsKey(sApp.Id)){
					mapContentDocIdLeadId.put(mapLeadContentDocLink.get(sApp.Id).ContentDocumentId, sApp);
				}
			}
		}

		List<ContentVersion> lstContentVersion = [	SELECT Id, ContentDocumentId, FileExtension, VersionData, Title 
													FROM ContentVersion 
													WHERE ContentDocumentId IN :mapContentDocIdLeadId.keySet() 
													AND FileExtension = :System.label.FileExtensionPdf];

		for(ContentVersion cv : lstContentVersion){
			if(mapContentDocIdLeadId.containsKey(cv.ContentDocumentId)){
				System.debug(mapContentDocIdLeadId.get(cv.ContentDocumentId).IZI_Order_Id__c + ' FileContent: ' + cv.VersionData + ' FileName: ' + cv.Title + ' cv: ' + cv.Id);
				// AP01_BlueWSCallout.fileUploadCallout(cv.VersionData,cv.Title,mapContentDocIdLeadId.get(cv.ContentDocumentId).Id, mapContentDocIdLeadId.get(cv.ContentDocumentId).IZI_Order_Id__c);				
			}
		}*/
	}
	
}
