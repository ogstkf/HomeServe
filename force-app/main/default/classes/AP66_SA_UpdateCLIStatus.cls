/**
 * @File Name          : AP66_SA_UpdateCLIStatus
 * @Description        : Controller for AP66_SA_UpdateCLIStatus
 * @Author             : Spoon Consulting 
 * @Group              : 
 * @Last Modified By   : DMU
 * @Last Modified On   : 09/03/2020, 14:15:00
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         09-03-2020     		DMU         Initial Version
**/

public class AP66_SA_UpdateCLIStatus {
 
  public static void updateCLIVEStatus(List<ServiceAppointment> lstNewServApp){
      Map<Id, Id> mapServAppCLI = new Map<Id, Id>();
      list<ContractLineItem> lstCLI = new list <ContractLineItem>();

      for (ServiceAppointment sa : lstNewServApp){
            mapServAppCLI.put(sa.id, sa.TECH_WOCaseCLI__c);
      }
	
      system.debug('*** mapServAppCLI:  '+mapServAppCLI);
      system.debug('*** mapServAppCLI size:  '+mapServAppCLI.size());
      for(ContractLineItem cli : [SELECT id, VE_Status__c, Customer_Absences__c FROM ContractLineItem where id IN: mapServAppCLI.values() ]){
        system.debug('*** in loop ');
        cli.VE_Status__c = cli.Customer_Absences__c >= 2 ? 'Double Absence' : 'On Going';
        lstCLI.add(cli);
      }
      if(lstCLI.size()>0){
        update lstCLI;
      }
  }
  public static void updateInterventionImpossible(List<ServiceAppointment> lstNewServApp){
    Map<Id, Id> mapServAppCLI = new Map<Id, Id>();
    list<ContractLineItem> lstCLI = new list <ContractLineItem>();

    for (ServiceAppointment sa : lstNewServApp){
          mapServAppCLI.put(sa.id, sa.TECH_WOCaseCLI__c);
    }
	
    system.debug('*** mapServAppCLI:  '+mapServAppCLI);
    system.debug('*** mapServAppCLI size:  '+mapServAppCLI.size());
    for(ContractLineItem cli : [SELECT id, VE_Status__c, Customer_Absences__c FROM ContractLineItem where id IN: mapServAppCLI.values() ]){
      if(cli.Customer_Absences__c < 2){
        cli.VE_Status__c = 'Intervention impossible';
        lstCLI.add(cli);
      }      
    }
    if(lstCLI.size()>0){
      update lstCLI;
    }
  }

  public static void updateVEStatusWhenSAStatusCancel(List<ServiceAppointment> lstNewServApp){
    Map<Id, Id> mapServAppCLI = new Map<Id, Id>();
    list<ContractLineItem> lstCLI = new list <ContractLineItem>();

    for (ServiceAppointment sa : lstNewServApp){
          mapServAppCLI.put(sa.id, sa.TECH_WOCaseCLI__c);
    }
	
    system.debug('*** mapServAppCLI:  '+mapServAppCLI);
    system.debug('*** mapServAppCLI size:  '+mapServAppCLI.size());
    for(ContractLineItem cli : [SELECT id, VE_Status__c, Customer_Absences__c FROM ContractLineItem where id IN: mapServAppCLI.values() ]){
      if(cli.Customer_Absences__c < 2 ){
        cli.VE_Status__c = 'On Going';        
      }
      if(cli.Customer_Absences__c >= 2){
        cli.VE_Status__c = 'Double Absence';        
      }      
      lstCLI.add(cli);
    }
    if(lstCLI.size()>0){
      update lstCLI;
    }
  }

  //DMU - planif BV-204 - A l'insert du WO au type=maintenance & status=New, le VE_Status du CLI passe en En cours
  public static void updateWOCLIVEStatus(List<WorkOrder> lstWO){

    map<string, string>mapWOIdCLIId = new map <string, string>();
    map<id, WorkOrder>mapCLIdWo = new map <Id, WorkOrder>();
    list<ContractLineItem> lstCLI = new list <ContractLineItem>();

    for(WorkOrder wo : lstWO){
      mapWOIdCLIId.put(wo.Id, wo.TECH_CaseContractLineItem__c); 
      mapCLIdWo.put(wo.TECH_CaseContractLineItem__c, wo);
    }

    for(ContractLineItem cli : [SELECT id, VE_Status__c, Customer_Absences__c FROM ContractLineItem where id IN: mapWOIdCLIId.values() ]){
      workorder wotmp = mapCLIdWo.get(cli.Id);
      System.debug('###### wotmp.IsGeneratedFromMaintenancePlan:'+wotmp.IsGeneratedFromMaintenancePlan);
      if (wotmp<> null && wotmp.IsGeneratedFromMaintenancePlan){
        cli.Customer_Absences__c=0;  
      }
      System.debug('###### cli.Customer_Absences__c:'+cli.Customer_Absences__c);      
        
      cli.VE_Status__c = 'On Going';
      lstCLI.add(cli);
    }
    if(lstCLI.size()>0){
      update lstCLI;
    }
  }
}