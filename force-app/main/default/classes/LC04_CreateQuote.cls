/**
 * @File Name          : LC04_CreateQuote.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 25-01-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    21/11/2019,        AMU                    Initial Version
**/
public with sharing class LC04_CreateQuote {
    @AuraEnabled
    public static Object createQuote(Case cse, Account acc, String woId) {
        Date todaydate = Date.newInstance(system.today().year(), system.today().month(), system.today().day());
        Savepoint sp = Database.setSavepoint();
        try {
            String servConId;

            Case cas = [SELECT Id, ContactId, AssetId, Contract_Line_Item__c, Service_Contract__c, Asset.Logement__c, Asset.Logement__r.Owner__c, Asset.Logement__r.Inhabitant__c, Asset.Logement__r.Street__c, Asset.Logement__r.City__c, Asset.Logement__r.Postal_Code__c, Asset.Logement__r.Country__c, Asset.Logement__r.Agency__c, Type, Reason__c FROM Case WHERE Id = :cse.Id LIMIT 1];
            String devisType = getDevisType(cas);
            Decimal duree_travaux = [SELECT Id, Duree_des_travaux__c FROM ServiceTerritory WHERE Id =: cse.Asset.Logement__r.Agency__c LIMIT 1].Duree_des_travaux__c;

            List<ServiceContract> lstSrvCon = [SELECT Id, Type__c, RecordTypeId, Payeur_du_contrat__c, Payeur_du_contrat__r.BillingStreet, Payeur_du_contrat__r.BillingCity, Payeur_du_contrat__r.BillingState, Payeur_du_contrat__r.BillingPostalCode, Payeur_du_contrat__r.BillingCountry, Payeur_du_contrat__r.Agence__c FROM ServiceContract WHERE Id = :cas.Service_Contract__c AND (Contract_Status__c = 'Pending first visit' OR Contract_Status__c = 'Actif - en retard de paiement' OR Contract_Status__c = 'Active') LIMIT 1 ];

            // List<ServiceContract> lstSrvCon = [SELECT Id, Type__c, Payeur_du_contrat__c, Payeur_du_contrat__r.BillingStreet, Payeur_du_contrat__r.BillingCity, Payeur_du_contrat__r.BillingState, Payeur_du_contrat__r.BillingPostalCode, Payeur_du_contrat__r.BillingCountry FROM ServiceContract WHERE Id = :servConId];

            //DMU 20201116 - TEC-204
            List<Pricebook2> lstPb = [SELECT Id FROM Pricebook2 WHERE RecordType.DeveloperName = 'Vente' AND Ref_Agence__c = true AND isActive = true AND Agence__c =:cas.Asset.Logement__r.agency__c]; 
            Id pbId = lstPb.size()>0 ? lstPb[0].Id : null;

            // CLA 13/01/22 - TEC-835
            Boolean devisCollectif = false;

            List<ContractLineItem> lstCli = [SELECT Id, ServiceContractId, ServiceContract.RootServiceContractId FROM ContractLineItem WHERE AssetId = :cas.AssetId AND IsActive__c = true];
            if (lstCli != null && lstCli.size() > 0 && lstCli[0].ServiceContractId != null && lstCli[0].ServiceContract.RootServiceContractId != null) {
                List<ServiceContract> lstSc = [SELECT Id, RecordTypeId, Payeur_du_contrat__c, Payeur_du_contrat__r.Agence__c FROM ServiceContract WHERE Id =: lstCli[0].ServiceContract.RootServiceContractId];
    
                if (lstSc != null && lstSc.size() > 0 && (lstSc[0].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives')
                        || lstSc[0].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_collectifs_publics')) ) {
    
                    // CLA 13/01/22 - TEC-835 - cocher la case Devis_Collectif__c si contrat collectif
                    devisCollectif = true;

                    if(lstSc[0].Payeur_du_contrat__c != null){
                        List<Pricebook2> lstPbCol = [SELECT Id FROM Pricebook2 WHERE RecordType.DeveloperName = 'Vente' AND isActive = true AND Agence__c =: lstSc[0].Payeur_du_contrat__r.Agence__c AND Compte__c =:lstSc[0].Payeur_du_contrat__c];
                        if (lstPbCol != null && lstPbCol.size() > 0){
                            pbId = lstPbCol[0].Id;
                        }
                    }
                }
            }


            Opportunity opp = new Opportunity(
                    AccountId = acc.Id
                    , Name = devisType + ' - ' + acc.Name
                    , StageName = 'Open'
                    , CloseDate = Date.today()
                    , Agency__c = cse.Asset.Logement__r.Agency__c
                    , Ordre_d_execution__c = String.isNotBlank(woId) ? woId : null
                    , Pricebook2Id = pbId
                    , RecordTypeId = AP_Constant.getRecTypeId('Opportunity', 'Opportunite_standard')
            );
            insert opp;

            Quote qte = new Quote(
                    Name = devisType + ' - ' + acc.Name
                    , OpportunityId = opp.Id
                    , ExpirationDate = Date.today().addMonths(2)
                    , Duree_de_validite_du_devis__c = '2 mois'
                    , Status = 'Draft'
                    , Objet_du_devis__c = devisType + ' - ' + acc.Name
                    , Type_de_devis__c = devisType
                    , Equipement__c = cas.AssetId
                    , Logement__c = cas.Asset.Logement__c
                    , Owner__c = cas.Asset.Logement__r.Owner__c
                    , Inhabitant__c = cas.Asset.Logement__r.Inhabitant__c
                    , ShippingStreet = cas.Asset.Logement__r.Street__c
                    , ShippingCity = cas.Asset.Logement__r.City__c
                    , ShippingPostalCode = cas.Asset.Logement__r.Postal_Code__c
                    , ShippingCountry = cas.Asset.Logement__r.Country__c
                    , BillingStreet = acc.BillingStreet
                    , BillingCity = acc.BillingCity
                    , BillingState = acc.BillingState
                    , BillingPostalCode = acc.BillingPostalCode
                    , BillingCountry = acc.BillingCountry
                    , Agency__c = cas.Asset.Logement__r.Agency__c
                    , Campagne__c = (todaydate < acc.Date_fin_retombees__c) ?  acc.Campagne__c  : null //RRA - 20200720 - CT-64 - l'ajout du code campagne - reference sur la page confluence
                    // , Contrat_de_service__c = cas.Service_Contract__c
                    // , Ligne_de_contrat__c = cas.Contract_Line_Item__c
                    , Ordre_d_execution__c = String.isNotBlank(woId) ? woId : null
                    , Pricebook2Id = pbId
                    , Nom_du_payeur__c = acc.Id
                    , Date_de_debut_des_travaux__c = Date.Today().addMonths(Integer.valueOf(duree_travaux))
                    , Devis_etabli_le__c = Date.Today()
                    , RecordTypeId = AP_Constant.getRecTypeId('Quote', 'Devis_standard')

                    // CLA 13/01/22 - TEC-835
                    , Devis_Collectif__c = devisCollectif
            );
            if(lstSrvCon.size()>0){
                qte.Contrat_de_service__c = lstSrvCon[0].Id;
                qte.Ligne_de_contrat__c = cas.Contract_Line_Item__c;
                if(lstSrvCon[0].Type__c == 'Collective'){
                    qte.BillingStreet = lstSrvCon[0].Payeur_du_contrat__r.BillingStreet;
                    qte.BillingCity = lstSrvCon[0].Payeur_du_contrat__r.BillingCity;
                    qte.BillingState = lstSrvCon[0].Payeur_du_contrat__r.BillingState;
                    qte.BillingPostalCode = lstSrvCon[0].Payeur_du_contrat__r.BillingPostalCode;
                    qte.BillingCountry = lstSrvCon[0].Payeur_du_contrat__r.BillingCountry;
                    qte.Nom_du_payeur__c = lstSrvCon[0].Payeur_du_contrat__c == null ? qte.Nom_du_payeur__c : lstSrvCon[0].Payeur_du_contrat__c;
                }
            }
            insert qte;

            if(qte.Type_de_devis__c == 'Prestations'){

                List<PricebookEntry> lstPBE = [SELECT Id, Pricebook2Id, Product2Id, UnitPrice, Name, Product2.Name, Product2.ProductCode, Product2.Description, Product2.Family FROM PricebookEntry WHERE Pricebook2Id = :qte.Pricebook2Id AND (Product2.ProductCode = 'MO' OR Product2.ProductCode = 'DEPL-ZONE') ORDER BY Name];

                List<Id> lstProdId = new List<Id>();
                for(PricebookEntry pbe : lstPBE){
                    lstProdId.add(pbe.Product2Id);
                }

                Map<String, Decimal> mapQuoIdTVA = AP48_GestionTVA.getTVADevis(new List<Id>{qte.Id});
                
                List<QuoteLineItem> lstQli = new List<QuoteLineItem>();
                for(PricebookEntry pbe : lstPBE){
                    QuoteLineItem qli = new QuoteLineItem();
                    qli.PricebookEntryId = pbe.Id;
                    qli.UnitPrice = pbe.UnitPrice;
                    qli.Quantity = 1;
                    qli.Product2Id = pbe.Product2Id;
                    qli.QuoteId = qte.Id;
                    qli.Taux_de_TVA__c = mapQuoIdTVA.get(qte.Id);
                    lstQli.add(qli);
                }
                Map<String, Map<String, QuoteLineItem>> mapRemises = AP50_GestionRemise.getRemiseQuoQli(new Map<String, List<QuoteLineItem>>{qte.Id => lstQli});
                Map<String, QuoteLineItem> mapProdRemise = mapRemises.containsKey(qte.Id) ? mapRemises.get(qte.Id) : new Map<String, QuoteLineItem>();
                for(QuoteLineItem qli :lstQli){
                    if(mapProdRemise.containsKey(qli.Product2Id)){
                        qli.Remise_en100__c = mapProdRemise.get(qli.Product2Id).Remise_en100__c;
                        qli.Remise_en_euros__c = mapProdRemise.get(qli.Product2Id).Remise_en_euros__c;
                    }
                }
                if(lstQli.size()>0){
                    insert lstQli;
                }
            }

            System.debug('accConId ANA : '+acc.PersonContactId);

            OpportunityContactRole OPC = new OpportunityContactRole(
                        //  ContactId = acc.PersonContactId
                         ContactId = cas.ContactId
                        ,OpportunityId = opp.Id
                        ,IsPrimary=true
                        );
            insert OPC;

            return new Map<String, Object>{
                    'success' => true, 'message' => null, 'qteId' => qte.Id
            };
        } catch (Exception e) {
            System.debug('##### ERROR: '+e.getMessage()+ ' '+ e.getStackTraceString());
            Database.rollback( sp );
            return new Map<String, Object>{
                    'success' => false, 'message' => e.getMessage()
            };
        }
    }

    public static String getDevisType(Case cse) {
        String devisType = null;
        if (cse.Type == 'First Maintenance Visit (new contract)') {
            devisType = cse.Reason__c == 'First maintenance' ? 'Prestations' : devisType;
            devisType = cse.Reason__c == 'Flat-rate visit' ? 'Visite forfaitaire' : devisType;
        } else if (cse.Type == 'Troubleshooting' || cse.Type == 'Contrôle' || cse.Type == 'Various Services') {
            devisType =  'Prestations';
        } else if (cse.Type == 'Information Request' || cse.Type == 'Installation') {
            devisType = 'Vente déquipement / Pose';
        } else if (cse.Type == 'Commissioning') {
            devisType = 'Mise en service';
        } else if (cse.Type == 'Maintenance') {
            devisType = cse.Reason__c == 'Visite sous contrat' ? 'Prestations' : devisType;
            devisType = cse.Reason__c == 'Visite hors contrat' ? 'Visite forfaitaire' : devisType;
        } else if (cse.Type == 'Administrative management') {
            devisType = cse.Reason__c == 'tous les motifs' ? null : devisType; //RRJ ***
        } else if (cse.Type == 'Claim') {
            devisType = cse.Reason__c == 'tous les motifs' ? null : devisType; //RRJ ***
        }
        return devisType;
    }

    public static String getServiceContract(Case cse){
        Case cas = [SELECT Id, AssetId, Asset.Logement__c, Contract_Line_Item__c, Service_Contract__c FROM Case WHERE Id = :cse.Id LIMIT 1];
        List<ServiceContract> lstCon = [SELECT Id FROM ServiceContract WHERE Id = :cas.Service_Contract__c AND (Contract_Status__c = 'Pending first visit' OR Contract_Status__c = 'Actif - en retard de paiement' OR Contract_Status__c = 'Active') LIMIT 1 ];
        if(lstCon.size()>0){
            return lstCon[0].Id;
        }
        system.debug('...will return null');
        return null;
    }
    
    //ZJO CT-1821  add WorkOrderNumber field
    @AuraEnabled
    public static Map<Id, String> fetchWo(String caseId){
        Map<Id, String> mapWo = new Map<Id, String>();
        for(WorkOrder wo : [SELECT Id, WorkOrderNumber, CreatedDate, WorkType.Name FROM WorkOrder WHERE CaseId = :caseId]){
            mapWo.put(wo.Id, wo.WorkType.Name+' '+Date.newInstance(wo.CreatedDate.year(), wo.CreatedDate.month(), wo.CreatedDate.day()).format()+' '+wo.WorkOrderNumber);
        }
        return mapWo;
    }

    @AuraEnabled
    public static Boolean checkEligibilityTocreateQuote(String caseId){
        Case cse = [
            select Id,  (SELECT Id FROM WorkOrders WHERE CaseId = :caseId) 
            from case where id =: caseId
            AND (Status = 'New' OR Status = 'In Progress' OR Status = 'En attente du Client' OR Status = 'En attente de Cham' OR Status = 'Pending Action from Cham' OR Status = 'Pending Action from Client')
            limit 1
        ];

        if(cse == null){
            return false;
        }else if(cse.WorkOrders == null){
            return false;
        }else{
            system.debug('cse.WorkOrders: ' + cse.WorkOrders);
            return true;
        }
    }

    @AuraEnabled
    public static Object createQuoteFromCase(String caseId, String woId) {
        //Get Case fields
        List<Case> cse = [
            SELECT CaseNumber, Status, CreatedDate, ClosedDate , Type, Reason__c, Number_of_WO__c, AccountId
                  ,Subject, isClosed, Priority, RecordType.DeveloperName, AssetId, Asset.Name, Asset.Logement__c
                  , Asset.Logement__r.Agency__c , Asset.Logement__r.Owner__c , Asset.Logement__r.Inhabitant__c, Asset.Logement__r.Country__c
                  , Asset.Logement__r.Street__c, Asset.Logement__r.City__c, Asset.Logement__r.Postal_Code__c, Campagne__c                       //RRA - 20200717 - CT-64 - l'ajout du code campagne - reference sur la page confluence
            FROM Case
            WHERE Id = :caseId
            AND (Status = 'New' OR Status = 'In Progress' OR Status = 'En attente du Client' OR Status = 'En attente de Cham' OR Status = 'Pending Action from Cham' OR Status = 'Pending Action from Client')
            LIMIT 1
		];
        if(cse.size()>0){

            //Get Account
            Account acc = [
                SELECT Id , Name, BillingCity, BillingPostalCode, BillingCountry, BillingStreet, BillingState, PersonContactId, Campagne__c, Date_fin_retombees__c // RRA Add field Campaign
                FROM Account 
                WHERE Id = :cse[0].AccountId 
                LIMIT 1
            ];

            system.debug(acc == null);

            if(acc != null & cse[0] != null){
                return createQuote(cse[0], acc, woId);
            }
        }else{
            return new Map<String, Object>{'success' => false, 'message' => 'La création d\'un devis n\'est pas permis sur ce devis'};
        }


        return null;
    }
}