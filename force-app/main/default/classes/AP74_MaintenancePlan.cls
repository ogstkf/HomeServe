/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 20-12-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-10-2020   ZJO   Initial Version
**/
public with sharing class AP74_MaintenancePlan {
    

    
    public static void createMaintenancePlanIndividual(List<ServiceContract> lstServCon){
        System.debug('### enter func createMaintenancePlan');
  
        List<MaintenancePlan> lstMPToInsert = new List<MaintenancePlan>();
        List<MaintenanceAsset> lstMAToInsert = new List<MaintenanceAsset>();
        Map<Id,String> mapScToEqpType = new Map<Id,String>();
        Map<Id,String> mapScAgenceCodeToId = new Map<Id,String>();
        Map<String,Id> mapEqpTypeToWorkType = new Map<String,Id>();

        Set<Id> SetAllConIds = new Set<Id>();
        Set<Id> SetMpConIds = new Set<Id>();
        Set<Id> SetMpToCreateConIds = new Set<Id>(); 
        List<ServiceContract> lstNewMpToCreateServCon = new List<ServiceContract>();

        System.debug('## All Service Contracts (Individual or collectif):' + lstServCon);
        
        for(ServiceContract con : lstServCon){
            SetAllConIds.add(con.Id);
        }

        //Check for existing Mps
        for(MaintenancePlan existingMp : [SELECT Id, ServiceContractId FROM MaintenancePlan WHERE ServiceContractId IN:SetAllConIds]){
            SetMpConIds.add(existingMp.ServiceContractId);   
        }
        System.debug('## Existing Maintenance Plans SetMpConIds:' + SetMpConIds);
        
        //Remove existing Mps from list of ServiceContracts
        for(ServiceContract sc : lstServCon){
            if(!SetMpConIds.contains(sc.Id) && !sc.Contrat_1ere_annee__c){
                lstNewMpToCreateServCon.add(sc);
                SetMpToCreateConIds.add(sc.Id);
            }
        }
        System.debug('## New Mp to be created:' + lstNewMpToCreateServCon);
        System.debug('## ServConIds for new Mp (compare):' + SetMpToCreateConIds);
  
        //Query only for RecType Individuel
        for(ServiceContract sc : [SELECT Id, Asset__c, Asset__r.Equipment_type_auto__c, Agency__c, Agency__r.Agency_Code__c FROM ServiceContract WHERE Id IN:SetMpToCreateConIds AND RecordType.DeveloperName = 'Contrats_Individuels']){
            if (sc.Asset__c == null)
                mapScToEqpType.put(sc.Id, null);
            else
                mapScToEqpType.put(sc.Id, sc.Asset__r.Equipment_type_auto__c);

            
            if (sc.Agency__c == null)
                mapScToEqpType.put(sc.Id, null);
            else
                mapScAgenceCodeToId.put(sc.Id, sc.Agency__r.Agency_Code__c);
        }
        System.debug('## mapScToEqpType:' + mapScToEqpType);
        
        if(mapScToEqpType.size() > 0){
            for(WorkType WT : [SELECT Id, Equipment_type__c FROM WorkType WHERE Type__c = 'Maintenance' AND Reason__c = 'Visite sous contrat' AND Equipment_type__c IN:mapScToEqpType.values()]){
                mapEqpTypeToWorkType.put(WT.Equipment_type__c, WT.Id);
            }
        }   
        System.debug('## mapEqpTypeToWorkType:' + mapEqpTypeToWorkType);
        
        Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> workTypesMap = buildWorkTypesMap(new Set<String>{'Maintenance'}, new Set<String>{'Visite sous contrat'},
                                                                                                            mapScToEqpType.values());
   
        System.debug('### before creating new MP');
        for(ServiceContract servCon : lstNewMpToCreateServCon){  
        
            MaintenancePlan newMP = new MaintenancePlan();

            //Check if RecType Individuel
            if(servCon.RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels')){      
                //Add WorkTypeId on Mp
                String ServConEqpType = mapScToEqpType.get(servCon.Id);
                System.debug('## ServCon EqpType:' + ServConEqpType);
                
                //MNA 07/06/2021 Ajout du code agence pour l'assignation du workType
                String AgenceCode = mapScAgenceCodeToId.get(servCon.Id);
                System.debug('## ServCon AgenceCode:' + AgenceCode);
                
                //MNA 06/04/2021 Assignation des "workType"
                //MNA 07/06/2021 Ajout du code agence pour l'assignation du workType
                WorkType wt = getWorktype(workTypesMap, 'Maintenance', 'Visite sous contrat', ServConEqpType, AgenceCode, 'Contrats_Individuels');
                System.debug(wt);
                newMP.WorkTypeId = (wt == null) ? null : wt.Id;

            }

            //Tec-228 ZJO 05.11.2020 - Added condition Statut VE and set DoesAutoGenerateWorkOrders to false
            if(servCon.Statut_VE__c == 'VE effectuée' || servCon.Statut_VE__c == 'VE non nécessaire'){
                newMp.DoesAutoGenerateWorkOrders = false;
            }else{
                newMp.DoesAutoGenerateWorkOrders = servCon.Generer_automatiquement_des_OE__c;
            }

            //Other fields apply for both Individuel/Collectifs
            newMp.AccountId = servCon.AccountId;
            newMp.ServiceContractId = servCon.Id;
            newMp.StartDate = servCon.StartDate;
            newMp.EndDate = servCon.EndDate;
            newMp.WorkOrderGenerationMethod = 'WorkOrderPerAsset';
            newMp.Frequency = Integer.valueOf(servCon.Frequence_de_maintenance__c);
            newMp.FrequencyType = servCon.Type_de_frequence__c;
            newMp.MaintenanceWindowStartDays = Integer.valueOf(servCon.Debut_de_la_periode_de_maintenance__c);
            newMp.MaintenanceWindowEndDays = Integer.valueOf(servCon.Fin_de_periode_de_maintenance__c);
            newMp.GenerationTimeframe = Integer.valueOf(servCon.Periode_de_generation__c);
            newMp.GenerationTimeframeType = servCon.Type_de_periode_de_generation__c;
            newMp.NextSuggestedMaintenanceDate = servCon.Date_du_prochain_OE__c;  
            newMp.GenerationHorizon = Integer.valueOf(servCon.Horizon_de_generation__c);

            lstMPToInsert.add(newMP);
            
        } 
        System.debug('### after creating new MP');
        System.debug('### List of new MP to be inserted:'+ lstMPToInsert);

        
       
        if(lstMPToInsert.size() > 0){
            Savepoint sp = Database.setSavepoint();
            try{
                System.debug('## before inserting list of MP');
                insert lstMPToInsert;
                System.debug('## after inserting list of MP');
     
                //Create Maintenance Assets for ServiceContract of RecType Individual
                for(MaintenancePlan mp : [SELECT Id, ServiceContract.Asset__c, WorkTypeId, NextSuggestedMaintenanceDate FROM MaintenancePlan WHERE ServiceContractId IN:SetMpToCreateConIds AND ServiceContract.RecordType.DeveloperName = 'Contrats_Individuels']){
                    MaintenanceAsset newMA = new MaintenanceAsset(
                        AssetId = mp.ServiceContract.Asset__c,
                        MaintenancePlanId = mp.Id,
                        WorkTypeId = mp.WorkTypeId,
                        NextSuggestedMaintenanceDate = mp.NextSuggestedMaintenanceDate
                    ); 
                    lstMAToInsert.add(newMA);
                }
                System.debug('## List of Maintenance Assets:' + lstMAToInsert);

                if (lstMAToInsert.size() > 0){
                    insert lstMAToInsert;
                }
            }catch(Exception e){
                System.debug('## error ocurred:'+e.getMessage());
                Database.rollback(sp);
                throw new AuraHandledException(e.getMessage());
            }
        }

        
    }

    // MNA 12/04/2021 Plan de maintenance pour les contrats collectifs
    public static void createMaintenancePlanCollective(List<SObject> lstSObj) {
        System.debug('### enter func createMaintenancePlan');

        Boolean isContrat;

        String query = 'SELECT Id, LineItemNumber, ServiceContractId, ServiceContract.RecordType.DeveloperName, ServiceContract.AccountId, ServiceContract.StartDate'
                                + ', ServiceContract.EndDate, ServiceContract.Debut_de_la_periode_de_maintenance__c, ServiceContract.Fin_de_periode_de_maintenance__c'
                                + ', ServiceContract.Date_du_prochain_OE__c, ServiceContract.Horizon_de_generation__c, AssetId, Asset.Equipment_type_auto__c'
                                + ', ServiceContract.Agency__c, ServiceContract.Agency__r.Agency_Code__c'
                                + ', Product2.ProductCode'
                                    + ' FROM ContractLineItem'
                                    + ' WHERE IsActive__c = true'
                                    + ' AND ServiceContract.Contrat_1ere_annee__c = false';
  
        List<MaintenancePlan> lstMPToInsert = new List<MaintenancePlan>();
        List<MaintenanceAsset> lstMAToInsert = new List<MaintenanceAsset>();

        Set<String> setEqpType = new Set<String>();
        Set<Id> setScId = new Set<Id>();
        Set<Id> setCliId = new Set<Id>();
        
        Map<String, MaintenancePlan> mapMpToScIdAndEqt = new Map<String, MaintenancePlan>();                        //MP existing
        Map<String, MaintenanceAsset> mapMaToScIdAndEqtAndAstId = new Map<String, MaintenanceAsset>();              //MA existing
        Map<String, String> mapScIdAndEqtToAstId = new Map<String, String>();

        for (SObject SObj : lstSObj) {
            if (String.valueOf(SObj.get('Id')).startsWith('810')) {
                setScId.add(String.valueOf(SObj.get('Id')));
                isContrat = true;
            }
            else {
                setScId.add(String.valueOf(SObj.get('ServiceContractId')));
                setCliId.add(String.valueOf(SObj.get('Id')));
                isContrat = false;
            }
        }

        if (isContrat) {
            query += ' AND ServiceContractId IN :setScId';
        }
        else {
            query += ' AND Id IN :setCliId';
        }

        System.debug(setCliId);

        System.debug(query);
        for(ContractLineItem cli : [SELECT Id, AssetId, Asset.Equipment_type_auto__c FROM ContractLineItem WHERE ServiceContractId IN:setScId]) {
            if (cli.AssetId != null && cli.Asset.Equipment_type_auto__c != null) 
                setEqpType.add(cli.Asset.Equipment_type_auto__c);
        }

        Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> workTypesMap = buildWorkTypesMap(new Set<String>{'Maintenance'}, new Set<String>{'Visite sous contrat'}, setEqpType);


        for (MaintenancePlan Mp : [SELECT Id, MaintenancePlanTitle, ServiceContractId, WorkTypeId, NextSuggestedMaintenanceDate
                                            FROM MaintenancePlan WHERE ServiceContractId IN:setScId]) {

            mapMpToScIdAndEqt.put(Mp.ServiceContractId + '-' + Mp.MaintenancePlanTitle, Mp);
        }

        for (MaintenanceAsset Ma : [SELECT Id, AssetId, MaintenancePlanId, MaintenancePlan.MaintenancePlanTitle, MaintenancePlan.ServiceContractId
                                            FROM MaintenanceAsset WHERE MaintenancePlan.ServiceContractId IN:setScId]) {

            mapMaToScIdAndEqtAndAstId.put(Ma.MaintenancePlan.ServiceContractId + '-' + Ma.MaintenancePlan.MaintenancePlanTitle + '-' + Ma.AssetId, Ma);
        }

        System.debug('#####' + mapMpToScIdAndEqt);
        System.debug('#####' + mapMaToScIdAndEqtAndAstId);
                                                 
        /*for (ContractLineItem cli: [SELECT Id, LineItemNumber, ServiceContractId, ServiceContract.RecordType.DeveloperName, ServiceContract.AccountId, ServiceContract.StartDate,
                                        ServiceContract.EndDate, ServiceContract.Debut_de_la_periode_de_maintenance__c, ServiceContract.Fin_de_periode_de_maintenance__c,
                                        ServiceContract.Date_du_prochain_OE__c, ServiceContract.Horizon_de_generation__c,
                                        AssetId, Asset.Equipment_type_auto__c
                                            FROM ContractLineItem WHERE ServiceContractId IN :setScId]) {*/
        for (ContractLineItem cli: Database.query(query)) {
            System.debug(cli.Id);            
            if (cli.AssetId != null && cli.Asset.Equipment_type_auto__c != null && cli.Product2.ProductCode.startsWith('VF-')) {
                
                if (!mapMpToScIdAndEqt.containsKey(cli.ServiceContractId + '-' + cli.Asset.Equipment_type_auto__c)) {
                    WorkType wt;
                    if (cli.ServiceContract.Agency__c == null)
                        wt = getWorktype(workTypesMap, 'Maintenance', 'Visite sous contrat', cli.Asset.Equipment_type_auto__c, null, cli.ServiceContract.RecordType.DeveloperName);
                    else
                        wt = getWorktype(workTypesMap, 'Maintenance', 'Visite sous contrat', cli.Asset.Equipment_type_auto__c, cli.ServiceContract.Agency__r.Agency_Code__c , cli.ServiceContract.RecordType.DeveloperName);
                    
                    System.debug(wt);
                    
                    MaintenancePlan newMP = new MaintenancePlan();
                    newMp.AccountId = cli.ServiceContract.AccountId;
                    newMp.ServiceContractId = cli.ServiceContractId;
                    newMp.StartDate = cli.ServiceContract.StartDate;
                    newMp.EndDate = cli.ServiceContract.EndDate;
                    newMp.WorkOrderGenerationMethod = 'WorkOrderPerAsset';
                    newMp.Frequency = 12;
                    newMp.FrequencyType = 'Months';
                    newMp.GenerationTimeframe = 12;
                    newMp.GenerationTimeframeType = 'Months';
                    newMp.MaintenanceWindowStartDays = Integer.valueOf(cli.ServiceContract.Debut_de_la_periode_de_maintenance__c);
                    newMp.MaintenanceWindowEndDays = Integer.valueOf(cli.ServiceContract.Fin_de_periode_de_maintenance__c);
                    newMp.NextSuggestedMaintenanceDate = cli.ServiceContract.Date_du_prochain_OE__c;  
                    newMp.GenerationHorizon = Integer.valueOf(cli.ServiceContract.Horizon_de_generation__c);
                    newMp.MaintenancePlanTitle = cli.Asset.Equipment_type_auto__c;
                    newMP.WorkTypeId = (wt == null) ? null : wt.Id;
                    lstMPToInsert.add(newMP);

                    mapMpToScIdAndEqt.put(cli.ServiceContractId + '-' + cli.Asset.Equipment_type_auto__c, newMP);
                }

                if (!mapScIdAndEqtToAstId.containsKey(cli.AssetId)) {
                    System.debug('here');
                    mapScIdAndEqtToAstId.put(cli.AssetId, cli.ServiceContractId + '-' + cli.Asset.Equipment_type_auto__c);
                }

            }
            
        }

        if (lstMPToInsert.size() > 0) {
            insert lstMPToInsert;
        }
        

        for(String AstId : mapScIdAndEqtToAstId.keySet()) {
            MaintenancePlan mp = mapMpToScIdAndEqt.get(mapScIdAndEqtToAstId.get(AstId));
            if (!mapMaToScIdAndEqtAndAstId.containsKey(Mp.ServiceContractId + '-' + Mp.MaintenancePlanTitle + '-' + AstId)) {
                lstMAToInsert.add(new MaintenanceAsset(
                    AssetId = AstId,
                    MaintenancePlanId = mp.Id,
                    WorkTypeId = mp.WorkTypeId,
                    NextSuggestedMaintenanceDate = mp.NextSuggestedMaintenanceDate
                ));
            }
        }

        if (lstMAToInsert.size() > 0) {
            insert lstMAToInsert;
        }
    }

    public static void fetchCliContracts(List<ContractLineItem> lstClis){
        

        /*Set<Id> SetServConIds = new Set<Id>();
        Set<Id> SetCliAssetIds = new Set<Id>();
        Set<Id> SetAssetIds = new Set<Id>();
        Set<Id> SetCliServConIds = new Set<Id>();
        List<ContractLineItem> lstNewMAToCreate = new List<ContractLineItem>();
        List<MaintenanceAsset> lstNewMAToInsert = new List<MaintenanceAsset>();
        Map<Id,MaintenancePlan> mapServConToMp = new Map<Id,MaintenancePlan>();

        Map<Id,String> mapCliToEqpType = new Map<Id,String>();
        Map<String,Id> mapEqpTowrkType = new Map<String,Id>();

        for(ContractLineItem clineItem : [SELECT Id, ServiceContractId FROM ContractLineItem WHERE Id IN:lstClis]){
            SetServConIds.add(clineItem.ServiceContractId);        
        }

        List<ServiceContract> lstCliServCon = [SELECT Id, RecordTypeId, AccountId, StartDate, EndDate, Frequence_de_maintenance__c, Type_de_frequence__c, Debut_de_la_periode_de_maintenance__c, Fin_de_periode_de_maintenance__c, Periode_de_generation__c, Type_de_periode_de_generation__c, Date_du_prochain_OE__c, Generer_automatiquement_des_OE__c, Horizon_de_generation__c FROM ServiceContract WHERE Id IN:SetServConIds];
        System.debug('## Contrats collectifs lstCliServCon:' + lstCliServCon);
        createMaintenancePlan(lstCliServCon);
     
        for(ContractLineItem lineItem : lstClis){
            SetCliAssetIds.add(lineItem.AssetId);
        }
        System.debug('## All cli Asset Ids:' + SetCliAssetIds);

        //Check for existing MA
        for(MaintenanceAsset existingMA : [SELECT Id, AssetId FROM MaintenanceAsset WHERE MaintenancePlan.ServiceContractId IN:SetServConIds AND AssetId IN:SetCliAssetIds]){
            SetAssetIds.add(existingMA.AssetId);
        }
        System.debug('## Existing Maintenance Assets:' + SetAssetIds);

        //Remove existing MAs from list of clis
        for(ContractLineItem conLineItem : lstClis){
            if(!SetAssetIds.contains(conLineItem.AssetId)){
                lstNewMAToCreate.add(conLineItem);
                SetCliServConIds.add(conLineItem.ServiceContractId);
            }
        }
        System.debug('## list of new MA to be created:' + lstNewMAToCreate);
        System.debug('## ServConIds for new MA(compare):' + SetCliServConIds);

        //Query Maintenance Plan associated with Cli ServiceContract
        for(MaintenancePlan MP : [SELECT Id, ServiceContractId, NextSuggestedMaintenanceDate FROM MaintenancePlan WHERE ServiceContractId IN:SetCliServConIds]){
            mapServConToMp.put(MP.ServiceContractId, MP);
        }
        System.debug('## map ServConId to MP:' + mapServConToMp);

        //Query Equipment Type on CLI
        for(ContractLineItem conLi : [SELECT Id, Asset.Equipment_type_auto__c FROM ContractLineItem WHERE Id IN:lstNewMAToCreate]){
            mapCliToEqpType.put(conLi.Id, conLi.Asset.Equipment_type_auto__c);
        }

        //Query WorkType
        if(mapCliToEqpType.size() > 0){
            for(WorkType wrkType : [SELECT Id, Equipment_type__c FROM WorkType WHERE Type__c = 'Maintenance' AND Reason__c = 'Visite sous contrat' AND Equipment_type__c IN:mapCliToEqpType.values()]){
                mapEqpTowrkType.put(wrkType.Equipment_type__c, wrkType.Id);
            }
        }

        //Create New MA for contracts RecType Collectifs
        for(ContractLineItem cli : lstNewMAToCreate){
            String cliEqpType = mapCliToEqpType.get(cli.Id);

            MaintenanceAsset MA = new MaintenanceAsset(
                MaintenancePlanId = mapServConToMp.get(cli.ServiceContractId).Id,
                AssetId = cli.AssetId,
                WorkTypeId = mapEqpTowrkType.get(cliEqpType),
                NextSuggestedMaintenanceDate = mapServConToMp.get(cli.ServiceContractId).NextSuggestedMaintenanceDate,
                ContractLineItemId = cli.Id
            );
            lstNewMAToInsert.add(MA);
        }
        System.debug('## List of MA to insert (collectifs):' + lstNewMAToInsert);

        if(lstNewMAToInsert.size() > 0){
            insert lstNewMAToInsert;
        }*/

    }

    public static void updateStartEndDate(List<ServiceContract> lstUpdatedServCon){
        List<MaintenancePlan> lstMPToUpdate = new List<MaintenancePlan>();

        for(MaintenancePlan mp : [SELECT Id, StartDate, EndDate, ServiceContract.StartDate, ServiceContract.EndDate FROM MaintenancePlan WHERE ServiceContractId IN:lstUpdatedServCon]){
            mp.StartDate = mp.ServiceContract.StartDate;
            mp.EndDate = mp.ServiceContract.EndDate;

            lstMPToUpdate.add(mp);
        }

        if(lstMPToUpdate.size() > 0){
            update lstMPToUpdate;
        }
    }

    public static void updateCaseSA(List<ServiceContract> lstCancelledServCon){
        Set<Id> SetMPIds = new Set<Id>();
        Set<Id> SetWOIds = new Set<Id>();
        Set<Id> SetCaseIds = new Set<Id>();
        List<ServiceAppointment> lstSaToUpdate = new List<ServiceAppointment>();
        List<Case> lstCaseToUpdate = new List<Case>();

        //Query MP
        for(MaintenancePlan MP : [SELECT Id, ServiceContractId FROM MaintenancePlan WHERE ServiceContractId IN:lstCancelledServCon]){
            SetMPIds.add(MP.Id);
        }

        //Query WO
        for(WorkOrder WO : [SELECT Id, CaseId, Status FROM WorkOrder WHERE MaintenancePlanId IN:SetMPIds AND Status = 'New']){
           SetWOIds.add(WO.Id);
           SetCaseIds.add(WO.CaseId);
        }

        //Query SA and Update Status to Cancelled
        for(ServiceAppointment SA : [SELECT Id, Status FROM ServiceAppointment WHERE Work_Order__c IN:SetWOIds]){
            SA.Status = 'Cancelled';
            lstSaToUpdate.add(SA);
        }

        //Query Case and update Status to Close
        for(Case cse : [SELECT Id, Status FROM Case WHERE Id IN:SetCaseIds]){
            cse.Status = 'Closed';
            lstCaseToUpdate.add(cse);
        }

        if(lstSaToUpdate.size() > 0){
            update lstSaToUpdate;
        }

        if(lstCaseToUpdate.size() > 0){
            update lstCaseToUpdate;
        }
    }
    
    public static void updateSC(List<String> lstMPId){
    	system.debug('***in handleAfterInsert: ');
        List <ServiceContract> lstScToUpd = new List <ServiceContract>();

        Set<Id> SetServConId = new Set<Id>();
        for(MaintenancePlan mp : [SELECT id, ServiceContractId FROM MaintenancePlan WHERE id IN:lstMPId]){
            SetServConId.add(mp.ServiceContractId);
        }

        for(ServiceContract sc : [SELECT id, TECH_Plan_de_maintenance_cr__c FROM ServiceContract WHERE id IN:SetServConId]){
            sc.TECH_Plan_de_maintenance_cr__c = true;
            lstScToUpd.add(sc);
        }

        if(lstScToUpd.size()>0){
            update lstScToUpd;
        }      
    }

    public static WorkType getWorktype(Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> workTypesMap, String type, String reason, String eqmtType, String AgenceCode, String scRecType){
        if(workTypesMap.containsKey(type) && workTypesMap.get(type).containsKey(reason) ) {
            if (workTypesMap.get(type).get(reason).containsKey(eqmtType) && workTypesMap.get(type).get(reason).get(eqmtType).containsKey(AgenceCode) 
                && (workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).containsKey('Tous')
                    || (workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).containsKey('Copro-Syndic') && scRecType == 'Contrats_Collectifs_Prives')
                    || (workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).containsKey('Bailleur') && scRecType == 'Contrats_collectifs_publics')
                    || (workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).containsKey('Particulier') && scRecType == 'Contrats_Individuels')
                )
            ) {
                if (scRecType == 'Contrats_Collectifs_Prives')
                    return workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).containsKey('Copro-Syndic')
                            ? workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).get('Copro-Syndic')
                            : workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).get('Tous');
                
                else if(scRecType == 'Contrats_collectifs_publics')
                    return workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).containsKey('Bailleur')
                            ? workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).get('Bailleur')
                            : workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).get('Tous');

                else if(scRecType == 'Contrats_Individuels') 
                    return workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).containsKey('Particulier')
                            ? workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).get('Particulier')
                            : workTypesMap.get(type).get(reason).get(eqmtType).get(AgenceCode).get('Tous');
            }

            else if (workTypesMap.get(type).get(reason).containsKey(eqmtType) && workTypesMap.get(type).get(reason).get(eqmtType).containsKey('Toutes') 
                && (workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').containsKey('Tous')
                    || (workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').containsKey('Copro-Syndic') && scRecType == 'Contrats_Collectifs_Prives')
                    || (workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').containsKey('Bailleur') && scRecType == 'Contrats_collectifs_publics')
                    || (workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').containsKey('Particulier') && scRecType == 'Contrats_Individuels')
                )
            ) {
                if (scRecType == 'Contrats_Collectifs_Prives')
                    return workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').containsKey('Copro-Syndic')
                            ? workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').get('Copro-Syndic')
                            : workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').get('Tous');
                
                else if(scRecType == 'Contrats_collectifs_publics')
                    return workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').containsKey('Bailleur')
                            ? workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').get('Bailleur')
                            : workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').get('Tous');

                else if(scRecType == 'Contrats_Individuels') 
                    return workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').containsKey('Particulier')
                            ? workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').get('Particulier')
                            : workTypesMap.get(type).get(reason).get(eqmtType).get('Toutes').get('Tous');
            }
        }
        return null;
    }

    public static Map<String,Map<String,Map<String,Map<String, Map<String,WorkType>>>>> buildWorkTypesMap(Set<String> types, Set<String> reasons, List<String> equipementTypes){
        Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> ret = new Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>>();
        List<WorkType> workTypes= [
            SELECT Id, Type__c, Reason__c, Equipment_Type__c, DurationType, EstimatedDuration, Type_de_client__c, Agence__c 
            FROM WorkType
            WHERE Type__c = :types OR Reason__c = : reasons OR Equipment_type__c = :equipementTypes];
        for(WorkType wt : workTypes){
            if(!ret.containsKey(wt.Type__c))                                                                                                        //Type
                ret.put(wt.Type__c, new Map<String,Map<String,Map<String,Map<String, WorkType>>>>());
            if(!ret.get(wt.Type__c).containsKey(wt.Reason__c))                                                                                      //Reason
                ret.get(wt.Type__c).put(wt.Reason__c, new Map<String,Map<String,Map<String, WorkType>>>());
            if(!ret.get(wt.Type__c).get(wt.Reason__c).containsKey(wt.Equipment_Type__c))                                                            //Type d'Equipment
                ret.get(wt.Type__c).get(wt.Reason__c).put(wt.Equipment_Type__c, new Map<String,Map<String, WorkType>>());                           
            if (!ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).containsKey(wt.Agence__c))                                         //Code agence
                ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).put(wt.Agence__c, new Map<String, WorkType>());
            if(!ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).get(wt.Agence__c).containsKey(wt.Type_de_client__c))                //Type de Client
                ret.get(wt.Type__c).get(wt.Reason__c).get(wt.Equipment_Type__c).get(wt.Agence__c).put(wt.Type_de_client__c, wt);
            
        }

        return ret;
    }
    
    public static Map<String,Map<String,Map<String,Map<String,Map<String, WorkType>>>>> buildWorkTypesMap(Set<String> types, Set<String> reasons, Set<String> equipementTypes){
        List<String> lstEqpType = new List<String>();
        lstEqpType.addAll(equipementTypes);
        return  buildWorkTypesMap(types, reasons, lstEqpType);
    }
}