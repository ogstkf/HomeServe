/**
 * @File Name          : AP41_OrderConditions.cls
 * @Description        : 
 * @Author             : Spoon Consulting (KZE)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 28/01/2020, 11:45:05
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         12-11-2019               KZE         Initial Version (CT-1175)
 * 1.1         14-11-2019               LGO          CT-1175 
 * 1.0         27/11/2019              AMO         Initial Version
**/


public with sharing class AP41_OrderConditions {
    
    //Method to generate PDF in case order is commande emise
    @Future(callout=true)
    public static void generatePDF(List<Id> lstNewOrder){
        
        system.debug('## starting method generatePDFOrder');
        List<ContentDocumentLink> lstPdfs = new List<ContentDocumentLink>();

        List<Order> lstOrd = [SELECT Id, Name, OrderNumber FROM Order WHERE Id IN :lstNewOrder];

        for(Order ord : lstOrd){
            PageReference pdf = Page.VFP39_OrderItemPDF;
            Blob body;

            pdf.getParameters().put('id',ord.Id);
            body = Test.isRunningTest() ? blob.valueOf('Unit.Test') : pdf.getContent(); //testMethods do not support getContent call
            //body =  pdf.getContent();
            ContentVersion conVer = new ContentVersion();
            conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
            conVer.PathOnClient = 'PDF Commande fournisseur.pdf'; // The files name, extension is very important here which will help the file in preview.
            
            //get current user time: 
            Datetime now = Datetime.now();
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);

            conVer.Title = ord.OrderNumber + '_' + local +'.pdf';// Display name of the files
            conVer.VersionData = body; // File body in blob; else needs convertion using EncodingUtil.base64Decode(body);
            insert conVer;    //Insert ContentVersion

            // First get the Content Document Id from ContentVersion Object
            Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
            //create ContentDocumentLink  record 
            ContentDocumentLink conDocLink = New ContentDocumentLink();
            conDocLink.LinkedEntityId = ord.Id; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
            conDocLink.ContentDocumentId = conDoc;  //ContentDocumentId Id from ContentVersion
            conDocLink.shareType = 'V';
            lstPdfs.add(conDocLink);

        }
      
        if(lstPdfs.size() > 0){
            insert lstPdfs;
        }
    }

    
    //Fill only dates based on order status
    public static void afterInsertOrderFD(List<Order> lstOrd){
        List<Order> lstOrder = new List<Order>();
        lstOrder = fillDate(lstOrd);
        Update lstOrder;
    }

    public static void beforeUpdtOrderFD(List<Order> lstOrd){
        System.debug('Enter conditions before update for date de livraison Enter AP');
        List<Order> lstOrder = new List<Order>();
        lstOrder = fillDate(lstOrd);
    }

    //public static void  updateOLIOrderChange(List<Order> lstOrders){

        //List<OrderItem> lstOrdItem = new List<OrderItem>();

        //List<Order> lstOrderPr =  [SELECT Id, Remise_mode__c, (SELECT Id, Remise_mode_de_transmission__c  FROM OrderItems) FROM Order WHERE Id IN :lstOrders];

        //for(Order ord : lstOrderPr){
        //    if(ord.OrderItems != null){
        //        for(OrderItem ordI : ord.OrderItems){
        //            ordI.Remise_mode_de_transmission__c = ord.Remise_mode__c;
        //            lstOrdItem.add(ordI);
        //        }
        //    }
        //}
        //Update lstOrdItem;
    //}
    
    /*public static void updateListUnitPrince(List<OrderItem> lstOrderItem){
        
        for(OrderItem ordItem :lstOrderItem){
            ordItem.UnitPrice = ordItem.ListPrice - ((ordItem.Remise100__c / 100)+ ordItem.Remise_mode_de_transmission__c);
        }
    }*/

    /*public static void InsertOrderProduct(List<OrderItem> lstOrderItem){
        
        List<OrderItem> lstOrderItems = new List<OrderItem>();
        Set<Id> setOrderId = new Set<Id>();

        for(OrderItem ord : lstOrderItem){
            setOrderId.add(ord.OrderId);
        }

        List<Order> lstNewOrder = new List<Order>();
        List<Order> lstOrder = [SELECT Id, AccountId, Pricebook2Id, Montant_total_de_la_commande__c, Type_de_livraison_de_la_commande__c, Mode_de_transmission__c, TotalAmount, EffectiveDate FROM Order WHERE Id IN :setOrderId];

        lstNewOrder = orderCalculations(lstOrder);

        if(lstNewOrder.size() > 0){
            Update lstNewOrder;
        }
        
    }*/

    /*public static void updateOrderConditionsORStatus(List<Order> lstNewOrder){
        List<Order> lstOrd = new List<Order>();
        lstOrd = orderCalculations(lstNewOrder);
    }*/
    
    public static Date getEndDate(Date inputDate, Integer numDelai){
        Integer numdays = 0; 
        Datetime inpDate = inputDate;

        while(numdays != numDelai){
            inpDate = inpDate.addDays(1);

            String day = inpDate.format('E');
            if(!(day.equalsIgnoreCase('Sat') || day.equalsIgnoreCase('Sun'))){
                numdays++;
            }
        }
        
        Date dateToreturn = Date.newInstance(inpDate.year(), inpDate.month(), inpDate.day());
        
        return dateToreturn;
    }
/*
    public static void reInitialiseOrder(Order o){
        o.EndDate = null;
        o.Remise_Mode__c = null;
        o.Frais_de_port__c = null;
        o.Franco_de_port_applicable__c = false;
    }
*/
/*
    public static void updateModeTransOnInit(List<Order> lstNewOrder){
        List<Order> lstOrdToUpd = new List<Order>();

        for(Order ord :[SELECT Id,
                        AccountId, 
                        Account.Mode_de_transmission_privil_gi__c
                        FROM Order
                        WHERE Id IN: lstNewOrder
                        AND Account.Mode_de_transmission_privil_gi__c != null]){
            lstOrdToUpd.add(New Order(Id = ord.Id, Mode_de_transmission__c= ord.Account.Mode_de_transmission_privil_gi__c));
        }

        List<Order> lstOrd = new List<Order>();
        lstOrd = orderCalculations(lstOrdToUpd);

        
        if(lstOrdToUpd.size() >0 ){
            update lstOrd;
        }

    }
*/
    /*public static void updateFromOrderItems(Set<Id> setOrders){

        List<Order> lstNewOrder = [SELECT AccountId 
                                            ,Pricebook2Id
                                            ,Montant_total_de_la_commande__c
                                            ,Type_de_livraison_de_la_commande__c
                                            ,Mode_de_transmission__c
                                            ,TotalAmount
                                            ,EffectiveDate
                                    FROM Order 
                                    WHERE Id IN :setOrders
        ];

        List<Order> lstOrd = new List<Order>();
        lstOrd = orderCalculations(lstNewOrder);
        
        if(!lstOrd.isEmpty()){
            update lstOrd;
        }
    }*/

    /*public static List<Order> orderCalculations(List<Order> lstOrder){
        Set<Id> setAccIds = new Set<Id>();
        Set<Id> setPriceBookIds = new Set<Id>();
        Boolean gotCondition= false;

        Map<Id, List<Conditions__c>> mapAccCondtions = new Map<Id, List<Conditions__c>>();
        List<Conditions__c> lstConditionOrder = new List<Conditions__c>();

        for(Order ord : lstOrder){
            setAccIds.add(ord.AccountId);
            setPriceBookIds.add(ord.Pricebook2Id);
        }

        List<Conditions__c> lstCon = [SELECT Mode__c, Id, Compte__c, Compte__r.Id, Catalogue__c, Actif__c, Delai__c, Min__c, Montant_maximum__c,Type__c,
                                            Frais_de_port__c, Remise_Mode__c, Heure_Limite__c FROM Conditions__c 
                                            WHERE  Compte__c IN :setAccIds AND Catalogue__c IN :setPriceBookIds  AND Actif__c = true
                                            AND RecordTypeId= :AP_Constant.getRecTypeId('Conditions__c', 'Header')];
        
        for(Conditions__c con : lstCon){
            if(mapAccCondtions.containsKey(con.Compte__r.Id)){
                mapAccCondtions.get(con.Compte__r.Id).add(con);
            }
            else{
                mapAccCondtions.put(con.Compte__r.Id, new List<Conditions__c>{con});
            }
        }

        for(Integer i = 0; i < lstOrder.size(); i++){
            System.debug('ENTER');
            gotCondition = false;
            reInitialiseOrder(lstOrder[i]);

            if(mapAccCondtions.containsKey(lstOrder[i].AccountId)){
                lstConditionOrder = mapAccCondtions.get(lstOrder[i].AccountId);

                if(lstConditionOrder.size() > 0){
                    for(Conditions__c c : lstConditionOrder){
                        if(c.Compte__c == lstOrder[i].AccountId && c.Catalogue__c == lstOrder[i].Pricebook2Id){
                            if(c.Min__c < lstOrder[i].Montant_total_de_la_commande__c && ((c.Montant_maximum__c == null) || (c.Montant_maximum__c != null && lstOrder[i].Montant_total_de_la_commande__c < c.Montant_maximum__c))
                                && (c.Mode__c != null && lstOrder[i].Mode_de_transmission__c != null && c.Mode__c.contains(lstOrder[i].Mode_de_transmission__c))
                                && (c.Type__c != null && lstOrder[i].Type_de_livraison_de_la_commande__c != null && c.Type__c == lstOrder[i].Type_de_livraison_de_la_commande__c)){
                                    lstOrder[i].Remise_Mode__c = c.Remise_Mode__c;
                                    lstOrder[i].Frais_de_port__c = c.Frais_de_port__c;

                                    if(lstOrder[i].Frais_de_port__c == 0){
                                        lstOrder[i].Franco_de_port_applicable__c = true;
                                    }
                                }
                                if(lstOrder[i].Mode_de_transmission__c != null){
                                    if(c.Frais_de_port__c == 0 && c.Type__c == 'Standard' && c.Mode__c.contains(lstOrder[i].Mode_de_transmission__c)){
                                        lstOrder[i].Seuil_Franco__c = c.Min__c + ' €';
                                        gotCondition = true;
                                    }
                                }    
                        }
                    }
                }
            }
            if(gotCondition == false)
                lstOrder[i].Seuil_Franco__c = 'N/A';
        }
        return lstOrder;
    }*/


     public static List<Order> fillDate (List<Order> lstOrder){
        /* System.debug('Enter conditions before update for date de livraison Enter Fill Date');
        Set<Id> setAccIds = new Set<Id>();
        Set<Id> setPriceBookIds = new Set<Id>();
        Boolean gotCondition= false;

        Map<Id, List<Conditions__c>> mapAccCondtions = new Map<Id, List<Conditions__c>>();
        List<Conditions__c> lstConditionOrder = new List<Conditions__c>();

        for(Order ord : lstOrder){
            setAccIds.add(ord.AccountId);
            setPriceBookIds.add(ord.Pricebook2Id);
        }

        List<Conditions__c> lstCon = [SELECT Mode__c, Id, Compte__c, Compte__r.Id, Catalogue__c, Actif__c, Delai__c, Min__c, Montant_maximum__c,Type__c,
                                            Frais_de_port__c, Remise_Mode__c, Heure_Limite__c FROM Conditions__c 
                                            WHERE  Compte__c IN :setAccIds AND Catalogue__c IN :setPriceBookIds  AND Actif__c = true
                                            AND RecordTypeId= :AP_Constant.getRecTypeId('Conditions__c', 'Header')];
        
        for(Conditions__c con : lstCon){
            if(mapAccCondtions.containsKey(con.Compte__r.Id)){
                mapAccCondtions.get(con.Compte__r.Id).add(con);
            }
            else{
                mapAccCondtions.put(con.Compte__r.Id, new List<Conditions__c>{con});
            }
        }

        for(Integer i = 0; i < lstOrder.size(); i++){
            gotCondition = false;
            reInitialiseOrder(lstOrder[i]);

            if(mapAccCondtions.containsKey(lstOrder[i].AccountId)){
                lstConditionOrder = mapAccCondtions.get(lstOrder[i].AccountId);

                if(lstConditionOrder.size() > 0){
                     System.debug('Enter conditions before update for date de livraison Enter Conditions');
                    for(Conditions__c c : lstConditionOrder){
                        if(c.Compte__c == lstOrder[i].AccountId && c.Catalogue__c == lstOrder[i].Pricebook2Id){
                            if(c.Min__c < lstOrder[i].Montant_total_de_la_commande__c && ((c.Montant_maximum__c == null) || (c.Montant_maximum__c != null && lstOrder[i].Montant_total_de_la_commande__c < c.Montant_maximum__c))
                                && (c.Mode__c != null && lstOrder[i].Mode_de_transmission__c != null && c.Mode__c.contains(lstOrder[i].Mode_de_transmission__c))
                                && (c.Type__c != null && lstOrder[i].Type_de_livraison_de_la_commande__c != null && c.Type__c == lstOrder[i].Type_de_livraison_de_la_commande__c)){
                                    Decimal delay = c.Delai__c == null? 2 : c.Delai__c;
                                    lstOrder[i].Date_de_livraison_prevue__c = AP41_OrderConditions.getEndDate(lstOrder[i].EffectiveDate, Integer.valueOf(delay));
                                    lstOrder[i].EndDate = null;
                                    lstOrder[i].Remise_Mode__c = c.Remise_Mode__c;
                                    lstOrder[i].Frais_de_port__c = c.Frais_de_port__c;

                                    if(lstOrder[i].Frais_de_port__c == 0){
                                        lstOrder[i].Franco_de_port_applicable__c = true;
                                    }
                            }
                            
                        }
                    }
                }
            }
            if(lstConditionOrder.size() == 0){
                System.debug('Enter conditions befor update for date de livraison where Conditions not present');
                lstOrder[i].Date_de_livraison_prevue__c = AP41_OrderConditions.getEndDate(lstOrder[i].EffectiveDate, 2);
            }
        }*/
        for(Integer i = 0; i < lstOrder.size(); i++){
            lstOrder[i].Date_de_livraison_prevue__c = AP41_OrderConditions.getEndDate(lstOrder[i].EffectiveDate, 2);
        }
        return lstOrder;
     }

/*
    public static String getConditionRecordType(String devName){
        return Schema.SObjectType.Conditions__c.getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
    }  
*/
}