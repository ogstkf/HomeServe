/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 09-11-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   09-11-2020   ZJO   Initial Version
**/
@isTest
public with sharing class LC74_GenerateAvisDeRelance_TEST {
    static User adminUser;
    static Account testAcc = new Account();
    static Product2 testProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static ServiceContract testServCon = new ServiceContract();
    static ContentVersion cv  = new ContentVersion();

    static{

        adminUser = TestFactory.createAdminUser('LC74_GenerateAvisDeRel@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){

            //Create Account
            testAcc = TestFactory.createAccount('testAcc');
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            //Update Account
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //Create Product
            testProd = TestFactory.createProduct('TestProd');
            insert testProd;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create ServCon
            testServCon =   new ServiceContract(
                name = 'servcon 1',
                Contract_Status__c = 'Draft',
                Pricebook2Id = lstPrcBk[0].Id,
                AccountId = testAcc.Id,
                TECH_Date_Edition_R1__c = System.today().addDays(-2)         
            );      
            insert testServCon;

            //Create Content Version
            cv = new ContentVersion(
                Title = 'testFile.pdf', 
                Description = 'This is a test class file',
                OwnerId = adminUser.Id,
                PathOnClient = 'testFile.pdf',
                VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body')
            );
            insert cv;
        }
    }

    @isTest
    public static void testsaveAvisRelance(){
        System.runAs(adminUser){

            Test.startTest();
                ContentDocumentLinkTriggerHandler.bypass = true;
                map<string,Object> mapOfResult = LC74_GenerateAvisDeRelance.saveAvisRelance(testServCon.Id);
            Test.stopTest();
        }
    }
}