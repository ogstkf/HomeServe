/**
 * @description       : 
 * @author            : MNA (Spoon Consulting)
 * @group             : 
 * @last modified on  : 09-11-2021
 * @last modified by  : MNA
 * Modifications Log 
 * =========================================================================================
 * Ver   Date               Author                               Modification
 * 1.0   04-02-2021         MNA                                  Initial Version
**/
public with sharing class LC76GenerateMassAvisDePassage {
    @AuraEnabled
    public static List<Object> getSA(List<Id> lstSAId) {
        List<ServiceAppointment> lstSA = [SELECT Id, AppointmentNumber, Residence__c, Residence__r.Visit_Notice_Recipient__c, Residence__r.Inhabitant__c,
                                            Residence__r.Owner__c, Residence__r.Legal_Guardian__c, Residence__r.Account__c
                                                    FROM ServiceAppointment WHERE Id IN :lstSAId ORDER BY AppointmentNumber ASC];
        List<getData> lstData = new List<getData>();
        System.debug(lstSAId.size());
        System.debug(lstSA.size());
        Map<String, ServiceAppointment> mapSaToId = new Map<String, ServiceAppointment>();
        Map<String, String> mapAccIdToSaId = new Map<String, String>();
                    
        for (ServiceAppointment SA: lstSA) {
            mapSaToId.put(SA.Id, SA);
            if (SA.Residence__c != null) {
                //MNA 15/07/2021 TEC-747
                switch on SA.Residence__r.Visit_Notice_Recipient__c {
                    when 'Inhabitant' {
                        if(SA.Residence__r.Inhabitant__c != null) 
                            mapAccIdToSaId.put(SA.Id, SA.Residence__r.Inhabitant__c);
                    }
                    when 'Owner' {
                        if(SA.Residence__r.Owner__c != null) 
                            mapAccIdToSaId.put(SA.Id, SA.Residence__r.Owner__c);
                    }
                    when 'Legal Guardian' {
                        if(SA.Residence__r.Legal_Guardian__c != null) 
                            mapAccIdToSaId.put(SA.Id, SA.Residence__r.Legal_Guardian__c);
                    }
                    when 'Administrator' {
                        if(SA.Residence__r.Account__c != null) 
                            mapAccIdToSaId.put(SA.Id, SA.Residence__r.Account__c);
                    }
                }
                
            }
        }
        
        Map<Id, Account> mapAccToId = new Map<Id, Account>(
                        [SELECT Id, RecordType.DeveloperName, Name, PersonEmail, BillingStreet,
                                BillingPostalCode, BillingCity,
                                (SELECT Id, Email FROM Contacts WHERE contact_principal__c = true)
                                FROM Account WHERE Id IN :mapAccIdToSaId.values()]);

        for (String strKey : mapSaToId.keySet()) {
            ServiceAppointment sa = mapSaToId.get(strKey);
            Account acc = null;
            if (mapAccIdToSaId.containsKey(strKey))
                acc = mapAccToId.get(mapAccIdToSaId.get(strKey));
            lstData.add(new getData(sa, acc));
        }
        System.debug(lstData.size());
        return lstData;

    }

    @AuraEnabled
    public static void generateAvisPassage(List<Id> lstSAIdEmail, List<Id> lstSAIdCourrier, List<Id> lstSAIdError) {
        /*System.debug(lstSAIdEmail);
        System.debug(lstSAIdCourrier);
        System.debug(lstSAIdError);
            
        

        BAT17_ServAppointmentAutomatisme bat17 = new BAT17_ServAppointmentAutomatisme(lstSAIdEmail, lstSAIdCourrier, lstSAIdError);
        Database.executeBatch(bat17, 1);*/

        List<Id> lstSAId = new List<Id>();
        List<ServiceAppointment> lstSAToUpdt = new List<ServiceAppointment>();
        if (lstSAIdEmail != null && lstSAIdEmail.size() > 0)
            lstSAId.addAll(lstSAIdEmail);
        
        if (lstSAIdCourrier != null && lstSAIdCourrier.size() > 0)
            lstSAId.addAll(lstSAIdCourrier);
        
        if (lstSAIdError != null && lstSAIdError.size() > 0)
            lstSAId.addAll(lstSAIdError);

        
        Database.executeBatch(new BAT17_UpdateSAFieldsForGenerate(lstSAId));
        Database.executeBatch(new BAT17_ServAppointmentAutomatisme(lstSAId), 1);
/*
        for (ServiceAppointment SA: [SELECT Id, Avis_de_passage_en_cours_de_g_n_ration__c FROM ServiceAppointment WHERE Id IN: lstSAId ]) {
            SA.Avis_de_passage_en_cours_de_g_n_ration__c = true;
            lstSAToUpdt.add(SA);
        }

        if (lstSAToUpdt.size() > 0)
            update lstSAToUpdt;

        Database.executeBatch(new BAT17_ServAppointmentAutomatisme(), 1);*/
    }

    public class getData {
        @AuraEnabled public ServiceAppointment sa {get;set;}
        @AuraEnabled public Account acc {get;set;}
        public getData(ServiceAppointment sa, Account acc) {
            this.sa = sa;
            this.acc = acc;
        }
    }
}