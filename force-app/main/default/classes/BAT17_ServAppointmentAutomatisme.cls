/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 09-11-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   04-05-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global without sharing class BAT17_ServAppointmentAutomatisme implements Database.Batchable <sObject>, Database.Stateful, Database.AllowsCallouts {
    
    private String query;
    private Integer count = 0;

    private String HtmlBody = '';

    private static EmailTemplate EmlTmp = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'Avis_de_passage'];

    public Set<String> setSaId;
    public List<String> lstSaIdEmail;
    public List<String> lstSaIdCourrier;
    public List<String> lstSaIdError;

    
    private List<Decimal> lstDocId;
    private List<ServiceAppointment> lstSAToUpdate;
    private List<ContentVersion> lstCV;
    private List<ContentDocumentLink> lstCdl;
    private List<WSTransactionLog__c> logs;
    private List<Task> lstTsk;
    private List<OrgWideEmailAddress> lstOrgWide = [SELECT Id, Address FROM OrgWideEmailAddress];
    private List<Contact> lstCon;

    private Map<String, ContentDocumentLink> mapcdlToCvTitle;
    private Map<String, String> mapCvIdToSaId;
    private Map<String, String> mapSendTypeToSaId;
    private Map<String, String> mapEmailConId;
    private Map<String, String> mapOrgIdToEmail;
    private Map<String, String> mapSenderEmailToSaId;
    private Map<String, String> mapSuccessToSaId = new Map<String, String>();
    private Map<String, String> mapErrorToSaId = new Map<String, String>();    
    private Map<String, Account> mapAccToId; 
    private Map<String, String> mapAccIdToSaId = new Map<String, String>();
                            

    global BAT17_ServAppointmentAutomatisme(){
        this.lstSaIdEmail = new List<String>();
        this.lstSaIdCourrier = new List<String>();
        this.lstSaIdError = new List<String>();
    }

    global BAT17_ServAppointmentAutomatisme(List<String> lstSaId) {
        setSaId = new Set<String>(lstSaId);
    }

    global BAT17_ServAppointmentAutomatisme(List<String> lstSaIdEmail, List<String> lstSaIdCourrier, List<String> lstSaIdError) {
        this.lstSaIdEmail = (lstSaIdEmail == null ? new List<String>() : lstSaIdEmail);
        this.lstSaIdCourrier = (lstSaIdCourrier == null ? new List<String>() : lstSaIdCourrier);
        this.lstSaIdError = (lstSaIdError == null ? new List<String>() : lstSaIdError);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        Set<Id> setAccId = new Set<Id>();
        Set<String> setEmail = new Set<String>();

        Set<Id> setEDocId = new Set<Id>();
        lstDocId = new List<Decimal>();
        List<ServiceAppointment> lstSA;
        /*List<Editique_Document_ID__c> lstEDoc = new List<Editique_Document_ID__c>();
        for (integer i = 0; i < size; i++) {
            lstEDoc.add(new Editique_Document_ID__c());
        }

        for (Database.SaveResult sr : Database.insert(lstEDoc)) {
            setEDocId.add(sr.getId());
        }

        for (Editique_Document_ID__c eDoc : [SELECT Document_ID__c FROM Editique_Document_ID__c WHERE Id IN :setEDocId]) {
            lstDocId.add(Decimal.valueOf(eDoc.Document_ID__c));
        }*/

        query = 'SELECT Id, AppointmentNumber, Residence__c, Residence__r.Visit_Notice_Recipient__c, Residence__r.Inhabitant__c, Residence__r.Owner__c, Residence__r.Legal_Guardian__c'
                    + ', Residence__r.Account__c, ServiceTerritoryId, ServiceTerritory.Email__c, TECH_Avis_de_passage_envoye_le__c'
                    + ', Avis_de_passage_en_cours_de_g_n_ration__c, Avis_de_passage_regenerer__c, Visit_Notice_Generated_In_Mass__c'
                            //+ ' FROM ServiceAppointment WHERE Id IN:lstSaIdEmail OR Id IN:lstSaIdCourrier OR Id IN:lstSaIdError';
                            + ' FROM ServiceAppointment';
        
        query += ' WHERE Id IN :setSaId';

        System.Debug('### query: ' + query);

        lstSA = Database.query(query);

        for (ServiceAppointment SA: lstSA) {
            if (SA.Residence__c != null) {
                //MNA 15/07/2021 TEC-747
                switch on SA.Residence__r.Visit_Notice_Recipient__c {
                    when 'Inhabitant' {
                        if(SA.Residence__r.Inhabitant__c != null) {
                            setAccId.add(SA.Residence__r.Inhabitant__c);
                            mapAccIdToSaId.put(SA.Id, SA.Residence__r.Inhabitant__c);
                        }
                    }
                    when 'Owner' {
                        if(SA.Residence__r.Owner__c != null) {
                            setAccId.add(SA.Residence__r.Owner__c);
                            mapAccIdToSaId.put(SA.Id, SA.Residence__r.Owner__c);
                        }
                    }
                    when 'Legal Guardian' {
                        if(SA.Residence__r.Legal_Guardian__c != null) {
                            setAccId.add(SA.Residence__r.Legal_Guardian__c);
                            mapAccIdToSaId.put(SA.Id, SA.Residence__r.Legal_Guardian__c);
                        }
                    }
                    when 'Administrator' {
                        if(SA.Residence__r.Account__c != null) {
                            setAccId.add(SA.Residence__r.Account__c);
                            mapAccIdToSaId.put(SA.Id, SA.Residence__r.Account__c);
                        }
                    }
                }
            }
        }

        
        mapAccToId = new Map<String, Account>();
        for (Account acc : [SELECT Id, RecordTypeId, RecordType.DeveloperName, Name, PersonEmail, BillingStreet, BillingPostalCode, BillingCity,
                                        (SELECT Id, FirstName, LastName, Email FROM Contacts WHERE Contact_principal__c = true)
                                        FROM Account WHERE Id=:setAccId]) {

            mapAccToId.put(acc.Id, acc);
            String Email = getEmail(acc);
            if (Email != null) {
                setEmail.add(Email);
            }
        }
        lstCon = [SELECT Id, Email FROM Contact WHERE EMail IN : setEmail];

        List<Editique_Document_ID__c> lstEDoc = new List<Editique_Document_ID__c>();
        for (integer i = 0; i < lstSA.size() * 2; i++) {
            lstEDoc.add(new Editique_Document_ID__c());
        }

        for (Database.SaveResult sr : Database.insert(lstEDoc)) {
            setEDocId.add(sr.getId());
        }

        for (Editique_Document_ID__c eDoc : [SELECT Document_ID__c FROM Editique_Document_ID__c WHERE Id IN :setEDocId]) {
            lstDocId.add(Decimal.valueOf(eDoc.Document_ID__c));
        }

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ServiceAppointment> lstSA) {
        Boolean isFirst = true;

        lstSAToUpdate = new List<ServiceAppointment>();
        lstCV = new List<ContentVersion>();
        lstCdl = new List<ContentDocumentLink>();
        logs = new List<WSTransactionLog__c>();
        lstTsk = new List<Task>();
        List<Messaging.SingleEmailMessage> lstEmailToSend = new List<Messaging.SingleEmailMessage>();

        mapcdlToCvTitle = new Map<String, ContentDocumentLink>();
        mapCvIdToSaId = new Map<String, String>();
        mapSendTypeToSaId = new Map<String, String>();
        mapEmailConId = new Map<String, String>();
        mapOrgIdToEmail = new Map<String, String>();
        mapSenderEmailToSaId = new Map<String, String>();
        
        if (lstCon != null) {
            for (Contact con : lstCon) {
                mapEmailConId.put(con.Email, con.Id);
            }
        }
        
        for (OrgWideEmailAddress orgWide: lstOrgWide) {
            mapOrgIdToEmail.put(orgWide.Address, orgWide.Id);
        }
/*
        for (String SaId: lstSaIdEmail) {
            mapSendTypeToSaId.put(SaId, 'Email');
        }
        for (String SaId: lstSaIdCourrier) {
            mapSendTypeToSaId.put(SaId, 'Courrier');
        }
        for (String SaId: lstSaIdError) {
            mapSendTypeToSaId.put(SaId, 'Error');
        }*/


        for (ServiceAppointment SA: lstSA) {
            if (SA.ServiceTerritoryId != null && SA.ServiceTerritory.Email__c != null) {
                mapSenderEmailToSaId.put(SA.AppointmentNumber, SA.ServiceTerritory.Email__c);
                System.debug('ok');
            }
            
            String type = getTypeSA(mapAccToId.get(mapAccIdToSaId.get(SA.Id)));
            mapSendTypeToSaId.put(SA.Id, type);
            System.debug(type);
            if (type == 'Error') {
                SA.Avis_de_passage_en_cours_de_g_n_ration__c = false;
                SA.Avis_de_passage_regenerer__c = true;

                if (SA.Residence__c == null) {
                    mapErrorToSaId.put(SA.AppointmentNumber, 'Champ Résidence vide');
                }
                else if (SA.Residence__r.Visit_Notice_Recipient__c == 'Inhabitant' && SA.Residence__r.Inhabitant__c == null) {
                    mapErrorToSaId.put(SA.AppointmentNumber, 'Champ Inhabitant vide');
                }
                else if (SA.Residence__r.Visit_Notice_Recipient__c == 'Owner' && SA.Residence__r.Owner__c == null) {
                    mapErrorToSaId.put(SA.AppointmentNumber, 'Champ Owner vide');
                }
                else if (SA.Residence__r.Visit_Notice_Recipient__c == 'Legal Guardian' && SA.Residence__r.Legal_Guardian__c == null) {
                    mapErrorToSaId.put(SA.AppointmentNumber, 'Champ Legal Guardian vide');
                }
                else if (SA.Residence__r.Visit_Notice_Recipient__c == 'Administrator' && SA.Residence__r.Account__c == null) {
                    mapErrorToSaId.put(SA.AppointmentNumber, 'Champ Account vide');
                }
                else {
                    mapErrorToSaId.put(SA.AppointmentNumber, 'Veuillez compléter l\'adresse email ou l\'adresse postale');
                }

                lstSAToUpdate.add(SA);
            }
            else {
                //Step 1 generate PDF in techeasy
                Datetime now = System.now();
                Map<String , Object> mapResponse = (Map<String , Object>) WS13_ChaineEditiquev2.createJSON(SA.Id, true, true, true, 'AVIS_TCH', lstDocId[count], true);
                count++;    
                

                List<WSTransactionLog__c> lstLog = (List<WSTransactionLog__c>) mapResponse.get('Log');
                logs.addAll(lstLog);
                
                String response = String.valueOf(mapResponse.get('String'));
                Map<String, Object> mapObjResponse = (Map<String, Object>) JSON.deserializeUntyped(response);

                System.debug(mapObjResponse);

                Integer statusCode = Integer.valueOf(mapObjResponse.get('statusCode'));

                System.debug(statusCode);
                
                ServiceAppointment newSA = (ServiceAppointment) mapResponse.get('Object');
                newSA.Avis_de_passage_en_cours_de_g_n_ration__c = false;

                if (statusCode < 200 || statusCode >= 300) {                                                //Erreur
                    newSA.Avis_de_passage_regenerer__c = true;
                    String message = String.valueOf(mapObjResponse.get('message'));
                    if (message == 'Le serveur ne répond pas') 
                        mapErrorToSaId.put(SA.AppointmentNumber, newSA.Response__c);
                    else
                        mapErrorToSaId.put(SA.AppointmentNumber, message);
                }
                else {                                                                                // On incrémente le documentId parce que l'appel a été un succès
                    
                    ContentVersion cv = (ContentVersion) mapResponse.get('ContentVersion');
                    lstCV.add(cv);
                    System.debug(cv);

                    ContentDocumentLink cdl = (ContentDocumentLink) mapResponse.get('ContentDocumentLink');
                    mapcdlToCvTitle.put(cv.Title, cdl);

                    if (type == 'Email') {
                        newSA.TECH_Avis_de_passage_envoye_le__c = now;                                      //Stockage de la date/heure si la génération du PDF a été est un succès
                        mapSendTypeToSaId.put(SA.Id, 'EmailSuccess');

                        System.debug('Email '+ SA.AppointmentNumber);
                    }
                    else {
                        mapResponse = (Map<String , Object>) WS13_ChaineEditiquev2.createJSON(SA.Id, false, true, false, 'AVIS_TCH', lstDocId[count], false);      
                        count++;        

                        lstLog = (List<WSTransactionLog__c>) mapResponse.get('Log');
                        logs.addAll(lstLog);
                        
                        response = String.valueOf(mapResponse.get('String'));
                        mapObjResponse = (Map<String, Object>) JSON.deserializeUntyped(response);

                        statusCode = Integer.valueOf(mapObjResponse.get('statusCode'));
                        
                        newSA = (ServiceAppointment) mapResponse.get('Object');
                        newSA.Avis_de_passage_en_cours_de_g_n_ration__c = false;

                        if (statusCode < 200 && statusCode >= 300) {                            //Erreur
                            newSA.Avis_de_passage_regenerer__c = true;

                            String message = String.valueOf(mapObjResponse.get('message'));
                            if (message == 'Le serveur ne répond pas') 
                                mapErrorToSaId.put(SA.AppointmentNumber, SA.Response__c);
                            else
                                mapErrorToSaId.put(SA.AppointmentNumber, message);
                        } 
                        else {                                                        
                            newSA.TECH_Avis_de_passage_envoye_le__c = now;                      //Stockage de la date/heure si l'acheminement a été est un succès
                            newSA.Visit_Notice_Generated_In_Mass__c = true;
                            newSA.Avis_de_passage_regenerer__c = false;
                    
                            Task tsk = new Task(Subject = 'Envoi de l\'avis de passage par courrier le ' + System.now().format('dd/MM/yyyy à HH:mm'), ActivityDate= System.today(), WhatId = SA.Id, Status = 'Completed');
                            lstTsk.add(tsk);
                            
                            System.debug('Courrier ' + SA.AppointmentNumber);
    
                            mapSuccessToSaId.put(SA.AppointmentNumber, 'Envoyé par courrier avec succès');
                        }
                    }

                }

                lstSAToUpdate.add(newSA);
            }
        }
/*
        System.debug(lstSAToUpdate);

        if (lstSAToUpdate != null && lstSAToUpdate.size() > 0) {
            try {
                update lstSAToUpdate;
            } catch (Exception e) {
                System.debug('Problème lors de la mise à jour du SA: ' + e.getMessage());
            }
        }*/

        System.debug(lstCV);
        if (lstCV != null && lstCV.size() > 0) {
            insert lstCV;
            lstCV = new List<ContentVersion>([SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Title IN : mapcdlToCvTitle.keySet()]);

            for (ContentVersion cv : lstCV) {
                ContentDocumentLink cdl = mapcdlToCvTitle.get(cv.Title);
                cdl.ContentDocumentId = cv.ContentDocumentId;
                lstCdl.add(cdl);
                mapCvIdToSaId.put(cdl.LinkedEntityId, cv.Id);
            }
        }

        System.debug(lstCdl);

        if (lstCdl != null && lstCdl.size() > 0)
            insert lstCdl;

        for (ServiceAppointment SA: lstSA) {
            if (mapSendTypeToSaId.get(SA.Id) == 'EmailSuccess') {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                Account acc;
                switch on SA.Residence__r.Visit_Notice_Recipient__c {
                    when 'Inhabitant' {
                        if(SA.Residence__r.Inhabitant__c != null) 
                        acc = mapAccToId.get(SA.Residence__r.Inhabitant__c);
                    }
                    when 'Owner' {
                        if(SA.Residence__r.Owner__c != null) 
                            acc = mapAccToId.get(SA.Residence__r.Owner__c);
                    }
                    when 'Legal Guardian' {
                        if(SA.Residence__r.Legal_Guardian__c != null) 
                            acc = mapAccToId.get(SA.Residence__r.Legal_Guardian__c);
                    }
                    when 'Administrator' {
                        if(SA.Residence__r.Account__c != null) 
                            acc = mapAccToId.get(SA.Residence__r.Account__c);
                    }
                }

                System.debug(acc);
                if (acc != null) {

                    System.debug(acc.RecordTypeId);
                    if (acc.RecordTypeId != null)
                        System.debug(acc.RecordType.DeveloperName);
                }

                String Email = getEmail(acc);
                
                String orgId = '';
                if (mapSenderEmailToSaId.containsKey(SA.AppointmentNumber)) {
                    String SenderEmail = mapSenderEmailToSaId.get(SA.AppointmentNumber);
                    if (mapOrgIdToEmail.containsKey(SenderEmail))
                        orgId = mapOrgIdToEmail.get(SenderEmail);
                }
    
                if (orgId != '')
                    mail.setOrgWideEmailAddressId(orgId);

                mail.setTemplateID(EmlTmp.Id);
                mail.setUseSignature(false);
                mail.setWhatId(SA.Id);
                mail.setTargetObjectId(mapEmailConId.get(Email));
                mail.setEntityAttachments(new List<String>{mapCvIdToSaId.get(SA.Id)});
                
                lstEmailToSend.add(mail);
            }
        }

        if (lstEmailToSend.size() > 0) {
            try{
                Messaging.SendEmailResult[] results = Messaging.sendEmail(lstEmailToSend);
                System.debug('### mail sent' + results);
                for (Integer i = 0; i < lstSAToUpdate.size(); i++) {
                    if (mapSendTypeToSaId.get(lstSAToUpdate[i].Id)== 'EmailSuccess') {           
                        lstSAToUpdate[i].Visit_Notice_Generated_In_Mass__c = true;
                        lstSAToUpdate[i].Avis_de_passage_regenerer__c  = false; 
                        mapSuccessToSaId.put(lstSAToUpdate[i].AppointmentNumber, 'Envoyé par mail avec succès');
                    }
                }
            }
            catch(Exception e){
                for (Integer i = 0; i < lstSAToUpdate.size(); i++) {
                    if (mapSendTypeToSaId.get(lstSAToUpdate[i].Id)== 'EmailSuccess') {
                        lstSAToUpdate[i].TECH_Avis_de_passage_envoye_le__c = null;
                        lstSAToUpdate[i].Avis_de_passage_regenerer__c = true; 
                        mapErrorToSaId.put(lstSAToUpdate[i].AppointmentNumber, 'L\'envoi d\'email a échoué');
                    }
                }
            }
        }

        System.debug(lstSAToUpdate);

        if (lstSAToUpdate != null && lstSAToUpdate.size() > 0) {
            try {
                update lstSAToUpdate;
            } catch (Exception e) {
                System.debug('Problème lors de la mise à jour du SA: ' + e.getMessage());
            }
        }
            
        if (lstTsk.size() > 0)
            insert lstTsk;
    }

    global void finish(Database.BatchableContext BC) {
        System.debug(mapSuccessToSaId);
        System.debug(mapErrorToSaId);

        if (mapSuccessToSaId.keySet().size() > 0) {
            HtmlBody += '<h3>Succès</h3>';
            for (String strKey: mapSuccessToSaId.keySet()) {
                HtmlBody += '<h4 style="padding-left:20px;"> - '+ strKey + '</h4>';
                HtmlBody += '<p style="padding-left:40px;">' + mapSuccessToSaId.get(strKey) + '</p>';
            }
            HtmlBody += '<hr/>';
        }

        if (mapErrorToSaId.keySet().size() > 0) {
            HtmlBody += '<h3>Erreur</h3>';
            for (String strKey: mapErrorToSaId.keySet()) {
                HtmlBody += '<h4 style="padding-left:20px;"> - '+ strKey + '</h4>';
                HtmlBody += '<p style="padding-left:40px;">' + mapErrorToSaId.get(strKey) + '</p>';
            }
        }

        List<Messaging.SingleEmailMessage> lstEmailToSend = new List<Messaging.SingleEmailMessage>();

        if (HtmlBody != '') {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Status de l\'envoi des avis de passage');

            //Update MNA TEC 865 27/09/2021
            Set<String> setEmails = new Set<String>();
            setEmails.addAll(System.label.BAT17_Email.split(','));
            setEmails.add(UserInfo.getUserEmail());                                            

            List<String> lstEmails = new List<String>();
            lstEmails.addAll(setEmails);

            mail.setHtmlBody(HtmlBody);
            mail.setToAddresses(lstEmails);

            lstEmailToSend.add(mail);
        }
        
        if (lstEmailToSend.size() > 0) {
            try{
                Messaging.SendEmailResult[] results = Messaging.sendEmail(lstEmailToSend);
                System.debug('### mail sent' + results);
            }
            catch(Exception e){
                System.debug('Erreur dans l\'email de rapport' + e.getMessage());
            }
        }/*
        if (i != 0)
            System.enqueueJob(new EnqueueUpdateDocumentId(wsChaineEditique.documentId__c + i));*/

    }

    /*global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT17_ServAppointmentAutomatisme');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(lstSAId), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(lstSAId), 1);
        }
    }*/
    
    public String getEmail(Account acc) {
        if (acc != null && acc.RecordTypeId != null) {
            if (acc.RecordType.DeveloperName == 'PersonAccount') {
                return acc.PersonEmail;
            }
            else if (acc.RecordType.DeveloperName == 'BusinessAccount') {
                if (acc.Contacts == null || acc.Contacts.size() == 0) {
                    return null;
                }
                else {
                    return acc.Contacts[0].Email;
                }
            }
        }
        return null;
    }

    public Boolean hasAddress(Account acc) {
        if (acc != null && acc.BillingStreet != null && acc.BillingPostalCode != null && acc.BillingCity != null)
            return true;
        return false;
    }

    public String getTypeSA(Account acc) {
        if (getEmail(acc) != null)
            return 'Email';
        else if (hasAddress(acc))
            return 'Courrier';
        else
            return 'Error';
    }
}