/**
 * @File Name          : BAT08_UpdateProductItem.cls
 * @Description        :
 * @Author             : SH
 * @Group              :
 * @Last Modified By   : SH
 * @Last Modified On   : 24/01/2020, 13:22:19
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * ---    -----------       -------           ------------------------
 * 1.0    24/01/2020          SH              Initial Version
 * ---    -----------       -------           ------------------------
**/
global class BAT08_UpdateProductItem implements Database.Batchable <sObject>, Database.Stateful, Schedulable {
    global Database.QueryLocator start(Database.BatchableContext BC){
        // String dt = System.Now().addMonths(-12).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String query = 
            'SELECT Id, Classification__c, Classe_de_rotation__c, ' + 
            '   (SELECT Id, TransactionType, Quantity' + 
            '    FROM ProductItemTransactions ' +
            '    WHERE CreatedDate >= LAST_N_MONTHS:12 AND TransactionType IN (\'Transferred\', \'Consumed\')) ' + 
            'FROM ProductItem ' +
            'WHERE Location.LocationType = \'Entrepôt\' AND Classification__c = \'A stocker\'';

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ProductItem> lstPIs) {
        for (ProductItem pi : lstPIs) {
            Boolean foundAtLeastOnePITpositive = false, foundAtLeastOnePIT = false;

            for (ProductItemTransaction pit : pi.ProductItemTransactions) {
                if (pit.Quantity > 0) { foundAtLeastOnePITpositive = true; }
                foundAtLeastOnePIT = true;
            }

            if (!foundAtLeastOnePITpositive) {
                pi.Classification__c = 'Stock mort';
                pi.Classe_de_rotation__c = 'M';
            }
            if (!foundAtLeastOnePIT) {
                pi.Classification__c = 'Inactif';
                pi.Classe_de_rotation__c = 'I';
            }
        }
        update lstPIs;
    }

    global void finish(Database.BatchableContext BC) {
    }

    global static String scheduleBatch() {
        BAT08_UpdateProductItem sc = new BAT08_UpdateProductItem();
        return System.schedule('Batch BAT08_UpdateProductItem:' + Datetime.now().format(),  '0 0 0 * * ?', sc);
    }

    global void execute(SchedulableContext sc){
        Database.executeBatch(new BAT08_UpdateProductItem());
    }
}