@RestResource(urlMapping='/essai/cts/*')

/**
 * Classe en cours de développement
 * Objectif : renvoyer les données permettant de maintenir l'indicateur extract_ratio_cts_cumul
 * par webservice rest
 *
 * @author Emmanuel Bruno <e.bruno@askelia.com>
 * @since 2020-02-18
 * @version 0.2
 */

global with sharing class EssaiRestIndicateurs {
    /**
     *   Code pour extraire le cumul des visites hors contrat
    **/
    @HttpPost
    global static String cts(RatioCtsCumuleRequest params)
    {
        //Lecture des paramètres de date
        Integer year = params.year;
        Integer month = params.month;
        INDIC_01 ind = new INDIC_01();
        Map<String, Map<String, Object>> res = ind.extractData(2019, 5); //(depuis le premier octobre 2019, 5 mois de cumul)
        System.debug(res);
        System.debug(JSON.serializePretty(res));
        return JSON.serialize(res);
    }

    global class RatioCtsCumuleRequest {
        global Integer year;
        global Integer month; //le nième mois de l'exercice : 1 pour octobre, 2 pour novembre, etc
        public RatioCtsCumuleRequest(Integer y, Integer m){
            year = y; month = m;
        }
    }
}