public class DeleteDraftInvoice {
@InvocableMethod
    public static void InvoiceDelete(List<ID> recordIds) {
        List <sofactoapp__Factures_Client__c> Factures  =[select id from sofactoapp__Factures_Client__c 
                          where (sofactoapp__Opportunit__c  in :recordIds or Sofactoapp_Contrat_de_service__c in :recordIds )
                       and sofactoapp__Etat__c  = 'Brouillon'
                       and id NOT IN (SELECT sofactoapp__Facture__c FROM sofactoapp__Ecriture_comptable__c)];
        
        
        
        try{
            System.debug('### début phase delete ');
            delete Factures;
            }catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
            }
        
   }
}