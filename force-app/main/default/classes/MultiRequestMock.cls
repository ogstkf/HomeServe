public with sharing class MultiRequestMock implements HttpCalloutMock {
/*----------------------------------------------------------------------
-- - Author        : Spoon Consulting
-- - Description   : Provide an implementation for the HttpCalloutMock interface 
--                   to fake a response sent in the respond method, which the Apex 
--                   runtime calls to send a response for a callout.
--                   Class implementing HttpCalloutMock to allow for multiple request.
-- -             
-- - Maintenance History:
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  ---------------------------------------
-- 24-JAN-2019  MGR   1.0      Initial version
----------------------------------------------------------------------
*/
	Map<String, HttpCalloutMock> requests;

    public MultiRequestMock(Map<String, HttpCalloutMock> requests) {
        this.requests = requests;
    }

    public HTTPResponse respond(HTTPRequest req) {
        HttpCalloutMock mock = requests.get(req.getEndpoint());
        if (mock != null) {
            return mock.respond(req);
        } else {
            return null;
        }
    }

    //public void addRequestMock(String url, HttpCalloutMock mock) {
    //    requests.put(url, mock);
    //}

}