/**
 * @File Name          : ShipmentTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 26/11/2019, 14:34:21
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    04/11/2019   AMO     Initial Version
 * 1.1    19/06/2020   DMU     Commented AP35_ShipmentRating, AP19_CreateShipmentOrPT, AP22_HandleProductDelivery due to homeServe Project
**/
public with sharing class ShipmentTriggerHandler{
    
    Bypass__c userBypass;
    public Static Set<Id> setAccountIds = new Set<Id>();

    public ShipmentTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void handleBeforeUpdate(List<Shipment> lstOld, List<Shipment> lstNew){

        list<Shipment> lstShip = new list<Shipment>();
        List<Shipment> lstShipmentRating = new List<Shipment>();
        

        for(integer i=0;i<lstNew.size();i++){
            if(lstNew[i].RecordTypeId==Schema.SObjectType.Shipment.getRecordTypeInfosByName().get(AP_Constant.RTReceptionFournisseur).getRecordTypeId()
            && (
                lstNew[i].Status == AP_Constant.ShipStatusLivre &&
                lstOld[i].Status != AP_Constant.ShipStatusLivre
               )
            
            ){
               lstShip.add(lstNew[i]);
            }

            //SBH: CT-1171 AP35
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP35')){
                System.debug('Record type: ' + Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get(AP_Constant.orderRtCommandeFournisseur));
                if(
                    lstNew[i].TECH_OrderRtId__c == Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get(AP_Constant.orderRtCommandeFournisseur).getRecordTypeId()
                    
                    &&
                        (lstNew[i].Status == AP_Constant.ShipStatusLivre &&
                        lstOld[i].Status != AP_Constant.ShipStatusLivre)
                    ){
                    // lstShipmentRating.add(lstNew[i]);
                    setAccountIds.add(lstNew[i].TECH_Account__c);
                }
                System.debug('Shipment\'s Account: ' + setAccountIds);
            }
            //END CT 1171 AP35

        }
        
        if(lstShip.size()>0){
            AP19_CreateShipmentOrPT.checkQuantityReceived(lstShip);
           // AP22_HandleProductDelivery.checkProductTransfer(lstShip);
        }

        //DMU 20200619 - Commented AP35_ShipmentRating due to homeserve project
        // if(!lstShipmentRating.isEmpty()){
        //     AP35_ShipmentRating.updateExpectedDeliveryDate(lstShipmentRating);
        // }

    }

    public void handleAfterUpdate(List<Shipment> lstOld, List<Shipment> lstNew){
        
        List<Shipment> lstShip = new List<Shipment>();
        List<Shipment> lstShipment = new List<Shipment>();
        
        for(integer i=0;i<lstNew.size();i++){
            if(lstNew[i].RecordTypeId==Schema.SObjectType.Shipment.getRecordTypeInfosByName().get(AP_Constant.RTReceptionFournisseur).getRecordTypeId()
            && (
                lstNew[i].Status == AP_Constant.ShipStatusLivre &&
                lstOld[i].Status != AP_Constant.ShipStatusLivre
            )
            
            ){
                //AP22
                lstShip.add(lstNew[i]);
                //AP35
                // lstShipment.add(lstNew[i]);
                // System.debug('List Shipment: ' + lstShipment);
            }
        }
        
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP22')){
            AP22_HandleProductDelivery.checkProductTransfer(lstShip);
        }
        //DMU 20200619 - Commented AP35_ShipmentRating due to homeserve project
        // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP35')){
        //     System.debug('List of AP35 After Update : ' + lstShipment); 
        //     AP35_ShipmentRating.handleafterupdate(lstShipment);
        // }



    }


}