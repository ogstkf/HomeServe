/**
 * @File Name          : ChamADUserRegistrationHandler.cls
 * @Description        :
 * @Author             : sc
 * @Group              :
 * @Last Modified By   : sc
 * @Last Modified On   : 3/30/2020, 5:09:41 PM
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    01/10/2019        sc                      Initial Version
 * 1.1    24/06/2020        DMU                     Commented class due to homeserve project
/ **/
public class ContractTriggerHandler {
/*
    List<ServiceContract> set_subForIndex = new List<ServiceContract>();
    Set<Id> set_subWithNewIndex = new Set<Id>();
    Set<Id> set_subWithNoMoreIndex = new Set<Id>();

    public void  handlebeforeinsert (List<ServiceContract> newcontrats)
    {
        for (ServiceContract lv_sub : newcontrats) 
        {
            if (lv_sub.Date_pour_indice_de_reference_1__c    == null) 
            {
                 lv_sub.Date_pour_indice_de_reference_1__c = lv_sub.Date_de_1ere_facture__c;
            }
            if (lv_sub.Indice_1__c != null && 
                        lv_sub.Date_pour_indice_de_reference_1__c != null) 
            {
                    set_subForIndex.add(lv_sub);
            } 
            // Gestion cohérence indice & coefficient
            else if (lv_sub.Valeur_d_indice_reference1__c != null && 
                        ServiceContractUtils.indexes.get(lv_sub.Valeur_d_indice_reference1__c) != null && 
                        lv_sub.Coefficient_indice_de_reference__c != ServiceContractUtils.indexes.get(lv_sub.Valeur_d_indice_reference1__c).sofactoapp__Coefficient__c) 
            {
                    lv_sub.Coefficient_indice_de_reference__c = ServiceContractUtils.indexes.get(lv_sub.Valeur_d_indice_reference1__c).sofactoapp__Coefficient__c;
            } 
         }

        if (set_subForIndex.isEmpty() == false) 
        {
            ServiceContractUtils.findIndex(set_subForIndex);
        }
    }

    public void handlebeforeUpdate (List<ServiceContract> newcontrats, List<ServiceContract> oldcontrats)
    {

        system.debug ('handle before update');
         for (integer i =0; i<newcontrats.size() ; i++)
         {
             // Lorsque l'on sélectionne un indice, 
                // il faut déclencher la recherche de valeur d'indice
                if (newcontrats[i].Indice_1__c != oldcontrats[i].Indice_1__c && 
                        newcontrats[i].Indice_1__c != null) 
                {
                    if (newcontrats[i].Date_pour_indice_de_reference_1__c == null) {
                        newcontrats[i].Date_pour_indice_de_reference_1__c = newcontrats[i].Date_de_1ere_facture__c;
                    }
                    set_subForIndex.add(newcontrats[i]);
                } 
                // Gestion modification de la date de référence pour indice
                else if (newcontrats[i].Date_pour_indice_de_reference_1__c != oldcontrats[i].Date_pour_indice_de_reference_1__c && 
                        newcontrats[i].Indice_1__c != null && 
                        newcontrats[i].Date_pour_indice_de_reference_1__c != null) 
                {
                    set_subForIndex.add(newcontrats[i]);                    
                }   
                // Gestion cohérence indice & coefficient
                else if (newcontrats[i].Valeur_d_indice_reference1__c != null && 
                        ServiceContractUtils.indexes.get(newcontrats[i].Valeur_d_indice_reference1__c) != null && 
                        newcontrats[i].Coefficient_indice_de_reference__c != ServiceContractUtils.indexes.get(newcontrats[i].Valeur_d_indice_reference1__c).sofactoapp__Coefficient__c) 
                {
                    newcontrats[i].Coefficient_indice_de_reference__c = ServiceContractUtils.indexes.get(newcontrats[i].Valeur_d_indice_reference1__c).sofactoapp__Coefficient__c;
                }

         }
        if (set_subForIndex.isEmpty() == false) 
        {
            ServiceContractUtils.findIndex(set_subForIndex);
        }
          
    }

    public void handleafterupdtae (List<ServiceContract> newcontrats, List<ServiceContract> oldcontrats)
    {
         system.debug ('handle after update');
         for (integer i =0; i<newcontrats.size() ; i++)
         {
             if (newcontrats[i].Indice_1__c != oldcontrats[i].Indice_1__c)
             {
                 if (newcontrats[i].Indice_1__c !=null)
                 {
                     set_subWithNewIndex.add(newcontrats[i].id);
                 }
                 else 
                {
                    set_subWithNoMoreIndex.add(newcontrats[i].id);
                }

             }
         }
     // Si le contrat a changé d'indice, il faut alors répercuter ce changement sur les factures
    
        if (set_subWithNewIndex.isEmpty() == false) {
        
            // Traitement des factures
            Map<Id, sofactoapp__Factures_Client__c> map_invoices = new Map<Id, sofactoapp__Factures_Client__c>(
            [SELECT Id 
            FROM sofactoapp__Factures_Client__c 
            WHERE Sofactoapp_Contrat_de_service__c IN :set_subWithNewIndex
            //AND IsDraft__c = true
            AND sofactoapp__Date_indice__c != null]
            );
        
            if (map_invoices.isEmpty() == false) {
                system.debug ('mis a jour index facture ' + map_invoices.keySet());
            SSA_FacturesUtils.findIndex(map_invoices.keySet());
            }
        }         
    }

*/

}