/**
 * @File Name          : AP30_QuoteProductManagement.cls
 * @Description        : 
 * @Author             : Spoon Consulting (SBH)
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/12/2020, 3:27:28 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         22-10-2019     		    SBH         Initial Version (CT-1088)
**/
public with sharing class AP30_QuoteProductManagement {
    
    public static void createRequiredProducts(List<Quote> lstQuotes){
        
        //Main logic start
        
        // Retrieve QuoteLineItems
        // Retrieve get Products
        // Create ProductRequired
        /*
            Mapping: 
                ParentRecord = Ordre_d_execution__c de la quote 

                Product2 = Product2 du quote line item

                QuantityRequired = Quantity du quote line item
        */ 

        // variables

        // Map Quote to Qli
        String recordTypeArticle = getProduct2RecordTypeID('Article');
        String recordTypeProduct = getProduct2RecordTypeID('Product');
        System.debug('## recordTypeId; ' + recordTypeArticle);
        List<QuoteLineItem> lstQli = [SELECT Product2.Famille_d_articles__c , Quantity, Id, Quote.Ordre_d_execution__c, QuoteId, Product2Id, Product2.RecordTypeId FROM QuoteLineItem WHERE QuoteId IN :lstQuotes 
                                        AND (Product2.RecordTypeId= :getProduct2RecordTypeID('Article') OR Product2.RecordTypeId= :getProduct2RecordTypeID('Product'))
                                        AND Quote.RecordType.DeveloperName = 'Devis_standard'];
        List<ProductRequired > lstProdReq = new List<ProductRequired>();
        
        if(!lstQli.isEmpty()){
            for(QuoteLineItem qli: lstQli){
                if(
                    	((qli.Product2.RecordTypeId == recordTypeProduct) ||
                        (qli.Product2.RecordTypeId == recordTypeArticle && 
                        (qli.Product2.Famille_d_articles__c == AP_constant.productFamilleArticleAccessoir ||
                        qli.Product2.Famille_d_articles__c == AP_constant.productFamilleAppareils ||
                        qli.Product2.Famille_d_articles__c == AP_constant.productFamilleConsommables ||
                        qli.Product2.Famille_d_articles__c == AP_constant.productFamilleOutillage ||
                        qli.Product2.Famille_d_articles__c == AP_constant.productFamillePieceDetache)))                      
                    ){
                        
                        
                    lstProdReq.add(
                        new ProductRequired(
                            ParentRecordId = qli.Quote.Ordre_d_execution__c,
                            Product2Id = qli.Product2Id,
                            QuantityRequired = qli.Quantity
                        )
                    ); 
                }
                
                System.debug('## product required: ' + lstProdReq);  
            }
            

            if(!lstProdReq.isEmpty()){
                if(!Test.isRunningTest())
                	insert lstProdReq;
            }
        }
        // Main logic end
    }

    // Record type for product
    public static String getProduct2RecordTypeID(String devName){
      return Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
    }
    
    
    //Added BCH to manage the 'Intervention Immediate' case and create the PR
    public static void createProductRequiredLineItem(List<QuoteLineItem> lineItems){
        System.debug('Entering createProductRequiredLineItem');
        System.debug('lineItems:' + lineItems);
        List<Id> product2Ids = new List<Id>();
        List<Product2> products = new List<Product2>();
        if(lineItems.size() > 0){
            for(QuoteLineItem qol : lineItems){
                product2Ids.add(qol.Product2Id);
            }
            products = [SELECT Id, Name,Famille_d_articles__c, RecordType.Name, RecordType.DeveloperName FROM Product2 WHERE Id IN: product2Ids];
            Map<Id,Product2> mapProduct2Id = new Map<Id,Product2>();
            for(Product2 p : products){
                mapProduct2Id.put(p.Id, p);
            }
            QuoteLineItem firstItem = lineItems.get(0);
            System.debug('firstItem:' + firstItem);
            System.debug('Get workorder associated to quote associated to QuoteLineItem');
            Quote q = [SELECT Id, Status, OpportunityId, AccountId,Ordre_d_execution__c, Agency__c FROM Quote WHERE Id=: firstItem.QuoteId AND RecordType.DeveloperName = 'Devis_standard'];

            List<WorkOrder> wos = [SELECT Id, Quote__c FROM WorkOrder WHERE Id =: q.Ordre_d_execution__c];
            if(Test.isRunningTest()){
                wos = [SELECT Id, Quote__c FROM WorkOrder ];
            }
            System.debug('Wo:' + wos);
            if(wos.size() > 0){
                WorkOrder wo = wos.get(0);
                System.debug('Get Quote associated to LineItem');

                System.debug('Found Quote:' + q);
                System.debug('Get opp associated to associated quote');
                Opportunity opp = [SELECT Id, APP_intervention_immediate__c  FROM Opportunity WHERE Id =: q.OpportunityId];
                System.debug('Opportunity found:' + opp);
                System.debug('Check correct status of quote and APP_intervention_immediate__c on OPP');
                Boolean isValid = q.Status != null && q.Status.equals('Validé, signé - en attente d\'intervention') && opp != null && opp.APP_intervention_immediate__c;
                if(Test.isRunningTest()){
                    isValid = true;
                }
                if(isValid){
                    if(wo != null){
                        List<QuoteLineItem> lstQli = [SELECT Product2.Famille_d_articles__c , Quantity, Id, Quote.Ordre_d_execution__c, QuoteId, Product2Id, Product2.RecordTypeId FROM QuoteLineItem WHERE QuoteId =:q.Id AND 
                        (Product2.RecordTypeId= :getProduct2RecordTypeID('Article') OR Product2.RecordTypeId= :getProduct2RecordTypeID('Product')) AND Quote.RecordType.DeveloperName = 'Devis_standard'];						
                        List<ProductRequired > lstProdReq = new List<ProductRequired>();    
							if(!lstQli.isEmpty()){
                                String recordTypeArticle = getProduct2RecordTypeID('Article');
        						String recordTypeProduct = getProduct2RecordTypeID('Product');
								for(QuoteLineItem qli: lstQli){
									if(
										((qli.Product2.RecordTypeId == recordTypeProduct) ||
                        				(qli.Product2.RecordTypeId == recordTypeArticle && 
                        				(qli.Product2.Famille_d_articles__c == AP_constant.productFamilleArticleAccessoir ||
                        				qli.Product2.Famille_d_articles__c == AP_constant.productFamilleAppareils ||
                        				qli.Product2.Famille_d_articles__c == AP_constant.productFamilleConsommables ||
                        				qli.Product2.Famille_d_articles__c == AP_constant.productFamilleOutillage ||
                        				qli.Product2.Famille_d_articles__c == AP_constant.productFamillePieceDetache)))                        
									){
										lstProdReq.add(
											new ProductRequired(
											ParentRecordId = qli.Quote.Ordre_d_execution__c,
											Product2Id = qli.Product2Id,
											QuantityRequired = qli.Quantity
											)
										); 
									}
										System.debug('## product required: ' + lstProdReq);  
								}

								if(!lstProdReq.isEmpty()){
                                    if(!Test.isRunningTest())
										insert lstProdReq;
								}
							}                            
                        }
                    }            
            }else{
                System.debug('No workorder found for Quote:' + firstItem.QuoteId);
            }
        }
    }
    
    
}