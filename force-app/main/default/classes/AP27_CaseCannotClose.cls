/**
 * @File Name          : AP27_CaseCannotClose.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 22/10/2019, 15:00:10
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    18/10/2019   AMO     Initial Version
**/
public with sharing class AP27_CaseCannotClose {
    public static void CaseCannotClose(List<Case> lstCaseNew){
        System.debug('####### lstCaseNew'+lstCaseNew);
        System.debug('Enter AP27');

        Map<String, List<WorkOrder>> mapCaseWO = new Map<String, List<WorkOrder>>();
        Map<String, List<ServiceAppointment>> mapCaseSA = new Map<String, List<ServiceAppointment>>();

        //Query all workorders
        List<WorkOrder> lstWO = [SELECT Id, CaseId FROM WorkOrder WHERE CaseId In :lstCaseNew AND 
                                    (
                                        WorkOrder.Status IN(:AP_Constant.wrkOrderStatusNouveau, :AP_Constant.wrkOrderStatusInProgress)
                                    )
                                 ];
        System.debug('Ally WO : ' + lstWO.size());

        //Query all service appointments
        List<ServiceAppointment> lstSA = [SELECT Id, ServiceAppointment.Work_Order__r.CaseId FROM ServiceAppointment WHERE Work_Order__r.CaseId In :lstCaseNew AND 
                                            (ServiceAppointment.Status IN (:AP_Constant.servAppStatusInProgress, :AP_Constant.servAppStatusEnAttenteClient, :AP_Constant.servAppStatusDispatched, :AP_Constant.servAppStatusOnHold, :AP_Constant.servAppStatusNone, :AP_Constant.servAppStatusScheduled))];
        System.debug('Ally SA : ' + lstSA.size());

        //Check workorder for the specific account and add to map
        for(WorkOrder wo : lstWO){
            if(mapCaseWO.containsKey(wo.CaseId)){
                mapCaseWO.get(wo.CaseId).add(wo);
            } else{
                mapCaseWO.put(wo.CaseId, new List<WorkOrder> {wo});
            }
        }

        //Check serviceappointment for the specific account and add to map
        for(ServiceAppointment sa : lstSA){
            if(mapCaseSA.containsKey(sa.Work_Order__r.CaseId) && mapCaseSA.get(sa.Work_Order__r.CaseId) != NULL){
                mapCaseSA.get(sa.Work_Order__r.CaseId).add(sa);
            } else{
                mapCaseSA.put(sa.Work_Order__r.CaseId, new List<ServiceAppointment> {sa});
            }
            
        }

        System.debug('##### mapCaseWO: '+mapCaseWO);
        System.debug('##### mapCaseSA: '+mapCaseSA);

        //check if one of the map contains id of the case and if so, display error msg.
        for(Case cs : lstCaseNew){
            if(mapCaseWO.containsKey(cs.Id) || mapCaseSA.containsKey(cs.Id)){
                cs.addError('Vous ne pouvez pas clôturer cette requête tant qu’il y a un ordre d’exécution et un rendez-vous de service ouvert');
                
            }
        }
    }
}