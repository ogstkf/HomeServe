/**
 * @File Name          : AP15_SetFieldAfterLeadConversion.cls
 * @Description        : Class to set field on SA created after Lead conversion
 * @Author             : Spoon Consulting
 * @Group              : 
 * @Last Modified By   : DMU
 * @Last Modified On   : 17/09/2019, 15:00:00
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-Sep-2019              DMU         Initial Version
**/
public with sharing class AP15_SetFieldAfterLeadConversion {
    
    public static void setField(List<ServiceAppointment> lstSerAppSetFieldLeadConv){
        system.debug('*** in AP15 setField');
        map<string,string> mapSAWO = new map <string,string>();
        map<string,WorkOrder> mapWODetails = new map <string,WorkOrder>();
        // list<ServiceAppointment> lstSAtoUpd = new list <ServiceAppointment>();
        for(ServiceAppointment sa : lstSerAppSetFieldLeadConv){
            mapSAWO.put(sa.Id, sa.Work_Order__c);
        }

        for(WorkOrder wo : [select case.Opportunity__c, caseId, Case.Reason__c from WorkOrder where id in:mapSAWO.values() ]){
            mapWODetails.put(wo.Id,wo);
        }

        for(ServiceAppointment sa : lstSerAppSetFieldLeadConv){
            system.debug('*** in loop sa');
            
            if(mapWODetails.Containskey(mapSAWO.get(sa.Id)) && string.IsNotBlank(mapWODetails.get(mapSAWO.get(sa.Id)).caseId) && mapWODetails.get(mapSAWO.get(sa.Id)).case.Reason__c == 'First maintenance'){
                system.debug('*** in loop sa 2: '+mapWODetails.get(mapSAWO.get(sa.Id)).case.Opportunity__c);

                sa.TECH_Cham_Digital__c = true;
                sa.TECH_SA_payed__c = true;
                sa.Opportunity__c = mapWODetails.get(mapSAWO.get(sa.Id)).case.Opportunity__c;
                sa.Status = 'Scheduled';
                // lstSAtoUpd.add(sa);
            }
		}
        // update lstSAtoUpd;
    }
}