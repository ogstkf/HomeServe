/**
 * @File Name          : BAT04_ActivateServiceContract_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    13/04/2020   ZJO     Initial Version
**/
@IsTest
private class BAT04_ActivateServiceContract_TEST {

    static user adminUser;
    static List<Account> lstAccount;
    static List<Contact> lstContact;
    static List<sofactoapp__Raison_Sociale__c> lstSofacto;
    static List<ServiceTerritory> lstAgence;
    static List<Logement__c> lstLogement;
    static List<Asset> lstAsset = new List<Asset>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<ServiceContract> lstServCon;

    static{
        adminUser = TestFactory.createAdminUser('BAT04@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){
            //List of Accounts 
            lstAccount = new List<Account>{
                TestFactory.createAccount('Test Acc 1'),
                TestFactory.createAccount('Test Acc 2'),
                TestFactory.createAccount('Test Acc 3')
            };
             
            insert lstAccount;
             
            //Update Accounts
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAccount.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstAccount.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstAccount.get(2).Id);
			lstAccount.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstAccount.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstAccount.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            
            update lstAccount;

            //List of Contacts
            lstContact = new List<Contact>{
                new Contact(LastName = 'Test Con 1'),
                new Contact(LastName = 'Test Con 2'),
                new Contact(LastName = 'Test Con 3')
            };

            insert lstContact;

            //List of Sofactos
            lstSofacto = new List<sofactoapp__Raison_Sociale__c>{
                new sofactoapp__Raison_Sociale__c(
                  Name = 'Test sofacto 1',
                  sofactoapp__Credit_prefix__c = '1234',
                  sofactoapp__Invoice_prefix__c = '2134'
                ),
                new sofactoapp__Raison_Sociale__c(
                  Name = 'Test sofacto 2',
                  sofactoapp__Credit_prefix__c = '1342',
                  sofactoapp__Invoice_prefix__c = '2431'
                ),
                new sofactoapp__Raison_Sociale__c(
                  Name = 'Test sofacto 3',
                  sofactoapp__Credit_prefix__c = '1432',
                  sofactoapp__Invoice_prefix__c = '2341'
                )
             };
             insert lstSofacto;

             //Create Operating Hour
             OperatingHours OprtHour = TestFactory.createOperatingHour('Oprt Hrs Test');
             insert OprtHour;
 
             //List of Agences
             lstAgence = new List<ServiceTerritory>{
                TestFactory.createServiceTerritory('Test agence 1', OprtHour.Id, lstSofacto[0].Id),
                TestFactory.createServiceTerritory('Test agence 2', OprtHour.Id, lstSofacto[1].Id),
                TestFactory.createServiceTerritory('Test agence 3', OprtHour.Id, lstSofacto[2].Id)
             };
             insert lstAgence;

             //List of Logements
             lstLogement = new List<Logement__c>{
                  TestFactory.createLogement(lstAccount[0].Id, lstAgence[0].Id),
                  TestFactory.createLogement(lstAccount[1].Id, lstAgence[1].Id),
                  TestFactory.createLogement(lstAccount[2].Id, lstAgence[2].Id)
             };
             insert lstLogement;

             //List of Assets
             Asset asset1 = TestFactory.createAsset('Equipment 1', AP_Constant.assetStatusActif, lstLogement[0].Id);
             asset1.AccountId = lstAccount[0].Id;
             Asset asset2 = TestFactory.createAsset('Equipment 2', AP_Constant.assetStatusActif, lstLogement[1].Id);
             asset2.AccountId = lstAccount[1].Id;
             Asset asset3 = TestFactory.createAsset('Equipment 3', AP_Constant.assetStatusActif, lstLogement[2].Id);
             asset3.AccountId = lstAccount[2].Id;

             lstAsset.add(asset1);
             lstAsset.add(asset2);
             lstAsset.add(asset3);

             insert lstAsset;

            //Create Product
            Product2 prod = TestFactory.createProduct('testProd');
            insert prod;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //List of Service Contracts
            lstServCon = new List<ServiceContract>{
                new ServiceContract(
                  Name = 'serv con 1',
                  AccountId = lstAccount[0].Id,
                  ContactId = lstContact[0].Id,
                  Logement__c = lstLogement[0].Id,
                  Agency__c = lstAgence[0].Id,
                  Asset__c = lstAsset[0].Id,
                  Pricebook2Id = lstPrcBk[0].Id,
                  Contrat_resilie__c = false,
                  Type__c = 'Individual',
                  Contract_Renewed__c = true,
                  Contract_Status__c = 'Pending Payment',
                  StartDate = System.today(),
                  EndDate = Date.Today().addYears(1),
                  Frequence_de_maintenance__c = 3,
                  Type_de_frequence__c = 'Days',
                  Debut_de_la_periode_de_maintenance__c = 1,
                  Fin_de_periode_de_maintenance__c = 6,
                  Periode_de_generation__c = 5,
                  Type_de_periode_de_generation__c = 'Days',
                  Date_du_prochain_OE__c = Date.Today().addDays(7),
                  Generer_automatiquement_des_OE__c = true,
                  Horizon_de_generation__c = 4
                ),
                new ServiceContract(
                  Name = 'serv con 2',
                  AccountId = lstAccount[1].Id,
                  ContactId = lstContact[1].Id,
                  Logement__c = lstLogement[1].Id,
                  Agency__c = lstAgence[1].Id,
                  Asset__c = lstAsset[1].Id,
                  Pricebook2Id = lstPrcBk[0].Id,
                  Contrat_resilie__c = false,
                  Type__c = 'Individual',
                  Contract_Renewed__c = true,
                  Contract_Status__c = 'Pending Payment',
                  StartDate = System.today(),
                  EndDate = Date.Today().addYears(1),
                  Frequence_de_maintenance__c = 3,
                  Type_de_frequence__c = 'Days',
                  Debut_de_la_periode_de_maintenance__c = 1,
                  Fin_de_periode_de_maintenance__c = 6,
                  Periode_de_generation__c = 5,
                  Type_de_periode_de_generation__c = 'Days',
                  Date_du_prochain_OE__c = Date.Today().addDays(7),
                  Generer_automatiquement_des_OE__c = true,
                  Horizon_de_generation__c = 4
                ),
                new ServiceContract(
                  Name = 'serv con 3',
                  AccountId = lstAccount[2].Id,
                  ContactId = lstContact[2].Id,
                  Logement__c = lstLogement[2].Id,
                  Agency__c = lstAgence[2].Id,
                  Asset__c = lstAsset[2].Id,
                  Pricebook2Id = lstPrcBk[0].Id,
                  Contrat_resilie__c = false,
                  Type__c = 'Individual',
                  Contract_Renewed__c = true,
                  Contract_Status__c = 'En attente de renouvellement',
                  StartDate = System.today(),
                  EndDate = Date.Today().addYears(1),
                  Frequence_de_maintenance__c = 3,
                  Type_de_frequence__c = 'Days',
                  Debut_de_la_periode_de_maintenance__c = 1,
                  Fin_de_periode_de_maintenance__c = 6,
                  Periode_de_generation__c = 5,
                  Type_de_periode_de_generation__c = 'Days',
                  Date_du_prochain_OE__c = Date.Today().addDays(7),
                  Generer_automatiquement_des_OE__c = true,
                  Horizon_de_generation__c = 4
                )
            };

            insert lstServCon;
        }
    }

    @IsTest
    public static void testBatch(){
        System.runAs(adminUser){
            Test.startTest();
            BAT04_ActivateServiceContract batch = new BAT04_ActivateServiceContract();
            Database.executeBatch(batch);
            Test.stopTest();
        }
    }
    
    @IsTest
    public static void testScheduleBatch(){
        System.runAs(adminUser){
            Test.startTest();
            BAT04_ActivateServiceContract.scheduleBatch();
            Test.stopTest();
        }
    }
  
}