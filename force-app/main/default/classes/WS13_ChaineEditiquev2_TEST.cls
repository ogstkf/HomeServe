/**
 * @File Name         : WS13_ChaineEditiquev2_TEST
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 02-08-2021
 * Modifications Log 
 * ========================================================================
 * Ver   Date         Author        Modification
 * 1.0   23-04-2021   MNA           Initial Version
**/


@isTest
public with sharing class WS13_ChaineEditiquev2_TEST {
    
    static User mainUser;
    static Bypass__c bp = new Bypass__c();
    static Account testAcc;
    static Account testBusinessAccount;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstCli = new List<ContractLineItem>();
    static List<Quote> lstQuote = new List<Quote>();
    static List<QuoteLineItem> lstQuoteLineItem = new List<QuoteLineItem>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PriceBookEntry> lstPrcBkEnt = new List<PriceBookEntry>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static OperatingHours opHrs = new OperatingHours();
    static List<ServiceTerritory> lstSrvTerr = new List<ServiceTerritory>();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static List<sofactoapp__Factures_Client__c> lstFacturesClients = new List<sofactoapp__Factures_Client__c>();
    static List<sofactoapp__Ligne_de_Facture__c> lstFacLne = new List<sofactoapp__Ligne_de_Facture__c>();
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Case> lstCase = new List<Case>();
    static List<Asset> lstTestAssetChild = new List<Asset>();
    static List<Contact> lstContacts;
    static List<ContentVersion> lstConVer = new List<ContentVersion>();
    static list<ContentDocument> lstConDoc = new List<ContentDocument>();
    static List<ContentDocumentLink> lstConDocLink;
    
    static{

        mainUser = TestFactory.createAdminUser('WS13@test.com', TestFactory.getProfileAdminId());
        insert mainUser;
        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        bp.BypassTrigger__c = 'AP07,AP10,AP12,AP27,AP14,AP16,AP76,AP53,MobileNotificationTrigger,ServiceAppointmentTrigger,LigneDeFactureTrigger,WorkOrderTrigger';
        insert bp;

        System.runAs(mainUser){

              
            //creating account
            testAcc = TestFactory.createAccount('WS13_ChaineEditiquev2_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            testBusinessAccount = TestFactory.createAccountBusiness('WS13_ChaineEditiquev2_TEST2');
            insert testBusinessAccount;

            //Create Contacts
            lstContacts = new List<Contact>{
                new Contact(LastName = 'Test Con 1',AccountId = testBusinessAccount.Id,Email = 'adarsh@spoon.com'),
                new Contact(LastName = 'Test Con 2',AccountId = testBusinessAccount.Id,Email = 'testAnr@gmail.com')
            };
            insert lstContacts;

            //Create PriceBook
            Id pricebookId = Test.getStandardPricebookId();  
            

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            //create service territory
            lstSrvTerr.add(TestFactory.createServiceTerritory('SBF Energies',opHrs.Id, raisonSocial.Id));
            lstSrvTerr.add(TestFactory.createServiceTerritory('VB Gaz',opHrs.Id, raisonSocial.Id));
            lstSrvTerr.add(TestFactory.createServiceTerritory('Electrogaz',opHrs.Id, raisonSocial.Id));

            lstSrvTerr[0].Agency_Code__c = '1234';
            lstSrvTerr[0].Corporate_Street__c = '12349';
            lstSrvTerr[0].Corporate_Street2__c = '12348';
            lstSrvTerr[0].Corporate_ZipCode__c = '12345';
            lstSrvTerr[0].Corporate_City__c = 'Paris';
            lstSrvTerr[0].Phone__c  = '123456789';
            lstSrvTerr[0].Email__c = 'contact@sbf-energies.com';
            lstSrvTerr[0].site_web__c = 'www.google.com';
            lstSrvTerr[0].IBAN__c = 'FR1023456213456';
            lstSrvTerr[0].Libelle_horaires__c = '1234';
            lstSrvTerr[0].horaires_astreinte__c = '1234';

            lstSrvTerr[1].Agency_Code__c = '3322';
            lstSrvTerr[1].Corporate_Street__c = '12349';
            lstSrvTerr[1].Corporate_Street2__c = '12348';
            lstSrvTerr[1].Corporate_ZipCode__c = '12345';
            lstSrvTerr[1].Corporate_City__c = 'Paris';
            lstSrvTerr[1].Phone__c  = '123456789';
            lstSrvTerr[1].Email__c = 'contact@sbf-energies.com';
            lstSrvTerr[1].site_web__c = 'www.google.com';
            lstSrvTerr[1].IBAN__c = 'FR1123456213456';
            lstSrvTerr[1].Libelle_horaires__c = '1234';
            lstSrvTerr[1].horaires_astreinte__c = '1234';

            lstSrvTerr[2].Agency_Code__c = '1144';
            lstSrvTerr[2].Corporate_Street__c = 'qdqw';
            lstSrvTerr[2].Corporate_Street2__c = '123qdwq48';
            lstSrvTerr[2].Corporate_ZipCode__c = '12345';
            lstSrvTerr[2].Corporate_City__c = 'Paris';
            lstSrvTerr[2].Phone__c  = '123456789';
            lstSrvTerr[2].Email__c = 'contact@sbf-energies.com';
            lstSrvTerr[2].site_web__c = 'www.google.com';
            lstSrvTerr[2].IBAN__c = 'FR1223456213456';
            lstSrvTerr[2].Libelle_horaires__c = '1234';
            lstSrvTerr[2].horaires_astreinte__c = '1234';
            insert lstSrvTerr;

            raisonSocial.sofactoapp_Agence__c = lstSrvTerr[0].Id;
            raisonSocial.sofactoapp__Email__c = 'contact@sbf-energies.com';
            update raisonSocial;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[0].Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement[0].Owner__c = testAcc.Id;
            lstLogement[0].Inhabitant__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[1].Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[2].Id));
            lstLogement[2].Postal_Code__c = 'lgmtPC 3';
            lstLogement[2].City__c = 'lgmtCity 3';
            lstLogement[2].Account__c = testAcc.Id;
            insert lstLogement;

            
            lstTestProd  = new List<Product2>{
                new Product2( Name = 'Product X1', ProductCode = 'MXZ-4F80VF3', isActive = true, Equipment_family__c = 'Pompe à chaleur', Statut__c   = 'Approuvée',
                                                Equipment_type__c = 'Pompe à chaleur air/eau', isBundle__c = true),
                new Product2( Name = 'Product X2', ProductCode = 'MXZ-4F80VF4', isActive = true, Equipment_family__c = 'Pompe à chaleur', Statut__c   = 'Approuvée',
                                                Equipment_type__c = 'Pompe à chaleur air/eau', isBundle__c = false),
                new Product2( Name = 'Product OPT1', ProductCode = 'OPT-4F80VF3', isActive = true, Equipment_family__c = 'Pompe à chaleur', Statut__c   = 'Approuvée',
                                                Equipment_type__c = 'Pompe à chaleur air/eau', isBundle__c = false),
                new Product2( Name = 'Product AIDECEE', ProductCode = 'AIDE-CEE-SBF', isActive = true, Equipment_family__c = 'Pompe à chaleur', Statut__c   = 'Approuvée',
                                                Equipment_type__c = 'Pompe à chaleur air/eau', isBundle__c = false),
                new Product2( Name = 'Product AIDECDP', ProductCode = 'AIDE-CDP-SBF', isActive = true, Equipment_family__c = 'Pompe à chaleur', Statut__c   = 'Approuvée',
                                                Equipment_type__c = 'Pompe à chaleur air/eau', isBundle__c = false),
                new Product2( Name = 'Product AIDEMPR', ProductCode = 'AIDE-MPR-SBF', isActive = true, Equipment_family__c = 'Pompe à chaleur', Statut__c   = 'Approuvée',
                                                Equipment_type__c = 'Pompe à chaleur air/eau', isBundle__c = false),
                new Product2( Name = 'Product PropContrat', ProductCode = 'PROPCONTRAT', isActive = true, Equipment_family__c = 'Pompe à chaleur', Statut__c   = 'Approuvée',
                                                Equipment_type__c = 'Pompe à chaleur air/eau', isBundle__c = false)
            };

            upsert lstTestProd;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd[0].Id, 0));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd[1].Id, 0));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd[2].Id, 0));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd[3].Id, 0));

            insert lstPrcBkEnt; 

            // creating Assets
            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lstLogement[0].Id);
            eqp.Product2Id = lstTestProd[0].Id;
            eqp.Logement__c = lstLogement[0].Id;
            eqp.AccountId = testAcc.Id;
            lstAsset.add(eqp);
            insert lstAsset;

             // creating Assets child
            Asset eqpEnf = TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id);
            eqpEnf.Product2Id = lstTestProd[0].Id;
            eqpEnf.Logement__c = lstLogement[0].Id;
            eqpEnf.AccountId = testacc.Id;
            eqpEnf.parentId = lstAsset[0].Id;
            lstTestAssetChild.add(eqpEnf);
           
            insert lstTestAssetChild;

            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon.add(TestFactory.createServiceContract('test serv con 2', testAcc.Id));

            lstServCon[0].Type__c = 'Individual';
            lstServCon[0].Agency__c = lstSrvTerr[0].Id;
            lstServCon[0].Asset__c = lstAsset[0].Id; //asset 0 with product equipment type Chaudière fioul
            lstServCon[0].PriceBook2Id = pricebookId;
            lstServCon[0].EndDate = Date.Today().addYears(1);
            lstServCon[0].StartDate = Date.Today();
            lstServCon[0].RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives') ;
            lstServCon[0].Type__c = 'Collective';
            lstServCon[0].Payeur_du_contrat__c = testAcc.Id;
            lstServCon[0].TransactionId__c = '123456789';
            lstServCon[0].Logement__c = lstLogement[0].Id;
            
            lstServCon[1].Type__c = 'Individual';
            lstServCon[1].Agency__c = lstSrvTerr[0].Id;
            lstServCon[1].EndDate = Date.Today().addYears(1);
            lstServCon[1].StartDate = Date.Today();
            lstServCon[1].RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives') ;
            lstServCon[1].Type__c = 'Collective';
            lstServCon[1].DocumentID__c = '12345678';

            insert lstServCon;

            lstCli.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));
            lstCli.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[1].Id, 150, 1));
            lstCli.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[2].Id, 150, 1));
            lstCli.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[3].Id, 150, 1));
            insert lstCli;

            // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'Maintenance', lstAsset[0].Id));
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase[0].Origin = 'Cham Digital';
            lstCase[0].Reason__c = 'Visite sous contrat';
            lstCase[0].RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            insert lstCase;



            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Maintenance';
            lstWrkTyp[0].Type_de_client__c = 'Tous';
            lstWrkTyp[0].Agence__c = 'Toutes';
            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());       
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].AssetId = lstAsset[0].Id;
            lstWrkOrd[0].CaseId = lstCase[0].Id;
            lstWrkOrd[0].AccountId = testAcc.Id;

            insert lstWrkOrd;

            
            //create opportunity
            lstOpp =  new List<Opportunity>{ 
                TestFactory.createOpportunity('Test1',testAcc.Id) ,
                TestFactory.createOpportunity('Test2',testAcc.Id) 
            };

            insert lstOpp;

            //Create Quotes
            lstQuote = new List<Quote> {
                new Quote(  Name ='Prestations'
                            ,Pricebook2Id = pricebookId
                            ,OpportunityId = lstOpp[0].Id
                            ,Devis_signe_par_le_client__c = false
                            ,Status = 'In Review'
                            ,Date_de_debut_des_travaux__c = System.today()
                            ,tech_deja_facture__c = true
                            ,Mode_de_paiement_de_l_acompte__c = 'CB'
                            ,Montant_de_l_acompte_verse__c=500
                            ,Logement__c = lstLogement[0].Id
                            ,Agency__c = lstSrvTerr[0].Id
                            ,Nom_du_payeur__c = testAcc.Id
                            ,TransactionId__c = '123456789',
                            Equipement__c = lstAsset[0].Id
                        ),
                new Quote(  Name ='Travaux'
                            ,Pricebook2Id = pricebookId
                            ,OpportunityId = lstOpp[0].Id
                            ,Devis_signe_par_le_client__c = false
                            ,Status = 'Needs Review'
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                            ,Logement__c = lstLogement[0].Id
                            ,Agency__c = lstSrvTerr[0].Id
                        ), 
                new Quote(  Name ='Remplacement'
                            ,Devis_signe_par_le_client__c = false
                            ,OpportunityId = lstOpp[1].Id
                            ,Pricebook2Id = pricebookId
                            ,Status = 'Draft'
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                            ,Logement__c = lstLogement[0].Id
                            ,Agency__c = lstSrvTerr[0].Id
                        ),
                new Quote(  Name ='Sous-traitance totale' 
                            ,Devis_signe_par_le_client__c = false
                            ,OpportunityId = lstOpp[1].Id
                            ,Pricebook2Id = pricebookId
                            ,Status = 'Denied'
                            //,Type_de_devis__c = 'Vente de pièces/équipements au guichet'    
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                            ,Agency__c = lstSrvTerr[0].Id
                        )
            };
            insert lstQuote;    

            //create QuoteLineItem
            lstQuoteLineItem =  new List<QuoteLineItem>{
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[0].Id,
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today(),
                                    Taux_de_TVA__c = 5.5
                                ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[0].Id,
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today(),
                                    Taux_de_TVA__c = 10
                                ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[3].Id,                                     //AIDE CEE
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today(),
                                    Taux_de_TVA__c = 10
                                ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[4].Id,                                 
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today(),
                                    Taux_de_TVA__c = 20
                                ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[5].Id,
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[6].Id,
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd[0].Id,
                                    QuoteId = lstQuote[1].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today(),
                                    Taux_de_TVA__c = 20
                                ),
                new QuoteLineItem( Quantity = 1,
                                    QuoteId = lstQuote[1].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                )
                };
            insert lstQuoteLineItem;


             // creating service appointment
             lstServiceApp = new List<ServiceAppointment>{
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Service_Contract__c = lstServCon[0].Id,
                    Work_Order__c = lstWrkOrd[0].Id,
                    Residence__c =   lstLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstSrvTerr[0].Id,
                    Quote__c = null
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Service_Contract__c = null,
                    Quote__c = lstQuote[0].Id,
                    Work_Order__c = lstWrkOrd[0].Id,
                    Residence__c =   lstLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstSrvTerr[1].Id,
                    DocumentID__c = '12345678'
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Work_Order__c = lstWrkOrd[0].Id,
                    Residence__c =   lstLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstSrvTerr[2].Id
                )
            };

            insert lstServiceApp;     
            

            lstFacturesClients = new List<sofactoapp__Factures_Client__c>{
                new sofactoapp__Factures_Client__c(
                    Sofactoapp_Contrat_de_service__c = lstServCon[0].Id,
                    sofactoapp__Compte__c = testAcc.Id,
                    sofactoapp__emetteur_facture__c = raisonSocial.Id,
                    SofactoappType_Prestation__c = 'Contrat individuel',
                    sofactoapp__Compte2__c = testAcc.Id,
                    TransactionId__c = '123456789',
                    sofactoapp__isLocked__c = false
                ),
                new sofactoapp__Factures_Client__c(
                    sofactoapp__Compte__c = testAcc.Id,
                    sofactoapp__emetteur_facture__c = raisonSocial.Id,
                    SofactoappType_Prestation__c = 'FacturedAcompte',
                    sofacto_devis__c = lstQuote[0].Id,
                    sofactoapp__Compte2__c = testAcc.Id,
                    DocumentID__c = '12345678'
                ),
                new sofactoapp__Factures_Client__c(
                    sofactoapp__Compte__c = testAcc.Id,
                    sofactoapp__emetteur_facture__c = raisonSocial.Id,
                    SofactoappType_Prestation__c = 'FacturedAcompte',
                    sofactoapp__Compte2__c = testAcc.Id
                )
            };


            insert lstFacturesClients;

            lstFacLne = new List<sofactoapp__Ligne_de_Facture__c>{
                new sofactoapp__Ligne_de_Facture__c(sofactoapp__Produit__c = lstTestProd[0].Id, sofactoapp__Facture__c = lstFacturesClients[0].Id, sofactoapp__Quantit__c = 1,
                                                        sofactoapp__Prix_Unitaire_HT__c = 100, sofactoapp__Description__c = 'test desc',
                                                        sofactoapp__Debut__c = System.today(), sofactoapp__Fin__c= System.Today().addDays(1)),
                new sofactoapp__Ligne_de_Facture__c(sofactoapp__Produit__c = lstTestProd[3].Id, sofactoapp__Facture__c = lstFacturesClients[0].Id, sofactoapp__Quantit__c = 1,
                                                        sofactoapp__Prix_Unitaire_HT__c = 100, sofactoapp__Description__c = 'test desc',
                                                        sofactoapp__Debut__c = System.today(), sofactoapp__Fin__c= System.Today().addDays(1)),
                new sofactoapp__Ligne_de_Facture__c(sofactoapp__Produit__c = lstTestProd[4].Id, sofactoapp__Facture__c = lstFacturesClients[0].Id, sofactoapp__Quantit__c = 1,
                                                        sofactoapp__Prix_Unitaire_HT__c = 100, sofactoapp__Description__c = 'test desc',
                                                        sofactoapp__Debut__c = System.today(), sofactoapp__Fin__c= System.Today().addDays(1)),
                new sofactoapp__Ligne_de_Facture__c(sofactoapp__Produit__c = lstTestProd[5].Id, sofactoapp__Facture__c = lstFacturesClients[0].Id, sofactoapp__Quantit__c = 1,
                                                        sofactoapp__Prix_Unitaire_HT__c = 100, sofactoapp__Description__c = 'test desc',
                                                        sofactoapp__Debut__c = System.today(), sofactoapp__Fin__c= System.Today().addDays(1)),
                new sofactoapp__Ligne_de_Facture__c(sofactoapp__Produit__c = lstTestProd[6].Id, sofactoapp__Facture__c = lstFacturesClients[0].Id, sofactoapp__Quantit__c = 1,
                                                        sofactoapp__Prix_Unitaire_HT__c = 100, sofactoapp__Description__c = 'test desc',
                                                        sofactoapp__Debut__c = System.today(), sofactoapp__Fin__c= System.Today().addDays(1))
            };
            insert lstFacLne;

            lstConVer.add(
                new ContentVersion(
                    Title = 'Penguins',
                    PathOnClient = 'Penguins.jpg',
                    VersionData = Blob.valueOf('Test Content'),
                    IsMajorVersion = true
                    )
            );
            insert lstConVer;    
            lstConDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

            //create ContentDocumentLink  record 

            lstConDocLink = new List<ContentDocumentLink>{
                new ContentDocumentLink(
                LinkedEntityId = lstQuote[0].id,
                ContentDocumentId = lstConDoc[0].Id,
                shareType = 'V'),
                new ContentDocumentLink(
                LinkedEntityId = lstServCon[0].id,
                ContentDocumentId = lstConDoc[0].Id,
                shareType = 'V'),
                new ContentDocumentLink(
                LinkedEntityId = lstFacturesClients[0].id,
                ContentDocumentId = lstConDoc[0].Id,
                shareType = 'V')
            };
            insert lstConDocLink;
        
        }


    }
    @isTest
    static void testcreateJSON1(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstServiceApp[0].Id,true,true,true,null,null,false);
            Test.stopTest();
        }
        
    }
    @isTest
    static void testcreateJSON2(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstServiceApp[1].Id,true,true,true,null,null,false);
            Test.stopTest();
        }   
    }
    @isTest
    static void testcreateJSON3(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusprinting());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstServiceApp[2].Id,true,true,true,null,null,false);
            Test.stopTest();
        }   
    }
    @isTest
    static void testcreateJSON4(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstQuote[0].Id,true,true,true,null,null,false);
            Test.stopTest();
        }
    }
    @isTest
    static void testcreateJSON5(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstFacturesClients[0].Id,true,true,false,null,null,false);
            Test.stopTest();
        }
    }
    @isTest
    static void testcreateJSON6(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusEmitted());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstFacturesClients[1].Id,true,true,false,null,null,false);
            Test.stopTest();
        }
    }
    @isTest
    static void testcreateJSON7(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatuserroremtting());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstFacturesClients[2].Id,true,true,false,null,null,false);
            Test.stopTest();
        }
    }
    @isTest
    static void testcreateJSON8(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstServCon[0].Id,true,true,true,'RENOUV1_TCH',null,false);
            Test.stopTest();
        }
    }
    @isTest
    static void testcreateJSON9(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatuserrorprinting());
            Test.startTest();
            WS13_ChaineEditiquev2.createJSON(lstServCon[1].Id,false,true,false,null,null,false);
            Test.stopTest();
        }
    }

    @isTest
    static void testAcheminement1(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusEmitted());
            Test.startTest();
            WS13_ChaineEditiquev2.acheminer(lstServiceApp[0].Id);
            Test.stopTest();
        }
    }
    @isTest
    static void testAcheminement2(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusprinting());
            Test.startTest();
            WS13_ChaineEditiquev2.acheminer(lstServCon[0].Id);
            Test.stopTest();
        }
    }
    @isTest
    static void testAcheminement3(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusEmitted());
            Test.startTest();
            WS13_ChaineEditiquev2.acheminer(lstQuote[1].Id);
            Test.stopTest();
        }
    }
    @isTest
    static void testAcheminement4(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatuserroremtting());
            Test.startTest();
            WS13_ChaineEditiquev2.acheminer(lstFacturesClients[0].Id);
            Test.stopTest();
        }
    }
    
    @isTest
    static void testGetPicklistValue(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
                WS13_ChaineEditiquev2.createJSON(lstFacturesClients[1].Id,true,true,true,null,null,false);
                WS13_ChaineEditiquev2.pickListvalue(lstQuote[0].Id);
                WS13_ChaineEditiquev2.pickListvalue(lstFacturesClients[0].Id);
                WS13_ChaineEditiquev2.pickListvalue(lstServCon[0].Id);
                
            Test.stopTest();
        }
        
    }

    @isTest
    static void testgetStringError(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusCode404());
            Test.startTest();
            WS13_ChaineEditiquev2.getString64(lstServCon[0].TransactionId__c, lstServCon[0].Id);
            WS13_ChaineEditiquev2.getString(lstServCon[0].TransactionId__c, lstServCon[0].Id);
            Test.stopTest();
        }
    }

    @isTest
    static void testSendEmail(){
        System.runAs(mainUser){
            Test.startTest();
            List<String> lstEmail = new List<String>{'adarsh@spoon.com'};

            WS13_ChaineEditiquev2.sendMail(lstQuote[0].Id,lstEmail);
            WS13_ChaineEditiquev2.sendMail(lstFacturesClients[0].Id,lstEmail);
            WS13_ChaineEditiquev2.sendMail(lstServCon[0].Id,lstEmail);
            Test.stopTest();
        }
    }

    public class calloutStatusprinting implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"printing"}');
            response.setStatusCode(200);
            return response; 
        }
    }

     public class calloutStatusPrinted implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"printed"}');
            response.setStatusCode(200);
            return response; 
        }
    }

    public class calloutStatusEmitted implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"emitted"}');
            response.setStatusCode(200);
            return response; 
        }
    }

    public class calloutStatuserrorprinting implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"error-printing"}');
            response.setStatusCode(200);
            return response; 
        }
    }

    public class calloutStatuserroremtting implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"error-emitting"}');
            response.setStatusCode(200);
            return response; 
        }
    }

    public class calloutStatusCode404 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setStatusCode(404);
            response.setBody('{"code":404,"status":"not found"}');
            return response; 
        }
    }

}