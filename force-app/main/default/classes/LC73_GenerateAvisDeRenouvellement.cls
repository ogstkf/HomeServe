/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 08-27-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-25-2020   ZJO   Initial Version
**/
public with sharing class LC73_GenerateAvisDeRenouvellement {

    @AuraEnabled
    public static map<String,Object> saveAvisDeRenouvellement(String servConId){
        System.debug('** Starting LC73_GenerateAvisDeRenouvellement');
        
        List<String> lstServConId = new List<String>();
        List<ContentVersion> lstCV = new List<ContentVersion>();
        map<String,Object> mapOfResult = new map<String,Object>();
        
        lstServConId.add(servConId);
        ServiceContract servCon = [SELECT Id, TECH_Date_d_edition__c FROM ServiceContract WHERE Id =:servConId];
        System.debug('** servConId:' + servConId);
        
        Savepoint sp = Database.setSavepoint();
        try{
            System.debug('** Starting PDF Generation');
            PageReference AvisDeRenTemplate = new PageReference('/apex/VFP72_ServiceContract');
            System.debug('** after VFpage');
            Blob docPDF;
            System.debug('** before params');
            AvisDeRenTemplate.getParameters().put('lstIds', JSON.serialize(lstServConId));
            System.debug('** after params');
            docPDF = (Test.isRunningTest() ? Blob.valueOf('Unit.Test') : AvisDeRenTemplate.getContentAsPDF());
            ContentVersion CV = new ContentVersion();
            Datetime now = Datetime.now();
            Integer offset = UserInfo.getTimezone().getOffset(now);
            Datetime local = now.addSeconds(offset/1000);
            CV.PathOnClient = 'Avis de renouvellement_' + local + '.pdf';
            CV.Title = 'Avis de renouvellement_' + local + '.pdf';
            CV.VersionData = docPDF;
            CV.IsMajorVersion = true;

            Insert CV;
            lstCV.add(CV);

            //Get Content Documents
            List<ContentVersion> docList = [SELECT ContentDocumentId FROM ContentVersion where Id=:lstCV];
            System.debug('## doc list:' + docList);
            Map<Id, Id> mapIdCVIdCDL = new Map<Id, Id>();
            for(ContentVersion contentV : docList){
                mapIdCVIdCDL.put(contentV.Id,contentV.ContentDocumentId);
            }

            //Create ContentDocumentLink 
            ContentDocumentLink conDocLink = New ContentDocumentLink();
            conDocLink.LinkedEntityId = servConId;
            conDocLink.ContentDocumentId = mapIdCVIdCDL.get(cv.Id);
            conDocLink.shareType = 'V';
            Insert conDocLink;

            //Update Last Modified Date
            servCon.TECH_Date_d_edition__c = System.today();
            Update servCon;

            mapOfResult.put('error',false);
	        mapOfResult.put('message', 'SUCCESS!!!');
            mapOfResult.put('AvisDeRen', conDocLink.ContentDocumentId);

        }catch(Exception e){

            Database.rollback(sp);
            mapOfResult.put('error',true);
	        mapOfResult.put('message',e.getMessage());   
            throw new AuraHandledException(e.getMessage());           
        }

        return mapOfResult;        
    }

}