/**
 * @File Name          : LC12_QuotesCase_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 07-15-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    05/11/2019   AMO     Initial Version
**/
@isTest
public with sharing class LC12_QuotesCase_TEST {

    static User adminUser;
    static Account testAcc;
    static Quote quo;
    static Opportunity opp;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Case> lstCase = new List<Case>();
    static List<Product2> lstProd = new List<Product2>();
    static List<WorkOrder> lstWorkOrder = new List<WorkOrder>();

    static{

        adminUser = TestFactory.createAdminUser('LC12_QuotesCase_TEST@test.COM', TestFactory.getProfileAdminId());
		insert adminUser;

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('AP12_ServiceAppointmentRules_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon[0].Type__c = 'Individual';
            lstServCon.add(TestFactory.createServiceContract('test serv con 2', testAcc.Id));
            lstServCon[1].Type__c = 'Individual';

            insert lstServCon;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc.Id;
            insert lstLogement;

            //creating products
            lstProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstProd.add(TestFactory.createProduct('Entretien gaz'));
            
            insert lstProd;

            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstProd[0].Id;
            lstAsset[0].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstLogement[1].Id));
            lstAsset[1].Product2Id = lstProd[0].Id;
            lstAsset[1].AccountId = testAcc.Id;
            insert lstAsset;

            // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[0].Id));
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[1].Id));
            lstCase[1].Service_Contract__c = lstServCon[1].Id;
            insert lstCase;

            //Create WorkOrder
            lstWorkOrder = new List<WorkOrder>{
                new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal, CaseId = lstCase[0].Id),
                new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal, CaseId = lstCase[1].Id)
            };

            insert lstWorkOrder;

            //create opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);
            insert opp;

            //create Quote
            quo = new Quote(Name ='Test1',
                            Devis_signe_par_le_client__c = true,
                            OpportunityId = opp.Id,
                            status = 'Validé, signé et terminé',
                            Date_de_debut_des_travaux__c = System.today(),
                            // Ordre_d_execution__c = wrkOrd.Id,
                            // Pricebook2Id = pricebookId,
                            Ordre_d_execution__c = lstWorkOrder[0].Id
                            );
            insert quo;
        }
    }


    @isTest
    static void testUpdate(){
        System.runAs(adminUser){
            Test.startTest();
                LC12_QuotesCase.fetchQuote(lstCase[0].Id);
            Test.stopTest();
        }
    }

}