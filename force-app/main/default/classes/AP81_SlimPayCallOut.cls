/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 07-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   23-02-2022   MNA   Initial Version
**/
public with sharing class AP81_SlimPayCallOut {
    public static void createOrders(List<ServiceContract> lstSc) {
        for (ServiceContract sc : lstSc) {
            WS16_GestionDesPrelevements.createOrderAsync(sc.Id, Util.generateUniqueString());
        }
    }

    public static void createMandates(Set<Id> setCBId) {
        for (ServiceContract sc : [SELECT Id FROM ServiceContract WHERE sofactoapp_Rib_prelevement__c IN : setCBId]) {
            WS16_GestionDesPrelevements.createMandateAsync(sc.Id);
        }
    }
}