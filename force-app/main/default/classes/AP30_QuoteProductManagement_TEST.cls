/**
 * @File Name          : AP30_QuoteProductManagement_TEST.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 09-14-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25-10-2019    LGO     Initial Version
**/
@isTest
public with sharing class AP30_QuoteProductManagement_TEST {
    
    static User mainUser;
    static Account testAcc = new Account();
	static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
	static PricebookEntry PrcBkEnt = new PricebookEntry();
    static WorkOrder wrkOrd = new WorkOrder();
    static QuoteLineItem qli = new QuoteLineItem();
    static Quote quo = new Quote();
    static Product2 prod = new Product2();
    // static List<SyncingQLI__c> syncQli = new List<SyncingQLI__c>(); 
    static Bypass__c bp = new Bypass__c();
    static{
        mainUser = TestFactory.createAdminUser('AP30@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){

            //insert custom settings 
            // syncQli.add(
            //     new SyncingQLI__c(
            //         Name = 'Description'
            //         ,QLI__c = 'Description'
            //         ,ToCreateQLI__c = true
            //         ,MappingType__c = 'OpportunityLineItem-QuoteLineItem'
            //     )
            // );   

            // syncQli.add(
            //     new SyncingQLI__c(
            //         Name = 'Quantity'
            //         ,QLI__c = 'Quantity'
            //         ,ToCreateQLI__c = true
            //         ,MappingType__c = 'OpportunityLineItem-QuoteLineItem'
            //     )
            // ); 

            // syncQli.add(
            //     new SyncingQLI__c(
            //         Name = 'TotalPrice'
            //         ,QLI__c = 'TotalPrice'
            //         ,ToCreateQLI__c = true
            //         ,MappingType__c = 'OpportunityLineItem-QuoteLineItem'
            //     )
            // );   

            // syncQLI.add(
            //     new SyncingQLI__c(
            //         Name = 'Pricebook2Id'
            //         ,ToCreateQLI__c = true
            //         ,QLI__c = 'Pricebook2Id'
            //         ,MappingType__c = 'Opportunity-Quote'
            //     )
            // );  
            
            // insert syncQli; 

            bp.SetupOwnerId  = mainUser.Id;
            bp.BypassTrigger__c = 'LC08';
            insert bp;
            

            //create account
            testAcc = TestFactory.createAccount('AP30_QuoteProductManagement_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

           //opportunity
            Opportunity testOpp = TestFactory.createOpportunity('test', testAcc.Id);
            insert testOpp;

            //work order
            wrkOrd = TestFactory.createWorkOrder();
            insert wrkOrd;

            Id pricebookId = Test.getStandardPricebookId();
            //Create your product
            prod = TestFactory.createProduct('Product X');
            prod.ProductCode = 'Pro-X';
            prod.Equipment_family__c = 'Chaudière';
            prod.Equipment_type__c = 'Chaudière gaz';
            prod.Famille_d_articles__c = AP_constant.productFamillePieceDetache;
            insert prod;

           //Create your pricebook entry
            PricebookEntry pbEntry = new PricebookEntry(Pricebook2Id = pricebookId,
                                                        Product2Id = prod.Id,
                                                        UnitPrice = 100.00,
                                                        IsActive = true
            );
            insert pbEntry;

            //create Location
            Schema.Location loc = new Schema.Location(Name='Test1',
                                      LocationType='Entrepôt',
                                      ParentLocationId=NULL,
                                      IsInventoryLocation=true);
            insert loc;

            //create Operating Hours
			OperatingHours oh = new OperatingHours(Name='Test1');
			insert oh;

			//create Corporate Name
			sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(
                Name='Test1',
                sofactoapp__Credit_prefix__c='Test1',
                sofactoapp__Invoice_prefix__c='Test1'
            );  
            insert srs;

            //create Service Territory
            ServiceTerritory st = new ServiceTerritory(Name='Test1',
                                      OperatingHoursId=oh.Id,
                                      Sofactoapp_Raison_Social__c=srs.Id);
            insert st;
            
            //create Service Territory Location
            ServiceTerritoryLocation stl = new ServiceTerritoryLocation(ServiceTerritoryId=st.Id,
                                               LocationId=loc.Id);
            insert stl;

            //create Quote
            quo = new Quote(Name ='Test1',
                Devis_signe_par_le_client__c = false,
                OpportunityId = testOpp.Id,
                Ordre_d_execution__c = wrkOrd.Id,
                Pricebook2Id = pricebookId,
                Agency__c = st.Id,
                Date_de_debut_des_travaux__c = System.today()
            );
            insert quo;

            //create QuoteLineItem

            qli  = new QuoteLineItem(Quantity= 2,
                                    Product2Id = prod.Id,
                                    QuoteId = quo.Id, 
                                    PricebookEntryId = pbEntry.Id,
                                    UnitPrice = 10);
            insert qli;
        }
    }

    @isTest
    public static void testcreateRequiredProducts(){
        System.runAs(mainUser){
            
            quo.Status = AP_Constant.quoteStatusValideSigne;
            quo.Devis_signe_par_le_client__c = true;
            quo.Date_de_debut_des_travaux__c = System.today();
            quo.Salarie_primable__c = mainUser.Id;
            Test.startTest();
            
                update quo;     
            List<Quote> qts = new List<Quote>();
            qts.add(quo);
            AP30_QuoteProductManagement.createRequiredProducts(qts);
            Test.stopTest();

           

            //System.assertEquals(lstprodReq.size(),1);
        }
    }

    @isTest
    public static void testcreateRequiredLineItems(){
        System.runAs(mainUser){
            
            quo.Status = AP_Constant.quoteStatusValideSigne;
            quo.Devis_signe_par_le_client__c = true;
            quo.Date_de_debut_des_travaux__c = System.today();
            quo.Salarie_primable__c = mainUser.Id;
            update quo; 
            Test.startTest();
                AP30_QuoteProductManagement.createProductRequiredLineItem(new List<QuoteLineItem>{qli});
            Test.stopTest();
        }
    }
}