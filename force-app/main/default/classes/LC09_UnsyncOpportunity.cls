public with sharing class LC09_UnsyncOpportunity {
     /**
    * @File Name          : LC09_UnsyncOpportunity.cls
    * @Description        : 
    * @Author             : Spoon Consulting (LGO)
    * @Group              : 
    * @Last Modified By   : 
    * @Last Modified On   : 
    * @Modification Log   : 
    *==============================================================================
    * Ver         Date                     Author      Modification
    *==============================================================================
    * 1.0         18-10-2019     		LGO         Initial Version (CT-1062)
    **/

    @AuraEnabled
    public static String clearDevisOpp(String oppId, Boolean toDelete){

        String message = '';
        
        try{
            
            Opportunity opp = [SELECT Id, 
                                Tech_Synced_Devis__c, 
                                Tech_Synced_Devis__r.IsSync__c,
                                (SELECT Id 
                                 FROM OpportunitylineItems)  
                                FROM Opportunity WHERE Id = :oppId];

            System.debug('beforetoUpdate opportunity '+ opp);
            //if tech_devis exist on opportunity then unsync
            if (opp.Tech_Synced_Devis__c <> null) {

               Opportunity oppUpd = new Opportunity(Id = opp.Id, Tech_Synced_Devis__c = null);

                System.debug('toUpdate opportunity '+ opp);
                Quote quo = new Quote(Id = opp.Tech_Synced_Devis__c, IsSync__c=false );

                update quo;
                update oppUpd;  

                message = 'L\'opportunité a été désynchronisée avec succès';

                //if chkbox delete is chosen then delete all opportunityLineItems
                if(toDelete){
                    System.debug('toDelete list '+ opp.OpportunityLineItems);
                    delete opp.OpportunityLineItems;
                    message += ' et les lignes de l\'opportunité ont été supprimées avec succès';
                }
            }
            else message = 'Aucune devis n\'a été retrouvé pour la désynchronisation'; 


            
            return message;
        }
        

        catch(Exception ex){
            system.debug('## error is :' + ex.getMessage());
            system.debug('## occuring at line :' + ex.getLineNumber());
            throw new AuraHandledException(ex.getMessage()); 
            
        }
    }    

}