/**
 * @File Name          : WS14_APISage.cls
 * @Description        : 
 * @Author             : MNA
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 28-02-2022
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0    09/02/2021                     MNA      Initial Version
**/
public without sharing class WS14_APISage {
    static String HtmlBody = '';

    static WebServiceConfiguration__mdt wsFacture = [SELECT Id, Endpoint__c, DeveloperName, Header_Content_Type__c, HeaderAuthorization__c
                                                        FROM WebServiceConfiguration__mdt WHERE DeveloperName = 'WS_Sage_Facture'];
    static WebServiceConfiguration__mdt wsReglement = [SELECT Id, Endpoint__c, DeveloperName, Header_Content_Type__c, HeaderAuthorization__c
                                                        FROM WebServiceConfiguration__mdt WHERE DeveloperName = 'WS_Sage_Reglement'];
    static WebServiceConfiguration__mdt wsRemise = [SELECT Id, Endpoint__c, DeveloperName, Header_Content_Type__c, HeaderAuthorization__c
                                                        FROM WebServiceConfiguration__mdt WHERE DeveloperName = 'WS_Sage_Remise'];
    static List<sofactoapp__External_Api__c> lstExtApiToUpdate = new List<sofactoapp__External_Api__c>();
    public static String appelWS(List<sofactoapp__External_Api__c> lstExtApi) {
        /*List<sofactoapp__External_Api__c> lstExtApi = new List<sofactoapp__External_Api__c>(
                                        [SELECT Id, Name, analytique_client__c, analytique_departement__c, analytique_etablissement__c, analytique_produit__c, code_client__c,
                                            Comptabilise__c, compte_general__c, date__c, date_echeance__c, devise__c, etablissement__c, Journal__c, libelle__c, mode_reglement__c,
                                            montant__c, nom__c, num_contrat__c, num_equipement__c, num_facture__c, numero_piece__c, sofactoapp__Plateforme__c, prenom__c,
                                            profil_tva__c, raison_sociale__c, sofactoapp__Response__c, sens__c, SIRET__c, Societe__c, sofactoapp__Status_Code__c,
                                            sofactoapp__TechError__c, tiers__c, type_ligne__c, Type_piece__c, URL__c, Type_Ecriture__c, Analytique_categorie_produit__c
                                                        FROM sofactoapp__External_Api__c]);*/

        List<WSTransactionLog__c> lstLog = new List<WSTransactionLog__c>();
        //List<WSTransactionLog__c> lstLogToDel = new List<WSTransactionLog__c>([SELECT Id FROM WSTransactionLog__c WHERE DateTime__c <= :System.now().addMonths(-6)]);
                                                
        if (lstExtApi == null || lstExtApi.size() == 0)
            return 'null';
        List<sofactoapp__External_Api__c> lstExpApiToRemove = new List<sofactoapp__External_Api__c>();
        Map<String, List<sofactoapp__External_Api__c>> mapExtApi = new Map<String, List<sofactoapp__External_Api__c>>();
        for (sofactoapp__External_Api__c ExtApi: lstExtApi) {
            Integer index = 0;
            List<String> lstExtApiName = new List<String>();
            lstExtApiName.addAll(mapExtApi.keySet());
            for (Integer i = 0; i < lstExtApiName.size(); i++) {
                if (ExtApi.Journal__c == mapExtApi.get(lstExtApiName[i])[0].Journal__c && ExtApi.Type_piece__c == mapExtApi.get(lstExtApiName[i])[0].Type_piece__c
                   && ExtApi.numero_piece__c == mapExtApi.get(lstExtApiName[i])[0].numero_piece__c && ExtApi.date__c == mapExtApi.get(lstExtApiName[i])[0].date__c
                   && ExtApi.Societe__c == mapExtApi.get(lstExtApiName[i])[0].Societe__c && ExtApi.libelle__c == mapExtApi.get(lstExtApiName[i])[0].libelle__c
                   && ExtApi.URL__c == mapExtApi.get(lstExtApiName[i])[0].URL__c && ExtApi.devise__c == mapExtApi.get(lstExtApiName[i])[0].devise__c
                   && ExtApi.num_contrat__c == mapExtApi.get(lstExtApiName[i])[0].num_contrat__c
                   && ExtApi.num_equipement__c == mapExtApi.get(lstExtApiName[i])[0].num_equipement__c
                   && ExtApi.prenom__c == mapExtApi.get(lstExtApiName[i])[0].prenom__c && ExtApi.nom__c == mapExtApi.get(lstExtApiName[i])[0].nom__c
                   && ExtApi.code_client__c == mapExtApi.get(lstExtApiName[i])[0].code_client__c && ExtApi.SIRET__c == mapExtApi.get(lstExtApiName[i])[0].SIRET__c
                   && ExtApi.raison_sociale__c == mapExtApi.get(lstExtApiName[i])[0].raison_sociale__c
                   && ExtApi.Type_Ecriture__c == mapExtApi.get(lstExtApiName[i])[0].Type_Ecriture__c) {
                       
                       lstExpApiToRemove.add(ExtApi);
                       if (mapExtApi.containsKey(lstExtApiName[i])) 
                           mapExtApi.get(lstExtApiName[i]).add(ExtApi);
                       
                       else 
                           mapExtApi.put(lstExtApiName[i], new List<sofactoapp__External_Api__c>{ExtApi});
                       
                       
                }
                else {
                    if (i == lstExtApiName.size() - 1){
                        mapExtApi.put(ExtApi.Name, new List<sofactoapp__External_Api__c>{ExtApi});
                    }
                }
            }
            if (lstExtApiName.size() == 0)
                mapExtApi.put(ExtApi.Name, new List<sofactoapp__External_Api__c>{ExtApi});
        }
        for (sofactoapp__External_Api__c ExtApi: lstExpApiToRemove) {
            mapExtApi.remove(ExtApi.Name);
        }
        
        List<sofactoapp__External_Api__c> lstFacAvoir = new List<sofactoapp__External_Api__c>();
        List<sofactoapp__External_Api__c> lstReglmnt = new List<sofactoapp__External_Api__c>();
        List<sofactoapp__External_Api__c> lstRemise = new List<sofactoapp__External_Api__c>();
        Map<String, List<sofactoapp__External_Api__c>> mapFacAvoir = new Map<String, List<sofactoapp__External_Api__c>>();
        Map<String, List<sofactoapp__External_Api__c>> mapReglmnt = new Map<String, List<sofactoapp__External_Api__c>>();
        Map<String, List<sofactoapp__External_Api__c>> mapRemise = new Map<String, List<sofactoapp__External_Api__c>>();

        System.debug(mapExtApi);
        for (String key: mapExtApi.keySet()) {
            if (mapExtApi.get(key)[0].Type_Ecriture__c == 'facture_liste')
                mapFacAvoir.put(key, mapExtApi.get(key));
            if (mapExtApi.get(key)[0].Type_Ecriture__c == 'liste_reglements')
                mapReglmnt.put(key, mapExtApi.get(key));
            if (mapExtApi.get(key)[0].Type_Ecriture__c == 'liste_remises')
                mapRemise.put(key, mapExtApi.get(key));
        }

        System.debug(lstExtApi);
        
        if (mapFacAvoir.keySet().size() > 0) {
            lstLog.add(send(mapFacAvoir, 'facture_liste'));
        } 
        if (mapReglmnt.keySet().size() > 0) {
            lstLog.add(send(mapReglmnt, 'liste_reglements'));
        }
        if (mapRemise.keySet().size() > 0) {
            lstLog.add(send(mapRemise, 'liste_remises'));
        }


        if (HtmlBody != '')
            sendMail('Error Sage', HtmlBody);

        if (lstExtApiToUpdate.size() > 0)
            update lstExtApiToUpdate;
/*
        if (lstLogToDel.size() > 0)
            delete lstLogToDel;*/
            
        if (lstLog.size() > 0)
            insert lstLog;

        return 'success';
    }

    public static WSTransactionLog__c send(Map<String, List<sofactoapp__External_Api__c>> mapExtApi, String Type_Ecriture) {

        Map<String, Boolean> mapBoolSuccessToExtApiName = new Map<String, Boolean>();
        Map<String, String> mapExtApiNameLineToExtApiName = new Map<String, String>();

        
        for (String strKey: mapExtApi.keySet()) {
            for (sofactoapp__External_Api__c extApi: mapExtApi.get(strKey)) {
                mapExtApiNameLineToExtApiName.put(extApi.Name, strKey);
            }
        }

        DateTime dtNow;

        Http http = new Http();
        HttpRequest req = new HttpRequest();


        req.setHeader('Authorization', wsFacture.HeaderAuthorization__c);
        req.setHeader('Content-Type', wsFacture.Header_Content_Type__c);
        
        String endpoint = '';

        if (Type_Ecriture == 'facture_liste')
            endpoint = wsFacture.Endpoint__c;

        else if (Type_Ecriture == 'liste_reglements')
            endpoint = wsReglement.Endpoint__c;

        else if (Type_Ecriture == 'liste_remises')
            endpoint = wsRemise.Endpoint__c;

        req.setEndpoint(endpoint);
        
        req.setMethod('POST');

        List<Facture> lstFac = new List<Facture>();

        for (String key: mapExtApi.keySet()) {
            if (Type_Ecriture == 'facture_liste')
                lstFac.add(new Liste_Facture(mapExtApi.get(key), Type_Ecriture));
            else if (Type_Ecriture == 'liste_reglements')
                lstFac.add(new Liste_Reglement(mapExtApi.get(key), Type_Ecriture));
            else if (Type_Ecriture == 'liste_remises')
                lstFac.add(new Liste_Remise(mapExtApi.get(key), Type_Ecriture));

        }

        String body = JSON.serialize((Object) lstFac);
        body = body.replaceAll('"datex":', '"date":');
        body = body.replaceAll('"date_PREVISIONREGLEMENT":', '"date_PREVISION REGLEMENT":');
        body = body.replaceAll('"date_REGLEMENT":', '"date_ REGLEMENT":');

        body = '{"'+ Type_Ecriture +'":'
                    + body
                + '}';

        System.debug('####body: ' + body);
        req.setBody(body);

        dtNow = System.now();                                                           //Date Heure de l'envoi

        httpResponse response = http.send(req);
        System.debug('###Envoi du JSON: '+response.getStatusCode()+'         '+response.getStatus()+'       ' + response.getBody() );
        System.debug(mapExtApi);
        if (response.getStatusCode() == 200) {
            Map<String, Object> mapObj = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            

            if (mapObj.get('code') == 0) {
                for (String key: mapExtApi.keySet()) {
                    for (sofactoapp__External_Api__c extApi: mapExtApi.get(key)) {
                        extApi.sofactoapp__Status_Code__c = String.valueOf(response.getStatusCode());
                        extApi.sofactoapp__Response__c = response.getBody();
                        extApi.sofactoapp__TechError__c = '';
                        lstExtApiToUpdate.add(extApi);
                    }
                }
            }
            else if (mapObj.get('code') == 1) {
                List<Object> lstObj = (List<Object>)mapObj.get('Errors');
                
                HtmlBody +=  '<h3>';
                if (Type_Ecriture == 'facture_liste')
                    HtmlBody +=  'Liste des factures';
        
                else if (Type_Ecriture == 'liste_reglements')
                    HtmlBody +=  'Liste des reglements';
        
                else if (Type_Ecriture == 'liste_remises')
                    HtmlBody +=  'Liste des remises';

                HtmlBody += '</h3>';

                List<String> lstExtApiName = new List<String>();
                Map<String, String> Errors = new Map<String, String>();
                Map<String, List<Map<String, Object>>> ErrorsObj = new Map<String, List<Map<String, Object>>>();
                for (Object o: lstObj) {
                    Map<String, Object> mapObj3 = (Map<String, Object>) o;

                    String key = String.valueOf(mapObj3.get('external_id'));
                    // Sert à vérifier si l'enregistrement a été envoyé pour savoir si on change Comptabilise__c en true ou non
                    //Remarque : Si une/plrs des lignes du JSON comporte une/des erreur(s) alors toutes les lignes ne sont pas envoyées
                    if (mapExtApi.containsKey(key)){
                        for (sofactoapp__External_Api__c extApi: mapExtApi.get(key)) {
                            if (!Errors.containsKey(extApi.Name))
                                Errors.put(extApi.Name, '');                        
                        }
                    }
                    
                    if (String.valueOf(mapObj3.get('field')) == 'compte_general') {
                        List<String> errorJSON = String.valueOf(mapObj3.get('Error')).split(' ');
                        String compte_general = '';
                        for (Integer i = 1 ;  i < errorJSON.size() - 1 ; i++) {
                            if (i != 1)
                                compte_general += ' ';
                            compte_general += errorJSON[i];
                        }
                        if (compte_general == '')
                            compte_general = null;
                        if (mapExtApi.containsKey(key)) {
                            for (sofactoapp__External_Api__c extApi: mapExtApi.get(key)) {
                                if (compte_general == extApi.compte_general__c) {
                                    Map<String, Object> mapError = new Map<String, Object>(mapObj3);
                                    mapError.put('external_id', (Object) extApi.Name);
                                    if (Errors.containsKey(extApi.Name) && Errors.get(extApi.Name) != '') {
                                        Errors.put(extApi.Name, Errors.get(extApi.Name) + ';' + String.valueOf((Object) mapError));
                                        List<Map<String, Object>> lstMapError = ErrorsObj.get(extApi.Name);
                                        lstMapError.add(mapError);
                                        ErrorsObj.put(extApi.Name, lstMapError);
                                    }
                                    else {
                                        Errors.put(extApi.Name, String.valueOf((Object) mapError));
                                        ErrorsObj.put(extApi.Name, new List<Map<String, Object>>{mapError});
                                    }
                                }
                            }
                        }
                    }
                    
                    else if (String.valueOf(mapObj3.get('field')) == 'sens') {
                        for (sofactoapp__External_Api__c extApi: mapExtApi.get(key)) {
                            if (extApi.sens__c == '') {
                                Map<String, Object> mapError = new Map<String, Object>(mapObj3);
                                mapError.put('external_id', (Object) extApi.Name);
                                if (Errors.containsKey(extApi.Name) && Errors.get(extApi.Name) != '') {
                                    Errors.put(extApi.Name, Errors.get(extApi.Name) + ';' + String.valueOf((Object) mapError));
                                    List<Map<String, Object>> lstMapError = ErrorsObj.get(extApi.Name);
                                    lstMapError.add(mapError);
                                    ErrorsObj.put(extApi.Name, lstMapError);
                                }
                                else {
                                    Errors.put(extApi.Name, String.valueOf((Object) mapError));
                                    ErrorsObj.put(extApi.Name, new List<Map<String, Object>>{mapError});
                                }
                            }
                        }
                    }

                    else {
                        if(mapExtApi.containsKey(key)){
                            for (sofactoapp__External_Api__c extApi: mapExtApi.get(key)) {
                            
                                Map<String, Object> mapError = new Map<String, Object>(mapObj3);
                                mapError.put('external_id', (Object) extApi.Name);
    
                                if (Errors.containsKey(extApi.Name) && Errors.get(extApi.Name) != '') {
                                    List<Map<String, Object>> lstMapError = ErrorsObj.get(extApi.Name);
                                    lstMapError.add(mapError);
                                    ErrorsObj.put(extApi.Name, lstMapError);
                                    Errors.put(extApi.Name, Errors.get(extApi.Name) + ';' + String.valueOf((Object) mapError));
                                }
                                else {
                                    Errors.put(extApi.Name, String.valueOf((Object) mapError));
                                    ErrorsObj.put(extApi.Name, new List<Map<String, Object>>{mapError});
                                }
                            }
                            
                        }
                    }
                    
                }
                
                List<sofactoapp__External_Api__c> lstAllExtApi = new List<sofactoapp__External_Api__c>();
                for (List<sofactoapp__External_Api__c> lstExtApi : mapExtApi.values()) {
                    lstAllExtApi.addAll(lstExtApi);
                }
                for (sofactoapp__External_Api__c extApi : lstAllExtApi) {
                    if(Errors.containsKey(extApi.Name)) {
                        HtmlBody += '<h4 style="padding-left:20px;">✓ '+ extApi.Name + '</h4>';
                        extApi.sofactoapp__Response__c = Errors.get(extApi.Name);
                        if (ErrorsObj.containsKey(extApi.Name)) {
                            if (ErrorsObj.get(extApi.Name).size() == 1) {
                                HtmlBody += '<p style="padding-left:40px;"> Erreur sur le champ <b>' + ErrorsObj.get(extApi.Name)[0].get('field') + '</b> du JSON : "'
                                                + ErrorsObj.get(extApi.Name)[0].get('Error') + '"</p>';
                            }
                            else {
                                for (Map<String, Object> mapError : ErrorsObj.get(extApi.Name)) {
                                    HtmlBody += '<p style="padding-left:40px;"> - Erreur sur le champ <b>' + mapError.get('field') + '</b> du JSON : "'
                                                    + mapError.get('Error') + '"</p>';
                                }
                            }
                            HtmlBody += '<br/>';
                        }
                    }
                    else {
                        extApi.sofactoapp__Response__c = '';
                        //extApi.Comptabilise__c = true;                                      //les enregistrements qui sont envoyés avec succès
                    }
                    lstExtApiToUpdate.add(extApi);
                }

                HtmlBody += '<hr/>';
            }
            
            else if (mapObj.get('code') == 2) {
                List<Object> lstObj = (List<Object>)mapObj.get('Errors');
                HtmlBody +=  '<h3>';
                if (Type_Ecriture == 'facture_liste')
                    HtmlBody +=  'Liste des factures';
        
                else if (Type_Ecriture == 'liste_reglements')
                    HtmlBody +=  'Liste des reglements';
        
                else if (Type_Ecriture == 'liste_remises')
                    HtmlBody +=  'Liste des remises';

                HtmlBody += '</h3>';
                for (Object o: lstObj) {
                    HtmlBody +=  '<p style="padding-left:20px;">' + String.valueOf(o) + '</p>';
                }
                HtmlBody += '<br/><hr/>';

            }

            if (mapObj.containsKey('Success')) {
                for (Object obj: (List<Object>)mapObj.get('Success')) {
                    Map<String, Object> mapObj2 = (Map<String, Object>) obj;
                    mapBoolSuccessToExtApiName.put(String.valueOf(mapObj2.get('external_id')), true);
                }
            }

            for (Integer i = 0; i < lstExtApiToUpdate.size(); i++) {
                if (mapExtApiNameLineToExtApiName.containsKey(lstExtApiToUpdate[i].Name)) {
                    String extApiName = mapExtApiNameLineToExtApiName.get(lstExtApiToUpdate[i].Name);
                    
                    if (mapBoolSuccessToExtApiName.containsKey(extApiName) && mapBoolSuccessToExtApiName.get(extApiName)) {
                        lstExtApiToUpdate[i].sofactoapp__Status_Code__c = String.valueOf(response.getStatusCode());
                        lstExtApiToUpdate[i].sofactoapp__Response__c = response.getBody();
                        lstExtApiToUpdate[i].Comptabilise__c = true;
                    }

                    lstExtApiToUpdate[i].DateEnvoi__c = dtNow;                                              //Date d'envoi de la requête
                }
            }

        }
        else {
            if (mapExtApi.keySet().size() > 0 ) {
                
                HtmlBody +=  '<h3>';
                if (Type_Ecriture == 'facture_liste')
                    HtmlBody +=  'Liste des factures';
        
                else if (Type_Ecriture == 'liste_reglements')
                    HtmlBody +=  'Liste des reglements';
        
                else if (Type_Ecriture == 'liste_remises')
                    HtmlBody +=  'Liste des remises';

                HtmlBody += '</h3>';

                for (String key: mapExtApi.keySet()) {
                    
                    for (sofactoapp__External_Api__c extApi: mapExtApi.get(key)) {
                        extApi.sofactoapp__Status_Code__c = String.valueOf(response.getStatusCode());
                        extApi.sofactoapp__Response__c = '';
                        extApi.sofactoapp__TechError__c = response.getBody();
                        extApi.DateEnvoi__c = dtNow;                                                        //Date d'envoi de la requête
                        lstExtApiToUpdate.add(extApi);
                        HtmlBody += '<h4 style="padding-left:20px;">✓ '+ extApi.Name + '</h4>';
                    }
                }

                
                HtmlBody +=  '<p style="padding-left:20px;">' + response.getBody() + '</p>';
                HtmlBody += '<br/><hr/>';


            }
        }

        WSTransactionLog__c log = new WSTransactionLog__c();
        log.DateTime__c = dtNow;
        log.Endpoint__c = endpoint;
        log.Request__c = body;
        log.StatusCode__c = String.valueOf(response.getStatusCode());
        log.Response__c = response.getBody();
        return log;
    }


    public static String value(String str) {
        if (str == null)
            return '';
        return str;
    }

    public static void sendMail(String subject, String HtmlBody) {
        
        List<Messaging.SingleEmailMessage> lstEmailToSend = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject(subject);
        mail.setHtmlBody(HtmlBody);
        List<String> Emails = System.label.WS14_Email.split(',');
        mail.setToAddresses(Emails);
        
        lstEmailToSend.add(mail);
        
        Messaging.sendEmail(lstEmailToSend);
    }

    public abstract class Facture {
        public Date datex{get;set;}

        public String external_id {get;set;}
        public String journal {get;set;}
        public String type_piece {get;set;}
        public String numero_piece {get;set;}
        public String societe {get;set;}
        public String libelle {get;set;}
        public String url {get;set;}
        public String devise {get;set;}
        public String mode_reglement {get;set;}
        public String num_contrat {get;set;}
        public String num_equipement {get;set;}

        public Tiers tiers {get;set;}
        public List<Ligne> lignes = new List<Ligne>();

        public facture() {
            
        }
    }

    public class Liste_Facture extends Facture {
        public Date date_echeance;

        public Liste_Facture(List<sofactoapp__External_Api__c> lstExtApi, String Type_Ecriture) {
            sofactoapp__External_Api__c extApi = lstExtApi[0];

            this.datex = extApi.date__c;
            this.date_echeance = extApi.date_echeance__c;

            this.external_id = value(extApi.Name);
            this.journal = value(extApi.Journal__c);
            this.type_piece = value(extApi.Type_piece__c);
            this.numero_piece = value(extApi.numero_piece__c);
            this.societe = value(extApi.Societe__c);
            this.libelle = value(extApi.libelle__c);
            this.url = value(extApi.URL__c);
            this.devise = value(extApi.devise__c);
            this.mode_reglement = value(extApi.mode_reglement__c);
            this.num_contrat = value(extApi.num_contrat__c);
            this.num_equipement = value(extApi.num_equipement__c);
            this.tiers = new Tiers(extApi);
            

            for(Integer i = 0; i < lstExtApi.size(); i++) {
                this.lignes.add(new Ligne(lstExtApi[i], Type_Ecriture));
            }
        } 
    }

    public class Liste_Reglement extends Facture {
        public Date date_PREVISIONREGLEMENT;

        public Liste_Reglement(List<sofactoapp__External_Api__c> lstExtApi, String Type_Ecriture) {
            sofactoapp__External_Api__c extApi = lstExtApi[0];

            this.datex = extApi.date__c;
            this.date_PREVISIONREGLEMENT = extApi.date_echeance__c;

            this.external_id = value(extApi.Name);
            this.journal = value(extApi.Journal__c);
            this.type_piece = value(extApi.Type_piece__c);
            this.numero_piece = value(extApi.numero_piece__c);
            this.societe = value(extApi.Societe__c);
            this.libelle = value(extApi.libelle__c);
            this.url = value(extApi.URL__c);
            this.devise = value(extApi.devise__c);
            this.mode_reglement = value(extApi.mode_reglement__c);
            this.num_contrat = value(extApi.num_contrat__c);
            this.num_equipement = value(extApi.num_equipement__c);
            this.tiers = new Tiers(extApi);

            for(Integer i = 0; i < lstExtApi.size(); i++) {
                this.lignes.add(new Ligne(lstExtApi[i], Type_Ecriture));
            }
        } 
    }
    
    public class Liste_Remise extends Facture {
        public Date date_REGLEMENT;

        public Liste_Remise(List<sofactoapp__External_Api__c> lstExtApi, String Type_Ecriture) {
            sofactoapp__External_Api__c extApi = lstExtApi[0];

            this.datex = extApi.date__c;
            this.date_REGLEMENT = extApi.date_echeance__c;

            this.external_id = value(extApi.Name);
            this.journal = value(extApi.Journal__c);
            this.type_piece = value(extApi.Type_piece__c);
            this.numero_piece = value(extApi.numero_piece__c);
            this.societe = value(extApi.Societe__c);
            this.libelle = value(extApi.libelle__c);
            this.url = value(extApi.URL__c);
            this.devise = value(extApi.devise__c);
            this.mode_reglement = value(extApi.mode_reglement__c);
            this.num_contrat = value(extApi.num_contrat__c);
            this.num_equipement = value(extApi.num_equipement__c);
            this.tiers = new Tiers(extApi);

            for(Integer i = 0; i < lstExtApi.size(); i++) {
                this.lignes.add(new Ligne_Remise(lstExtApi[i], Type_Ecriture));
            }
        } 
    }

    public class Tiers {
        public String prenom;
        public String nom;
        public String code_client;
        public String siret;
        public String raison_sociale;

        public Tiers (sofactoapp__External_Api__c extApi) {
            this.prenom = value(extApi.prenom__c);
            this.nom = value(extApi.nom__c);
            this.code_client = value(extApi.code_client__c);
            this.siret = value(extApi.SIRET__c);
            this.raison_sociale = value(extApi.raison_sociale__c);
        }
    }

    public virtual class Ligne {
        public String compte_general;
        public String type_ligne;
        public String analytique_etablissement;
        public String analytique_departement;
        public String analytique_client;
        public String analytique_produit;
        public String analytique_sous_produit;
        public String montant;
        public String sens;
        public String profil_tva;
        public String num_facture;

        public Ligne() {

        }

        public Ligne (sofactoapp__External_Api__c extApi, String Type_Ecriture) {

            this.compte_general = value(extApi.compte_general__c);
            this.type_ligne = value(extApi.type_ligne__c);
            this.analytique_etablissement = value(extApi.analytique_etablissement__c);
            this.analytique_departement = value(extApi.analytique_departement__c);
            this.analytique_client = value(extApi.analytique_client__c);
            this.analytique_produit = value(extApi.analytique_produit__c);
            this.analytique_sous_produit = value(extApi.Analytique_categorie_produit__c);
            this.montant = value(extApi.montant__c);
            this.sens = value(extApi.sens__c);
            this.profil_tva = value(extApi.profil_tva__c);
            this.num_facture = value(extApi.num_facture__c);
        } 
    }

    public class Ligne_Remise extends Ligne {
        public String libelle;

        public Ligne_Remise (sofactoapp__External_Api__c extApi, String Type_Ecriture) {
            this.libelle = value(extApi.Libelle_remise__c);
            this.compte_general = value(extApi.compte_general__c);
            this.type_ligne = value(extApi.type_ligne__c);
            this.analytique_etablissement = value(extApi.analytique_etablissement__c);
            this.analytique_departement = value(extApi.analytique_departement__c);
            this.analytique_client = value(extApi.analytique_client__c);
            this.analytique_produit = value(extApi.analytique_produit__c);
            this.analytique_sous_produit = value(extApi.Analytique_categorie_produit__c);
            this.montant = value(extApi.montant__c);
            this.sens = value(extApi.sens__c);
            this.profil_tva = value(extApi.profil_tva__c);
            this.num_facture = value(extApi.num_facture__c);
        }
    }

}