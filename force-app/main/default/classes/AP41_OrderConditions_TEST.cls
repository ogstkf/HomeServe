/**
 * @File Name          : AP41_OrderConditions_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    27/11/2019   AMO     Initial Version
**/
@isTest
public with sharing class AP41_OrderConditions_TEST {
 /**
 * @File Name          : AP41_OrderConditions_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (KZE)
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         13-11-2019               KZE         Initial Version
**/
static User mainUser;
    static List<Order> lstOrd = new List<Order>();
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
    static List<OrderItem> lstOrdItem = new List<OrderItem>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static OrderItem ordItem = new OrderItem();
    static List<Conditions__c> lstCond = new List<Conditions__c>();
    static  Bypass__c userBypass = new Bypass__c();


    static{
        mainUser = TestFactory.createAdminUser('AP23@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;
       userBypass.SetupOwnerId  = mainUser.Id;
        userBypass.BypassValidationRules__c = true;
        insert userBypass;


        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccountBusiness('AP23');
            // testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();

            insert testAcc;

             //create products
            prod = TestFactory.createProduct('testProd');
            insert prod;


            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            // System.debug('##### TEST lstPrcBk: '+lstPrcBk);

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;


            //create Order
            lstOrd = new List<Order>{
                                    new Order(AccountId = testAcc.Id
                                            ,Name = 'Test1'
                                            ,EffectiveDate = System.today()
                                            ,Status = AP_Constant.OrdStatusAchat
                                            ,Pricebook2Id = lstPrcBk[0].Id
                                            ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId()
                                            ,Type_de_livraison_de_la_commande__c  = ''
                                            ,Mode_de_transmission__c = ''
                                            ,Frais_de_port__c = 0
                                            ,Remise_mode__c = 5
                                    ),
                                    new Order(AccountId = testAcc.Id
                                            ,Name = 'Test2'
                                            ,EffectiveDate = System.today()
                                            ,Status = AP_Constant.OrdStatusAchat
                                            ,Pricebook2Id = lstPrcBk[0].Id
                                            ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId()
                                            ,Type_de_livraison_de_la_commande__c  = ''
                                            ,Mode_de_transmission__c = ''
                                            ,Frais_de_port__c = 0
                                    )
            };
            insert lstOrd;

            //create Order item
            lstOrdItem = new List<OrderItem>{
                                    new OrderItem(
                                        OrderId=lstOrd[0].Id,
                                        Quantity=decimal.valueof('1'),
                                        PricebookEntryId=PrcBkEnt.Id,
                                        Remise_mode_de_transmission__c = 2,
                                        UnitPrice = 145,
                                        Remise100__c = 10
                                        ),
                                        
                                    new OrderItem(
                                        OrderId=lstOrd[0].Id,
                                        Quantity=decimal.valueof('1'),
                                        PricebookEntryId=PrcBkEnt.Id,
                                        Remise_mode_de_transmission__c = 3,
                                        UnitPrice = 145),
                                    new OrderItem(
                                        OrderId=lstOrd[1].Id,
                                        Quantity=decimal.valueof('1'),
                                        PricebookEntryId=PrcBkEnt.Id,
                                        UnitPrice = 145)
            };        
            insert lstOrdItem;

            lstCond = new List<Conditions__c>{
                                    new Conditions__c(
                                            Compte__c = testAcc.Id,  
                                            Catalogue__c = lstPrcBk[0].Id ,                                       
                                            Delai__c = 4,
                                            Min__c = 0,
                                            Montant_maximum__c = 100,
                                            Frais_de_port__c = 12,
                                            Remise_Mode__c =0,
                                            Debut__c = System.today() -3,
                                            Fin__c = System.today() + 3,
                                            RecordTypeId= AP_Constant.getRecTypeId('Conditions__c', 'Header')                    
                                    ),
                                    new Conditions__c(
                                            Compte__c = testAcc.Id,  
                                            Catalogue__c = lstPrcBk[0].Id ,                                         
                                            Delai__c = 4,
                                            Min__c = 100,
                                            Montant_maximum__c = 200,
                                            Frais_de_port__c = 13,
                                            Remise_Mode__c =0,
                                            Debut__c = System.today() -3,
                                            Fin__c = System.today() + 3,
                                            RecordTypeId= AP_Constant.getRecTypeId('Conditions__c', 'Header')                    
                                    )
            };
            insert lstCond;           
        }
    }

    @isTest
    public static void testupdateType(){
        System.runAs(mainUser){
            Test.startTest();
                lstOrd[0].Type_de_livraison_de_la_commande__c = 'Express';
                lstOrd[1].Type_de_livraison_de_la_commande__c = 'Express';
                update lstOrd;
            Test.stopTest(); 

            list<Order> lstNewOrder = [ SELECT Status, EffectiveDate, EndDate, Frais_de_port__c FROM Order
                                            WHERE Id = :lstOrd[0].Id ] ;       
            // System.assertEquals(lstNewOrder[0].EndDate,  AP41_OrderConditions.getEndDate(lstNewOrder[0].EffectiveDate, Integer.valueOf(lstCond[1].Delai__c)) );     
            // System.assertEquals(lstNewOrder[0].Frais_de_port__c, lstCond[1].Frais_de_port__c );
        }
    }
    @isTest
    public static void testupdateStatus(){
        System.runAs(mainUser){
            Test.startTest();
                lstOrd[0].Status = AP_Constant.ordStatusCommandeEmise;
                lstOrd[1].Status = AP_Constant.ordStatusCommandeEmise;
                update lstOrd;
            Test.stopTest();

            list<Order> lstNewOrder = [ SELECT Status, Date_de_livraison_prevue__c, EffectiveDate, EndDate, Frais_de_port__c FROM Order
                                            WHERE Id = :lstOrd[0].Id ] ;       
            // System.assertEquals(lstNewOrder[0].Date_de_livraison_prevue__c, AP41_OrderConditions.getEndDate(System.today(), Integer.valueOf(lstCond[1].Delai__c)) );
        }
    }

    @isTest
    public static void testUpdateRemise(){
         System.runAs(mainUser){
            lstOrd[0].Remise_mode__c = 7;

            Test.startTest();
                update lstOrd[0];
            Test.stopTest();

            List<OrderItem> lstOrdItem = [SELECT Id, OrderId, Remise_mode_de_transmission__c FROM OrderItem WHERE OrderId =:lstOrd[0].Id];
            System.assertEquals(lstOrd[0].Remise_mode__c, 7);
         }

    }

    @isTest
    public static void testUpdateUnitPrice(){
         System.runAs(mainUser){
            lstOrdItem[0].Remise_mode_de_transmission__c = 5;

            Test.startTest();
                update lstOrdItem[0];
            Test.stopTest();

            List<OrderItem> lstOrdItems = [SELECT Id, OrderId, UnitPrice, Remise_mode_de_transmission__c FROM OrderItem WHERE Id  =:lstOrdItem[0].Id];
            System.assertEquals(lstOrdItem[0].UnitPrice, 145);
         }

    }

    @isTest
    public static void testInsertOrderProduct(){
         System.runAs(mainUser){

            Test.startTest();
                update lstOrdItem[0];
            Test.stopTest();

            List<OrderItem> lstOrdItems = [SELECT Id, OrderId, UnitPrice, Remise_mode_de_transmission__c FROM OrderItem WHERE Id  =:lstOrdItem[0].Id];
            System.assertEquals(lstOrdItem[0].Remise100__c, 10);
         }

    }
}