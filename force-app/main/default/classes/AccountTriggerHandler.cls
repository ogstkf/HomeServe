/**
 * @File Name          : AccountTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 15/10/2019, 14:43:57
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    15/10/2019   AMO     Initial Version
 * 1.1    19/06/2020   DMU     Commented AP20_NewAccountNewLocation due to homeserve project
**/
public with sharing class AccountTriggerHandler {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Class to update Logement__c.Owner__c with Account created on
-- -                 creation of Logement__c
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 28-JAN-2019  DMU    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/

    Bypass__c userBypass;

    public AccountTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void UpdLogement(List<Account> lstNewAcc){
        system.debug('### In UpdLogement');

        map<Id, Id> mapAccDetailLogementId = new map <Id, Id>();
        for(Account acc : lstNewAcc){
            mapAccDetailLogementId.put(acc.Id, acc.TECH_LogementId__c);
        }

        list<Logement__c> logementListToUpd = new list<Logement__c> ();     

        for(Account acc : lstNewAcc){
            if(string.IsNotBlank(acc.TECH_LogementId__c)){
                Logement__c lo = new Logement__c (id=mapAccDetailLogementId.get(acc.Id), Owner__c=acc.Id);
                logementListToUpd.add(lo);
            }
        }

        try{
            update logementListToUpd;
        }catch(Exception e){
            system.debug('### update fail: '+e.getMessage());
        }
    }
    
    public void handleAfterInsert(List<Account> lstNewAccs){
        List<Account> lstAccountAddLocation = new List<Account>();

        for(integer i=0; i<lstNewAccs.size(); i++){
            if(lstNewAccs[i].recordTypeId == AP_Constant.getRecTypeId('Account', 'Fournisseurs')){
              lstAccountAddLocation.add(lstNewAccs[i]);
            }
                
        }
        
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP20')){
          if(lstNewAccs.size()>0){
             AP20_NewAccountNewLocation.AccountLocation(lstAccountAddLocation);
         }
            
        }
            
    }
}