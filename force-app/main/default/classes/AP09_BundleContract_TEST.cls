/**
 * @File Name          : AP09_BundleContract_TEST.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    30/07/2019, 19:23:31   RRJ     Initial Version
**/
@isTest
public class AP09_BundleContract_TEST {
    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<Logement__c> lstTestLog = new List<Logement__c>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<Bundle__c> lstTestBundle = new List<Bundle__c>();
    static List<Product_Bundle__c> lstTestBundleProd = new List<Product_Bundle__c>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();
    //bypass
    static Bypass__c bp = new Bypass__c();


    static {
        adminUser = TestFactory.createAdminUser('AP09_BundleContract_TEST@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;
        
        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;


        System.runAs(adminUser){
            Integer parentNum = 1;
            Integer childNum = 5;


            //create accounts
            for(Integer i=0; i<parentNum; i++){
                lstTestAcc.add(TestFactory.createAccount('testAcc'+1));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
                lstTestAcc[i].BillingPostalCode = '1234'+i;
            }
            insert lstTestAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc[0].Id);

            lstTestAcc[0].sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update lstTestAcc;

            // System.debug('##### TEST lstTestAcc: '+lstTestAcc);

            //create logements
            // for(integer i =0; i<lstTestAcc.size(); i++){
            //     lstTestLogement.add(TestFactory.createLogement(lstTestAcc[i].Id, lstTestAgency[i].Id, i));
            //     lstTestLogement[i].RecordTypeId = AP_Constant.getRecTypeId('Logement__c', 'Logement');
            // }
            // insert lstTestLogement;

            //create products
            for(Integer i=0; i<parentNum*childNum; i++){
                lstTestProd.add(TestFactory.createProduct('testProd'+i));
                if(Math.mod(i, 5)== 0){
                    lstTestProd[i].IsBundle__c = false;
                    lstTestProd[i].Name = 'testBundleProd'+i/5;
                }
            }

            insert lstTestProd;
            // System.debug('##### TEST lstTestProd: '+lstTestProd);

            //create bundles
            for(Integer i=0; i<parentNum; i++){
                lstTestBundle.add(TestFactory.createBundle('testBundle'+i, 150));
            }

            insert lstTestBundle;
            // System.debug('##### TEST lstTestBundle: '+lstTestBundle);

            //create bundle products related to bundle and product
            for(Integer i=0; i<parentNum*childNum; i++){
                lstTestBundleProd.add(TestFactory.createBundleProduct(lstTestBundle[i/5].Id, lstTestProd[i].Id));
            }

            lstTestBundleProd[0].Bundle__c = lstTestBundle[0].Id;
            lstTestBundleProd[0].Optional_Item__c = false;
            insert lstTestBundleProd;
            // System.debug('##### TEST lstTestBundleProd: '+lstTestBundleProd);

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            // System.debug('##### TEST lstPrcBk: '+lstPrcBk);

            //pricebook entry
            for(Integer i=0; i<parentNum*childNum; i++){
                lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[i].Id, 0));
                if(lstTestProd[i].IsBundle__c){
                    lstPrcBkEnt[i].UnitPrice = 150;
                }
            }

            insert lstPrcBkEnt;
            // System.debug('##### TEST lstPrcBkEnt: '+lstPrcBkEnt);

            //service contract
            for(Integer i=0; i<parentNum; i++){
                lstServCon.add(TestFactory.createServiceContract('testServCon'+i, lstTestAcc[i].Id));
                lstServCon[i].PriceBook2Id = lstPrcBk[0].Id;
                lstServCon[i].EndDate = Date.Today().addYears(1);
                lstServCon[i].StartDate = Date.Today();
            }

            insert lstServCon;
            // System.debug('##### TEST lstServCon: '+lstServCon);
            
            //contract line item
            for(Integer i=0; i<parentNum*childNum; i++){
                lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[i/childNum].Id, lstPrcBkEnt[i].Id, 150, 1));
            }
            // insert lstConLnItem;
        }
    }

    @isTest
    static void testInsertContractLines(){
        System.runAs(adminUser){
            Test.startTest();
            insert lstConLnItem;
            Test.stopTest();
            //assert tbd
        }
    }

    // @isTest
    // static void testDeleteContractLines(){
    //     System.runAs(adminUser){
    //         insert lstConLnItem;
    //         Test.startTest();
    //         delete lstConLnItem;
    //         Test.stopTest();
    //     }
    // }
}