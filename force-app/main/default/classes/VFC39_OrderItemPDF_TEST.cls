/**
 * @File Name          : VFC39_OrderItemPDF_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 26-05-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         25-11-2019     		KZE         Initial Version
**/
@isTest
public with sharing class VFC39_OrderItemPDF_TEST {
    static User mainUser;
    static Order ord ;
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
	static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
	static PricebookEntry PrcBkEnt = new PricebookEntry();
    static  Bypass__c userBypass;
    static OrderItem ordItem = new OrderItem();
    

    static{
        mainUser = TestFactory.createAdminUser('AP39@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            //create account
            testAcc = new Account(Name='Test Acc');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();

            insert testAcc;

            //create products
            prod = TestFactory.createProduct('testProd');
            insert prod;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;

            //create Order
            ord = new Order(AccountId = testAcc.Id
                                    ,Name = 'Test1'
                                    ,EffectiveDate = System.today()
                                    ,Status = AP_Constant.OrdStatusAchat
                                    ,Pricebook2Id = lstPrcBk[0].Id
                                    ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId());

            insert ord;

            ordItem = new OrderItem(
				            OrderId=ord.Id,
				            Quantity=decimal.valueof('1'),
				            PricebookEntryId=PrcBkEnt.Id,
				            UnitPrice = 145);
        
            insert ordItem;

        }
    }

    public static testMethod void testSaveVF(){
        System.runAs(mainUser){
            Test.startTest();
              VFC39_OrderItemPDF.saveVF(ord.id);
             Test.stopTest(); 
        }
    }

    public static testMethod void testPdfGeneratorFail() {

        PageReference pref = Page.VFP39_OrderItemPDF;
        pref.getParameters().put('id',ord.id);
        Test.setCurrentPage(pref);
            
        Test.startTest();
            Map<string,Object> results =  VFC39_OrderItemPDF.checkIfActive(ord.id);
            // assert that an attachment was not saved for the record
            System.assertNotEquals(1,[select count() from ContentDocumentLink where LinkedEntityId  = :ord.id]);
        Test.stopTest(); 

    }
    public static testMethod void testPdfGenerator() { // To rework by selecting files
        ord.StatusCode = 'Activated';
        ord.Status = AP_Constant.OrdStatusSolde;
        update ord;

        PageReference pref = Page.VFP39_OrderItemPDF;
        pref.getParameters().put('id',ord.id);
        Test.setCurrentPage(pref);    
            
        Test.startTest();
            Map<string,Object> results =  VFC39_OrderItemPDF.checkIfActive(ord.id);
            // assert that an attachment was saved for the record
            System.assertEquals(1,[select count() from ContentDocumentLink where LinkedEntityId = :ord.id]);
        Test.stopTest(); 

    }

    public static testMethod void testgetORderInfo() {
        PageReference pref = Page.VFP39_OrderItemPDF;
        pref.getParameters().put('id',ord.id);
        Test.setCurrentPage(pref);   
               
        Test.startTest();         
            ApexPages.StandardController sc = new ApexPages.StandardController(ord);         
            VFC39_OrderItemPDF con = new VFC39_OrderItemPDF(sc); 
            // System.assertEquals( con.theOrder.EffectiveDate , ord.EffectiveDate);
        Test.stopTest(); 

    }
}