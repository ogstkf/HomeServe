/**
 * @description       : 
 * @author            : ANR
 * @group             : 
 * @last modified on  : 04-07-2021
 * @last modified by  : ANR
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   04-05-2021   ANR   Initial Version
**/
@isTest
public with sharing class WS13_ChaineEditique_TEST {

    static User mainUser;
    static Account testAcc;
    static Account testBusinessAccount;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<Quote> lstQuote = new List<Quote>();
    static List<QuoteLineItem> lstQuoteLineItem = new List<QuoteLineItem>();
    static Product2 lstTestProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PriceBookEntry> lstPrcBkEnt = new List<PriceBookEntry>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static OperatingHours opHrs = new OperatingHours();
    static List<ServiceTerritory> lstSrvTerr = new List<ServiceTerritory>();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static List<sofactoapp__Factures_Client__c> lstFacturesClients = new List<sofactoapp__Factures_Client__c>();
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Case> lstCase = new List<Case>();
    static List<Asset> lstTestAssetChild = new List<Asset>();
    static Bypass__c bp = new Bypass__c();
    static List<Contact> lstContacts;
    static List<ContentVersion> lstConVer = new List<ContentVersion>();
    static list<ContentDocument> lstConDoc = new List<ContentDocument>();
    static List<ContentDocumentLink> lstConDocLink;

    static{

        mainUser = TestFactory.createAdminUser('WS14@test.com', TestFactory.getProfileAdminId());
        insert mainUser;
        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        insert bp;

        System.runAs(mainUser){

            //creating account
            testAcc = TestFactory.createAccount('WS13_ChaineEditique_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            testBusinessAccount = TestFactory.createAccountBusiness('WS13_ChaineEditique_TEST2');
            insert testBusinessAccount;

            //Create Contacts
            lstContacts = new List<Contact>{
                new Contact(LastName = 'Test Con 1',AccountId = testBusinessAccount.Id,Email = 'adarsh@spoon.com'),
                new Contact(LastName = 'Test Con 2',AccountId = testBusinessAccount.Id,Email = 'testAnr@gmail.com')
            };
            insert lstContacts;

            //Create PriceBook
            Id pricebookId = Test.getStandardPricebookId();  
            

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;

            //create service territory
            lstSrvTerr.add(TestFactory.createServiceTerritory('SBF Energies1',opHrs.Id, raisonSocial.Id));
            lstSrvTerr.add(TestFactory.createServiceTerritory('ID Energies1',opHrs.Id, raisonSocial.Id));
            lstSrvTerr.add(TestFactory.createServiceTerritory('Electrogaz1',opHrs.Id, raisonSocial.Id));

            lstSrvTerr[0].Agency_Code__c = '1234';
            lstSrvTerr[0].Corporate_Street__c = '12349';
            lstSrvTerr[0].Corporate_Street2__c = '12348';
            lstSrvTerr[0].Corporate_ZipCode__c = '12345';
            lstSrvTerr[0].Corporate_City__c = 'Paris';
            lstSrvTerr[0].Phone__c  = '123456789';
            lstSrvTerr[0].Email__c = 'contact@sbf-energies.com';
            lstSrvTerr[0].site_web__c = 'www.google.com';
            lstSrvTerr[0].IBAN__c = 'FR1023456213456';
            lstSrvTerr[0].Libelle_horaires__c = '1234';
            lstSrvTerr[0].horaires_astreinte__c = '1234';

            lstSrvTerr[1].Agency_Code__c = '3322';
            lstSrvTerr[1].Corporate_Street__c = '12349';
            lstSrvTerr[1].Corporate_Street2__c = '12348';
            lstSrvTerr[1].Corporate_ZipCode__c = '12345';
            lstSrvTerr[1].Corporate_City__c = 'Paris';
            lstSrvTerr[1].Phone__c  = '123456789';
            lstSrvTerr[1].Email__c = 'contact@sbf-energies.com';
            lstSrvTerr[1].site_web__c = 'www.google.com';
            lstSrvTerr[1].IBAN__c = 'FR1123456213456';
            lstSrvTerr[1].Libelle_horaires__c = '1234';
            lstSrvTerr[1].horaires_astreinte__c = '1234';

            lstSrvTerr[2].Agency_Code__c = '1144';
            lstSrvTerr[2].Corporate_Street__c = 'qdqw';
            lstSrvTerr[2].Corporate_Street2__c = '123qdwq48';
            lstSrvTerr[2].Corporate_ZipCode__c = '12345';
            lstSrvTerr[2].Corporate_City__c = 'Paris';
            lstSrvTerr[2].Phone__c  = '123456789';
            lstSrvTerr[2].Email__c = 'contact@sbf-energies.com';
            lstSrvTerr[2].site_web__c = 'www.google.com';
            lstSrvTerr[2].IBAN__c = 'FR1223456213456';
            lstSrvTerr[2].Libelle_horaires__c = '1234';
            lstSrvTerr[2].horaires_astreinte__c = '1234';
            insert lstSrvTerr;

            raisonSocial.sofactoapp_Agence__c = lstSrvTerr[0].Id;
            raisonSocial.sofactoapp__Email__c = 'contact@sbf-energies.com';
            update raisonSocial;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[0].Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[1].Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[2].Id));
            lstLogement[2].Postal_Code__c = 'lgmtPC 3';
            lstLogement[2].City__c = 'lgmtCity 3';
            lstLogement[2].Account__c = testAcc.Id;
            insert lstLogement;

            
            lstTestProd  = new Product2(Name = 'Product X1',
                ProductCode = 'MXZ-4F80VF3',
                isActive = true,
                Equipment_family__c = 'Pompe à chaleur',
                Statut__c   = 'Approuvée',
                Equipment_type__c = 'Pompe à chaleur air/eau'
            );

            upsert lstTestProd;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd.Id, 0));

            insert lstPrcBkEnt; 

            // creating Assets
            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lstLogement[0].Id);
            // eqp.Product2Id = lstTestProd.Id;
            eqp.Logement__c = lstLogement[0].Id;
            eqp.AccountId = testAcc.Id;
            lstAsset.add(eqp);
            insert lstAsset;

             // creating Assets child
            Asset eqpEnf = TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id);
            // eqpEnf.Product2Id = lstTestProd.Id;
            eqpEnf.Logement__c = lstLogement[0].Id;
            eqpEnf.AccountId = testacc.Id;
            eqpEnf.parentId = lstAsset[0].Id;
            lstTestAssetChild.add(eqpEnf);
           
            insert lstTestAssetChild;

            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon[0].Type__c = 'Individual';
            lstServCon[0].Agency__c = lstSrvTerr[0].Id;
            lstServCon[0].Asset__c = lstAsset[0].Id; //asset 0 with product equipment type Chaudière fioul
            lstServCon[0].PriceBook2Id = pricebookId;
            lstServCon[0].EndDate = Date.Today().addYears(1);
            lstServCon[0].StartDate = Date.Today();
            lstServCon[0].RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives') ;
            lstServCon[0].Type__c = 'Collective';
            lstServCon[0].Payeur_du_contrat__c = testAcc.Id;
            lstServCon[0].pdfGenerer__c = 'CONTRAT_TCH';

            insert lstServCon;

            

            // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'Maintenance', lstAsset[0].Id));
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase[0].Origin = 'Cham Digital';
            lstCase[0].Reason__c = 'Visite sous contrat';
            lstCase[0].RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            insert lstCase;



            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Maintenance';
            lstWrkTyp[0].Type_de_client__c = 'Tous';
            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());       
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].AssetId = lstAsset[0].Id;
            lstWrkOrd[0].CaseId = lstCase[0].Id;
            lstWrkOrd[0].AccountId = testAcc.Id;

            insert lstWrkOrd;

             // creating service appointment
             lstServiceApp = new List<ServiceAppointment>{
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Service_Contract__c = lstServCon[0].Id,
                   Work_Order__c = lstWrkOrd[0].Id,
                   Residence__c =   lstLogement[0].Id,
                   pdfGenerer__c = 'INTER_TCH',
                   TransactionId__c = '123456789'                
                   )
            };

            insert lstServiceApp;     

            // // creating Assets
            // lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            // lstAsset[0].Product2Id = lstTestProd.Id;
            // lstAsset[0].Logement__c = lstLogement[0].Id;
            // lstAsset[0].AccountId = testAcc.Id;
            // lstAsset.add(TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstLogement[1].Id));
            // lstAsset[1].Product2Id = lstTestProd.Id;
            // lstAsset[1].Logement__c = lstLogement[1].Id;
            // lstAsset[1].AccountId = testAcc.Id;
            // lstAsset.add(TestFactory.createAccount('equipement 3', AP_Constant.assetStatusActif, lstLogement[2].Id));
            // lstAsset[2].Product2Id = lstTestProd.Id;
            // lstAsset[2].Logement__c = lstLogement[2].Id;
            // lstAsset[2].AccountId = testAcc.Id;
            // insert lstAsset;

            // // create case
            // lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[0].Id));
            // lstCase[0].Service_Contract__c = lstServCon[0].Id;
            // lstCase[0].type = 'Commissioning';
            // lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[1].Id));
            // lstCase[1].Service_Contract__c = lstServCon[1].Id;
            // lstCase[1].type = 'Commissioning';
            // lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[2].Id));
            // lstCase[2].Service_Contract__c = lstServCon[1].Id;
            // lstCase[2].type = 'Commissioning';
            // insert lstCase;

            // // creating work type
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            // lstWrkTyp[0].Type__c = 'Commissioning';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType2', 'Hours', 1));
            // lstWrkTyp[1].Type__c = 'Information Request';
            // lstWrkTyp[1].Reason__c = 'Commercial Visit';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType3', 'Hours', 1));
            // lstWrkTyp[2].Type__c = 'Information Request';
            // lstWrkTyp[2].Reason__c = 'Commercial Visit';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType4', 'Hours', 1));
            // lstWrkTyp[3].Type__c = 'Installation';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType5', 'Hours', 1));
            // lstWrkTyp[4].Type__c = 'Maintenance';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType6', 'Hours', 1));
            // lstWrkTyp[5].Type__c = 'Maintenance';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType7', 'Hours', 1));
            // lstWrkTyp[6].Type__c = 'Troubleshooting';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType8', 'Hours', 1));
            // lstWrkTyp[7].Type__c = 'Various Services';
            // lstWrkTyp.add(TestFactory.createWorkType('wrkType8', 'Hours', 1));
            // lstWrkTyp[8].Type__c = 'Various Services';

            // insert lstWrkTyp;

            // // creating work order
            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            // lstWrkOrd[0].CaseId = lstCase[0].Id;

            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[1].WorkTypeId = lstWrkTyp[1].Id;
            // lstWrkOrd[1].CaseId = lstCase[1].Id;

            // lstWrkOrd.add(TestFactory.createWorkOrder());
            // lstWrkOrd[2].WorkTypeId = lstWrkTyp[2].Id;
            // lstWrkOrd[2].CaseId = lstCase[2].Id;

            // insert lstWrkOrd;

            // // creating service appointment
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[0].Id, lstSrvTerr[0].Id));
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[1].Id, lstSrvTerr[1].Id));
            // lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[2].Id, lstSrvTerr[2].Id));         
            // insert lstServiceApp;



        //create opportunity
            lstOpp =  new List<Opportunity>{ 
                TestFactory.createOpportunity('Test1',testAcc.Id) ,
                TestFactory.createOpportunity('Test2',testAcc.Id) 
            };

        insert lstOpp;

        //Create Quotes
            lstQuote = new List<Quote> {
                new Quote(  Name ='Prestations'
                            ,Pricebook2Id = pricebookId
                            ,OpportunityId = lstOpp[0].Id
                            ,Devis_signe_par_le_client__c = false
                            ,Status = 'In Review'
                            ,Date_de_debut_des_travaux__c = System.today()
                            ,tech_deja_facture__c = true
                            ,Mode_de_paiement_de_l_acompte__c = 'CB'
                            ,Montant_de_l_acompte_verse__c=500
                            ,Logement__c = lstLogement[0].Id
                            ,Agency__c = lstSrvTerr[0].Id
                            ,Nom_du_payeur__c = testAcc.Id
                            ,TransactionId__c = '123456789'
                            ),
            new Quote(  Name ='Travaux'
                            ,Pricebook2Id = pricebookId
                            ,OpportunityId = lstOpp[0].Id
                            ,Devis_signe_par_le_client__c = false
                            ,Status = 'Needs Review'
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                            ,Logement__c = lstLogement[0].Id
                            ,Agency__c = lstSrvTerr[0].Id
                            ), 
                new Quote(  Name ='Remplacement'
                            ,Devis_signe_par_le_client__c = false
                            ,OpportunityId = lstOpp[1].Id
                            ,Pricebook2Id = pricebookId
                            ,Status = 'Draft'
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                            ,Logement__c = lstLogement[0].Id
                            ,Agency__c = lstSrvTerr[0].Id
                            ),
                new Quote(  Name ='Sous-traitance totale' 
                            ,Devis_signe_par_le_client__c = false
                            ,OpportunityId = lstOpp[1].Id
                            ,Pricebook2Id = pricebookId
                            ,Status = 'Denied'
                            //,Type_de_devis__c = 'Vente de pièces/équipements au guichet'    
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                            ,Agency__c = lstSrvTerr[0].Id
                        )
            };
            insert lstQuote;    

            //create QuoteLineItem
            lstQuoteLineItem =  new List<QuoteLineItem>{
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd.Id,
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd.Id,
                                    QuoteId = lstQuote[1].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstTestProd.Id,
                                    QuoteId = lstQuote[2].Id, 
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                )
                };
            insert lstQuoteLineItem;


            lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                Sofactoapp_Contrat_de_service__c = lstServCon[0].Id
                , sofactoapp__Compte__c = testAcc.Id
                , sofactoapp__emetteur_facture__c = raisonSocial.Id
                , SofactoappType_Prestation__c = 'Contrat individuel'
                ,sofactoapp__Compte2__c = testAcc.Id
                ,TransactionId__c = '123456789'
            ));
            
            lstFacturesClients.add(new sofactoapp__Factures_Client__c(
                 sofactoapp__Compte__c = testAcc.Id
                , sofactoapp__emetteur_facture__c = raisonSocial.Id
                , SofactoappType_Prestation__c = 'Contrat individuel'
                ,sofacto_devis__c = lstQuote[0].Id
                ,sofactoapp__Compte2__c = testAcc.Id
            ));

            insert lstFacturesClients;


            lstConVer.add(
                new ContentVersion(
                    Title = 'Penguins',
                    PathOnClient = 'Penguins.jpg',
                    VersionData = Blob.valueOf('Test Content'),
                    IsMajorVersion = true
                    )
            );
            insert lstConVer;    
            lstConDoc = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

            //create ContentDocumentLink  record 

            lstConDocLink = new List<ContentDocumentLink>{
                new ContentDocumentLink(
                LinkedEntityId = lstQuote[0].id,
                ContentDocumentId = lstConDoc[0].Id,
                shareType = 'V'),
                new ContentDocumentLink(
                LinkedEntityId = lstServCon[0].id,
                ContentDocumentId = lstConDoc[0].Id,
                shareType = 'V'),
                new ContentDocumentLink(
                LinkedEntityId = lstFacturesClients[0].id,
                ContentDocumentId = lstConDoc[0].Id,
                shareType = 'V')
            };
            insert lstConDocLink;
        }

    }


    @isTest
    static void testcreateJSON(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
                WS13_ChaineEditique.createJSON(lstServiceApp[0].Id,true,true,true,null,null,false);
            Test.stopTest();
        }
        
    }
    @isTest
    static void testcreateJSON2(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
                Test.startTest();
                WS13_ChaineEditique.createJSON(lstQuote[0].Id,true,true,true,null,null,false);
            Test.stopTest();
        }
        
    }

    @isTest
    static void testcreateJSON3(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
                WS13_ChaineEditique.createJSON(lstFacturesClients[0].Id,true,true,true,null,null,false);
            Test.stopTest();
        }
        
    }

    @isTest
    static void testcreateJSON4(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
                WS13_ChaineEditique.createJSON(lstServCon[0].Id,true,true,true,null,null,false);
            Test.stopTest();
        }
        
    }

    @isTest
    static void testcreateJSON5(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
                WS13_ChaineEditique.acheminer(lstServiceApp[0].Id);
            Test.stopTest();
        }
        
    }

    @isTest
    static void testcreateJSON7(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
                WS13_ChaineEditique.createJSON(lstServCon[0].Id,true,true,false,null,null,false);
            Test.stopTest();
        }
        
    }

    @isTest
    static void testGetPicklistValue(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
                WS13_ChaineEditique.createJSON(lstFacturesClients[1].Id,true,true,true,null,null,false);
                WS13_ChaineEditique.pickListvalue(lstQuote[0].Id);
                WS13_ChaineEditique.pickListvalue(lstFacturesClients[0].Id);
                WS13_ChaineEditique.pickListvalue(lstServCon[0].Id);
                
            Test.stopTest();
        }
        
    }

    @isTest
    static void testSendEmail(){
        System.runAs(mainUser){
            Test.startTest();
            List<String> lstEmail = new List<String>{'adarsh@spoon.com'};

                 WS13_ChaineEditique.sendMail(lstQuote[0].Id,lstEmail);
                 WS13_ChaineEditique.sendMail(lstFacturesClients[0].Id,lstEmail);
                 WS13_ChaineEditique.sendMail(lstServCon[0].Id,lstEmail);
            Test.stopTest();
        }
        
    }

    @isTest
    static void testGenerateRandomKey(){
        System.runAs(mainUser){
            Test.startTest();
                WS13_ChaineEditique.generateRandomKey();
            Test.stopTest();
        }
        
    }

     public class calloutStatusPrinted implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"printed"}');
            response.setStatusCode(200);
            return response; 
        }
    }

    public class calloutStatusEmitted implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"emitted"}');
            response.setStatusCode(200);
            return response; 
        }
    }

    public class calloutStatuserrorprinting implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"error-printing"}');
            response.setStatusCode(200);
            return response; 
        }
    }

    public class calloutStatuserroremtting implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"error-emitting"}');
            response.setStatusCode(200);
            return response; 
        }
    }

    

}