/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 22-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   10-03-2022   MNA   Initial Version
**/
public with sharing class WS20_SlimPayBulk {
    public static void payInBulk() {
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse response;
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('ftp-host', 'test.rebex.net');
        req.setHeader('ftp-type', 'SFTP');
        req.setHeader('username', 'demo');
        req.setHeader('password', 'password');
        req.setHeader('port', '22');

        req.setEndpoint('https://www.ftp-api.com/ftp/checkconnection');

        req.setMethod('GET');

        response = http.send(req);
        System.debug(response.getStatusCode());
        System.debug(response.getBody());
    }

    
    public static void payInBulk2() {
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse response;
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('ftp-host', 'sftp.preprod.slimpay.com');
        req.setHeader('ftp-type', 'SFTP');
        req.setHeader('username', 'vbgaz01');
        req.setHeader('password', '');
        req.setHeader('port', '6321');

        req.setEndpoint('https://www.ftp-api.com/ftp/checkconnection');

        req.setMethod('GET');

        response = http.send(req);
        System.debug(response.getStatusCode());
        System.debug(response.getBody());
    }
}
