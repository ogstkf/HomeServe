/**
 * @File Name          : AP29_UpdateStatusSA_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-05-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    03/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class AP29_UpdateStatusSA_TEST {
/**
 * @File Name          : AP29_UpdateStatusSA_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-05-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         21-10-2019     		    KZE         Initial Version
**/
static User mainUser;
    static Order ord ;
    static Account testAcc = new Account();
    static list <ServiceAppointment> sappLst;
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static Product2 lstTestProd = new Product2();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstConLnItem = new List<ContractLineItem>();    
    static List<Case> lstCase= new List<Case>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static Bypass__c bp = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('AP24@test.COM', 
                                                TestFactory.getProfileAdminId());
        
        insert mainUser;
        
        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(mainUser){
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;   

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
			testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //create products            
            Product2 lstTestProd = new Product2(Name = 'Product X',
                                    ProductCode = 'Pro-X',
                                    Statut__c = 'Approuvée',
                                    isActive = true,
                                    Equipment_family__c = 'Chaudière',
                                        Equipment_type__c = 'Chaudière gaz'
            );
            insert lstTestProd;
           
            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            // lstPrcBk.add(standardPricebook);
            // update lstPrcBk;
           
            //pricebook entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd.Id, 0));          
            insert lstPrcBkEnt;
            
            //service contract           
            lstServCon.add(TestFactory.createServiceContract('testServCon', testAcc.Id));
            lstServCon[0].PriceBook2Id = standardPricebook.Id;          
            insert lstServCon;   
                        
            //contract line item
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));      
            lstConLnItem[0].Customer_Absences__c = 1;
            lstConLnItem[0].Customer_Allowed_Absences__c = 3;
            lstConLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPrcBkEnt[0].Id, 150, 1));      
            insert lstConLnItem;            

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            //create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa;

            //create service territory
            srvTerr = TestFactory.createServiceTerritory('test Territory',opHrs.Id, sofa.Id);
            srvTerr.Sofactoapp_Raison_Social__c =sofa.Id;            
            insert srvTerr;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            insert lstLogement;

            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstTestProd.Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = testAcc.Id;
            insert lstAsset;     

             // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'Troubleshooting', lstAsset[0].Id));
            lstCase[0].Contract_Line_Item__c = lstConLnItem[0].Id;
            lstCase[0].recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            // lstCase[0].Reason__c ='Desembouage circuit radiateur';
            lstCase.add(TestFactory.createCase(testAcc.Id, 'Troubleshooting', lstAsset[0].Id));
            lstCase[1].recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_case').getRecordTypeId();
            lstCase[1].Contract_Line_Item__c = lstConLnItem[1].Id;
            // lstCase[1].Reason__c ='Desembouage circuit radiateur';
            insert lstCase;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Commissioning';

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].caseId = lstCase[0].Id;  
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].Type__c = 'Maintenance';
            lstWrkOrd[0].Status = 'In Progress';
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[1].caseId = lstCase[1].Id;  
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[1].Type__c = 'Installation';
            lstWrkOrd[1].Status = 'In Progress';
            insert lstWrkOrd;     

            sappLst = new List<ServiceAppointment>{
                                    new ServiceAppointment(
                                            Status = 'Cancelled',                                            
                                            ParentRecordId = lstWrkOrd[0].Id,                                         
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Work_Order__c = lstWrkOrd[0].Id                       
                                    ),
                                    new ServiceAppointment(
                                            Status = 'Cancelled',                                            
                                            ParentRecordId = lstWrkOrd[1].Id,                                         
                                            EarliestStartTime = System.now(),
                                            DueDate = System.now() + 30,
                                            Work_Order__c = lstWrkOrd[1].Id                       
                                    )
            };
            insert sappLst;
        }
    }

    @isTest
    public static void testupdateWOCase(){
        System.runAs(mainUser){
            Test.startTest();
                sappLst[0].Status = 'Cancelled';
                sappLst[1].Status = 'Cancelled';
                update sappLst;
            Test.stopTest();

            list<WorkOrder> lstNewWO = [ SELECT Status FROM WorkOrder
                                            WHERE Id = :lstWrkOrd[0].Id ] ;       
            System.assertEquals(lstNewWO[0].Status, 'Cancelled' );

            list<Case> lstCase = [ SELECT Status  FROM Case  WHERE Id = :lstCase[0].Id] ;       
            System.assertEquals(lstCase[0].Status, 'Pending Action from Cham' );
        }
    }

}