/**
 * @File Name          : AP47_SAUpdatePrli_TEST.cls
 * @Description        : 
 * @Author             : ANA
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-05-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         25-11-2019     		     ANA        Initial Version
**/
@isTest
public class AP47_SAUpdatePrli_TEST {
    static User mainUser;
    static Account testAcc = new Account();
    static List<ServiceAppointment> lstServiceApp;
    static List<ProductRequest> lstProdRequest;
    static WorkOrder wrkOrd = new WorkOrder();
    static ProductRequestLineItem prli;
    static Order ord;
    static Product2 prod;
    static OrderItem ordItem = new OrderItem();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    //bypass
    static Bypass__c bp = new Bypass__c();


    static{
        mainUser = TestFactory.createAdminUser('AP47@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;
        
        System.runAs(mainUser){
            //Insert Accounts
            testAcc = TestFactory.createAccount('AP22_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;
            
			//Insert logement
			Logement__c lo = TestFactory.createLogement('Lo_Test', 'test01.sc@mauritius.com', 'test_lname');
            lo.Blue_Order_Id__c = '9711';             
            insert lo;

            //Insert products
            prod = TestFactory.createProduct('testProd');
            insert prod;

            //Insert product Requests
            lstProdRequest = new List<ProductRequest> {
                new ProductRequest()
            };  
            insert lstProdRequest;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2( Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;

            //create Order
            ord = new Order(
                AccountId = testAcc.Id
                ,Name = 'TestOrder'
                ,EffectiveDate = System.today()
                ,Status = AP_Constant.OrdStatusAchat
                ,Pricebook2Id = standardPricebook.Id
                ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId()
            );

            //Insert Asset & Case
            Asset asst = TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lo.Id);
            asst.accountId = testAcc.Id;
            insert asst;

            Case cse = TestFactory.createCase(testAcc.Id, 'Troubleshooting', asst.Id);
            insert cse;

            //Insert WorkOrder
            wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse.Id;
            insert wrkOrd;
            
            //Create Product Request Line Item
            prli = new ProductRequestLineItem(
                Parent = lstProdRequest[0],
                ParentId = lstProdRequest[0].Id,
                Product2Id=prod.Id,
                Commande__c=ordItem.OrderId,
                QuantityRequested=decimal.valueof('3'),
                Status = 'Réservé à préparer', 
                WorkOrderId = wrkOrd.id
            );                 
            insert prli;
			
			//Insert serviceAppointment
			lstServiceApp = new List<ServiceAppointment>{
                new ServiceAppointment(
                    // Status = 'Done OK',
                    Subject = 'this is a test subject',
                    ParentRecordId = wrkOrd.Id,
                    EarliestStartTime = System.now(),
                    DueDate = System.now() + 30,                                        
                    Category__c = AP_Constant.ServAppCategoryVisiteEntr,
                    Residence__c = lo.Id  , 
                    SchedStartTime = System.today() ,
                    SchedEndTime = System.today().addDays(100)                           
                )
            };
            insert lstServiceApp;    
        }
    }

    @isTest
    public static void testUpdateNeedByDate(){
        Date updatedStartDate = System.today().addDays(10);

        System.runAs(mainUser){
            Test.startTest();
                lstServiceApp[0].SchedStartTime = updatedStartDate;
                update lstServiceApp;
            Test.stopTest();

            List<ProductRequestLineItem> lstPRLI = [
                SELECT Id, NeedByDate
                FROM ProductRequestLineItem
                WHERE WorkOrderId =: wrkOrd.id
            ];

            if(!lstPRLI.isEmpty()){
                system.assertEquals(updatedStartDate, lstPRLI[0].NeedByDate);
            }
        }
    }

}