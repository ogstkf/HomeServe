@isTest
public with sharing class AP23_OrderItemManager_TEST {
 /**
 * @File Name          : AP23_OrderItemManager_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (KZE)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-10-2019               KZE         Initial Version
**/
static User mainUser;
    static Order ord ;
    static Account testAcc = new Account();
    static Product2 prod = new Product2();
    static Shipment ship = new Shipment();
    static ProductTransfer prodTrans = new ProductTransfer();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static OrderItem ordItem = new OrderItem();

    static{
        mainUser = TestFactory.createAdminUser('AP23@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

 
        System.runAs(mainUser){

            //create account
            testAcc = TestFactory.createAccountBusiness('AP23');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            testAcc.Livraison_express_possible__c  = true;
            insert testAcc;

             //create products
            prod = TestFactory.createProduct('testProd');
            insert prod;


            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
            // System.debug('##### TEST lstPrcBk: '+lstPrcBk);

            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;


            //create Order
            ord = new Order(AccountId = testAcc.Id
                                    ,Name = 'Test1'
                                    ,EffectiveDate = System.today()
                                    ,Status = AP_Constant.OrdStatusAchat
                                    ,Pricebook2Id = lstPrcBk[0].Id
                                    ,RecordTypeId=Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Commandes_fournisseurs').getRecordTypeId());

            insert ord;

            ordItem = new OrderItem(
                            OrderId=ord.Id,
                            Quantity=decimal.valueof('1'),
                            PricebookEntryId=PrcBkEnt.Id,
                            UnitPrice = 145);
        
            insert ordItem;
        }
    }

    @isTest
    public static void testDeletecheckOrderStatus(){
        System.runAs(mainUser){
            ord.Status= AP_Constant.OrdStatusAchatApprove;
            ord.Type_de_livraison_de_la_commande__c ='Standard';
            ord.Mode_de_transmission__c = 'Email';
            update ord;

            Test.startTest();
                try{
                    delete ordItem;
                }
                catch(Exception e){
                    System.debug('e.getMessage() : '+e.getMessage());
                    System.assertNotEquals(e.getMessage(), null);
                    // System.assertEquals(e.getMessage(),label.QuantityReceivedNull);
                }                
            Test.stopTest();
        }
    }
    @isTest
    public static void testInsertcheckOrderStatus(){
        System.runAs(mainUser){
            ord.Status= AP_Constant.OrdStatusAchatApprove;
            ord.Type_de_livraison_de_la_commande__c ='Standard';
            ord.Mode_de_transmission__c = 'Email';
            update ord;

            Test.startTest();
                try{
                    OrderItem newOrdItem = new OrderItem(
                                                OrderId=ord.Id,
                                                Quantity=decimal.valueof('10'),
                                                PricebookEntryId=PrcBkEnt.Id,
                                                UnitPrice = 145);
                    insert newOrdItem;
                }
                catch(Exception e){
                    System.debug('e.getMessage() : '+e.getMessage());
                    System.assertNotEquals(e.getMessage(), null);
                    // System.assertEquals(e.getMessage(),label.QuantityReceivedNull);
                }                
            Test.stopTest();
        }
    }

}