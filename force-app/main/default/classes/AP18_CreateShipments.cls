/**
 * @File Name          : AP18_CreateShipments.cls
 * @Description        : creation of shipments from orders
 * @Author             : SBH
 * @Group              : Spoon Consulting
 * @Last Modified By   : SBH
 * @Last Modified On   : 21/10/2019, 15:24
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    16/10/2019, 17:18:17          SBH                Initial Version (CT-1083)
 **/
public with sharing class AP18_CreateShipments {


    public static void addError(){

    }

    public static void createShipmentsForOrders(List<Order> lstOrders, Set<Id> setServTerritory){

        System.debug('## createShipmentsForOrders start ');
        // Initialisations
        List<Shipment> lstShipmentToCreate = new List<Shipment>();
        Id currentUserId = UserInfo.getUserId();
        Id destinationLocationId;
        List<ProductTransfer> lstProdTransferToIns = new List<ProductTransfer>();
        //todo:  fill orderToOrderItems: 
        Map<Id, List<OrderItem>> mapOrderToOrderItems = new Map<Id, List<OrderItem>>();
        // End initialisations 

        // retrieve required infos start
        List<OrderItem> lstOrderItem = [SELECT Quantity, Product2Id, OrderId, TECH_ProductLot__c 
                                        FROM OrderItem
                                        WHERE OrderId IN :lstOrders];
        for(OrderItem oi : lstOrderItem){
            if(mapOrderToOrderItems.containsKey(oi.OrderId) ){
                mapOrderToOrderItems.get(oi.OrderId).add(oi);
            }else{
                mapOrderToOrderItems.put(oi.OrderId, new List<OrderItem>{oi});
            }
        }
        // retrieve required infos end

        // SBH: Retrieve Service resource of User 
        
        // List<Schema.Location> lstUserServiceResourceLocation = [SELECT Id, LocationType FROM Location WHERE Service_Resource__r.RelatedRecordId = :currentUserId AND LocationType = 'Entrepôt'];
        // System.debug('## lstUserServiceResource: ' + lstUserServiceResourceLocation);                                                   

        System.debug('## lst service territory ids: ' + setServTerritory);
        List<ServiceTerritoryLocation> lstServTerLoc = [
            SELECT ServiceTerritoryId, LocationId, Location.LocationType, Location.ParentLocationId
            FROM ServiceTerritoryLocation
            WHERE ServiceTerritoryId IN :setServTerritory
            AND Location.LocationType = 'Entrepôt'
            AND Location.ParentLocationId = null
        ];

        Map<Id, Id> mapServTerritory_location = new Map<Id, Id>();
        if(!lstServTerLoc.isEmpty()){
            for(ServiceTerritoryLocation stl: lstServTerLoc){
                mapServTerritory_location.put(stl.ServiceTerritoryId, stl.LocationId);
            }
        }

        System.debug('##mapServTerritory_location ' + mapServTerritory_location);

        // if(!lstUserServiceResourceLocation.isEmpty()){
        //     destinationLocationId = lstUserServiceResourceLocation[0].Id;
        // }
        // Retrieve required infos end



        for(Integer i = 0; i < lstOrders.size(); i++){

            // create shipments
            Shipment shipmentToCreate = new Shipment();
            
            // Filling infos: 
            shipmentToCreate.RecordTypeId = getShipmentRecordTypeID('Reception_Fournisseur'); 
            shipmentToCreate.Order__c = lstOrders[i].Id;
            shipmentToCreate.SourceLocationId = lstOrders[i].TECH_OrderAccountLocation__c;
            
            shipmentToCreate.ShipToName = lstOrders[i].OrderNumber;
            shipmentToCreate.ExpectedDeliveryDate = lstOrders[i].Date_de_livraison_prevue__c != null ? Datetime.newInstance(lstOrders[i].Date_de_livraison_prevue__c, Time.newInstance(14,0,0,0)) : null;
            shipmentToCreate.DeliveredToId = lstOrders[i].OwnerId;


            //setting location Id 
            String destinationId;
            if(lstOrders[i].Agence__c != null){
                destinationId = mapServTerritory_location.get(lstOrders[i].Agence__c);
            }
            

            shipmentToCreate.DestinationLocationId = destinationId != null ? destinationId : null;

            lstShipmentToCreate.add(shipmentToCreate);          
        }
        System.debug('## lstShipmentToCreate ' + lstShipmentToCreate);

        if(!lstShipmentToCreate.isEmpty()){
            insert lstShipmentToCreate;

            // create product transfers
            for(Shipment curShipment: lstShipmentToCreate){
                
                // Get List of OrderItems: 
                List<OrderItem> lstCurrentOrderItems = mapOrderToOrderItems.get(curShipment.Order__c);
                if(lstCurrentOrderItems != null && !lstCurrentOrderItems.isEmpty()){
                    
                    for(OrderItem oi : lstCurrentOrderItems){
                        ProductTransfer prdTransfer = new ProductTransfer();
                        prdTransfer.ShipmentId = curShipment.id;
                        prdTransfer.Commande__c = curShipment.Order__c;
                        prdTransfer.SourceLocationId = curShipment.SourceLocationId;
                        prdTransfer.DestinationLocationId = curShipment.DestinationLocationId;
                        prdTransfer.QuantitySent = oi.TECH_ProductLot__c != null ?  oi.Quantity * oi.TECH_ProductLot__c : oi.Quantity;
                        prdTransfer.Quantity_lot_sent__c = oi.Quantity;
                        //29/01/2021 - BCH - TEC-451 - Set by default the Quantity Lot Received = order item quantity
                        prdTransfer.Quantity_lot_received__c = oi.Quantity;
                        prdTransfer.Product2Id = oi.Product2Id;
                        lstProdTransferToIns.add(prdTransfer);
                    }

                }
            }

            if(!lstProdTransferToIns.isEmpty()){
                insert lstProdTransferToIns;
            }
        }

        System.debug('## createShipmentsForOrders end ');
        
    }

    public static String getShipmentRecordTypeID(String devName){
      return Schema.SObjectType.Shipment.getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
    }
}