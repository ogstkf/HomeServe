/**
 * @File Name          : LC10_ProdReqLineItemTEST.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    06/11/2019   AMO   Initial Version
*  1.1    26/11/2019   ANA     
**/
@isTest
public with sharing class LC10_ProdReqLineItemTEST {
    static User mainUser;
    static Account testAcc = new Account();
    static Account testFournisseur = new Account();
    static List<ProductRequest> lstProdRequest;
    static List<ProductRequestLineItem> lstPRLI;
    static List<ServiceTerritory> lstServiceTerritory;
    static List<Fournisseur_agence__c> lstFounAgnc;
    static WorkOrder wrkOrd = new WorkOrder();
    static Order ord;
    static Product2 prod;
    static OrderItem ordItem = new OrderItem();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<LC10_ProdReqLineItemOrder.PbWrapper> lstWrapper = new List<LC10_ProdReqLineItemOrder.PbWrapper>();
    static List<LC10_ProdReqLineItemOrder.PrliWrapper> lstPrliWrap = new List<LC10_ProdReqLineItemOrder.PrliWrapper>();
    static Conditions__c con = new Conditions__c();
    static LC10_ProdReqLineItemOrder.PbWrapper Wrapper = new LC10_ProdReqLineItemOrder.PbWrapper();
    static Bypass__c bp = new Bypass__c();


    static{
        mainUser = TestFactory.createAdminUser('LC10@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP16,WorkOrderTrigger';
        bp.BypassValidationRules__c = True;
        insert bp;
        
        System.runAs(mainUser){
            //Insert Accounts
            testAcc = TestFactory.createAccount('LC10');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.Mode_de_transmission__c = 'EDI';
            testAcc.Mode_de_transmission_privil_gi__c = 'EDI';
            insert testAcc;

            testFournisseur = TestFactory.createAccountBusiness('Test2');
            testFournisseur.BillingPostalCode = '1233456';
            testFournisseur.Actif__c = true;
            testFournisseur.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            insert testFournisseur;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(), 
                IsActive = true,
                RecordTypeId= Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId(),
                Compte__c = testFournisseur.Id
            );
            lstPrcBk.add(standardPricebook);
            Pricebook2 prcbk2 = new Pricebook2(
                Name = 'testPrcBk',
                IsActive = true,
                RecordTypeId= Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Achat').getRecordTypeId(),
                Compte__c = testFournisseur.Id
            );
            lstPrcBk.add(prcbk2);
            upsert lstPrcBk;


            //Insert Conditions
            con.recordtypeId = Schema.SObjectType.Conditions__c.getRecordTypeInfosByDeveloperName().get('Header').getRecordTypeId();
            con.Frais_de_port__c = 0;
            con.Type__c = 'Standard';
            con.Compte__c = testFournisseur.Id;
            con.Mode__c = 'EDI';
            con.Catalogue__c = lstPrcBk[1].Id;
            con.Min__c = 2;
            con.Debut__c = System.today().addDays(-1);
            con.Fin__c = System.today().addDays(1);
            // con.Actif__c = true;
            insert con;
            
            //Insert logement
            Logement__c lo = TestFactory.createLogement('Lo_Test', 'test01.sc@mauritius.com', 'test_lname');
            lo.RecordTypeId= Schema.SObjectType.Logement__c.getRecordTypeInfosByDeveloperName().get('Logement').getRecordTypeId();            
            System.debug('logement'+lo);
            lo.Blue_Order_Id__c = '9711';             
            insert lo;

            //Insert products
            prod = TestFactory.createProduct('testProd');
            prod.Lot__c=1000;
            insert prod;

            //Insert product Requests
            lstProdRequest = new List<ProductRequest> {
                new ProductRequest(),
                new ProductRequest(),
                new ProductRequest()
            };  
            insert lstProdRequest;


            //pricebook entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, prod.Id, 150);
            insert PrcBkEnt;
            pricebookentry PrcBkEnt2  = TestFactory.createPriceBookEntry(lstPrcBk[1].Id, prod.Id, 150);
            insert PrcBkEnt2;
            
            //Insert Asset & Case
            Asset asst = TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lo.Id);
            asst.accountId = testAcc.Id;
            insert asst;

            Case cse = TestFactory.createCase(testAcc.Id, 'Commissioning', asst.Id);
            insert cse;

            //Insert WorkOrder
            wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse.Id;
            insert wrkOrd;

            //Create operating hours
            OperatingHours newOperatingHour = TestFactory.createOperatingHour('test1');
            insert newOperatingHour;

            TimeSlot newTimeSlot = TestFactory.createTimeSlot('Monday', newOperatingHour);
            insert newTimeSlot;

            //create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa;
            sofactoapp__Raison_Sociale__c sofa1 = new sofactoapp__Raison_Sociale__c(Name= 'TEST1', sofactoapp__Credit_prefix__c= '2341', sofactoapp__Invoice_prefix__c='4321');
            insert sofa1;

            //Insert Service territories/ agencies
            lstServiceTerritory = new List<ServiceTerritory>{
                new ServiceTerritory(
                    Name = 'test',
                    Agency_Code__c = '0007',
                    OperatingHoursId = newOperatingHour.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa.Id
                ),
                new ServiceTerritory(
                    Name = 'test1',
                    Agency_Code__c = '0008',
                    OperatingHoursId = newOperatingHour.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa1.Id
                )};
            insert lstServiceTerritory;

            lstFounAgnc = new List<Fournisseur_agence__c>();
            lstFounAgnc.add(
                new Fournisseur_agence__c(
                    Agence__c = lstServiceTerritory[0].Id,
                    Fournisseur__c = testFournisseur.Id
                )
            );
            lstFounAgnc.add(
                new Fournisseur_agence__c(
                    Agence__c = lstServiceTerritory[1].Id,
                    Fournisseur__c = testFournisseur.Id
                )
            );

            insert lstFounAgnc;
            
            //Create Product Request Line Items
            lstPRLI = new List<ProductRequestLineItem>{
                new ProductRequestLineItem(
                    Parent = lstProdRequest[0],
                    ParentId = lstProdRequest[0].Id,
                    Product2Id=prod.Id,
                    Commande__c=ordItem.OrderId,
                    QuantityRequested=decimal.valueof('3'),
                    // Status = AP_Constant.prliStatusACommander, 
                    WorkOrderId = wrkOrd.id,
                    agence__c = lstServiceTerritory[0].Id,
                    NeedByDate = System.today().addDays(5)
                ),
                new ProductRequestLineItem(
                    Parent = lstProdRequest[0],
                    ParentId = lstProdRequest[0].Id,
                    Product2Id=prod.Id,
                    Commande__c=ordItem.OrderId,
                    QuantityRequested=decimal.valueof('3'),
                    // Status = AP_Constant.prliStatusACommander, 
                    WorkOrderId = wrkOrd.id,
                    agence__c = lstServiceTerritory[0].Id,
                    NeedByDate = System.today().addDays(5)
                )};
                
                insert lstPRLI;
            
                LC10_ProdReqLineItemOrder.PrliWrapper prliwrap = new LC10_ProdReqLineItemOrder.PrliWrapper();
                prliwrap.prli = lstPRLI[0];
                prliwrap.prodName = lstPRLI[0].Product2.Name;
                prliwrap.unitPrice = null;
                prliwrap.totPrice = null;
                prliwrap.QuantityRequested = lstPRLI[0].QuantityRequested;   
                prliwrap.agence = lstPRLI[0].Agence__c;  
                prliwrap.productId = PrcBkEnt.Product2Id;
                List<LC10_ProdReqLineItemOrder.PrliWrapper> lstPRLIS = new List<LC10_ProdReqLineItemOrder.PrliWrapper>();
                lstPRLIS.add(prliwrap);

                Wrapper.pb = lstPrcBk[0];
                Wrapper.PriceBkId = standardPricebook.Id;
                Wrapper.pricebookname = standardPricebook.Name;
                Wrapper.pricebook2Id = standardPricebook.Id;
                Wrapper.acc = standardPricebook.Compte__c;
                Wrapper.lstPrliWrap=lstPRLIS;

                Wrapper.lstPrliWrap[0].pbeId = PrcBkEnt.Id;
        }
    }

    @isTest
    public static void testGetRecords(){
        System.runAs(mainUser){
            String recordIds = '';
            if(!lstPRLI.isEmpty()){
                for(ProductRequestLineItem prli : lstPRLI){
                    recordIds += prli.id + ',';
                }
            }

            Object Records = LC10_ProdReqLineItemOrder.getData(recordIds);
            List<Object> lstObj = new List<Object>();
            lstObj.add(Records);
            system.assertEquals(lstObj.size(), 1);
        }
    }

    @isTest
    public static void testGetRecordsAgenceNull(){
        System.runAs(mainUser){
            String recordIds = '';
            if(!lstPRLI.isEmpty()){
                for(ProductRequestLineItem prli : lstPRLI){
                    prli.agence__c = null;
                    update prli;
                    recordIds += prli.id + ',';
                }
            }

            Object Records = LC10_ProdReqLineItemOrder.getData(recordIds);
            List<Object> lstObj = new List<Object>();
            lstObj.add(Records);
            system.assertEquals(lstObj.size(), 1);
        }
    }

    @isTest
    public static void testGetRecordsNone(){
        System.runAs(mainUser){
            String recordIds = '';
  
        try{
            Object Records = LC10_ProdReqLineItemOrder.getData(recordIds);
            }
        catch(Exception e)
            {
                Boolean expectedExceptionThrown =  e.getMessage().contains('Sélectionner au moins une ligne') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
        }
    }

    @isTest
    public static void testGetRecords2(){
        System.runAs(mainUser){
            String recordIds = '';
            if(!lstPRLI.isEmpty()){
                lstPRLI[1].agence__c = lstServiceTerritory[1].Id;
                update lstPRLI[1];
                for(ProductRequestLineItem prli : lstPRLI){
                    recordIds += prli.id + ',';
                }
            }
  
        try{
            Object Records = LC10_ProdReqLineItemOrder.getDataReport(recordIds);
            }
        catch(Exception e)
            {
                // Boolean expectedExceptionThrown =  e.getMessage().contains('Sélectionner au moins une ligne') ? true : false;
                // System.AssertEquals(expectedExceptionThrown, true);
            }
        }
    }

    @isTest
    public static void testGetRecordsMoreThanOneAgency(){
        System.runAs(mainUser){
            String recordIds = '';
            if(!lstPRLI.isEmpty()){
                lstPRLI[1].agence__c = lstServiceTerritory[1].Id;
                update lstPRLI[1];
                for(ProductRequestLineItem prli : lstPRLI){
                    recordIds += prli.id + ',';
                }
            }

            try{
            Object Records = LC10_ProdReqLineItemOrder.getData(recordIds);
            }
        catch(Exception e)
            {
                Boolean expectedExceptionThrown =  e.getMessage().contains('Vous ne devez sélectionner que des lignes au statut \'A commander\' pour la même agence') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }

        }
    }

    @isTest
    public static void testGetRecordsTypeDeFournisseur(){
        System.runAs(mainUser){
            String recordIds = '';
            if(!lstPRLI.isEmpty()){
                testAcc.Type_de_fournisseurs__c = 'Constructeur';
                update testAcc;
                for(ProductRequestLineItem prli : lstPRLI){
                    recordIds += prli.id + ',';
                }
            }

            Object Records = LC10_ProdReqLineItemOrder.getData(recordIds);
            List<Object> lstObj = new List<Object>();
            lstObj.add(Records);
            system.assertEquals(lstObj.size(), 1);
        }
    }

    @isTest
    public static void testCreateRecords(){
        System.runAs(mainUser){
            String PRLI = lstPRLI[0].Id;
            String pricebookId = lstPrcBk[0].Id;
            String pricebookEntryId = PrcBkEnt.Id;
            String concatenatedIds = PRLI + '_' + pricebookId + '_' + PricebookEntryId;
            List<String> lstInputStr = new List<String>{concatenatedIds};
            String json = System.json.serialize(Wrapper);
            NullPointerException nullexc;
            try{
                Test.startTest();
                LC10_ProdReqLineItemOrder.saveOrders(json);
                Test.stopTest();
            }catch (AuraHandledException e){
                System.assert(e.getMessage().contains('fournisseur'));
            }
        }
    }

    @isTest
    public static void testCreateOrder(){
        System.runAs(mainUser){
            String PRLI = lstPRLI[0].Id;
            String pricebookId = lstPrcBk[0].Id;
            String pricebookEntryId = PrcBkEnt.Id;
            String concatenatedIds = PRLI + '_' + pricebookId + '_' + PricebookEntryId;
            String json = System.json.serialize(Wrapper);

            Test.startTest();   
                LC10_ProdReqLineItemOrder.saveOrders(System.JSON.serialize(Wrapper));
            Test.stopTest();

            Integer count = [
                SELECT Count()
                FROM order 
                WHERE pricebook2Id =:pricebookId 
                AND effectiveDate =:System.today()
                AND Status =: AP_Constant.OrdStatusAchat
            ];

            system.assertEquals(1, count);
        }
    }

    @isTest
    public static void testCreateOrderItems(){
        System.runAs(mainUser){
            Wrapper.lstPrliWrap[0].unitPrice = 3;
            String json = System.json.serialize(Wrapper);

            Test.startTest();   
                LC10_ProdReqLineItemOrder.saveOrders(System.JSON.serialize(Wrapper));
            Test.stopTest();

            Integer count = [
                SELECT Count()
                FROM order 
                WHERE pricebook2Id =:lstPrcBk[0].Id 
                AND effectiveDate =:System.today()
                AND Status =: AP_Constant.OrdStatusAchat
            ];

            Order ord = [SELECT Id FROM Order WHERE pricebook2Id =:lstPrcBk[0].Id AND effectiveDate =:System.today() AND Status =: AP_Constant.OrdStatusAchat];

            List<OrderItem> lstOrdItm = [SELECT Id, OrderId, Product2Id, Quantity, UnitPrice, PriceBookEntryId FROM OrderItem WHERE OrderId =: ord.Id];

            System.assertEquals(1, lstOrdItm.size());
            System.assertEquals(Wrapper.lstPrliWrap[0].QuantityRequested, lstOrdItm[0].Quantity);
            System.assertEquals(Wrapper.lstPrliWrap[0].unitPrice, lstOrdItm[0].UnitPrice);
            System.assertEquals(Wrapper.lstPrliWrap[0].pbeId, lstOrdItm[0].PriceBookEntryId);
            System.assertEquals(Wrapper.lstPrliWrap[0].productId, lstOrdItm[0].Product2Id);
        }
    }

    @isTest
    public static void testGetUrl(){
        System.runAs(mainUser){
            Test.startTest();
            LC10_ProdReqLineItemOrder.baseUrl();
            Test.stopTest();
        }
    }
}