/**
 * @File Name         : WS16_GestionDesPrelevements.cls
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 21-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * ==============================================================
 * 1.0   01-02-2022   MNA   Initial Version
**/

public with sharing class WS16_GestionDesPrelevements {
    
    static WebServiceConfiguration__mdt apiSlimPay = [SELECT Id, Endpoint__c, DeveloperName, Method__c, Header_Content_Type__c
                                                        FROM WebServiceConfiguration__mdt WHERE DeveloperName = 'Slimpay'];

    private static String getAccessToken(ServiceTerritory agency) {
        String authorization;
        Map<String, Object> mapObj;

        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse response;
        
        req.setEndpoint('callout:slimpay_' + agency.Slimpay_Creditor_Name__c);
        req.setMethod('POST');
        
        req.setHeader('Authorization', 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf('{!$Credential.UserName}' + ':' + '{!$Credential.Password}')));
        req.setHeader('Content-Type',  'application/x-www-form-urlencoded');
        req.setHeader('Accept',  'application/json');
        
        req.setBody('grant_type=client_credentials&scope=api');

        try {
            response = http.send(req);
        }
        catch (CalloutException ex) {
            System.debug(ex.getMessage());
            throw new CalloutException('Erreur lors de l\'authentification');
        }


        mapObj = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        
        return String.valueOf(mapObj.get('access_token'));
    }

    @Future(callout=true)
    public static void createOrderAsync(String scId, String itmRef) {
        ServiceContract sc = [SELECT Id, Mandate_signed__c, sofactoapp_IBAN__c, Payeur_du_contrat__c, sofactoapp_Rib_prelevement__c, Numero_du_contrat__c, Asset__c,
                                Asset__r.Equipment_type_auto__c
                                    FROM ServiceContract WHERE Id=:scId];
        Account acc = [SELECT Id, Agence__c, ClientNumber__c, FirstName, LastName, Name, Salutation, BillingStreet, Adress_complement__c, BillingPostalCode , BillingCity,
                        BillingCountry, PersonMobilePhone, PersonEmail
                   		    FROM Account WHERE Id =: sc.Payeur_du_contrat__c];
        ServiceTerritory agency = [SELECT Id, Name, Slimpay_Creditor_Name__c, Nom_court__c, Address, Email__c, Phone__c FROM ServiceTerritory WHERE Id =: acc.Agence__c];
        createOrder(acc, agency, sc, sc.sofactoapp_IBAN__c, itmRef);
    }
    
    public static Map<String, Object> createOrder(Account acc, ServiceTerritory agency, ServiceContract sc, String iban, String itmRef) {
        WSTransactionLog__c log = new WSTransactionLog__c();
        String accessToken = getAccessToken(agency);
        String body;
        slimPayObjectWrp.ResponseOrder resOrder;
        slimPayObjectWrp.ResponsePost resPost = new slimPayObjectWrp.responsePost();
        slimPayObjectWrp.OrderWrp order = new slimPayObjectWrp.OrderWrp(acc, agency, iban, 'SEPA.DIRECT_DEBIT.CORE', itmRef);
        Map<String, Object> mapReturnValue = new Map<String, Object>();

        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(apiSlimPay.Endpoint__c + 'orders');
        req.setMethod('POST');
        
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setHeader('Content-Type',  'application/json');
        
        req.setBody(JSON.serialize(order));

        log.DateTime__c = System.now();
        HttpResponse response = http.send(req);
        
        log.Endpoint__c = apiSlimPay.Endpoint__c + 'orders';
        log.StatusCode__c = String.valueOf(response.getStatusCode());
        log.Request__c = req.getBody();
        log.Response__c = response.getBody();

        System.debug(response.getBody());

        body = response.getBody();
        body = body.replaceAll('_links', 'links');
        body = body.replaceAll('https://api.slimpay.net/alps#user-approval', 'urlToGet');

        mapReturnValue.put('statusCode', (Object) response.getStatusCode());

        
        if (response.getStatusCode() >= 200 && response.getStatusCode() < 300 ) {
            resOrder = (slimPayObjectWrp.ResponseOrder) JSON.deserialize(body, slimPayObjectWrp.ResponseOrder.class);

            resPost.transaction_id = resOrder.reference;
            if (resOrder == null || resOrder.links == null || resOrder.links.urlToGet == null || resOrder.links.urlToGet.href == null || resOrder.links.urlToGet.href == '') {
                resPost.status = 'pending';
            }
            else {
                resPost.status = 'finished';
                resPost.payload = new slimPayObjectWrp.PayLoad();
                resPost.payload.customer_id = acc.ClientNumber__c;
                resPost.payload.contract_id = sc.Numero_du_contrat__c;
                resPost.payload.redirect_user_url = resOrder.links.urlToGet.href;
            }
                
            mapReturnValue.put('returnValue', resPost);
            
            if (System.isFuture())
                send(acc, sc, agency, resPost.payload.redirect_user_url);

           	insert new sofactoapp__External_Api__c(
                    RecordTypeId = Schema.SObjectType.sofactoapp__External_Api__c.getRecordTypeInfosByDeveloperName().get('Transaction_Slimpay').getRecordTypeId(),
                    transaction__c = resPost.transaction_id,
                    sofactoapp__Status_Code__c = '200',
                    sofactoapp__Response__c = JSON.serialize(resPost)
                      
            );
        	sc.Id_Mandate_Order__c = resOrder.reference;
            update sc;
        }
        else {
            Map<String, Object> mapObj = (Map<String, Object>) JSON.deserializeUntyped(body);
            mapReturnValue.put('returnValue', slimPayObjectWrp.getErrorApex(response.getStatusCode(), (String) mapObj.get('message')));
        }

        insert log;
        
        return mapReturnValue;
    } 

    @future(callout=true)
    public static void createMandateAsync(String scId) {
        WSTransactionLog__c log = new WSTransactionLog__c();
        ServiceContract sc = [SELECT Id, Mandate_signed__c, sofactoapp_IBAN__c, Payeur_du_contrat__c, sofactoapp_Rib_prelevement__c,
                              	sofactoapp_Rib_prelevement__r.Date_signature_du_mandat__c, sofactoapp_Rib_prelevement__r.RUM__c, Contrat_signe_par_le_client__c
                                    FROM ServiceContract WHERE Id=:scId];

        Account acc = [SELECT Id, Agence__c, ClientNumber__c, FirstName, LastName, Name, Salutation, BillingStreet, Adress_complement__c, BillingPostalCode , BillingCity,
                        BillingCountry, PersonMobilePhone, PersonEmail
                           FROM Account WHERE Id =: sc.Payeur_du_contrat__c];
                           
        ServiceTerritory agency = [SELECT Id, Slimpay_Creditor_Name__c FROM ServiceTerritory WHERE Id =: acc.Agence__c];

        String accessToken = getAccessToken(agency);
        String body;
        slimPayObjectWrp.ResponseOrder resOrder;
        slimPayObjectWrp.MandateWrp mandate = new slimPayObjectWrp.MandateWrp(acc, agency, sc);

        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(apiSlimPay.Endpoint__c + 'mandates');
        req.setMethod('POST');

        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setHeader('Content-Type',  'application/json');

        req.setBody(JSON.serialize(mandate));

        log.DateTime__c = System.now();
        HttpResponse response = http.send(req);
        
        log.Endpoint__c = apiSlimPay.Endpoint__c + 'mandates';
        log.StatusCode__c = String.valueOf(response.getStatusCode());
        log.Request__c = req.getBody();
        log.Response__c = response.getBody();
        
        System.debug(response.getBody());

        body = response.getBody();
        body = body.replaceAll('_links', 'links');
        
        resOrder = (slimPayObjectWrp.ResponseOrder) JSON.deserialize(body, slimPayObjectWrp.ResponseOrder.class);
        
		insert log;
        sc.Contrat_signe_par_le_client__c = true;
        sc.ReferenceIBAN__c = resOrder.Id;
        update sc;
        update new sofactoapp__Coordonnees_bancaires__c(Id = sc.sofactoapp_Rib_prelevement__c, rum__c = resOrder.rum);
    }

    public static void payIn(sofactoapp__R_glement__c payment) {
        sofactoapp__External_Api__c prelevement_history = new sofactoapp__External_Api__c();
        WSTransactionLog__c log = new WSTransactionLog__c();
        slimPayObjectWrp.ResponseOrder resOrder;

        ServiceTerritory agency = [SELECT Id, Slimpay_Creditor_Name__c FROM ServiceTerritory WHERE Id =: payment.Sofacto_Service_Contract__r.Agency__c];

        String accessToken = getAccessToken(agency);

        slimPayObjectWrp.PaymentWrp paymentWrp = new slimPayObjectWrp.PaymentWrp(payment, agency);

        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(apiSlimPay.Endpoint__c + 'payments/in');
        req.setMethod('POST');
        
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setHeader('Content-Type',  'application/json');

        req.setBody(JSON.serialize(paymentWrp));
        
        prelevement_history.RecordTypeId = Schema.SObjectType.sofactoapp__External_Api__c.getRecordTypeInfosByDeveloperName().get('Prelevement_History').getRecordTypeId();
        prelevement_history.R_glement__c = payment.Id;
        prelevement_history.DateEnvoi__c = System.now();

        log.DateTime__c = System.now();
        
        HttpResponse response = http.send(req);
        
        log.Endpoint__c = apiSlimPay.Endpoint__c + 'payments/in';
        log.StatusCode__c = String.valueOf(response.getStatusCode());
        log.Request__c = req.getBody();
        log.Response__c = response.getBody();
        
        System.debug(response.getBody());

        resOrder = (slimPayObjectWrp.ResponseOrder) JSON.deserialize(response.getBody(), slimPayObjectWrp.ResponseOrder.class);

        prelevement_history.Id_SlimPay__c = resOrder.Id;
        prelevement_history.sofactoapp__Status_Code__c = String.valueOf(response.getStatusCode());
        prelevement_history.sofactoapp__Response__c = response.getBody();

		insert log;
        insert prelevement_history;
    }

    public static void buildCSVPayIn(List<sofactoapp__R_glement__c> lstPay, ServiceTerritory agency) {
        Map<String, Object> mapBody = buildLineCSV(lstPay);
        String csv = '0;PAYMENTS;' + agency.Slimpay_Creditor_Name__c + ';' + agency.Name + ';\n'
                    + '1;IN;' + mapBody.get('size') + ';' + mapBody.get('amount') + ';EUR;\n'
                    + 'PAYMENT_DIRECTION;PAYMENT_REFERENCE;PAYMENT_AMOUNT;PAYMENT_LABEL;MANDATE_REFERENCE;\n'
                    + mapBody.get('body');

        blob b = Blob.valueOf(csv);
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        string csvname= 'Account.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(b);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new list<string> {'ogstkf@gmail.com'};
        String subject ='Account CSV';
        email.setSubject(subject);
        email.setToAddresses( toAddresses );
        email.setPlainTextBody('Account CSV ');
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }


    public static Map<String, Object> buildLineCSV(List<sofactoapp__R_glement__c> lstPay) {
        Integer size = 0;
        Decimal totalAmount = 0;
        String csv = '';
        Map<String, Object> mapReturnValue = new Map<String, Object>();

        for (sofactoapp__R_glement__c pay : lstPay) {
            if (pay.Sofacto_Service_Contract__c != null && pay.Sofacto_Service_Contract__r.sofactoapp_Rib_prelevement__c != null) {
                size++;
                totalAmount += pay.sofactoapp__Montant__c;
                csv += 'IN;'                                                                            //PAYMENT_DIRECTION
                    + pay.Name + '-' + System.now().format('yyyyMMdd') + ';'                            //PAYMENT_REFERENCE
                    + pay.sofactoapp__Montant__c + ';'                                                  //PAYMENT_AMOUNT
                    + 'Contrat ' + pay.Sofacto_Service_Contract__r.Numero_du_contrat__c + ';'           //PAYMENT_LABEL
                    + pay.Sofacto_Service_Contract__r.sofactoapp_Rib_prelevement__r.RUM__c + ';\n';     //MANDATE_REFERENCE
            }
        }

        mapReturnValue.put('size', size);
        mapReturnValue.put('amount', totalAmount);
        mapReturnValue.put('body', csv);
        return mapReturnValue;
    }

    public static void getFailedPayment(Integer page, ServiceTerritory agency) {
        String accessToken = getAccessToken(agency);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(apiSlimPay.Endpoint__c + 'payment-issues?scheme=SEPA.DIRECT_DEBIT.CORE&executionStatus=toprocess&size=100&page=' + page);
        req.setMethod('GET');
        
    }

    public static void rejectPayment(sofactoapp__External_Api__c prelevement_history, ServiceTerritory agency) {
        String accessToken = getAccessToken(agency);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(apiSlimPay.Endpoint__c + 'payment-issues/'+ prelevement_history.Id_SlimPay__c + '/ack');
        req.setMethod('POST');
        
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setHeader('Content-Type',  'application/json');

        HttpResponse response = http.send(req);
    }

    public static String getMandate(ServiceTerritory agency, String reference) {
        String accessToken = getAccessToken(agency);
        slimPayObjectWrp.responseOrder resOrder;
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse response;
        req.setEndpoint(apiSlimPay.Endpoint__c + 'mandates?creditorReference=' + agency.Slimpay_Creditor_Name__c + '&reference=' + reference);
        req.setMethod('GET');
        
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setHeader('Content-Type',  'application/json');

        try {
            response = http.send(req);
        } catch (Exception ex) {
            throw new AuraHandledException('Erreur: ' + ex.getMessage());
        }

        resOrder = (slimPayObjectWrp.ResponseOrder) JSON.deserialize(response.getBody(), slimPayObjectWrp.ResponseOrder.class);
        return resOrder.Id;
    }

    public static Map<String, String> revokeMandate(ServiceTerritory agency, String idMandate) {
        String accessToken = getAccessToken(agency);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse response;
        req.setEndpoint(apiSlimPay.Endpoint__c + 'mandates/' + idMandate + '/revocation');
        req.setMethod('POST');
        
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setHeader('Content-Type',  'application/json');

        try {
            response = http.send(req);
        } catch (Exception ex) {
            throw new AuraHandledException('Erreur: ' + ex.getMessage());
        }
        
        if (response.getStatusCode() >= 200 && response.getStatusCode() < 300 ) {
            return UtilJSON.showToast('Succès', 'Le mandat a été révoqué', 'success') ;        
        }
        else {
            Map<String, Object> mapObj = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            return UtilJSON.showToast('Erreur', (String) mapObj.get('message'), 'error') ;        
        }
    }

    private static String buildMsg(Account acc, ServiceContract sc, ServiceTerritory agency, String url) {
        String address = (agency.Address.getStreet() == null ? '' : agency.Address.getStreet())
                        + (agency.Address.getPostalCode() == null ? '' : ' ' + agency.Address.getPostalCode())
                        + (agency.Address.getCity() == null ? '' : ' ' + agency.Address.getCity())
                        + (agency.Address.getCountry() == null ? '' : ' ' + agency.Address.getCountry());

        return acc.Salutation + ' ' + acc.Name + ',\n'
                + 'Afin d\'activer votre souscription au contrat d\'entretien '+ sc.Numero_du_contrat__c + ' relatif à votre équipement ' + (sc.Asset__c == null ? '' : sc.Asset__r.Equipment_type_auto__c) + ' veuillez cliquer sur le lien ci-dessous et renseigner le code que vous avez reçu par SMS au numéro ' + acc.PersonMobilePhone + '.\n'
                + '\n'
                + url + '\n'
                + '\n'
                + 'Si vous rencontrez des difficultés, n\'hésitez pas à nous contacter au ' + agency.Phone__c + '.\n'
                + '\n'
                + 'Cordialement,\n'
                + '\n'
                + agency.Name + '\n'
                + address + '\n'
                + agency.Phone__c + '\n';
    }

    private static void send(Account acc, ServiceContract sc, ServiceTerritory agency, String url) {
        String body = buildMsg(acc, sc, agency, url);
        if (acc.PersonEmail != null) {
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            List<OrgWideEmailAddress> lstOrgWideEmail = [SELECT Id, Address FROM OrgWideEmailAddress WHERE Address =: agency.Email__c];
            if(lstOrgWideEmail != null && lstOrgWideEmail.size() != 0)
                mail.setOrgWideEmailAddressId(lstOrgWideEmail[0].Id);

            mail.setToAddresses( new List<String> { acc.PersonEmail } );
            mail.setPlainTextBody(body);
            mails.add(mail);
            Messaging.sendEmail(mails);
        }
        else if(acc.PersonMobilePhone != null) {
            body = body.replace('\n', '\\\\n');
            WS10_SendSms.sendSMS(acc.PersonMobilePhone, body, agency.Nom_Court__c);
        }
    }

}