/**
 * @File Name          : ProductRequiredTriggerHandler.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 15/01/2020, 17:26:29
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/12/2019   RRJ     Initial Version
**/
public with sharing class ProductRequiredTriggerHandler {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Creation of two new productTransfer
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
 * 1.0         13-11-2019     		    LGO         Initial Version (CT-1182)
--------------------------------------------------------------------------------------
**************************************************************************************/

    Bypass__c userBypass;

    public ProductRequiredTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

	public void handleAfterUpdate(List<ProductRequired> lstOldProdReq, List<ProductRequired> lstNewProdReq){
		System.debug('ProductRequiredTriggerHandler handleAfterUpdate');

		List<ProductRequired> lstProdReqToTransfer = new List<ProductRequired>();

		for(Integer i=0;i<lstNewProdReq.size();i++){

			if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP42;')){
				if(lstOldProdReq[i].APP_Checked__c != lstNewProdReq[i].APP_Checked__c && lstNewProdReq[i].ParentRecordId != null){
					lstProdReqToTransfer.add(lstNewProdReq[i]);
				}
			}
		}

		if(lstProdReqToTransfer.size()>0){
			AP42_CreateProductTransfer.createProdTransfer(lstProdReqToTransfer);
		}
	}

}