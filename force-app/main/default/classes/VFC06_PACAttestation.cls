/**
 * @File Name          : VFC06_PACAttestation.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/7/2020, 6:24:34 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    8/1/2019, 2:40:06 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
 * 1.1    4/11/2019              DMG                                    Retour PDFs - corrections
 * 1.2    18/12/2019             SH                                     Added new fields in SELECT
**/
public with sharing class VFC06_PACAttestation {

    public ServiceAppointment currentServiceAppointment {get;set;}
    public List<ServiceAppointment> lstServiceAppointment {get;set;}
    public Measurement__c currentMeasurement {get;set;}
    public ServiceContract currentContrat {get;set;}
    public string coAmbiantStr {get;set;}
    public Datetime dateEntretien {get;set;}
    public Datetime dateRamonage {get;set;}
    public string addrLogement {get;set;}
    public List<wrapperAnomalyClient> lstWrapperAnomaly {get;set;}
    public string idSignature {get;set;}
    public string idSignatureTechnicien {get;set;}
    public string adresseCompte {get;set;}
    public string noSGS {get;set;}
    public string nomTechnincien {get;set;}
    public Double offset {get;set;}
    public Boolean containBurnerSection {get;set;}
    public Boolean equipmentTypePAC {get;set;}

    public class wrapperAnomalyClient{
        public String typeReason{get;set;}
        public String description{get;set;}

        public wrapperAnomalyClient(String typeReason,String description){
            this.typeReason = typeReason;
            this.description = description;
        }
    }

    public VFC06_PACAttestation() {
        System.debug('**** In constructor VFC06_PACAttestation ****');

        try{
            string servAppId = ApexPages.currentPage().getParameters().get('id');

            if(String.isNotBlank(servAppId)){
                lstServiceAppointment = new List<ServiceAppointment>([SELECT Id
                                                        ,ContactId
                                                        ,Account.Id
                                                        ,Account.ClientNumber__c
                                                        ,Account.Phone
                                                        ,Account.PersonMobilePhone
                                                        ,Account.PersonEmail
                                                        ,Account.BillingStreet
                                                        ,Account.BillingPostalCode
                                                        ,Account.BillingCity
                                                        ,Service_Contract__r.ContractNumber
                                                        ,Service_Contract__r.Name
                                                        ,Service_Contract__r.EndDate
                                                        ,ServiceTerritory.Name
                                                        ,ServiceTerritory.TerritoryStat__c
                                                        ,ServiceTerritory.Address
                                                        ,ServiceTerritory.Street
                                                        ,ServiceTerritory.City
                                                        ,ServiceTerritory.State
                                                        ,ServiceTerritory.PostalCode
                                                        ,ServiceTerritory.Country
                                                        ,ServiceTerritory.Phone__c
                                                        ,ServiceTerritory.Email__c                                                      
                                                        ,ServiceTerritory.Siret__c
                                                        ,ServiceTerritory.Siren__c 
                                                        ,ServiceTerritory.Intra_Community_VAT__c
                                                        ,Account.Name
                                                        ,Residence__r.Street__c
                                                        ,Residence__r.City__c                                                       
                                                        ,Residence__r.Postal_Code__c
                                                        ,Residence__r.Country__c
                                                        ,Residence__r.FullAddress__c 

                                                        ,AppointmentNumber  
                                                        ,Work_Order__r.Case.Type
                                                        ,Work_Order__r.Comments__c
                                                        // ,Work_Order__r.Case.Asset.TECH_Equipement_ID_cham__c
                                                        ,Work_Order__r.Case.Asset.IDEquipmenta__c
                                                        ,ActualEndTime
                                                        ,ActualStartTime
                                                        ,Work_Order__r.Mille_Bulles__c
                                                        ,Work_Order__r.TECH_TVA__c
                                                        ,Work_Order__r.TECH_TVA_Condition__c 
                                                        ,Work_Order__r.Compte_rendu_intervention__c
                                                        ,Work_Order__r.TECH_Numero_SGS_Agence__c
                                                        ,Work_Order__r.assetId
                                                        ,Work_Order__r.asset.Name 
                                                        ,Work_Order__r.asset.Brand__c
                                                        ,Work_Order__r.asset.Model__c
                                                        ,Work_Order__r.asset.Power_kW_auto__c
                                                        ,Work_Order__r.asset.Type_of_gaz__c
                                                        ,Work_Order__r.asset.Product2.Evacuation_type__c
                                                        ,Work_Order__r.asset.SerialNumber
                                                        ,Work_Order__r.asset.InstallDate
                                                        ,Work_Order__r.asset.BurnerBrand__c
                                                        ,Work_Order__r.asset.BurnerModel__c
                                                        ,Work_Order__r.asset.Burner_power_in_kw__c
                                                        ,Work_Order__r.asset.Burner_Serial_number__c
                                                        ,Work_Order__r.asset.Burner_Installation_date__c
                                                        ,Work_Order__r.asset.Quantity
                                                        ,Work_Order__r.asset.Type_of_energy__c
                                                        ,Work_Order__r.Case.asset.Brand__c
                                                        ,Work_Order__r.Case.asset.Model__c
                                                        ,Work_Order__r.Case.asset.Power_kW_auto__c
                                                        ,Work_Order__r.Case.asset.Type_of_gaz__c
                                                        ,Work_Order__r.Case.asset.Product2.Evacuation_type__c
                                                        ,Work_Order__r.Case.asset.Product2.Brand__c
                                                        ,Work_Order__r.Case.asset.Product2.Model__c
                                                        ,Work_Order__r.Case.asset.SerialNumber
                                                        ,Work_Order__r.Case.asset.InstallDate
                                                        ,Work_Order__r.Case.asset.BurnerBrand__c
                                                        ,Work_Order__r.Case.asset.BurnerModel__c
                                                        ,Work_Order__r.Case.asset.Burner_power_in_kw__c
                                                        ,Work_Order__r.Case.asset.Burner_Serial_number__c
                                                        ,Work_Order__r.Case.asset.Burner_Installation_date__c
                                                        ,Work_Order__r.Case.asset.Quantity
                                                        ,Work_Order__r.Case.asset.Type_of_energy__c
                                                        ,Work_Order__r.Case.asset.Brand_auto__c
                                                        ,Work_Order__r.Case.asset.Model_auto__c
                                                        ,Work_Order__r.Anomalie__c
                                                        ,Work_Order__r.Anomalie_reason__c
                                                        ,Work_Order__r.asset.Product2.Rendement__c
                                                        ,Work_Order__r.Case.asset.Product2.Efficiency__c
                                                        ,Work_Order__r.asset.Nox__c
                                                        // AAP 24 JUL 2019 : START
                                                        ,Work_Order__r.Case.asset.Equipment_type_auto__c
                                                        ,Work_Order__r.Case.asset.TECH_Equipement_ID_cham__c
                                                        // AAP 24 JUL 2019 : END
                                                        ,Work_Order__r.Case.asset.Product2.Puissance_thermique_chaud__c
                                                        ,Work_Order__r.Case.asset.Product2.Puissance_thermique_froid__c
                                                        ,Work_Order__r.Case.asset.Chauffage__c 
                                                        ,Work_Order__r.Case.asset.eau_chaude__c
                                                        // DMG 25 JUL 2019 : START
                                                        ,Work_Order__r.Case.asset.Date_de_premi_re_allumage__c
                                                        ,Work_Order__r.TECH_WO_Ramonage__c
                                                        ,Work_Order__r.Certificat_de_conformit__c
                                                        ,Work_Order__r.Notice_de_l_quipement__c
                                                        ,Work_Order__r.Bon_de_garantie__c
                                                        ,Work_Order__r.TECH_LogementId__c
                                                        // DMG 25 JUL 2019 : END
                                                        ,Gas_index_startTime__c
                                                        ,Gas_index_endTime__c
                                                        ,Gas_index_start__c
                                                        ,Gas_index_end__c
                                                        ,Sealing_check_impossible__c
                                                        ,Work_Order__r.WorkType.Type__c
                                                        //RRJ 09 AUG 2019 START
                                                        ,Work_Order__r.Case.AssetId
                                                        ,SchedEndTime
                                                        //RRJ 09 AUG 2019 END
                                                        ,Signature_Client_absence__c 
                                                        ,Signature_Client_not_approved__c //DMG 4/11/2019
                                                        //SH 18 DEC 2019 START
                                                        ,Street
                                                        ,PostalCode
                                                        ,City
                                                        ,Residence__r.Adress_complement__c
                                                        ,Signature_Client_approved__c
                                                        //SH 18 DEC 2019 END
                                                        // RRJ 20200507 START CT-1716
                                                        ,TECH_HomeComplement__c
                                                        ,Immeuble_R_sidence__c
                                                        ,Porte__c
                                                        ,Compl_ment__c
                                                        ,Residence__r.Imm_Res__c
                                                        ,Residence__r.Door__c
                                                        // RRJ 20200507 END 
                                                FROM ServiceAppointment 
                                                WHERE Id = :servAppId]);
                System.debug('**** lstServiceAppointment ' + lstServiceAppointment);
                

                if(lstServiceAppointment.size() > 0){

                    coAmbiantStr='';
                    currentServiceAppointment = lstServiceAppointment[0];

                    //DMG 18/11/2019 - check if EquipmentType = PAC
                    if (currentServiceAppointment.Street != null) {
                        currentServiceAppointment.Street = currentServiceAppointment.Street.replace('\r\n', '<br />');
                    }


                    //DMG 18/11/2019 - check if EquipmentType = PAC
                    if(currentServiceAppointment.Work_Order__r.Case.asset.Equipment_type_auto__c!=null){
                        if(currentServiceAppointment.Work_Order__r.Case.asset.Equipment_type_auto__c.contains('Pompe à chaleur')){
                            equipmentTypePAC = true;
                        }else{
                            equipmentTypePAC = false;
                        }
                    }else{
                         equipmentTypePAC = false;
                    }
                

                    //DMG 04/11/2019 Check Burner Section 
                    containBurnerSection = true;
                    if(currentServiceAppointment.Work_Order__r.Case.asset.BurnerBrand__c ==null && 
                        currentServiceAppointment.Work_Order__r.Case.asset.BurnerModel__c ==null && 
                        (currentServiceAppointment.Work_Order__r.Case.asset.Burner_power_in_kw__c ==null || currentServiceAppointment.Work_Order__r.Case.asset.Burner_power_in_kw__c ==0) && 
                        currentServiceAppointment.Work_Order__r.Case.asset.Burner_Serial_number__c ==null && 
                        currentServiceAppointment.Work_Order__r.Case.asset.Burner_Installation_date__c ==null 
                    ){
                        containBurnerSection = false;
                    }

                    adresseCompte =currentServiceAppointment.Account.BillingStreet + ' ' + currentServiceAppointment.Account.BillingPostalCode + ' ' + currentServiceAppointment.Account.BillingCity;
                    System.debug('**** adresseCompte: '+adresseCompte);

                    TimeZone tz = UserInfo.getTimeZone();
                    //Milliseconds to Day
                    offset= tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);

                    if(string.isNotBlank(currentServiceAppointment.ServiceTerritoryId)){
                        List<AgencyAccreditation__c> lstA =  [SELECT Id, name, Numero_SGS__c FROM AgencyAccreditation__c WHERE Agency__c =:currentServiceAppointment.ServiceTerritoryId AND Name LIKE '%SGS%' LIMIT 1];                  
                            if(lstA.size() > 0){
                                noSGS = lstA[0].Numero_SGS__c;
                            }       
                    }
                    if(string.isNotBlank(currentServiceAppointment.Work_Order__c) && (string.isNotBlank(currentServiceAppointment.Work_Order__r.assetId))) {
                        System.debug('**** workOrder 7 workOrder Asset not null ' +currentServiceAppointment.Work_Order__r.assetId + ' '+currentServiceAppointment.Work_Order__c);
                        list <Measurement__c> measurementlst = [SELECT id 
                                                                ,Appareil_de_mesure__c 
                                                                ,Tension_d_alimentation_Phase_neutre__c
                                                                ,Tension_d_alimentation_neutre_terre__c
                                                                ,Intensite_General__c
                                                                ,Intensite_Compresseur__c
                                                                ,Resserrage_bornes_sur_PAC__c
                                                                ,Resserrage_bornes_sur_coffret__c
                                                                ,Disjoncteur_protection__c
                                                                ,Temperature_de_Desurchauffe__c
                                                                ,Temperature_de_surchauffe__c
                                                                ,Temperature_de_refroidissement__c
                                                                ,Temperature_de_sous_refroidissement__c
                                                                ,Temperature_aspiration_compresseur__c
                                                                ,Temperature_refoulement_compresseur__c
                                                                ,Ventilation_Groupe_branches_feuilles__c
                                                                ,Nettoyage_ailettes_vaporateur__c
                                                                ,Verification_ailettes_peigne__c
                                                                ,Nettoyage_filtre_hydraulique__c
                                                                ,Fuite_hydraulique__c
                                                                ,Isolant_tubulaire__c
                                                                ,Fixation_PAC__c
                                                                ,Evacuation_des_condensateurs__c
                                                                ,Nettoyage_carrosserie__c
                                                                ,glycol__c
                                                                ,pH_glycol__c
                                                                ,Pression_hydraulique__c
                                                                ,Debit_soufflage_vaporateur__c
                                                                ,Traces_d_huile__c
                                                                ,Verification_cheminement_tuyauterie__c
                                                                ,T_exterieur__c
                                                                ,T_depart_PAC__c
                                                                ,T_retour_PAC__c
                                                                ,delta_T__c
                                                                ,T_consigne_eau_chauffage_hiver__c
                                                                ,T_enclenchement_appoint__c
                                                                ,T_consigne_confort__c
                                                                ,pente__c
                                                                ,Controle_de_l_intensite_de_l_anode_du_ba__c
                                                                ,T_min_regional__c
                                                                ,T_maxi_retour__c
                                                                ,T_depart_installation__c
                                                                ,T_consigne_ECS__c
                                                                ,T_mesuree_ECS__c
                                                                ,pH_eau_de_chauffage__c

                                                                ,T_Fumee__c
                                                                ,CO_fumees_en_ppm__c
                                                                ,CO_Ambiant_ppm__c
                                                                ,Temperature_eau_chaude_en_C__c
                                                                ,Temperature_eau_froide_en_C__c
                                                                ,Debit_litre_en_L_min__c
                                                                ,Rendement_PCI_Percent__c
                                                                ,Emission_valu_s_de_Nox__c  
                                                                ,Tirage_si_B1__c
                                                                // DMG 25 JUL 2019 : START
                                                                ,CO2_Max_en__c
                                                                ,CO2_Min_en__c
                                                                ,O2_Max__c
                                                                ,O2_Min__c
                                                                ,Pression_gaz_amont_Dynamique_mbar__c
                                                                ,Pression_gaz_amont_Statique_mbar__c
                                                                ,Opacite__c 
                                                                // DMG 25 JUL 2019 : END
                                                                //RRJ 09 AUG START
                                                                ,Puissance__c
                                                                //RRJ 09 AUG 2019 END

                                                                FROM Measurement__c
                                                                WHERE Asset__c =:currentServiceAppointment.Work_Order__r.assetId
                                                                    AND Parent_Work_Order__c = :currentServiceAppointment.Work_Order__c
                                                                ORDER BY createdDate DESC 
                                                                LIMIT 1];
                        //list <Measurement__c> measurementEntretien=[Select id,LastModifiedDate  from Measurement__c where Parent_Work_Order__r.Type__c='Maintenance' and Asset__c =:currentServiceAppointment.Work_Order__r.assetId  order by LastModifiedDate  desc limit 1 ];
                        // list <ContractLineItem> lstCLI = [Select Id,Completion_Date__c,VE__c from ContractLineItem where AssetId=:currentServiceAppointment.Work_Order__r.assetId and VE__c =true order by createdDate desc limit 1];
                        list <ContractLineItem> lstCLI = [Select Id,Completion_Date__c,StartDate,VE__c from ContractLineItem where AssetId=:currentServiceAppointment.Work_Order__r.assetId AND Completion_Date__c != null ORDER BY StartDate Desc];
                        System.debug('***** lstCLI: '+lstCLI.size());
                        list <Measurement__c> measurementRamonage=[Select id,LastModifiedDate,Createddate  from Measurement__c where Parent_Work_Order__r.Reason__c='Chimney sweeping' and Asset__c =:currentServiceAppointment.Work_Order__r.assetId   order by LastModifiedDate  desc limit 1 ];
                        System.debug('***** measurementRamonage: '+measurementRamonage.size());
                        list<Logement__c> lstLogement=[SELECT Id, FullAddress__c FROM Logement__c where id=:lstServiceAppointment[0].Work_Order__r.TECH_LogementId__c];
                        System.debug('***** lstLogement: '+lstLogement.size());
                        List<ContractLineItem> lstConLine = [SELECT ServiceContractId FROM ContractLineItem WHERE AssetId = :lstServiceAppointment[0].Work_Order__r.Case.AssetId AND IsBundle__c = true AND StartDate < :currentServiceAppointment.SchedEndTime.date() AND EndDate > :currentServiceAppointment.SchedEndTime.date() ORDER BY LastModifiedDate DESC LIMIT 1];
                        System.debug('***** lstConLine: '+lstConLine.size());
                        // lstServiceContract = [ SELECT ContractNumber, EndDate FROM ServiceContract WHERE Logement__c =:lstServiceAppointment[0].Work_Order__r.TECH_LogementId__c order by LastModifiedDate  desc limit 1];                                        
                        
                        if(lstConLine.size() > 0){
                            // currentContrat = lstServiceContract[0];
                            currentContrat = [SELECT Id, ContractNumber, EndDate FROM ServiceContract WHERE Id = :lstConLine[0].ServiceContractId LIMIT 1];
                        }
                        
                        if(lstLogement.size() > 0 ){
                           addrLogement= lstLogement[0].FullAddress__c;

                        }
                        // if(measurementEntretien.size() > 0){
                        //     dateEntretien=measurementEntretien[0].LastModifiedDate;
                        // }
                        if(lstCLI.size() > 0){
                            dateEntretien=lstCLI[0].Completion_Date__c;
                        }
                        if(measurementRamonage.size() > 0){
                            dateRamonage=measurementRamonage[0].createddate;
                        }
                        if(measurementlst.size()>0){
                            System.debug('***** measurementlst: '+measurementlst.size());

                            currentMeasurement = measurementlst[0];
                            System.debug('***** currentMeasurement: '+currentMeasurement);
                            Decimal coAmbiant = currentMeasurement.CO_Ambiant_ppm__c;
                            System.debug('***** coAmbiant: '+coAmbiant);
                            coAmbiantStr = coAmbiant < 10? 'inferieur_a_10' : (coAmbiant >= 50 ? 'superieur_ou_egale_50' : (coAmbiant == null ? 'blank':'10_a_50' ));
                            
                        }

                        List<Case> lstCaseClientAnomaly = new List<Case>();
                        lstCaseClientAnomaly = [Select Id,Type,Description,Anomaly_reason__c,recordtype.Name from Case where AssetId =:currentServiceAppointment.Work_Order__r.assetId and recordtype.Name ='Client anomaly' order by createddate desc limit 3];
                        System.debug('**** lstCaseClientAnomaly: '+ lstCaseClientAnomaly.size()); 
                        if(lstCaseClientAnomaly.size() > 0){
                            lstWrapperAnomaly =new List<wrapperAnomalyClient>();
                            for(Case c : lstCaseClientAnomaly){
                                wrapperAnomalyClient ano= new wrapperAnomalyClient(c.Type+ ' - '+c.Anomaly_reason__c,c.Description);
                                lstWrapperAnomaly.add(ano);
                            }  
                            System.debug('**** lstWrapperAnomaly: '+ lstWrapperAnomaly); 
                        }
                    }

                    List<AssignedResource> lstAssignedResource = [select ServiceResource.Name from AssignedResource where ServiceAppointmentId =:currentServiceAppointment.Id order by CreatedDate desc limit 1];
                    if(lstAssignedResource.size() > 0){
                        nomTechnincien =lstAssignedResource[0].ServiceResource.Name;
                    }

                    List<Attachment> lstAttachmentClient = new List<Attachment>();
                    lstAttachmentClient = [select Id,Name from Attachment where ParentId=:lstServiceAppointment[0].Id  and  Name='SignatureClient.png' order by createdDate desc];
                    if(lstAttachmentClient.size() > 0){
                        idSignature = '/servlet/servlet.FileDownload?file='+lstAttachmentClient[0].id;
                    }else{
                        idSignature = 'null';
                    }
                    List<Attachment> lstAttachmentTechnicien = new List<Attachment>();
                    lstAttachmentTechnicien = [select Id,Name from Attachment where ParentId=:lstServiceAppointment[0].Id  and  Name='SignatureTechnician.png' order by createdDate desc];
                     if(lstAttachmentTechnicien.size() > 0){
                        idSignatureTechnicien = '/servlet/servlet.FileDownload?file='+lstAttachmentTechnicien[0].id;
                    }else{
                        idSignatureTechnicien = 'null';
                    }                  

                }
            }            

       }catch(exception e){
            System.debug('**** exception msg: '+ e.getMessage());
       }
    }
}