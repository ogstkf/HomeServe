/**
 * @File Name          : AP51_AddPricebookEntryAuto_TEST.cls
 * @Description        : 
 * @Author             : KJB
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/12/2019         KJB                    Initial Version
**/

@isTest
public class AP51_AddPricebookEntryAuto_TEST {

    static User mainUser;
    static Account testAcc = new Account();
    static Opportunity opp = new Opportunity();
    static list<Product2> lstProd = new list<Product2>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    

    static{

        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            //Create Account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            //Create Opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);            
            insert opp;

            //Create Products
            lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                
                new Product2(Name='Prod2',
                             Famille_d_articles__c='Pièces détachées',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId())
            };
            insert lstProd;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create Pricebook Entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            insert PrcBkEnt;

        }

    }

    @isTest
    public static void checkPriceBookEntry_TEST(){
       System.runAs(mainUser){

            List<Product2> lstProductTocreate = new List<Product2>();

            Test.StartTest();
               AP51_AddPricebookEntryAuto.checkPriceBookEntry(lstProd); 
            Test.StopTest();

            lstProductTocreate = [SELECT Id, Name FROM Product2 WHERE Id =: lstProd[1].Id];
            System.assertEquals(lstProductTocreate.size(),1);

       } 
    } 

}