/**
 * @File Name          : BAT16_WSExternalApi_TEST.cls
 * @description       : 
 * @author            : MNA
 * @group             : 
 * @last modified on  : 20-09-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   22/03/2021   MNA                                   Initial Version
**/
@isTest
public with sharing class BAT16_WSExternalApi_TEST {
    static User adminUser;
    static List<sofactoapp__External_Api__c> lstExtApi;
    static sofactoapp__Ecriture_comptable__c bookEntry;
    static sofactoapp__Raison_Sociale__c raisonSociale;
    static {
        adminUser = TestFactory.createAdminUser('BAT16@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser) {
            //insert raison sociale
            raisonSociale = new sofactoapp__Raison_Sociale__c(sofactoapp__Credit_prefix__c = '520', sofactoapp__Invoice_prefix__c  = '520', Sage_Actif__c = true);
            insert raisonSociale;

            //insert Book Entry
            bookEntry = new sofactoapp__Ecriture_comptable__c(sofactoapp__Raison_sociale__c = raisonSociale.Id);
            insert bookEntry;


            lstExtApi = new List<sofactoapp__External_Api__c>{
                new sofactoapp__External_Api__c(Comptabilise__c = false, date__c = System.today(), Type_Ecriture__c = 'facture_liste', Ecriture_comptable__c = bookEntry.Id),
                new sofactoapp__External_Api__c(Comptabilise__c = false, Type_Ecriture__c = 'liste_reglements', Ecriture_comptable__c = bookEntry.Id),
                new sofactoapp__External_Api__c(Comptabilise__c = true, Type_Ecriture__c = 'liste_remises', Ecriture_comptable__c = bookEntry.Id)
            };
            insert lstExtApi;
        }
    }
    
    @isTest static void testBatch(){
        System.runAs(adminUser){
        	Test.setMock(HttpCalloutMock.class, new sageCallOut());
            Test.startTest();
            //Database.executeBatch(new BAT16_WSExternalApi());
            BAT16_WSExternalApi_facture bat16 = new BAT16_WSExternalApi_facture();
            bat16.manualExecution(200);
            Test.stopTest();
        }
    }

    
    @isTest
    public static void testScheduleBatch_Facture(){
        System.runAs(adminUser){
        	Test.setMock(HttpCalloutMock.class, new sageCallOut());
            Test.startTest();
            System.schedule('Batch BAT16' + Datetime.now().format(),  '0 0 0 * * ?', new BAT16_WSExternalApi_facture());
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testScheduleBatch_Remise(){
        System.runAs(adminUser){
        	Test.setMock(HttpCalloutMock.class, new sageCallOut());
            Test.startTest();
            System.schedule('Batch BAT16' + Datetime.now().format(),  '0 0 0 * * ?', new BAT16_WSExternalApi_remise());
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testScheduleBatch_Reglement(){
        System.runAs(adminUser){
        	Test.setMock(HttpCalloutMock.class, new sageCallOut());
            Test.startTest();
            System.schedule('Batch BAT16' + Datetime.now().format(),  '0 0 0 * * ?', new BAT16_WSExternalApi_reglement());
            Test.stopTest();
        }
    }
    
    public class sageCallOut implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"IsSession":"2F538F9B1C60000402110000009E00A0"}');
            response.setStatusCode(200);
            return response; 
        }
    }
}