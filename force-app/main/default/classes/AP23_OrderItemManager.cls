public with sharing class AP23_OrderItemManager {
 /**
 * @File Name          : AP23_OrderItemManager.cls
 * @Description        : 
 * @Author             : Spoon Consulting (KZE)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         17-10-2019     		    KZE         Initial Version (CT-1127)
**/
    public static void checkOrderStatus(list<OrderItem> lstOI){
        System.debug('starting method AP23_OrderItemManager.checkOrderStatus');
        Set<Id> setOrderIds =new Set<Id>();

        for(OrderItem oi : lstOI){
            setOrderIds.add(oi.OrderId);
        }

        Map<Id, Order> mapOrder = new Map<Id, Order>([SELECT Id, Status, RecordTypeId FROM Order where id IN :setOrderIds]);

        for(OrderItem oi : lstOI){            
            // System.debug('mapOrder.get(oi.OrderId).Status: ' +  mapOrder.get(oi.OrderId).Status);
            if( (   
                    mapOrder.get(oi.OrderId).Status == AP_Constant.OrdStatusAchatApprove ||
                    mapOrder.get(oi.OrderId).Status == AP_Constant.OrdStatusAccuseReceptionInterne  ||
                    mapOrder.get(oi.OrderId).Status == AP_Constant.OrdStatusAccuseReception  ||
                    mapOrder.get(oi.OrderId).Status == AP_Constant.ordStatusEnCoursLivraison  ||
                    mapOrder.get(oi.OrderId).Status == AP_Constant.ordStatusReceptPartiellement||
                    mapOrder.get(oi.OrderId).Status == AP_Constant.OrdStatusSolde 
                )
                && 
                mapOrder.get(oi.OrderId).RecordTypeId == AP_Constant.getRecTypeId('Order', 'Commandes_fournisseurs')
            ){
                oi.addError(Label.AP23_ErrorMsg);
            }
        }
    }
}