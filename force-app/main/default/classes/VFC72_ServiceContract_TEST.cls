/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 08-31-2020
 * @last modified by  : AMO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-10-2020   AMO   Initial Version
**/
@isTest
public with sharing class VFC72_ServiceContract_TEST {
    static User mainUser;
    static Account testAcc = new Account();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstCli;
    static List<Product2> lstProd = new List<Product2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<Id> lstServCOnId = new List<Id>();
    
    static{
        
        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;


        System.runAs(mainUser){
            
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.PersonMobilePhone = '12345678';
            insert testAcc;

            //create sofactoapp__Compte_auxiliaire__c
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create Service Contract
            // servCon = new ServiceContract(name = 'Service Con',
            // Pricebook2Id = lstPrcBk[0].Id,
            // AccountId = testAcc.Id,
            // Contract_Status__c = 'Expired' ,
            // StartDate = System.today(),
            // EndDate = System.today() + 1,
            // // Type__c = 'Collective',
            // Option_P2_avec_seuil__c = false,
            // Type_de_seuil__c = null,
            // Valeur_du_seuil_P2__c = 0);
            // // RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId());
            // insert servCon;

            lstServCon = new List<ServiceContract>{
                new ServiceContract(
                    name = 'Service Con',
                    Pricebook2Id = lstPrcBk[0].Id,
                    AccountId = testAcc.Id,
                    Contract_Status__c = 'Expired' ,
                    StartDate = System.today(),
                    EndDate = System.today() + 1,
                    // Type__c = 'Collective',
                    Option_P2_avec_seuil__c = false,
                    Type_de_seuil__c = null,
                    Valeur_du_seuil_P2__c = 0,
                    Sofactoapp_Mode_de_paiement__c = 'Virement',
                    RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId()
                    )
            };
            insert lstServCon;

            lstServCOnId.add(lstServCon[0].Id);

                        //Create Products
            lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                             ProductCode = 'COUV-DEPL-GAR-CHAM',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId(),
                             IsBundle__c = true),

                new Product2(Name='Prod2',
                             ProductCode = 'DEPL-ZONE',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),

                new Product2(Name='Prod3',
                             ProductCode = AP_Constant.productCodeCouvMoGarCham,
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                TestFactory.createProductAsset('asset')
            };
            insert lstProd;


            //Create Pricebook Entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[2].Id, 150));
            insert lstPrcBkEnt;

            //Create Contract Line Item
            lstCli = new list<ContractLineItem>{
                new ContractLineItem(ServiceContractId = lstServCon[0].Id,
                                    PricebookEntryId = lstPrcBkEnt[0].Id,
                                     Quantity = 5,
                                     UnitPrice = 100,
                                     VE_Number_Counter__c =0)
            };
            insert lstCli;
        }
        
    }  


	@isTest
	public static void doGenerate(){

		System.runAs(mainUser){
			
			Test.startTest();

				PageReference pageRef = Page.VFP72_ServiceContract; 
				pageRef.getParameters().put('lstIds', '['+JSON.serialize(lstServCon[0].Id)+']');
				Test.setCurrentPage(pageRef);

                VFC72_ServiceContract devis = new VFC72_ServiceContract();

	        Test.stopTest();

		}
	}
}