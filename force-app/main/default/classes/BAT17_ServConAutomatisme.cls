/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-09-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-26-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global with sharing class BAT17_ServConAutomatisme implements Database.Batchable <sObject>, Database.Stateful, Database.AllowsCallouts, Schedulable {
    
    global String query;
    
    global Date renouvdate;
    global Date relancedate;
    global Date resildate;
    global Integer count = 0;

    global List<BAT17ContractRestriction__c> lstConRestriction = [SELECT Id, agencyCode__c, Type__c FROM BAT17ContractRestriction__c WHERE Blocked__c = true
                                                                            ORDER BY Type__c ASC, agencyCode__c DESC];
    global List<Decimal> lstDocId;
    global List<ServiceContract> lstSc;
    global List<ContentVersion> lstCV;
    global List<ContentDocumentLink> lstCdl;
    global List<WSTransactionLog__c> logs;
    global List<Task> lstTsk;
    global List<OrgWideEmailAddress> lstOrgWide = [SELECT Id, Address FROM OrgWideEmailAddress];
    global static List<EmailTemplate> lstEmlTmp = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName IN 
                                                    ('Mail_de_renouvellement_contrat', 'Mail_de_relance_contrat')];
    global List<Contact> lstCon;

    global Map<String, ContentDocumentLink> mapcdlToCvTitle;
    global Map<String, String> mapCvIdToScId;
    global Map<Id, String> mapEmailToScId;
    global Map<String, String> mapEmlTmpIdToDevName;
    global Map<String, String> mapEmailConId;
    global Map<String, String> mapOrgIdToEmail;
    global Map<String, String> mapSenderEmailToScId;
    global Map<String, Account> mapAccToId;

    // To run batch for specific date : Database.executeBatch(new BAT17_ServConAutomatisme( Date.newInstance(2019,11,27) , null ));
    global BAT17_ServConAutomatisme(){
        renouvdate = System.Today().addMonths(1);
        relancedate = System.Today().addMonths(-2);
        resildate = System.Today().addMonths(-6);


        query = 'SELECT Id, Contract_Status__c, StartDate, Payeur_du_contrat__c, Agency__r.Email__c '
                +   ' FROM ServiceContract ';
/*
        query += ' WHERE (StartDate =:relancedate AND Contract_Status__c = \'Actif - en retard de paiement\')'
            +   ' OR (StartDate =:resildate AND Contract_Status__c = \'Résiliation pour retard de paiement\')'
            +   ' OR (StartDate =:renouvdate AND Contract_Status__c IN (\'Pending Payment\', \'En attente de renouvellement\'))'
            +   ' ORDER BY StartDate ASC';*/

        query += getQueryCondition() + ' ORDER BY StartDate ASC';       //TEC 788 MNA 01/09/2021

        List<ServiceContract> lstSc = Database.query(query);
        
        if (lstSc != null && lstSc.size() > 0) {
            Integer size = lstSc.size() * 2;
            Set<Id> setEDocId = new Set<Id>();
            lstDocId = new List<Decimal>();
            List<Editique_Document_ID__c> lstEDoc = new List<Editique_Document_ID__c>();
            for (integer i = 0; i < size; i++) {
                lstEDoc.add(new Editique_Document_ID__c());
            }
    
            for (Database.SaveResult sr : Database.insert(lstEDoc)) {
                setEDocId.add(sr.getId());
            }
    
            for (Editique_Document_ID__c eDoc : [SELECT Document_ID__c FROM Editique_Document_ID__c WHERE Id IN :setEDocId]) {
                lstDocId.add(Decimal.valueOf(eDoc.Document_ID__c));
            }
        }
        /*
        if (lstSc != null && lstSc.size() > 0)
            System.enqueueJob(new ChaineEditiqueQueueable(wsChaineEditique.documentId__c + lstSc.size() * 2));*/
    }    

    global Database.QueryLocator start(Database.BatchableContext BC){
        Set<Id> setAccId = new Set<Id>();
        Set<String> setEmail = new Set<String>();

        system.Debug('### query: ' + query);
        
        for (ServiceContract sc: Database.query(query)) {
            if (sc.Payeur_du_contrat__c != null) {
                setAccId.add(sc.Payeur_du_contrat__c);
            }
        }
        
        mapAccToId = new Map<String, Account>();
        for (Account acc : [SELECT Id, RecordTypeId, RecordType.DeveloperName, Name, PersonEmail, BillingStreet, BillingPostalCode, BillingCity,
                                        (SELECT Id, FirstName, LastName, Email FROM Contacts WHERE Contact_principal__c = true)
                                        FROM Account WHERE Id=:setAccId]) {

            mapAccToId.put(acc.Id, acc);
            String Email = getEmail(acc);
            if (Email != null) {
                setEmail.add(Email);
            }
        }
        lstCon = [SELECT Id, Email FROM Contact WHERE EMail IN : setEmail];

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ServiceContract> lstServContr) {
        system.debug('### : ' + lstServContr.size() );
        
        lstSc = new List<ServiceContract>();
        lstCV = new List<ContentVersion>();
        lstCdl = new List<ContentDocumentLink>();
        logs = new List<WSTransactionLog__c>();
        lstTsk = new List<Task>();
        List<Messaging.SingleEmailMessage> lstEmailToSend = new List<Messaging.SingleEmailMessage>();
        
        mapcdlToCvTitle = new Map<String, ContentDocumentLink>();
        mapCvIdToScId = new Map<String, String>();
        mapEmailToScId = new Map<Id, String>();
        mapEmlTmpIdToDevName = new Map<String, String>();
        mapEmailConId = new Map<String, String>();
        mapOrgIdToEmail = new Map<String, String>();
        mapSenderEmailToScId = new Map<String, String>();
        
        for (EmailTemplate emlTmp : lstEmlTmp) {
            mapEmlTmpIdToDevName.put(emlTmp.DeveloperName, emlTmp.Id);
        }
        System.debug(lstCon);

        if (lstCon != null) {
            for (Contact con : lstCon) {
                mapEmailConId.put(con.Email, con.Id);
            }
        }

        for (OrgWideEmailAddress orgWide: lstOrgWide) {
            mapOrgIdToEmail.put(orgWide.Address, orgWide.Id);
        }


        for(ServiceContract servContr : lstServContr){
            if (servContr.Agency__c != null && servContr.Agency__r.Email__c != null)
                mapSenderEmailToScId.put(servContr.Id, servContr.Agency__r.Email__c);
            
            String Email = null;
            Boolean hasAddress = false;
            String template;
            if (servContr.StartDate == renouvdate) {
                template = 'RENOUV1_TCH';
            }
            else if (servContr.StartDate == relancedate) {
                template = 'RENOUV2_TCH';
            }
            else if (servContr.StartDate == resildate) {
                template = 'RESIL_TCH';
            }

            Account acc = mapAccToId.get(servContr.Payeur_du_contrat__c);
            if (servContr.Payeur_du_contrat__c != null)
                Email = getEmail(acc);

            if (Email == null || template == 'RESIL_TCH') {             /// Si il n'y a pas d'mail on recherche l'adresse
                                                                        /// La résiliation doit toujours être envoyée par courrier
                hasAddress = checkAddress(acc);
                Email = null;                                           
            }


            if (Email != null || hasAddress) {
                System.debug(hasAddress);
                Boolean isEmail = (Email == null ? false : true);
                
                //1er appel génération PDF
                Map<String , Object> mapResponse = (Map<String , Object>) WS13_ChaineEditiquev2.createJSON(servContr.Id, true, true, true, template, lstDocId[count], true);
                count++;
            
                System.debug(mapResponse);

                ServiceContract sc = (ServiceContract) mapResponse.get('Object');
                lstSc.add(sc);
                
                String response = String.valueOf(mapResponse.get('String'));
                Map<String, Object> mapObjResponse = (Map<String, Object>) JSON.deserializeUntyped(response);
                Integer statusCode = Integer.valueOf(mapObjResponse.get('statusCode'));
                System.debug(response);
                if (statusCode < 200 || statusCode >= 300) {                            //Erreur
                    System.debug(String.valueOf(mapObjResponse.get('message')));
                }
                else {
                    
                    ContentVersion cv = (ContentVersion) mapResponse.get('ContentVersion');
                    lstCV.add(cv);

                    ContentDocumentLink cdl = (ContentDocumentLink) mapResponse.get('ContentDocumentLink');
                    mapcdlToCvTitle.put(cv.Title, cdl);

                    if (Email != null) {
                        mapEmailToScId.put(sc.Id, Email);
                    }
                    else {
                        //2ème appel acheminement
                        mapResponse = (Map<String , Object>) WS13_ChaineEditiquev2.createJSON(servContr.Id, false, true, false, template, lstDocId[count], false);
                        count++;
                        
                        sc = (ServiceContract) mapResponse.get('Object');
                        lstSc.remove(lstSc.size() - 1);
                        lstSc.add(sc);

                        response = String.valueOf(mapResponse.get('String'));
                        mapObjResponse = (Map<String, Object>) JSON.deserializeUntyped(response);
                        statusCode = Integer.valueOf(mapObjResponse.get('statusCode'));
                        
                        if (statusCode < 200 && statusCode >= 300) {                            //Erreur
                            System.debug(String.valueOf(mapObjResponse.get('message')));
                        }
                        else {
                            
                            String type = (template == 'RENOUV1_TCH' ? 'renouvellement' : 
                                            (template == 'RENOUV2_TCH' ? 'relance' : 
                                            (template == 'RESIL_TCH' ? 'résiliation' : '' )));

                            Task tsk = new Task(Subject = 'Envoi du courrier de '+ type + ' le ' + System.now().format('dd/MM/yyyy à HH:mm'), ActivityDate= System.today(), WhatId = sc.Id, Status = 'Completed');
                            lstTsk.add(tsk);
                        }
                    }
                }


                List<WSTransactionLog__c> lstLog = (List<WSTransactionLog__c>) mapResponse.get('Log');
                logs.addAll(lstLog);
                
            }

        }
        
        if (lstSc != null && lstSc.size() > 0) {
            try {
                update lstSc;
            } catch (Exception e) {
                System.debug('Problème lors de la mise à jour du contrat: ' + e.getMessage());
            }
        }
/*
        if (logs != null && logs.size() > 0)
            insert logs;*/

        for (WSTransactionLog__c log : logs) {
            System.debug(log);
        }

        if (lstCV != null && lstCV.size() > 0) {
            insert lstCV;
            lstCV = new List<ContentVersion>([SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Title IN : mapcdlToCvTitle.keySet()]);

            for (ContentVersion cv : lstCV) {
                ContentDocumentLink cdl = mapcdlToCvTitle.get(cv.Title);
                cdl.ContentDocumentId = cv.ContentDocumentId;
                lstCdl.add(cdl);
                mapCvIdToScId.put(cdl.LinkedEntityId, cv.Id);
            }
        }

        System.debug(lstCdl);

        if (lstCdl != null && lstCdl.size() > 0)
            insert lstCdl;
        
        for (ServiceContract sc: lstSc) {
            if (mapEmailToScId.containsKey(sc.Id)) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
                String Email = mapEmailToScId.get(sc.Id);
                String templateId;
                if (sc.StartDate == renouvdate) {
                    templateId = mapEmlTmpIdToDevName.get('Mail_de_renouvellement_contrat');
                }
                else if (sc.StartDate == relancedate) {
                    templateId = mapEmlTmpIdToDevName.get('Mail_de_relance_contrat');
                }
                
                String orgId = '';

                if (mapSenderEmailToScId.containsKey(sc.Id)) {
                    String SenderEmail = mapSenderEmailToScId.get(sc.Id);
                    if (mapOrgIdToEmail.containsKey(SenderEmail))
                        orgId = mapOrgIdToEmail.get(SenderEmail);
                }
    
                if (orgId != '')
                    mail.setOrgWideEmailAddressId(orgId);

                mail.setTemplateID(templateId);
                mail.setUseSignature(false);
                mail.setWhatId(sc.Id);
                mail.setTargetObjectId(mapEmailConId.get(Email));
                mail.setEntityAttachments(new List<String>{mapCvIdToScId.get(sc.Id)});
                
                lstEmailToSend.add(mail);
            }
        }

        if (lstEmailToSend.size() > 0)
            Messaging.sendEmail(lstEmailToSend);

        if (lstTsk.size() > 0)
            insert lstTsk;
    }

    global void finish(Database.BatchableContext BC) {
        
    }

    global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT17_ServConAutomatisme');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT17_ServConAutomatisme(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT17_ServConAutomatisme(), 1);
            
        }
    }

    //TEC 788 MNA 31/08/2021
    private String getQueryCondition() {
        Map<String, String> mapConResAgencyByType = new Map<String, String>();
        String queryCondition = ' WHERE';
        Boolean isFirst = true;

        //#region Map assignement value
        if (lstConRestriction != null && lstConRestriction.size() > 0 ) {
            for (BAT17ContractRestriction__c conRestriction : lstConRestriction) {
                String type = '';
                if (conRestriction.Type__c.toLowerCase() == 'résiliation' || conRestriction.Type__c.toLowerCase() == 'resiliation') {
                    type = 'resiliation';
                }
                else {
                    type = conRestriction.Type__c.toLowerCase();
                }

                if (!mapConResAgencyByType.containsKey(type)) {
                    if (conRestriction.agencyCode__c.toLowerCase() == 'toutes')
                        mapConResAgencyByType.put(type, 'toutes');
                    else
                        mapConResAgencyByType.put(type, '\'' + conRestriction.agencyCode__c + '\'');
                }
                else {
                    if (mapConResAgencyByType.get(type).toLowerCase() != 'toutes')
                        if (conRestriction.agencyCode__c.toLowerCase() == 'toutes')
                            mapConResAgencyByType.put(type, 'toutes');
                        else 
                            mapConResAgencyByType.put(type, mapConResAgencyByType.get(type) + ',\'' + conRestriction.agencyCode__c + '\'');
                }
            }
        }

        //#endregion

        //#region Construct queryCondition 
        if (!mapConResAgencyByType.containsKey('relance') || (mapConResAgencyByType.containsKey('relance') && mapConResAgencyByType.get('relance') != 'toutes')) {
            queryCondition += ' (';

            if (!mapConResAgencyByType.containsKey('relance')) 
                queryCondition += '(StartDate =:relancedate AND Contract_Status__c = \'Actif - en retard de paiement\')';
            else
                queryCondition += '(StartDate =:relancedate AND Contract_Status__c = \'Actif - en retard de paiement\' AND Agency__r.Agency_Code__c NOT IN ('+ mapConResAgencyByType.get('relance') +'))';

            isFirst = false;
        }

        if (!mapConResAgencyByType.containsKey('resiliation') || (mapConResAgencyByType.containsKey('resiliation') && mapConResAgencyByType.get('resiliation') != 'toutes')) {
            if (isFirst)
                queryCondition += ' (';
            else
                queryCondition += ' OR ';

            if (!mapConResAgencyByType.containsKey('resiliation'))
                queryCondition += '(StartDate =:resildate AND Contract_Status__c = \'Résiliation pour retard de paiement\')';
            else
                queryCondition += '(StartDate =:resildate AND Contract_Status__c = \'Résiliation pour retard de paiement\' AND Agency__r.Agency_Code__c NOT IN ('+ mapConResAgencyByType.get('resiliation') +'))';

            isFirst = false;
        }

        if (!mapConResAgencyByType.containsKey('renouvellement') || (mapConResAgencyByType.containsKey('renouvellement') && mapConResAgencyByType.get('renouvellement') != 'toutes')) {
            if (isFirst)
                queryCondition += ' (';
            else
                queryCondition += ' OR ';

            if (!mapConResAgencyByType.containsKey('renouvellement'))
                queryCondition += '(StartDate =:renouvdate AND Contract_Status__c IN (\'Pending Payment\', \'En attente de renouvellement\'))';
            else
                queryCondition += '(StartDate =:renouvdate AND Contract_Status__c IN (\'Pending Payment\', \'En attente de renouvellement\' AND Agency__r.Agency_Code__c NOT IN ('+ mapConResAgencyByType.get('renouvellement') +')))';

            isFirst = false;
        }

        if (isFirst)
            queryCondition += ' Id = null';
        else 
            queryCondition += ' )';

        queryCondition += ' AND Agency__c != null';

        //#endregion
        
        return queryCondition;
    }
    
    private String getEmail(Account acc) {
        if (acc.RecordType.DeveloperName == 'PersonAccount') {
            return acc.PersonEmail;
        }
        else if (acc.RecordType.DeveloperName == 'BusinessAccount') {
            if (acc.Contacts == null || acc.Contacts.size() == 0) {
                return null;
            }
            else {
                return acc.Contacts[0].Email;
            }
        }
        return null;
    } 

    private Boolean checkAddress (Account acc) {
        if (acc.BillingStreet == null || acc.BillingPostalCode == null || acc.BillingCity == null) {
            return false;
        }
        else {
            return true;
        }
    }
    
}