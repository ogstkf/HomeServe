/**
 * @File Name          : AP55_WSVaillant_TEST.cls
 * @Description        : 
 * @Author             : KJB   
 * @Group              : 
 * @Last Modified By   : KJB
 * @Last Modified On   : 23/12/2019, 13:41:00
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * ---    -----------       -------           ------------------------ 
 * 1.0    20/12/2019         KJB              Initial Version  (CT-1231)
 * ---    -----------       -------           ------------------------ 
**/

@isTest
public class AP55_WSVaillant_TEST {

    static User mainUser;
    static Account testAcc1 = new Account();
    static Account testAcc2 = new Account();
    static List<Product2> lstProd = new List<Product2>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry PrcBkEnt = new PricebookEntry();
    static List<Order> lstOrders = new List<Order>();  
    static List<OrderItem> lstOrderItems = new List<OrderItem>();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static List<ServiceTerritory> lstServTerritory;
    static OperatingHours opHrs = new OperatingHours();
    static List<MappingAgenceFourn__c> mappings = new List<MappingAgenceFourn__c>();

    static{

        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){

            //Create Account
            testAcc1 = TestFactory.createAccount('Test1');
            testAcc1.Code_client_CHAM_chez_le_fournisseur__c = '1234';
            testAcc1.Type_EDI__c = 'Vaillant';
           
            insert testAcc1;

            testAcc2 = TestFactory.createAccount('Test2');
            testAcc2.Code_client_CHAM_chez_le_fournisseur__c = '1234';
            testAcc2.Livraison_express_possible__c = true;
            testAcc2.Type_EDI__c = 'Saunier Duval';
            insert testAcc2;
            
            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;
            
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;
            
            lstServTerritory = new List<ServiceTerritory>{
                TestFactory.createServiceTerritory('SGS - test Territory', opHrs.Id, raisonSocial.Id),
                TestFactory.createServiceTerritory('SGS - test Territory 2', opHrs.Id, raisonSocial.Id)
            };
            insert lstServTerritory;
            
            MappingAgenceFourn__c mapping1 = new MappingAgenceFourn__c();
            mapping1.ID_Fournisseur__c = testAcc1.Id;
            mapping1.Code_Agence__c = lstServTerritory.get(0).Agency_Code__c;
			mapping1.Mapping__c = 'customer1';
            mapping1.Name = 'mapping1';
            
            MappingAgenceFourn__c mapping2 = new MappingAgenceFourn__c();
            mapping2.ID_Fournisseur__c = testAcc2.Id;
            mapping2.Code_Agence__c = lstServTerritory.get(1).Agency_Code__c;
			mapping2.Mapping__c = 'customer2';
            mapping2.Name = 'mapping2';
            
            mappings.add(mapping1);
            mappings.add(mapping2);
            
            insert mappings;
            //Create Products
            lstProd = new List<Product2>{
                      new Product2(Name = 'Test Product 1', ProductCode='VA00000000'),
                      new Product2(Name = 'Test Product 2', ProductCode = 'VA00000001')
            };
            insert lstProd;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create Pricebook Entry
            PrcBkEnt  = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstProd[0].Id, 150);
            PrcBkEnt.reference_fournisseur__c = 'VA00000000';
            insert PrcBkEnt;

            //Create Orders
            lstOrders = new List<Order>{
                        new Order(AccountId = testAcc1.Id,
                                  Name = 'Test Order 1',
                                  Type_de_livraison_de_la_commande__c = 'Standard',
                                  OwnerId = mainUser.Id,
                                  EffectiveDate = Date.today(),
                                  Status = 'Demande d\'achats',
                                  Mode_de_transmission__c='EDI',
                                  Agence__c = lstServTerritory.get(0).Id,
                                  Pricebook2Id = lstPrcBk[0].Id),
                        new Order(AccountId = testAcc2.Id,
                                  Name = 'Test Order 2',
                                  Type_de_livraison_de_la_commande__c = 'Express',
                                  OwnerId = mainUser.Id,
                                  EffectiveDate = Date.today(),
                                  Status = 'Demande d\'achats',
                                  Mode_de_transmission__c='EDI',
                                  Agence__c = lstServTerritory.get(1).Id,
                                  Pricebook2Id = lstPrcBk[0].Id)
                            /*,
                            new Order(AccountId = testAcc1.Id,
                                  Name = 'Test Order 3',
                                  Type_de_livraison_de_la_commande__c = 'Standard',
                                  OwnerId = mainUser.Id,
                                  EffectiveDate = Date.today(),
                                  Status = 'Demande d\'achats',
                                  Mode_de_transmission__c='Fax',
                                  Pricebook2Id = lstPrcBk[0].Id)*/
            };
            insert lstOrders;

            //Create Order Items
            lstOrderItems = new List<OrderItem>{
                        new OrderItem(OrderId = lstOrders[0].Id,
                                      Quantity = decimal.valueof('2'),
                                      UnitPrice = 5,
                                      Product2Id = lstProd[0].Id,
                                      PricebookEntryId=PrcBkEnt.Id),
                        new OrderItem(OrderId = lstOrders[1].Id,
                                      Quantity =  decimal.valueof('10'),
                                      UnitPrice = 5,
                                      Product2Id = lstProd[1].Id,
                                      PricebookEntryId=PrcBkEnt.Id)
            };
            insert lstOrderItems;

        }

    }

    @isTest
    public static void saveFile_TEST1(){

         System.runAs(mainUser){
            Test.startTest();
                AP55_WSVaillant.instantiateMethod(lstOrders[0].Id);
            Test.stopTest(); 
        }

    }

    @isTest
    public static void saveFile_TEST2(){

        System.runAs(mainUser){
            Test.startTest();
                AP55_WSVaillant.instantiateMethod(lstOrders[1].Id);
            Test.stopTest(); 
        }

    }

    @isTest
    public static void saveFile_TEST3(){

        System.runAs(mainUser){
            Test.startTest();
                //AP55_WSVaillant.saveFile(null, null);
            Test.stopTest(); 
        }

    }

}