/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-22-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-19-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public without sharing class WS14_APISage_TEST {
    static User mainUser;
    static List<sofactoapp__External_Api__c> lstExtApi;
    static {
        mainUser = TestFactory.createAdminUser('WS14@test.com', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            lstExtApi = new List<sofactoapp__External_Api__c>{
                new sofactoapp__External_Api__c(Journal__c = 'VE1', Type_piece__c = 'FC', type_ligne__c = 'G', date__c = System.today(), Type_Ecriture__c = 'facture_liste'),
                new sofactoapp__External_Api__c(Journal__c = 'VE1', Type_piece__c = 'FC', type_ligne__c = 'X', Type_Ecriture__c = 'facture_liste'),
                new sofactoapp__External_Api__c(Type_Ecriture__c = 'liste_reglements'),
                new sofactoapp__External_Api__c(Type_Ecriture__c = 'liste_remises')
            };
            insert lstExtApi;
            lstExtApi = [SELECT Id, Name, analytique_client__c, analytique_departement__c, analytique_etablissement__c, analytique_produit__c, code_client__c,
                          Comptabilise__c, compte_general__c, date__c, date_echeance__c, devise__c, etablissement__c, Journal__c, libelle__c, mode_reglement__c,
                          montant__c, nom__c, num_contrat__c, num_equipement__c, num_facture__c, numero_piece__c, sofactoapp__Plateforme__c, prenom__c,
                          profil_tva__c, raison_sociale__c, sofactoapp__Response__c, sens__c, SIRET__c, Societe__c, sofactoapp__Status_Code__c,
                          sofactoapp__TechError__c, tiers__c, type_ligne__c, Type_piece__c, URL__c, Type_Ecriture__c, Analytique_categorie_produit__c,
                          Libelle_remise__c, DateEnvoi__c
                          		FROM sofactoapp__External_Api__c];
        }
    }

    @isTest static void appelWS_Test () {
        Test.setMock(HttpCalloutMock.class, new sageCallOutSuccess());
        Test.startTest();
        WS14_APISage.appelWS(lstExtApi);
        Test.stopTest();
    }
    
    @isTest static void appelWSError_Test () {
        Test.setMock(HttpCalloutMock.class, new sageCallOutFailed());
        Test.startTest();
        WS14_APISage.appelWS(lstExtApi);
        Test.stopTest();
    }
    
    @isTest static void appelWSError_Test2 () {
        Test.setMock(HttpCalloutMock.class, new sageCallOutFailed2());
        Test.startTest();
        WS14_APISage.appelWS(lstExtApi);
        Test.stopTest();
    }

    public class sageCallOutSuccess implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"IsSession":"2F538F9B1C60000402110000009E00A0"}');
            response.setStatusCode(200);
            return response; 
        }
    }
    
    public class sageCallOutFailed implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":1,"ErreurNb":2,"Errors":[{"field":"societe","Error":"Société VB inexistant","Occurence":"1","external_id":"' + lstExtApi[2].Name + '"}'
                             								+',{"field":"compte_general","Error":"Compte  inexistant","Occurence":"2","external_id":"' + lstExtApi[3].Name + '"}'
                             								
                             								+']}');
            response.setStatusCode(200);
            return response; 
        }
    }
    
    public class sageCallOutFailed2 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":2,"Errors":["Could not convert variant of type (UnicodeString) into type (Date)"]}');
            response.setStatusCode(200);
            return response; 
        }
    }
    
}