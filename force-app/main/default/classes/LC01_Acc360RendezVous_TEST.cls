/**
 * @File Name          : LC01_Acc360Equipements_TEST.cls
 * @Description        : Test class for LC01_Acc360Equipements
 * @Author             : ASO
 * @Group              : Spoon Consulting
 *==============================================================================
 * Ver         Date                     Author                 Modification
 *==============================================================================
 * 1.0        02/08/2019, 14:28:15       ASO                   Initial Version
**/
@isTest
public class LC01_Acc360RendezVous_TEST {
    static User adminUser;
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<ContractLineItem> lstTestContLnItem = new List<ContractLineItem>();
    static List<ServiceContract> lstServiceContracts = new List<ServiceContract>();
    
    static {
        adminUser = TestFactory.createAdminUser('C01_Acc360RendezVous_TEST@test.com', TestFactory.getProfileAdminId());
		insert adminUser;

        System.runAs(adminUser){

            //creating account
            testAcc = TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating logement
            for(integer i =0; i<5; i++){
                Logement__c lgmt = TestFactory.createLogement('logementTest'+i, 'logement@test.com'+i, 'lgmt'+i);
                lgmt.Postal_Code__c = 'lgmtPC'+i;
                lgmt.City__c = 'lgmtCity'+i;
                lgmt.Account__c = testAcc.Id;
                lstTestLogement.add(lgmt);
            }
            insert lstTestLogement;

            //creating products
            lstTestProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstTestProd.add(TestFactory.createProduct('Entretien gaz'));
            
            insert lstTestProd;

            // creating Assets
            for(Integer i = 0; i<5; i++){
                Asset eqp = TestFactory.createAccount('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[i].Id);
                eqp.Product2Id = lstTestProd[0].Id;
                eqp.Logement__c = lstTestLogement[i].Id;
                eqp.AccountId = testacc.Id;
                lstTestAssset.add(eqp);
            }
            insert lstTestAssset;

            //create pricebook 
            PriceBook2 aPriceBook = new PriceBook2(
                Id = Test.getStandardPricebookId(),
                Active_online__c = TRUE,
                IsActive = TRUE,
                Active_from__c = date.today().addMonths(-1),
                Active_to__c = date.today().addMonths(1)
            );
            update aPriceBook;

            //create pricebookentry
            List<PricebookEntry> lstPriceBooks = new List<PricebookEntry>();
            lstPriceBooks.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstTestProd[0].Id, 500 ));
            insert lstPriceBooks;
        
            //create service contract
            lstServiceContracts.add(TestFactory.createServiceContract('test', testAcc.Id));
            lstServiceContracts[0].PriceBook2Id = aPriceBook.Id;
            insert lstServiceContracts;

            //insert contractline
            ContractLineItem ctrt = TestFactory.createContractLineItem( (String)lstServiceContracts[0].Id, (String)lstPriceBooks[0].Id, 5, 10);
            ctrt.VE__C = true;
            // ctrt.Status = 'Active';
            ctrt.StartDate = Date.today().addDays(-1);
            ctrt.EndDate = Date.today().addDays(10);
            ctrt.AssetId = lstTestAssset[0].Id;
            lstTestContLnItem.add(ctrt);
            insert lstTestContLnItem;

            // create case
            case cse = new case(AccountId = testacc.id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id);
            insert cse;


            WorkType wrkType1 = TestFactory.createWorkType('wrkType1','Hours',1);
            wrkType1.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            WorkType wrkType2 = TestFactory.createWorkType('wrkType2','Hours',2);
            wrkType2.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            WorkType wrkType3 = TestFactory.createWorkType('wrkType3','Hours',3);
            wrkType3.Type__c = AP_Constant.wrkTypeTypeMaintenance;

            lstWrkTyp = new list<WorkType>{wrkType1, wrkType2, wrkType3};
            insert lstWrkTyp;

            System.debug('lstWrkTyp ' + lstWrkTyp);

            for(Integer i=0; i<3; i++){
                WorkOrder wrkOrd = TestFactory.createWorkOrder();
                wrkOrd.caseId = cse.id;
                wrkOrd.AccountId = testAcc.Id;
                wrkOrd.WorkTypeId = lstWrkTyp[0].Id;
                lstWrkOrd.add(wrkOrd);
            }
            insert lstWrkOrd;

            ServiceAppointment serApp1 = TestFactory.createServiceAppointment(lstWrkOrd[0].Id);
            serApp1.DueDate = Date.today().addDays(10);
            serApp1.Status = '';
            ServiceAppointment serApp2 = TestFactory.createServiceAppointment(lstWrkOrd[1].Id);
            serApp2.DueDate = Date.today().addDays(10);
            serApp2.Status = 'In Progress';
            ServiceAppointment serApp3 = TestFactory.createServiceAppointment(lstWrkOrd[2].Id);
            serApp3.DueDate = Date.today().addDays(10);
            serApp3.Status = 'In Progress';


            lstServiceApp = new List<ServiceAppointment>{serApp1, serApp2, serApp3};
            insert lstServiceApp;
        }
    }
    @isTest
    static void testFetch(){
        Test.startTest();
        LC01_Acc360RendezVous.fetchRDV(testAcc.Id);
        Test.stopTest();
    }

    @isTest
    static void testAnnulerRdv(){
        System.runAs(adminUser){
            ServiceAppointmentTriggerHandler.isTest = false;
            Test.startTest();
            try{
                LC01_Acc360RendezVous.annulerRdv(lstServiceApp[0].Id);
            }catch(Exception e){
                System.debug('######'+e);
            }
            Test.stopTest();
        }
    }

    @isTest
    static void testFetchPickVal(){
        System.runAs(adminUser){
            Test.startTest();
            LC01_Acc360RendezVous.getPicklistLabel('ServiceAppointment', 'Status', 'In Progress' );
            Test.stopTest();
        }
    }

}