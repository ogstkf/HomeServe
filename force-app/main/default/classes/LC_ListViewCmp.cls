/**
 * @File Name          : LC_ListViewCmp.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 06/05/2020, 09:55:00
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/05/2020   ZJO     Initial Version
**/
public with sharing class LC_ListViewCmp {

    @AuraEnabled
    public static DataTableResponse getCliRecords(String strObjectName, String strFieldSetName, String servConId){

        //Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);

        //To hold the table hearders 
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
        
        //Field to be queried - fetched from fieldset
        List<String> lstFieldsToQuery = new List<String>();

        //The final wrapper response to return to component
        DataTableResponse response = new DataTableResponse();
    
        for( Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ){
            String dataType = String.valueOf(eachFieldSetMember.getType()).toLowerCase(); 
            String fieldName = String.valueOf(eachFieldSetMember.getFieldPath());
            String labelName = String.valueOf(eachFieldSetMember.getLabel());
            
            //change date format to text
            if(dataType == 'date'){
                dataType = 'text';
            }

            DataTableColumns datacolumns = null;

            if (dataType == 'reference'){
                if (fieldName == 'Collective_Account__c'){
                    datacolumns = new DataTableColumns('Client collectif', 'ClientCollectif', 'text', true);

                }else if (fieldName == 'AssetId'){
                    datacolumns = new DataTableColumns('Asset Name', 'AssetName', 'text', true);

                }else if (fieldName == 'Product2Id'){
                    datacolumns = new DataTableColumns('Product Name', 'ProductName', 'text', true);
                }
            }else{
                datacolumns = new DataTableColumns(labelName, fieldName, dataType, true);
                lstFieldsToQuery.add(fieldName);
            }

            //Set editable on required columns
            if (labelName.toLowerCase() == 'line item number'){
                datacolumns.editable = false;
            }else{
                datacolumns.editable = true;
            }  
             
			lstDataColumns.add(datacolumns);
        }

        //Form an SOQL to fetch the data - Set the wrapper instance and return as response
        if(! lstDataColumns.isEmpty()){            
            response.lstDataTableColumns = lstDataColumns;
            String fieldSetValues = String.join(lstFieldsToQuery, ', ');
            String query = 'SELECT Id, ServiceContractId, UnitPrice, Asset.Name, Product2.Name, Collective_Account__r.Name, ' + fieldSetValues + ' FROM ContractLineItem WHERE ServiceContractId = \'' + servConId + '\' AND UnitPrice > 0';
            System.debug('### query:' + query);
            response.lstDataTableData = Database.query(query);
        }

        return response;     
    }

    @AuraEnabled    
    public static boolean updateCliRecords(List<ContractLineItem> updatedCliList) {            
        try {         
            update updatedCliList;  
            return true;          
        } catch(Exception e) {         
            return false;           
        }        
    }    

    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled       
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        @AuraEnabled
        public Boolean sortable {get;set;}
        @AuraEnabled
        public Boolean editable {get;set;}
        
        public DataTableColumns(String label, String fieldName, String type, Boolean sortable){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;  
            this.sortable = sortable;         
        }
    }

    //Wrapper calss to hold response
    public class DataTableResponse {
        @AuraEnabled
        public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled
        public List<sObject> lstDataTableData {get;set;}                
        
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    }

}