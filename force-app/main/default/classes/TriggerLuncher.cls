/**
 * @File Name          : TriggerLuncher.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 21/11/2019, 15:13:10
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    21/11/2019   AMO     Initial Version
**/
public with sharing class TriggerLuncher {
    private final List<String> PROJECTS = new List<String>{'Core','FSL'};
    private List<ITriggerHandleable> handlers = new List<ITriggerHandleable>();
    string objectName;

    public TriggerLuncher(String objectName){
        this.objectName = objectName;
        for(String project : PROJECTS){
            
            Type t = Type.forName(project+'_'+objectName+'Handler');
            if(t != null){
                handlers.add((ITriggerHandleable)t.newInstance());
            }
        }
    }

    public void execute(){
        Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains(this.objectName+'Trigger')){
            if(Trigger.isBefore){
                if(Trigger.isInsert){
                    for(ITriggerHandleable handler : handlers){
                        handler.onBeforeInsert(Trigger.new);
                    }
                }
                if(Trigger.isUpdate){
                    for(ITriggerHandleable handler : handlers){
                        handler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
                    }
                }
                if(Trigger.isDelete){
                    for(ITriggerHandleable handler : handlers){
                        handler.onBeforeDelete(Trigger.old);
                    }
                }
            }
            else if(Trigger.isAfter){
                if(Trigger.isInsert){
                    for(ITriggerHandleable handler : handlers){
                        handler.onAfterInsert(Trigger.new);
                    }
                }
                if(Trigger.isUpdate){
                    for(ITriggerHandleable handler : handlers){
                        handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
                    }
                }
                if(Trigger.isDelete){
                    for(ITriggerHandleable handler : handlers){
                        handler.onAfterDelete(Trigger.old);
                    }
                }
            }

        }
    }

}