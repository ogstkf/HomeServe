/**
 * @File Name          : ServiceContractTriggerHandler.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08-12-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    14/11/2019   AMO     Initial Version
 * 1.1    14/11/2019   DMU     Added logic for CT-1177
 * 1.2    16/12/2019   LGO    Added logic for 
 * 1.3    25/01/2020   DMU    Added logic for AP62 (Planification VE)
 * 1.4    21/07/2020   DMU    Added method handleBeforeInsert
**/

public class MaintenanceAssetTriggerHandler {

    Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
	
    public void handleAfterInsert(List<MaintenanceAsset> lstMA){
        system.debug('***in handleAfterInsert: ');

        List<String> lstMPId = new List<String>();
        
        for(Integer i=0; i<lstMA.size(); i++){
            
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP74')){
                if (lstMA[i].MaintenancePlanId <> null) {           		
                    lstMPId.add(lstMA[i].MaintenancePlanId);
                }
            }
        }

        if (lstMPId.size() > 0){
            AP74_MaintenancePlan.updateSC(lstMPId);
        }
    }
    
}