/**
 * @File Name          : OrderItemTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 18/12/2019, 15:37:18
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    27/11/2019   AMO     Initial Version
 * 1.1    19/06/2020   DMU     Commented AP23_OrderItemManager, AP38_ProdReqstLineManager, AP41_OrderConditions due to homeServe Project
**/
public with sharing class OrderItemTriggerHandler {

    Bypass__c userBypass;

    public OrderItemTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }

    public void handleBeforeInsert(list<OrderItem> lstNewOrderItem){
        // List<OrderItem> lstOrderItemBI = new List<OrderItem>();
        //DMU 20200619 - Commented AP23_OrderItemManager due to homeServe Project
        //AP23        
        // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP23')){            
        //     AP23_OrderItemManager.checkOrderStatus(lstNewOrderItem);
        // }
        //AP41
        for(Integer i = 0; i < lstNewOrderItem.size(); i++){
            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41')){
            //     if(lstNewOrderItem[i].Remise_mode_de_transmission__c != null && lstNewOrderItem[i].Remise100__c != null && lstNewOrderItem[i].ListPrice != null){
            //             lstOrderItemBI.add(lstNewOrderItem[i]);
            //     }
            // }
        }
        //DMU 20200619 Commented AP41_OrderConditions due to homeserve project
        // if(lstOrderItemBI.size() > 0){
        //     AP41_OrderConditions.updateListUnitPrince(lstOrderItemBI);
        // }
    }

    public void handleBeforeUpdate(List<OrderItem> lstNew, List<OrderItem> lstOld){
        //AP41
        // List<OrderItem> lstOrderItem = new List<OrderItem>();

        for(Integer i = 0; i < lstNew.size(); i++){
            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41')){
            //     if(lstNew[i].Remise_mode_de_transmission__c != lstOld[i].Remise_mode_de_transmission__c && lstNew[i].Remise_mode_de_transmission__c != null && lstNew[i].Remise100__c != null){
            //         lstOrderItem.add(lstNew[i]);
            //     }
            // }
        }
        //DMU 20200619 - Commented AP41_OrderConditions due to homeserve project
        // if(lstOrderItem.size() > 0){
        //     AP41_OrderConditions.updateListUnitPrince(lstOrderItem);
        // }
    }

    public void handleAfterInsert(List<OrderItem> lstNewOrderItems){
        
        // Set<Id> setOrdersToUpdate = new set<Id>();
        // List<OrderItem> lstOrderItem = new List<OrderItem>();
        //List<OrderItem> lstOrderItemLC = new List<OrderItem>();
        
        for(Integer i = 0 ; i < lstNewOrderItems.size(); i++){
            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41')){
            //     setOrdersToUpdate.add(lstNewOrderItems[i].OrderId);
            // }
            //Conditions for CT-1154
            // lstNewOrderItems[i].Remise_mode_de_transmission__c != null && lstNewOrderItems[i].Remise100__c != null && 
            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41')){
            //     if(lstNewOrderItems[i].Quantity != null && lstNewOrderItems[i].UnitPrice != null){
            //         lstOrderItem.add(lstNewOrderItems[i]);
            //     }
            // }
        }

        //DMU 20190619 - Commented AP41_OrderConditions due to homeserve project
        // if(lstOrderItem.size() > 0){
        //     AP41_OrderConditions.InsertOrderProduct(lstOrderItem);
        // }

        // if(!setOrdersToUpdate.isEmpty()){
        //     AP41_OrderConditions.updateFromOrderItems(setOrdersToUpdate);
        // }
        
    }



    public void handleBeforeDelete(list<OrderItem> lstOrderItem){
        //AP23
        //DMU 20200619 - Commented AP23_OrderItemManager due to homeServe Project
        // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP23')){
        //     AP23_OrderItemManager.checkOrderStatus(lstOrderItem);
        // }
    }

    public void handleAfterUpdate(list<OrderItem> lstOldOrderItem, list<OrderItem> lstNewOrderItem){

        Set<String> setConcatenatedFields = new Set<String>();
        Set<Id> setPrliId = new Set<Id>();
        // Set<Id> setOrderIds = new Set<Id>();
        List<ProductRequestLineItem> lstPrli = new List<ProductRequestLineItem>();
        List<OrderItem> lstOrdI = new List<OrderItem>();
      
        for(integer i=0;i<lstNewOrderItem.size();i++){
            if(lstNewOrderItem[i].Quantite_recue__c != lstOldOrderItem[i].Quantite_recue__c){
                lstOrdI.add(lstNewOrderItem[i]);
            }



            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41;')){
            //     if(lstNewOrderItem[i].Quantity != null && lstNewOrderItem[i].UnitPrice != null && ( lstNewOrderItem[i].Quantity !=  lstOldOrderItem[i].Quantity || lstNewOrderItem[i].UnitPrice != lstOldOrderItem[i].UnitPrice)){
            //     }
            //     setOrderIds.add(lstNewOrderItem[i].OrderId);
            // }
        }  
        //DMU 20200619 - commented AP38_ProdReqstLineManager due to homeserve project
        //AP38
        if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP38')){
             if(lstOrdI.size() > 0){
                 AP38_ProdReqstLineManager.changePrliStatus(lstOrdI);
             }                
        }

        //DMU 20200619 - commented AP41_OrderConditions due to homeserve project
        //AP41 : CT 1175
        // if(!setOrderIds.isEmpty()){
        //     AP41_OrderConditions.updateFromOrderItems(setOrderIds);
        // }

    }

    public void handleAfterDelete(List<OrderItem> lstOld){
        
        // set<Id> setOrderIds = new Set<Id>();

        for(Integer i = 0 ; i < lstOld.size(); i++){
            // if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP41;')){
            //     setOrderIds.add(lstOld[i].OrderId);
            // }
        }
        //DMU 20200619 Commented AP41_OrderConditions due to homeserve project
        //AP41 CT-1175
        // if(!setOrderIds.isEmpty()){
        //     AP41_OrderConditions.updateFromOrderItems(setOrderIds);
        // }
    }
}