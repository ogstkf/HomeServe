public with sharing class AP29_DeleteOpportunityLineItem {
     /**
 * @File Name          : AP29_DeleteOpportunityLineItem.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0        21-10-2019     		LGO         Initial Version
**/
    public static void deleteRelateOppLineItem(set<Id> setQuoId){
        system.debug('start deleteRelateOppLineItem '+setQuoId);


        //retrieve all opportunity line items that is associated with a quoteline item
        List<OpportunityLineItem> lstOli = [SELECT id, 
                                                TECH_CorrespondingQLItemId__c 
                                            FROM OpportunityLineItem 
                                            WHERE TECH_CorrespondingQLItemId__c  <> null
                                            AND TECH_CorrespondingQLItemId__c IN:setQuoId];
   


        //delete all opportunitylineitems
        if(lstOli.size() >0 ){
            system.debug('toDelete oppLineItem '+lstOli);
            delete lstOli;
        }
    }

}