@isTest
public with sharing class BAT01_SendPisteCSV_TEST {
/**
 * @author  SC (YGO)
 * @version 1.0
 * @since   30/01/2019
 *
 * Description : 
 * 
 *-- Maintenance History: 
 *--
 *-- Date         Name  Version  Remarks 
 *-- -----------  ----  -------  ------------------------
 *-- 30-01-2019   YGO    1.0     Initial version
 *-------------------------------------------------------
 * 
*/
	static User mainUser;
	static List<Lead> lstLeads;
    static List<EmailAddress__c> lstCSEmail;
    static List<User> lstUser;

	static{
		mainUser = TestFactory.createAdminUser('BAT01_SendPisteCSV_TEST@test.COM', TestFactory.getProfileAdminId());
                insert mainUser;

                System.runAs(mainUser){
                	
                	lstLeads = new List<Lead>{
                				new Lead(
                					Salutation = 'Miss', 
            						LastName = 'Jenna', 
            						Email = 'jenna@test.com', 
            						Phone = '574387434', 
            						RecordTypeId = TestFactory.getRecordTypeId('Lead', 'Pistes RTC'), 
            						LeadSource = AP_Constant.leadLeadSourceBlue, 
            						Payment__c = true, 
            						Visit_status__c = AP_Constant.leadVisitStatusDoneOk
                				)
                	};
                	insert lstLeads;

                    lstCSEmail = new List<EmailAddress__c>{
                                            new EmailAddress__c(
                                                    Name = 'Jane Doe'
                                            )
                    };
                    insert lstCSEmail;

                    lstUser = new List<User>{
                                    new User(
                                        FirstName = 'Jane',
                                        LastName ='Doe',
                                        Email = 'jane.doe@scf.dev',
                                        ProfileId = UserInfo.getProfileId(),
                                        UserName = 'jane.doe@scf.dev',          
                                        Alias = 'TST1',
                                        TimeZoneSidKey = 'America/New_York',
                                        EmailEncodingKey = 'ISO-8859-1',
                                        LocaleSidKey = 'en_US',
                                        LanguageLocaleKey = 'en_US'
                                    )
                    };
                    insert lstUser;

                }

	}

    static testMethod void testBatch() {  
                ID batchProcessId;

                Test.startTest();
                        batchProcessId = Database.executeBatch(new BAT01_SendPisteCSV());
                Test.stopTest();
    }

     //test for BAT01_SendPisteCSV Scheduler
    static testMethod void testSchedule() { 
        System.runAs(mainUser){

            list <CronTrigger> cronlist = new list <CronTrigger> ();
            cronlist = [Select Id From CronTrigger];
            if (cronlist.size() != 0){
            for(CronTrigger c : cronlist){ 
            System.abortJob(c.id);
            } 
            }
            String jobId = BAT01_SendPisteCSV.scheduleBatch();
            CronTrigger ct = [Select Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where Id = :jobId]; 

            System.assertEquals(ct.TimesTriggered,0); 
        }
    }
}