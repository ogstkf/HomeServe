/**
 * @File Name          : AP72_CodeCampagne.cls
 * @Description        : 
 * @Author             : DMU
 * @Group              : 
 * @Last Modified By   : DMU
 * @Last Modified On   : 21/07/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    21/07/2020         DMU                    Use in serviceContractTriggerHandler to set campagne
**/
public class AP72_CodeCampagne {
    public static void setCampagne(list<ServiceContract> lstSC){
        for(ServiceContract sc : lstSC){
            sc.Campagne__c = sc.TECH_AccountCampagne__c;
        }
    }
}