public with sharing class SingleRequestMock implements HttpCalloutMock {
/*----------------------------------------------------------------------
-- - Author        : Spoon Consulting
-- - Description   : Provide an implementation for the HttpCalloutMock interface 
--                   to fake a response sent in the respond method, which the Apex 
--                   runtime calls to send a response for a callout.
--                   Class implementing HttpCalloutMock for allowing single call.
-- -             
-- - Maintenance History:
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  ---------------------------------------
-- 24-JAN-2019  MGR   1.0      Initial version
----------------------------------------------------------------------
*/	
	protected Integer code;
    protected String status;
    protected String bodyAsString;
    protected Blob bodyAsBlob;
    protected Map<String, String> responseHeaders;
    
    public SingleRequestMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.bodyAsString = body;
        this.bodyAsBlob = null;
        this.responseHeaders = responseHeaders;
    }
    
    //public SingleRequestMock(Integer code, String status, Blob body, Map<String, String> responseHeaders) {
    //    this.code = code;
    //    this.status = status;
    //    this.bodyAsBlob = body;
    //    this.bodyAsString = null;
    //    this.responseHeaders = responseHeaders;
    //}
    
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse resp = new HttpResponse();
        resp.setStatusCode(code);
        resp.setStatus(status);
        if (bodyAsBlob != null) {
            resp.setBodyAsBlob(bodyAsBlob);
        } else {
            resp.setBody(bodyAsString);
        }
        if (responseHeaders != null) {
            for (String key : responseHeaders.keySet()) {
                resp.setHeader(key, responseHeaders.get(key));
            }
        }
        return resp;
    }
}