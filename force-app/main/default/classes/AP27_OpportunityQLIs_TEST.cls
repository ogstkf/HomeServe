/**
 * @File Name          : AP27_OpportunityQLIs_TEST.cls
 * @Description        : 
 * @Author             : RRJ
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    06/02/2020   RRJ     Initial Version
**/
@isTest
public  class AP27_OpportunityQLIs_TEST {

    static User adminUser;
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<ContractLineItem> lstTestContLnItem = new List<ContractLineItem>();
    static List<ServiceContract> lstServiceContracts = new List<ServiceContract>();
    static sofactoapp__Compte_auxiliaire__c aux;
    static PriceBook2 standardPricebook;
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<OpportunityLineItem> lstOppLine = new List<OpportunityLineItem>();
    //bypass
    static Bypass__c bp = new Bypass__c();
    
    static {
        adminUser = TestFactory.createAdminUser('C01_Acc360RendezVous_TEST@test.com', TestFactory.getProfileAdminId());
        insert adminUser;

        bp.SetupOwnerId  = adminUser.Id;
        bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(adminUser){
            TestFactory.initQuoteSyncSetting();

            //creating account
            testAcc = TestFactory.createAccount('AP10_GenerateCompteRenduPDF_Test');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            aux = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux.Id;
            
            update testAcc;

            //creating logement
            for(integer i =0; i<5; i++){
                Logement__c lgmt = TestFactory.createLogement('logementTest'+i, 'logement@test.com'+i, 'lgmt'+i);
                lgmt.Postal_Code__c = 'lgmtPC'+i;
                lgmt.City__c = 'lgmtCity'+i;
                lgmt.Account__c = testAcc.Id;
                lstTestLogement.add(lgmt);
            }
            insert lstTestLogement;

            //creating products
            lstTestProd.add(TestFactory.createProductAsset('Ramonage gaz'));
            lstTestProd.add(TestFactory.createProduct('Entretien gaz'));
            
            insert lstTestProd;

            // creating Assets
            for(Integer i = 0; i<5; i++){
                Asset eqp = TestFactory.createAccount('equipement'+i, AP_Constant.assetStatusActif, lstTestLogement[i].Id);
                eqp.Product2Id = lstTestProd[0].Id;
                eqp.Logement__c = lstTestLogement[i].Id;
                eqp.AccountId = testacc.Id;
                lstTestAssset.add(eqp);
            }
            insert lstTestAssset;

            //create pricebook 
            standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                Active_online__c = TRUE,
                isActive = true,
                Active_from__c = date.today().addMonths(-1),
                Active_to__c = date.today().addMonths(1),
                Ref_Agence__c = true,
                recordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Vente').getRecordTypeId()
            );
            update standardPricebook;

            //create pricebookentry
            List<PricebookEntry> lstPriceBooks = new List<PricebookEntry>();
            lstPriceBooks.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd[0].Id, 500 ));
            lstPriceBooks.add(TestFactory.createPriceBookEntry(standardPricebook.Id, lstTestProd[1].Id, 500 ));
            insert lstPriceBooks;
        
            //create service contract
            lstServiceContracts.add(TestFactory.createServiceContract('test', testAcc.Id));
            lstServiceContracts[0].PriceBook2Id = standardPricebook.Id;
            insert lstServiceContracts;

            //insert contractline
            ContractLineItem ctrt = TestFactory.createContractLineItem( (String)lstServiceContracts[0].Id, (String)lstPriceBooks[0].Id, 5, 10);
            ctrt.VE__C = true;
            // ctrt.Status = 'Active';
            ctrt.StartDate = Date.today().addDays(-1);
            ctrt.EndDate = Date.today().addDays(10);
            ctrt.AssetId = lstTestAssset[0].Id;
            lstTestContLnItem.add(ctrt);
            insert lstTestContLnItem;

            // create case
            case cse = new case(AccountId = testacc.id
                                ,type = 'Troubleshooting'
                                ,AssetId = lstTestAssset[0].Id);
            insert cse;


            WorkType wrkType1 = TestFactory.createWorkType('wrkType1','Hours',1);
            wrkType1.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType1.Type_de_client__c = 'Tous';
            WorkType wrkType2 = TestFactory.createWorkType('wrkType2','Hours',2);
            wrkType2.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType2.Type_de_client__c = 'Tous';
            WorkType wrkType3 = TestFactory.createWorkType('wrkType3','Hours',3);
            wrkType3.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            wrkType3.Type_de_client__c = 'Tous';

            lstWrkTyp = new list<WorkType>{wrkType1, wrkType2, wrkType3};
            insert lstWrkTyp;

            System.debug('lstWrkTyp ' + lstWrkTyp);

            for(Integer i=0; i<3; i++){
                WorkOrder wrkOrd = TestFactory.createWorkOrder();
                wrkOrd.caseId = cse.id;
                wrkOrd.AccountId = testAcc.Id;
                wrkOrd.WorkTypeId = lstWrkTyp[0].Id;
                lstWrkOrd.add(wrkOrd);
            }
            insert lstWrkOrd;

            ServiceAppointment serApp1 = TestFactory.createServiceAppointment(lstWrkOrd[0].Id);
            serApp1.DueDate = Date.today().addDays(10);
            serApp1.Status = '';
            ServiceAppointment serApp2 = TestFactory.createServiceAppointment(lstWrkOrd[1].Id);
            serApp2.DueDate = Date.today().addDays(10);
            serApp2.Status = 'In Progress';
            ServiceAppointment serApp3 = TestFactory.createServiceAppointment(lstWrkOrd[2].Id);
            serApp3.DueDate = Date.today().addDays(10);
            serApp3.Status = 'In Progress';


            lstServiceApp = new List<ServiceAppointment>{serApp1, serApp2, serApp3};
            insert lstServiceApp;

            lstOpp.add(TestFactory.createOpportunity('opp1', testAcc.Id));
            lstOpp[0].Ordre_d_execution__c = lstWrkOrd[0].Id;
            lstOpp[0].PriceBook2Id = standardPricebook.Id;
            lstOpp[0].Counter__c = '1';
            lstOpp[0].Signature_client__c = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QBaRXhpZgAATU0AKgAAAAgABQMBAAUAAAABAAAASgMDAAEAAAABAAAAAFEQAAEAAAABAQAAAFERAAQAAAABAAAOw1ESAAQAAAABAAAOwwAAAAAAAYagAACxj//bAEMAAgEBAgEBAgICAgICAgIDBQMDAwMDBgQEAwUHBgcHBwYHBwgJCwkICAoIBwcKDQoKCwwMDAwHCQ4PDQwOCwwMDP/bAEMBAgICAwMDBgMDBgwIBwgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIABkANQMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP38orD+J3gj/hZvw28Q+G/7X1zw/wD8JBplzpv9qaLdfZNS03zomj+0W02D5U8e7cj4O1lU4OK/Gv4Uf8E/PF3jT/gsl8T/ANny7/bB/baXwX4H8Cad4psbyP4rTDU5rm4liR0lcwmJogHOAsSt0yxohaVWNJ6c1/wjKT/CLCdo0nUfS1/nKMV+Mj9sKK/nr1D9qP4e+MP+CmX7TPhL9oL9tr9rD4MyaN8QDo3gbRfBHizVobGe2LvGymOG0uYotrCIDmIfMTg8ketf8FKfjf4j+GX/AAVJ1jwd+0H+0D+0x+zz8ELfQNNtvhZ4s8DXUtro+q33kR/aG1aeGKQ3MvnK+5GUYCksYo2DPFKftIUppfxFddvh5rX25tUuXe7G1yzqQlvC9+/xct7b23d+yuft1RX41/8ABdf4DeLvhB+y14Z+PPgn9rL9pCTV/FOqeF9AkTw149bTvCmoQTwRW8t/aWlquI2nEfn5WZkLzMcEEV+mf7FX7IP/AAxd8Mb/AMM/8LQ+MPxY+36k+pf2t8R/En9u6nbboo4/s8c3lx7YB5e4JjhpHOfmraMbqbb+GTj80ov8pJ6+hm5q8VHXmipfJ8y/OLWnqew0UUVBYV8m/Cz9hHxd4H/4LHfFH9oW71Hw5J4L8beA9O8L2NlDcTHVIrm3lid3ljMQiEZCHBWVm6ZUV9ZUULScai3je3zjKL/CTFJc0HTezt+ElJfjFH5c+Bf+Cf37cX7Hf7YX7Q3jf4Hap+ylqHhb44eLT4k8rx1Nr76haKvmeWm2ziSNDiVtw3SZwMEdK7z9u/8AZF/bb/aj0Hx58OdM8Y/sw6j8I/iXYfYrs+JNA1I614XSW2SKdLFYt0E+yUSSxSXHzhmHTauP0KorP2UfZRoy1ioqOv8AKkkk/kl5+ervfPLndSOjbctO7d2/vb8vwPz+/bG/4I6+IPiH/wAEmvhZ+zd8OPEmkTXfw21PQbj+1fEs81vHew2Dl5m/cxSsruSdibdoGFLcZr9AaKK3lOUnJy3lJzfq1FP8IoxhSjDlUfsxUV6Jya/9KYUUUVBof//Z';
            lstOpp.add(TestFactory.createOpportunity('opp2', testAcc.Id));
            lstOpp[1].PriceBook2Id = standardPricebook.Id;
            lstOpp[1].Ordre_d_execution__c = lstWrkOrd[1].Id;
            lstOpp[1].Counter__c = '1';
            lstOpp.add(TestFactory.createOpportunity('opp2', testAcc.Id));
            lstOpp[2].PriceBook2Id = standardPricebook.Id;
            lstOpp[2].Ordre_d_execution__c = lstWrkOrd[2].Id;
            lstOpp[2].Counter__c = '1';
            insert lstOpp;

            for(Opportunity opp : lstOpp){
                OpportunityLineItem oppLine0 = TestFactory.createOpportunityLine(opp.Id, lstPriceBooks[0].Id);
                lstOppLine.add(oppLine0);
                OpportunityLineItem oppLine1 = TestFactory.createOpportunityLine(opp.Id, lstPriceBooks[1].Id);
                lstOppLine.add(oppLine1);
            }
            insert lstOppLine;
        }
    }

    @isTest
    public static void testUpdate(){
        lstOpp[1].APP_intervention_immediate__c = true;
        // lstOpp[2].StageName = AP_Constant.oppStageDevisValideEnAttenteInter;
        // lstOpp[2].Tech_Devis_sign_par_le_client__c = true;
        // lstOpp[2].TECH_date_prevue_de_debut_des_travaux__c = Date.today();
        Test.startTest();
        lstOpp[0].Counter__c = '2';
        lstOpp[1].Counter__c = '2';
        lstOpp[2].Counter__c = '2';
        update lstOpp;
        Test.stopTest();
    }
}