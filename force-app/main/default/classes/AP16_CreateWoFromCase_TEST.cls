/**
 * @File Name          : AP16_CreateWoFromCase_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : MNA
 * @Last Modified On   : 07-06-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         14-10-2019     		ANA         Initial Version
**/
@isTest
public with sharing class AP16_CreateWoFromCase_TEST {


    static User mainUser;
    static list<Case> lstCase;
    static list<Account> lstAcc;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<Product2> lstTestProd;
    static List<Logement__c> lstTestLogement;
    static list<Asset> lstAsset;
    static list<WorkType> lstWt;
    static list<ServiceTerritory> lstServTerritory;
    static OperatingHours opHrs = new OperatingHours();
    static Agency_Fixed_Slots__c age;
    static sofactoapp__Raison_Sociale__c raisonSocial;
    static Bypass__c bp = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('AP14_SetCaseStatus@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = True;
        insert bp;
        
        System.runAs(mainUser){
			
            lstAcc = new list<Account>{
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='John ANA'),
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='Johnny ANA'),
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='Janardhun ANA')
            };

            insert lstAcc;
            
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAcc.get(0).Id);
            sofactoapp__Compte_auxiliaire__c aux1 = DataTST.createCompteAuxilaire(lstAcc.get(1).Id);
            sofactoapp__Compte_auxiliaire__c aux2 = DataTST.createCompteAuxilaire(lstAcc.get(2).Id);
			lstAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            lstAcc.get(1).sofactoapp__Compte_auxiliaire__c = aux1.Id;
            lstAcc.get(2).sofactoapp__Compte_auxiliaire__c = aux2.Id;
            
            update lstAcc;
            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', lstAcc[0].Id));
            lstServCon[0].Type__c = 'Individual';
            lstServCon.add(TestFactory.createServiceContract('test serv con 2', lstAcc[1].Id));
            lstServCon[1].Type__c = 'Individual';
            lstServCon.add(TestFactory.createServiceContract('test serv con 3', lstAcc[2].Id));
            lstServCon[2].Type__c = 'Individual';
			System.debug(lstServCon);
            insert lstServCon;
            
            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;

            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;
            
            lstServTerritory = new list<ServiceTerritory>{
                TestFactory.createServiceTerritory('SGS - test Territory', opHrs.Id, raisonSocial.Id),
                TestFactory.createServiceTerritory('SGS - test Territory 2', opHrs.Id, raisonSocial.Id),
                TestFactory.createServiceTerritory('SGS - test Territory 3', opHrs.Id, raisonSocial.Id)
            };
            insert lstServTerritory;

            lstTestLogement = new list<Logement__c>{
                new Logement__c(Account__c =lstAcc[0].Id),
                new Logement__c(Account__c =lstAcc[1].Id),
                new Logement__c(Account__c =lstAcc[2].Id)
            };

            lstTestLogement[0].City__c='Cityy1';
            lstTestLogement[0].Postal_Code__c='1234';
            lstTestLogement[0].Street__c='Royale Road';
            lstTestLogement[0].Agency__c=lstServTerritory[0].Id;

            lstTestLogement[1].City__c='Cityy2';
            lstTestLogement[1].Postal_Code__c='123456';
            lstTestLogement[1].Street__c='Royale Road 2';
            lstTestLogement[1].Agency__c=lstServTerritory[1].Id;

            lstTestLogement[2].City__c='Cityy';
            lstTestLogement[2].Postal_Code__c='123478';
            lstTestLogement[2].Street__c='Royale Road 3';
            lstTestLogement[2].Agency__c=lstServTerritory[2].Id;

            insert lstTestLogement;

            age = new Agency_Fixed_Slots__c(name='Agency 1',Start_Time_of_the_slot__c=DateTime.now(),
                                            EndTime_of_the_slot__c=DateTime.now().addDays(1) , Agence__c=lstServTerritory[1].Id);

            insert age;
            lstTestProd = new List<Product2>{
                new Product2(Name = 'Test' , Equipment_type1__c = 'Chaudière gaz', isActive = true)
            };
            insert lstTestProd;
            
            lstAsset = new list<Asset>{
                TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lstTestLogement[0].Id),
                TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstTestLogement[1].Id),
                TestFactory.createAccount('equipement 3', AP_Constant.assetStatusActif, lstTestLogement[2].Id)
            };

            lstAsset[0].AccountId=lstAcc[0].Id;
            lstAsset[2].AccountId=lstAcc[2].Id;
            lstAsset[1].AccountId=lstAcc[1].Id;
            
            lstAsset[0].Product2Id=lstTestProd[0].Id;
            lstAsset[1].Product2Id=lstTestProd[0].Id;
            lstAsset[2].Product2Id=lstTestProd[0].Id;

            insert lstAsset;

            lstCase = new List<Case>{
                new Case(RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Client case').getRecordTypeId(),
                    AccountId=lstAcc[0].Id ,Type=AP_Constant.caseTypeInstallation,VE_Planning__c=false
                    ,Description='generateWo',Subject='gWoSub'
                    ,AssetId=lstAsset[0].Id,Due_Date__c=Date.today().addDays(3),Start_Date__c=Date.today())
                
                , new Case(AccountId=lstAcc[1].Id ,Type=AP_Constant.caseTypeMaintenance,Due_Date__c=Date.today().addDays(3),
                    Optimized__c=true,VE_Planning__c=true, AssetId=lstAsset[1].Id,
                    Start_Date__c=Date.today(),Description='generateWoVePlanning',Subject='G2'
                    ,RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Client case').getRecordTypeId())
                
               /* , new Case(AccountId=lstAcc[2].Id, Type=AP_Constant.caseTypeInstallation,VE_Planning__c=false,Due_Date__c=Date.today().addDays(3),
                    Origin=AP_Constant.caseOriginChamDigital,TECH_Payment_ok__c=true,AssetId=lstAsset[2].Id,Start_Date__c=Date.today()
                    ,Agency_fixed_slot__c=age.Id,Description='generateWoRetrofit',Subject='G3'
                    ,RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Client case').getRecordTypeId())*/
            };
                System.debug([SELECT Id, Product2Id FROM Asset WHERE Id = : lstAsset[0].Id].Product2Id);
            lstWt = new List<WorkType>{
                new WorkType(Type__c = AP_Constant.caseTypeInstallation, Name='Test', Type_de_client__c = 'Tous', Equipment_type__c='Chaudière gaz',
                                        EstimatedDuration = 12, Agence__c ='Toutes'),
                new WorkType(Type__c = AP_Constant.caseTypeMaintenance, Name='Test', Type_de_client__c = 'Tous', Equipment_type__c='Chaudière gaz',
                                        EstimatedDuration = 12, Agence__c ='Toutes')
            };
			insert lstWt;
        }
    }

    @isTest
    public static void testInsert(){
        System.runAs(mainUser){
            Test.startTest();
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase[1].Service_Contract__c = lstServCon[1].Id;
           // lstCase[2].Service_Contract__c = lstServCon[2].Id;
            insert lstCase;
            Test.stopTest();

            list<Case> lstCreCase = [SELECT CreatedDate FROM Case WHERE Id IN : lstCase];
            System.debug('lstCreCase : '+lstCreCase);

            list<WorkOrder> lstWo1 = [SELECT AccountId,Acknowledged_On__c,CaseId,ContactId,Description,
                                    Reason__c,Status,Subject,Type__c,City,Country,PostalCode,ServiceTerritoryId,Street
                                    ,EndDate FROM WorkOrder WHERE CaseId =: lstCase[0].Id ];
 
            System.assertEquals(lstWo1[0].AccountId,lstCase[0].AccountId);
            System.assertEquals(lstWo1[0].Acknowledged_On__c,lstCreCase[0].CreatedDate);
            System.assertEquals(lstWo1[0].Description,lstCase[0].Description);
            System.assertEquals(lstWo1[0].Reason__c,lstCase[0].Reason__c);
            System.assertEquals(lstWo1[0].Status,'New');
            System.assertEquals(lstWo1[0].Subject,lstCase[0].Subject);
            System.assertEquals(lstWo1[0].Type__c,lstCase[0].Type);
            System.assertEquals(lstWo1[0].City,lstTestLogement[0].City__c);
            System.assertEquals(lstWo1[0].Country,'France');
            System.assertEquals(lstWo1[0].PostalCode,lstTestLogement[0].Postal_Code__c);
            System.assertEquals(lstWo1[0].ServiceTerritoryId,lstTestLogement[0].Agency__c);
            System.assertEquals(lstWo1[0].Street,lstTestLogement[0].Street__c);

            list<WorkOrder> lstWor2 = [SELECT AccountId,Acknowledged_On__c,CaseId,ContactId,Description,
                                    Reason__c,Status,Subject,Type__c,City,Country,PostalCode,ServiceTerritoryId,Street
                                    ,EndDate,VE_Planning__c,AssetId,StartDate FROM WorkOrder WHERE CaseId =: lstCase[1].Id ];

            System.assertEquals(lstWor2[0].AccountId,lstCase[1].AccountId);
            System.assertEquals(lstWor2[0].Acknowledged_On__c,lstCreCase[1].CreatedDate);
            System.assertEquals(lstWor2[0].AssetId,lstCase[1].AssetId);
            System.assertEquals(lstWor2[0].Description,lstCase[1].Description);
            System.assertEquals(lstWor2[0].Reason__c,lstCase[1].Reason__c);
            System.assertEquals(lstWor2[0].Status,'New');
            System.assertEquals(lstWor2[0].StartDate,Date.today());
            System.assertEquals(lstWor2[0].Subject,lstCase[1].Subject);
            System.assertEquals(lstWor2[0].Type__c,lstCase[1].Type);
            System.assertEquals(lstWor2[0].VE_Planning__c,true); 
            System.assertEquals(lstWor2[0].City,lstTestLogement[1].City__c);
            System.assertEquals(lstWor2[0].Country,'France');
            System.assertEquals(lstWor2[0].PostalCode,lstTestLogement[1].Postal_Code__c);
            System.assertEquals(lstWor2[0].ServiceTerritoryId,lstTestLogement[1].Agency__c);
            System.assertEquals(lstWor2[0].Street,lstTestLogement[1].Street__c);

           /* list<WorkOrder> lstWor3 = [SELECT AccountId,Acknowledged_On__c,CaseId,ContactId,Description,
                                    Reason__c,Status,Subject,Type__c,City,Country,PostalCode,ServiceTerritoryId,Street
                                    ,EndDate,StartDate,AssetId FROM WorkOrder WHERE CaseId =: lstCase[2].Id ];

            System.assertEquals(lstWor3[0].AccountId,lstCase[2].AccountId);
            System.assertEquals(lstWor3[0].Acknowledged_On__c,lstCreCase[2].CreatedDate);
            System.assertEquals(lstWor3[0].Description,lstCase[2].Description);
            System.assertEquals(lstWor3[0].AssetId,lstCase[2].AssetId);
            System.assertEquals(lstWor3[0].Reason__c,lstCase[2].Reason__c);
            System.assertEquals(lstWor3[0].Status,'New');
            System.assertEquals(lstWor3[0].Subject,lstCase[2].Subject);
            System.assertEquals(lstWor3[0].Type__c,lstCase[2].Type);
            System.assertEquals(lstWor3[0].City,lstTestLogement[2].City__c);
            System.assertEquals(lstWor3[0].Country,'France');
            System.assertEquals(lstWor3[0].PostalCode,lstTestLogement[2].Postal_Code__c);
            System.assertEquals(lstWor3[0].ServiceTerritoryId,lstTestLogement[2].Agency__c);
            System.assertEquals(lstWor3[0].Street,lstTestLogement[2].Street__c);
            System.assertEquals(lstWor3[0].StartDate,age.Start_Time_of_the_slot__c);
            System.assertEquals(lstWor3[0].EndDate,age.EndTime_of_the_slot__c);*/
            
        }
    }

    @isTest
    public static void testUpdate(){
        System.runAs(mainUser){
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase[1].Service_Contract__c = lstServCon[1].Id;
           // lstCase[2].Service_Contract__c = lstServCon[2].Id;
            insert lstCase;
            Test.startTest();
            update lstCase;
            Test.stopTest();

            list<Case> lstCreCase = [SELECT CreatedDate FROM Case WHERE Id IN : lstCase];
            System.debug('lstCreCase : '+lstCreCase);

            list<WorkOrder> lstWo1 = [SELECT AccountId,Acknowledged_On__c,CaseId,ContactId,Description,
                                    Reason__c,Status,Subject,Type__c,City,Country,PostalCode,ServiceTerritoryId,Street
                                    ,EndDate FROM WorkOrder WHERE CaseId =: lstCase[0].Id ];

            System.assertEquals(lstWo1[0].AccountId,lstCase[0].AccountId);
            System.assertEquals(lstWo1[0].Acknowledged_On__c,lstCreCase[0].CreatedDate);
            System.assertEquals(lstWo1[0].Description,lstCase[0].Description);
            System.assertEquals(lstWo1[0].Reason__c,lstCase[0].Reason__c);
            System.assertEquals(lstWo1[0].Status,'New');
            System.assertEquals(lstWo1[0].Subject,lstCase[0].Subject);
            System.assertEquals(lstWo1[0].Type__c,lstCase[0].Type);
            System.assertEquals(lstWo1[0].City,lstTestLogement[0].City__c);
            System.assertEquals(lstWo1[0].Country,'France');
            System.assertEquals(lstWo1[0].PostalCode,lstTestLogement[0].Postal_Code__c);
            System.assertEquals(lstWo1[0].ServiceTerritoryId,lstTestLogement[0].Agency__c);
            System.assertEquals(lstWo1[0].Street,lstTestLogement[0].Street__c);

            list<WorkOrder> lstWor2 = [SELECT AccountId,Acknowledged_On__c,CaseId,ContactId,Description,
                                    Reason__c,Status,Subject,Type__c,City,Country,PostalCode,ServiceTerritoryId,Street
                                    ,EndDate,VE_Planning__c,AssetId,StartDate FROM WorkOrder WHERE CaseId =: lstCase[1].Id ];

            System.assertEquals(lstWor2[0].AccountId,lstCase[1].AccountId);
            System.assertEquals(lstWor2[0].Acknowledged_On__c,lstCreCase[1].CreatedDate);
            System.assertEquals(lstWor2[0].AssetId,lstCase[1].AssetId);
            System.assertEquals(lstWor2[0].Description,lstCase[1].Description);
            System.assertEquals(lstWor2[0].Reason__c,lstCase[1].Reason__c);
            System.assertEquals(lstWor2[0].Status,'New');
            System.assertEquals(lstWor2[0].StartDate,Date.today());
            System.assertEquals(lstWor2[0].Subject,lstCase[1].Subject);
            System.assertEquals(lstWor2[0].Type__c,lstCase[1].Type);
            System.assertEquals(lstWor2[0].VE_Planning__c,true); 
            System.assertEquals(lstWor2[0].City,lstTestLogement[1].City__c);
            System.assertEquals(lstWor2[0].Country,'France');
            System.assertEquals(lstWor2[0].PostalCode,lstTestLogement[1].Postal_Code__c);
            System.assertEquals(lstWor2[0].ServiceTerritoryId,lstTestLogement[1].Agency__c);
            System.assertEquals(lstWor2[0].Street,lstTestLogement[1].Street__c);

           /* list<WorkOrder> lstWor3 = [SELECT AccountId,Acknowledged_On__c,CaseId,ContactId,Description,
                                    Reason__c,Status,Subject,Type__c,City,Country,PostalCode,ServiceTerritoryId,Street
                                    ,EndDate,StartDate,AssetId FROM WorkOrder WHERE CaseId =: lstCase[2].Id ];

            System.assertEquals(lstWor3[0].AccountId,lstCase[2].AccountId);
            System.assertEquals(lstWor3[0].Acknowledged_On__c,lstCreCase[2].CreatedDate);
            System.assertEquals(lstWor3[0].Description,lstCase[2].Description);
            System.assertEquals(lstWor3[0].AssetId,lstCase[2].AssetId);
            System.assertEquals(lstWor3[0].Reason__c,lstCase[2].Reason__c);
            System.assertEquals(lstWor3[0].Status,'New');
            System.assertEquals(lstWor3[0].Subject,lstCase[2].Subject);
            System.assertEquals(lstWor3[0].Type__c,lstCase[2].Type);
            System.assertEquals(lstWor3[0].City,lstTestLogement[2].City__c);
            System.assertEquals(lstWor3[0].Country,'France');
            System.assertEquals(lstWor3[0].PostalCode,lstTestLogement[2].Postal_Code__c);
            System.assertEquals(lstWor3[0].ServiceTerritoryId,lstTestLogement[2].Agency__c);
            System.assertEquals(lstWor3[0].Street,lstTestLogement[2].Street__c);
            System.assertEquals(lstWor3[0].StartDate,age.Start_Time_of_the_slot__c);
            System.assertEquals(lstWor3[0].EndDate,age.EndTime_of_the_slot__c);*/
            
        }
    }
}