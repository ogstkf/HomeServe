/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 09-11-2021
 * Modifications Log 
 * =====================================================================
 * Ver   Date           Author                  Modification
 * 1.0   27-04-2021     MNA                     Initial Version
**/
@isTest
public with sharing class BAT17_ServAppointmentAutomatisme_TEST {
    
    static User mainUser;
    static Bypass__c bp = new Bypass__c();
    static Account testAcc;
    static Contact testCon;
    static Account testBusinessAccount;
    static OperatingHours opHrs = new OperatingHours();
    static sofactoapp__Raison_Sociale__c raisonSocial;
    
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<ServiceTerritory> lstSrvTerr = new List<ServiceTerritory>();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    
    static{

        mainUser = TestFactory.createAdminUser('WS13@test.com', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            bp.SetupOwnerId  = mainUser.Id;
            bp.BypassValidationRules__c = true;
            bp.BypassWorkflows__c = true;
            bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53,MobileNotificationTrigger,AssignedResourceTrigger,AP10,AP79';
            insert bp;
              
            //creating account
            testAcc = TestFactory.createAccount('WS13_ChaineEditiquev2_TEST');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.PersonEmail = 'test@spoon.com';
            insert testAcc;

            testBusinessAccount = TestFactory.createAccountBusiness('WS13_ChaineEditiquev2_TEST2');
            testBusinessAccount.BillingStreet = 'Street 1';
            testBusinessAccount.BillingPostalCode = '1233456';
            testBusinessAccount.BillingCity = 'City 1';
            insert testBusinessAccount;

            //Create PriceBook
            Id pricebookId = Test.getStandardPricebookId();  
            

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            
            raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;
            
            //create service territory
            lstSrvTerr.add(TestFactory.createServiceTerritory('SBF Energies',opHrs.Id, raisonSocial.Id));
            lstSrvTerr.add(TestFactory.createServiceTerritory('VB Gaz',opHrs.Id, raisonSocial.Id));
            lstSrvTerr.add(TestFactory.createServiceTerritory('Electrogaz',opHrs.Id, raisonSocial.Id));

            lstSrvTerr[0].Agency_Code__c = '1234';
            lstSrvTerr[0].Corporate_Street__c = '12349';
            lstSrvTerr[0].Corporate_Street2__c = '12348';
            lstSrvTerr[0].Corporate_ZipCode__c = '12345';
            lstSrvTerr[0].Corporate_City__c = 'Paris';
            lstSrvTerr[0].Phone__c  = '123456789';
            lstSrvTerr[0].Email__c = 'contact@sbf-energies.com';
            lstSrvTerr[0].site_web__c = 'www.google.com';
            lstSrvTerr[0].IBAN__c = 'FR1023456213456';
            lstSrvTerr[0].Libelle_horaires__c = '1234';
            lstSrvTerr[0].horaires_astreinte__c = '1234';

            insert lstSrvTerr;

            raisonSocial.sofactoapp_Agence__c = lstSrvTerr[0].Id;
            raisonSocial.sofactoapp__Email__c = 'contact@sbf-energies.com';
            update raisonSocial;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[0].Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement[0].Inhabitant__c = testAcc.Id;
            lstLogement[0].Owner__c = testAcc.Id;
            lstLogement[0].Legal_Guardian__c = testAcc.Id;
            lstLogement[0].Visit_Notice_Recipient__c = 'Inhabitant';
            
            lstLogement.add(TestFactory.createLogement(testAcc.Id, lstSrvTerr[0].Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testBusinessAccount.Id;
            lstLogement[1].Inhabitant__c = testBusinessAccount.Id;

            insert lstLogement;


            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon[0].Type__c = 'Individual';
            lstServCon[0].Agency__c = lstSrvTerr[0].Id;
            lstServCon[0].PriceBook2Id = pricebookId;
            lstServCon[0].EndDate = Date.Today().addYears(1);
            lstServCon[0].StartDate = Date.Today();
            lstServCon[0].RecordTypeId = AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives') ;
            lstServCon[0].Type__c = 'Collective';
            lstServCon[0].Payeur_du_contrat__c = testAcc.Id;
            lstServCon[0].TransactionId__c = '123456789';

            insert lstServCon;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('Maintenance', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Maintenance';
            lstWrkTyp[0].Type_de_client__c = 'Tous';
            lstWrkTyp[0].Agence__c = 'Toutes';
            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());       
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].AccountId = testAcc.Id;

            insert lstWrkOrd;


            // creating service appointment
            lstServiceApp = new List<ServiceAppointment>{
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Service_Contract__c = lstServCon[0].Id,
                    Work_Order__c = lstWrkOrd[0].Id,
                    Residence__c =   lstLogement[0].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstSrvTerr[0].Id,
                    Avis_de_passage_en_cours_de_g_n_ration__c = true
                ),
                new ServiceAppointment(
                    ActualStartTime = System.today(),
                    EarliestStartTime = System.today(),
                    ActualEndTime = System.today().addDays(1),
                    DueDate = System.today().addDays(1),
                    ParentRecordId = lstWrkOrd[0].Id,
                    Service_Contract__c = lstServCon[0].Id,
                    Work_Order__c = lstWrkOrd[0].Id,
                    Residence__c =   lstLogement[1].Id,
                    TransactionId__c = '123456789',
                    SchedStartTime = System.now(),
                    SchedEndTime = System.now().addHours(1),
                    ServiceTerritoryId = lstSrvTerr[0].Id
                )
            };

            insert lstServiceApp;
        
        }

    }

    /*@isTest
    static void testScheduleExecuteEmail(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            System.schedule('Batch BAT17' + Datetime.now().format(),  '0 0 0 * * ?',
                                            new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }*/

    @isTest
    static void testBatchExecuteEmail1(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            testAcc.PersonEmail = null;
            update testAcc;
            System.debug(testAcc);
            Test.startTest();
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }

    @isTest
    static void testBatchExecuteEmail2(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatuserrorprinting());
            Test.startTest();
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }

    @isTest
    static void testBatchExecuteEmail3(){
        System.runAs(mainUser){
            lstLogement[0].Visit_Notice_Recipient__c = 'Owner';
            update lstLogement[0];
            Test.setMock(HttpCalloutMock.class, new calloutStatuserrorprinting());
            Test.startTest();
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }
    
    @isTest
    static void testBatchExecuteEmail4(){
        System.runAs(mainUser){
            lstLogement[0].Visit_Notice_Recipient__c = 'Legal Guardian';
            update lstLogement[0];
            Test.setMock(HttpCalloutMock.class, new calloutStatuserrorprinting());
            Test.startTest();
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }

    @isTest
    static void testBatchExecuteEmail5(){
        System.runAs(mainUser){
            lstLogement[0].Visit_Notice_Recipient__c = 'Administrator';
            update lstLogement[0];
            Test.setMock(HttpCalloutMock.class, new calloutStatuserrorprinting());
            Test.startTest();
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }

    @isTest
    static void testgetEmail1(){
        System.runAs(mainUser){
            Test.startTest();
            testBusinessAccount = [SELECT Id, RecordType.DeveloperName, (SELECT Id, Email FROM Contacts) FROM Account WHERE Id =:testBusinessAccount.Id];
            BAT17_ServAppointmentAutomatisme bat17 = new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id});
            bat17.getEmail(testBusinessAccount);
            Test.stopTest();
        }
    }

    @isTest
    static void testgetEmail2(){
        System.runAs(mainUser){
            Test.startTest();
            testCon = new Contact(AccountId = testBusinessAccount.Id, Email = 'test2@gmail.com', LastName = 'test2');
            insert testCon;
            testBusinessAccount = [SELECT Id, RecordType.DeveloperName, (SELECT Id, Email FROM Contacts) FROM Account WHERE Id =:testBusinessAccount.Id];
            BAT17_ServAppointmentAutomatisme bat17 = new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id});
            bat17.getEmail(testBusinessAccount);
            Test.stopTest();
        }
    }
    

    @isTest
    static void testBatchExecuteCourrier1(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }

    @isTest
    static void testBatchExecuteCourrier2(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatuserrorprinting());
            Test.startTest();
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }

    @isTest
    static void testBatchExecuteError(){
        System.runAs(mainUser){
            Test.setMock(HttpCalloutMock.class, new calloutStatusPrinted());
            Test.startTest();
            Database.executeBatch(new BAT17_ServAppointmentAutomatisme(new List<String>{lstServiceApp[0].Id}));
            Test.stopTest();
        }
    }

    public class calloutStatusPrinted implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"printed"}');
            response.setStatusCode(200);
            return response; 
        }
    }
    
    public class calloutStatuserrorprinting implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"code":0,"status":"error-printing"}');
            response.setStatusCode(200);
            return response; 
        }
    }
}