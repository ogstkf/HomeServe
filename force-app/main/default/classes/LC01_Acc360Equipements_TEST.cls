/**
 * @File Name          : LC01_Acc360Equipements_TEST.cls
 * @Description        : Test class for LC01_Acc360Equipements
 * @Author             : ZJO
 * @Group              : Spoon Consulting
 * @Last Modified By   : ZJO
 * @Last Modified On   : 07-15-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    27/06/2019, 10:07:38   RRJ     Initial Version
 * 1.1    02/08/2019, 14:28:15   ASO     Modifications
**/
@isTest
public class LC01_Acc360Equipements_TEST {
    static User adminUser;
    static Account testAcc;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstContLnItem = new List<ContractLineItem>();
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Asset> lstAsset = new List<Asset>();
    static List<Case> lstCase = new List<Case>();
    static List<PricebookEntry> lstPriceBkEntry = new List<PricebookEntry>();
    static List<Product2> lstProd = new List<Product2>();
    static List<WorkOrder> lstWrkOrd = new List<WorkOrder>();
    static List<WorkType> lstWrkTyp = new List<WorkType>();
    static List<ServiceAppointment> lstServiceApp = new List<ServiceAppointment>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static PricebookEntry prcBkEnt = new PricebookEntry();
    static List<ContractLineItem> lstCli = new List<ContractLineItem>();
    static Product2 prod1 = new Product2();
    static Product2 prod2 = new Product2();
    static Product2 prod3 = new Product2();

    static {
        adminUser = TestFactory.createAdminUser('VFC07_MassAvisDePassagePDF_TEST@test.COM', TestFactory.getProfileAdminId());
		insert adminUser;

        System.runAs(adminUser){


            //creating account
            testAcc = TestFactory.createAccount('test Acc');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update testAcc;

            //creating products
            Product2 prod1 = new Product2(Name = 'Ramonage gaz',
                                    ProductCode = 'Pro-X',
                                    isActive = true,
                                    Equipment_family__c = 'Chaudière',
                                    Equipment_type__c = 'Chaudière gaz',
                                    Statut__c = 'Approuvée'
            );
            Product2 prod2 = new Product2(Name = 'Entretien gaz',
                                    ProductCode = 'Pro-X',
                                    isActive = true,
                                    Equipment_family__c = 'Chaudière',
                                    Equipment_type__c = 'Chaudière gaz',
                                    Statut__c = 'Approuvée'
            );
            Product2 prod3 = new Product2(Name = 'Test',
                                    ProductCode = 'Pro-X',
                                    isActive = true,
                                    Equipment_family__c = 'Chaudière',
                                    Equipment_type__c = 'Chaudière gaz',
                                    Statut__c = 'Approuvée'
            );
            lstProd.add(prod1);
            lstProd.add(prod2);
            lstProd.add(prod3);
            // lstProd.add(TestFactory.createProduct('Ramonage gaz'));
            // lstProd.add(TestFactory.createProduct('Entretien gaz'));
            // lstProd.add(TestFactory.createProduct('Test'));
            lstProd[0].IsBundle__c	= true;
            lstProd[1].IsBundle__c = false;
            lstProd[2].IsBundle__c = true;

            insert lstProd;

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            //create Corporate Name
            sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(Name='Test1',
            sofactoapp__Credit_prefix__c='Test1',
            sofactoapp__Invoice_prefix__c='Test1');  
            insert srs; 

            ServiceTerritory srvTerr = new ServiceTerritory(IsActive = true, Name = 'test Territory' , OperatingHoursId = opHrs.Id, Sofactoapp_Raison_Social__c=srs.Id);
            insert srvTerr;

            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = 'lgmtPC 1';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc.Id;
            lstLogement.add(TestFactory.createLogement(testAcc.Id, srvTerr.Id));
            lstLogement[1].Postal_Code__c = 'lgmtPC 2';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc.Id;
            insert lstLogement;

            // creating Assets
            lstAsset.add(TestFactory.createAccount('equipement 1', AP_Constant.assetStatusActif, lstLogement[0].Id));
            lstAsset[0].Product2Id = lstProd[0].Id;
            lstAsset[0].Logement__c = lstLogement[0].Id;
            lstAsset[0].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstLogement[1].Id));
            lstAsset[1].Product2Id = lstProd[0].Id;
            lstAsset[1].Logement__c = lstLogement[1].Id;
            lstAsset[1].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 2', AP_Constant.assetStatusActif, lstLogement[1].Id));
            lstAsset[2].Product2Id = lstProd[2].Id;
            lstAsset[2].Logement__c = lstLogement[1].Id;
            lstAsset[2].AccountId = testAcc.Id;
            lstAsset.add(TestFactory.createAccount('equipement 3', AP_Constant.assetStatusActif, lstLogement[1].Id));
            lstAsset[3].Product2Id = lstProd[2].Id;
            lstAsset[3].Logement__c = lstLogement[1].Id;
            lstAsset[3].AccountId = testAcc.Id;
            insert lstAsset;
            
            //creating service contract
            lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
            lstServCon[0].Type__c = 'Individual';
            lstServCon[0].Contract_Status__c = 'Active';
            lstServCon[0].Pricebook2Id = Test.getStandardPricebookId();
            lstServCon[0].Asset__c = lstAsset[0].Id;
            lstServCon[0].Frequence_de_maintenance__c = 2;
            lstServCon[0].Type_de_frequence__c = 'Weeks';
            lstServCon[0].Debut_de_la_periode_de_maintenance__c = 3;
            lstServCon[0].Fin_de_periode_de_maintenance__c = 5;
            lstServCon[0].Periode_de_generation__c = 6;
            lstServCon[0].Type_de_periode_de_generation__c = 'Weeks';
            lstServCon[0].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            lstServCon[0].Generer_automatiquement_des_OE__c = true;
            lstServCon[0].Horizon_de_generation__c = 5;

            lstServCon.add(TestFactory.createServiceContract('test serv con 2', testAcc.Id));
            lstServCon[1].Contract_Status__c = 'Draft';
            lstServCon[1].Type__c = 'Individual';
            lstServCon[1].Pricebook2Id = Test.getStandardPricebookId();
            lstServCon[1].Asset__c = lstAsset[1].Id;
            lstServCon[1].Frequence_de_maintenance__c = 2;
            lstServCon[1].Type_de_frequence__c = 'Weeks';
            lstServCon[1].Debut_de_la_periode_de_maintenance__c = 3;
            lstServCon[1].Fin_de_periode_de_maintenance__c = 5;
            lstServCon[1].Periode_de_generation__c = 6;
            lstServCon[1].Type_de_periode_de_generation__c = 'Weeks';
            lstServCon[1].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            lstServCon[1].Generer_automatiquement_des_OE__c = true;
            lstServCon[1].Horizon_de_generation__c = 5;

            lstServCon.add(TestFactory.createServiceContract('test serv con 3', testAcc.Id));
            lstServCon[2].Type__c = 'Individual';
            lstServCon[2].Pricebook2Id = Test.getStandardPricebookId();
            lstServCon[2].Contract_Status__c = 'Pending Client Validation';
            lstServCon[2].Asset__c = lstAsset[2].Id;
            lstServCon[2].Frequence_de_maintenance__c = 2;
            lstServCon[2].Type_de_frequence__c = 'Weeks';
            lstServCon[2].Debut_de_la_periode_de_maintenance__c = 3;
            lstServCon[2].Fin_de_periode_de_maintenance__c = 5;
            lstServCon[2].Periode_de_generation__c = 6;
            lstServCon[2].Type_de_periode_de_generation__c = 'Weeks';
            lstServCon[2].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            lstServCon[2].Generer_automatiquement_des_OE__c = true;
            lstServCon[2].Horizon_de_generation__c = 5;

            lstServCon.add(TestFactory.createServiceContract('test serv con 4', testAcc.Id));
            lstServCon[3].Type__c = 'Individual';
            lstServCon[3].Pricebook2Id = Test.getStandardPricebookId();
            lstServCon[3].Contract_Status__c = 'Expired';
            lstServCon[3].Asset__c = lstAsset[3].Id;
            lstServCon[3].Type_de_frequence__c = 'Weeks';
            lstServCon[3].Debut_de_la_periode_de_maintenance__c = 3;
            lstServCon[3].Fin_de_periode_de_maintenance__c = 5;
            lstServCon[3].Periode_de_generation__c = 6;
            lstServCon[3].Type_de_periode_de_generation__c = 'Weeks';
            lstServCon[3].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
            lstServCon[3].Generer_automatiquement_des_OE__c = true;
            lstServCon[3].Horizon_de_generation__c = 5;

            lstServCon[0].StartDate =  Date.newInstance(2019, 12, 20);
            lstServCon[1].StartDate =  Date.newInstance(2018, 12, 20);
            lstServCon[2].StartDate =  Date.newInstance(2018, 1, 12);
            lstServCon[3].StartDate =  Date.newInstance(2018, 1, 12);


            insert lstServCon;


            //create pricebookentry
            List<PricebookEntry> lstPriceBkEntry = new List<PricebookEntry>();
            lstPriceBkEntry.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstProd[0].Id, 500 ));
            lstPriceBkEntry.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstProd[1].Id, 200 ));
            lstPriceBkEntry.add(TestFactory.createPriceBookEntry(Test.getStandardPricebookId(), lstProd[2].Id, 300 ));
            insert lstPriceBkEntry;

            //insert contractline
            lstContLnItem.add(TestFactory.createContractLineItem(lstServCon[0].Id, lstPriceBkEntry[0].Id, 5, 10));
            lstContLnItem[0].VE__C = true;
            lstContLnItem[0].StartDate = Date.today().addDays(-1);
            lstContLnItem[0].EndDate = Date.today().addDays(10);
            lstContLnItem[0].AssetId = lstAsset[0].Id;
            lstContLnItem[0].UnitPrice = 500;

            lstContLnItem.add(TestFactory.createContractLineItem(lstServCon[1].Id, lstPriceBkEntry[1].Id, 5, 10));
            lstContLnItem[1].VE__C = true;
            // lstContLnItem[1].TotalPrice = 100;
            lstContLnItem[1].StartDate = Date.today().addDays(-1);
            lstContLnItem[1].EndDate = Date.today().addDays(10);
            lstContLnItem[1].AssetId = lstAsset[1].Id;

            lstContLnItem.add(TestFactory.createContractLineItem(lstServCon[2].Id, lstPriceBkEntry[2].Id, 5, 10));
            lstContLnItem[2].VE__C = true;
            lstContLnItem[2].UnitPrice = 100;
            lstContLnItem[2].Quantity = 2;
            lstContLnItem[2].StartDate = Date.today().addDays(-1);
            lstContLnItem[2].EndDate = Date.today().addDays(10);
            lstContLnItem[2].AssetId = lstAsset[2].Id;

            lstContLnItem.add(TestFactory.createContractLineItem(lstServCon[3].Id, lstPriceBkEntry[2].Id, 5, 10));
            lstContLnItem[3].VE__C = true;
            lstContLnItem[3].UnitPrice = 100;
            lstContLnItem[3].Quantity = 2;
            lstContLnItem[3].StartDate = Date.today().addDays(-1);
            lstContLnItem[3].EndDate = Date.today().addDays(10);
            lstContLnItem[3].AssetId = lstAsset[3].Id;
            insert lstContLnItem;

            // create case
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[0].Id));
            lstCase[0].Service_Contract__c = lstServCon[0].Id;
            lstCase.add(TestFactory.createCase(testAcc.Id, 'A1 - Logement', lstAsset[1].Id));
            lstCase[1].Service_Contract__c = lstServCon[1].Id;
            insert lstCase;

            // creating work type
            lstWrkTyp.add(TestFactory.createWorkType('wrkType1', 'Hours', 1));
            lstWrkTyp[0].Type__c = 'Commissioning';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType2', 'Hours', 1));
            lstWrkTyp[1].Type__c = 'Information Request';
            lstWrkTyp[1].Reason__c = 'Commercial Visit';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType3', 'Hours', 1));
            lstWrkTyp[2].Type__c = 'Information Request';
            lstWrkTyp[2].Reason__c = 'Commercial Visit';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType4', 'Hours', 1));
            lstWrkTyp[3].Type__c = 'Installation';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType5', 'Hours', 1));
            lstWrkTyp[4].Type__c = 'Maintenance';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType6', 'Hours', 1));
            lstWrkTyp[5].Type__c = 'Maintenance';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType7', 'Hours', 1));
            lstWrkTyp[6].Type__c = 'Troubleshooting';
            lstWrkTyp.add(TestFactory.createWorkType('wrkType8', 'Hours', 1));
            lstWrkTyp[7].Type__c = 'Various Services';

            insert lstWrkTyp;

            // creating work order
            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[0].WorkTypeId = lstWrkTyp[0].Id;
            lstWrkOrd[0].ServiceTerritoryId = srvTerr.Id;
            lstWrkOrd[0].StartDate =  Date.newInstance(2019, 11, 20);
            lstWrkOrd[0].EndDate  = Date.newInstance(2020, 1, 20);

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[1].WorkTypeId = lstWrkTyp[1].Id;
            lstWrkOrd[1].ServiceTerritoryId = srvTerr.Id;
            lstWrkOrd[1].StartDate =  Date.newInstance(2019, 11, 20);
            lstWrkOrd[1].EndDate  = Date.newInstance(2020, 1, 20);

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[2].WorkTypeId = lstWrkTyp[2].Id;
            lstWrkOrd[2].ServiceTerritoryId = srvTerr.Id;
            lstWrkOrd[2].StartDate =  Date.newInstance(2019, 11, 20);
            lstWrkOrd[2].EndDate  = Date.newInstance(2020, 1, 20);

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[3].WorkTypeId = lstWrkTyp[3].Id;
            lstWrkOrd[3].ServiceTerritoryId = srvTerr.Id;
            lstWrkOrd[3].StartDate =  Date.newInstance(2019, 11, 20);
            lstWrkOrd[3].EndDate  = Date.newInstance(2020, 1, 20);

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[4].WorkTypeId = lstWrkTyp[4].Id;
            lstWrkOrd[4].ServiceTerritoryId = srvTerr.Id;
            lstWrkOrd[4].CaseId = lstCase[0].Id;
            lstWrkOrd[4].StartDate =  Date.newInstance(2019, 11, 20);
            lstWrkOrd[4].EndDate  = Date.newInstance(2020, 1, 20);

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[5].WorkTypeId = lstWrkTyp[5].Id;
            lstWrkOrd[5].ServiceTerritoryId = srvTerr.Id;
            lstWrkOrd[5].CaseId = lstCase[1].Id;
            lstWrkOrd[5].StartDate =  Date.newInstance(2019, 11, 20);
            lstWrkOrd[5].EndDate  = Date.newInstance(2020, 1, 20);

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[6].WorkTypeId = lstWrkTyp[6].Id;
            lstWrkOrd[6].ServiceTerritoryId = srvTerr.Id;
            lstWrkOrd[6].StartDate =  Date.newInstance(2019, 11, 20);
            lstWrkOrd[6].EndDate  = Date.newInstance(2020, 1, 20);

            lstWrkOrd.add(TestFactory.createWorkOrder());
            lstWrkOrd[7].WorkTypeId = lstWrkTyp[7].Id;
            lstWrkOrd[7].ServiceTerritoryId = srvTerr.Id;
            lstWrkOrd[7].StartDate =  Date.newInstance(2019, 11, 20);
            lstWrkOrd[7].EndDate  = Date.newInstance(2020, 1, 20);

            insert lstWrkOrd;

            // creating service appointment
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[0].Id));
            lstServiceApp[0].ServiceTerritoryId = srvTerr.Id;
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[1].Id));
            lstServiceApp[1].ServiceTerritoryId = srvTerr.Id;
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[2].Id));
            lstServiceApp[2].ServiceTerritoryId = srvTerr.Id;
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[3].Id));
            lstServiceApp[3].ServiceTerritoryId = srvTerr.Id;
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[4].Id));
            lstServiceApp[4].ServiceTerritoryId = srvTerr.Id;
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[5].Id));
            lstServiceApp[5].ServiceTerritoryId = srvTerr.Id;
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[6].Id));
            lstServiceApp[6].ServiceTerritoryId = srvTerr.Id;
            lstServiceApp.add(TestFactory.createServiceAppointment(lstWrkOrd[7].Id));
            lstServiceApp[7].ServiceTerritoryId = srvTerr.Id;
            
            insert lstServiceApp;

            

        }
    }
    
    @isTest
    public static void testFetch(){
        Test.startTest();
        LC01_Acc360Equipements.fetchEqp(testAcc.Id);
        Test.stopTest();
    }

    @isTest
    public static void getPicklistLabel_Test(){
        Test.startTest();
            String str = LC01_Acc360Equipements.getPicklistLabel('Account','Status__c','Client');
        Test.stopTest();

        System.assertEquals(str, 'Client');
    }

    @isTest
    public static void initialiseWrapper_Test(){
        Test.startTest();
            LC01_Acc360Equipements.EqpData res = new LC01_Acc360Equipements.EqpData(lstAsset[0],lstServCon[0]);
            LC01_Acc360Equipements.EqpData res2 = new LC01_Acc360Equipements.EqpData(lstAsset[0],lstServCon[1]);
            LC01_Acc360Equipements.EqpData res3 = new LC01_Acc360Equipements.EqpData(lstAsset[0],lstServCon[2]);
        Test.stopTest();
    }

    @isTest
    public static void initialiseWrapper_Test1(){
        Test.startTest();
            LC01_Acc360Equipements.EqpData res1 = new LC01_Acc360Equipements.EqpData(lstAsset[3]);
        Test.stopTest();
    }
}