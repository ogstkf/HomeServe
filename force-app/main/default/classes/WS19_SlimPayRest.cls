/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 08-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-03-2022   MNA   Initial Version
**/
@RestResource(urlMapping='/account/*/contracts/*')
global class WS19_SlimPayRest {

    @HttpPost
    global static void dataReceivingAPI() {
        DateTime now = System.now();
        Object returnValue;
        String customer_id;
        String contract_id;
        Account acc;
        ServiceContract sc;
        ServiceTerritory agency;
        slimPayObjectWrp.dataReceiving dr;
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        Map<String, Object> mapResponse = new Map<String, Object>();

        customer_id = request.requestURI.substringBetween('account/', '/contracts');
        contract_id = request.requestURI.substringBetween('contracts/', '/mandates');

        
        try {
            dr = (slimPayObjectWrp.dataReceiving) JSON.deserialize(request.requestBody.toString(), slimPayObjectWrp.dataReceiving.class);
        } catch(Exception ex) {
            insert WebServiceLog.generateLog(now, request.requestURI, '404', request.requestBody.toString(), 'Veuillez vérifier les informations envoyées');
            response.statusCode = 404;
            response.responseBody = slimPayObjectWrp.getErrorApex(404, 'Veuillez vérifier les informations envoyées');
            return;
        }
        

        try {
            acc = [SELECT Id, ClientNumber__c, Agence__c, FirstName, LastName, Name, Salutation, BillingStreet, Adress_complement__c, BillingPostalCode , BillingCity, BillingCountry,
                        PersonMobilePhone, PersonEmail
                   		    FROM Account WHERE ClientNumber__c =: customer_id LIMIT 1];
        } catch(Exception ex) {
            insert WebServiceLog.generateLog(now, request.requestURI, '404', request.requestBody.toString(), 'Numéro du compte inexistant');

            response.statusCode = 404;
            response.responseBody = slimPayObjectWrp.getErrorApex(404, 'Numéro du compte inexistant');
            return;
        }

        try {
            sc = [SELECT Id, Numero_du_contrat__c, Mandate_signed__c, sofactoapp_IBAN__c, Payeur_du_contrat__c, sofactoapp_Rib_prelevement__c, Id_Mandate_Order__c FROM ServiceContract
                                    WHERE Numero_du_contrat__c =: contract_id LIMIT 1];
        } catch(Exception ex) {
            insert WebServiceLog.generateLog(now, request.requestURI, '404', request.requestBody.toString(), 'Numéro du contrat inexistant');

            response.statusCode = 404;
            response.responseBody = slimPayObjectWrp.getErrorApex(404, 'Numéro du contrat inexistant');
            return;
        }

        if (acc.Id != sc.Payeur_du_contrat__c) {
            insert WebServiceLog.generateLog(now, request.requestURI, '404', request.requestBody.toString(), 'Le client n\'est pas le payeur de ce contrat');

            response.statusCode = 404;
            response.responseBody = slimPayObjectWrp.getErrorApex(404, 'Le client n\'est pas le payeur de ce contrat');
            return;
        }
        
        agency = [SELECT Id, Slimpay_Creditor_Name__c FROM ServiceTerritory WHERE Id =: acc.Agence__c];

        System.debug(agency);
        if (!ValidateIban(dr.iban)) {
            insert WebServiceLog.generateLog(now, request.requestURI, '404', request.requestBody.toString(), 'Merci de vérifier les données bancaires saisies');

            response.statusCode = 404;
            response.responseBody = slimPayObjectWrp.getErrorApex(404, 'Merci de vérifier les données bancaires saisies');
            return;
        }
        
        if (sc.Mandate_signed__c) {
            if (sc.sofactoapp_IBAN__c != dr.iban) {
                insert WebServiceLog.generateLog(now, request.requestURI, '403', request.requestBody.toString(), 'Le mandat est déjà signé');

                response.statusCode = 403;
                response.responseBody = slimPayObjectWrp.getErrorApex(403, 'Le mandat est déjà signé');
                return;
            }
            else {
                insert WebServiceLog.generateLog(now, request.requestURI, '409', request.requestBody.toString(),
                                        'L’IBAN envoyé correspond à celui renseigné pour ce contrat et le mandat est déjà signé.');

                response.statusCode = 409;
                response.responseBody = slimPayObjectWrp.getErrorApex(409, 'L’IBAN envoyé correspond à celui renseigné pour ce contrat et le mandat est déjà signé.');
                return;
            }
        }
        else {
            ServiceContractTriggerHandler.runByWebMobile = true;
            mapResponse = WS16_GestionDesPrelevements.createOrder(acc, agency, sc, dr.iban, Util.generateUniqueString());
            response.statusCode = (Integer) mapResponse.get('statusCode');
            returnValue = mapResponse.get('returnValue');
            upsCB(dr, acc, sc);
            insert WebServiceLog.generateLog(now, request.requestURI, String.Valueof(mapResponse.get('statusCode')), request.requestBody.toString(),
                                                 JSON.serialize(mapResponse.get('returnValue')));
        }
        if (response.statusCode >= 200 && response.statusCode < 300)
            response.responseBody = Blob.valueOf(JSON.serialize(returnValue));
        else 
            response.responseBody = Blob.valueOf(JSON.serialize(returnValue));
    }
    
    @HttpGet
    global static void getMethod() {
        DateTime now = System.now();
        slimPayObjectWrp.dataAccScWrp returnValue;
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        String customer_id = request.requestURI.substringBetween('account/', '/contracts');
        String contract_id = request.requestURI.substringBetween('contracts/', '/');
        Account acc;
        ServiceContract sc;

        if  (contract_id == null)
            contract_id = request.requestURI.substringAfter('contracts/');


        response.addHeader('Content-Type', 'application/json');
        System.debug(contract_id);
        try {
            acc = [SELECT Id, FirstName, LastName, BillingStreet, Adress_complement__c, BillingPostalCode , BillingCity, BillingCountry, PersonMobilePhone, PersonEmail
                   		FROM Account WHERE ClientNumber__c =: customer_id LIMIT 1];
        } catch(Exception ex) {
            insert WebServiceLog.generateLog(now, request.requestURI, '404', request.requestBody.toString(), 'L’ID client n’existe pas');

            response.statusCode = 404;
            response.responseBody = slimPayObjectWrp.getErrorApex(404, 'L’ID contrat n’existe pas');
            return;
        }

        try {
            sc = [SELECT Id, Name, sofactoapp_IBAN__c, Payeur_du_contrat__c, GrandTotal FROM ServiceContract
                                    WHERE Numero_du_contrat__c =: contract_id LIMIT 1];
        } catch(Exception ex) {
            insert WebServiceLog.generateLog(now, request.requestURI, '404', request.requestBody.toString(), 'L’ID contrat n’existe pas');
            
            response.statusCode = 404;
            response.responseBody = slimPayObjectWrp.getErrorApex(404, 'L’ID contrat n’existe pas');
            return;
        }
        
        if (acc.Id != sc.Payeur_du_contrat__c) {
            insert WebServiceLog.generateLog(now, request.requestURI, '404', request.requestBody.toString(), 'L’ID contrat ne correspond pas à un contrat détenu par l’ID client');

            response.statusCode = 404;
            response.responseBody = slimPayObjectWrp.getErrorApex(404, 'L’ID contrat ne correspond pas à un contrat détenu par l’ID client');
            return;
        }

        returnValue = new slimPayObjectWrp.dataAccScWrp(acc, sc);

        insert WebServiceLog.generateLog(now, request.requestURI, '200', request.requestBody.toString(), JSON.serialize(returnValue));

        response.statusCode = 200;
        response.responseBody = Blob.valueOf(JSON.serialize(returnValue));
    }
    
    public static void upsCB (slimPayObjectWrp.dataReceiving dr, Account acc, ServiceContract sc) {
        sofactoapp__Coordonnees_bancaires__c CB;
        List<sofactoapp__Coordonnees_bancaires__c> lstCB = [SELECT Id, sofactoapp__Compte__c FROM sofactoapp__Coordonnees_bancaires__c WHERE sofactoapp__IBAN__c = :dr.iban AND sofactoapp__BIC__c =: dr.bic];
        if (lstCB.size() > 0) 
            CB = lstCB[0];
        
        if (CB == null) {
            System.debug('here');
            CB = new sofactoapp__Coordonnees_bancaires__c(Name = dr.nom_debiteur, sofactoapp__BIC__c = dr.bic, sofactoapp__IBAN__c = dr.iban, sofactoapp__Compte__c = acc.Id);
            insert CB;
        }
        else {
            if (CB.sofactoapp__Compte__c != acc.Id) {
                CB.sofactoapp__Compte__c = acc.Id;
                update CB;
            }
        }
        sc.sofactoapp_Rib_prelevement__c = CB.Id;
        sc.Payment_Frequency__c = dr.frequency;
        update sc;
    }

    public static Boolean ValidateIban (String X) {
        if (X == null)
            return false;
        X =X.replaceAll('[^a-zA-Z0-9\\s]', '36');
        X =X.replaceAll('[\\s]', '');
        String X1 = X.left(4);
        String X2 = X.right((X.length()) - 4);
        X=X2+X1;
      
        X = X.replaceAll('[Aa]','10');
        X = X.replaceAll('[Bb]','11');
        X = X.replaceAll('[Cc]','12');
        X = X.replaceAll('[Dd]','13');
        X = X.replaceAll('[Ee]','14');
        X = X.replaceAll('[Ff]','15');
        X = X.replaceAll('[Gg]','16');
        X = X.replaceAll('[Hh]','17');
        X = X.replaceAll('[Ii]','18');
        X = X.replaceAll('[Jj]','19');
        X = X.replaceAll('[Kk]','20');
        X = X.replaceAll('[Ll]','21');
        X = X.replaceAll('[Mm]','22');
        X = X.replaceAll('[Nn]','23');
        X = X.replaceAll('[Oo]','24');
        X = X.replaceAll('[Pp]','25');
        X = X.replaceAll('[Qq]','26');
        X = X.replaceAll('[Rr]','27');
        X = X.replaceAll('[Ss]','28');
        X = X.replaceAll('[Tt]','29');
        X = X.replaceAll('[Uu]','30');
        X = X.replaceAll('[Vv]','31');
        X = X.replaceAll('[Ww]','32');
        X = X.replaceAll('[Xx]','33');
        X = X.replaceAll('[Yy]','34');
        X = X.replaceAll('[Zz]','35');
        // decomposition du Numero IBan pour calcul du modulo (97)
        While ((X.length())>12) {
            System.debug(' X au total ' +  X);
            X1 = X.left(9);
        
            Integer A = integer.valueOf(X1);

            Long Result =math.mod(A,97);
        
            X1 = X.right((X.length()) - 9);
        
            X = Result + X1;
            }
       
        Long C = long.valueOf(X) ;
        return  math.mod(C,97) == 1;
    }
}