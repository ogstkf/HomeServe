/**
 * @File Name          : AP39_UpdtQUoteStatus.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 09-04-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    05/11/2019   AMO     Initial Version
**/
public with sharing class AP39_UpdtQUoteStatus {
    public static void updateQuote(List<Quote> lstQuoteUpdt){

        //map opportunity id and quote id
        Map<String, Quote> mapOppQu = new Map<String, Quote>();
        List<Opportunity> lstOppUpdt = new List<Opportunity>();

        for(Quote qu : lstQuoteUpdt){
            mapOppQu.put(qu.OpportunityId, qu);
        }

        for(Opportunity opp : [SELECT Id, StageName FROM Opportunity WHERE Id IN: mapOppQu.keyset()]){

            // Expired
            if(mapOppQu.get(opp.Id).Status == AP_Constant.quoteStatusExpired){
                opp.StageName = AP_Constant.oppStageExpired;
                lstOppUpdt.add(opp);
            } 
            
            //Denied
            else if(mapOppQu.get(opp.Id).Status == AP_Constant.quoteStatusDenied){
                opp.StageName = AP_Constant.oppStageClosedLost;
                lstOppUpdt.add(opp);
            }

            //Abandonner
            else if(mapOppQu.get(opp.Id).Status == AP_Constant.quoteStatusValideSigneAbandonne){
                opp.StageName = AP_Constant.oppStageClosedLost;
                lstOppUpdt.add(opp);
            }

            //Conforme
            else  if(mapOppQu.get(opp.Id).Status == 'Validé, signé et terminé'){
                opp.StageName = AP_Constant.oppStageClosedWon;
                lstOppUpdt.add(opp);
            }

            //Intervention(check comma)
            else if(mapOppQu.get(opp.Id).Status == AP_Constant.quoteStatusValideSigne){
                opp.StageName = AP_Constant.oppStageDevisValideEnAttenteInter;
                if(mapOppQu.get(opp.Id).IsSync__c){
                    opp.Tech_Devis_sign_par_le_client__c = true;
                    opp.Tech_Synced_Devis__c = mapOppQu.get(opp.Id).Id;
                }
                lstOppUpdt.add(opp);
            }

            //Retour cham 
            else if(mapOppQu.get(opp.Id).Status == AP_Constant.quoteStatusNeedsReview){
                opp.StageName = AP_Constant.oppStageNegotiation;
                lstOppUpdt.add(opp);
            }

            //Proposal 
            else if(mapOppQu.get(opp.Id).Status == AP_Constant.quoteStatusInReview){
                opp.StageName = AP_Constant.oppStageProposal;
                lstOppUpdt.add(opp);
            }

            //Qualification(DONE)
            else if(mapOppQu.get(opp.Id).Status == AP_Constant.quoteStatusDraft){
                opp.StageName = AP_Constant.oppStageQua;
                lstOppUpdt.add(opp);
            }

            //Open
            else if(mapOppQu.get(opp.Id).Status == null){
                opp.StageName = AP_Constant.oppStageOpen;
                lstOppUpdt.add(opp);
            }

            //New Value Autre option retenue AMO 04.09.20
            else if(mapOppQu.get(opp.Id).Status == AP_Constant.quoteStatusAutreOptionRetenue){
                opp.StageName = AP_Constant.oppStageAutreOptionRetenue;
                lstOppUpdt.add(opp);
            }

            
        }
        Update lstOppUpdt;
                   
    }
}