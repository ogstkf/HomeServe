/**
 * @File Name         : BAT19_OldCodeCampagne_TEST
 * @Description       : 
 * @Author            : Spoon Consutling (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 27-09-2021
 * Modifications Log 
 * ==========================================================
 * Ver   Date               Author          Modification
 * 1.0   22-09-2021         MNA             Initial Version
**/
@isTest
public with sharing class BAT19_OldCodeCampagne_TEST {
    static User mainUser;
    static Bypass__c bp = new Bypass__c();

    static Account testAcc;

    static List<Campaign> testLstCpg;

    static {
        mainUser = TestFactory.createAdminUser('BAT19@test.com', TestFactory.getProfileAdminId());
        insert mainUser;
        
        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = true;
        bp.BypassWorkflows__c = true;
        insert bp;

        System.runAs(mainUser) {
            testLstCpg = new List<Campaign> {
                new Campaign(Name = 'Campagne 1', IsActive = true, Code__c = 'TEST.BAT19'),
                new Campaign(Name = 'Campagne 2', IsActive = true, Code__c = 'OLD.TEST.BAT19', EndDate = System.today().addYears(1))
            };
            insert testLstCpg;

            testAcc = TestFactory.createAccount('testAcc');
            testAcc.Campagne__c = testLstCpg[0].Id;
            testAcc.Date_fin_retombees__c = System.today().addDays(-1);
            insert testAcc;

        }
    }

    @isTest
    static void testScheduleExecute1() {
        System.runAs(mainUser) {
            Test.startTest();
            System.schedule('Batch BAT19' + Datetime.now().format(),  '0 0 0 * * ?',  new BAT19_OldCodeCampagne());
            Test.stopTest();
        }
    }

    @isTest
    static void testScheduleExecute2() {
        System.runAs(mainUser) {
            insert new batchConfiguration__c(Name = 'BAT19_OldCodeCampagne', BatchSize__c = 200);
            Test.startTest();
            System.schedule('Batch BAT19' + Datetime.now().format(),  '0 0 0 * * ?',  new BAT19_OldCodeCampagne());
            Test.stopTest();
        }        
    }

    @isTest
    static void testBatchExecute() {
        System.runAs(mainUser) {
            String oldCpgId = [SELECT Id, Campagne__c FROM Account WHERE Id =: testAcc.Id].Campagne__c;
            System.assertEquals(testLstCpg[0].Id, oldCpgId);
            Test.startTest();
            Database.executeBatch(new BAT19_OldCodeCampagne());
            Test.stopTest();
            Account acc = [SELECT Id, Campagne__c, Date_d_affectation__c, Date_fin_retombees__c FROM Account WHERE Id =: testAcc.Id];
            System.assertEquals(testLstCpg[1].Id, acc.Campagne__c);
            System.assertEquals(System.today(), acc.Date_d_affectation__c);
            System.assertEquals(testLstCpg[1].EndDate, acc.Date_fin_retombees__c);
        }
    }
}