public  interface ITriggerHandleable {
    void onBeforeInsert(List<Sobject> newList);
    void onAfterInsert(List<Sobject> newList);
    void onBeforeUpdate(List<Sobject> newList, Map<Id, Sobject> oldMap);
    void onAfterUpdate(List<Sobject> newList, Map<Id, Sobject> oldMap);
    void onBeforeDelete(List<Sobject> old);
    void onAfterDelete(List<Sobject> old);
}