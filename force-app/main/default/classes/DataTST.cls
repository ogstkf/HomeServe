/** Classe pour créer un jeu de test complet **/
@isTest
public class DataTST{

    private static final String personAccountRecordType = System.Label.PersonAccountLabel;
    private static final String assetType = 'Chaudière gaz';
    public static final String promoCode = 'ABC123';
    
    // Créé une liste de comptes
    public static List<Account> createAccounts(Integer nbAccountsToCreate){
        List<Account> result = new List<Account>();

        for(Integer i = 0; i < nbAccountsToCreate; i++){
            result.add(new Account(
                RecordTypeId = Utils.getAccountRecordTypeId(personAccountRecordType),
                FirstName = 'FirstName' + i,
                LastName = 'LastName' + i,
                PersonEmail = 'cham.user.test@cham.com',
                PersonMobilePhone = '12345'
            ));
        }
        
        insert result;
        for(Integer i =0; i <nbAccountsToCreate; i++ ){
         sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(result.get(i).Id);
            result.get(i).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
        }
        update result;
        result = [
            SELECT Id, FirstName, LastName, PersonEmail,  PersonContactId
            FROM Account
            WHERE Id IN :result
        ];

        System.debug('--- accountList: ' + result);

        return result;
    }

    // Créé une liste d'utilisateurs
    public static List<User> createUsers(Integer nbUsersToCreate, List<Account> accountsToLink){
      List<User> result = new List<User>();
      User adminUser = TestFactory.createAdminUser('AP08_GenerateVisitNotice_TEST@test.COM', TestFactory.getProfileAdminId());


    //   for(Integer i = 0; i < nbUsersToCreate; i++){
        System.runAs(adminUser) {
        Profile portalProfile = [SELECT Id FROM Profile Limit 1];
        result.add(new User(
            FirstName = accountsToLink[0].FirstName,
            LastName = accountsToLink[0].LastName,
            //ContactId = accountsToLink[0].PersonContactId;
            Username = accountsToLink[0].FirstName + '.' + accountsToLink[0].LastName + '@cham.fr',
            Email = accountsToLink[0].PersonEmail,
            TimeZoneSidKey = 'America/Phoenix',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'en_US',
             //UserRoleId = portalRole.Id,
            ProfileId = TestFactory.getProfileAdminId(),
            IsActive = true,
            Alias = 'user1' 
        ));

        insert result;
      }

      return result;
    }

    // Créé une liste d'utilisateurs pour contourner le pb : 'FIELD_INTEGRITY_EXCEPTION, only portal users can be associated to a contact'
    public static List<User> createUserss(Integer nbUsersToCreate){
        List<User> result = new List<User>();
       // User adminUser = TestFactory.createAdminUser('AP08_GenerateVisitNotice_TEST@test.COM', TestFactory.getProfileAdminId());
      
        Account acc1 = new Account (
            Name = 'newAcc1'
            );  
            insert acc1;
            Contact conCase = new Contact (
            AccountId = acc1.id,
            LastName = 'portalTestUserv1'
            );
            insert conCase;

            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
      //   for(Integer i = 0; i < nbUsersToCreate; i++){
          System.runAs(thisUser) {
           // Profile prfile = [select Id,name from Profile where Name = 'Standard User' LIMIT 1];
            UserRole ur = [Select Id, PortalType, PortalAccountId From UserRole where PortalType ='None' limit 1];
            Profile p = [select Id,name from Profile where Name = 'User' LIMIT 1];

          result.add(new User(
              FirstName = 'FirstName',
              LastName = 'LastName',
             // ContactId = conCase.Id,
              Username = 'FirstNameLastName@cham.fr',
              Email = 'firstNLastN@gmail.com',
              TimeZoneSidKey = 'America/Los_Angeles',
              LocaleSidKey = 'en_US',
              EmailEncodingKey = 'UTF-8',
              LanguageLocaleKey = 'en_US',
              UserRoleId = ur.Id,
              ProfileId = p.Id,
              Alias = 'user1' 
          ));
  
          insert result;
        }
  
        return result;
      }

    // Créé une liste d'operating hours
    public static List<OperatingHours> createOperatigHours(Integer nbOperatingHoursToCreate){
        List<OperatingHours> result = new List<OperatingHours>();

        for(Integer i = 0; i < nbOperatingHoursToCreate; i++){
            result.add(new OperatingHours(
                Name = 'Operating hour ' + i
            ));
        }

        insert result;

        return result;
    }
    
    public static sofactoapp__Raison_Sociale__c createRaisonSocial(){
        sofactoapp__Raison_Sociale__c raison = new sofactoapp__Raison_Sociale__c();
        raison.Name = 'raisonsocia';
        raison.sofactoapp__Credit_prefix__c = 'toto';
       raison.sofactoapp__Invoice_prefix__c = 'titi';
        return raison;
    }
    
    public static sofactoapp__Plan_comptable__c createPlanComptable(){
        sofactoapp__Plan_comptable__c plan = new sofactoapp__Plan_comptable__c();
        plan.Name = 'sofactoapp__Plan_comptable__c';
        insert plan;
        return plan;
    }
    
    public static sofactoapp__Compte_comptable__c createCompteComptable(){
        sofactoapp__Plan_comptable__c chartAccount = createPlanComptable();
        sofactoapp__Compte_comptable__c compte = new sofactoapp__Compte_comptable__c();
        compte.Name = 'compte comptable';
        compte.sofactoapp__Plan_comptable__c = chartAccount.Id;
        compte.sofactoapp__Libelle__c = 'sofactoapp__Libelle__c';
        insert compte;
        return compte;
            
    }
    
    public static sofactoapp__Compte_auxiliaire__c createCompteAuxilaire(Id accountId){
        sofactoapp__Compte_comptable__c comptable = createCompteComptable();
        sofactoapp__Compte_auxiliaire__c compte = new sofactoapp__Compte_auxiliaire__c();
        compte.sofactoapp__Compte__c = accountId;
        compte.sofactoapp__Compte_comptable__c = comptable.Id;
        compte.Compte_comptable_Prelevement__c = comptable.Id;
        compte.sofactoapp__Libelle__c = 'sofactoapp__Libelle__c';
        insert compte;
        return compte;
        
    }

    // Créé une liste de service territories
    public static List<ServiceTerritory> createServiceTerritories(Integer nbServiceTerritoriesToCreate, OperatingHours operatingHourToUse){
        List<ServiceTerritory> result = new List<ServiceTerritory>();
        sofactoapp__Raison_Sociale__c raison = new sofactoapp__Raison_Sociale__c();
        raison.Name = 'raisonsocia';
        raison.sofactoapp__Credit_prefix__c = 'toto';
       raison.sofactoapp__Invoice_prefix__c = 'titi';
        insert raison;
        for(Integer i = 0; i < nbServiceTerritoriesToCreate; i++){
            result.add(new ServiceTerritory(
                Name = 'Service Territory ' + i,
                OperatingHoursId = operatingHourToUse.Id,
                IsActive = true
               , Sofactoapp_Raison_Social__c=raison.Id
                ,
                FSL__TerritoryLevel__c=1
            ));
        }       

        insert result;

        return result;
    }

    // Créé une liste de logements
    public static List<Logement__c> createLogements(Integer nbLogementsToCreate, List<Account> accountsToLink, List<ServiceTerritory> serviceTerritoriesToLink){
        List<Logement__c> result = new List<Logement__c>();

        for(Integer i = 0; i < nbLogementsToCreate; i++){
            result.add(new Logement__c(
                Account__c = accountsToLink[0].Id,
                Agency__c = serviceTerritoriesToLink[0].Id,
                Street__c = '15 Avenue des Champs-Elysées',
                City__c = 'Paris',
                Postal_Code__c = '75009'

            ));
        }

        for(Integer i = 0; i < nbLogementsToCreate; i++){
            result.add(new Logement__c(
                Account__c = accountsToLink[0].Id,
                Agency__c = serviceTerritoriesToLink[1].Id,
                Street__c = '15 Avenue des Champs-Elysées',
                City__c = 'Paris',
                Postal_Code__c = '75009'
            ));
        }

        insert result;

        return result;
    }

    // Créé une liste d'équipements
    public static List<Asset> createAssets(Integer nbAssetsByAccount, List<Account> accountsToLink, List<Logement__c> logementsToLink){
      List<Asset> result = new List<Asset>();
      System.debug('###logement ' + logementsToLink);
      for(Integer i = 0; i < 5; i++){
            result.add(new Asset(
                AccountId = accountsToLink[0].Id,
                Name = 'Asset1 ' + i,
                Equipment_type__c = assetType,
                Logement__c = logementsToLink[0].Id
            ));
        }

        for(Integer i = 0; i < 5; i++){
            result.add(new Asset(
                AccountId = accountsToLink[0].Id,
                Name = 'Asset2 ' + i,
                Equipment_type__c = assetType,
                Logement__c = logementsToLink[1].Id
            ));
        }
        
        insert result;

        return result;
    }

    // Créé une liste de contrats
    // public static List<ServiceContract> createContracts(Integer nbContractsToCreate, List<Account> accountsToLink, List<Asset> assetsToLink, List<ServiceTerritory> serviceTerritories, Date startDateToUse, Integer contractTerm){
    //     List<ServiceContract> result = new List<ServiceContract>();

    //     for(Integer i = 0; i < nbContractsToCreate; i++){
    //         result.add(new ServiceContract(
    //             Name = 'Contract ' + i,
    //             AccountId = accountsToLink[0].Id,
    //             Asset__c = assetsToLink[0].Id,
    //             Agency__c = serviceTerritories[0].Id,
    //             StartDate = startDateToUse,
    //             Term = contractTerm
    //         ));
    //     }

    //     insert result;

    //     return result;
    // }
       // Créé une liste de contrats
    public static List<Contract> createContracts(Integer nbContractsToCreate, List<Account> accountsToLink, List<Asset> assetsToLink, List<ServiceTerritory> serviceTerritories, Date startDateToUse, Integer contractTerm){
        List<Contract> result = new List<Contract>();

        for(Integer i = 0; i < nbContractsToCreate; i++){
            result.add(new Contract(
                Name = 'Contract ' + i,
                AccountId = accountsToLink[0].Id,
                Covered_Equipment__c = assetsToLink[0].Id,
                Issuing_Agency__c = serviceTerritories[0].Id,
                StartDate = startDateToUse,
                ContractTerm = contractTerm
            ));
        }

        insert result;

        return result;
    }

    // Créé une liste de rendez-vous
    public static List<ServiceAppointment> createMeetings(Integer nbMeetingsToCreate, List<String> parentsRecordId, DateTime dateTimeToUse, String status){
        List<ServiceAppointment> result = new List<ServiceAppointment>();

        for(Integer i = 0; i < nbMeetingsToCreate; i++){
            result.add(new ServiceAppointment(
                ParentRecordId = parentsRecordId[0],
                Status = status,
                EarliestStartTime = dateTimeToUse.addDays(-10),
                SchedStartTime = dateTimeToUse.addDays(-10),
                DueDate = dateTimeToUse.addDays(-10),
                SchedEndTime = dateTimeToUse.addDays(-10).addHours(2)
            ));
        }

        for(Integer i = 0; i < nbMeetingsToCreate; i++){
            result.add(new ServiceAppointment(
                ParentRecordId = parentsRecordId[0],
                Status = status,
                EarliestStartTime = dateTimeToUse.addMonths(2),
                SchedStartTime = dateTimeToUse.addMonths(2),
                DueDate = dateTimeToUse.addMonths(2),
                SchedEndTime = dateTimeToUse.addMonths(2).addHours(2)
            ));
        }

        insert result;

        return result;
    }

    // Créé une liste de produit
    public static List<Product2> createProductsAndPriceBookEntries(Integer nbProductsToCreate){
        List<Product2> result = new List<Product2>();
        List<PricebookEntry> priceBookEntries = new List<PricebookEntry>();

        Id pricebookId = Test.getStandardPricebookId();
        String assetTpe = 'Pompe à chaleur air/eau';

        for(Integer i = 0; i < nbProductsToCreate; i++){
            result.add(new Product2(
                Name = 'Produit ' + i,
                Statut__c = 'Approuvée',
                ProductCode = 'abcde' + i,
                IsActive = TRUE,
                Equipment_family__c = 'Pompe à chaleur',
                Equipment_type__c = assetTpe
            ));
        }

        insert result;

        for(Product2 aProduct : result){
            priceBookEntries.add(new PricebookEntry(
                Pricebook2Id = pricebookId,
                IsActive = TRUE,
                Product2Id = aProduct.Id,
                UnitPrice =  1
            ));
        }

        insert priceBookEntries;

        return result;
    }

    // Créé une liste de catalogues d'agences
    public static List<Agency_Catalogue__c> createAgencyCatalogues(List<Product2> productList, List<ServiceTerritory> serviceTerritories){
        List<Agency_Catalogue__c> result = new List<Agency_Catalogue__c>();

        String chaudiereGaz = 'Chaudière gaz';
        String chamDigital = 'Cham Digital';

        for(ServiceTerritory aServiceTerritory : serviceTerritories){
            
            for(Integer i = 0; i < productList.size(); i++){
                result.add(new Agency_Catalogue__c(
                    Name = 'Agence ' + i,
                    Agency__c = aServiceTerritory.Id,
                    Product__c = productList[i].Id,
                    IsActive__c = TRUE,
                    Equipement__c = chaudiereGaz,
                    Channels__c = chamDigital,
                    OfferCode__c = '1234'
                ));
            }
        }

        insert result;

        return result;
    }

    // Créé une liste de zone d'intervention des agences
    public static List<Agency_intervention_zones__c> createAgencyInterventionZones(List<ServiceTerritory> serviceTerritories){
        List<Agency_intervention_zones__c> result = new List<Agency_intervention_zones__c>();

        List<String> postalCodes = new List<String>{
            '18000',
            '75009',
            '59000'
        };

        for(Integer i = 0; i < postalCodes.size(); i++){
            result.add(new Agency_intervention_zones__c(
                Agence__c = serviceTerritories[i].Id,
                Remote_Zone__c = TRUE,
                Zip_Code__c = postalCodes[i]
            ));
        }

        insert result;

        return result;
    }

    // Créé une liste d'opportunités
    public static List<Opportunity> createOpportunities(Integer nbOpportunitiesToCreate, Account accountToUse, Product2 productToLink, Agency_intervention_zones__c agencyInterventionZone){
        List<Opportunity> result = new List<Opportunity>();
        List<OpportunityLineItem> opportunityLineItems = new List<OpportunityLineItem>();

        String opportunityStage = 'Qualification';
        String opportunityType = 'New Business';
        String leadSource = 'Site Cham Digital';
        String offerLabel = 'CONTRAT SÉCURITÉ';
        DateTime now = DateTime.now();
        Decimal ht = 100;
        Decimal tva = 5.5;
        Id pricebookId = Test.getStandardPricebookId();

        for(Integer i = 0; i < nbOpportunitiesToCreate; i++){
            result.add(new Opportunity(
                Name = now.format('dd/MM/yyyy HH:mm:ss'),
                AccountId = accountToUse.Id,
                Pricebook2Id = pricebookId,
                VAT_rate__c = tva,
                Personnal_with_residence_less_than_2__c = true,
                Personnal_with_residence_older_than_2__c = false,
                Other_profiles_pros__c = false,
                HEP_Equipment__c = false,
                StageName = opportunityStage,
                CloseDate = Date.today(),
                Type = opportunityType,
                Pre_tax_Price__c = ht,
                Agency__c = agencyInterventionZone.Agence__c,
                LeadSource = leadSource,
                Zip_code_entered__c = agencyInterventionZone.Zip_Code__c,
                Offer_Label__c = offerLabel
            ));
        }

        insert result;

        for(Opportunity anOpportunity : result){
            opportunityLineItems.add(new OpportunityLineItem(
                OpportunityId = anOpportunity.Id,
                Product2Id = productToLink.Id,
                Quantity = 1,
                UnitPrice = 1,
                Description = 'Bla bla'
            ));
        }

        insert opportunityLineItems;

        return result;
    }

    // Créé une liste de rendez-vous disponibles pour une agence Cham
    public static List<Agency_Fixed_Slots__c> createAgencyFixedSlots(Integer nbRecordsToCreate, List<ServiceTerritory> serviceTerritories){
        List<Agency_Fixed_Slots__c> result = new List<Agency_Fixed_Slots__c>();
        Date today = Date.today().addDays(10);
        Integer slotTime = 2;

        for(ServiceTerritory aServiceTerritory : serviceTerritories){
            Integer i = 0;
            Integer a;
            today = today.addDays(1);
            
            // Nombre de jours à créer par agence
            while(i < nbRecordsToCreate){
                DateTime dateTimeToUse = DateTime.newInstanceGmt(today.year(), today.month(), today.day());    
                a = 0;

                // Nombre de slots par jour par agence
                while(a < 3){
                    result.add(new Agency_Fixed_Slots__c(
                        Name = 'Slot ' + i,
                        Agence__c = aServiceTerritory.Id,
                        Start_Time_of_the_slot__c = dateTimeToUse,
                        EndTime_of_the_slot__c  = dateTimeToUse.addHours(slotTime),
                        Slot_taken__c = false
                        //Slot_for__c = ScheduleMeetingCtrl.chamDigitalSlot
                    ));

                    dateTimeToUse = dateTimeToUse.addHours(slotTime);
                    i++;
                    a++;
                }
            }
        }

        insert result;

        return result;
    }

    // Créé une liste de rendez-vous
    public static List<ServiceAppointment> createServiceAppointments(Integer nbRecordsToCreate, List<Contract> contracts, List<Asset> assets, List<Opportunity> opportunities, List<Logement__c> logements, List<Agency_Fixed_Slots__c> slots){
        List<ServiceAppointment> result = new List<ServiceAppointment>();

        for(Integer i = 0; i < nbRecordsToCreate; i++){
            result.add(new ServiceAppointment(
                //Subject = ScheduleMeetingCtrl.meetingSubject,
                ParentRecordId = assets[i].Id,
                Fixed_slot__c = slots[i].Id,
                Opportunity__c = opportunities[i].Id,
                Contract__c = contracts[i].Id,
                //Category__c = ScheduleMeetingCtrl.meetingSubjectAPI,
                ServiceTerritoryId = logements[i].Agency__c,
                Residence__c = logements[i].Id,
                //Status = ScheduleMeetingCtrl.meetingStatus,
                ContactId = logements[i].Main_Contact__c,
                Street = logements[i].Street__c,
                PostalCode = logements[i].Postal_Code__c,
                City = logements[i].City__c,
                Country = logements[i].Country__c,
                DueDate = slots[i].Start_Time_of_the_slot__c,
                EarliestStartTime = slots[i].Start_Time_of_the_slot__c,
                SchedStartTime = slots[i].Start_Time_of_the_slot__c,
                SchedEndTime = slots[i].Start_Time_of_the_slot__c.addHours(2),
                Duration = 2
            ));
        }

        insert result;

        return result;
    }

    // Créé une liste d'offres
    public static List<Offer__c> createOffers(Integer nbRecordsToCreate, Decimal discountToUse, List<Product2> products){
        List<Offer__c> result = new List<Offer__c>();
        Date startDate = Date.today().addDays(-10);

        for(Integer i = 0; i < nbRecordsToCreate; i++){
            result.add(new Offer__c(
                Name = 'Offre ' + i,
                Product_of_the_offer__c = products[i].Id,
                Discount__c = discountToUse,
                Start_date_of_the_offer__c = startDate,
                End_date_of_the_offer__c = startDate.addMonths(1)
            ));
        }

        insert result;

        return result;
    }

    // Créé une liste de promotions
  /*  public static List<Promotional_Code__c> createPomoCodes(Integer nbRecordsToCreate, List<Offer__c> offers){
        List<Promotional_Code__c> result = new List<Promotional_Code__c>();

        for(Integer i = 0; i < nbRecordsToCreate; i++){
            result.add(new Promotional_Code__c(
                Name = DataTST.promoCode  + i,
                Offer__c = offers[i].Id,
                Max_number_of_use__c = 5,
                Number_of_time_where_the_code_is_used__c = 1
            ));
        }
        
        insert result;

        return result;
    }*/

    // START ARA 02/08/2019
    // create serviceContract
    public static List<serviceContract> createServiceContrract(String accId,String asssetId){
        List<serviceContract> lstserviceContr = new List<serviceContract>();
        for(Integer i=0;i<4;i++){
            lstserviceContr.add(new serviceContract(Name='testservicecontract'+ i
                                                    ,Contract_Status__c ='Active'
                                                    ,AccountId = accId
                                                    ,Asset__c = asssetId));
        }

        insert lstserviceContr;
        
        return lstserviceContr;
    }

    //RRJ 20190802
    // Créé une liste de rendez-vous
    public static List<ServiceAppointment> createServiceAppointments(Integer nbRecordsToCreate, List<ServiceContract> contracts, List<Asset> assets, List<Opportunity> opportunities, List<Logement__c> logements, List<Agency_Fixed_Slots__c> slots){
        List<ServiceAppointment> result = new List<ServiceAppointment>();

        for(Integer i = 0; i < nbRecordsToCreate; i++){
            result.add(new ServiceAppointment(
                //Subject = ScheduleMeetingCtrl.meetingSubject,
                ParentRecordId = assets[i].Id,
                Fixed_slot__c = slots[i].Id,
                Opportunity__c = opportunities[i].Id,
                Service_Contract__c = contracts[i].Id,
                //Category__c = ScheduleMeetingCtrl.meetingSubjectAPI,
                ServiceTerritoryId = logements[i].Agency__c,
                Residence__c = logements[i].Id,
                //Status = ScheduleMeetingCtrl.meetingStatus,
                ContactId = logements[i].Main_Contact__c,
                Street = logements[i].Street__c,
                PostalCode = logements[i].Postal_Code__c,
                City = logements[i].City__c,
                Country = logements[i].Country__c,
                DueDate = slots[i].Start_Time_of_the_slot__c,
                EarliestStartTime = slots[i].Start_Time_of_the_slot__c,
                SchedStartTime = slots[i].Start_Time_of_the_slot__c,
                SchedEndTime = slots[i].Start_Time_of_the_slot__c.addHours(2),
                Duration = 2
            ));
        }

        insert result;

        return result;
    }

    // create serviceContract
    public static List<serviceContract> createServiceContracts(Integer numToCreate, List<Account> accList, List<Asset> astList, List<ServiceTerritory> lstServTerritory, Date startDateToUse, Integer contractTerm){
        //nbRecordsToCreate, accountList, assetList, serviceTerritoryList, Date.today(), 12
        List<ServiceContract> lstServiceContr = new List<ServiceContract>();
        for(Integer i=0; i<numToCreate; i++){
            lstServiceContr.add(new ServiceContract(
                Name='testservicecontract'+ i
                ,Contract_Status__c ='Active'
                ,AccountId = accList[0].Id
                ,Asset__c = astList[0].Id
                ,Agency__c = lstServTerritory[0].Id
                ,StartDate = startDateToUse
                ,Term = contractTerm
            ));
        }

        // insert lstServiceContr;

        return lstServiceContr;
    }

    // Créé une liste de logements
    public static List<Logement__c> createLogements2(List<Account> accountsToLink, List<ServiceTerritory> serviceTerritoriesToLink){
        List<Logement__c> result = new List<Logement__c>();

        for(integer i=0; i<accountsToLink.size(); i++){
            for(Integer j=0; i<serviceTerritoriesToLink.size(); i++){
                result.add(new Logement__c(
                    Account__c = accountsToLink[i].Id,
                    Agency__c = serviceTerritoriesToLink[j].Id,
                    Street__c = '15 Avenue des Champs-Elysées',
                    City__c = 'Paris',
                    Postal_Code__c = '75009'

                ));
            }
        }

        insert result;

        return result;
    }

    //END RRJ 20190802
}