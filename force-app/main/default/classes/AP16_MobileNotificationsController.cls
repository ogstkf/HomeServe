public class AP16_MobileNotificationsController {

    
    @AuraEnabled
    public static List<Map<String,Object>> getMessages(){
        List<Mobile_Notifications__c> notifs = [SELECT Id, Name, Status__c, all_ressources__c, message__c, Agency__r.Name FROM Mobile_Notifications__c ORDER BY CreatedDate DESC];
        
        List<Map<String,Object>> result = new List<Map<String,Object>>();
        for(Mobile_Notifications__c notif : notifs){
            Map<String,Object> line = new Map<String,Object>();
            line.put('Id', notif.Id);
            line.put('Name', notif.Name);
            line.put('Agency__c', notif.Agency__r.Name);
            line.put('Status__c', notif.Status__c);
            line.put('all_ressources__c', notif.all_ressources__c);
            line.put('message__c', notif.message__c);
            result.add(line);
        }
        
        return result;
    }
    
    @AuraEnabled
    public static String sendMessage(Id messageId){ 
        
        Mobile_Notifications__c notif  = [SELECT Id, Status__c FROM Mobile_Notifications__c   WHERE Id = :messageId];
        notif.Status__c = 'Sent';
        
        update notif;
        return 'success';
    }
    @AuraEnabled
    public static String deleteMessage(Id messageId){
        
        Mobile_Notifications__c notif  = [SELECT Id, Status__c FROM Mobile_Notifications__c   WHERE Id = :messageId];
        
        delete notif;
        return 'success';
    }
    @AuraEnabled
    public static List<Map<String,Object>> createMessage(String message, String agency, Boolean allResources){
        Mobile_Notifications__c notif = new Mobile_Notifications__c();
        notif.Agency__c = agency;
        notif.message__c = message;
        notif.all_ressources__c = allResources;
       	notif.Status__c = 'waiting';
        
        insert notif;
        
        return getMessages();
    }
    
}