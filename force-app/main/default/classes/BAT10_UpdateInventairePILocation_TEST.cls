/**
 * @File Name          : BAT10_UpdateInventairePILocation_TEST.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-08-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/03/2020   ZJO     Initial Version
**/
@isTest
public with sharing class BAT10_UpdateInventairePILocation_TEST {
    static User adminUser;
    static Account testAcc;
    static List<sofactoapp__Raison_Sociale__c> lstSofactos;
    static Set<Id> setAgenceId = new Set<Id>();
    static List<ServiceTerritory> lstAgences;
    static List<Order> lstOrder;
    static List<Shipment> lstShipments;
    static List<ProductTransfer> lstProdTransfer;
    static  Bypass__c userBypass = new Bypass__c();
    static Account FournisseurAcc;

    static{
        adminUser = TestFactory.createAdminUser('batch1@test.com', TestFactory.getProfileAdminId());
        insert adminUser;
        userBypass.SetupOwnerId  = adminUser.Id;
        userBypass.BypassValidationRules__c = true;
        insert userBypass;

        System.runAs(adminUser) {
            
            //Create Account
            testAcc = TestFactory.createAccount('Test Acc');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            FournisseurAcc= new Account(Name='ChamFournissuer');
            FournisseurAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Fournisseurs').getRecordTypeId();
            insert FournisseurAcc;

            //Create operating hour
            OperatingHours newOperatingHour = TestFactory.createOperatingHour('test1');
            insert newOperatingHour;

            //List of Sofactos
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            sofactoapp__Raison_Sociale__c sofa1 = new sofactoapp__Raison_Sociale__c(Name= 'TEST1', sofactoapp__Credit_prefix__c= '2341', sofactoapp__Invoice_prefix__c='4321');
            sofactoapp__Raison_Sociale__c sofa2 = new sofactoapp__Raison_Sociale__c(Name= 'TEST2', sofactoapp__Credit_prefix__c= '2342', sofactoapp__Invoice_prefix__c='4322');
            
            lstSofactos = new List<sofactoapp__Raison_Sociale__c>{
               sofa,
               sofa1,
               sofa2
            };
            insert lstSofactos;
  
             //List of Agences
             ServiceTerritory agence1 = TestFactory.createServiceTerritory('agence 1', newOperatingHour.Id, sofa.Id);
             agence1.Inventaire_en_cours__c = false;
             ServiceTerritory agence2 = TestFactory.createServiceTerritory('agence 2', newOperatingHour.Id, sofa1.Id);
             agence2.Inventaire_en_cours__c = false;
             ServiceTerritory agence3 = TestFactory.createServiceTerritory('agence 3', newOperatingHour.Id, sofa2.Id);
             agence3.Inventaire_en_cours__c = false;

             lstAgences = new List<ServiceTerritory>{
                 agence1,
                 agence2,
                 agence3
             };

             insert lstAgences;

            //Populate Set AgenceId

            for (ServiceTerritory agence : lstAgences){
                setAgenceId.add(agence.Id);
            }

            //List of Orders
            lstOrder = new List<Order>{
                new Order (
                    AccountId = FournisseurAcc.Id,
                    EffectiveDate = System.today(),
                    Status = 'Demande d\'achats'
                    )
            };
            insert lstOrder;

            //List of Shipments
            lstShipments = new List<Shipment>{
                new Shipment (
                    ShipToName = 'Shipment 1',
                    Order__c = lstOrder[0].Id,
                    Status = 'Livré',
                    N_de_bon_de_livraison__c = '2'
                ),
                new Shipment (
                    ShipToName = 'Shipment 2',
                    Order__c = lstOrder[0].Id,
                    Status = 'Livré',
                    N_de_bon_de_livraison__c = '2'
                ),
                new Shipment (
                    ShipToName = 'Shipment 3',
                    Order__c = lstOrder[0].Id,
                    Status = 'Livré',
                    N_de_bon_de_livraison__c = '2'
                )    
            };
            insert lstShipments;

             //Create Source Location
             Schema.Location sourceLoc = new Schema.Location(
             Name= 'Test Souce Loc',
             LocationType='Virtual',
             Agence__c = lstAgences[0].Id);
       
             insert sourceLoc;

              //Create Destination Location
              Schema.Location desLoc = new Schema.Location(
                Name= 'Test Destination Loc',
                LocationType='Virtual',
                Agence__c = lstAgences[1].Id);
          
                insert desLoc;
    
            //List of Product Transfers
            lstProdTransfer = new List<ProductTransfer>{
                new ProductTransfer (
                    ShipmentId = lstShipments[0].Id,
                    SourceLocationId = sourceLoc.Id, 
                    DestinationLocationId = desLoc.Id,   
                    QuantitySent = 1,
                    QuantityReceived = 1,
                    Inventaire_en_cours__c = True
                ),
                new ProductTransfer (
                    ShipmentId = lstShipments[1].Id,
                    SourceLocationId = sourceLoc.Id,  
                    DestinationLocationId = desLoc.Id,     
                    QuantitySent = 2,
                    QuantityReceived = 1,
                    Inventaire_en_cours__c = True
                ),
                new ProductTransfer (
                    ShipmentId = lstShipments[2].Id,
                    SourceLocationId = sourceLoc.Id, 
                    DestinationLocationId = desLoc.Id, 
                    QuantitySent = 3,
                    QuantityReceived = 2,
                    Inventaire_en_cours__c = True
                )      
            };
            insert lstProdTransfer;
        }

    }

    @isTest
    public static void testBatch1(){
        System.runAs(adminUser){
            Test.startTest();
                BAT10_UpdateInventairePILocation batch = new BAT10_UpdateInventairePILocation(setAgenceId);
                Database.executeBatch(batch);
            Test.stopTest();

        List<ProductTransfer> lstProdTransfers = [SELECT Id, DestinationLocation.Agence__c, IsReceived, Inventaire_en_cours__c FROM ProductTransfer Where DestinationLocation.Agence__c IN: setAgenceId];
        system.assertEquals(lstProdTransfers[0].IsReceived, True);
        system.assertEquals(lstProdTransfers[0].Inventaire_en_cours__c, False);
        
        }  
    }
}