public without sharing class TaskTriggerHandler {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Assigned ownerID to Admin de l'agence
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 13-AUG-2019  DMU    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/    

    public void handleAfterInsert(List<Task> lstNewTask){
        System.debug('***in handleAfterInsert');

        List<Task> lstTasksToUpdate = new List<Task>();
        map<string,string> mapTaskIdQueueId = new map<string,string>();
        Map<string, list<string>> mapQueueUsers = new Map<string, List<string>>();
        list <string> lstUserId = new list <string>(); 
        list<Task> lstNewTaskToIns = new list<Task>();

        for(Task ta : lstNewTask){
            if(string.IsNotBlank(ta.TECH_Support_Group__c)){
                mapTaskIdQueueId.put(ta.Id,ta.TECH_Support_Group__c);                
            }
        }
        system.debug('***mapTaskIdQueueId size: '+mapTaskIdQueueId.size());        

        for(GroupMember gm : [SELECT Group.Id, Group.Name, UserOrGroupId FROM GroupMember where Group.Id in: mapTaskIdQueueId.values()]){
            lstUserId.add(gm.UserOrGroupId);
            mapQueueUsers.put(gm.Group.Id, lstUserId);
        }        
        system.debug('***mapQueueUsers size: '+mapQueueUsers.size());
        
        for(Task ta : lstNewTask){
            if(string.IsNotBlank(ta.TECH_Support_Group__c) && !ta.IsChildTask__c){
                if(mapQueueUsers.get(ta.TECH_Support_Group__c).size()>0){
                    for(integer i=0; i<mapQueueUsers.get(ta.TECH_Support_Group__c).size(); i++){
                        Task newTask = new Task(
                            subject = ta.subject, 
                            priority = ta.priority, 
                            status = ta.status,
                            WhatId = ta.WhatId,
                            Description = ta.Description,
                            TaskSubtype = ta.TaskSubtype,
                            Work_In_Progress__c = ta.Work_In_Progress__c,
                            Work_In_Progress_By__c = ta.Work_In_Progress_By__c,
                            WhoId = ta.WhoId,
                            ownerId = Id.valueOf(mapQueueUsers.get(ta.TECH_Support_Group__c)[i]),
                            TECH_GroupTask_CommonId__c = ta.Id,
                            IsChildTask__c = true
                            ,TECH_Support_Group__c = ta.TECH_Support_Group__c
                            );
                        lstNewTaskToIns.add(newTask);
                    }
                    lstTasksToUpdate.add(new Task(Id = ta.Id, Status = 'Completed'));
                }
            }
        }
        system.debug('***lstNewTaskToIns size: '+lstNewTaskToIns.size());
        if(lstNewTaskToIns.size()>0){
            insert lstNewTaskToIns;
        }      

        //SRA 28082019
        if(lstTasksToUpdate.size()>0){
            // update lstTasksToUpdate;
            Delete lstTasksToUpdate;
        }      
    }

    public void handleAfterUpdate(List<Task> lstOldTask, List<Task> lstNewTask){
        System.debug('***in handleAfteUpdate');

        // map <string, string> mapTaskOwnerId = new map <string, string>();
        map <string, Task> mapTaskOwnerId = new map <string, Task>();
        map<string, string> mapTaskIdUserWorkInProg = new map <string, string>();
        List<Task> lstTasks = new List<Task>();
        // map<string,string> mapTaskStatus = new map <string,string>();
        map<string,Task> mapTaskStatus = new map <string,Task>();
        map<string,string> mapTaskStatusToUpt = new map <string,string>();
        set<string> setTaskIdUserWorkInProg = new set<string>();

        Id systemAdminProfileId = [Select Id From Profile
	            Where name = 'Administrateur système'
	            OR name = 'System Administrator'
	            OR name = 'Amministratore del sistema'
	            OR name = 'Systemadministrator'
	            OR name = 'Systemadministratör'
	            OR name = 'Administrador do sistema'
	            OR name = 'Systeembeheerder'
	            OR name = 'Systemadministrator'
            limit 1    
         ].Id;

        for(integer i=0;i<lstNewTask.size();i++){            
            
            if(lstNewTask[i].Work_In_Progress__c != lstOldTask[i].Work_In_Progress__c && lstNewTask[i].Work_In_Progress__c == true && lstNewTask[i].Work_In_Progress_By__c == null) {    
                setTaskIdUserWorkInProg.add(lstNewTask[i].TECH_GroupTask_CommonId__c);
                mapTaskIdUserWorkInProg.put(lstNewTask[i].Id, lstNewTask[i].TECH_GroupTask_CommonId__c);                
                // mapTaskOwnerId.put(lstNewTask[i].TECH_GroupTask_CommonId__c, lstNewTask[i].OwnerId);
                mapTaskOwnerId.put(lstNewTask[i].TECH_GroupTask_CommonId__c, lstNewTask[i]);
            }
            else if(lstOldTask[i].Work_In_Progress__c == true && lstNewTask[i].Status != lstOldTask[i].Status && lstNewTask[i].Status == 'Completed' && lstNewTask[i].Work_In_Progress_By__c != null && (UserInfo.getUserId() == lstOldTask[i].Work_In_Progress_By__c || UserInfo.getProfileId() == systemAdminProfileId) ){
                mapTaskStatus.put(lstNewTask[i].TECH_GroupTask_CommonId__c, lstNewTask[i]);
                mapTaskStatusToUpt.put(lstNewTask[i].Id, lstNewTask[i].TECH_GroupTask_CommonId__c);
            }
            else if(lstNewTask[i].Work_In_Progress__c == false && lstNewTask[i].Work_In_Progress__c != lstOldTask[i].Work_In_Progress__c && lstNewTask[i].Work_In_Progress_By__c != null ) { //&& UserInfo.getUserId() == lstNewTask[i].Work_In_Progress_By__c
                lstTasks.add(lstNewTask[i]);
            }
            else if( (UserInfo.getProfileId() != systemAdminProfileId) && (                
                (lstNewTask[i].Work_In_Progress__c == true && lstOldTask[i].Work_In_Progress__c != lstNewTask[i].Work_In_Progress__c && lstNewTask[i].Work_In_Progress_By__c != null && UserInfo.getUserId() != lstNewTask[i].Work_In_Progress_By__c )
             || (lstNewTask[i].Work_In_Progress_By__c != null && UserInfo.getUserId() != lstNewTask[i].Work_In_Progress_By__c &&  lstNewTask[i].Status != lstOldTask[i].Status)
             || (lstOldTask[i].Work_In_Progress_By__c != null && UserInfo.getUserId() != lstOldTask[i].Work_In_Progress_By__c &&  lstNewTask[i].Status != lstOldTask[i].Status)
             || (lstOldTask[i].Work_In_Progress_By__c != null && lstOldTask[i].Work_In_Progress_By__c != lstNewTask[i].Work_In_Progress_By__c && UserInfo.getUserId() != lstOldTask[i].Work_In_Progress_By__c)
            ))
            {             
               lstNewTask[0].addError('Cette tâche est actuellement en cours de traitement par une autre personne : ' + lstOldTask[i].TECH_Work_In_Progress_ByUserName__c + '. Seule cette personne est habilitée à clôturer cette tâche.' );            
            }
        }
        system.debug('***in mapTaskOwnerId size: '+mapTaskOwnerId.size());
        system.debug('***in mapTaskIdUserWorkInProg size: '+mapTaskIdUserWorkInProg.size());

        system.debug('***in mapTaskStatus size: '+mapTaskStatus.size());
        system.debug('***in mapTaskStatusToUpt size: '+mapTaskStatusToUpt.size());

        if(lstTasks.size() > 0) {
            AP13_HandleTask.updateTaskUserWorkInProgress(lstTasks);
        }

        list<Task> lsttaToUpdate = new list <Task> ();

        if(setTaskIdUserWorkInProg.size()>0){
            for(Task taskToUpd : [select id, Work_In_Progress__c, Work_In_Progress_By__c, TECH_GroupTask_CommonId__c from Task where TECH_GroupTask_CommonId__c in: setTaskIdUserWorkInProg]){
                taskToUpd.Work_In_Progress_By__c = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).OwnerId;

                taskToUpd.subject = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).subject;
                taskToUpd.priority = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).priority; 
                taskToUpd.status = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).status;
                taskToUpd.WhatId = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).WhatId;
                taskToUpd.Description = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).Description;
                // taskToUpd.TaskSubtype = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).TaskSubtype;
                taskToUpd.Work_In_Progress__c = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).Work_In_Progress__c;                
                taskToUpd.WhoId = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).WhoId;
                taskToUpd.TECH_Support_Group__c = mapTaskOwnerId.get(taskToUpd.TECH_GroupTask_CommonId__c).TECH_Support_Group__c;

                lsttaToUpdate.add(taskToUpd);
            }
        }

        if(mapTaskStatus.size()>0){
            system.debug('*** in mapTaskStatus>0: ');
            for(Task taskToUpd : [select id, TECH_GroupTask_CommonId__c from Task where TECH_GroupTask_CommonId__c in: mapTaskStatusToUpt.values()]){
                taskToUpd.Status = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).Status;

                taskToUpd.subject = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).subject;
                taskToUpd.priority = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).priority;
                taskToUpd.status = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).status;
                taskToUpd.WhatId = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).WhatId;
                taskToUpd.Description = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).Description;
                // taskToUpd.TaskSubtype = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).TaskSubtype;
                taskToUpd.Work_In_Progress__c = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).Work_In_Progress__c;                
                taskToUpd.WhoId = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).WhoId;              
                taskToUpd.TECH_Support_Group__c = mapTaskStatus.get(taskToUpd.TECH_GroupTask_CommonId__c).TECH_Support_Group__c;

                lsttaToUpdate.add(taskToUpd);
            }
        }

        if(lsttaToUpdate.size()>0){
            system.debug('*** update lsttaToUpdate: '+ lsttaToUpdate.size());
            Set<Task> settaToUpdate = new Set<Task>(lsttaToUpdate);
            lsttaToUpdate.clear();
            lsttaToUpdate.addAll(settaToUpdate);            
            update lsttaToUpdate;
        }
    }
}