/**
 * @File Name          : LC02_EligibilityWizard_TEST.cls
 * @Description        : test class for LC02_EligibilityWizard
 * @Author             : RRJ
 * @Group              : Spoon Consulting
 * @Last Modified By   : MNA
 * @Last Modified On   : 06-08-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    01/08/2019, 18:06:39   RRJ     Initial Version
**/
@isTest
public class LC02_EligibilityWizard_TEST {


    static User adminUser;
    static User adminUser1;
    static Account testAcc1;
    static List<Account> lstAccIns;
    static OperatingHours opHrs = new OperatingHours();
    static ServiceTerritory srvTerr = new ServiceTerritory();
    static ServiceTerritory srvTerr1 = new ServiceTerritory();
    static List<Logement__c> lstLogement = new List<Logement__c>();
    static List<Product2> lstProd = new List<Product2>();
    static sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c();
    static Agency_intervention_zones__c aInZone = new Agency_intervention_zones__c();

    static {
        adminUser = TestFactory.createAdminUser('VFC07_MassAvisDePassagePDF_TEST@test.COM', TestFactory.getProfileAdminId());
        insert adminUser;
        
        adminUser1 = TestFactory.createAdminUser('VFC07_MassAvisDePassagePDF_TEST@test.COM', TestFactory.getProfileAdminId());
        adminUser1.Societe_Agence__c = 'HomeServe';
        insert adminUser1;
        
        System.runAs(adminUser){

            //creating account
            testAcc1 = TestFactory.createAccount('test');
            testAcc1.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc1.FirstName = 'acc';
            testAcc1.LastName = 'test';
            testAcc1.BillingPostalCode = '1233456';
            testAcc1.Phone = '1234567890';
            testAcc1.PersonEmail = 'test@mail.com';
            insert testAcc1;

            lstAccIns = [SELECT Id, ClientNumber__c FROM Account WHERE Id  = :testAcc1.Id];

            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

                     //create Corporate Name
         sofactoapp__Raison_Sociale__c srs = new sofactoapp__Raison_Sociale__c(Name='Test1',
         sofactoapp__Credit_prefix__c='Test1',
         sofactoapp__Invoice_prefix__c='Test1');  
         insert srs; 

         srvTerr = new ServiceTerritory(IsActive = true, Name = 'test Territory' , OperatingHoursId = opHrs.Id, Sofactoapp_Raison_Social__c=srs.Id);
        insert srvTerr;
        srvTerr1 = new ServiceTerritory(IsActive = true, Name = 'HomeServe' , OperatingHoursId = opHrs.Id, Sofactoapp_Raison_Social__c=srs.Id);
        insert srvTerr1;


            //creating logement
            lstLogement.add(TestFactory.createLogement(testAcc1.Id, srvTerr.Id));
            lstLogement[0].Postal_Code__c = '12345';
            lstLogement[0].City__c = 'lgmtCity 1';
            lstLogement[0].Account__c = testAcc1.Id;
            lstLogement[0].Inhabitant__c = testAcc1.Id;
            lstLogement.add(TestFactory.createLogement(testAcc1.Id, srvTerr1.Id));
            lstLogement[1].Postal_Code__c = '121345';
            lstLogement[1].City__c = 'lgmtCity 2';
            lstLogement[1].Account__c = testAcc1.Id;
            insert lstLogement;

            //creating products
            lstProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstProd[0].recordtypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Product').getRecordTypeId();
            lstProd[0].Brand__c = 'INCONNU';
            lstProd[0].Equipment_family__c = 'Produit VMC';
            lstProd[0].Equipment_type1__c = 'Bouche VMC';
            //lstProd.add(TestFactory.createProduct('Entretien gaz'));
            //lstProd[1].recordtypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Product').getRecordTypeId();
            //lstProd[1].Brand__c = 'INCONNU';
            // lstProd[1].Equipment_type1__c = 'Bouche VMC';
            // lstProd[1].Equipment_type1__c = 'Pompe à chaleur hybride Gaz';
            insert lstProd;

            aInZone = TestFactory.createAgencyInterventionZone('59000', srvTerr.Id);
            insert aInZone;
        }
    }
    
    @isTest
    static void testResearchClientPerso(){
        System.runAs(adminUser){
            String strData = '{"clSoc":"false","clPays":"France","clPaysLog":"France","clSocPays":"France","clName":"test","clFirstName":"acc","clNum":"'
                                +lstAccIns[0].ClientNumber__c+'","clEmail":"test@mail.com","clAdresseLogStreet":"test rue","clAdresseLogPostalCode":"123456"'
                                +',"clAdresseLogCity":"testCity", "clImm_Res":"Résidence TEST", "clStair":"testStair", "clFloor":"testFloor", "clDoor":"testDoor"'
                                +', "clNumeroESI":"testNumESI", "clPhone":"1234567890","clSocName":""}';
            Test.startTest();
            LC02_EligibilityWizard.search(strData);
            Test.stopTest();
        }
    }

    @isTest
    static void testResearchClientSoc(){
        System.runAs(adminUser){
            String strData = '{"clSoc":true,"clPays":"France","clPaysLog":"France","clSocPays":"France","clName":"test","clFirstName":"acc","clNum":"'
            +lstAccIns[0].ClientNumber__c+'","clEmail":"test@mail.com","clAdresseLog":"12345","clAdresseLogStreet":"test rue","clAdresseLogPostalCode":"123456"'
            +',"clAdresseLogCity":"testCity", "clImm_Res":"Résidence TEST", "clStair":"testStair", "clFloor":"testFloor", "clDoor":"testDoor"'
            +', "clNumeroESI":"testNumESI","clPhone":"1234567890","clSocName":"testSocc"}';
            Test.startTest();
            LC02_EligibilityWizard.search(strData);
            Test.stopTest();
        }
    }

    @isTest
    static void testAnalyseGeo(){
        System.runAs(adminUser){
            String strData = '{"clSoc":false,"clPays":"France","clPaysLog":"France","clSocPays":"France","clCPLog":"59000","clMotif":""}';
            Test.startTest();
            LC02_EligibilityWizard.analyseGeo(strData);
            Test.stopTest();
        }
    }
    @isTest
    static void testAnalyseGeo1(){
        System.runAs(adminUser1){
            String strData = '{"clSoc":false,"clPays":"France","clPaysLog":"France","clSocPays":"France","clCPLog":"59000","clMotif":""}';
            Test.startTest();
            LC02_EligibilityWizard.analyseGeo(strData);
            Test.stopTest();
        }
    }
    @isTest
    static void testAnalyseGeo_AgencyIntZone54000(){

        list<Agency_intervention_zones__c> lstAIZ = new list <Agency_intervention_zones__c>{TestFactory.createAgencyInterventionZone('54000', srvTerr.Id), TestFactory.createAgencyInterventionZone('54000', srvTerr.Id)};
        insert lstAIZ;
        
        System.runAs(adminUser){
            String strData = '{"clSoc":false,"clPays":"France","clPaysLog":"France","clSocPays":"France","clCPLog":"54000","clMotif":""}';
            Test.startTest();
            LC02_EligibilityWizard.analyseGeo(strData);
            Test.stopTest();
        }
    }

    @isTest
    static void testAnalysePresta(){
        System.runAs(adminUser){
            String strData = '{"clSoc":false,"clPays":"France","clPaysLog":"France","clSocPays":"France","clCPLog":"59000","clEqpType":"Chaudière fioul","clPresta":"First Maintenance Visit (new contract)","clMotif":"First maintenance"}';
            Test.startTest();
            LC02_EligibilityWizard.analysePresta(strData);
            Test.stopTest();
        }
    }

    @isTest
    static void testCreationDemandePerso(){
        System.runAs(adminUser){
            String strData = '{"clPays":"France","clPaysLog":"France","clUsagePro":"Oui (plus de 50% d\'usage professionnel)","clAgeLogement":"Moins_2ans","clEqpType":"Chaudière gaz","clPresta":"Administrative management","clMotif":"Update customer data","clPriorite":"High","clName":"test wizard","clFirstName":"RRJ 8","clEmail":"test@example.com","clPhone":"1234567890","clMobile":"1234567890","clRueLog":"13, Rue labamem","clCPLog":"59000","clVilleLog":"Paris","clCP":"19487","clRue":"13, Rue labamem","clVille":"Paris","clComments":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur","clProprietaire":true,"clOccupant":true,"clEquipementHPE":true,"clAstEqpType":"Chaudière","clReqAgenceLocalId":"'+srvTerr.Id+'","clLogAgenceLocalId":"'+srvTerr.Id+'"}';
            Test.startTest();
            try{
                LC02_EligibilityWizard.creerDemandePerso(strData);
            }catch(Exception e){
                System.debug('#### Error: '+e);
            }
            Test.stopTest();
        }
    }

    @isTest
    static void testCreationDemandePersoNegative(){
        System.runAs(adminUser){
            String strData = '{"clPays":"France","clPaysLog":"France","clUsagePro":"Oui (plus de 50% d\'usage professionnel)","clAgeLogement":"Moins_2ans","clEqpType":"Chaudière gaz","clPresta":"Administrative management","clMotif":"Update customer data","clPriorite":"High","clName":"test wizard","clFirstName":"RRJ 8","clEmail":"test@example.com","clPhone":"1234567890","clMobile":"1234567890","clRueLog":"13, Rue labamem","clCPLog":"59000","clVilleLog":"Paris","clCP":"19487","clRue":"13, Rue labamem","clVille":"Paris","clComments":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur","clProprietaire":true,"clOccupant":true,"clEquipementHPE":true,"clAstEqpType":"Chaudière","clReqAgenceLocalId":"'+srvTerr.Id+'","clLogAgenceLocalId":"'+srvTerr.Id+'"}';
            delete lstProd;
            Test.startTest();
            try{
                LC02_EligibilityWizard.creerDemandePerso(strData);
            }catch(Exception e){
                System.debug('#### Error: '+e);
            }
            Test.stopTest();
        }
    }

    @isTest
    static void testCreationDemandeSoc(){
        System.runAs(adminUser){
            String strData = '{"clPays":"France","clPaysLog":"France","clUsagePro":"Oui (plus de 50% d\'usage professionnel)","clAgeLogement":"Moins_2ans","clEqpType":"Chaudière gaz","clPresta":"Administrative management","clMotif":"Update customer data","clPriorite":"High","clName":"test wizard","clFirstName":"RRJ 8","clEmail":"test@example.com","clPhone":"1234567890","clMobile":"1234567890","clRueLog":"13, Rue labamem","clCPLog":"59000","clVilleLog":"Paris","clCP":"19487","clRue":"13, Rue labamem","clVille":"Paris","clComments":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur","clProprietaire":true,"clOccupant":true,"clEquipementHPE":true,"clAstEqpType":"Pompe à chaleur","clReqAgenceLocalId":"'+srvTerr.Id+'","clLogAgenceLocalId":"'+srvTerr.Id+'","clSoc":true,"clSocCP":"59000","clSocRue":"13, Rue labamem","clSocVille":"Paris","clSocPays":"France","clSocName":"RRJ Test COMP","clSocType":"test"}';
            Test.startTest();
            try{
                LC02_EligibilityWizard.creerDemandeSociete(strData);
            }catch(Exception e){
                System.debug('#### Error: '+e);
            }
            Test.stopTest();
        }
    }

    @isTest
    static void testCreationDemandeSocNegative(){
        System.runAs(adminUser){
            String strData = '{"clPays":"France","clPaysLog":"France","clUsagePro":"Oui (plus de 50% d\'usage professionnel)","clAgeLogement":"Moins_2ans","clEqpType":"Chaudière gaz","clPresta":"Administrative management","clMotif":"Update customer data","clPriorite":"High","clName":"test wizard","clFirstName":"RRJ 8","clEmail":"test@example.com","clPhone":"1234567890","clMobile":"1234567890","clRueLog":"13, Rue labamem","clCPLog":"59000","clVilleLog":"Paris","clCP":"19487","clRue":"13, Rue labamem","clVille":"Paris","clComments":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur","clProprietaire":true,"clOccupant":true,"clEquipementHPE":true,"clAstEqpType":"Pompe à chaleur","clReqAgenceLocalId":"'+srvTerr.Id+'","clLogAgenceLocalId":"'+srvTerr.Id+'","clSoc":true,"clSocCP":"59000","clSocRue":"13, Rue labamem","clSocVille":"Paris","clSocPays":"France","clSocName":"RRJ Test COMP","clSocType":"test"}';
            delete lstProd;
            Test.startTest();
            try{
                LC02_EligibilityWizard.creerDemandeSociete(strData);
            }catch(Exception e){
                System.debug('#### Error: '+e);
            }
            Test.stopTest();
        }
    }

    @isTest
    static void testGetDepPickList(){
        System.runAs(adminUser){
            Test.startTest();
            LC02_EligibilityWizard.getDepPickVals('Case', 'Type', 'Reason__c');
            Test.stopTest();
        }
    }

    @isTest
    static void testGetPickLists(){
        String strPickVals = '[{"label":"Logement__c","value":"Professional_Use__c"}]';
        System.runAs(adminUser){
            Test.startTest();
            LC02_EligibilityWizard.getPicklists(strPickVals);
            Test.stopTest();
        }
    }

    @isTest
    static void testGetLogmentAssets(){
         String strData = '{"clPays":"France","clPaysLog":"France","clUsagePro":"Oui (plus de 50% d\'usage professionnel)","clAgeLogement":"Moins_2ans","clEqpType":"Chaudière gaz","clPresta":"Administrative management","clMotif":"Update customer data","clPriorite":"High","clName":"test wizard","clFirstName":"RRJ 8","clEmail":"test@example.com","clPhone":"1234567890","clMobile":"1234567890","clRueLog":"13, Rue labamem","clCPLog":"59000","clVilleLog":"Paris","clCP":"19487","clRue":"13, Rue labamem","clVille":"Paris","clComments":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur","clProprietaire":true,"clLogId":"'+ lstLogement[0].Id +'","clOccupant":true,"clEquipementHPE":true,"clAstEqpType":"Chaudière","clReqAgenceLocalId":"'+srvTerr.Id+'","clLogAgenceLocalId":"'+srvTerr.Id+'"}';
            
        System.runAs(adminUser){
            Test.startTest();
            LC02_EligibilityWizard.getLogmentAssets(strData);
            Test.stopTest();
        }
    }

    @isTest
    static void testGetResults(){
         String strData = 'Name Test';

        System.runAs(adminUser){
            Test.startTest();
            LC02_EligibilityWizard.getResults(strData);
            Test.stopTest();
        }
    }
}