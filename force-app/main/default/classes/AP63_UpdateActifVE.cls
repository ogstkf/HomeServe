/**
 * @File Name          : AP63_UpdateActifVE.cls
 * @Description        : 
 * @Author             : LGO
 * @Group              : 
 * @Last Modified By   : LGO
 * @Last Modified On   : 16/12/2019, 09:19:45
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/12/2019         LGO                    Initial Version
**/
public with sharing class AP63_UpdateActifVE {

    public static void updateActifVEFromCLI(List<Product2> lstProd){

        List<ContractLineItem> lstUpdateCLI = new List<ContractLineItem>();
        
        for(ContractLineItem cli : [SELECT Id,IsBundle__c, VE__c, IsActive__c FROM ContractLineItem WHERE PricebookEntry.Product2Id IN :lstProd]){
            lstUpdateCLI.add(new ContractLineItem(Id = cli.Id,VE__c=true, IsActive__c=true ));       
   		}

        System.debug('ContractLineItem items to update: ' + lstUpdateCLI);
        if(!lstUpdateCLI.isEmpty()){
            Update lstUpdateCLI;
            system.debug('** in after upd');  
        }
        
    }    
}