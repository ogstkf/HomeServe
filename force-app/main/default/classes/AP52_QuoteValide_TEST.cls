/**
 * @File Name          : AP52_QuoteValide_TEST.cls
 * @Description        : 
 * @Author             : SHU
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         09-12-2019     		     SHU        Initial Version
**/
@ isTest
public with sharing class AP52_QuoteValide_TEST {
    static User mainUser;
    static List<Account> lstAccount = new List<Account>();
    static sofactoapp__Compte_auxiliaire__c CA = new sofactoapp__Compte_auxiliaire__c (); // Sub-ledger Account
    static sofactoapp__Coordonnees_bancaires__c CB = new sofactoapp__Coordonnees_bancaires__c(); // Bank Details
    static sofactoapp__Compte_comptable__c CC = new sofactoapp__Compte_comptable__c (); // Accounting Account
    static List<sofactoapp__Plan_comptable__c> PC = new List<sofactoapp__Plan_comptable__c> (); // Chart of Accounts
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<WorkOrder> lstWorkOrder = new List<WorkOrder>();

    static Opportunity opp = new Opportunity();
    static List<Product2> lstProd = new List<Product2>();
    static List<Quote> lstQuote = new List<Quote>();
    static List<QuoteLineItem> lstQuoteLineItem = new List<QuoteLineItem>();
    static List<OpportunityLineItem> lstOppLineItem = new List<OpportunityLineItem>();
    static List<PricebookEntry> lstPriceBookEntry =  new List<PricebookEntry>();

    static{
        mainUser = TestFactory.createAdminUser('UserAP48@example.com', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            // Master detail relationship
            PC = new List<sofactoapp__Plan_comptable__c> {
                new sofactoapp__Plan_comptable__c(),
                new sofactoapp__Plan_comptable__c(),
                new sofactoapp__Plan_comptable__c()
            };  
            insert PC; 

            // create sofactoapp__Compte_comptable__c (Accounting Account)
            CC.Name = 'test CC';
            CC.sofactoapp__Libelle__c = 'test libellé';
            CC.sofactoapp__Plan_comptable__c = PC[0].Id;
            insert CC; 

            // create sofactoapp__Compte_auxiliaire__c (Sub-ledger Account)
            CA.Name = 'test account aux';
            CA.sofactoapp__Compte_comptable__c = CC.Id;
            CA.Compte_comptable_Prelevement__c = CC.Id;
            insert CA; 

            //Create account
            lstAccount = new List<Account> {
                 new Account( Name ='Apple Inc', sofactoapp__Compte_auxiliaire__c = CA.Id),
                new Account( Name ='Google Inc',Type = 'Entreprise', sofactoapp__Compte_auxiliaire__c = CA.Id),
                new Account( Name ='Microsoft Inc',Type = 'Autre', sofactoapp__Compte_auxiliaire__c = CA.Id),
                new Account( Name ='Microsoft2 Inc',Type = 'Autre', sofactoapp__Compte_auxiliaire__c = CA.Id)
            };     
            insert lstAccount; 

            //Create PriceBook
            Id pricebookId = Test.getStandardPricebookId();            

            //create opportunity
        
            opp = TestFactory.createOpportunity('Prestation panneaux',lstAccount[0].Id);
            opp.Pricebook2Id = pricebookId;
            insert opp; 

            //Creating products
            lstProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstProd.add(TestFactory.createProduct('Entretien gaz'));
            lstProd[0].Equipment_family__c = 'Chaudière';
            lstProd[0].Equipment_type1__c = 'Chaudière fioul';
            lstProd[1].Equipment_family__c = 'Chaudière';
            lstProd[1].Equipment_type1__c = 'Chaudière fioul';
            insert lstProd;


            //create PricebookEntry
            lstPriceBookEntry =  new List<PricebookEntry>{
                new PricebookEntry( Pricebook2Id = pricebookId,
                                    Product2Id = lstProd[0].Id,
                                    UnitPrice = 150.00,
                                    IsActive = true
                                  ),
                new PricebookEntry( Pricebook2Id = pricebookId,
                                    Product2Id = lstProd[1].Id,
                                    UnitPrice = 50.00,
                                    IsActive = true
                                  )
            };
            insert lstPriceBookEntry;     
            
             //Create WorkOrder
             lstWorkOrder = new List<WorkOrder>{
                new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal)
            };

            insert lstWorkOrder;

            //Create Quotes
            lstQuote = new List<Quote> {
                new Quote(  Name ='Prestations'
                            ,OpportunityId = opp.Id
                            ,Pricebook2Id = pricebookId
                            ,Devis_signe_par_le_client__c = false
                            ,Status = 'In Review'
                            ,Date_de_debut_des_travaux__c = System.today()
                            ,tech_deja_facture__c = true
                            ,Mode_de_paiement_de_l_acompte__c = 'CB'
                            ,Montant_de_l_acompte_verse__c=500
                            ),
               new Quote(  Name ='Travaux'
                            ,OpportunityId = opp.Id
                            ,Pricebook2Id = pricebookId
                            ,Devis_signe_par_le_client__c = false
                            ,Status = 'Needs Review'
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                            ), 
                new Quote(  Name ='Remplacement'
                            ,OpportunityId = opp.Id
                            ,Devis_signe_par_le_client__c = false
                            ,Pricebook2Id = pricebookId
                            ,Status = 'Draft'
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                            ),
                new Quote(  Name ='Sous-traitance totale'
                            ,OpportunityId = opp.Id 
                            ,Devis_signe_par_le_client__c = false
                            ,Pricebook2Id = pricebookId
                            ,Status = 'Denied'
                            //,Type_de_devis__c = 'Vente de pièces/équipements au guichet'    
                            ,Date_de_debut_des_travaux__c = System.today()
                            , tech_deja_facture__c = true
                            , Mode_de_paiement_de_l_acompte__c = 'CB'
                            , Montant_de_l_acompte_verse__c=500
                         )
            };
            insert lstQuote;    

         /*   lstQuote =  new List<Quote>{
                new Quote(Name ='Test1',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = opp.Id, 
                    Status = 'Denied', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'Prélèvement', Montant_de_l_acompte_verse__c=200) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = opp.Id, 
                    Status = 'Needs Review', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'Prélèvement', Montant_de_l_acompte_verse__c=200) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = opp.Id, 
                    Status = 'In Review', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'Prélèvement', Montant_de_l_acompte_verse__c=200) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = opp.Id, 
                    Status = 'Draft', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'Prélèvement', Montant_de_l_acompte_verse__c=200) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = opp.Id, 
                    Status = 'Denied', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'Prélèvement', Montant_de_l_acompte_verse__c=200) ,
                new Quote(Name ='Test2',  
                    Devis_signe_par_le_client__c = false, 
                    OpportunityId = opp.Id, 
                    Status = 'Denied', tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'Prélèvement', Montant_de_l_acompte_verse__c=200)
               
                };
                insert lstQuote;*/

                lstQuote[0].Montant_de_l_acompte_verse__c = 300;
                lstQuote[1].Montant_de_l_acompte_verse__c = 300;
                lstQuote[2].Montant_de_l_acompte_verse__c= 300;
                lstQuote[3].Montant_de_l_acompte_verse__c= 300;
              //  lstQuote[4].Montant_de_l_acompte_verse__c= 300;
               // lstQuote[5].Montant_de_l_acompte_verse__c= 300;
               // lstQuote[6].Montant_de_l_acompte_verse__c= 300;
                //lstQuote[7].Montant_de_l_acompte_verse__c= 300;
                update lstQuote;


            //create QuoteLineItem
            lstQuoteLineItem =  new List<QuoteLineItem>{
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstProd[0].Id,
                                    QuoteId = lstQuote[0].Id, 
                                    PricebookEntryId = lstPriceBookEntry[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                 ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstProd[0].Id,
                                    QuoteId = lstQuote[1].Id, 
                                    PricebookEntryId = lstPriceBookEntry[0].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                 ),
                new QuoteLineItem( Quantity = 1,
                                    Product2Id = lstProd[1].Id,
                                    QuoteId = lstQuote[2].Id, 
                                    PricebookEntryId = lstPriceBookEntry[1].Id,
                                    UnitPrice = 10,
                                    Remise_en100__c = 1,
                                    ServiceDate = System.today()
                                 )
                };
            insert lstQuoteLineItem;

            //create OpportunityLineItem
            lstOppLineItem =  new List<OpportunityLineItem>{
                new OpportunityLineItem(OpportunityId = opp.Id,
                                        //QuoteId = lstQuote[0].Id,
                                        Quantity = 1,
                                        PricebookEntryId = lstPriceBookEntry[1].Id,
                                        TotalPrice = lstPriceBookEntry[1].UnitPrice,
                                        TECH_CorrespondingQLItemId__c =lstQuoteLineItem[2].Id
                                        )
            };
            insert lstOppLineItem;                         

            opp.Tech_Synced_Devis__c = lstQuote[0].Id;
            update opp;
          
        }
    }
    
    //@isTest
    @isTest //to be removed and create the custom settings in test data
    public static void testSyncOppLineItems(){
        System.runAs(mainUser){
            lstQuote[1].Status = AP_Constant.quoteStatusValideSigne;
            //lstQuote[1].Status = 'Validé,signé mais abandonné';
            //lstQuote[1].Raisons_de_l_abandon_du_devis__c = 'Important';
            lstQuote[1].Devis_signe_par_le_client__c = true;
            lstQuote[1].Date_de_debut_des_travaux__c = System.today();

            Test.startTest();
            update lstQuote[1];
            Test.stopTest();

            system.assertEquals([select id from opportunitylineitem where OpportunityId = :opp.Id ].size(),
                                 4, 
                                 'Number of OPL is 4'
                                 );
            //system.assertEquals(Expected, Actual, Msg);
        }
    }    
}