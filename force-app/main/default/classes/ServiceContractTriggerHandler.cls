/**
 * @File Name          : ServiceContractTriggerHandler.cls
 * @Description        : 
 * @Author             : ZJO
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 11-03-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    14/11/2019   AMO     Initial Version
 * 1.1    14/11/2019   DMU     Added logic for CT-1177
 * 1.2    16/12/2019   LGO    Added logic for 
 * 1.3    25/01/2020   DMU    Added logic for AP62 (Planification VE)
 * 1.4    21/07/2020   DMU    Added method handleBeforeInsert
**/
public with sharing class ServiceContractTriggerHandler {

    Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    public static Boolean firstRun = true;
    public static Boolean runByWebMobile = false;

    public void handleBeforeInsert(List<ServiceContract> lstNewServ){
        
        List<ServiceContract> lstSCSetCampagne = new List<ServiceContract>(); //TEC-64 variable
        Date today = Date.newInstance(system.today().year(), system.today().month(), system.today().day());//TEC-64 variable

        for(Integer i=0; i<lstNewServ.size() ; i++){
            
            //TEC-64            
            system.debug('*** lstNewServ[i].TECH_AccountCampagne__c: '+lstNewServ[i].TECH_AccountCampagne__c);
            if(lstNewServ[i].AccountId <> null && lstNewServ[i].TECH_AccountCampagne__c <> null &&  lstNewServ[i].TECH_AccountDateFinRetombee__c > today){
                lstSCSetCampagne.add(lstNewServ[i]);
            }
        }        

        //set campagne on SC
		if(lstSCSetCampagne.size()>0){
			AP72_CodeCampagne.setCampagne(lstSCSetCampagne);
		}
    }
    
    //TEC 125 - Create Maintenance Plan
    public void handleAfterInsert(List<ServiceContract> lstNewServ){
        system.debug('***in handleAfterInsert: ');


        List<ServiceContract> lstServConIndividual = new List<ServiceContract>();//Tec-125
        List<ServiceContract> lstServConCollectifs = new List<ServiceContract>();//Tec-140
        

        for(Integer i=0; i<lstNewServ.size(); i++){
            
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP74')){
                //Tec 125 - Contrats Individuels
                if ((lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels') && lstNewServ[i].Contract_Renewed__c == false && (lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratActive || lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutEnAttente)) 
                || (lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels') && lstNewServ[i].Contract_Renewed__c == true && (lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratActive || lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratPendingPayment || lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratAttenteRen))){          
                    lstServConIndividual.add(lstNewServ[i]);
                }
            }
            
        }

        if (lstServConIndividual.size() > 0){
            AP74_MaintenancePlan.createMaintenancePlanIndividual(lstServConIndividual);
        }

    }

    public void handlebeforeupdate(List<ServiceContract> lstOldServ, List<ServiceContract> lstNewServ){
     
        List<ServiceContract> lstServContratSigne = new List<ServiceContract>(); //AP43
        
        for(Integer i=0; i<lstNewServ.size() ; i++){
            
            //CT-1177
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP43') && firstRun){
                if(lstNewServ[i].Contrat_signe_par_le_client__c != lstOldServ[i].Contrat_signe_par_le_client__c && lstNewServ[i].Contrat_signe_par_le_client__c && lstNewServ[i].Contract_Renewed__c == false && lstNewServ[i].Type__c == AP_Constant.serviceContractTypeInd){
                    lstServContratSigne.add(lstNewServ[i]);
                }
            }
        }        
        if(lstServContratSigne.size() > 0  && firstRun){
             System.debug('lstServContratSigne : ' + lstServContratSigne.size());
            //AP43_UpdateServiceContractStatus.UpdateStatus(lstServContratSigne);
        }     
  
    }

    public void handleafterupdate(List<ServiceContract> lstOldServ, List<ServiceContract> lstNewServ){
        Set<Id> payeurAccordClient = new Set<Id>();
        List<ServiceContract> lstServCon = new List<ServiceContract>(); //AP44
        List<ServiceContract> lstServConUpd = new List<ServiceContract>(); //AP53
        List<ServiceContract> lstServConUpd1 = new List<ServiceContract>(); //AP53
        List<ServiceContract> lstServConActfi = new List<ServiceContract>(); //AP44 2
        List<ServiceContract> lstServConUpdMonths = new List<ServiceContract>(); //AP62

        List<ServiceContract> lstServConIndividual = new List<ServiceContract>(); //TEC-125
        List<ServiceContract> lstScIndUpdDate = new List<ServiceContract>(); // TEC-125
        List<ServiceContract> lstCancelledScInd = new List<ServiceContract>();//TEC-125

        List<ServiceContract> lstServConCollectifs = new List<ServiceContract>(); //TEC-140
        List<ServiceContract> lstScColUpdDate = new List<ServiceContract>(); // TEC-140
        List<ServiceContract> lstCancelledScCol = new List<ServiceContract>();//TEC-140

        List<ServiceContract> lstSCStatutVE = new List<ServiceContract>();//TEC-229

        List<ServiceContract> lstScToCreateOrderSlimPay = new List<ServiceContract>(); // TEC-952 TEC-980
        Set<Id> setCBToCreateMandateSlimPay = new Set<Id>(); // TEC-952 TEC-953

        Set<Id> setCBToRevokeMandateSlimPay = new Set<Id>(); // TEC-1042
        
        for(Integer i=0; i<lstNewServ.size() ; i++){
            if((lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratExpired || lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratResilie) && (lstNewServ[i].Contract_Status__c != lstOldServ[i].Contract_Status__c)){
                lstServCon.add(lstNewServ[i]);
            }

            if((lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratActive || lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutEnAttentePremiereVisitValid 
                || lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutActifenretarddepaiement) && (lstNewServ[i].Contract_Status__c != lstOldServ[i].Contract_Status__c)){
                lstServConActfi.add(lstNewServ[i]);
            }

            //DMU - 20200817 - Remove condition on && lstNewServ[i].Contrat_Cham_Digital__c == false - TEC-123
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP53')){
                if((lstOldServ[i].Type__c <> lstNewServ[i].Type__c
                    || lstOldServ[i].Contract_Renewed__c <> lstNewServ[i].Contract_Renewed__c
                    || (lstOldServ[i].Contrat_signe_par_le_client__c <> lstNewServ[i].Contrat_signe_par_le_client__c 
                        && lstNewServ[i].Contract_Renewed__c == false))
                    && lstNewServ[i].Type__c == AP_Constant.serviceContractTypeInd 
                    && lstOldServ[i].Contract_Status__c != 'Cancelled' 
                    && lstOldServ[i].Contract_Status__c != 'Résilié' 
                    && lstOldServ[i].Contract_Status__c != 'Expired'){
                    lstServConUpd.add(lstNewServ[i]);            
                }

                if(lstOldServ[i].Contrat_signe_par_le_client__c <> lstNewServ[i].Contrat_signe_par_le_client__c && lstNewServ[i].Contrat_signe_par_le_client__c == True){
                    lstSCStatutVE.add(lstNewServ[i]);
                }
            }
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP62')){
                if(lstOldServ[i].Months__c <> lstNewServ[i].Months__c){
                    lstServConUpdMonths.add(lstNewServ[i]);
                }
            }

            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP74')){
                //TEC-125 - Maintenance Plan for ServiceContracts Individuel
                /*if(lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels') && lstNewServ[i].Contract_Renewed__c == false && lstOldServ[i].Contract_Status__c <> lstNewServ[i].Contract_Status__c && (lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratActive || lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutEnAttente)){
                    lstServConIndividual.add(lstNewServ[i]);
                }

                //Tec-140 - Maintenance Plan for ServiceContracts Collectifs
                if((lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_collectifs_publics') || lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives')) && lstNewServ[i].Contract_Renewed__c == false && lstOldServ[i].Contract_Status__c <> lstNewServ[i].Contract_Status__c && lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratActive){
                    lstServConCollectifs.add(lstNewServ[i]);
                }
    
                //TEC-125 - Update StartDate and EndDate of Maintenance Plan for Contrats Individuels
                if(lstOldServ[i].StartDate <> lstNewServ[i].StartDate && lstOldServ[i].EndDate <> lstNewServ[i].EndDate && lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels')){
                    lstScIndUpdDate.add(lstNewServ[i]);
                }

                //TEC-140 - Update StartDate and EndDate of Maintenance Plan for Contrats Collectifs
                if(lstOldServ[i].StartDate <> lstNewServ[i].StartDate && lstOldServ[i].EndDate <> lstNewServ[i].EndDate && (lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_collectifs_publics') || lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives'))){
                    lstScColUpdDate.add(lstNewServ[i]);
                }

                //TEC-125 - Update Status on SA & Case (Individuels)
                if(lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels') && lstOldServ[i].Contract_Status__c <> lstNewServ[i].Contract_Status__c && lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutCancelled){
                    lstCancelledScInd.add(lstNewServ[i]);
                }

                //TEC-140 - Update Status on SA & Case (Collectifs)
                if((lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_collectifs_publics') || lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives')) && lstOldServ[i].Contract_Status__c <> lstNewServ[i].Contract_Status__c && lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutCancelled){
                    lstCancelledScCol.add(lstNewServ[i]);
                }*/

                if(lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Individuels')){
                    if (lstNewServ[i].Contract_Renewed__c == false && lstOldServ[i].Contract_Status__c <> lstNewServ[i].Contract_Status__c && (lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratActive || lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutEnAttente)) {
                        lstServConIndividual.add(lstNewServ[i]);
                    }
                    if (lstOldServ[i].StartDate <> lstNewServ[i].StartDate && lstOldServ[i].EndDate <> lstNewServ[i].EndDate) {
                        lstScIndUpdDate.add(lstNewServ[i]);
                    }
                    if (lstOldServ[i].Contract_Status__c <> lstNewServ[i].Contract_Status__c && lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutCancelled) {
                        lstCancelledScInd.add(lstNewServ[i]);
                    }
                }
                else if (lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_collectifs_publics') || lstNewServ[i].RecordTypeId == AP_Constant.getRecTypeId('ServiceContract', 'Contrats_Collectifs_Prives')) {
                    if (lstNewServ[i].Contract_Renewed__c == false && lstOldServ[i].Contract_Status__c <> lstNewServ[i].Contract_Status__c && lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratActive) {
                        lstServConCollectifs.add(lstNewServ[i]);
                    }
                    if (lstOldServ[i].StartDate <> lstNewServ[i].StartDate && lstOldServ[i].EndDate <> lstNewServ[i].EndDate) {
                        lstScColUpdDate.add(lstNewServ[i]);
                    }
                    if (lstOldServ[i].Contract_Status__c <> lstNewServ[i].Contract_Status__c && lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutCancelled) {
                        lstCancelledScCol.add(lstNewServ[i]);
                    }
                }
                
            }

            
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP79')) {
                //TEC-825
                if (lstOldServ[i].Contrat_signe_par_le_client__c != lstNewServ[i].Contrat_signe_par_le_client__c && lstNewServ[i].Contrat_signe_par_le_client__c && !lstNewServ[i].Contract_Renewed__c) {
                    payeurAccordClient.add(lstNewServ[i].Payeur_du_contrat__c);
                }
            }
            if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP81')) {
                if (lstNewServ[i].sofactoapp_Rib_prelevement__c != lstOldServ[i].sofactoapp_Rib_prelevement__c && lstNewServ[i].sofactoapp_Rib_prelevement__c != null && !runByWebMobile) {
                    if (lstNewServ[i].Mandate_signed__c && lstNewServ[i].Mandate_signed__c != lstOldServ[i].Mandate_signed__c) {
                        setCBToCreateMandateSlimPay.add(lstNewServ[i].sofactoapp_Rib_prelevement__c);
                    }
                    else {
                        lstScToCreateOrderSlimPay.add(lstNewServ[i]);
                    }
                }

            }

            if (userBypass.BypassTrigger__c == null) {
                if (lstNewServ[i].Contract_Status__c != lstOldServ[i].Contract_Status__c && lstNewServ[i].Contract_Status__c == AP_Constant.serviceContractStatutContratResilie) {
                    setCBToRevokeMandateSlimPay.add(lstNewServ[i].sofactoapp_Rib_prelevement__c);
                }
            }
        }
        /*System.debug('List of service contract: ' + lstServConIndividual);
        System.debug('List of service contract LGO: ' + lstServConUpd.size());*/

        if(lstSCStatutVE.size()>0){
            AP53_UpdateServiceContratStatus.updateStatusVE(lstSCStatutVE);
        }

        if(lstServConUpd.size() > 0 && firstRun){
            AP53_UpdateServiceContratStatus.updateStatusFromContract(lstServConUpd);
            // firstRun = false;
        }

        if(lstServCon.size() > 0){
            AP44_UpdateCLIStatus.UpdateStatus(lstServCon);
        }

        if(lstServConActfi.size() > 0){
            AP44_UpdateCLIStatus.UpdateStatusActive(lstServConActfi);
        }

        if(lstServConIndividual.size() > 0){
            AP74_MaintenancePlan.createMaintenancePlanIndividual(lstServConIndividual);
        }

        if(lstServConCollectifs.size() > 0){
            AP74_MaintenancePlan.createMaintenancePlanCollective(lstServConCollectifs);
        }

        if(lstScIndUpdDate.size() > 0){
            AP74_MaintenancePlan.updateStartEndDate(lstScIndUpdDate);         
        }

        if(lstScColUpdDate.size() > 0){
            AP74_MaintenancePlan.updateStartEndDate(lstScColUpdDate);
        }

        if(lstCancelledScInd.size() > 0){
            AP74_MaintenancePlan.updateCaseSA(lstCancelledScInd);
        }

        if(lstCancelledScCol.size() > 0){
            AP74_MaintenancePlan.updateCaseSA(lstCancelledScCol);
        }

        
        if (payeurAccordClient.size() > 0) {
            AP79_AccordClient.updtScAcc(payeurAccordClient);
        }

        if (lstScToCreateOrderSlimPay.size() > 0) {
            AP81_SlimPayCallOut.createOrders(lstScToCreateOrderSlimPay);
        }

        if (setCBToCreateMandateSlimPay.size() > 0 ) {
            AP81_SlimPayCallOut.createMandates(setCBToCreateMandateSlimPay);
        }

        //DMU - 4/6/20 - commented below code due to HomeServe Project
        // if(lstServConUpdMonths.size() > 0){
        //     // AP62_UpdateCLIDesiredDate.setDesiredDate(lstServConUpd); 
        // }
    
    }
    
}