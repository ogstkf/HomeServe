/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 07-03-2022
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-03-2022   MNA   Initial Version
**/
public without sharing class WebServiceLog {
    public static WSTransactionLog__c generateLog (DateTime dt, String endpoint, String statusCode, String request, String response) {
        return new WSTransactionLog__c(
            DateTime__c = dt,
            Endpoint__c = endpoint,
            StatusCode__c = statusCode,
            Request__c = request,
            Response__c = response
        );
    }
}