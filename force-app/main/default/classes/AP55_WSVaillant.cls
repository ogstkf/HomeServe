/**
* @File Name          : AP55_WSVaillant.cls
* @Description        : 
* @Author             : SHU   
* @Group              : 
* @Last Modified By   : RRJ
* @Last Modified On   : 03/02/2020, 13:09:22
* @Modification Log   : 
* Ver       Date            Author                 Modification
* ---    -----------       -------           ------------------------ 
* 1.0    18/12/2019         SHU              Initial Version  (CT-1231) 
* 1.0    23/12/2019         KJB              Initial Version  (CT-1231)
* 1.1    17/01/2020         CRA              Add logic to get the Mapping in the Custom Setting MappingAgenceFourn__c
* ---    -----------       -------           ------------------------ 
**/
public with sharing class AP55_WSVaillant {

    public static String xmlHeader = '<?xml version="1.0"?>'
        + '<?xml-stylesheet type="text/xsl" href="VISUCOMMANDESAUNIER.xslt"?>';
    
    public static String STATUS_NO_MAPPING= 'Pas de mapping client';
    public static String STATUS_SENT_WITH_ERROR = 'Envoyée avec erreur';
    public static String STATUS_SENT_WITH_SUCCESS = 'Envoyée avec succès';
    public static String STATUS_COMMANDE_PAS_VALID = 'Commande invalide';
    public static String STATUS_SERVEUR_NON_CONFIGURE = 'Serveur n\'est pas defini';
    
    public static Boolean IS_TEST = Boolean.valueOf(System.Label.IsTest);
    
    public static String generateXML(Order order){
        
        String result = null;
        
        String mapping = null;
        
        //List<MappingAgenceFourn__c> lstMappingAgcFourn = MappingAgenceFourn__c.getall().values(); //CRA20200117 - Check if the Mapping exist in MappingAgenceFourn__c
        
        List<MappingAgenceFourn__c> lstMappingAgcFourn = [SELECT ID_Fournisseur__c, Code_Agence__c, Mapping__c FROM MappingAgenceFourn__c WHERE ID_Fournisseur__c=: order.AccountId AND Code_Agence__c =: order.Agence__r.Agency_Code__c];
        for (MappingAgenceFourn__c mapAgcFourn : lstMappingAgcFourn) {
           // if (order.AccountId == mapAgcFourn.ID_Fournisseur__c && order.Agence__r.Agency_Code__c == mapAgcFourn.Code_Agence__c) {
                mapping = mapAgcFourn.Mapping__c;
                break;
           // }
        }    
        if (String.isNotBlank(mapping)) {
            
            result = buildXML(order, mapping);
        }
        
        return result;
        
    }
    
    public static Order getOrder(String orderId){
        Order order = [SELECT  Id
                       , Name
                       , Account.Code_client_CHAM_chez_le_fournisseur__c
                       , Account.Type_EDI__c
                       , Type_de_livraison_de_la_commande__c
                       , OrderNumber
                       , Owner.Email
                       , AccountId
                       , Reponse_EDI__c
                       , Statut_EDI__c 
                       , Commande_EDI__c
                       , Mode_de_transmission__c
                       , Status
                       , Agence__r.Agency_Code__c
                       , (SELECT Id, PricebookEntry.reference_fournisseur__c
                          , Product2.Reference_article__c, Quantity 
                          , Product2.ProductCode
                          FROM OrderItems) 
                       FROM Order WHERE Id =:orderId];
        
        return order;
    }
    
    private static String pad(String str, Integer numchar, String fillChar){
        String result = str;
       
        
        if(str.length() < numchar){
            Integer missing = numchar - str.length();
            for(Integer i = 0; i < missing;i++){
                str = fillChar + str;
            }
        }
        
        return str;
        
    }
    
    public static String buildXML(Order order, String mapping){
        String deliveryMethod = '';
        String xmlOrderItemlist = '';
        String xmlOrderItem ='';
        
        deliveryMethod = (order.Type_de_livraison_de_la_commande__c == 'Express' ? 'RAPID' : '');
        deliveryMethod = (order.Type_de_livraison_de_la_commande__c == 'Standard' ? 'NORMAL' : '');
        System.debug('Before padding custom num:' + mapping);
        String custNum = pad(mapping,10, '0');
        System.debug('real custNum:' + custNum);
        //if(IS_TEST)
        	//custNum = '0016001018';
        String xmlOrderVaillant = xmlHeader + 
            //'<Orders CustomerNumber="'+ lstOrder[0].Account.Code_client_CHAM_chez_le_fournisseur__c +'">' CRA20200117
            '<Orders CustomerNumber="'+ custNum +'">'                        
            + '<Order>'
            + '<DeliveryMethod>'+ deliveryMethod +'</DeliveryMethod>'
            + '<CustomersOrderReference>'+ order.OrderNumber +'</CustomersOrderReference>'
            + '<ShipTo><ContactEmail>'+ order.Owner.Email +'</ContactEmail></ShipTo>'
            + '$OrderItemList'
            + '</Order></Orders>';
        
        for(OrderItem oi : order.OrderItems){
            String itemIdStr = oi.PricebookEntry.reference_fournisseur__c != null? oi.PricebookEntry.reference_fournisseur__c : oi.Product2.ProductCode;
            
            if(itemIdStr.startsWith('VA')){
                itemIdStr = itemIdStr.removeStart('VA');
                itemIdStr = pad(itemIdStr,10, '0');
            }
            system.debug('## oi : ' + oi);
            String quantity = oi.Quantity + '';
            if(quantity.indexOf('.')>=0)
                quantity = quantity.substring(0, quantity.indexOf('.'));
            xmlOrderItem  += '<OrderedItem>'
                + '<ItemID>'
                +  itemIdStr
                //+ '0020025929'
                + '</ItemID>'
                + '<Quantity>'+ quantity +'</Quantity>'
                + '</OrderedItem>';
            
            system.debug('## xmlOrderItem : ' + xmlOrderItem);
        }
        
        xmlOrderVaillant = xmlOrderVaillant.replaceAll('\\$OrderItemList', Matcher.quoteReplacement(xmlOrderItem));
        
        return xmlOrderVaillant;
    }
    
    private static Boolean isOrderValid(Order order){
        Boolean result = false;
        if(order.Status != null && (order.Status.equals('Accusé de réception fournisseurs') || 
                                    order.Status.equals('Demande_d\'achats_approuvée') ||
                                    order.Status.equals('Demande d\'achats'))){
                                        if(order.Mode_de_transmission__c != null && order.Mode_de_transmission__c.equals('EDI')){
                                            result = true;
                                        }
                                    }
        
        return result;
    }
    
    private static Boolean isVaillant(Order order){
        if(order.AccountId != null && order.Account.Type_EDI__c != null && order.Account.Type_EDI__c.equals('Vaillant')){
            return true;
        }else{
            return false;
        }
    }
    
    
    @AuraEnabled
    public static Map<String,String> instantiateMethod(String ordId){
        // String message;
        Order order = getOrder(ordId);
        Map<String,String> result = new Map<String,String>();
        if(order.AccountId == null || order.Account.Type_EDI__c == null){
            System.debug('Serveur nest pas configuree');
             logOrder(order,null,STATUS_SERVEUR_NON_CONFIGURE, null);
            result.put('state', 'error');
            result.put('msg', 'La commande ne peut pas être envoyée par EDI: Le Type EDI n\'est pas défini');
            return result;
        }
        Boolean orderValid = isOrderValid(order);
        if(orderValid){
            String xml = generateXML(order);
            
            if (xml != null) { 
                // message = saveFile(wsVaillant.lstOrder[0].Id, wsVaillant.xmlOrderVaillant);
                Boolean vaillant = isVaillant(order);
                String response = postFile(xml, vaillant);
                String orderNumber = extractOrderNumber(response);
                if(orderNumber != null && orderNumber.length() > 0){
                    logOrder(order,orderNumber,STATUS_SENT_WITH_SUCCESS, response);
                    result.put('state', 'success');
            		result.put('msg', 'La commande est envoyée par EDI. Numéro de commande:' + orderNumber);
            		return result;
                    
                }else{
                    logOrder(order,null,STATUS_SENT_WITH_ERROR, response);
                    try{
                        //essaye d'extraire le message d'erreur
                        String msg = extractResponseMessage(response);
                        result.put('state', 'error');
           				result.put('msg',  Label.LC55_Erreur_Envoi_EDI + ':' + msg);
            			return result;
                        
                    }catch(Exception e){
                        //do nothing here;
                    }
                    result.put('state', 'error');
                    result.put('msg',  Label.LC55_Erreur_Envoi_EDI);
                    return result;
                }
            }
            else {
                logOrder(order,null,STATUS_NO_MAPPING, null);
                result.put('state', 'error');
                result.put('msg',  Label.LC55_MessageMappingNotFound);
                return result;
               // return Label.LC55_MessageMappingNotFound ;
            }
        }else{
            System.debug('Order.Status=' + order.Status);
            System.debug('order.Mode_de_transmission__c=' + order.Mode_de_transmission__c);
            logOrder(order,null,STATUS_COMMANDE_PAS_VALID, null);
            result.put('state', 'error');
            result.put('msg',   Label.LC55_Erreur_Envoi_EDI);
            return result;
            
        }
        
        //return message;
    }
    
    
    public static void logOrder(Order order,String orderNumber, String status, String serviceResponse){
        System.debug('Status to save::::' + status);
        if(orderNumber != null){
            order.Status = 'Accusé de réception fournisseurs'; 
            order.Commande_EDI__c = orderNumber;
            
        }
        if(serviceResponse != null)
        	order.Reponse_EDI__c  = serviceResponse;
        
        order.Statut_EDI__c = status;
        
        update order;

        
        
    }
    
    
    public static String postFile(String xmlGenerated, Boolean isVaillant){
        HttpRequest req = new HttpRequest();
        String endpoint = 'https://edi.saunierduval.fr/Saunier.aspx';
        if(isVaillant){
            endpoint= 'https://edi.saunierduval.fr/Vaillant.aspx';
        }
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        
        System.debug('xmlGenerated:' + xmlGenerated);
        req.setHeader('Access-Control-Allow-Origin', '*');
        req.setHeader('Content-Type', 'xml/html');
        req.setBody(xmlGenerated);
        
        
        Http http = new Http();
        String resBody = '<?xml version="1.0"?><Orders><SendSAP result ="OK" OrderNumber=\'1602293612\' /><Response="Récapitulatif de votre commande 1602293612 Bonjour CHAM" /></Orders>';
        if(!Test.isRunningTest() && !IS_TEST){
        	HTTPResponse res = http.send(req);
        	resBody = res.getBody();
        }else{
            if(!isVaillant){
                resBody = '<?xml version="1.0"?><Orders><SendSAP result ="OK" OrderNumber=\'\' /><Response="Veuillez verifier les points suivants avant de repasser votre commande :  - Article CIR10103342 : Reference inconnue. " /></Orders>';
            }
        }
        System.debug('resBody:' + resBody);
        return resBody;
        //return res.getBody();
    }
    
    public static String extractOrderNumber(String resBody){
        String orderNumber = resBody.split('OrderNumber=')[1].split('/>')[0];
        System.debug('orderNumber:' + orderNumber);
        orderNumber = orderNumber.replace('\'', '');
        System.debug('orderNumber:' + orderNumber);
        orderNumber = orderNumber.trim();
        return orderNumber;
    }
    
    public static String  extractResponseMessage(String resBody){
        if(resBody.contains('<error msg=')){
            String response = resBody.split('<error msg="')[1];
            response = response.remove('/><Response=" " /></Orders>');
            response = response.replace('"', '');
        	return response.trim();
        }else{
        	String response = resBody.split('<Response="')[1];
        	response = response.remove('/></Orders>');
        	response = response.replace('"', '');
        	return response.trim();
        }
    }
    
    /* public static String saveFile(String ordId, String xmlGenerated){
system.debug('## starting method saveFile');

String msg = '';
Datetime dteTime = System.now();
Boolean errorFlag = false;
Blob body;

try{
body = blob.valueOf(xmlGenerated);

ContentVersion conVer = new ContentVersion();
conVer.ContentLocation = 'S'; // to use S specify this document is in Salesforce, to use E for external files
conVer.PathOnClient = 'WSVaillant.' + string.valueOfGmt(dteTime) +'.xml'; // The files name, extension is very important here which will help the file in preview.
conVer.Title = 'WSVaillant ' + string.valueOfGmt(dteTime); // Display name of the files
conVer.VersionData = body; // File body in blob; else needs convertion using EncodingUtil.base64Decode(body);
insert conVer;    //Insert ContentVersion

// First get the Content Document Id from ContentVersion Object
Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
//create ContentDocumentLink  record 
ContentDocumentLink conDocLink = New ContentDocumentLink();
conDocLink.LinkedEntityId = ordId; // Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
conDocLink.ContentDocumentId = conDoc;  //ContentDocumentId Id from ContentVersion
conDocLink.shareType = 'V';
insert conDocLink; 

msg = 'The XML file has been generated successfully.'; 
}
catch(Exception e){            
msg = 'An exception occurred: ' + e.getMessage();
} 

return msg;               

}   */
    
}