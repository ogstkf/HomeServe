/**
 * @File Name          : AP50_GestionRemise_TEST.cls
 * @Description        : 
 * @Author             : KJB
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 09-03-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    12/12/2019         KJB                    Initial Version
**/

@isTest
public class AP50_GestionRemise_TEST {

    static User mainUser;
    static Account testAcc,testAcc1 = new Account();
    static Opportunity opp = new Opportunity();
    static List<Product2> lstProd = new List<Product2>();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static ServiceContract servCon = new ServiceContract();
    static List<ContractLineItem> lstCli = new List<ContractLineItem>();
    static List<Quote> lstQuo = new List<Quote>();
    static List<QuoteLineItem> lstQli = new List<QuoteLineItem>();
    static Asset eqp, eqp1 = new Asset();
    static OperatingHours opHrs;
    static sofactoapp__Raison_Sociale__c srs;
    static ServiceTerritory srvTerr;
    static List<Product2> lstTestProd = new List<Product2>();
    static Asset ast;
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static WorkOrder wrkOrd;
    static Quote quo;
    static WorkType wt;
    
    static{
        

        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;
        
        System.runAs(mainUser){

            //Create Account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

             //Create Account
             testAcc1 = TestFactory.createAccount('Test2');
             testAcc1.BillingPostalCode = '200000';
             testAcc1.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
             insert testAcc1;

            

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;



            //create operating hours
            opHrs = TestFactory.createOperatingHour('test OpHrs');
            insert opHrs;

            //create Corporate Name
            srs = new sofactoapp__Raison_Sociale__c(Name='Test1',
            sofactoapp__Credit_prefix__c='Test1',
            sofactoapp__Invoice_prefix__c='Test1');  
            insert srs;

            // create service territory
            srvTerr = new ServiceTerritory(IsActive = true, Name = 'test Territory' , OperatingHoursId = opHrs.Id, Sofactoapp_Raison_Social__c=srs.Id);
            insert srvTerr;


            // create logement
            Logement__c lgmt = new Logement__c(
                Account__c = testAcc.Id,
                Street__c = 'New Street',
                Postal_Code__c = 'lgmtPC 1',
                City__c = 'lgmtCity 1'
                );
            insert lgmt;

            //Create Opportunity
            opp = TestFactory.createOpportunity('Test1',testAcc.Id);  
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Opportunite_standard').getRecordTypeId();
            system.debug('opp: '+opp);          
            insert opp;

            //Create Products
          /*  lstProd = new list<Product2>{
                new Product2(Name='Prod1',
                             Famille_d_articles__c='Consommables',
                             ProductCode = 'COUV-DEPL-GAR-CHAM',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),

                new Product2(Name='Prod2',
                             Famille_d_articles__c='Pièces détachées',
                             ProductCode = 'DEPL-ZONE',
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),

                new Product2(Name='Prod3',
                             Famille_d_articles__c='Pièces détachées',
                             ProductCode = AP_Constant.productCodeCouvMoGarCham,
                             RecordTypeId =Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('Article').getRecordTypeId()),
                TestFactory.createProductAsset('asset')
            };
            insert lstProd;*/

             // create products
             lstTestProd.add(TestFactory.createProductAsset('testAst'));
             lstTestProd.add(TestFactory.createProduct('cli1'));
             lstTestProd[0].Equipment_family__c = 'Chaudière';
             lstTestProd[0].Equipment_type1__c = 'Chaudière gaz';
             lstTestProd[1].Famille_d_articles__c = 'Pièces détachées';
             lstTestProd[1].ProductCode = 'COUV-DEPL-GAR-CHAM';
             lstTestProd.add(TestFactory.createProduct('qli1'));
             lstTestProd[2].Famille_d_articles__c = 'Pièces détachées';
             lstTestProd[2].ProductCode = 'COUV-DEPL-GAR';
             lstTestProd.add(TestFactory.createProduct('cliP3r'));
             lstTestProd[3].ProductCode = 'P3R';
             lstTestProd.add(TestFactory.createProduct('cliP3'));
             lstTestProd[4].ProductCode = 'P3';
             insert lstTestProd;

            
            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true, recordTypeId = Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Vente').getRecordTypeId());
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create Pricebook Entry
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[0].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[2].Id, 150));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[3].Id, 250));
            lstPrcBkEnt.add(TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[4].Id, 50));
            insert lstPrcBkEnt;

            //creating assets
            ast = TestFactory.createAsset('equipement 1', AP_Constant.assetStatusActif, lgmt.Id);
            ast.Product2Id = lstTestProd[0].Id;
            ast.Logement__c = lgmt.Id;
            ast.AccountId = testAcc.Id;
            insert ast;

            //Create Service Contract
            /*servCon = new ServiceContract(name = 'Service Con',
                                                  Pricebook2Id = lstPrcBk[0].Id,
                                                  AccountId = testAcc.Id,
                                                  Contract_Status__c = 'Draft' ,
                                                  StartDate = System.today(),
                                                  EndDate = System.today() + 1,
                                                  Type__c = 'Collective',
                                                  Option_P2_avec_seuil__c = false,
                                                  Type_de_seuil__c = null,
                                                  Valeur_du_seuil_P2__c = 0,
                                                  RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId());
            insert servCon;*/
             //creating service contract

        
             lstServCon.add(TestFactory.createServiceContract('test serv con 1', testAcc.Id));
             lstServCon[0].Type__c = 'Collective';
             lstServCon[0].Contract_Status__c = 'Active';
             lstServCon[0].Pricebook2Id = lstPrcBk[0].Id;
             lstServCon[0].Asset__c = ast.Id;
             lstServCon[0].StartDate = Date.today();
             lstServCon[0].EndDate = Date.today().addMonths(12);
             lstServCon[0].RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Collectifs_Prives').getRecordTypeId();
             lstServCon[0].Option_P2_avec_seuil__c = true;
             lstServCon[0].Type_de_seuil__c = 'Seuil fixe : les pièces dont le montant est supérieur sont à régler en intégralité';
             lstServCon[0].Valeur_du_seuil_P2__c = 0;

             lstServCon.add(TestFactory.createServiceContract('test serv con 2', testAcc.Id));
             lstServCon[1].Type__c = 'Collective';
             lstServCon[1].Contract_Status__c = 'Active';
             lstServCon[1].Pricebook2Id = lstPrcBk[0].Id;
             lstServCon[1].Asset__c = ast.Id;
             lstServCon[1].StartDate = Date.today();
             lstServCon[1].EndDate = Date.today().addMonths(12);
             lstServCon[1].RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_collectifs_publics').getRecordTypeId();
             lstServCon[1].Option_P2_avec_seuil__c = true;
             lstServCon[1].Type_de_seuil__c = 'Seuil fixe : les pièces dont le montant est supérieur sont à régler en intégralité';
             lstServCon[1].Valeur_du_seuil_P2__c = 0;
             //insert lstServCon;
             lstServCon.add(TestFactory.createServiceContract('test serv con 3', testAcc.Id));
             lstServCon[2].Type__c = 'Collective';
             lstServCon[2].Contract_Status__c = 'Active';
             lstServCon[2].Pricebook2Id = lstPrcBk[0].Id;
             lstServCon[2].Asset__c = ast.Id;
             lstServCon[2].StartDate = Date.today();
             lstServCon[2].EndDate = Date.today().addMonths(12);
             lstServCon[2].RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_collectifs_publics').getRecordTypeId();
             lstServCon[2].ParentServiceContractId = lstServCon[1].Id;
             lstServCon[2].Option_P2_avec_seuil__c = true;
             lstServCon[2].Type_de_seuil__c = 'Seuil remise : les pièces dont le montant est supérieur sont remisées de ce seuil';
             lstServCon[2].Valeur_du_seuil_P2__c = 0;

             lstServCon[0].Asset__c = ast.Id;
             lstServCon[0].Frequence_de_maintenance__c = 2;
             lstServCon[0].Type_de_frequence__c = 'Weeks';
             lstServCon[0].Debut_de_la_periode_de_maintenance__c = 3;
             lstServCon[0].Fin_de_periode_de_maintenance__c = 5;
             lstServCon[0].Periode_de_generation__c = 6;
             lstServCon[0].Type_de_periode_de_generation__c = 'Weeks';
             lstServCon[0].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
             lstServCon[0].Generer_automatiquement_des_OE__c = true;
             lstServCon[0].Horizon_de_generation__c = 5;

             lstServCon[1].Asset__c = ast.Id;
             lstServCon[1].Frequence_de_maintenance__c = 2;
             lstServCon[1].Type_de_frequence__c = 'Weeks';
             lstServCon[1].Debut_de_la_periode_de_maintenance__c = 3;
             lstServCon[1].Fin_de_periode_de_maintenance__c = 5;
             lstServCon[1].Periode_de_generation__c = 6;
             lstServCon[1].Type_de_periode_de_generation__c = 'Weeks';
             lstServCon[1].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
             lstServCon[1].Generer_automatiquement_des_OE__c = true;
             lstServCon[1].Horizon_de_generation__c = 5;

             lstServCon[2].Asset__c = ast.Id;
             lstServCon[2].Frequence_de_maintenance__c = 2;
             lstServCon[2].Type_de_frequence__c = 'Weeks';
             lstServCon[2].Debut_de_la_periode_de_maintenance__c = 3;
             lstServCon[2].Fin_de_periode_de_maintenance__c = 5;
             lstServCon[2].Periode_de_generation__c = 6;
             lstServCon[2].Type_de_periode_de_generation__c = 'Weeks';
             lstServCon[2].Date_du_prochain_OE__c = Date.valueOf(Datetime.newInstance(2020, 8, 10).format('yyyy-MM-dd'));
             lstServCon[2].Generer_automatiquement_des_OE__c = true;
             lstServCon[2].Horizon_de_generation__c = 5;

             lstServCon[0].StartDate =  Date.newInstance(2019, 12, 20);
             lstServCon[1].StartDate =  Date.newInstance(2018, 12, 20);
             lstServCon[2].StartDate =  Date.newInstance(2018, 1, 12);

             insert lstServCon;



           /* eqp = TestFactory.createAsset('equipement', AP_Constant.assetStatusActif, lgmt.Id); 
            eqp.Product2Id = lstProd[3].Id;
            eqp.AccountId = testAcc.Id;

            eqp1 = TestFactory.createAsset('equipement1', AP_Constant.assetStatusActif, lgmt.Id);
            eqp1.Product2Id = lstProd[3].Id;
            eqp1.AccountId = testAcc.Id;

            insert eqp;
            insert eqp1;*/

            
            //create WorkType for WorkOrder
            wt = TestFactory.createWorkType('test', 'minutes', 12);
            wt.Type__c='Maintenance' ;
            wt.Reason__c='Visite sous contrat';
            wt.Equipment_type__c='Chaudière gaz';
            wt.Type_de_client__c = 'Tous';
            insert wt;
            
            //Create WorkOrder
            wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.Type__c = wt.Type__c;
            wrkOrd.Reason__c = wt.Reason__c;
            wrkOrd.AssetId = ast.Id;
            wrkOrd.ServiceContractId = lstServCon[0].Id;
            insert wrkOrd;
    
            //Create Contract Line Item
            lstCli = new list<ContractLineItem>{
                new ContractLineItem(
                                    ServiceContractId = lstServCon[0].Id,
                                     PricebookEntryId = lstPrcBkEnt[0].Id,
                                     Quantity = 5,
                                     UnitPrice = 100,
                                     IsActive__c = true,
                                     AssetId = ast.Id,
                                     Remplacement_effectue__c = false
                    ),
                    new ContractLineItem(
                                    ServiceContractId = lstServCon[2].Id,
                                    PricebookEntryId = lstPrcBkEnt[2].Id,
                                    Quantity = 5,
                                    UnitPrice = 100,
                                    IsActive__c = true,
                                    AssetId = ast.Id,
                                    Remplacement_effectue__c = false
                                    ),
                new ContractLineItem(
                                    ServiceContractId = lstServCon[2].Id,
                                    PricebookEntryId = lstPrcBkEnt[3].Id,
                                    Quantity = 5,
                                    UnitPrice = 100,
                                    IsActive__c = true,
                                    AssetId = ast.Id,
                                    Remplacement_effectue__c = false
                                    )
            };
            insert lstCli;

            //Create Quote
            /*lstQuo = new List<Quote>{
                new Quote(Name ='Quote 1',
                          OpportunityId=opp.Id,
                          Contrat_de_service__c = servCon.Id,
                          Pricebook2Id = lstPrcBk[0].Id,
                          isSync__c = false,
                          RecordTypeId = AP_Constant.getRecTypeId('Quote', 'Devis_standard'),
                          Equipement__c = eqp.Id
                          )
            };
            insert lstQuo; */
            
            //create Quote
            quo = new Quote(    Name ='Test1',
                                Devis_signe_par_le_client__c = false,
                                OpportunityId = opp.Id,
                                Ordre_d_execution__c = wrkOrd.Id,
                                Pricebook2Id = lstPrcBk[0].Id,
                                isSync__c = false,
                                Contrat_de_service__c = lstServCon[0].Id,
                                Equipement__c = ast.Id,
                                RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Devis_standard').getRecordTypeId()
            );
            insert quo;
                       
            //Create Quote Line Items
            lstQli = new list<QuoteLineItem>{
                new QuoteLineItem(QuoteId = quo.Id, 
                                  Product2Id = lstTestProd[1].Id,
                                  PricebookEntryId = lstPrcBkEnt[1].Id,
                                  Quantity = 5,
                                  Remise_en100__c = 100,
                                  Remise_en_euros__c = null,
                                  UnitPrice = 100),

                new QuoteLineItem(QuoteId = quo.Id, 
                                  Product2Id = lstTestProd[2].Id,
                                  PricebookEntryId = lstPrcBkEnt[1].Id,
                                  Quantity = 5,
                                  Remise_en100__c = 100,
                                  Remise_en_euros__c = null,
                                  UnitPrice = 100)
            };
            insert lstQli;

        }

    }

  /*  @isTest
    public static void getRemises_TEST1(){

        System.runAs(mainUser){

            Test.StartTest();

                List<String> lstQuoId = new List<String>();
                lstQuoId.add(lstQuo[0].Id);
                AP50_GestionRemise.getRemises(lstQuoId);

            Test.StopTest();

        }

    }

    @isTest
    public static void getRemises_TEST2(){

        System.runAs(mainUser){

            Test.StartTest();

                AP50_GestionRemise.getRemises(lstQuo);

            Test.StopTest();

        }

    }

    @isTest
    public static void getRemises_TEST3(){
        System.runAs(mainUser){
            // lstCli[0].
            lstProd[0].ProductCode = 'P3R';
            update lstProd[0];
            lstQuo[0].Type_de_devis__c = 'Vente déquipement / Pose';
            update lstQuo[0];

        
            
            Test.StartTest();
            AP50_GestionRemise.getRemises(lstQuo);
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemises_TEST4(){
        System.runAs(mainUser){
            // lstCli[0].
            lstProd[0].ProductCode = 'COUV-MO-GAR-CHAM';
            lstProd[2].ProductCode = 'MO';
            update lstProd[0];
            lstQuo[0].Type_de_devis__c = 'Vente déquipement / Pose';
            update lstQuo[0];

            lstCli[0].IsActive__c = false;
            update lstCli[0];

            Test.StartTest();
            AP50_GestionRemise.getRemises(lstQuo);
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemises_TEST5(){
        System.runAs(mainUser){
            // lstCli[0]

            lstQli[0].UnitPrice = 90;
            update lstQli;

            servCon.Option_P2_avec_seuil__c = true;
            servCon.Type_de_seuil__c = 'Seuil fixe : les pièces dont le montant est supérieur sont à régler en intégralité';
            servCon.Valeur_du_seuil_P2__c = 100;
            update servCon;

            List<String> lstQuoId = new List<String>();
            lstQuoId.add(lstQuo[0].Id);

            lstCli[0].IsActive__c = true;
            update lstCli[0];

            lstProd[0].Famille_d_articles__c = 'Consommables';
            update lstProd[0];

          
           
            Test.StartTest();
            AP50_GestionRemise.GestionRemise(lstQuo);
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemises_TEST6(){
        System.runAs(mainUser){
            // lstCli[0]
            lstQli[0].UnitPrice = 90;
            update lstQli;

            lstProd[0].ProductCode = 'COUV-DEPL-GAR-CHAM';
            lstProd[2].ProductCode = 'DEPL-ZONE';
            update lstProd[0];
           
            servCon.Option_P2_avec_seuil__c = true;
            servCon.Type_de_seuil__c = 'Seuil remise : les pièces dont le montant est supérieur sont remisées de ce seuil';
            servCon.Valeur_du_seuil_P2__c = 60;
            update servCon;

            lstCli[0].IsActive__c = true;
            update lstCli[0];

            lstProd[0].Famille_d_articles__c = 'Pièces détachées';
            update lstProd[0];
           
            Test.StartTest();
            AP50_GestionRemise.GestionRemise(lstQuo);
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemises_TEST7(){
        System.runAs(mainUser){
            // lstCli[0]

            lstProd[0].ProductCode = 'COUV-DEPL-GAR-CHAM';
            lstProd[1].ProductCode = 'DEPL-ZONE';
            update lstProd[0];

            lstCli[0].IsActive__c = true;
            update lstCli[0];
           
            Test.StartTest();
            AP50_GestionRemise.GestionRemise(lstQuo);
            Test.StopTest();
        }
    }*/

    @isTest
    public static void getRemisesIndividual1(){
        System.runAs(mainUser){
            
            Test.StartTest();
            AP50_GestionRemise.getRemises(new List<Quote>{quo});
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemisesIndividual2(){
        System.runAs(mainUser){
            
            Test.StartTest();
            AP50_GestionRemise.getRemises(new List<String>{quo.Id});
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemisesCollectifP3R(){
        System.runAs(mainUser){
            quo.Contrat_de_service__c = lstServCon[2].Id;
            quo.Type_de_devis__c = 'Vente déquipement / Pose';
            update quo;
            Test.StartTest();
            AP50_GestionRemise.getRemises(new List<Quote>{quo});
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemisesCollectifP2P3(){
        System.runAs(mainUser){
            lstTestProd[2].Famille_d_articles__c = 'Pièces détachées';
            lstTestProd[2].Piece_d_usure__c = false;
            update lstTestProd;
            quo.Contrat_de_service__c = lstServCon[2].Id;
            update quo;
            Test.StartTest();
            AP50_GestionRemise.getRemises(new List<Quote>{quo});
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemisesCollectifP2SeuilFixed(){
        System.runAs(mainUser){

            lstQli[0].UnitPrice = 40;
            update lstQli;

            lstTestProd[1].ProductCode = 'COUV-DEPL-GAR-CHAM';
            lstTestProd[2].ProductCode = 'DEPL-ZONE';
            update lstTestProd[0];

            lstServCon[1].Option_P2_avec_seuil__c = true;
            lstServCon[1].Type_de_seuil__c = 'Seuil fixe : les pièces dont le montant est supérieur sont à régler en intégralité';
            lstServCon[1].Valeur_du_seuil_P2__c = 60;
            update lstServCon[1];

            lstTestProd[2].Famille_d_articles__c = 'Pièces détachées';
            lstTestProd[2].Piece_d_usure__c = false;
            update lstTestProd;
            quo.Contrat_de_service__c = lstServCon[2].Id;
            update quo;
            Test.StartTest();
            AP50_GestionRemise.getRemises(new List<Quote>{quo});
            Test.StopTest();
        }
    }

    @isTest
    public static void getRemisesCollectifP2SeuilRemise(){
        System.runAs(mainUser){
            lstQli[0].UnitPrice = 90;
            update lstQli;

            lstTestProd[1].ProductCode = 'COUV-DEPL-GAR-CHAM';
            lstTestProd[2].ProductCode = 'DEPL-ZONE';
            update lstTestProd[0];

            lstServCon[1].Option_P2_avec_seuil__c = true;
            lstServCon[1].Type_de_seuil__c = 'Seuil remise : les pièces dont le montant est supérieur sont remisées de ce seuil';
            lstServCon[1].Valeur_du_seuil_P2__c = 60;
            update lstServCon[1];

            
            lstCli[0].IsActive__c = true;
            update lstCli[0];

            lstTestProd[2].Famille_d_articles__c = 'Pièces détachées';
            lstTestProd[2].Piece_d_usure__c = false;
            // lstTestProd[2].ProductCode = '';
            update lstTestProd;

            quo.Contrat_de_service__c = lstServCon[2].Id;
            update quo;
            Test.StartTest();
            AP50_GestionRemise.getRemises(new List<Quote>{quo});
            Test.StopTest();
        }
    }
    
}