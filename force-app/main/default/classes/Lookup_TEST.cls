/**
 * @File Name          : Lookup_TEST.cls
 * @Description        : 
 * @Author             : DMO
 * @Group              : 
 * @Last Modified By   : DMO
 * @Last Modified On   : 06/02/2020, 10:30
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/02/2020   DMO   Initial Version
 
**/
@isTest
public with sharing class Lookup_TEST {
    static User mainUser;
    static Account testAcc= new Account();
    static List<ServiceTerritory> lstServiceTerritory = new List<ServiceTerritory>();

    static{
        mainUser = TestFactory.createAdminUser('LC23', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            //Create operating hours
            OperatingHours newOperatingHour1 = TestFactory.createOperatingHour('test1');
            insert newOperatingHour1;
            OperatingHours newOperatingHour2 = TestFactory.createOperatingHour('test2');
            insert newOperatingHour2;

            //create sofacto
            sofactoapp__Raison_Sociale__c sofa1 = new sofactoapp__Raison_Sociale__c(Name= 'TEST1', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa1;
            //create sofacto
            sofactoapp__Raison_Sociale__c sofa2 = new sofactoapp__Raison_Sociale__c(Name= 'TEST2', sofactoapp__Credit_prefix__c= '2341', sofactoapp__Invoice_prefix__c='4321');
            insert sofa2;
            //Insert Service territories/ agencies
            lstServiceTerritory = new List<ServiceTerritory>{
                new ServiceTerritory(
                    Name = 'testabc',
                    Agency_Code__c = '0007',
                    OperatingHoursId = newOperatingHour1.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa1.Id
                ),
                new ServiceTerritory(
                    Name = 'testabcdef',
                    Agency_Code__c = '0008',
                    OperatingHoursId = newOperatingHour2.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa2.Id
                )
            };
            insert lstServiceTerritory;
        }
    }

    @isTest
    public static void testSearchDb(){
        System.runAs(mainUser){
            Test.startTest();
            string result = Lookup.searchDB('ServiceTerritory','Id','Name',10,'Name','abc');
            Test.stopTest();

            List<Lookup.ResultWrapper> lstWrapper = (List<Lookup.ResultWrapper>)JSON.deserialize(result, List<Lookup.ResultWrapper>.class);
            System.assertEquals(2,lstWrapper.size());
        }
    }
}