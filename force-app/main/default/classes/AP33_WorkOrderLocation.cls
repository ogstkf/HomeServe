public with sharing class AP33_WorkOrderLocation {
/**
 * @File Name          : AP33_WorkOrderLocation.cls
 * @Description        : 
 * @Author             : Spoon Consulting (KZE)
 * @Group              : 
 * @Last Modified By   : KZE
 * @Last Modified On   : 30/10/2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         30-10-2019     		    KZE         Initial Version (CT-1161)
**/
    public static void updateLocation(List<AssignedResource> lstAssignedResource){
        // System.debug('## method AP33_WorkOrderLocation.updateLocation START');

        // Set<Id> setSRIds = new Set<Id>();
        // Set<Id> setSAIds = new Set<Id>();
        // Map<Id, id > mapARtoSR = new Map<Id, id> ();
        // Map<Id, List<id> > mapSAtoARInserted = new Map<Id, List<id>> ();
        // Map<Id, Schema.Location > mapSRtoLO = new Map<Id, Schema.Location> ();
        // Map<Id, Id > mapWOtoLocation = new Map<Id, Id> ();
        // Schema.Location locationNeeded = new Schema.Location();
        // List<WorkOrder> lstWOToUpdate = new List<WorkOrder>();

        // for(AssignedResource AR : lstAssignedResource){
        //     setSAIds.add(AR.ServiceAppointment.Id);
        //     setSRIds.add(AR.ServiceResource.Id);

        //     //if map contains key
        //         //pa ajout dan ajoute
        //         //remove setId
        //     if(mapSAtoARInserted.containsKey(AR.Id)){
        //         mapSAtoARInserted.get(AR.ServiceAppointment.Id).add(AR.Id); 
        //     }
        //     else{
        //         mapSAtoARInserted.put(AR.ServiceAppointment.Id , new List<Id>{AR.Id}); 
        //     }
        //     mapARtoSR.put(AR.Id, AR.ServiceResource.Id);   
        // }
        
        // System.debug('## mapSAtoARInserted:' + mapSAtoARInserted);

        // for(Schema.Location lo: [SELECT Id , LocationType, Service_Resource__r.Id, Service_Resource__c from Location WHERE Service_Resource__c IN :setSRIds ]){
        //     mapSRtoLO.put(lo.Service_Resource__r.Id, lo);
            
        //     System.debug('## lo.Service_Resource__r.Id:' + lo.Service_Resource__r.Id);
        //     System.debug('## lo.LocationType:' + lo.LocationType);
        // }
        
        // for(ServiceAppointment SA: [SELECT Id,Work_Order__c, (SELECT id, ServiceResource.Id from ServiceResources) FROM ServiceAppointment WHERE id IN :setSAIds ]){
        //     List<AssignedResource> lstAR =  SA.ServiceResources;
        //     System.debug('## SA.ServiceResources:' + SA.ServiceResources);
        //     System.debug('## lstAR.size():' + lstAR.size());
        //     System.debug('## lstAR[0].Id:' +lstAR[0].Id);
        //     System.debug('## mapSAtoARInserted.get(SA.Id):' + mapSAtoARInserted.get(SA.Id));

        //     if(lstAR.size() == 1 && lstAR[0].Id == mapSAtoARInserted.get(SA.Id)[0]){
        //         Id SRId = mapARtoSR.get(lstAR[0].Id) ;
        //         locationNeeded = mapSRtoLO.get(SRId);
        //         if(locationNeeded.LocationType == 'Véhicule'){
        //             mapWOtoLocation.put(SA.Work_Order__c, locationNeeded.Id);
        //         }
        //     }
        //     System.debug('## mapWOtoLocation:' + mapWOtoLocation);
        // }

        // for(Id woid : mapWOtoLocation.keySet()){
        //     WorkOrder w = new WorkOrder(id= woid , LocationId= mapWOtoLocation.get(woid));
        //     lstWOToUpdate.add(w);
        // }
        
        // if(lstWOToUpdate.size() > 0){
        //     System.debug('## lstWOToUpdate:' + lstWOToUpdate);
        //     update lstWOToUpdate;
        // }
        // System.debug('## method AP33_WorkOrderLocation.updateLocation END');
    }
}