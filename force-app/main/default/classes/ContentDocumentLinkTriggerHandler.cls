/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 07-13-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-13-2020   ZJO   Initial Version
**/
public with sharing class ContentDocumentLinkTriggerHandler {

/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : handler ContentDocumentLink
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 28-JAN-2019  YGO    1.0     Initial version
-- 24-JUN-2020  DMU    1.1     Commented AP03_ManageLeadFiles.callWSBlue due to homeserve project
--------------------------------------------------------------------------------------
**************************************************************************************/  
	public static boolean bypass = false;

	public void handleAfterInsert(List<ContentDocumentLink> lstNewCDL){
		/*
		if(!bypass){
			System.debug('### handleAfterInsert');

			Map<String, ContentDocumentLink> mapLeanCDL = new Map<String, ContentDocumentLink>();

			for(integer i=0; i<lstNewCDL.size(); i++){
				String linkedId = lstNewCDL[i].LinkedEntityId;
				//DMU 20190411 - Lift & paste logic from LeadTriggerHandler to lauch WS AP01_BlueWSCallout when ServApp.Status is modified (MAJ commande et ficheir PDF) 
				//Requirement on mail "Transfert des flux Blue sur l'objet RDV de service" 20190410
				//if(linkedId.startsWith(Lead.SObjectType.getDescribe().getKeyPrefix())){
				
				if(linkedId.startsWith(ServiceAppointment.SObjectType.getDescribe().getKeyPrefix())){
					mapLeanCDL.put(linkedId, lstNewCDL[i]);
				}
				
			}

			// if(mapLeanCDL.size() > 0) AP03_ManageLeadFiles.callWSBlue(mapLeanCDL);
		}*/
	}
}