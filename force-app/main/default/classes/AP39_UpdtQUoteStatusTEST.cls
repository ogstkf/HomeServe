/**
 * @File Name          : AP39_UpdtQUoteStatusTEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ARA
 * @Last Modified On   : 04-07-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    05/11/2019   AMO     Initial Version
**/
@isTest
public with sharing class AP39_UpdtQUoteStatusTEST {

    static User adminUser;
    static Account testAcc = new Account();
    static sofactoapp__Compte_auxiliaire__c CA = new sofactoapp__Compte_auxiliaire__c (); // Sub-ledger Account
    static sofactoapp__Coordonnees_bancaires__c CB = new sofactoapp__Coordonnees_bancaires__c(); // Bank Details
    static sofactoapp__Compte_comptable__c CC = new sofactoapp__Compte_comptable__c (); // Accounting Account
    static List<sofactoapp__Plan_comptable__c> PC = new List<sofactoapp__Plan_comptable__c> (); // Chart of Accounts
    static List<Quote> lstQuote = new List<Quote>();
    static List<Opportunity> lstOpp = new List<Opportunity>();
    static List<WorkOrder> lstWorkOrder = new List<WorkOrder>();
    static List<ProductRequestLineItem> lstprli = new List<ProductRequestLineItem>();
    static List<ProductRequest> lstProdRequest = new List<ProductRequest>();
    static Product2 prod = new Product2();
    //bypass
    static Bypass__c bp = new Bypass__c();

        static {
            adminUser = TestFactory.createAdminUser('AP39_UpdtQUoteStatus@test.COM', TestFactory.getProfileAdminId());
		    insert adminUser;

            bp.SetupOwnerId  = adminUser.Id;
            bp.BypassTrigger__c = 'AP07,AP12,AP27,AP14,AP16,WorkOrderTrigger,AP76,AP53';
            bp.BypassValidationRules__c = true;
            bp.BypassWorkflows__c = true;
            insert bp;

            System.runAs(adminUser){

                PC = new List<sofactoapp__Plan_comptable__c> {
                    new sofactoapp__Plan_comptable__c(),
                    new sofactoapp__Plan_comptable__c(),
                    new sofactoapp__Plan_comptable__c()
                };  
                insert PC; 

                // create sofactoapp__Compte_comptable__c (Accounting Account)
                CC.Name = 'test CC';
                CC.sofactoapp__Libelle__c = 'test libellé';
                CC.sofactoapp__Plan_comptable__c = PC[0].Id;
                insert CC; 

                // create sofactoapp__Compte_auxiliaire__c (Sub-ledger Account)
                CA.Name = 'test account aux';
                CA.sofactoapp__Compte_comptable__c = CC.Id;
                CA.Compte_comptable_Prelevement__c = CC.Id;
                insert CA; 
                        

                testAcc = TestFactory.createAccount('Test1');
                testAcc.BillingPostalCode = '1233456';
                testAcc.sofactoapp__Compte_auxiliaire__c =  CA.Id;
                testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

                insert testAcc;

                sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
                testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
                
                update testAcc;    

                lstOpp =  new List<Opportunity>{ TestFactory.createOpportunity('Test1',testAcc.Id) ,
                                                TestFactory.createOpportunity('Test2',testAcc.Id) ,
                                                TestFactory.createOpportunity('Test2',testAcc.Id) ,
                                                TestFactory.createOpportunity('Test2',testAcc.Id) ,
                                                TestFactory.createOpportunity('Test2',testAcc.Id) ,
                                                TestFactory.createOpportunity('Test2',testAcc.Id) ,
                                                TestFactory.createOpportunity('Test2',testAcc.Id) ,
                                                TestFactory.createOpportunity('Test2',testAcc.Id) 
                };
                lstOpp[0].APP_intervention_immediate__c = true;
               // lstOpp[0].IsWon = true; 
                lstOpp[0].StageName = 'Denied';
                lstOpp[1].APP_intervention_immediate__c = true;
                //lstOpp[1].IsWon = true; 
                lstOpp[1].StageName = 'Denied';
                lstOpp[2].APP_intervention_immediate__c = true;
                //lstOpp[2].IsWon = true; 
                lstOpp[2].StageName = 'Denied';
                lstOpp[3].APP_intervention_immediate__c = true;
                //lstOpp[3].IsWon = true; 
                lstOpp[3].StageName = 'Denied';
                lstOpp[4].APP_intervention_immediate__c = true;
                //lstOpp[4].IsWon = true; 
                lstOpp[4].StageName = 'Denied';
                lstOpp[5].APP_intervention_immediate__c = true;
                //lstOpp[5].IsWon = true; 
                lstOpp[5].StageName = 'Denied';
                lstOpp[6].APP_intervention_immediate__c = true;
                lstOpp[6].StageName = 'Denied';
                //lstOpp[6].IsWon = true; 
                lstOpp[7].APP_intervention_immediate__c = true;
                lstOpp[7].StageName = 'Denied';
                //lstOpp[7].IsWon = true; 
                insert lstOpp;

                //Create WorkOrder
                lstWorkOrder = new List<WorkOrder>{
                    new WorkOrder(Status = AP_Constant.wrkOrderStatusNouveau, Priority = AP_Constant.wrkOrderPriorityNormal)
                };

                insert lstWorkOrder;

                            lstProdRequest = new List<ProductRequest> {
                new ProductRequest(),
                new ProductRequest(),
                new ProductRequest()
            };  
            insert lstProdRequest;    

                        //Create product
            Product2 prod = new Product2(Name = 'Product X',
                                        ProductCode = 'Pro-X',
                                        isActive = true,
                                        Statut__c = 'Approuvée',
                                        Equipment_family__c = 'Chaudière',
                                        Equipment_type__c = 'Chaudière gaz'
            );
            insert prod;

            Product2 newProduct2 = TestFactory.createProduct('Sécurité (P2) Chaudière Gaz');
            newProduct2.IsBundle__c = true;
            newProduct2.ProductCode='SECUG';
            newProduct2.Statut__c = 'Approuvée';

            insert newProduct2;


                lstprli = new List<ProductRequestLineItem>{
                    new ProductRequestLineItem(                        
                            Product2Id = newProduct2.Id,
                            QuantityRequested = 1
                            ,ParentId = lstProdRequest[0].Id
                            ,Status = 'En cours de commande'
                            // ,Commande__c = lstOrder[0].Id)
                            ,WorkOrderId = lstWorkOrder[0].Id
                        )
                   /* new ProductRequestLineItem(                        
                            Product2Id = newProduct2.Id,
                            QuantityRequested = 1
                            ,ParentId = lstProdRequest[1].Id
                            ,Status = 'Réservé préparé'
                            // ,Commande__c = lstOrder[0].Id)
                            ,WorkOrderId = lstWorkOrder[0].Id
                        ),
                    new ProductRequestLineItem(                        
                            Product2Id = newProduct2.Id,
                            QuantityRequested = 1
                            ,ParentId = lstProdRequest[2].Id
                            ,Status = 'A commander'
                            // ,Commande__c = lstOrder[0].Id)
                            ,WorkOrderId = lstWorkOrder[0].Id
                        )*/
                    
                };

                insert lstprli;

                //create Quotes
                lstQuote =  new List<Quote>{new Quote(Name ='Test1',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[0].Id, Status = 'Denied', Date_de_debut_des_travaux__c = System.today(), tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200) ,
                                            // new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[1].Id, Status = 'Validé,signé mais abandonné') ,
                                            // new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[2].Id, Status = 'Validé, signé et terminé') ,
                                            new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[3].Id, Status = 'Needs Review', Date_de_debut_des_travaux__c = System.today(), tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200) ,
                                            new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[4].Id, Status = 'In Review', Date_de_debut_des_travaux__c = System.today(),tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200) ,
                                            new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[5].Id, Status = 'Draft', Date_de_debut_des_travaux__c = System.today(), tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200) ,
                                            new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[6].Id, Status = 'Denied', Date_de_debut_des_travaux__c = System.today(), tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200) ,
                                            new Quote(Name ='Test2',  Devis_signe_par_le_client__c = false, OpportunityId = lstOpp[7].Id, Status = 'Denied', Date_de_debut_des_travaux__c = System.today(), tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200),
                                            //CT-1181
                                            new Quote(Name ='Test2',  Devis_signe_par_le_client__c = true, OpportunityId = lstOpp[7].Id, Status = 'Validé, signé - en attente d\'intervention', Ordre_d_execution__c = lstWorkOrder[0].Id, Date_de_debut_des_travaux__c = System.today(), tech_deja_facture__c = true, Mode_de_paiement_de_l_acompte__c = 'CB', Montant_de_l_acompte_verse__c=200)  
                };
                insert lstQuote;
                
                lstQuote[0].Montant_de_l_acompte_verse__c = 300;
                lstQuote[1].Montant_de_l_acompte_verse__c = 300;
                lstQuote[2].Montant_de_l_acompte_verse__c= 300;
                lstQuote[3].Montant_de_l_acompte_verse__c= 300;
                lstQuote[4].Montant_de_l_acompte_verse__c= 300;
                lstQuote[5].Montant_de_l_acompte_verse__c= 300;
                lstQuote[6].Montant_de_l_acompte_verse__c= 300;
                update lstQuote;

            }
        }

    @isTest
    public static void testUpdate(){
        System.runAs(adminUser){

            lstQuote[0].Status = 'Expired';
            // lstQuote[1].Status = 'Denied';
            // lstQuote[2].Status = 'Validé,signé mais abandonné';
            // lstQuote[3].Status = 'Validé, signé et terminé';
            lstQuote[4].Status = 'Needs Review';
            lstQuote[5].Status = 'In Review';
            // lstQuote[6].Status = 'In Review';


            Test.startTest();
            update lstQuote;
            Test.stopTest();

            Opportunity opp1 = [SELECT Id, StageName FROM Opportunity WHERE Id = :lstOpp[0].Id LIMIT 1];
            // System.assertEquals(opp1.StageName, 'Expired');
            // Opportunity opp2 = [SELECT Id, StageName FROM Opportunity WHERE Id = :lstOpp[3].Id LIMIT 1];
            // System.assertEquals(opp2.StageName, 'Closed Won');
            // System.assertEquals(lstOpport[4].StageName, 'Negotiation');
        }
    }

    @isTest
    public static void testNegotiation(){
        System.runAs(adminUser){


            lstQuote[6].Status = 'Validé,signé mais abandonné';
            lstQuote[6].Devis_signe_par_le_client__c = true;
            lstQuote[6].Date_de_debut_des_travaux__c = Date.today();
            lstQuote[6].Raisons_de_l_abandon_du_devis__c = 'Important';


            Test.startTest();
            update lstQuote[6];
            Test.stopTest();

            System.assertEquals(lstQuote[6].Status, 'Validé,signé mais abandonné');

            ProductRequestLineItem prli1 = [SELECT Id, Status FROM ProductRequestLineItem WHERE Id = :lstprli[0].Id];
            System.assertEquals(prli1.Status, 'En cours de commande');
          //  ProductRequestLineItem prli2 = [SELECT Id, Status FROM ProductRequestLineItem WHERE Id = :lstprli[2].Id];
           // System.assertEquals(prli2.Status, 'Annulé');
        }
    }

    @isTest
    public static void testPrli(){
        System.runAs(adminUser){


            lstQuote[4].Status = 'Needs Review';
            lstQuote[5].Status = 'In Review';
            lstQuote[3].Status =  null;
            lstQuote[1].Status = 'Draft';
            lstQuote[2].Status = 'Denied';

            Test.startTest();
            update lstQuote;
            Test.stopTest();

            System.assertEquals(lstQuote[4].Status, 'Needs Review');
            System.assertEquals(lstQuote[5].Status, 'In Review');
            System.assertEquals(lstQuote[1].Status, 'Draft');
            System.assertEquals(lstQuote[2].Status, 'Denied');
            System.assertEquals(lstQuote[3].Status, null);

            List<Opportunity> lstTest = [SELECT Id, StageName FROM Opportunity WHERE Id IN :lstOpp];
            System.debug('Opportunity:' + lstTest);
            Opportunity opp3 = [SELECT Id, StageName FROM Opportunity WHERE Id = :lstOpp[6].Id];
            // System.assertEquals(opp3.StageName, 'Negotiation');
            Opportunity opp4 = [SELECT Id, StageName FROM Opportunity WHERE Id = :lstOpp[7].Id];
            System.assertEquals(opp4.StageName, 'Proposal');
            Opportunity opp5 = [SELECT Id, StageName FROM Opportunity WHERE Id = :lstOpp[3].Id];
            System.assertEquals(opp5.StageName, 'Qualification');
            Opportunity opp6 = [SELECT Id, StageName FROM Opportunity WHERE Id = :lstOpp[4].Id];
            System.assertEquals(opp6.StageName, 'Closed Lost');
            Opportunity opp7 = [SELECT Id, StageName FROM Opportunity WHERE Id = :lstOpp[5].Id];
            System.assertEquals(opp7.StageName, 'Open');


        }
    }
}