/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 28-04-2021
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-09-2020   AMO   Initial Version
**/
public with sharing class VFC72_ServiceContract {

    public List<ServiceContract> lstServiceContract {get;set;}
    public ServiceContract actualServCon {get;set;}
    public ContractLineItem actualConLineItem {get;set;}
    public String statut {get; set;}
    public String Telproprietaire {get; set;}
    public Set<Id> setProdId {get; set;}    
    public Map<Id, ContractLineItem> mapProdCli {get; set;}
    public Boolean RemiseBln {get; set;}
    public Boolean PrixHTRemiseBln {get; set;}
    public string idSignature {get;set;}    
    public List<asset> lstAssEnf {get; set;}
    public DateTime dateDuJr {get; set;}

    public Boolean lstBundleProdSize {get; set;} //To remove
    public List<Product_Bundle__c> lstBundleProd {get; set;} //To remove

    public list<ContractLineItem> lstCLI = new list<ContractLineItem>();
    public list<ContractLineItem> lstCLIOption {get; set;}
    public list<ContractLineItem> lstCLINOTOpt {get; set;}
    public Boolean lstCLIOptionSize {get; set;}

    //MNA 28/04/2021
    public Decimal ScSubtotal {get;set;}
    public Decimal ScTotalPrice {get;set;}
    public Decimal ScGrandTotal {get;set;}
    
    public Decimal CliUnitPrice {get;set;}
    public Decimal CliSubtotal {get;set;}
    public Decimal CliTotalPrice {get;set;}

    public List<wrapCli> lstCliOptionArrondi {get;set;}

    public VFC72_ServiceContract(){
        System.debug('** starting VFC72_ServiceContract');
        String ids = ApexPages.currentPage().getParameters().get('lstIds');
        List<Id> lstIds = (List<Id>)JSON.deserialize(ids, List<Id>.class);
        System.debug('** lstIds:' + lstIds);

        dateDuJr = system.now(); //pour la date d'edition

        if(lstIds.size() > 0){
            lstServiceContract = new List<ServiceContract>([SELECT Id,
                                            RecordType.DeveloperName,
                                            Agency__r.Phone__c,
                                            Agency__r.Libelle_horaires__c,
                                            Payeur_du_contrat__r.IsPersonAccount,
                                            Payeur_du_contrat__r.Salutation,
                                            Payeur_du_contrat__r.FirstName,
                                            Payeur_du_contrat__r.LastName,
                                            Payeur_du_contrat__r.Raison_sociale__c,
                                            Payeur_du_contrat__r.Imm_Res__c,
                                            Payeur_du_contrat__r.Door__c,
                                            Payeur_du_contrat__r.Floor__c,
                                            Payeur_du_contrat__r.BillingStreet,
                                            Payeur_du_contrat__r.Adress_Complement__c,
                                            Payeur_du_contrat__r.BillingPostalCode,
                                            Payeur_du_contrat__r.BillingCity,
                                            Payeur_du_contrat__r.ClientNumber__c,
                                            Payeur_du_contrat__r.RecordType.Name,
                                            Payeur_du_contrat__r.PersonHomePhone,
                                            Payeur_du_contrat__r.Phone,
                                            Payeur_du_contrat__r.PersonMobilePhone,
                                            Payeur_du_contrat__r.PersonEmail,
                                            Payeur_du_contrat__r.BusinessAccountEmail__c,
                                            Payeur_du_contrat__r.PersonOptInEMailSMSCompany__pc,
                                            Numero_du_contrat__c,
                                            TECH_Date_d_edition__c,
                                            StartDate,
                                            EndDate,
                                            GrandTotal,
                                            Agency__r.Site_web__c,
                                            TotalPrice,
                                            Tax__c,
                                            Agency__r.IBAN__c,
                                            Agency__r.Capital_in_Eur__c,
                                            Agency__r.name,
                                            Agency__r.Address,
                                            Agency__r.street,
                                            Agency__r.Street2__c,
                                            Agency__r.PostalCode,
                                            Agency__r.City,
                                            Agency__r.Email__c,
                                            Agency__r.Legal_Form__c,
                                            Agency__r.TECH_CapitalInEur__c,
                                            Agency__r.Siren__c,
                                            Agency__r.RCS__c,
                                            Agency__r.SIRET__c,
                                            Agency__r.APE__c,
                                            Agency__r.Intra_Community_VAT__c,
                                            Agency__r.Logo_Name_Agency__c,
                                            Agency__r.Horaires_astreinte__c,
                                            Asset__c,
                                            Asset__r.Equipment_type_auto__c,
                                            Asset__r.Brand_auto__c,
                                            Asset__r.Equipment_type__c,
                                            Asset__r.Product2.Equipment_family__c,
                                            Asset__r.Model_auto__c,
                                            Asset__r.SerialNumber,
                                            Asset__r.Date_de_mise_en_service__c,
                                            Asset__r.Manufacturer_s_end_of_warranty_auto__c,
                                            Agency__r.Telephone_d_astreinte__c,
                                            Contact.Account.Salutation,
                                            Contact.Account.FirstName,
                                            Contact.Account.LastName,
                                            Inhabitant__c,
                                            Nombre_d_interventions__c,
                                            Home_Owner__c,
                                            Home_Owner__r.Name,
                                            Home_Owner__r.PersonMobilePhone,
                                            Home_Owner__r.BillingStreet,
                                            Home_Owner__r.BillingPostalCode,
                                            Home_Owner__r.BillingCity,
                                            Home_Owner__r.BillingCountry,
                                            Inhabitant__r.Name,
                                            Inhabitant__r.BillingStreet,
                                            Inhabitant__r.BillingPostalCode,
                                            Inhabitant__r.BillingCity,
                                            Inhabitant__r.PersonMobilePhone,
                                            Payeur_du_contrat__c,
                                            logement__r.Legal_Guardian__c,
                                            logement__r.Legal_Guardian__r.Name, 
                                            logement__r.Legal_Guardian__r.BillingStreet,
                                            logement__r.Legal_Guardian__r.BillingPostalCode,
                                            logement__r.Legal_Guardian__r.BillingCity,
                                            logement__r.Legal_Guardian__r.BillingCountry,
                                            logement__r.Legal_Guardian__r.PersonMobilePhone,
                                            Logement__r.City__c,
                                            Logement__r.Inhabitant__c,
                                            Logement__r.Owner__c,
                                            Tax,
                                            Subtotal,
                                            Discount,
                                            TECH_Date_Edition_R1__c,
                                            Date_de_signature_client__c,
                                            Signature_Hash_client__c, 
                                            (SELECT Id, Subtotal, Product2.Name, Asset.Equipment_type_auto__c, IsBundle__c, tech_nom_produit__c, TECH_nombre_d_interventions__c, Product2Id, Product2.Type_de_garantie__c, UnitPrice, Discount, TotalPrice, Quantity, Product2.productCode  FROM ContractLineItems)
                                            FROM ServiceContract 
                                            WHERE Id IN:lstIds]);
            
            System.debug('** List of ServiceContracts:' + lstServiceContract);

        }

        if(lstServiceContract.size()>0){
            System.debug('** ServiceContract present');

            actualServCon=lstServiceContract[0];

            //arrondi à 2 chiffres après virgule pour le service Contract
            ScSubtotal = arrondi(actualServCon.Subtotal);
            ScTotalPrice = arrondi(actualServCon.TotalPrice);
            actualServCon.Tax = arrondi(actualServCon.Tax);
            ScGrandTotal = arrondi(actualServCon.GrandTotal);

            System.debug('** actualServCon:' + actualServCon);

            if(actualServCon.ContractLineItems.size() > 0){
                System.debug('** actualServCon contain CLIs');
                lstCLIOption = new list<ContractLineItem>();
                lstCLINOTOpt = new list<ContractLineItem>();
                lstCliOptionArrondi = new List<wrapCli>();
                actualConLineItem = new ContractLineItem();
                lstCLI = actualServCon.ContractLineItems;
                lstCLIOptionSize = false;
                setProdId = new set<Id>();
                mapProdCli = new Map<Id, ContractLineItem>();
                list<decimal> lstRemise = new list<decimal>();
                List<decimal> lstPrixHTRemise = new List<decimal>();
                RemiseBln = false;
                lstBundleProdSize = false;
                PrixHTRemiseBln = false;

                for(ContractLineItem cli : lstCLI){
                    if(cli.IsBundle__c){
                        actualConLineItem = cli;

                        CliUnitPrice = arrondi(cli.UnitPrice);
                        CliSubtotal = arrondi(cli.Subtotal);
                        CliTotalPrice = arrondi(cli.TotalPrice);

                        setProdId.add(cli.Product2Id);
                        mapProdCli.put(cli.Product2Id, cli);
                    }
                    if(cli.IsBundle__c==false && (cli.Product2.productCode).startsWith('OPT')){   // cli.UnitPrice>0
                        lstCLIOption.add(cli);
                        lstCliOptionArrondi.add(new wrapCli(cli));
                    }
                    lstCLIOptionSize = lstCLIOption.size()>0 ? true : false;

                    system.debug('**lstCLIOption:'+lstCLIOption);
                    system.debug('**lstCLIOptionSize:'+lstCLIOptionSize);

                    if(cli.IsBundle__c==false && !(cli.Product2.productCode).startsWith('OPT')){  
                        lstCLINOTOpt.add(cli);
                    }

                    if(cli.Discount <> null && cli.Discount <> 0.00){
                        system.debug('*** dmu discount: '+cli.Discount);
                        lstRemise.add(cli.Discount);
                    }
                    RemiseBln = lstRemise.size() > 0 ? true : false;
                    if(cli.TotalPrice != 0){
                        lstPrixHTRemise.add(cli.TotalPrice);
                    }
                    PrixHTRemiseBln = lstPrixHTRemise.size() > 0 ? true : false;
                }                    
                lstBundleProd = new List<Product_Bundle__c>([SELECT Id, Optional_Item__c, Product__c, Product__r.Name, Bundle__r.Name 
                                                            FROM Product_Bundle__c 
                                                            WHERE Optional_Item__c = true 
                                                            AND Product__c IN :setProdId]);
                lstBundleProdSize = lstBundleProd.size() > 0 ? true : false;
                System.debug('** lstBundleProd' + lstBundleProd);

                System.debug('** lstCLI' + lstCLI);

                System.debug('** lstCLIOption' + lstCLIOption);
                System.debug('** lstBundleProdSize' + lstBundleProdSize);
                System.debug('** RemiseBln' + RemiseBln);
                System.debug('** lstRemise' + lstRemise);
            }

            //Statut for VFP73
            if(actualServCon.Payeur_du_contrat__c == actualServCon.Logement__r.Owner__c && actualServCon.Logement__r.Owner__c == actualServCon.Logement__r.Inhabitant__c){
                statut = 'propriétaire occupant';
            }
            if(actualServCon.Payeur_du_contrat__c == actualServCon.Logement__r.Owner__c && actualServCon.Payeur_du_contrat__c != actualServCon.Logement__r.Inhabitant__c){
                statut = 'propriétaire non occupant';
            }
            if(actualServCon.Payeur_du_contrat__c == actualServCon.Logement__r.Inhabitant__c && actualServCon.Payeur_du_contrat__c != actualServCon.Logement__r.Owner__c){
                statut = 'Occupant';
            }
            if(actualServCon.Payeur_du_contrat__c != actualServCon.Logement__r.Inhabitant__c && actualServCon.Payeur_du_contrat__c != actualServCon.Logement__r.Owner__c){
                statut = '';
            }

            //Legal Guardian null or not null
            if(actualServCon.logement__r.Legal_Guardian__c != null){
                // Champlibre = actualServCon.logement__r.Legal_Guardian__r.Name + ' ' + actualServCon.logement__r.Legal_Guardian__r.BillingStreet + ' ' + actualServCon.logement__r.Legal_Guardian__r.BillingPostalCode + ' ' + actualServCon.logement__r.Legal_Guardian__r.BillingCity + ' ' + actualServCon.logement__r.Legal_Guardian__r.BillingCountry;
                Telproprietaire = actualServCon.logement__r.Legal_Guardian__r.PersonMobilePhone;
            }
            if(actualServCon.logement__r.Legal_Guardian__c == null){
                // Champlibre = actualServCon.Home_Owner__r.Name + ' ' + actualServCon.Home_Owner__r.BillingStreet + ' ' + actualServCon.Home_Owner__r.BillingPostalCode + ' ' + actualServCon.Home_Owner__r.BillingCity + ' ' + actualServCon.Home_Owner__r.BillingCountry;
                Telproprietaire = actualServCon.Home_Owner__r.PersonMobilePhone;
            }

            //Logique pour la signature
            List<Attachment> lstAttachmentClient = new List<Attachment>();
            //List<ContentDocumentLink> lstContentDocumentClient = new List<ContentDocumentLink>();
            lstAttachmentClient = [select Id,Name from Attachment where ParentId=:actualServCon.Id  and  Name='SignatureClient.png' order by createdDate desc];
            //lstContentDocumentClient = [SELECT Id, ContentDocument.Title, ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink where LinkedEntityId=:actualServCon.Id and ContentDocument.Title='SignatureClient' order by systemmodstamp desc];
            if(lstAttachmentClient.size() > 0){
                idSignature = '/servlet/servlet.FileDownload?file='+lstAttachmentClient[0].Id;
            }else{
                idSignature = 'null';
            }
            system.debug('######idSignature:'+idSignature);
            //Requete pour trouver les assets enfants  
            lstAssEnf = new List<asset>();                
            if(actualServCon.Asset__c <> null){
                lstAssEnf = [SELECT id, Brand_auto__c, Model_auto__c, SerialNumber FROM Asset WHERE ParentId =: actualServCon.Asset__c AND Active__c = true ];
            }
        }
    }
    public static Decimal arrondi(Decimal dec) {
        if (dec == null)
            return null;
        else
            return (Math.round(dec * 100) / 100) + 0.00;
    }

    public class wrapCli {
        public String ProductName {get;set;}
        public Decimal UnitPrice {get;set;}
        public Decimal Quantity {get;set;}
        public Decimal Subtotal {get;set;}
        public Decimal Discount {get;set;}
        public Decimal TotalPrice {get;set;}
        
        public wrapCli(ContractLineItem cli) {
            this.ProductName = cli.Product2.Name;
            this.UnitPrice = arrondi(cli.UnitPrice);
            this.Quantity = cli.Quantity;
            this.Subtotal = arrondi(cli.Subtotal);
            this.Discount = cli.Discount;
            this.TotalPrice = arrondi(cli.TotalPrice);
        }
    }
}