/**
 * @File Name          : VFC74_MassAvisDeRelancePDF.cls
 * @Description        : Génération PDF en masse de plusieurs avis de relance contrat
 * @Author             : RRA
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 26/08/2020, 12:25:00
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    26/08/2020, 12:25:00   RRA     Initial Version */
public with sharing class VFC74_MassAvisDeRelancePDF {
    public List<ServiceContract> lstServiceContract {get;set;}
    public List<ContractLineItem> lstContractLineItem {get;set;}
    public ServiceContract actualServCon {get;set;}
    public ContractLineItem actualConLineItem {get;set;}
    public String scAddress {get; set;}
    public String statut {get; set;}
    public String Telproprietaire {get; set;}
    public Set<Id> setProdId {get; set;}
    public List<Product_Bundle__c> lstBundleProd {get; set;}
    public Map<Id, ContractLineItem> mapProdCli {get; set;}
    public Boolean RemiseBln {get; set;}

    public VFC74_MassAvisDeRelancePDF(){
        System.debug('##Starting VFC72_ServiceContract');

        String scIds = ApexPages.currentPage().getParameters().get('lstIds');
        System.debug('## scIds:' + scIds);
        List<Id> lstScIds = (List<Id>)JSON.deserialize(scIds, List<Id>.class);
        System.debug('## lstScIds:' + lstScIds);
            if(lstScIds.size() > 0){
                    lstServiceContract = new List<ServiceContract>([SELECT Id,
                                                    Agency__r.Phone__c,
                                                    Agency__r.Libelle_horaires__c,
                                                    Payeur_du_contrat__r.salutation,
                                                    Payeur_du_contrat__r.lastname,
                                                    Payeur_du_contrat__r.FirstName,
                                                    Payeur_du_contrat__r.Imm_Res__c,
                                                    Payeur_du_contrat__r.Door__c,
                                                    Payeur_du_contrat__r.Floor__c,
                                                    Payeur_du_contrat__r.BillingStreet,
                                                    Payeur_du_contrat__r.Adress_Complement__c,
                                                    Payeur_du_contrat__r.BillingPostalCode,
                                                    Payeur_du_contrat__r.BillingCity,
                                                    Payeur_du_contrat__r.ClientNumber__c,
                                                    Payeur_du_contrat__r.RecordType.Name,
                                                    Payeur_du_contrat__r.PersonHomePhone,
                                                    Payeur_du_contrat__r.Phone,
                                                    Payeur_du_contrat__r.PersonMobilePhone,
                                                    Payeur_du_contrat__r.PersonEmail,
                                                    Payeur_du_contrat__r.BusinessAccountEmail__c,
                                                    Payeur_du_contrat__r.PersonOptInEMailSMSCompany__pc,
                                                    Numero_du_contrat__c,
                                                    TECH_Date_d_edition__c,
                                                    StartDate,
                                                    EndDate,
                                                    GrandTotal,
                                                    Agency__r.Site_web__c,
                                                    TotalPrice,
                                                    Tax__c,
                                                    Agency__r.IBAN__c,
                                                    Agency__r.Capital_in_Eur__c,
                                                    Agency__r.name,
                                                    Agency__r.Address,
                                                    Agency__r.street,
                                                    Agency__r.Street2__c,
                                                    Agency__r.PostalCode,
                                                    Agency__r.City,
                                                    Agency__r.Email__c,
                                                    Agency__r.Legal_Form__c,
                                                    Agency__r.TECH_CapitalInEur__c,
                                                    Agency__r.Siren__c,
                                                    Agency__r.SIRET__c,
                                                    Agency__r.APE__c,
                                                    Agency__r.Intra_Community_VAT__c,
                                                    Agency__r.Logo_Name_Agency__c,
                                                    Asset__r.Equipment_type_auto__c,
                                                    Asset__r.Brand_auto__c,
                                                    Asset__r.Equipment_type__c,
                                                    Asset__r.Model_auto__c,
                                                    Asset__r.SerialNumber,
                                                    Asset__r.Date_de_mise_en_service__c,
                                                    Asset__r.Manufacturer_s_end_of_warranty_auto__c,
                                                    Agency__r.Telephone_d_astreinte__c,
                                                    Inhabitant__c,
                                                    Home_Owner__c,
                                                    Home_Owner__r.Name,
                                                    Home_Owner__r.PersonMobilePhone,
                                                    Home_Owner__r.BillingStreet,
                                                    Home_Owner__r.BillingPostalCode,
                                                    Home_Owner__r.BillingCity,
                                                    Home_Owner__r.BillingCountry,
                                                    Inhabitant__r.Name,
                                                    Inhabitant__r.BillingStreet,
                                                    Inhabitant__r.BillingPostalCode,
                                                    Inhabitant__r.BillingCity,
                                                    Inhabitant__r.PersonMobilePhone,
                                                    Payeur_du_contrat__c,
                                                    logement__r.Legal_Guardian__c,
                                                    logement__r.Legal_Guardian__r.Name, 
                                                    logement__r.Legal_Guardian__r.BillingStreet,
                                                    logement__r.Legal_Guardian__r.BillingPostalCode,
                                                    logement__r.Legal_Guardian__r.BillingCity,
                                                    logement__r.Legal_Guardian__r.BillingCountry,
                                                    logement__r.Legal_Guardian__r.PersonMobilePhone,
                                                    Logement__r.City__c,
                                                    Tax,
                                                    Subtotal,
                                                    Discount,
                                                    TECH_Date_Edition_R1__c,
                                                    Date_de_signature_client__c,
                                                    Signature_Hash_client__c, 
                                                    (SELECT Id, Product2.Name, Asset.Equipment_type_auto__c, IsBundle__c, tech_nom_produit__c, TECH_nombre_d_interventions__c, Product2Id, Product2.Type_de_garantie__c, UnitPrice, Discount, TotalPrice FROM ContractLineItems WHERE IsBundle__c = true)
                                                    FROM ServiceContract 
                                                    WHERE Id = :lstScIds]);
                }

                lstContractLineItem = new List<ContractLineItem>([SELECT Id, IsBundle__c, Product2Id, Subtotal, tech_nom_produit__c, ServiceContractId, TECH_DoNotPopulateBundle__c,
                                                    Product2.Type_de_garantie__c, UnitPrice, Discount, TotalPrice, IsOptionPieces__c FROM ContractLineItem 
                                                    WHERE ServiceContractId IN :lstServiceContract]);
                System.debug('lstContractLineItem: ' + lstContractLineItem);
                
                setProdId = new set<Id>();
                mapProdCli = new Map<Id, ContractLineItem>();
                list<decimal> lstRemise = new list<decimal>();
                RemiseBln = false;

                
                if(lstContractLineItem.size() > 0){
                    for(ContractLineItem cli : lstContractLineItem){
                        if(cli.IsBundle__c == true){
                            setProdId.add(cli.Product2Id);
                            mapProdCli.put(cli.Product2Id, cli);
                        }
                        if(cli.Discount != null){
                            lstRemise.add(cli.Discount);
                        }
                        // if(cli.TotalPrice != null){
                        //     lstRemise.add(cli.TotalPrice);
                        // }
                    }
                    
                    if(lstRemise.size() > 0){
                        RemiseBln = true;
                    }

                    lstBundleProd = new List<Product_Bundle__c>([SELECT Id, Optional_Item__c, Product__c, Product__r.Name, Bundle__r.Name FROM Product_Bundle__c WHERE Optional_Item__c = true AND Product__c IN :setProdId]);
                    System.debug('lstBundleProd' + lstBundleProd);
                }

                if(lstServiceContract.size()>0){
                    actualServCon=lstServiceContract[0];


                    if(actualServCon.Payeur_du_contrat__r.Imm_Res__c != null){
                        scAddress = 'Bât.: '+actualServCon.Payeur_du_contrat__r.Imm_Res__c ;
                    }
                    
                    if(actualServCon.Payeur_du_contrat__r.Imm_Res__c != null || actualServCon.Payeur_du_contrat__r.Door__c != null){
                        scAddress = scAddress + ',';
                    }
                    if(actualServCon.Payeur_du_contrat__r.Floor__c != null ){
                        scAddress = scAddress + ' Etage: ' + actualServCon.Payeur_du_contrat__r.Floor__c + ' ';
                    }
                    if(actualServCon.Payeur_du_contrat__r.Door__c != null){
                        scAddress = scAddress + 'Porte: ' + ' ' + actualServCon.Payeur_du_contrat__r.Door__c;
                    }



                    //Statut for VFP73
                    if(actualServCon.Payeur_du_contrat__c == actualServCon.Home_Owner__c && actualServCon.Home_Owner__c == actualServCon.Inhabitant__c){
                        statut = 'propriétaire occupant';
                    }
                    if(actualServCon.Payeur_du_contrat__c == actualServCon.Home_Owner__c && actualServCon.Payeur_du_contrat__c != actualServCon.Inhabitant__c){
                        statut = 'propriétaire non occupant';
                    }
                    if(actualServCon.Payeur_du_contrat__c == actualServCon.Inhabitant__c && actualServCon.Payeur_du_contrat__c != actualServCon.Home_Owner__c){
                        statut = 'Occupant';
                    }
                    if(actualServCon.Payeur_du_contrat__c != actualServCon.Inhabitant__c && actualServCon.Payeur_du_contrat__c != actualServCon.Home_Owner__c){
                        statut = '';
                    }

                    //Legal Guardian null or not null
                    if(actualServCon.logement__r.Legal_Guardian__c != null){
                        // Champlibre = actualServCon.logement__r.Legal_Guardian__r.Name + ' ' + actualServCon.logement__r.Legal_Guardian__r.BillingStreet + ' ' + actualServCon.logement__r.Legal_Guardian__r.BillingPostalCode + ' ' + actualServCon.logement__r.Legal_Guardian__r.BillingCity + ' ' + actualServCon.logement__r.Legal_Guardian__r.BillingCountry;
                        Telproprietaire = actualServCon.logement__r.Legal_Guardian__r.PersonMobilePhone;
                    }
                    if(actualServCon.logement__r.Legal_Guardian__c == null){
                        // Champlibre = actualServCon.Home_Owner__r.Name + ' ' + actualServCon.Home_Owner__r.BillingStreet + ' ' + actualServCon.Home_Owner__r.BillingPostalCode + ' ' + actualServCon.Home_Owner__r.BillingCity + ' ' + actualServCon.Home_Owner__r.BillingCountry;
                        Telproprietaire = actualServCon.Home_Owner__r.PersonMobilePhone;
                    }
                }

                if(actualServCon.ContractLineItems.size() > 0){
                    actualConLineItem = actualServCon.ContractLineItems[0];
                }

                System.debug('actualServCon' + actualServCon.Payeur_du_contrat__r.RecordType.Name);
    }

}