public with sharing class SSA_FacturesUtils {
    
    // Recherche d'un indice de révision pour une liste de facture
    public static void findIndex (Set<Id> p_factureIds) {
        findIndex (p_factureIds, null);
    }
    // Recherche d'un indice de révision pour une liste de facture, 
    // excluant un ensemble de valeurs d'indice
    public static void findIndex (Set<Id> p_factureIds, Set<Id> p_excludedValues) {
        List<sofactoapp__Factures_Client__c> lst_invoiceForIndexCalculation = 
            [SELECT Id, sofactoapp__Date_indice__c, Indice1_ID__c, 
            sofactoapp__Indice_date_de_facture__c, sofactoapp__Coefficient_indice_date__c
            FROM sofactoapp__Factures_Client__c
            WHERE Id IN :p_factureIds
            AND sofactoapp__IsLocked__c = false
            AND Indice1_ID__c != null
            AND Sofactoapp_Contrat_de_service__r.Indice_1__r.sofactoapp__Actif__c  = true];

        Date lv_startDateForIndexComputing = null;
        Set<Id> set_indexeIds = new Set<Id>();

        for (sofactoapp__Factures_Client__c lv_invoice : lst_invoiceForIndexCalculation) {
            set_indexeIds.add(lv_invoice.Indice1_ID__c);
            if (lv_startDateForIndexComputing == null || 
                    lv_invoice.sofactoapp__Date_indice__c < lv_startDateForIndexComputing) 
            {
                lv_startDateForIndexComputing = lv_invoice.sofactoapp__Date_indice__c;
            }
        }
        
        findIndex(lst_invoiceForIndexCalculation, set_indexeIds, lv_startDateForIndexComputing, true, p_excludedValues);
    }

    // Recherche d'un indice de révision pour une liste de facture
    // Pas de transaction base de donnée car méthode appelée en pré-insert/update
    public static void findIndex (List<sofactoapp__Factures_Client__c> p_factures) {
        
        Date lv_startDateForIndexComputing = null;
        Set<Id> set_indexeIds = new Set<Id>();

        for (sofactoapp__Factures_Client__c lv_invoice : p_factures) {
            set_indexeIds.add(lv_invoice.Indice1_ID__c);
            if (lv_startDateForIndexComputing == null || 
                    lv_invoice.sofactoapp__Date_indice__c < lv_startDateForIndexComputing) 
            {
                lv_startDateForIndexComputing = lv_invoice.sofactoapp__Date_indice__c;
            }
        }
        
        findIndex(p_factures, set_indexeIds, lv_startDateForIndexComputing, false);
    }

    // Recherche d'un indice de révision pour une liste de facture
    public static void findIndex (List<sofactoapp__Factures_Client__c> p_factures, Set<Id> p_indexeIds, Date p_startDateForIndexComputing, Boolean p_commit) {
        findIndex (p_factures, p_indexeIds, p_startDateForIndexComputing, p_commit, null);
    }
    public static void findIndex (List<sofactoapp__Factures_Client__c> p_factures, Set<Id> p_indexeIds, Date p_startDateForIndexComputing, Boolean p_commit, Set<Id> p_excludedValues) {

        if (p_startDateForIndexComputing != null && p_factures != null) {
            
            Map<Id, List<sofactoapp__Valeur_indice__c>> map_indexes = new Map<Id, List<sofactoapp__Valeur_indice__c>>();
            for (sofactoapp__Valeur_indice__c lv_value : 
                    [SELECT Id, sofactoapp__Debut__c, sofactoapp__Coefficient__c, sofactoapp__Indice__c
                    FROM sofactoapp__Valeur_indice__c
                    WHERE sofactoapp__Debut__c <= :p_startDateForIndexComputing
                    AND IsDeleted = false
                    AND Id NOT IN :p_excludedValues
                    AND sofactoapp__Indice__c IN :p_indexeIds
                    AND sofactoapp__Indice__r.sofactoapp__Actif__c = true
                    ORDER BY sofactoapp__Debut__c DESC]) 
            {
                if (map_indexes.get(lv_value.sofactoapp__Indice__c) == null) {
                    map_indexes.put(lv_value.sofactoapp__Indice__c, new List<sofactoapp__Valeur_indice__c>());    
                    map_indexes.get(lv_value.sofactoapp__Indice__c).add(lv_value);
                }
            }

            for (sofactoapp__Valeur_indice__c lv_value : 
                    [SELECT Id, sofactoapp__Debut__c, sofactoapp__Coefficient__c , sofactoapp__Indice__c
                    FROM sofactoapp__Valeur_indice__c
                    WHERE sofactoapp__Debut__c > :p_startDateForIndexComputing
                    AND IsDeleted = false
                    AND Id NOT IN :p_excludedValues
                    AND sofactoapp__Indice__c IN :p_indexeIds
                    AND sofactoapp__Indice__r.sofactoapp__Actif__c = true
                    ORDER BY sofactoapp__Debut__c ASC]) 
            {
                if (map_indexes.get(lv_value.sofactoapp__Indice__c) == null) {
                    map_indexes.put(lv_value.sofactoapp__Indice__c, new List<sofactoapp__Valeur_indice__c>());
                }
                map_indexes.get(lv_value.sofactoapp__Indice__c).add(lv_value);
            }

            // Enfin, on détermine le taux applicable pour chaque facture
            if (map_indexes.isEmpty() == false) {
                for (sofactoapp__Factures_client__c lv_invoice : p_factures) {

                    lv_invoice.sofactoapp__Indice_date_de_facture__c = null;
                    lv_invoice.sofactoapp__Coefficient_indice_date__c = null;

                    if (map_indexes.get(lv_invoice.Indice1_ID__c) != null) {

                        Date lv_indexDate = null;
                        for (sofactoapp__Valeur_indice__c lv_value : map_indexes.get(lv_invoice.Indice1_ID__c)) {
                            if (lv_value.sofactoapp__Debut__c <= lv_invoice.sofactoapp__Date_indice__c) {
                                lv_invoice.sofactoapp__Indice_date_de_facture__c = lv_value.Id;
                                lv_invoice.sofactoapp__Coefficient_indice_date__c = lv_value.sofactoapp__Coefficient__c;
                            } else {
                                break;
                            }
                        }
                    }
                }
            }

            if (p_commit) {
                List<Database.SaveResult> map_results = Database.update(p_factures, false);
                for (Integer i=0; i<map_results.size(); i++) {
                    Database.SaveResult lv_result = map_results.get(i);
                    if (!lv_result.isSuccess()) {
                        // Operation failed, so get all errors 
                        String lv_msg = 'FacturesClientUtils[940] Error while updating unit price for invoice line item ';
                        lv_msg += p_factures.get(i).Name;
                        lv_msg += ' : '; 
                        for (Database.Error lv_err : lv_result.getErrors()) {
                            lv_msg += '\n';
                            lv_msg += lv_err.getMessage();
                        }
                        System.debug(lv_msg);
                    }
                }
            }
        }
    }    

}