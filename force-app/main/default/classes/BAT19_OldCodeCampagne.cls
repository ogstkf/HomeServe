/**
 * @File Name         : BAT19_OldCodeCampagne
 * @Description       : 
 * @Author            : Spoon Consutling (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 27-09-2021
 * Modifications Log 
 * ==========================================================
 * Ver   Date               Author          Modification
 * 1.0   21-09-2021         MNA             Initial Version
**/
global with sharing class BAT19_OldCodeCampagne implements Database.Batchable <sObject>, Database.Stateful, Database.AllowsCallouts, Schedulable {
    public String query;
    public String bypassId;

    private Set<Id> setAccId = new Set<Id>();
    private Set<String> setCampCode = new Set<String>();

    private Map<String,String> mapCpgCodeByAccId = new Map<String,String>();
    private Map<String,Campaign> mapOldCpgByCpgCode = new Map<String,Campaign>();

    global Database.QueryLocator start(Database.BatchableContext BC) {
        Id UsrId = UserInfo.getUserId();
        List<Bypass__c> lstBp = [SELECT Id, BypassValidationRules__c FROM Bypass__c WHERE SetupOwnerId =: UsrId LIMIT 1];

        if (lstBp == null || lstBp.size() == 0 ) {
            Database.SaveResult sr = Database.insert(new Bypass__c(SetupOwnerId = UsrId, BypassValidationRules__c = true));
            bypassId = sr.getId();
        }
        else {
            if (!lstBp[0].BypassValidationRules__c) {
                lstBp[0].BypassValidationRules__c = true;
                update lstBp;
                bypassId = lstBp[0].Id;
            }
        }

        Date today = System.today();
        query = 'SELECT Id, Campagne__c, Campagne__r.Code__c FROM Account WHERE Date_fin_retombees__c < :today AND (NOT(Campagne__r.Code__c LIKE \'OLD%\'))';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> lstAcc) {
        List<Account> lstAccToUpdt = new List<Account>();
        for (Account acc: lstAcc) {
            setCampCode.add('OLD.' + acc.Campagne__r.Code__c);
        }
        for (Campaign cpg : [SELECT Id, Code__c, EndDate FROM Campaign WHERE Code__c IN :setCampCode AND IsActive = true]) {
            mapOldCpgByCpgCode.put(cpg.Code__c.split('OLD.')[1], cpg);
        }

        for (Account acc : lstAcc) {
            if (mapOldCpgByCpgCode.containsKey(acc.Campagne__r.Code__c)) {
                Campaign cpg = mapOldCpgByCpgCode.get(acc.Campagne__r.Code__c);
                acc.Campagne__c = cpg.Id;
                acc.Date_d_affectation__c = System.today();
                acc.Date_fin_retombees__c = cpg.EndDate;
                lstAccToUpdt.add(acc);
            }
        }

        if (lstAccToUpdt.size() > 0)
            update lstAccToUpdt;
    }

    global void finish(Database.BatchableContext BC) {
        if (bypassId != null) {
            Bypass__c bp = [SELECT Id, BypassValidationRules__c FROM Bypass__c WHERE Id =: bypassId];
            bp.BypassValidationRules__c =false;
            update bp;
        }
             
    }

    global void execute(SchedulableContext sc) {
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT19_OldCodeCampagne');
        if(batchConfig != null && batchConfig.BatchSize__c != null) {
            Database.executeBatch(new BAT19_OldCodeCampagne(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT19_OldCodeCampagne());
        }
    }
}