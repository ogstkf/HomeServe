/**
 * @File Name          : AP22_HandleProductDelivery.cls
 * @Description        : 
 * @Author             : Spoon Consulting (LGO)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         16-10-2019     		LGO         Initial Version (CT-1115)
**/
public with sharing class AP22_HandleProductDelivery {
    public static void checkProductTransfer(List<Shipment> lstShip){
   


        list<Order> lstupdOrder = new list<Order>();        
        Map<Id, List<ProductTransfer> > mapIdShipPrdTrans = new Map<Id, List<ProductTransfer>>();
        Set<Id> lstShipment = new Set<Id>(); 

        for(Shipment s: [SELECT Id,
                                (SELECT Id,ShipmentId,QuantityReceived,QuantitySent
                                FROM ProductTransfers
                                 )
                        FROM Shipment
                        WHERE Id IN :lstShip]){

            
            List<ProductTransfer> lstPrdTrans = new List<ProductTransfer>();

            for(ProductTransfer ProT : s.ProductTransfers){
                //for each shipment, check if qtyrec === qtysent
                if(ProT.QuantityReceived == ProT.QuantitySent){
                    lstPrdTrans.add(ProT);
                }
            }
            //verify if size of listProT is same when equal qty, then update order
            if(lstPrdTrans.size()==s.ProductTransfers.size()){
                lstShipment.add(s.Id);
            }

        }

        system.debug('### toUpdateShipList '+lstShipment);


        //Get all order to update for the shipment
        for (Order ord: [SELECT Id,
                                Date_de_livraison_reelle__c, 
                                Status ,
                                (SELECT Id, Date_du_bon_de_livraison__c
                                FROM Reception__r
                                WHERE Id IN :lstShipment
                                 )
                        FROM Order
                        ])
        {
            //update Order if shipment exist
             if(ord.Reception__r.size() > 0){                 
                    Order newOrd = new Order (Id = ord.Id, Date_de_livraison_reelle__c = ord.Reception__r[0].Date_du_bon_de_livraison__c ,Status = AP_Constant.OrdStatusSolde);                    
                 	System.debug('#####in update order');
                 	lstupdOrder.add(newOrd);
             }
        
        }
		System.debug('#####lstupdOrder.size()'+lstupdOrder.size());
        if(lstupdOrder.size() >0 ){
            update lstupdOrder;
        }
    }
}