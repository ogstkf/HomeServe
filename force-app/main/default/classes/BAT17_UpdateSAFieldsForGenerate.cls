/**
 * @File Name         : 
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 09-11-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   03-11-2021   MNA   Initial Version
**/
public without sharing class BAT17_UpdateSAFieldsForGenerate implements Database.Batchable <sObject>, Database.Stateful, Database.AllowsCallouts {
    String query;
    List<Id> lstSAId;

    public BAT17_UpdateSAFieldsForGenerate(List<Id> lstSAId) {
        this.lstSAId = lstSAId;
    }
    public Database.QueryLocator start(Database.BatchableContext BC) {
        query = 'SELECT Id, Avis_de_passage_en_cours_de_g_n_ration__c '
                + ' FROM ServiceAppointment'
                + ' WHERE Id IN: lstSAId';

        System.debug('####query: ' + query);
        System.debug('###lstSAId: ' + lstSAId);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<ServiceAppointment> lstSA) {
        List<ServiceAppointment> lstSaToUpd = new List<ServiceAppointment>();
        for (ServiceAppointment SA : lstSA) {
            SA.Avis_de_passage_en_cours_de_g_n_ration__c = true;
            lstSaToUpd.add(SA);
        }
        if (lstSaToUpd.size() > 0)
            update lstSaToUpd;
    }

    public void finish(Database.BatchableContext BC) {
    }
}