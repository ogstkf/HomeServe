/**
 * @File Name         : BAT16_WSExternalApi_remise
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 20-09-2021
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   20-09-2021   MNA   Initial Version
**/
global with sharing class BAT16_WSExternalApi_remise extends BAT16_WSExternalApi {
    global BAT16_WSExternalApi_remise(String queryCondition) {
        this.queryCondition = ' Id IN (' + queryCondition + ') AND Type_Ecriture__c = \'liste_remises\' ';
    }

    global BAT16_WSExternalApi_remise() {

    }

    global override void manualExecution(Integer batchSize) {
        List<String> lstQueryCondition = this.join(batchSize);
        
        for (String queryCondition: lstQueryCondition) {
            Database.executeBatch(new BAT16_WSExternalApi_remise(queryCondition), batchSize);
        }
    }
}