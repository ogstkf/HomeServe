/*******************************************************
-- - Author        : Spoon Consulting
-- - Description   : Handler for the MesuresEquipementsSecondaires triggers
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  ------------------------
-- 24-03-2021  LGO    1.0     Initial version
-------------------------------------------------------
*******************************************************/
public class MesuresEquipementSecondTriggerHandler {
    Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());

	public void handleAfterInsert(List<Mesures_quipements_secondaires__c> lstMESNew) {
		System.debug('### handleAfterInsert MesuresEquipementsSecondaires - START');
		
		// AP78_UpdtContractLineItem ap78_manager = new AP78_UpdtContractLineItem();		

        // for(integer i=0;i<lstMESNew.size();i++){
        //     if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP78')){
        //         //if Référence mesure is true, send related Service Appointment for calc of Contract line items
        //         if(lstMESNew[i].R_f_rence_mesure__c){
        //                 ap78_manager.setSAIds.add(lstMESNew[i].Rendez_vous_de_service__c);		        
        //         }
        //     }
    	// }

    	// if(!ap78_manager.setSAIds.isEmpty()) ap78_manager.calculateCLI();   

    }
    public void handleAfterUpdate(List<Mesures_quipements_secondaires__c> lstMESNew, List<Mesures_quipements_secondaires__c> lstMESOld) {
        System.debug('### handleAfterUpdate MesuresEquipementsSecondaires - START');

        // AP78_UpdtContractLineItem ap78_manager = new AP78_UpdtContractLineItem();
        
        // for(integer i=0;i<lstMESNew.size();i++){
        //     if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP78')){
        //         //if Référence mesure is true, send related Service Appointment for calc of Contract line items
        //         if(lstMESNew[i].R_f_rence_mesure__c
        //         && lstMESNew[i].R_f_rence_mesure__c <> lstMESOld[i].R_f_rence_mesure__c){
        //                 ap78_manager.setSAIds.add(lstMESNew[i].Rendez_vous_de_service__c);		        
        //         }
        //     }
    	// }

    	// if(!ap78_manager.setSAIds.isEmpty()) ap78_manager.calculateCLI();   

    }
}