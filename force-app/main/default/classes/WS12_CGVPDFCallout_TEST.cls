@isTest
public with sharing class WS12_CGVPDFCallout_TEST {

/**
 * @File Name          : WS12_CGVPDFCallout_TEST.cls
 * @Description        : 
 * @Author             : Spoon Consulting (YGO)
 * @Group              : 
 * @Last Modified By   : YGO
 * @Last Modified On   : 24-01-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         24-01-2019     		    YGO         Initial Version
**/
    static User mainUser;

    static{
        mainUser = TestFactory.createAdminUser('WS12_CGVPDFCallout_TEST@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;
    }

    @isTest
    public static void getPdfContent_testGaz(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, String> mapRes = WS12_CGVPDFCallout.getPdfContent('GAZ','devis');
                System.debug('res: '+mapRes);
            Test.stopTest();

            System.assertEquals(mapRes.get('Status:'), 'SUCCESS');
        }
    }

    @isTest
    public static void getPdfContent_testPAC(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, String> mapRes = WS12_CGVPDFCallout.getPdfContent('PAC','contrat');
            Test.stopTest();

            System.assertEquals(mapRes.get('Status:'), 'SUCCESS');

        }
    }

    @isTest
    public static void getPdfContent_testFIOUL(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, String> mapRes = WS12_CGVPDFCallout.getPdfContent('FIOUL','contrat');
            Test.stopTest();

            System.assertEquals(mapRes.get('Status:'), 'SUCCESS');

        }
    }

    @isTest
    public static void getPdfContent_testBois(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, String> mapRes = WS12_CGVPDFCallout.getPdfContent('BOIS','contrat');
            Test.stopTest();

            System.assertEquals(mapRes.get('Status:'), 'SUCCESS');

        }
    }

    @isTest
    public static void getPdfContent_testSolaire(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, String> mapRes = WS12_CGVPDFCallout.getPdfContent('Solaire','contrat');
            Test.stopTest();

            System.assertEquals(mapRes.get('Status:'), 'SUCCESS');

        }
    }

    @isTest
    public static void getPdfContent_testDepannage(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, String> mapRes = WS12_CGVPDFCallout.getPdfContent('DEPANNAGE','contrat');
            Test.stopTest();

            System.assertEquals(mapRes.get('Status:'), 'SUCCESS');

        }
    }

    @isTest
    public static void getPdfContent_testVisite(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, String> mapRes = WS12_CGVPDFCallout.getPdfContent('Visite','contrat');
            Test.stopTest();

            System.assertEquals(mapRes.get('Status:'), 'SUCCESS');

        }
    }

    @isTest
    public static void getPdfContent_testElse(){
        System.runAs(mainUser){
            Test.startTest();
                Map<String, String> mapRes = WS12_CGVPDFCallout.getPdfContent('devis','devis');
            Test.stopTest();

            System.assertEquals(mapRes.get('Status:'), 'SUCCESS');

        }
    }

}