/**
 * @File Name          : OpportunityTriggerHandler.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 5/14/2020, 1:35:30 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0   14-JUIN-2019  DMU     Initial version
 * 1.1   17-OCT-2019   LGO     AP24_QuoteSyncedOpportunity
 * 1.1   11/10/2019    AMO     
 * 1.2   21-JUL-2020   DMU     TEC-64 L'assignation code campagne
**/
public with sharing class OpportunityTriggerHandler {
    Bypass__c userBypass;
	private static String standardRecTypeId = AP_Constant.getRecTypeId('Opportunity', 'Opportunite_standard');

    public OpportunityTriggerHandler(){
        userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    }


	public void handleBeforeInsert(List<Opportunity> lstNewOpp){
		System.debug('OpportunityTriggerHandler handleBeforeInsert');

		List <Opportunity> lstOppSetCampagne = new list <Opportunity>(); //DMU 20200721 - TEC-64 L'assignation code campagne
		Date today = Date.newInstance(system.today().year(), system.today().month(), system.today().day());

		for(integer i=0;i<lstNewOpp.size();i++){
			if(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP27')){
				if(lstNewOpp[i].AccountId <> null && lstNewOpp[i].TECH_AccountCampagne__c <> null &&  lstNewOpp[i].TECH_AccountDateFinRetombee__c > today){
					lstOppSetCampagne.add(lstNewOpp[i]);
				}
			}
		}

		//set campagne on opp
		if(lstOppSetCampagne.size()>0){
			AP27_OpportunityQLIs.setCampagne(lstOppSetCampagne);
		}
	}


	public void handleAfterUpdate(List<Opportunity> lstOldOpp, List<Opportunity> lstNewOpp){
		System.debug('OpportunityTriggerHandler handleAfterUpdate');

		// List<ServiceAppointment> lstServAppToInsert = new List<ServiceAppointment>();
		// map<string, ServiceAppointment> mapOppServAppExisting = new map <string,ServiceAppointment>();

		set<Id> setQuoId = new set<Id>();
		set<Id> setOppAP27 = new set<Id>();
		List<Opportunity> lstOppNew = new List<Opportunity>();

		// List<Id> lstOppIdMyChauffage = new List<Id>();
		// for(Integer i=0;i<lstNewOpp.size();i++){
		// 	if( lstNewOpp[i].StageName != lstOldOpp[i].StageName 
		// 		&& lstNewOpp[i].StageName == AP_Constant.oppStageClosedWon 
		// 		&& lstNewOpp[i].TECH_AccountSource__c == AP_Constant.accSourceSiteMyChauffage
		// 		&& lstNewOpp[i].RecordTypeId == standardRecTypeId //RRJ 20200514 CT-1710
		// 	){
		// 		lstOppIdMyChauffage.add(lstNewOpp[i].Id);
		// 	}
		// }

		// if(lstOppIdMyChauffage.size()>0){
		// 	for(ServiceAppointment servAppExisting : [select id, Opportunity__c, EarliestStartTime, DueDate, Status, ContactId, Residence__c, ParentRecordId, ServiceTerritoryId from ServiceAppointment where Opportunity__c in: lstOppIdMyChauffage]){
		// 		mapOppServAppExisting.put(servAppExisting.Opportunity__c, servAppExisting);
		// 	}
		// }

		for(Integer i=0;i<lstNewOpp.size();i++){

			//DMU - Commented SA creation for Mychauffage due to homeserve project
			/*
			if(lstNewOpp[i].StageName != lstOldOpp[i].StageName 
				&& lstNewOpp[i].StageName == AP_Constant.oppStageClosedWon 
				&& lstNewOpp[i].TECH_AccountSource__c == AP_Constant.accSourceSiteMyChauffage
				&& lstNewOpp[i].RecordTypeId == standardRecTypeId //RRJ 20200514 CT-1710
			){

				
			   	if(mapOppServAppExisting.containskey(lstNewOpp[i].Id)){

			   		for(Integer j=0; j<2; j++){
			   			lstServAppToInsert.add(new ServiceAppointment(Opportunity__c = lstNewOpp[i].Id
			   			,EarliestStartTime = mapOppServAppExisting.get(lstNewOpp[i].Id).EarliestStartTime
			   			,DueDate = mapOppServAppExisting.get(lstNewOpp[i].Id).DueDate			   			
			   			,Status = AP_Constant.servAppStatusScheduled
			   			,ContactId = mapOppServAppExisting.get(lstNewOpp[i].Id).ContactId
			   			,Residence__c = mapOppServAppExisting.get(lstNewOpp[i].Id).Residence__c
			   			,ParentRecordId = mapOppServAppExisting.get(lstNewOpp[i].Id).ParentRecordId
			   			,ServiceTerritoryId = mapOppServAppExisting.get(lstNewOpp[i].Id).ServiceTerritoryId
			   			));
			   		}

			   		System.debug('** lstServAppToInsert: '+ lstServAppToInsert.size());

			   		lstServAppToInsert[0].Category__c = AP_Constant.ServAppCategoryPose;
			   		lstServAppToInsert[1].Category__c = AP_Constant.ServAppCategoryMisEnServ;   		

			   	}else{
			   		for(Integer j=0; j<2; j++){
			   			lstServAppToInsert.add(new ServiceAppointment(Opportunity__c = lstNewOpp[i].Id, EarliestStartTime = System.now(), DueDate = System.now()+14, Status = AP_Constant.servAppStatusScheduled));
			   		}
			   		lstServAppToInsert[0].Category__c = AP_Constant.ServAppCategoryPose;
			   		lstServAppToInsert[1].Category__c = AP_Constant.ServAppCategoryMisEnServ;
				   }		
				      

			}
			*/

			if(lstNewOpp[i].Ordre_d_execution__c != lstOldOpp[i].Ordre_d_execution__c
				&& lstNewOpp[i].RecordTypeId == standardRecTypeId //RRJ 20200514 CT-1710
			){
				lstOppNew.add(lstNewOpp[i]);
			}

			//LGO- 17.10.19 - if tech devis signe is true then update the devis
			if(lstNewOpp[i].Tech_Devis_sign_par_le_client__c != lstOldOpp[i].Tech_Devis_sign_par_le_client__c 
					&& lstNewOpp[i].Tech_Devis_sign_par_le_client__c
					&& lstNewOpp[i].Tech_Synced_Devis__c != null
					&& (userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP24'))
					&& lstNewOpp[i].RecordTypeId == standardRecTypeId //RRJ 20200514 CT-1710
				){
						setQuoId.add(lstNewOpp[i].Tech_Synced_Devis__c);
            			
        	}

			//KZE- 18.10.19 - if field changes on opportunity, create quote, create qli, delete existing
			if(	lstNewOpp[i].Counter__c != lstOldOpp[i].Counter__c && 
				(userBypass.BypassTrigger__c == null || !userBypass.BypassTrigger__c.contains('AP27'))
				&& lstNewOpp[i].RecordTypeId == standardRecTypeId //RRJ 20200514 CT-1710	
			){
				setOppAP27.add(lstNewOpp[i].Id);
            			
        	}
		}		

		//DMU - Commented SA creation for Mychauffage due to homeserve project
		/*
		if(!lstServAppToInsert.isEmpty()){
			try{
				insert lstServAppToInsert;
			}catch(exception e){
				System.debug('insert fail: '+ e.getMessage());
			}
		}
		*/

			//aaly call ap class here

		if(!setQuoId.isEmpty())
			System.debug('** setQuoId: '+ setQuoId.size());
			// AP24_QuoteSyncedOpportunity.updateOppDevis(setQuoId);

		if(setOppAP27.size()> 0 && !AP27_OpportunityQLIs.hasRunAp27){
			System.debug('>> setOppAP27.size(): '+ setOppAP27.size());
			AP27_OpportunityQLIs.createQLIs(setOppAP27);
		}

		if(lstOppNew.size()>0){
			AP32_UpdtWorkOrderInQuote.updateOpportunity(lstOppNew);
		}		
	}
}