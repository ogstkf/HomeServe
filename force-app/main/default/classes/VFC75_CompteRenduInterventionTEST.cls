/**
 * @description       : 
 * @author            : AMO
 * @group             : 
 * @last modified on  : 09-09-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-17-2020   AMO   Initial Version
**/
@isTest
public with sharing class VFC75_CompteRenduInterventionTEST {
    static User mainUser;
    static Account testAcc = new Account();
    static List<ServiceAppointment> lstServiceApp;
    static ServiceAppointment actualSA;
    static ServiceContract servCon = new ServiceContract();
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<Asset> lstTestAssset = new List<Asset>();
    static List<Product2> lstTestProd = new List<Product2>();
    static Bypass__c bp = new Bypass__c();
    
    static{
        
        mainUser = TestFactory.createAdminUser('AP36@test.COM', TestFactory.getProfileAdminId());
        insert mainUser;

        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassValidationRules__c = True;
        insert bp;


        System.runAs(mainUser){
            
            //create account
            testAcc = TestFactory.createAccount('Test1');
            testAcc.BillingPostalCode = '1233456';
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            testAcc.firstname = 'm';
            insert testAcc;

            //create sofactoapp__Compte_auxiliaire__c
            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;

            
            Logement__c lo = TestFactory.createLogement('Lo_Test', 'test01.sc@mauritius.com', 'test_lname');
            lo.Blue_Order_Id__c = '9711';             
            insert lo;

            //creating products
            lstTestProd.add(TestFactory.createProduct('Ramonage gaz'));
            lstTestProd.add(TestFactory.createProduct('Entretien gaz'));
            
            insert lstTestProd;

            // creating Assets
            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lo.Id);
            eqp.Product2Id = lstTestProd[0].Id;
            eqp.Logement__c = lo.Id;
            eqp.AccountId = testAcc.Id;
            lstTestAssset.add(eqp);
            insert lstTestAssset;

            WorkType wrkType1 = TestFactory.createWorkType('wrkType1','Hours',1);
            wrkType1.Type__c = AP_Constant.wrkTypeTypeMaintenance;
            insert wrkType1;
            
            case cse = new case(AccountId = testAcc.id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id);
            insert cse;

            WorkOrder wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse.id;
            wrkOrd.AccountId = testAcc.Id;
            wrkOrd.WorkTypeId = wrkType1.Id;
            insert wrkOrd;

            //Create Pricebook
            Pricebook2 standardPricebook = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
            lstPrcBk.add(standardPricebook);
            update lstPrcBk;

            //Create Service Contract
            servCon = new ServiceContract(name = 'Service Con',
            Pricebook2Id = lstPrcBk[0].Id,
            AccountId = testAcc.Id,
            Contract_Status__c = 'Expired' ,
            StartDate = System.today(),
            EndDate = System.today() + 1,
            // Type__c = 'Collective',
            Option_P2_avec_seuil__c = false,
            Type_de_seuil__c = null,
            Valeur_du_seuil_P2__c = 0,
            Sofactoapp_Mode_de_paiement__c = 'Virement',
            RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId());
            insert servCon;

           
            lstServiceApp = new List<ServiceAppointment>{
                                new ServiceAppointment(
                                        Status = 'Done OK',
                                        Subject = 'this is a test subject',
                                        ParentRecordId = testAcc.Id,
                                        EarliestStartTime = System.now(),
                                        DueDate = System.now() + 30,                                        
                                        Category__c = AP_Constant.ServAppCategoryVisiteEntr,
                                        ActualStartTime = Date.Today(),
                                        ActualEndTime = Date.Today().addDays(60),
                                        Service_Contract__c = servCon.Id,
                                        Work_order__c = wrkOrd.Id                          
                                )
            };
            insert lstServiceApp;

            //creating measurement
            Measurement__c mes = new Measurement__c(Parent_Work_Order__c=wrkOrd.Id); 
            mes.Asset__c = lstTestAssset[0].Id;        
            insert mes;
        }
        
    }  


	@isTest
	public static void doGenerate(){

		System.runAs(mainUser){
			
			Test.startTest();

				PageReference pageRef = Page.VFP75_CompteRenduIntervention; 
				pageRef.getParameters().put('id', lstServiceApp[0].Id);
				Test.setCurrentPage(pageRef);

                VFC75_CompteRenduIntervention devis = new VFC75_CompteRenduIntervention();

	        Test.stopTest();    
		}
	}
}