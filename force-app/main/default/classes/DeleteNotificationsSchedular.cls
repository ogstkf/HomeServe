global class DeleteNotificationsSchedular implements Schedulable {
	global void execute(SchedulableContext SC) {
        	AP16_MobileNotifications.deleteExpiredLogs();
    }
}