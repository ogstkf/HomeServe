@IsTest
public with sharing class FSL_DateUtills_Test {
    @IsTest
    public static void test_FSL_DateUtills_Test() {
        System.assertEquals(FSL_DateUtills.getNameOfMonth(1), 'January');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(2), 'February');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(3), 'March');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(4), 'April');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(5), 'May');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(6), 'June');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(7), 'July');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(8), 'August');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(9), 'September');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(10), 'October');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(11), 'November');
        System.assertEquals(FSL_DateUtills.getNameOfMonth(12), 'December');

        Date newDate = Date.newInstance(2019,08,01);
        System.assertEquals(FSL_DateUtills.dow(newDate), 'Thursday');
        newDate = Date.newInstance(2019,08,02);
        System.assertEquals(FSL_DateUtills.dow(newDate), 'Friday');
        newDate = Date.newInstance(2019,08,03);
        System.assertEquals(FSL_DateUtills.dow(newDate), 'Saturday');
        newDate = Date.newInstance(2019,08,04);
        System.assertEquals(FSL_DateUtills.dow(newDate), 'Sunday');
        newDate = Date.newInstance(2019,08,05);
        System.assertEquals(FSL_DateUtills.dow(newDate), 'Monday');
        newDate = Date.newInstance(2019,08,06);
        System.assertEquals(FSL_DateUtills.dow(newDate), 'Tuesday');
        newDate = Date.newInstance(2019,08,07);
        System.assertEquals(FSL_DateUtills.dow(newDate), 'Wednesday');
    }
}