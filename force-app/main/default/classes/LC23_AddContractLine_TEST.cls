/**
 * @File Name          : LC23_AddContractLine_TEST.cls
 * @Description        : 
 * @Author             : DMO
 * @Group              : 
 * @Last Modified By   : RRJ
 * @Last Modified On   : 12-07-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    03/02/2020   DMO   Initial Version
 
**/
@isTest
public with sharing class LC23_AddContractLine_TEST {
    static User mainUser;
    static Account testAcc= new Account();
    static List<Product2> lstTestProd = new List<Product2>();   
    static List<Pricebook2> lstPrcBk = new List<Pricebook2>();
    static List<PricebookEntry> lstPrcBkEnt = new List<PricebookEntry>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();
    static List<ContractLineItem> lstContractLineItems = new List<ContractLineItem>();
    static List<Product_Bundle__c> lstProductBundle = new List<Product_Bundle__c>();
    static List<Tarifs_Unifies_Locaux__c> lstTUL = new List<Tarifs_Unifies_Locaux__c>();
    static List<LC23_AddContractLine.CliWrapper> lstCliWrapper = new List<LC23_AddContractLine.CliWrapper>();
    static List<ServiceTerritory> lstServiceTerritory = new List<ServiceTerritory>();
    static LC23_AddContractLine.CliWrapper wrapperToCompare1;
    static LC23_AddContractLine.CliWrapper wrapperToCompare2;
    static LC23_AddContractLine.CliWrapper wrapperToCompare3;
    static LC23_AddContractLine.CliWrapper wrapperToCompare4;

    static{
        mainUser = TestFactory.createAdminUser('LC23', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            //creating account
            testAcc = TestFactory.createAccount('test Acc');
            testAcc.BillingPostalCode = '1233456';
            testAcc.Actif__c = true;
            testAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
            insert testAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;   

            //create products    
            Product2 prod = TestFactory.createProduct('testProd');
            prod.ProductCode='1234';
            prod.Description='1234';
            prod.Family='Autres';
            prod.IsBundle__c = true;
            prod.Ferme_souscription__c = false;
            lstTestProd.add(prod); 

            Product2 prod1 = TestFactory.createProduct('testProd');
            prod1.ProductCode='12333334';
            prod1.Description='33';
            prod1.IsBundle__c = false;
            prod1.Family = 'Offre d’entretien individuelle';
            prod1.Ferme_souscription__c = false;
            lstTestProd.add(prod1); 
         
            insert lstTestProd;

            //pricebook
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true,
                RecordTypeId= Schema.SObjectType.Pricebook2.getRecordTypeInfosByDeveloperName().get('Vente').getRecordTypeId(),
                // Compte__c = testAcc.Id,
                Ref_Agence__c = true
            );

            lstPrcBk.add(standardPricebook);
            update lstPrcBk;
        
            //pricebook entry
            PricebookEntry pbe0 = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[0].Id, 100);
            lstPrcBkEnt.add(pbe0);                 
            PricebookEntry pbe1 = TestFactory.createPriceBookEntry(lstPrcBk[0].Id, lstTestProd[1].Id, 100);
            lstPrcBkEnt.add(pbe1);                 
            insert lstPrcBkEnt;
            
            //Create operating hours
            OperatingHours newOperatingHour = TestFactory.createOperatingHour('test1');
            insert newOperatingHour;
            //create sofacto
            sofactoapp__Raison_Sociale__c sofa = new sofactoapp__Raison_Sociale__c(Name= 'TEST', sofactoapp__Credit_prefix__c= '234', sofactoapp__Invoice_prefix__c='432');
            insert sofa;
            //Insert Service territories/ agencies
            lstServiceTerritory = new List<ServiceTerritory>{
                new ServiceTerritory(
                    Name = 'test',
                    Agency_Code__c = '0007',
                    OperatingHoursId = newOperatingHour.Id,
                    IsActive = True,
                    Sofactoapp_Raison_Social__c =sofa.Id
                )};
            insert lstServiceTerritory;

            //service contract           
            lstServCon.add(TestFactory.createServiceContract('testServCon', testAcc.Id));
            lstServCon[0].PriceBook2Id = lstPrcBk[0].Id;    
            lstServCon[0].Agency__c = lstServiceTerritory[0].Id;
            lstServCon[0].RecordTypeId = Schema.SObjectType.ServiceContract.getRecordTypeInfosByDeveloperName().get('Contrats_Individuels').getRecordTypeId();
            insert lstServCon;   

            lstContractLineItems = new List<ContractLineItem>{
                new ContractLineItem(
                    Customer_Absences__c = 2,
                    Completion_Date__c = System.today(),
                    VE_Number_Counter__c = 20,
                    VE_Status__c = 'Not Planned',
                    ServiceContractId = lstServCon[0].Id,
                    PricebookEntryId=lstPrcBkEnt[0].Id,
                    Quantity=10,
                    UnitPrice=100),
                new ContractLineItem(
                    Customer_Absences__c = 1,
                    Completion_Date__c = System.today(),
                    VE_Number_Counter__c = 20,
                    VE_Status__c = 'Not Planned',
                    ServiceContractId = lstServCon[0].Id,
                    PricebookEntryId=lstPrcBkEnt[0].Id,
                    Quantity=10,
                    UnitPrice=100)
            };
            insert lstContractLineItems;

            LC23_AddContractLine.CliWrapper wrapper1 = new LC23_AddContractLine.CliWrapper();
            wrapper1.name='test1';
            wrapper1.cli=lstContractLineItems[0];
            wrapper1.isBundle=true;
            wrapper1.action='upsert';
            LC23_AddContractLine.CliWrapper wrapper2 = new LC23_AddContractLine.CliWrapper();
            wrapper2.name='test1';
            wrapper2.cli=lstContractLineItems[1];
            wrapper2.isBundle=true;
            wrapper2.action='delete';
            lstCliWrapper = new List<LC23_AddContractLine.CliWrapper>{wrapper1,wrapper2};

            Bundle__c bundleParent = TestFactory.createBundle('testProdBundleParent',100);
            insert bundleParent;
            Product_Bundle__c productBundleParent = TestFactory.createBundleProduct(bundleParent.Id,lstTestProd[0].Id);
            productBundleParent.Price__c = 0;
            productBundleParent.Optional_Item__c = false;
            Product_Bundle__c productBundleParent1 = TestFactory.createBundleProduct(bundleParent.Id,lstTestProd[1].Id);
            productBundleParent.Price__c = 5;
            productBundleParent.Optional_Item__c = true;
            lstProductBundle.add(productBundleParent1);
            insert lstProductBundle;

            lstTUL.add(TestFactory.createTarifsUnifiesLocaux('testTarif',20,bundleParent.Id,lstServiceTerritory[0].Id,230));
            insert lstTUL;

            wrapperToCompare1 = new LC23_AddContractLine.CliWrapper();
            wrapperToCompare1.name='wrapperToCompare1';
            wrapperToCompare1.isGuaranty = true;

            wrapperToCompare2 = new LC23_AddContractLine.CliWrapper();
            wrapperToCompare2.name='wrapperToCompare2';
            wrapperToCompare2.isBundle = true;

            wrapperToCompare3 = new LC23_AddContractLine.CliWrapper();
            wrapperToCompare3.name='wrapperToCompare3';
            wrapperToCompare3.isGuaranty = true;

            wrapperToCompare4 = new LC23_AddContractLine.CliWrapper();
            wrapperToCompare4.name='wrapperToCompare4';
            wrapperToCompare4.isGuaranty = true;
        }
    }

    @isTest
    public static void testFetchContractDetails(){
        System.runAs(mainUser){
            Test.startTest();
            ServiceContract srvCon = LC23_AddContractLine.fetchContractDetails(lstServCon[0].Id);
            Test.stopTest();

            System.assertEquals(lstPrcBk[0].Id,srvCon.Pricebook2Id);
        }
    }

    @isTest
    public static void testFetchPb(){
        System.runAs(mainUser){
            Test.startTest();
            List<Pricebook2> priceBooks = LC23_AddContractLine.fetchPb(lstServCon[0]);
            Test.stopTest();

            System.assertEquals(0,priceBooks.size());
        }
    }

    @isTest
    public static void testFetchCLI(){
        System.runAs(mainUser){
            Test.startTest();
            List<ContractLineItem> contractLines = LC23_AddContractLine.fetchCLI(lstServCon[0].Id,lstPrcBk[0].Id);
            Test.stopTest();

            System.assertEquals(2,contractLines.size());
        }
    }

    @isTest
    public static void testFetchPriceBookEntries(){
        System.runAs(mainUser){
            Test.startTest();
            List<PricebookEntry> lstPbe= LC23_AddContractLine.fetchPriceBookEntries(lstPrcBk[0].Id,'','123', lstServCon[0]);
            Test.stopTest();

            System.assertEquals(0,lstPbe.size());
        }
    }

    @isTest
    public static void testSaveCLI(){
        System.runAs(mainUser){
            String json = System.json.serialize(lstCliWrapper);
            Test.startTest();
            Object result= LC23_AddContractLine.saveCLI(json,lstServCon[0],lstPrcBk[0], true);
            Test.stopTest();

            System.assertNotEquals(null,result);
        }
    }

    @isTest
    public static void testPopulateCLI(){
        System.runAs(mainUser){
            Test.startTest();
            Object lstNewCliWrap= LC23_AddContractLine.populateCLI(new List<String>{lstPrcBkEnt[0].Id, lstPrcBkEnt[1].Id},lstServCon[0].Id,lstPrcBk[0].Id);
            Test.stopTest();

            System.assertNotEquals(null,lstNewCliWrap);
        }
    }

    @isTest
    public static void testClipperWrapperCompare1(){
        System.runAs(mainUser){

            Test.startTest();
            Integer result= wrapperToCompare1.compareTo(wrapperToCompare2);
            Test.stopTest();

            System.assertEquals(1,result);
        }
    }

    @isTest
    public static void testClipperWrapperCompare2(){
        System.runAs(mainUser){

            Test.startTest();
            Integer result= wrapperToCompare1.compareTo(wrapperToCompare3);
            Test.stopTest();

            System.assertEquals(0,result);
        }
    }

    @isTest
    public static void testClipperWrapperCompare3(){
        System.runAs(mainUser){

            Test.startTest();
            Integer result= wrapperToCompare2.compareTo(wrapperToCompare4);
            Test.stopTest();

            System.assertEquals(-1,result);
        }
    }
}