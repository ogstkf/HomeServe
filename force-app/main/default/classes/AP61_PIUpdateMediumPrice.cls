public with sharing class AP61_PIUpdateMediumPrice {

    public static void updatePrice(List<ProductItemTransaction> lstPIT){
        List<Id> pitIds = new List<Id>();
        List<Id> orderIds = new List<Id>();
        List<Id> productItemIds = new List<Id>();

        // Get Ids of PITList
        for (ProductItemTransaction pit : lstPIT) {
            pitIds.add(pit.Id);
        }

        // Get all the info needed (except OrderItem)
        List<ProductItemTransaction> PITs = [
            SELECT 
                Id, ProductItemId, ProductItem.Product2Id, 
                TYPEOF RelatedRecord
                  WHEN ProductTransfer THEN ProductTransferNumber, Commande__c, QuantityReceived
                END
            FROM ProductItemTransaction
            WHERE 
                ProductItem.Location.LocationType = 'Entrepôt' AND
                Id IN :pitIds
        ];

        // Get Ids of all the Orders and all the ProductItems
        for (ProductItemTransaction pit : PITs) {
            if (pit.RelatedRecord instanceof ProductTransfer) {
                ProductTransfer pt = pit.RelatedRecord;
                orderIds.add(pt.Commande__c);
                productItemIds.add(pit.ProductItemId);
            }
        }

        // Get all the OrderItem of the corresponding Orders
        List<OrderItem> OIs = [
            SELECT Id, Product2Id, TotalPrice, Quantity, OrderId
            FROM OrderItem 
            WHERE OrderId IN :orderIds
        ];

        // Get all the ProductItem of the corresponding Orders
        // Add QuantityOnHand on the select  TCO - 10/03/2021   
        List<ProductItem> PIs = [
            SELECT Id, Quantite_achetee__c, Prix_moyen__c, Product2Id, Depreciation__c, QuantityOnHand
            FROM ProductItem 
            WHERE Id IN :productItemIds
        ];

        // Update the fields
        for (ProductItemTransaction pit : PITs) {
            if (pit.RelatedRecord instanceof ProductTransfer) {
                ProductTransfer pt = pit.RelatedRecord;
                //29/01/2021 - TEC-480 Change of logic to not use a PMP but a FIFO Method - rolled back to PMP   
                /*for(OrderItem oi : OIs) {
                    //on ne met pas à jour le prix de l'article s'il était à 0, on conserve le prix précédent
                    if (pit.ProductItem.Product2Id == oi.Product2Id && pt.Commande__c == oi.OrderId && oi.totalPrice>0) {
                        for (ProductItem pi : PIs) {
                            if (pit.ProductItemId == pi.Id) {
                                System.debug ('---> GLOBAL');
                                System.debug ('---> pit.ProductItem.Product2Id : ' + pit.ProductItem.Product2Id);
                                System.debug ('---> oi.Product2Id : ' + oi.Product2Id);
                                System.debug ('---> pt.Commande__c : ' + pt.Commande__c);
                                System.debug ('---> oi.OrderId : ' + oi.OrderId);
                                System.debug ('---> pit.ProductItemId : ' + pit.ProductItemId);
                                System.debug ('---> pi.Id : ' + pi.Id);
                                System.debug ('--->      ');
                                System.debug ('---> AVANT');
                                System.debug ('---> pi.Quantite_achetee__c : ' + pi.Quantite_achetee__c);
                                System.debug ('---> pi.Prix_moyen__c : ' + pi.Prix_moyen__c);
                                System.debug ('---> oi.TotalPrice : ' + oi.TotalPrice);
                                System.debug ('---> pt.QuantityReceived : ' + pt.QuantityReceived);
                                System.debug ('---> oi.Quantity : ' + oi.Quantity);
                                System.debug ('---> pi.Depreciation__c: ' + pi.Depreciation__c);

                                // Previously, they were null as default (now, they are 0)
                                if (pi.Prix_moyen__c == null) { pi.Prix_moyen__c = 0; }
                                if (pi.Quantite_achetee__c == null) { pi.Quantite_achetee__c = 0; }

                                // To avoid div by 0
                                if ((oi.Quantity) != 0) {
                                    pi.Prix_moyen__c = oi.TotalPrice / oi.Quantity;
                                    pi.depreciation__c = 0;
                                }
                                // The quantity entered in stock
                                pi.Quantite_achetee__c = pt.QuantityReceived;
                                System.debug ('---> APRES');
                                System.debug ('---> pi.Prix_moyen__c : ' + pi.Prix_moyen__c);
                                System.debug ('---> pi.Quantite_achetee__c : ' + pi.Quantite_achetee__c);
                                System.debug ('---> pi.Depreciation__c: ' + pi.Depreciation__c);
                            }
                        }
                    }
                }
                */
                
                //BCH - 24/02/2021 - TEC-480 use of the PMP for the calculation, but don't take into account if order price is 0 (free products) to update the PMP
                for(OrderItem oi : OIs) {
                    if (pit.ProductItem.Product2Id == oi.Product2Id && pt.Commande__c == oi.OrderId) {
                        for (ProductItem pi : PIs) {
                            if (pit.ProductItemId == pi.Id) {
                                System.debug ('---> GLOBAL');
                                System.debug ('---> pit.ProductItem.Product2Id : ' + pit.ProductItem.Product2Id);
                                System.debug ('---> oi.Product2Id : ' + oi.Product2Id);
                                System.debug ('---> pt.Commande__c : ' + pt.Commande__c);
                                System.debug ('---> oi.OrderId : ' + oi.OrderId);
                                System.debug ('---> pit.ProductItemId : ' + pit.ProductItemId);
                                System.debug ('---> pi.Id : ' + pi.Id);
                                System.debug ('--->      ');
                                System.debug ('---> AVANT');
                                System.debug ('---> pi.Quantite_achetee__c : ' + pi.Quantite_achetee__c);
                                System.debug ('---> pi.Prix_moyen__c : ' + pi.Prix_moyen__c);
                                System.debug ('---> oi.TotalPrice : ' + oi.TotalPrice);
                                System.debug ('---> pt.QuantityReceived : ' + pt.QuantityReceived);
                                System.debug ('---> oi.Quantity : ' + oi.Quantity);
                                System.debug ('######---> pi.QuantityOnHand : ' + pi.QuantityOnHand);

                                // Previously, they were null as default (now, they are 0)
                                if (pi.Prix_moyen__c == null) { pi.Prix_moyen__c = 0; }
                                if (pi.Quantite_achetee__c == null) { pi.Quantite_achetee__c = 0; }

                                // To avoid div by 0
                                // Change of Quantite_achetee__c for QuantityOnHand - TCO 10/03/2021
                                if ((pi.QuantityOnHand + pt.QuantityReceived) != 0) {
                                    if (oi.TotalPrice >0){
                                        pi.Prix_moyen__c = (((pi.QuantityOnHand - pt.QuantityReceived) * pi.Prix_moyen__c) + (oi.TotalPrice * pt.QuantityReceived / oi.Quantity)) / 
                                                        (pi.QuantityOnHand);
                                        pi.Quantite_achetee__c = pi.Quantite_achetee__c + pt.QuantityReceived;         
                                    }
                                }
                                //pi.Quantite_achetee__c = pi.Quantite_achetee__c + pt.QuantityReceived;
                                System.debug ('---> APRES');
                                System.debug ('---> pi.Prix_moyen__c : ' + pi.Prix_moyen__c);
                                System.debug ('---> pi.Quantite_achetee__c : ' + pi.Quantite_achetee__c);
                            }
                        }
                    }
                }
            }
        }
        update PIs;
    }
}