public class AP13_HandleTask {
    /**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Assigned ownerID to Admin de l'agence
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 13-AUG-2019  SRA    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/    
    
    public static void updateTaskUserWorkInProgress(List<Task> lstTasks) {
        List<Task> lstTasksToUpdate = new List<Task>();
        Set<string> setTaskIdUserWorkInProg = new Set<string>();
        
        for(Task task : lstTasks) {
            setTaskIdUserWorkInProg.add(task.TECH_GroupTask_CommonId__c);
            lstTasksToUpdate.add(new Task(Id = task.Id, Work_In_Progress_By__c = null));
        }

        for(Task taskToUpd : [SELECT Id, Work_In_Progress__c, Work_In_Progress_By__c, TECH_GroupTask_CommonId__c FROM Task WHERE TECH_GroupTask_CommonId__c IN :setTaskIdUserWorkInProg AND Id NOT IN :lstTasks]){
           
            if(taskToUpd.Work_In_Progress_By__c != null) {
                lstTasksToUpdate.add(new Task(Id = taskToUpd.Id, Work_In_Progress_By__c = null, Work_In_Progress__c=false));
            }
        }

        if(lstTasksToUpdate.size() > 0) {
            update lstTasksToUpdate;
        }
    }
    
}