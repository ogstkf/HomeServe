/**
 * @File Name          : AP16_MobileNotifications_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class AP16_MobileNotifications_TEST {
	/**
 * @File Name          : AP16_MobileNotifications_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         07-10-2019     		ANA         Initial Version
**/
   static Case cse;
    static ServiceAppointment serappGaz;
    static User mainUser;
    static User use;
    static List<Asset> lstTestAssset = new List<Asset>();
    static Account testAcc;
    static List<Logement__c> lstTestLogement = new List<Logement__c>();
    static Product2 lstTestProd = new Product2();
    static WorkOrder wrkOrd;
    static ServiceTerritory servTerritory = new ServiceTerritory();
    static OperatingHours opHrs = new OperatingHours();
    static AssignedResource ASR;

    static{
        mainUser = TestFactory.createAdminUser('MobileTest@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            testAcc = TestFactory.createAccount('Test Account');
            testAcc.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

            insert testAcc;
			sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(testAcc.Id);
            testAcc.sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update testAcc;
            //creating logement
            Logement__c lgmt = new Logement__c(Account__c = testAcc.Id);
            lstTestLogement.add(lgmt);
            insert lstTestLogement;

            //creating products
            Product2 lstTestProd = new Product2(Name = 'Product X',
                                    ProductCode = 'Pro-X',
                                    IsActive = true,
                                    Equipment_family__c = 'Chaudière',
                                    Equipment_type__c = 'Chaudière gaz',
                                    Statut__c = 'Approuvée'
            );
            insert lstTestProd;

            // lstTestProd.RecordType.DeveloperName = 'Article';
            // update lstTestProd;


            // creating Assets
            Asset eqp = TestFactory.createAccount('equipement', AP_Constant.assetStatusActif, lstTestLogement[0].Id);
            eqp.Product2Id = lstTestProd.Id;
            eqp.Logement__c = lstTestLogement[0].Id;
            eqp.AccountId = testacc.Id;
            lstTestAssset.add(eqp);
            insert lstTestAssset;

            // create case
            cse = new case(AccountId = testacc.id
                                ,type = 'A1 - Logement'
                                ,AssetId = lstTestAssset[0].Id);
            cse.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Client_anomaly').getRecordTypeId();
            insert cse;

            wrkOrd = TestFactory.createWorkOrder();
            wrkOrd.caseId = cse.id;
            wrkOrd.Type__c =  'Maintenance';
            wrkOrd.assetId = lstTestAssset[0].Id;
            insert wrkOrd;

            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;

            sofactoapp__Raison_Sociale__c raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;
            
            servTerritory = TestFactory.createServiceTerritory('SGS - test Territory', opHrs.Id,raisonSocial.Id);
            insert servTerritory;

            serappGaz = TestFactory.createServiceAppointment(wrkOrd.Id);
            serappGaz.ServiceTerritoryId = servTerritory.Id;
            serappGaz.Status='None';
            serappGaz.SchedStartTime =  DateTime.newInstance(System.now().year() , System.now().month(), System.now().day(), 0, 0, 0);
            serappGaz.SchedEndTime = DateTime.newInstance(System.now().year() , System.now().month(), System.now().day(), 23, 59, 59);

            insert serappGaz;

            Use = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Test User ANA', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', 
                TimeZoneSidKey='America/Los_Angeles', UserName='TestUserANA@testorg.com',
                Android_Device_Token__c='1234',
                          ProfileId=TestFactory.getProfileAdminId());

            insert Use;

            ServiceResource SR = new ServiceResource(IsActive=true,Name='Test User ANA',RelatedRecordId=Use.Id);
            insert SR;

            ServiceTerritoryMember  STM = new ServiceTerritoryMember(EffectiveStartDate=Datetime.now().addYears(-1),ServiceTerritoryId=servTerritory.Id,ServiceResourceId=SR.Id);
            insert STM;

            ASR = new AssignedResource(ServiceAppointmentId = serappGaz.Id,ServiceResourceId=SR.Id);
        }
    }

    @isTest
    public static void onServiceAppointmentAssignedTEST(){
        System.runAs(mainUser){

            Integer statusCode = 200;
			String status = 'CREATED';
            
            string body ='Votre planning a été mis à jour';
            string title ='NECIA Mobile';
            string obj ='ServiceAppointement';
            string ids =String.valueOf(serappGaz.Id);
            string foregrounds = 'false';
            string clickaction ='UPDATE_DATA';

            String reqbody ='{"to":"'+use.Android_Device_Token__c+'", "notification":{"body":"'+body+'", "title":"'+title+'", "object":"'+obj+'", "id":"'+ids+'", "foreground":"'+foregrounds+'", "click_action":"'+clickaction+'"} }';
            SingleRequestMock asyncMock = new SingleRequestMock(statusCode, status, body, null);
            Test.setMock(HttpCalloutMock.class, asyncMock); 
            
            Test.startTest();
            insert ASR;
            AP16_MobileNotifications.deleteExpiredLogs();
            Test.stopTest();

        }
    }

    public class SingleRequestMock implements HttpCalloutMock {
		protected Integer code;
		protected String status;
		protected String bodyAsString;
		protected Blob bodyAsBlob;
		protected Map<String, String> responseHeaders;

		public SingleRequestMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
			this.code = code;
			this.status = status;
			this.bodyAsString = body;
			this.bodyAsBlob = null;
			this.responseHeaders = responseHeaders;
		}

		public SingleRequestMock(Integer code, String status, Blob body, Map<String, String> responseHeaders) {
			this.code = code;
			this.status = status;
			this.bodyAsBlob = body;
			this.bodyAsString = null;
			this.responseHeaders = responseHeaders;
		}


	  	public HTTPResponse respond(HTTPRequest req) {

		    HttpResponse resp = new HttpResponse();
		    resp.setStatusCode(code);
		    resp.setStatus(status);
		    if (bodyAsBlob != null) {
		      		resp.setBodyAsBlob(bodyAsBlob);
		   	} 
		   	else {
		      	resp.setBody(bodyAsString);
		    }
	    
		    if (responseHeaders != null) {
	      		for (String key : responseHeaders.keySet()) {
	       			resp.setHeader(key, responseHeaders.get(key));
	      		}
		    }
	    	return resp;
	  	}
	}

    @isTest
    public static void sendNotificationTEST(){
        System.runAs(mainUser){          
            Test.startTest();            
                AP16_MobileNotifications.sendNotification('testTopic', 'msg_test', null);
            Test.stopTest();
        }
    }

    @isTest
    public static void onSaChangedTEST(){
        System.runAs(mainUser){          
            Test.startTest();            
                AP16_MobileNotifications.onSaChanged(new list<ServiceAppointment>{serappGaz});
            Test.stopTest();
        }
    }

    @isTest 
    public static void deleteNotificationsSchedularTEST(){
        System.runAs(mainUser){          
            Test.startTest();
                DeleteNotificationsSchedular abs= new DeleteNotificationsSchedular();
                String jobId = System.schedule('myJobTestJobName', '0 0 0 15 3 ? 2022', abs);
                abs.execute(null);
            Test.stopTest();
        }
    }

    @isTest 
    public static void BeforeSendingNotiff(){
        System.runAs(mainUser){     
            insert ASR;
            Id asrID = [select id FROM AssignedResource where id=: asr.Id].Id;
            Test.startTest();
                AP16_MobileNotifications.BeforeSendingNotiff(new list<Id>{asrID});                
            Test.stopTest();
        }
    }

    @isTest 
    public static void onSaChanged(){
        System.runAs(mainUser){          
            Test.startTest();
                AP16_MobileNotifications.onSaChanged(new list<ServiceAppointment>{serappGaz});                
            Test.stopTest();
        }
    }

}