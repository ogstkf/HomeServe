/**
 * @File Name          : VFC08_AnomalieA1A2.cls
 * @Description        : 
 * @Author             : Spoon Consulting 
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 21/11/2019, 10:38:51
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    8/1/2019, 2:40:06 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
public with sharing class VFC08_AnomalieA1A2{

    public Case currentCase {get;set;}
    public List<Case> lstCase {get;set;}
    public ServiceAppointment sa {get;set;}
    public String currentDate  {get;set;}
    public String assetAccName {get;set;}
    public String SRName {get;set;}
    public Boolean BoolCheckClientPresent{get;set;}
    public Boolean BoolCheckClientAbsent{get;set;}
    public static ServiceAppointment testSa {get;set;}

    public VFC08_AnomalieA1A2() {
        System.debug('**** In constructor VFC06_PACAttestation ****');

        //try{
            string caseId = ApexPages.currentPage().getParameters().get('id');
            currentDate = string.valueOf(Datetime.now().format('dd/MM/yyyy'));

            if(String.isNotBlank(caseId)){
                lstCase = new List<Case>([SELECT Id
                                                ,Type
                                                ,Anomaly_reason__c                                                        
                                                ,Account.Id
                                                ,Account.ClientNumber__c
                                                ,Account.Phone
                                                ,Account.PersonMobilePhone
                                                ,Account.PersonEmail
                                                ,Account.BillingStreet
                                                ,Account.BillingPostalCode
                                                ,Account.BillingCity
                                                ,Service_Contract__r.ContractNumber
                                                ,Service_Contract__r.Name
                                                ,Service_Contract__r.EndDate
                                                ,Local_Agency__r.Name
                                                ,Local_Agency__r.TerritoryStat__c
                                                ,Local_Agency__r.Address
                                                ,Local_Agency__r.Street
                                                ,Local_Agency__r.City
                                                ,Local_Agency__r.State
                                                ,Local_Agency__r.PostalCode
                                                ,Local_Agency__r.Country
                                                ,Local_Agency__r.Phone__c
                                                ,Parent.Local_Agency__r.Phone__c
                                                ,Local_Agency__r.Fax__c
                                                ,Local_Agency__r.Email__c                                                      
                                                ,Local_Agency__r.Siret__c
                                                ,Local_Agency__r.Siren__c 
                                                ,Local_Agency__r.Intra_Community_VAT__c
                                                ,Local_Agency__r.Street2__c
                                                ,Asset.AccountId
                                                ,Asset.Account.IsPersonAccount
                                                ,Asset.Account.Name
                                                ,Asset.Account.FirstName
                                                ,Asset.Account.LastName
                                                ,Asset.Account.BillingStreet
                                                ,Asset.Account.Imm_Res__c  
                                                ,Asset.Account.Floor__c
                                                ,Asset.Account.Door__c 
                                                ,Asset.Account.BillingCity
                                                ,Asset.Account.BillingPostalCode  
                                                ,Asset.Account.ClientNumber__c
                                                ,Asset.Account.BillingAddress
                                                ,ParentId
                                                              
                                                FROM Case 
                                                WHERE Id = :caseId]);
                System.debug('**** lstCase ' + lstCase);

                if(lstCase.size() > 0){
                    currentCase = lstCase[0];                    
                    System.debug('##### [hone: '+currentCase.Local_Agency__r.Phone__c);
                    assetAccName = (lstCase[0].AssetId != null && Asset.AccountId != null) ? (lstCase[0].Asset.Account.IsPersonAccount == true ? lstCase[0].Asset.Account.FirstName + ' ' + lstCase[0].Asset.Account.LastName : lstCase[0].Asset.Account.Name) : ' ';
                    list<ServiceAppointment> saLst = test.isRunningTest() ? new list<ServiceAppointment>{testSa}: [select id, SchedStartTime,Status from ServiceAppointment where Work_Order__r.CaseId =: lstCase[0].ParentId order by createdDate desc limit 1 ];
                    system.debug('*** saLst 1: '+ saLst);
                    if(saLst.size()>0){
                        system.debug('*** saLst 2: '+ saLst);
                        sa = saLst[0];

                        System.debug('sa.Id : '+sa.Id);

                        //ANA START
                        System.debug('## SA Status: ' + sa.Status); 
                        if(sa.Status==AP_Constant.servAppStatusImposFinish || sa.Status==AP_Constant.servAppStatusEfectueConc || sa.Status==AP_Constant.servAppStatusWorkPlan){
                            BoolCheckClientPresent=true;
                        }
                        else if(sa.Status==AP_Constant.servAppStatusClientAbsent){
                            BoolCheckClientAbsent=true;
                        }
                        else {
                            BoolCheckClientPresent=false;
                            BoolCheckClientAbsent=false;
                        }

                        SRName='';

                        List<AssignedResource> lstASR = [SELECT ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId=:sa.Id ORDER BY CreatedDate DESC LIMIT 1];
                        if(lstASR.size()>0){

                            List<ServiceResource> lstSR = [SELECT RelatedRecordId FROM ServiceResource WHERE Id =:lstASR[0].ServiceResourceId];

                            if(lstSR.size()>0){
                                User lstUser = [SELECT Name FROM User WHERE Id =:lstSR[0].RelatedRecordId];
                                SRName = lstUser.Name;
                            }
                        }

                        //ANA END
                    }

                }
        }
        // }
        // catch(exception e){
        //     system.debug('**** exception msg: '+ e.getMessage());
        // }
    }
}