public with sharing class LC14_GETContractLineItem {
    	/**
 * @File Name          : 
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : 
 * @Last Modified On   : 
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         29-11-2019     		ANA         Initial Version
 * 1.1         04-06-2020           DMU         Commented class due to Homeserve Project
**/

/*
    @AuraEnabled 
    public static Map<String, Object> getContractLinetem(integer index,integer currYear, string scType, Integer pageNumber, Integer pageSize) {  

        System.debug('index cont : '+index);
        System.debug('cureee '+currYear);
        System.debug('## page number: ' + pageNumber);
        System.debug('## pageSize: ' + pageSize);

        FSL_SearchVesPlanning_BA fx = new FSL_SearchVesPlanning_BA();
        Date minDateMplus1 = Date.newInstance(currYear, index , 01);    
        Date maxDateMplus1 = Date.newInstance(minDateMplus1.year(), minDateMplus1.month(), fx.getLastDayOfMonth(minDateMplus1.month()));
        string str_maxDateMplus1 = string.valueOf(maxDateMplus1).replace(' ', 'T');
        System.debug('Min Date of Month plus 1: ' + minDateMplus1);
        System.debug('Max Date of Month plus 1: ' + maxDateMplus1);
        System.debug('str_maxDateMplus1: ' + str_maxDateMplus1);

        Date minDateMplus2 = minDateMplus1.addMonths(1);
        Date maxDateMplus2 = Date.newInstance(minDateMplus2.year(), minDateMplus2.month() ,fx.getLastDayOfMonth(minDateMplus2.month()));
        System.debug('Min Date of Month plus 2: ' + minDateMplus2);
        System.debug('Max Date of Month plus 2: ' + maxDateMplus2);

        Date dM = minDateMplus1.addMonths(-1);
        Date maxDatedM = Date.newInstance(dM.year(), dM.month(), fx.getLastDayOfMonth(dM.month()));
        string str_maxDatedM = string.valueOf(maxDatedM).replace(' ', 'T');
        System.debug('Min Date of Month M: ' + dM);
        System.debug('Max Date of Month M: ' + maxDatedM);
        System.debug('str_maxDatedM: ' + str_maxDatedM);

        user userConnected = [SELECT id, companyName FROM User WHERE id=: UserInfo.getUserId()];
        string companyName = userConnected.companyName;
        // Date dtoday = system.today();
        string dtoday = string.valueOf(system.today()).replace(' ', 'T');
        System.debug('**  dtoday: '+dtoday);

        string mainQuery = 'SELECT Id, ServiceContract.Type__c, ServiceContract.Logement__r.Name, ServiceContract.Logement__c, ServiceContract.AccountId, ServiceContract.Account.AccountNumber, ServiceContract.Account.Name, Collective_Account__c, Collective_Account__r.Name, Collective_Account__r.AccountNumber, VE_Max_Date__c, VE_Min_DAte__c, ServiceContract.RootServiceContract.Account.Name, ServiceContract.Account.ClientNumber__c, Collective_Account__r.ClientNumber__c, AssetId, Asset.IDEquipmenta__c, Asset.Equipment_type_auto__c, Product2.Id, Product2.Name, UnitPrice, Desired_VE_Date__c FROM ContractLineItem ';
        String countQuery = 'SELECT Count(ID) FROM ContractLineItem ';
        
        String query = '';
        query += ' WHERE ';
        //SBH Commenting start
        
        // query += ' (ServiceContract.Contract_Status__c = \'Active\' OR ServiceContract.Contract_Status__c = \'Pending Payment\' OR ServiceContract.Contract_Status__c = \'Pending first visit\' OR ServiceContract.Contract_Status__c = \'En attente de renouvellement\' OR ServiceContract.Contract_Status__c = \'Actif - en retard de paiement\' )';

        // // 25/01/2020 - Removed condition ServiceContract.Contract_Renewed__c AND added condition on VE_Status__c='On Going'
        // //  (requirement on https://docs.google.com/spreadsheets/d/1LKrlLTmmrAAAOvv53a_-zp9BfGNnYxkT/edit#gid=301301757)
        // //query += ' AND ServiceContract.Contract_Renewed__c = true ';

        // query += ' AND (VE_Status__c = \'Not Planned\' OR VE_Status__c = \'On Going\')  ';
        // query += ' AND Customer_Absence_Range__c = true';
        // query += ' AND VE__c = true';
        // query += ' AND ServiceContract.Agency__r.Name like \'%' + companyName + '%\' ' ;
        // query += ' AND TECH_IsVECounterLessThanVETarget__c = true';
        // query += ' AND IsActive__c = true ';
        // query += ' AND Product2.IsBundle__c = true ';
        // query += ' AND EndDate > ' + dtoday + ' ' ;

        // query += ' AND ( ';
        // query += ' VE_Max_Date__c <= ' + str_maxDatedM + ' ' ;
        // // query += ' (CALENDAR_MONTH(VE_Max_Date__c) < ' + maxDatedM.month() + ' ' ;
        // query += ' OR (CALENDAR_MONTH(VE_Max_Date__c) = ' + maxDateMplus1.month() + ' AND CALENDAR_YEAR(VE_Max_Date__c) = ' + maxDateMplus1.year() + ') ';
        // query += ' OR (CALENDAR_MONTH(VE_Max_Date__c) = ' + maxDateMplus2.month() + ' AND CALENDAR_YEAR(VE_Max_Date__c) = ' + maxDateMplus2.year() + ') ';
        // query += ' OR (CALENDAR_MONTH(VE_Max_Date__c) > ' + maxDateMplus2.month() + ' AND CALENDAR_YEAR(VE_Max_Date__c) >= ' + maxDateMplus2.year() + ' AND VE_Min_DAte__c <= ' + str_maxDateMplus1 + ') ';
        // query += ' ) ';

        // if(scType=='Tout'){
        //     System.debug('**  in Tout');
        //     query += ' AND (ServiceContract.Type__c = \'Individual\' OR ServiceContract.Type__c = \'Collective\') ';
        // }else if(scType=='Individuel'){
        //     query += ' AND ServiceContract.Type__c = \'Individual\' ';
        // }else if(scType=='Collectif'){
        //     query += ' AND ServiceContract.Type__c = \'Collective\' ';
        // }
        // query += ' ORDER BY VE_Max_Date__c, ServiceContract.Type__c, ServiceContract.Logement__c ASC ';
        
        //SBH commenting end

        query += ' CreatedDate >= 2020-02-19T00:00:00.000Z';
        // query += ' LIMIT 5 ';
        String queryWithoutOffset = query;

        //SBH pagination start
        Integer ps = Integer.valueOf(pageSize);
        Integer pn = Integer.valueOf(pageNumber)-1;

        query += 'LIMIT ' + ps + ' OFFSET ' + (ps * pn);

        countQuery += queryWithoutOffset;
        mainQuery += query;

        //SBH Pagination end 

        System.debug('mainQuery : '+mainQuery);
        System.debug('countQuery ' + countQuery);

        List<ContractLineItem> lstConLine = Database.query(mainQuery);
        AggregateResult[] groupedResults = Database.query(countQuery);

        System.debug('##groupedResults ' + groupedResults);
        System.debug('lstConLine size : '+lstConLine.size());

        Map<String, Object> mapRes = new Map<String, Object>();
        mapRes.put('count' , groupedResults);
        mapRes.put('data', lstConLine);
        // return lstConLine;
        return mapRes;
    }

    @AuraEnabled
    public static Map<String, Object> save(String ids, Integer index, Integer year){
        System.debug('##ids : ' + ids);
        List<String> lstIds = ids.split(',');
        System.debug('lstids: ' + lstIds);
        String error; 
         Map<String, Object> mapRes = new Map<String, Object>();
        
        try{
            Id batchJobId = Test.isRunningTest() ? 'xxx' : Database.executeBatch(new FSL_SearchVesPlanning_BA(lstIds, year , index, 'individual'));
            // if(!Test.isRunningTest())
            //  	batchJobId = Database.executeBatch(new FSL_SearchVesPlanning_BA(lstIds, year , index, 'individual'));
            System.debug('## Batch Id: '+ batchJobId);
            mapRes.put('error', false);
            mapRes.put('batch_id' , batchJobId);
            mapRes.put('msg' , 'Batch lancé avec succès');
        }catch(Exception e){
            error = e.getMessage();
            System.debug('## error : ' + error);
            mapRes.put('error', true);
            mapRes.put('msg', error);
        }

        return mapRes;
    }
*/
}