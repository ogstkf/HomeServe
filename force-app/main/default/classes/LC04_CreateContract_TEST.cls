/**
 * @File Name          : LC04_CreateContract_TEST.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : MNA
 * @Last Modified On   : 05-10-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/02/2020   AMO     Initial Version
**/
@isTest
public with sharing class LC04_CreateContract_TEST {
    	/**
 * @File Name          : 
 * @Description        : 
 * @Author             : Spoon Consulting (ANA)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 05-10-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         28-11-2019     		ANA         Initial Version
**/

    static User mainUser;
    static List<Account> lstAcc;
    static logement__c loge;
    static Asset asse;
    static Product2 testpro;
    static Bypass__c bp = new Bypass__c();

    static{
        mainUser = TestFactory.createAdminUser('LC04CreateContract', TestFactory.getProfileAdminId());
        insert mainUser;
        
        bp.SetupOwnerId  = mainUser.Id;
        bp.BypassTrigger__c = 'AP16,WorkOrderTrigger';
        bp.BypassValidationRules__c = True;
        insert bp;

        System.runAs(mainUser){

            lstAcc = new list<Account>{
                new Account(Name='Test Account ANA'
                    ,BillingStreet='Street'
                    ,BillingCity='City'
                    ,BillingState='State'
                    ,BillingPostalCode='Code'
                    ,BillingCountry='France'),

                TestFactory.createAccount('Test loge Owner'),
                TestFactory.createAccount('Test Inhabitant')
            };

            insert lstAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAcc.get(0).Id);
			lstAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update lstAcc;

            loge = new logement__c(
                Owner__c = lstAcc[1].Id,
                Inhabitant__c = lstAcc[2].Id,
                Street__c = 'Royal road surinam',
                City__c = 'Paris',
                Postal_Code__c='Code',
                Country__c='France',
                Account__c = lstAcc[0].Id
            );

            insert loge;

            testpro = new Product2(Name = 'Product X1',
                                ProductCode = 'Pro-X1',
                                isActive = true,
                                Equipment_family__c = 'Chaudière',
                                Statut__c = 'Approuvée',
                                Equipment_type1__c = AP_Constant.wrkTypeEquipmentTypeChaudieregaz
            );
            insert testpro;

            asse = TestFactory.createAsset('TEST ASSET ANA','Actif',loge.Id);
            asse.AccountId = lstAcc[0].Id;
            asse.Product2Id = testpro.Id;
            insert asse;


        }
    }

    @isTest
    public static void TESTInsertContract(){
        System.runAs(mainUser){
            
            Test.startTest();
            
            LC04_CreateContract.CreateContract(asse.Id);
            Test.stopTest();

            list<ServiceContract> lstServCon = [SELECT Name,Home_Owner__c,Type__c,BillingCity,ShippingStreet FROM ServiceContract];
            System.assertEquals(lstServCon[0].Home_Owner__c,loge.Owner__c);
            System.assertEquals(lstServCon[0].Type__c,'Individual');
            System.assertEquals(lstServCon[0].BillingCity,lstAcc[0].BillingCity);
            System.assertEquals(lstServCon[0].ShippingStreet,loge.Street__c);

        }
    }

    @isTest
    public static void TestFetchServCon(){
        System.runAs(mainUser){
            
            Test.startTest();
            List<ServiceContract> lstServCon = LC04_CreateContract.fetchServiceCon(asse.Id);
            Test.stopTest();

            System.assertNotEquals(null, lstServCon.size());
        }
    }

     @isTest
     public static void TestremoveLinkEquipementAndAccount(){
         System.runAs(mainUser){
            
             Test.startTest();
             LC04_CreateContract.removeLinkEquipementAndAccount(asse.Id, System.today());
             Test.stopTest();
         }
     }

}