/**
 * @File Name          : AP43_UpdateServiceContractStatus_TEST.cls
 * @Description        : 
 * @Author             : DMU
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 03/02/2020, 15:38:40
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    14/11/2019   DMU     Initial Version
**/
@isTest
public with sharing class AP43_UpdateServiceContractStatus_TEST {

    static User adminUser;
    static List<Account> lstTestAcc = new List<Account>();
    static List<ServiceContract> lstServCon = new List<ServiceContract>();

    static {
        adminUser = TestFactory.createAdminUser('AP43_User', TestFactory.getProfileAdminId());
        insert adminUser;

        System.runAs(adminUser){
           
            //create accounts
            for(Integer i=0; i<1; i++){
                lstTestAcc.add(TestFactory.createAccount('testAcc'+1));
                lstTestAcc[i].RecordTypeId = AP_Constant.getRecTypeId('Account', 'PersonAccount');
                lstTestAcc[i].BillingPostalCode = '1234'+i;
            }
            insert lstTestAcc;

            sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstTestAcc.get(0).Id);
			lstTestAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            
            update lstTestAcc;

            //service contract
            for(Integer i=0; i<3; i++){
                lstServCon.add(TestFactory.createServiceContract('testServCon'+i, lstTestAcc[0].Id));               
                lstServCon[i].Contract_Status__c = AP_Constant.serviceContractStatutContratResilie;
            }
            insert lstServCon;            
        }
    }

    @isTest
    static void testUpdateServiceCOntract(){
        System.runAs(adminUser){          
            
            Test.startTest();

            AP43_UpdateServiceContractStatus.UpdateStatus(lstServCon);
               // lstServCon[0].Contrat_signe_par_le_client__c = true;
               // lstServCon[1].Contrat_signe_par_le_client__c = true;
               // Update lstServCon;
                
            Test.stopTest();

            list <ServiceContract> lstSerConUpd = [SELECT Id, Contract_Status__c FROM ServiceContract WHERE Id IN :lstServCon];
           // System.assertEquals( AP_Constant.serviceContractStatutEnAttentePremiereVisitValid, lstSerConUpd[0].Contract_Status__c);
           // System.assertEquals( AP_Constant.serviceContractStatutEnAttentePremiereVisitValid, lstSerConUpd[1].Contract_Status__c);
        }
    }


}