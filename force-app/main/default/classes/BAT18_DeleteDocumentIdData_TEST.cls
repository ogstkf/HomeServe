/**
 * @File Name         : BAT18_DeleteDocumentIdData_TEST
 * @Description       : 
 * @Author            : Spoon Consutling (MNA)
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 22-09-2021
 * Modifications Log 
 * ==========================================================
 * Ver   Date               Author          Modification
 * 1.0   10-08-2021         MNA             Initial Version
**/
@isTest
public with sharing class BAT18_DeleteDocumentIdData_TEST {
    static User mainUser;

    static {
        mainUser = TestFactory.createAdminUser('BAT18@test.com', TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
            insert new Editique_Document_ID__c();
        }
    }

    @isTest
    static void testScheduleExecute1() {
        System.runAs(mainUser) {
            Test.startTest();
            System.schedule('Batch BAT18' + Datetime.now().format(),  '0 0 0 * * ?',  new BAT18_DeleteDocumentIdData());
            Test.stopTest();
        }
    }

    @isTest
    static void testScheduleExecute2() {
        System.runAs(mainUser) {
            insert new batchConfiguration__c(Name = 'BAT18_DeleteDocumentIdData', BatchSize__c = 200);
            Test.startTest();
            System.schedule('Batch BAT18' + Datetime.now().format(),  '0 0 0 * * ?',  new BAT18_DeleteDocumentIdData());
            Test.stopTest();
        }
    }

    @isTest
    static void testBatchExecute1() {
        System.runAs(mainUser) {
            Integer actuelSize = [SELECT Count() FROM Editique_Document_ID__c];
            System.assertEquals(1, actuelSize);
            Test.startTest();
            Database.executeBatch(new BAT18_DeleteDocumentIdData());
            Test.stopTest();
            actuelSize = [SELECT Count() FROM Editique_Document_ID__c];
            System.assertEquals(0, actuelSize);
        }
    }
    
}