public with sharing class VFC09_AfterProdReqLineItemSelection {
    
    private ApexPages.StandardSetController setCon;
    public List<ProductRequestLineItem>lstSelectedRecs {get;set;}
    public String selectedRecords{get;set;}
    public String filterId{get;set;}

    public VFC09_AfterProdReqLineItemSelection(ApexPages.StandardSetController controller) {
        System.debug('##lst Product request line');
        setCon = controller;
        lstSelectedRecs = (List<ProductRequestLineItem>) setCon.getSelected();
        filterId = setCon.getFilterId();
        System.debug('## FilterId : ' + filterId);
        selectedRecords = '';
        for(ProductRequestLineItem prli : lstSelectedRecs){
            System.debug('## Prli: ' + prli);
            selectedRecords += prli.Id + ',';
        }

        selectedRecords = selectedRecords.removeEnd(',');

        System.debug('## selected Records: ' + selectedRecords);
        System.debug('## lstSelectedProdReqLineItem : ' + lstSelectedRecs);
        System.debug('##VFC09_AfterProdReqLineItemSelection end');
    }
}