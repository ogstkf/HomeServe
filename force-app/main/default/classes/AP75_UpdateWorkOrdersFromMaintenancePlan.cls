/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 28-10-2021
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   08-25-2020   ZJO   Initial Version
**/
public with sharing class AP75_UpdateWorkOrdersFromMaintenancePlan {

    public static void updWOGenerateFromMaintenancePlan(List<WorkOrder> lstWo, map<string,string> mapWOIdSCId){
        System.debug('## ap68 updWOGenerateFromMaintenancePlan start');

        Set<Id> setAccId = new Set<Id>();
        map<String, ServiceContract> mapScIdSC = new map <String, ServiceContract>();
        Map<Id, Account> mapAcc;

        //TEC 819  MNA 28/10/2021 
        for (WorkOrder WO: lstWo) {
            if (WO.AccountId != null)
                setAccId.add(WO.AccountId);
        }
        mapAcc = new Map<Id, Account>([SELECT Id, (SELECT Id, Contact_principal__c FROM Contacts ORDER BY Contact_principal__c DESC) FROM Account WHERE Id IN :setAccId AND RecordType.DeveloperName != 'PersonAccount']);

        for(ServiceContract sc : [SELECT id, RecordType.DeveloperName, Agency__c, ContactId, Account.Campagne__c, Asset__c, Asset__r.Logement__r.Inhabitant__c, Asset__r.Logement__r.Inhabitant__r.RecordTypeId, Asset__r.Logement__r.Inhabitant__r.PersonContactId, Asset__r.Logement__r.Inhabitant__r.Main_Contact__c, Asset__r.Logement__r.City__c, Asset__r.Logement__r.Country__c, Asset__r.Logement__r.Postal_Code__c, Asset__r.Logement__r.Street__c FROM ServiceContract WHERE id IN: mapWOIdSCId.Values()]){
            mapScIdSC.put(sc.Id, sc);
        }
        
        for(WorkOrder wo : lstWo){
            wo.AssetId = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__c;
            wo.ServiceTerritoryId = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Agency__c;
            wo.Type__c = 'Maintenance';
            wo.Reason__c = 'Visite sous contrat';
            wo.Campagne__c = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Account.Campagne__c;
            wo.Acknowledged_On__c = system.now();
            wo.Status = 'New';
            wo.City = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__r.Logement__r.City__c;
            wo.Country = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__r.Logement__r.Country__c; 
            wo.PostalCode = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__r.Logement__r.Postal_Code__c;
            wo.Street = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__r.Logement__r.Street__c;
            
            //Contrats Individuels
            if(mapScIdSC.get(mapWOIdSCId.get(wo.Id)).RecordType.DeveloperName == 'Contrats_Individuels'){
                wo.ContactId = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).ContactId;
            }

            //Contrats Collectifs
            if(mapScIdSC.get(mapWOIdSCId.get(wo.Id)).RecordType.DeveloperName == 'Contrats_collectifs_publics' || mapScIdSC.get(mapWOIdSCId.get(wo.Id)).RecordType.DeveloperName == 'Contrats_Collectifs_Prives'){
                wo.AccountId = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__r.Logement__r.Inhabitant__c;
                
                //Check Type of Account (Person/Business)
                if(mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__r.Logement__r.Inhabitant__r.RecordTypeId == AP_Constant.getRecTypeId('Account', 'PersonAccount')){
                    wo.ContactId = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__r.Logement__r.Inhabitant__r.PersonContactId;
                }else{
                    wo.ContactId = mapScIdSC.get(mapWOIdSCId.get(wo.Id)).Asset__r.Logement__r.Inhabitant__r.Main_Contact__c;
                }    
            }

            //TEC 819  MNA 28/10/2021 for Business Account
            if (mapAcc.containsKey(wo.AccountId) && mapAcc.get(wo.AccountId).Contacts.size() > 0) {
                wo.ContactId = mapAcc.get(WO.AccountId).Contacts[0].Id;
            }
        }
    }

}