/**
 * @File Name          : BAT10_UpdateInventaireSA.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : ZJO
 * @Last Modified On   : 01/04/2020, 16:30:18
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/03/2020   AMO     Initial Version
**/
global with sharing class BAT10_UpdateInventaireSA implements Database.Batchable<Sobject> {

    private List<ServiceTerritory> lstServiceTerritory;
    private Set<Id> setAgenceId;
    
    public BAT10_UpdateInventaireSA(Set<Id> setAgencyId){
        System.debug('Batch 3 agence: ' + setAgencyId);
        setAgenceId = setAgencyId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('BAT10_UpdateInventaireSA');
        String query = 'SELECT Id, Inventaire_en_cours__c FROM ServiceAppointment WHERE  ServiceTerritoryId IN :setAgenceId AND Inventaire_en_cours__c = true';
        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext BC, List<ServiceAppointment> lstServiceAppointment) {
        List<ServiceAppointment> lstSAs = new List<ServiceAppointment>();
        Set<Id> setWkOrderIds = new Set<Id>();
        Map<Id, Set<Id>> mapWotoSas = new Map<Id, Set<Id>>(); 
        List<ServiceAppointment> lstSAToUpdate = new List<ServiceAppointment>();
        
        //Retrieve Work Order
        for(ServiceAppointment sa : [SELECT Id, Work_Order__c, Inventaire_en_cours__c FROM ServiceAppointment WHERE Id IN :lstServiceAppointment]){
            setWkOrderIds.add(sa.Work_Order__c);
            lstSAs.add(sa);
        }

        System.debug('## SetWorkOrderId :' + setWkOrderIds);
        System.debug('## List of SAs :' + lstSAs);

        //Build MapWoToSas

        for (ServiceAppointment servApp : lstSAs){

            servApp.Inventaire_en_cours__c = false;
            lstSAToUpdate.add(servApp);

            If (mapWotoSas.containsKey(servApp.Work_Order__c)){
                mapWotoSas.get(servApp.Work_Order__c).add(servApp.Id);
            }else{
                mapWotoSas.put(servApp.Work_Order__c, new Set<Id> {servApp.Id});
            }
        }

        System.debug('## Map WO To SAs :' + mapWotoSas);

        //Call function in AP34
        AP34_CreateProductConsumed.createProdConsumed(lstSAs, setWkOrderIds, mapWotoSas);

        //Update SAs

        System.debug('## List of SAs to update :' + lstSAToUpdate);

        If (lstSAToUpdate.size() > 0){
            update lstSAToUpdate;
        }
        
    }

    global void finish(Database.BatchableContext BC) {

        // system.debug('## finish method ');
        // system.debug('Number of records processed: ' + recordsProcessed);
        
    }

}