/**
* @author  SC (CLA)
* @version 1.0
* @since   28/05/2020
*
* Description : For Jira ticket CT-1795
* 
*-- Maintenance History: 
*--
*-- Date         Name  Version  Remarks 
*-- -----------  ----  -------  ------------------------
*-- 28-05-2020   CLA    1.0     Initial version
*-------------------------------------------------------
* 
*/

global with sharing class BAT12_CreateServiceContractCollectifs implements Database.Batchable <sObject>, Database.Stateful, Schedulable{

    Integer numDaysPrior = Integer.valueOf(System.Label.NbDaysPriorRenewalCollectifs);

    public static String query = ' SELECT Type__c, Name, Numero_du_contrat__c, Gestionnaire_Syndic__c, ParentServiceContractId, ParentServiceContract.EndDate, '
                    + ' RootServiceContractId, Lot__c, Logement__c, AccountId, ContactId, Payeur_du_contrat__c, Agency__c, '
                    + ' Contract_Status__c, StartDate, EndDate, Term, Mode_de_facturation__c, Type_de_facturation__c, '
                    + ' Sofactoapp_Mode_de_paiement__c, Ech_ancier_Facturation__c, Sofacto_DateOfSignature__c, '
                    + ' Date_de_1ere_facture__c, sofactoapp_Rib_prelevement__c, Echeancier_Paiement__c, '
                    + ' Indice_1__c, Indice_2__c, Indice_3__c, Indice_4__c, '
                    + ' Valeur_d_indice_reference1__c, Valeur_d_indice_reference2__c, Valeur_d_indice_reference_3__c, Valeur_d_indice_reference_4__c, '
                    + ' Nombre_de_renouvellements_restants__c, Tacite_reconduction__c, Option_P2_avec_seuil__c, Type_de_seuil__c, '
                    + ' Valeur_du_seuil_P2__c, P3_Cap_valeur_du_seuil__c, BillingAddress, ShippingAddress, Pricebook2Id, '
                    + ' RecordTypeId, '
                    + ' RootServiceContract.Prise_en_compte_des_indices__c '
                    + ' FROM ServiceContract'
                    + ' WHERE TECH_BypassBAT__c = false '
                    + ' AND Contract_Status__c = \'Active\' '
                    + ' AND ( RecordType.Developername = \'Contrats_collectifs_publics\'  '
                    + '       OR RecordType.Developername = \'Contrats_Collectifs_Prives\' ) '
                    + ' AND Type__c = \'Collective\' '
                    + ' AND Nombre_de_renouvellements_restants__c >= 1 '
                    + ' AND Contrat_resilie__c = false '
                    + ' AND ParentServiceContractId != null ';

    global BAT12_CreateServiceContractCollectifs(){
        query += ' AND TECH_NumberOfDaysPriorEndDate__c = '+numDaysPrior;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){     
        System.debug('**** BAT12_CreateServiceContractCollectifs start *****');
        System.Debug('### query : ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ServiceContract> lstOldCon) { 
        System.debug('**** BAT12_CreateServiceContractCollectifs execute *****');
        createNewContracts(lstOldCon);
    }

    public static List<ServiceContract> createNewContracts(List<ServiceContract> lstOldCon){
        String soqlSelectIndice = 'SELECT id, sofactoapp__Debut__c, sofactoapp__Indice__c FROM sofactoapp__Valeur_indice__c WHERE ';
        String soqlWhereIndice = ' ';
        Map<String, String> mapDateIndexKeyToIndiceId = new Map<String, String>();
        //Map<String, List<Revalorisation_annuelle__c>> mapConIdToLstRA = new Map<String, List<Revalorisation_annuelle__c>>();

        Map<String, Decimal> mapContractIdProductToCoeffOld = new Map<String, Decimal>();

		List<Revalorisation_annuelle__c> lstOldRevalorisation = [
			SELECT
				A__c,
				B__c,
				C__c,
				D__c,
				E__c,
				Contrat_de_service__c,
				Formule_de_revalorisation__c,
				Indice_1_origine__c,
				Indice_2_origine__c,
				Indice_3_origine__c,
				Indice_4_origine__c,
				Indice_1_en_cours__c,
				Indice_1_en_cours__r.sofactoapp__Debut__c,
				Indice_1_en_cours__r.sofactoapp__Indice__c,
				Indice_2_en_cours__c,
				Indice_2_en_cours__r.sofactoapp__Debut__c,
				Indice_2_en_cours__r.sofactoapp__Indice__c,
				Indice_3_en_cours__c,
				Indice_3_en_cours__r.sofactoapp__Debut__c,
				Indice_3_en_cours__r.sofactoapp__Indice__c,
				Indice_4_en_cours__c,
				Indice_4_en_cours__r.sofactoapp__Debut__c,
				Indice_4_en_cours__r.sofactoapp__Indice__c,
				Produit__c,
				Type__c,
				Cap_de_revalorisation__c,
				Coefficient_de_revalorisation__c,
				Revalorisation_precedente__r.Coefficient_de_revalorisation__c,
				Revalorisation_precedente__r.Cap_de_revalorisation__c
			FROM Revalorisation_annuelle__c
			WHERE Contrat_de_service__c IN :lstOldCon
		];

		Map<Id, Revalorisation_annuelle__c> mapOldRA = new Map<Id, Revalorisation_annuelle__c>(lstOldRevalorisation);

		System.debug('####lstrevaluation : ' + lstOldRevalorisation);
		System.debug('####mapOldRA : ' + mapOldRA);

		if (lstOldRevalorisation.size() > 0) {
			for (Revalorisation_annuelle__c ra : lstOldRevalorisation) {
				// Gathing all the new StartDate (old StartDate + 1 year) used to retrieve new Indice
				if (ra.Indice_1_en_cours__c != null) {
					Date startDate1 = ra.Indice_1_en_cours__r.sofactoapp__Debut__c.addYears(1);
					soqlWhereIndice += ' ( CALENDAR_MONTH(sofactoapp__Debut__c) = ' + startDate1.month();
					soqlWhereIndice += ' AND CALENDAR_YEAR(sofactoapp__Debut__c) = ' + startDate1.year();
					soqlWhereIndice += ' AND sofactoapp__Indice__c = \'' + ra.Indice_1_en_cours__r.sofactoapp__Indice__c + '\') OR ';
				}

				if (ra.Indice_2_en_cours__c != null) {
					Date startDate2 = ra.Indice_2_en_cours__r.sofactoapp__Debut__c.addYears(1);
					soqlWhereIndice += ' ( CALENDAR_MONTH(sofactoapp__Debut__c) = ' + startDate2.month();
					soqlWhereIndice += ' AND CALENDAR_YEAR(sofactoapp__Debut__c) = ' + startDate2.year();
					soqlWhereIndice += ' AND sofactoapp__Indice__c = \'' + ra.Indice_2_en_cours__r.sofactoapp__Indice__c + '\') OR ';
				}

				if (ra.Indice_3_en_cours__c != null) {
					Date startDate3 = ra.Indice_3_en_cours__r.sofactoapp__Debut__c.addYears(1);
					soqlWhereIndice += ' ( CALENDAR_MONTH(sofactoapp__Debut__c) = ' + startDate3.month();
					soqlWhereIndice += ' AND CALENDAR_YEAR(sofactoapp__Debut__c) = ' + startDate3.year();
					soqlWhereIndice += ' AND sofactoapp__Indice__c = \'' + ra.Indice_3_en_cours__r.sofactoapp__Indice__c + '\') OR ';
				}

				if (ra.Indice_4_en_cours__c != null) {
					Date startDate4 = ra.Indice_4_en_cours__r.sofactoapp__Debut__c.addYears(1);
					soqlWhereIndice += ' ( CALENDAR_MONTH(sofactoapp__Debut__c) = ' + startDate4.month();
					soqlWhereIndice += ' AND CALENDAR_YEAR(sofactoapp__Debut__c) = ' + startDate4.year();
					soqlWhereIndice += ' AND sofactoapp__Indice__c = \'' + ra.Indice_4_en_cours__r.sofactoapp__Indice__c + '\') OR ';
				}

				// map contract id to list Revalorisation
				if (mapConIdToLstRA.containsKey(ra.Contrat_de_service__c)) {
					mapConIdToLstRA.get(ra.Contrat_de_service__c).add(ra);
				} else {
					mapConIdToLstRA.put(ra.Contrat_de_service__c, new List<Revalorisation_annuelle__c>{ ra });
				}

				String key = ra.Contrat_de_service__c + '-' + ra.Produit__c;
				if (ra.Revalorisation_precedente__r.Cap_de_revalorisation__c == null) {
					mapContractIdProductToCoeffOld.put(key, ra.Revalorisation_precedente__r.Coefficient_de_revalorisation__c);
				} else {
					mapContractIdProductToCoeffOld.put(key, math.min(ra.Revalorisation_precedente__r.Coefficient_de_revalorisation__c, ra.Revalorisation_precedente__r.Cap_de_revalorisation__c));
				}
			}

			soqlWhereIndice = soqlWhereIndice.removeEnd(' OR ');

			// System.debug('soqlSelectIndice + soqlWhereIndice: ' + soqlSelectIndice + soqlWhereIndice);
			for (sofactoapp__Valeur_indice__c indice : Database.query(soqlSelectIndice + soqlWhereIndice)) {
				String key = indice.sofactoapp__Debut__c.month() + '-' + indice.sofactoapp__Debut__c.year() + '-' + indice.sofactoapp__Indice__c;
				mapDateIndexKeyToIndiceId.put(key, indice.Id);

				// System.debug('### indice key: ' + key + ' -> ' + indice.Id);
			}
        }
        
        // Set<Id> setAgenceId = new Set<Id>(); // used to get TUL
        Map<Id, ServiceContract> mapOldConIdToNewCon = new Map<Id, ServiceContract>();
        //Map<String, List<Revalorisation_annuelle__c>> mapOldConIdToNewLstRA = new Map<String, List<Revalorisation_annuelle__c>>();

        for(ServiceContract oldCon : lstOldCon){
            // setAgenceId.add(oldCon.Agency__c);

            ServiceContract newCon = oldCon.clone(false, false, false, false);
            newCon.Contract_Renewed__c = true;
            newCon.Contract_Status__c = 'En attente de renouvellement';
            newCon.StartDate = oldCon.StartDate.addYears(1);
            newCon.EndDate = oldCon.EndDate.addYears(1);
            newCon.Term = 12;
            newCon.Name = oldCon.Name.split('_')[0] + '_' + String.valueOf(newCon.StartDate);

            if(newCon.EndDate > oldCon.ParentServiceContract.EndDate){
                newCon.EndDate = oldCon.ParentServiceContract.EndDate;
                newCon.Term = newCon.StartDate.monthsBetween(newCon.EndDate) + 1;
            }

            newCon.Nombre_de_renouvellements_restants__c = oldCon.Nombre_de_renouvellements_restants__c - 1;
            
            mapOldConIdToNewCon.put(oldCon.Id, newCon);

            // if old contract has Revalorisation Annuelle
            /*if(mapConIdToLstRA.containsKey(oldCon.Id)){
                List<Revalorisation_annuelle__c> lstRAtoCreate = new List<Revalorisation_annuelle__c>();

                for(Revalorisation_annuelle__c ra : mapConIdToLstRA.get(oldCon.Id)){
                    Revalorisation_annuelle__c newRA = ra.clone(false, false, false, false);

					newRa.Revalorisation_precedente__c = ra.Id;
                    newRA.Contrat_de_service__c = null;

                    if(oldCon.RootServiceContract.Prise_en_compte_des_indices__c == 'Indices de l\'année précédente'){
                        newRA.Indice_1_origine__c = ra.Indice_1_en_cours__c;
                        newRA.Indice_2_origine__c = ra.Indice_2_en_cours__c;
                        newRA.Indice_3_origine__c = ra.Indice_3_en_cours__c;
                        newRA.Indice_4_origine__c = ra.Indice_4_en_cours__c;
                    }

                    if(ra.Indice_1_en_cours__c != null){
                        Date startDate1 = ra.Indice_1_en_cours__r.sofactoapp__Debut__c.addYears(1);
                        String key1 = startDate1.month() +'-'+ startDate1.year() +'-'+  ra.Indice_1_en_cours__r.sofactoapp__Indice__c;
                        if(!mapDateIndexKeyToIndiceId.containsKey(key1)){
							newRA.Indice_1_en_cours__c = mapDateIndexKeyToIndiceId.get(key1);
                        }
                        
                    }
        
                    if(ra.Indice_2_en_cours__c != null){
                        Date startDate2 = ra.Indice_2_en_cours__r.sofactoapp__Debut__c.addYears(1);
                        String key2 = startDate2.month() +'-'+ startDate2.year() +'-'+  ra.Indice_2_en_cours__r.sofactoapp__Indice__c;
                        if(!mapDateIndexKeyToIndiceId.containsKey(key2)){
							newRA.Indice_2_en_cours__c = mapDateIndexKeyToIndiceId.get(key2);
                        }

                    }
        
                    if(ra.Indice_3_en_cours__c != null){
                        Date startDate3 = ra.Indice_3_en_cours__r.sofactoapp__Debut__c.addYears(1);
                        String key3 = startDate3.month() +'-'+ startDate3.year() +'-'+  ra.Indice_3_en_cours__r.sofactoapp__Indice__c;
                        if(!mapDateIndexKeyToIndiceId.containsKey(key3)){
							newRA.Indice_3_en_cours__c = mapDateIndexKeyToIndiceId.get(key3);
                        }

                    }
        
                    if(ra.Indice_4_en_cours__c != null){
                        Date startDate4 = ra.Indice_4_en_cours__r.sofactoapp__Debut__c.addYears(1);
                        String key4 = startDate4.month() +'-'+ startDate4.year() +'-'+  ra.Indice_4_en_cours__r.sofactoapp__Indice__c;
                        if(!mapDateIndexKeyToIndiceId.containsKey(key4)){
                        newRA.Indice_4_en_cours__c = mapDateIndexKeyToIndiceId.get(key4);
                        }

                    }

                    if(mapOldConIdToNewLstRA.containsKey(oldCon.Id)){
                        mapOldConIdToNewLstRA.get(oldCon.Id).add(newRA);
                    }
                    else{
                        mapOldConIdToNewLstRA.put(oldCon.Id, new List<Revalorisation_annuelle__c>{newRA});
                    }
                }
                  
                // if number old RA != number new RA, do not clone the contract
                if(( mapOldConIdToNewLstRA.containsKey(oldCon.Id) && 
                        mapConIdToLstRA.get(oldCon.Id).size() != mapOldConIdToNewLstRA.get(oldCon.Id).size() )
                    || !mapOldConIdToNewLstRA.containsKey(oldCon.Id) ){
                    mapOldConIdToNewCon.remove(oldCon.Id);
                }

            }*/
        }

        if(mapOldConIdToNewCon.size() > 0){
            insert mapOldConIdToNewCon.values();
        }

        // old contract that were cloned, set TECH_BypassBAT__c = true
        List<ServiceContract> lstOldToUpdateTechBypass = new List<ServiceContract>();
        for(String conId : mapOldConIdToNewCon.keySet()){
            lstOldToUpdateTechBypass.add(new ServiceContract(id = conId, TECH_BypassBAT__c = true));
        }
        if(lstOldToUpdateTechBypass.size() > 0){
            update lstOldToUpdateTechBypass;
        }


        // after insert of new contract, loop in contract, and add contract Id to RA before insert new RA
        /*List<Revalorisation_annuelle__c> lstRAtoInsert = new List<Revalorisation_annuelle__c>();
        for(String oldConId : mapOldConIdToNewCon.keySet()){

            if(mapOldConIdToNewLstRA.containsKey(oldConId)){
                for(Revalorisation_annuelle__c ra : mapOldConIdToNewLstRA.get(oldConId)){
                    ra.Contrat_de_service__c = mapOldConIdToNewCon.get(oldConId).Id;
                    lstRAtoInsert.add(ra);
                }
            }
        }*/

        /*Map<String, Decimal> mapContractIdProductToCoeff = new Map<String, Decimal>(); //used to calculate new UnitPrice of cloned CLI
        if(lstRAtoInsert.size() > 0){
            insert lstRAtoInsert;

            for(Revalorisation_annuelle__c newRA : [SELECT Coefficient_de_revalorisation__c, Cap_de_revalorisation__c, Produit__c, Contrat_de_service__c
                                                    FROM Revalorisation_annuelle__c
                                                    WHERE id IN :lstRAtoInsert]){
                String key = newRA.Contrat_de_service__c +'-'+ newRA.Produit__c;
                
                if (mapOldRA.containsKey(newRA.Revalorisation_precedente__c)) {
					Revalorisation_annuelle__c oldRA = mapOldRA.get(newRA.Revalorisation_precedente__c);
					if (newRA.Cap_de_revalorisation__c == null) {
						mapContractIdProductToCoeffNew.put(key, oldRA.Coefficient_de_revalorisation__c);
					} else {
						mapContractIdProductToCoeffNew.put(key, math.min(oldRA.Coefficient_de_revalorisation__c, oldRA.Cap_de_revalorisation__c));
					}
				}
            }
            
            System.debug('### mapContractIdProductToCoeff: '+mapContractIdProductToCoeff);
        }*/

        List<ContractLineItem> lstOldCLI = [SELECT Id, Product2Id, ServiceContractId, Description, AccountId__c,
                                                IsBundle__c, StartDate, EndDate, Quantity, UnitPrice, Discount,
                                                Collective_Account__c, AssetId, Raisons_de_la_suspension__c,
                                                Fin_de_la_suspension__c, Debut_de_la_suspension__c,
                                                VE_Number_Target__c, VE_Number_Counter__c,
                                                Statut_facturation__c,
                                                PricebookEntryId, PricebookEntry.Product2Id, 
                                                PricebookEntry.Product2.IsBundle__c,
                                                ListPrice, TVA__c, Remplacement_effectue__c,
                                                Desired_VE_Date__c, VE_Min_Date__c, VE_Max_Date__c,
                                                Completion_Date__c
                                            FROM ContractLineItem 
                                            WHERE ServiceContractId IN :mapOldConIdToNewCon.keySet() 
                                            AND ( IsActive__c = true 
                                                    OR (IsActive__c = false 
                                                        AND debut_de_la_suspension__c != null
                                                        AND fin_de_la_suspension__c != null)
                                                )];

        System.debug(lstOldCLI);

        Map<Id, List<ContractLineItem>> mapOldConIdToOldClis = new Map<Id, List<ContractLineItem>>();
        Set<Id> setProdId = new Set<Id>();
        Set<Id> setBundleProdId = new Set<Id>(); // used to get TULS

        for(ContractLineItem cli : lstOldCLI){
            setProdId.add(cli.PricebookEntry.Product2Id);
            if(mapOldConIdToOldClis.containsKey(cli.ServiceContractId)){
                mapOldConIdToOldClis.get(cli.ServiceContractId).add(cli);
            }else{
                mapOldConIdToOldClis.put(cli.ServiceContractId, new List<ContractLineItem>{cli});
            }

            if(cli.PricebookEntry.Product2.IsBundle__c){
                setBundleProdId.add(cli.PricebookEntry.Product2Id);
            }
        }

        List<Product_Bundle__c> lstProdBundleParent = [SELECT Id, Bundle__c, Price__c, Optional_Item__c, Product__c 
                                                        FROM Product_Bundle__c 
                                                        WHERE Product__r.isBundle__c = true 
                                                        AND Product__c IN :setBundleProdId 
                                                        ORDER BY Bundle__c];
        
        // Map<Id, List<Id>> mapProdToBundles = new Map<Id, List<Id>>();
        Set<Id> setBundleId = new Set<Id>();
        
        for(Product_Bundle__c prodBun : lstProdBundleParent){
            setBundleId.add(prodBun.Bundle__c);
        }

        List<Product_Bundle__c> lstOptionBundles = [SELECT Id, Bundle__c, Price__c, Optional_Item__c, Product__c 
                                                    FROM Product_Bundle__c 
                                                    WHERE Optional_Item__c = true 
                                                    AND Bundle__c IN :setBundleId 
                                                    ORDER BY Bundle__c];

        Map<Id, Set<Id>> mapBundleToOption = new Map<Id, Set<Id>>();
        for(Product_Bundle__c prodBun : lstOptionBundles){
            if(mapBundleToOption.containsKey(prodBun.Bundle__c)){
                mapBundleToOption.get(prodBun.Bundle__c).add(prodBun.Product__c);
            }else{
                mapBundleToOption.put(prodBun.Bundle__c, new Set<Id>{prodBun.Product__c});
            }
        }

        Map<String, PricebookEntry> mapAllPbe = new Map<String, PricebookEntry>();
        // NEW Pricebook 
        List<PricebookEntry> lstPBE = [SELECT Id, Product2Id, Pricebook2Id, UnitPrice, Pricebook2.Active_for_renewal__c, Pricebook2.IsActive 
                                        FROM PricebookEntry 
                                        WHERE Pricebook2.Active_for_renewal__c = true 
                                        AND Pricebook2.Ref_Agence__c = true 
                                        AND Product2.recordtype.developername='Cham_offer' 
                                        AND Product2Id IN :setProdId 
                                        AND IsActive = true];

        Map<Id, PricebookEntry> mapRenewalProdIdToPbe = new Map<Id, PricebookEntry>();
        
        for(PricebookEntry pbe: lstPBE){
            mapRenewalProdIdToPbe.put(pbe.Product2Id, pbe);
            mapAllPbe.put(pbe.Pricebook2Id+'-'+pbe.Product2Id, pbe);
        }
        
        // Existing Pricebook
        List<PricebookEntry> lstExistingPBE = [SELECT Id, Product2Id, Pricebook2Id, UnitPrice, Pricebook2.Active_for_renewal__c, Pricebook2.IsActive 
                                                FROM PricebookEntry 
                                                WHERE Product2Id IN: setProdId 
                                                AND Pricebook2.Ref_Agence__c = true 
                                                AND Pricebook2.IsActive = true 
                                                AND IsActive = true];
        
        Map<Id, PricebookEntry> mapNormalProdIdToPbe = new Map<Id, PricebookEntry>();
        for(PricebookEntry pbe : lstExistingPBE){
            mapNormalProdIdToPbe.put(pbe.Product2Id, pbe);
            mapAllPbe.put(pbe.Pricebook2Id+'-'+pbe.Product2Id, pbe);
        }

        System.debug(mapOldConIdToOldClis);

        Map<Id, ServiceContract> mapConToUpdt = new Map<Id, ServiceContract>();
        Map<Id, List<ContractLineItem>> mapNewConIdToCLI = new Map<Id, List<ContractLineItem>>();

        for(Id oldConId : mapOldConIdToNewCon.keySet()){
            if(mapOldConIdToOldClis.containsKey(oldConId)){
                Decimal oldBundlePrc;
                Id parentBundleId;

                for(ContractLineItem oldCli: mapOldConIdToOldClis.get(oldConId)){
                    ServiceContract newCon = mapOldConIdToNewCon.get(oldConId);
                    
                    ContractLineItem newCli = oldCli.clone(false, false, false, false);
                    newCli.ServiceContractId = newCon.Id;
                    newCli.Discount = 0;
                    newCli.VE_Number_Counter__c = 0;
                    newCli.Statut_facturation__c = null;
                    newCli.IsActive__c = false;

                    if(oldCli.Desired_VE_Date__c != null){
                        newCli.Desired_VE_Date__c = oldCli.Desired_VE_Date__c.addYears(1);
                    }

                    if(oldCli.VE_Min_Date__c != null){
                        newCli.VE_Min_Date__c = oldCli.VE_Min_Date__c.addYears(1);
                    }

                    if(oldCli.VE_Max_Date__c != null){
                        newCli.VE_Max_Date__c = oldCli.VE_Max_Date__c.addYears(1);
                    }
                    
                    newCli.TECH_CompletionDateAnnee_M_1__c = oldCli.Completion_Date__c;
                    newCli.Completion_Date__c = null;
                    
                    Boolean isBundle = oldCli.PricebookEntry.Product2.IsBundle__c;
                    Boolean isOption = false;
                    if(isBundle){
                        oldBundlePrc = oldCli.UnitPrice;
                        
                        if(mapRenewalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                            newCli.PricebookEntryId = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                            // newCli.UnitPrice = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                            newCon.Pricebook2Id = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                            mapConToUpdt.put(newCon.Id, newCon);
                        }else if(mapNormalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                            newCli.PricebookEntryId = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                            // newCli.UnitPrice = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                            newCon.Pricebook2Id = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                            mapConToUpdt.put(newCon.Id, newCon);
                        }                    
                        newCli.VE__c = true;
                    }else{
                        if(mapBundleToOption.containsKey(parentBundleId)){
                            isOption = mapBundleToOption.get(parentBundleId).contains(oldCli.PricebookEntry.Product2Id);
                        }
                    }

                    if(isOption){
                        if(mapRenewalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                            newCli.PricebookEntryId = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                            newCli.UnitPrice = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                            newCon.Pricebook2Id = mapRenewalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                        }else if(mapNormalProdIdToPbe.containsKey(oldCli.PricebookEntry.Product2Id)){
                            newCli.PricebookEntryId = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Id;
                            newCli.UnitPrice = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).UnitPrice;
                            newCon.Pricebook2Id = mapNormalProdIdToPbe.get(oldCli.PricebookEntry.Product2Id).Pricebook2Id;
                        }
                        mapConToUpdt.put(newCon.Id, newCon);
                    }

                    // calculating new UnitPrice
                    /*System.debug('### coeff key: '+ newCon.Id + '-' + newCli.PricebookEntry.Product2Id);
                    Decimal coeff = mapContractIdProductToCoeff.get(newCon.Id + '-' + newCli.PricebookEntry.Product2Id);
                    if(coeff != null){
                        Decimal newUnitPrice = oldCli.UnitPrice * coeff;
                        newCli.UnitPrice = newUnitPrice.setScale(2, RoundingMode.HALF_UP);
                    }*/

                    if(mapNewConIdToCLI.containsKey(newCon.Id)){
                        mapNewConIdToCLI.get(newCon.Id).add(newCli);
                    }else{
                        mapNewConIdToCLI.put(newCon.Id, new List<ContractLineItem>{newCli});
                    }
                    
                    System.debug(newCli);
                }
            }
            System.debug(mapNewConIdToCLI);
            System.debug(mapConToUpdt);

            List<ContractLineItem> lstNewCli = new List<ContractLineItem>();
            for(ServiceContract con : mapConToUpdt.values()){
                /*for(ContractLineItem cli : mapNewConIdToCLI.get(con.Id)){
                    if(mapAllPbe.containsKey(con.Pricebook2Id+'-'+cli.Product2Id)){
                        cli.PricebookEntryId = mapAllPbe.get(con.Pricebook2Id+'-'+cli.Product2Id).Id;
                        lstNewCli.add(cli);
                    }
                }*/
                for (Integer i = 0; i< mapNewConIdToCLI.get(con.Id).size(); i++) {
                    if(mapAllPbe.containsKey(con.Pricebook2Id+'-'+mapNewConIdToCLI.get(con.Id)[i].Product2Id)){
                        mapNewConIdToCLI.get(con.Id)[i].PricebookEntryId = mapAllPbe.get(con.Pricebook2Id+'-'+mapNewConIdToCLI.get(con.Id)[i].Product2Id).Id;
                    }
                }
            }
            if(mapConToUpdt.size()>0){
                AP48_GestionTVA.getTVAServContr(new List<Id>(mapConToUpdt.keySet()));
                update mapConToUpdt.values();
            }
            for (String strKey:mapNewConIdToCLI.KeySet()) {
                lstNewCli.addAll(mapNewConIdToCLI.get(strKey));
            }

            if(lstNewCli.size()>0){
                upsert lstNewCli;
            }

        }

        return mapOldConIdToNewCon.values();
    }

    global void finish(Database.BatchableContext BC) {
        System.debug('**** BAT12_CreateServiceContractCollectifs finish *****');
    }

    global void execute(SchedulableContext sc){
        batchConfiguration__c batchConfig = batchConfiguration__c.getValues('BAT12_CreateServiceContractCollectifs');

        if(batchConfig != null && batchConfig.BatchSize__c != null){
            Database.executeBatch(new BAT12_CreateServiceContractCollectifs(), Integer.valueof(batchConfig.BatchSize__c));
        }
        else{
            Database.executeBatch(new BAT12_CreateServiceContractCollectifs());
        }
    }
}