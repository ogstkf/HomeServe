/**
 * Name               : VFC76_AvisDePassageEditique
 * @description       : 
 * @author            : MNA
 * @group             : 
 * @last modified on  : 07-12-2021
 * @last modified by  : MNA
 * Modifications Log 
 * ================================================================================
 * Ver   Date           Author                               Modification
 * 1.0   03-26-2021     MNA                                  Initial Version
**/
public without sharing class VFC76_AvisDePassageEditique {
    public String listViewName {get;set;}
    public String columns {get;set;}
    public String query {get;set;}

    public VFC76_AvisDePassageEditique(Apexpages.StandardSetController controller) {
        listViewName = [SELECT Name FROM ListView WHERE Id =:controller.getFilterId()].Name;

        dataWrapper dw = describe('ServiceAppointment', controller.getFilterId());
        Integer length = dw.columns.size();
        for (Integer i = 0; i < length; i++) {
            if (dw.columns[i].hidden) {
                dw.columns.remove(i);
                i--;
                length--;
            }
            else {
                //dw.columns[i].label = String.join(dw.columns[i].label.split('\''), '');
                switch on dw.columns[i].type {
                    when 'id', 'reference', 'string', 'picklist', 'textarea', 'address' {
                        dw.columns[i].type = 'text';
                    }
                    when 'datetime' {
                        dw.columns[i].type = 'date';
                    }
                    when 'double' {
                        dw.columns[i].type = 'number';
                    }
                    when else {
                        
                    }
                }
            }
        }

        columns = JSON.serialize((Object) dw.columns);   
        columns = String.join(columns.split('\''), '789');
        query = String.join(dw.query.split('\''), '"');
    }

    /*public PageReference redirectToLC(){
        String returnUrl = '/lightning/cmp/c__LC76GenerateMassAvisDePassage';
        PageReference pgReturnPage = new PageReference(returnUrl);
        //pgReturnPage.getParameters().put('c__listofSA', SAIds);
        pgReturnPage.setRedirect(true);
        System.debug(pgReturnPage);        
        return pgReturnPage;
    }*/
    
    private dataWrapper describe(String objType, String listViewId){
        String endpoint = URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v52.0/sobjects/'+objType+'/listviews/'+listViewId+'/describe';
        String body = request(endpoint);
        dataWrapper dw = (dataWrapper)json.deserialize(body, dataWrapper.class);
        
        return dw;
    }

    private String request(String endpoint) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        
        req.setEndpoint(endpoint);
        HttpResponse res = new Http().send(req);
        system.debug('Res body: '+res.getBody());
        return res.getBody();
    }

    public class dataWrapper{
        String query {get;set;}
        List<columnWrapper> columns {get;set;}
    }

    public class columnWrapper {
        String fieldNameOrPath {get;set;}
        public Boolean hidden {get;set;}
        String label {get;set;}
        String type {get;set;}
    }
}