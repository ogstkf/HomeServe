/**
 * @File Name         : LC78_QuoteCustomCard.cls
 * @Description       : 
 * @Author            : MNA
 * @group             : 
 * @last modified by  : MNA
 * @last modified on  : 17-06-2021
 * Modifications Log 
 * ==============================================================
 * Ver   Date               Author          Modification
 * 1.0   28-05-2021         MNA             Initial Version
**/
public without sharing class LC78_QuoteCustomCard {
    @AuraEnabled
    public static Object getData(Id accId){
        try {
            List <wrapperQuotesWithType> lstwrpQuoType = new List <wrapperQuotesWithType>();
            for (Quote quo :[SELECT Id, Name, Devis_etabli_le__c, Prix_TTC__c, Status, QuoteNumber,Nom_du_payeur__c, Owner__c, Inhabitant__c FROM Quote
                                        WHERE Nom_du_payeur__c =:accId OR Owner__c =:accId OR Inhabitant__c =:accId]) {
                String type = '';
                
                if (quo.Nom_du_payeur__c == accId)
                    type += 'Payeur';
                if (quo.Owner__c == accId) {
                    if (type != '')
                        type += ',';
                    type += 'Propriétaire';   
                } 
                if (quo.Inhabitant__c == accId) {
                    if (type != '')
                        type += ',';
                    type += 'Occupant';  
                }
                quo.Status = getStatusLabel(quo.Status);
                lstwrpQuoType.add(new wrapperQuotesWithType(type, quo));
            }
            
            Account acc = [SELECT Id, Salutation, Name FROM Account WHERE Id =: accId];

            return new wrapperAccQuotes(acc, lstwrpQuoType);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static String delQuote(Id quoId){
        try {
            Quote quo = [SELECT Id, Name FROM Quote WHERE Id =: quoId];
            delete quo;
            return quo.Name;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<String> statusPicklistValue() {
        List<String> ret = new List<String>();
        for (Schema.PicklistEntry f : Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap().get('Status').getDescribe().getPicklistValues()) {
            ret.add(f.getLabel());
        }
        return ret;
    }

    public static String getStatusLabel(String value) {
        for (Schema.PicklistEntry f : Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap().get('Status').getDescribe().getPicklistValues()) {
            if (f.getValue() == value)
                return f.getLabel();
        }
        return null;
    }

    public class wrapperAccQuotes {
        @AuraEnabled public Account acc {get;set;}
        @AuraEnabled public List<wrapperQuotesWithType> lstwrpQuoType {get;set;}
        public wrapperAccQuotes (Account acc, List<wrapperQuotesWithType> lstwrpQuoType) {
            this.acc = acc;
            this.lstwrpQuoType = lstwrpQuoType;
        }
    }

    public class wrapperQuotesWithType {
        @AuraEnabled public String type {get;set;}
        @AuraEnabled public Quote quo {get;set;}
        public wrapperQuotesWithType (String type, Quote quo) {
            this.type = type;
            this.quo = quo;
        }
    }
}