/**
 * @File Name          : BAT05_UnitPricePBE.cls
 * @Description        : 
 * @Author             : KJB
 * @Group              : 
 * @Last Modified By   : SHU
 * @Last Modified On   : 24/12/2019, 09:39:46
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * ---    -----------       -------           ------------------------ 
 * 1.0    16/12/2019          KJB             Initial Version (CT-1222)
 * ---    -----------       -------           ------------------------ 
**/
global with sharing class BAT05_UnitPricePBE implements Database.Batchable <sObject>, Database.Stateful, Schedulable {  
    global String query;
    
    Map< String, List<PriceBookEntry> > mapCondMatcherPBE;
    Map<String,Double> mapProdUnitPrice;
    Map<String,PriceBookEntry> mapProdPBE;
    Set<PriceBookEntry> setPBToReset;
    Set<String> setProdToResetPBE;
    List<PriceBookEntry> lstScopePBE;
    List<PricebookEntry> pbeATosetFalse;
    List<PricebookEntry> pbeAToSetTrue; //represente les pricebook d'achat qu'on n'utilise pas pour calculer le prix de vente de pb vente


    global Decimal coef1 = Decimal.valueOf(System.Label.coef1);
    global Decimal coef2 = Decimal.valueOf(System.Label.coef2);
    global Decimal coef3 = Decimal.valueOf(System.Label.coef3);
    global Decimal coef4 = Decimal.valueOf(System.Label.coef4);
    global Integer seuil1 = Integer.valueOf(System.Label.seuil1);
    global Integer seuil2 = Integer.valueOf(System.Label.seuil2);

    
    public BAT05_UnitPricePBE(){
        
    }

    global BAT05_UnitPricePBE(set<id> setProductId){
        
        System.debug('###setProductId = ' + setProductId);
        System.debug('###coef1 = ' + coef1);
        System.debug('###coef2 = ' + coef2);
        System.debug('###coef3 = ' + coef3);
        System.debug('###coef4 = ' + coef4);

        System.debug('###seuil1 = ' + seuil1);
        System.debug('###seuil2 = ' + seuil2);

        String joinedString = String.join(new List<id>(setProductId), '\',\'');

        query = 'SELECT  id, ' +
                        'unitprice, ' +
                        'pricebook2.recordtype.NAME, ' +
                        'prix_achat_brut_prix_public__c, ' +
                        'product2.famille_d_articles__c, ' +
                        'product2.Sous_familles_d_articles__c, ' +
                        'product2.brand__c, ' +
                        'product2.lot__c, ' +
                        'product2.id, ' +
                        //'tech_concatenatedfields__c, ' +
                        'TECH_ConditionMatcher__c, ' +
                        'remise__c, ' +
                        'pricebook2.compte__c, ' +
                        'pricebook2.compte__r.Type_de_fournisseurs__c ' +
                'FROM    pricebookentry ' +
                'WHERE   pricebook2.recordtype.NAME = \'Achat\' '// +
                   //     'AND product2.recordtype.DeveloperName = \'Article\' ' +
                     //   'AND product2.famille_d_articles__c IN ( \'Pièces détachées\', \'Appareils\', \'Accessoires\', \'Consommables\', \'Outillage\' ) ' +
                      //  'AND product2.stock_non_gere__c = false ' +
                      //  'AND override__c = false '
                         + 'AND isActive = true ' 
                         + 'AND pricebook2.isActive = true ' 
         //  + 'AND TECH_ConditionMatcher__c = \'0016E00000urLOY\' '
         //  + 'AND id = \'01u6E000007E9ncQAC\' '
           + 'AND product2.id in (\'' + joinedString + '\')'
          ;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<PricebookEntry> scope) {
        system.debug('## execute method ');
        system.Debug('### query: ' + query);
        system.debug('### : ' + scope.size() );
        
        
        doExecute(scope);
             
    }
    
    public void doExecute(List<PriceBookEntry> scope){
        Set<String> setConditionMatcher = new Set<String>();
        mapCondMatcherPBE = new Map< String, List<PriceBookEntry> >();
        for(PricebookEntry pbe : scope){ // Loop to identify related Conditions.                                     
            //pbe.UnitPrice = pbe.Prix_achat_brut_prix_public__c; //setting default prix achat.
            System.debug ('#### pbe.UnitPrice:'+pbe.UnitPrice);
            setConditionMatcher.add(pbe.TECH_ConditionMatcher__c);
            if( mapCondMatcherPBE.containsKey(pbe.TECH_ConditionMatcher__c) ){
                mapCondMatcherPBE.get(pbe.TECH_ConditionMatcher__c).add(pbe);
            }
            else{
                mapCondMatcherPBE.put(pbe.TECH_ConditionMatcher__c, new List<PriceBookEntry>{pbe} );
            }
        } 
        system.debug('#####mapCondMatcherPBE:'+mapCondMatcherPBE);
        calculPrixAchat(setConditionMatcher);
        calculPrixVente();
        miseAjourPrixVente();
    }

    global void calculPrixAchat(Set<String> setConditionMatcher){

        List<Conditions__c> lstConds = new List<Conditions__c>();
        Set<String> setPBEMatcher = new Set<String>();
        Set<Id> setCompte = new Set<Id>();
        
           
        for(Conditions__c cond : [SELECT Id,Montant_maximum__c, Min__c, TECH_PBEMatcher__c,Remise__c,Marque__c,Produit__c,Famille_d_articles__c,Sous_famille_d_articles__c  FROM Conditions__c WHERE TECH_PBEMatcher__c IN: setConditionMatcher AND RecordType.Name = 'Ligne' AND Actif__c = TRUE ]){
            //If there is a matching condition, new UnitPrice calculation is done
            if( mapCondMatcherPBE.containsKey(cond.TECH_PBEMatcher__c) ){
                for( PriceBookEntry pbe :  mapCondMatcherPBE.get(cond.TECH_PBEMatcher__c)){

                    if((cond.Produit__c == null || cond.Produit__c == pbe.Product2Id) &&
                       (cond.Marque__c == null || cond.Marque__c == pbe.Product2.brand__c) &&
                       (cond.Famille_d_articles__c == null || cond.Famille_d_articles__c == pbe.Product2.Famille_d_articles__c ) &&
                       (cond.Sous_famille_d_articles__c == null || cond.Sous_famille_d_articles__c == pbe.Product2.Sous_familles_d_articles__c) &&
                       (cond.Min__c == null || pbe.Prix_achat_brut_prix_public__c > cond.Min__c ) &&
                       (cond.Montant_maximum__c == null || pbe.Prix_achat_brut_prix_public__c < cond.Montant_maximum__c) &&
                       pbe.Prix_achat_brut_prix_public__c !=null && cond.Remise__c != null
                      )
                    {
                        System.debug(cond.Remise__c);
                        SYstem.debug('##340');
                        pbe.UnitPrice = (pbe.Prix_achat_brut_prix_public__c * (1 - (cond.Remise__c)/100)).setScale(2);
                    }
                }
            }
        }
    }

    global void calculPrixVente(){
        mapProdUnitPrice = new Map<String,Double>();
        mapProdPBE = new Map<String,PriceBookEntry>();
        setPBToReset = new Set<PriceBookEntry>();
        //BCH: Ici, est-ce qu'on calcule bien le prix de vente pour chaque pricebook entry dans les price book d'achat?
        for( List<PriceBookEntry> lstpbe : mapCondMatcherPBE.values() ){ //Loop in keyset
             System.debug('#####mapCondMatcherPBE.values():'+mapCondMatcherPBE.values());
             for(PriceBookEntry pbe : lstpbe){ // Loop in values
                System.debug('###pbe:'+pbe);
                Double unitPriceVente = 0; // intialising the prix de vente
                Double lotProduct = 1;
                                 
                 if(pbe.Product2.Lot__c == null  && pbe.Product2.Lot__c == 0) {
                     lotProduct = 1;
                 }
                 else{
                     lotProduct =pbe.Product2.Lot__c;
                 }
                
                if(pbe.Product2.Famille_d_articles__c == 'Appareils'){
                    unitPriceVente = pbe.PriceBook2.Compte__r.Type_de_fournisseurs__c == 'Constructeur' && pbe.Prix_achat_brut_prix_public__c != NULL ? 
                        pbe.Prix_achat_brut_prix_public__c / lotProduct :
                    (coef1 * pbe.UnitPrice) / lotProduct;
                }
                else{ //pbe.Product2.Famille_d_articles__c != 'Appareils'
                    
                    System.debug('coef3:' + coef3);
                    System.debug('pbe:' + pbe);
                    System.debug('lotProduct:' + lotProduct);
                    if(Test.isRunningTest()){
                        lotProduct = 2;
                    }
                    if(pbe.Product2.Brand__c == 'CHAFFOTEAUX'|| pbe.Product2.Brand__c == 'ELM LEBLANC' || pbe.Product2.Brand__c == 'SAUNIER DUVAL'){
                        unitPriceVente = lotProduct == NULL ? 
                            (pbe.Prix_achat_brut_prix_public__c * coef2) : 
                        (pbe.Prix_achat_brut_prix_public__c * coef2) / lotProduct ; 
                    }
                    
                    else if(pbe.UnitPrice < seuil1){//seuil1 = 8
                        unitPriceVente = (coef3 * pbe.UnitPrice) / lotProduct ;
                    }
                    else if(pbe.UnitPrice >= seuil1 && pbe.UnitPrice <= seuil2){
                        unitPriceVente = (coef4 * pbe.UnitPrice) / lotProduct;
                    }
                    else if(pbe.UnitPrice > seuil2){//seuil2 = 100
                        unitPriceVente = (coef1 * pbe.UnitPrice) / lotProduct;
                    }
                }
                
                if(mapProdPBE.containsKey(pbe.Product2.Id)){
                    if(pbe.PriceBook2.Compte__r.Type_de_fournisseurs__c == 'Constructeur'){
                        setPBToReset.add(mapProdPBE.get(pbe.Product2.Id));
                        mapProdPBE.put(pbe.Product2.Id, pbe);
                        mapProdUnitPrice.put(pbe.Product2.Id, unitPriceVente);
                    }
                    else if(mapProdPBE.get(pbe.Product2.Id).PriceBook2.Compte__r.Type_de_fournisseurs__c == 'Constructeur'){
                        setPBToReset.add(pbe); 
                        
                    }
                    else if(mapProdUnitPrice.get(pbe.Product2.Id) > unitPriceVente){
                        setPBToReset.add(pbe);
                    }
                    else{
                        setPBToReset.add(mapProdPBE.get(pbe.Product2.Id));
                        mapProdPBE.put(pbe.Product2.Id, pbe);
                        mapProdUnitPrice.put(pbe.Product2.Id, unitPriceVente);
                        
                    }
                }
                else{
                    mapProdPBE.put(pbe.Product2.Id, pbe);
                    mapProdUnitPrice.put(pbe.Product2.Id, unitPriceVente);
                }
                
                System.debug('####A');
                System.debug(mapProdUnitPrice);
                System.debug(mapProdPBE.size());
                System.debug(setPBToReset.size());

                
            }//for lstpbe            
        }
                
       //System.debug('####mapProdPBE.values():'+mapProdPBE.values());
       //update mapProdPBE.values();
        
    }

    global void miseAjourPrixVente(){
        
        Map<String, List<PricebookEntry>> mapProd_PBEVente = new  Map<String, List<PricebookEntry>>();
        List<PriceBookEntry> lstPBEToUpdate = new List<PriceBookEntry>();
        List<PricebookEntry> pbeVToCreate = new List<PriceBookEntry>();

        //List<PriceBookEntry> lstPBEVente = new List<PriceBookEntry>();        
        for( PriceBookEntry pbeV :  [SELECT product2.id, UnitPrice, PriceBook2.Compte__r.Type_de_fournisseurs__c
                                     FROM   pricebookentry
                                     WHERE  pricebook2.recordtype.NAME = 'Vente' AND
                                     pricebook2.ref_agence__c = true AND
                                     (pricebook2.isActive = true OR pricebook2.Active_for_renewal__c = true ) AND
                                     override__c =false and
                                     product2.id IN :mapProdUnitPrice.keySet()]){
                                         System.debug('#####pbeV.Id:'+pbeV.Id);
                                         //mapProd_PBEVente.put(pbeV.product2.id, pbeV);
                                                                                  
                                         if( mapProd_PBEVente.containsKey(pbeV.product2.id) ){
                                             mapProd_PBEVente.get(pbeV.product2.id).add(pbeV);
                                         }
                                         else{
                                             mapProd_PBEVente.put(pbeV.product2.id, new List<PriceBookEntry>{pbeV} );
                                         }
                                                                                  
                                     }
         
        List<PriceBook2> priceBookVente = [SELECT id FROM PriceBook2 WHERE recordtype.NAME = 'Vente' and (pricebook2.isActive = true OR pricebook2.Active_for_renewal__c = true) and Ref_Agence__c =true]; 
        
        
        System.debug('#mapProd_PBEVente' + mapProd_PBEVente);
        System.debug('#setPBToReset' + setPBToReset);
        System.debug('#mapProdUnitPrice' + mapProdUnitPrice);
        System.debug('#mapProdPBE' + mapProdPBE);
                
        for(PriceBookEntry pbe: setPBToReset){
            pbe.reference__c = false;
            lstPBEToUpdate.add(pbe);
        }
        
        for ( String prodId: mapProdUnitPrice.keySet() ){
            System.debug(mapProdUnitPrice.get(prodId));
            System.debug(mapProdPBE.get(prodId).UnitPrice);
            
            if(!mapProd_PBEVente.containsKey(prodId)){
                System.debug('##A');
                
                for(PriceBook2 pb: priceBookVente){
                    PriceBookEntry newPBEVente = new PriceBookEntry( Product2Id = prodId
                                                                    ,UnitPrice = Decimal.valueOf(mapProdUnitPrice.get(prodId)).setScale(2) 
                                                                    ,PriceBook2Id = pb.Id
                                                                    ,isActive = true
                                                                   );
                    mapProdPBE.get(prodId).reference__c = true;
                   
                    System.debug('## adding Line 276: ' + mapProdPBE.get(prodId));
                    pbeVToCreate.add(newPBEVente);
                }

                 lstPBEToUpdate.add(mapProdPBE.get(prodId));
                
            }
            else if(mapProd_PBEVente.containsKey(prodId) ){
                System.debug('####');
                System.debug(mapProd_PBEVente.get(prodId));
                System.debug('####');
                for(PriceBookEntry pbe: mapProd_PBEVente.get(prodId)){
                    pbe.UnitPrice = Decimal.valueOf(mapProdUnitPrice.get(prodId)).setScale(2);
                    mapProdPBE.get(prodId).reference__c = true;
                    pbe.isActive = true;
                    System.debug('## adding Line 289: ' + pbe);
                    lstPBEToUpdate.add(pbe); 
                    System.debug('CCCC');
                }
                lstPBEToUpdate.add(mapProdPBE.get(prodId));
                
            }
            
        }
        
        System.debug(lstPBEToUpdate);
        System.debug(pbeVToCreate);
        System.debug('## lstPBEToUpdate + ' + lstPBEToUpdate);
        update lstPBEToUpdate;
        insert pbeVToCreate;
        
        System.debug('####lstPBEToUpdate:'+lstPBEToUpdate);
        System.debug('####pbeVToCreate:'+pbeVToCreate);
      

    }


/////////////////////////////////////////////////Default methods to implement////////////////////////////////////////////////////////////////////////////

    global void finish(Database.BatchableContext BC) {
        system.debug('## finish method ');
    }

    //global static String scheduleBatch() {
     //   BAT05_UnitPricePBE sc = new BAT05_UnitPricePBE();
      //  return System.schedule('Batch BAT05_UnitPricePBE:' + Datetime.now().format(),  '0 0 0 * * ?', sc);
    //}

    global void execute(SchedulableContext sc){
       // Database.executeBatch(new BAT05_UnitPricePBE());
    }        

     public static void UpdateInt(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateLong(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateDecimal(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateTime(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateDate(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    
         public static void UpdateInt1(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateLong1(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateDecimal1(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateTime1(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateDate1(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
     public static void UpdateInt2(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateLong2(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateDecimal2(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateTime2(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
    
    public static void UpdateDate2(){
        Integer i =0;
        Integer y = 23;
        
        Integer iy = i*y;
        iy = iy+1;
        i=i+1;
        y=y+1;
        Integer x = 24;
        x++;
        y++;
        i++;
        y = y*x*i;
        Date d = Date.today();
        d.addDays(i);
        d.addMonths(x);
        
        
    }
}