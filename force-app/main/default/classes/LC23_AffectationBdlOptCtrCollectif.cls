/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-12-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-17-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class LC23_AffectationBdlOptCtrCollectif {
    @AuraEnabled
    public static List<Product_Bundle__c> getBundleChild(string bundleProdId){
        try {
            List<Product_Bundle__c> lstProdBundleParent = [SELECT Id, Bundle__c, Price__c, Optional_Item__c, Product__c FROM Product_Bundle__c WHERE Product__c  =:bundleProdId];
            Set<Id> setBundleId = new Set<Id>();
            for(Product_Bundle__c prodBun : lstProdBundleParent){
                setBundleId.add(prodBun.Bundle__c);
            }
            List<Product_Bundle__c> lstProdBundleChild = [SELECT Id, Bundle__c, Price__c, Optional_Item__c, Product__c FROM Product_Bundle__c WHERE Bundle__c IN :setBundleId];
            system.debug('PRA lstProdBundleChild '+lstProdBundleChild);
            return lstProdBundleChild;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}