/**
 * @description       : 
 * @author            : ZJO
 * @group             : 
 * @last modified on  : 09-09-2020
 * @last modified by  : ZJO
 * Modifications Log 
 * Ver   Date         Author   Modification
 * 1.0   07-15-2020   ZJO   Initial Version
**/
public without sharing class Utils{
    
   public static List<ContentDocumentLink> contentDocumentLinkListTest = new List<ContentDocumentLink>();
   public static Boolean isTest;

  // Retourne l'identifiant des comptes des users connectés
  // public static map<String, String> getAccountIdFromCommunityUser(List<String> usersIds){
  //   System.debug('--- getAccountIdFromCommunityUser Start !');
  //   map<String, String> result = new map<String, String>();
  //   List<String> contactsId = new List<String>();        
    
  //   List<User> users = [
  //       SELECT Id, Name, ContactId
  //       FROM User
  //       WHERE Id IN :usersIds
  //       AND ContactId != NULL
  //   ];
  //   System.debug('--- users : ' + users);
    
  //   for(User anUser : users){
  //       contactsId.add(anUser.ContactId);
  //   }
    
  //   if(contactsId.size() > 0){
        
  //     for(Contact aContact : [
  //         SELECT Id, Name, AccountId
  //         FROM Contact
  //         WHERE Id IN :contactsId
  //         // AND IsPersonAccount = true
  //     ]){
  //         System.debug('--- aContact : ' + aContact);
          
  //       for(User anUser : users){
  //         if(anUser.ContactId.equals(aContact.Id)){
  //           result.put(anUser.Id, aContact.AccountId);
              
  //           break;
  //         }
  //       }
  //     }
  //   }
    
  //   System.debug('--- getAccountIdFromCommunityUser End !');
    
  //   return result;
  // }
    
  // Retourne une map avec pour chaque enregistrement en clé, le lien du dernier document public en valeur
  // public static map<String, String> getPublicLinkMap(List<String> recordsId){
  //   System.debug('--- getPublicLinkMap Start !');
      
  //   map<String, String> result = new map<String, String>();    
  //   System.debug('--- recordsId size: '+recordsId.size());
  //   System.debug('--- recordsId: '+recordsId );

  //   // Récupération des ContentDocumentLinks des enregistrements demandés
  //   List<ContentDocumentLink> contentDocumentLinkList = (test.isRunningTest() && isTest) ? contentDocumentLinkListTest : [
  //     SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.CreatedDate
  //     FROM ContentDocumentLink
  //     WHERE LinkedEntityId IN :recordsId
  //     AND Visibility = 'AllUsers'
  //     AND IsDeleted = False
  //     ORDER BY ContentDocument.CreatedDate DESC
  //   ];

  //   System.debug('--- contentDocumentLinkList size: '+contentDocumentLinkList.size());

  //   if(contentDocumentLinkList.size() > 0 || (contentDocumentLinkListTest.size()>0 && isTest)){
  //     List<String> contentDocumentId = new List<String>();
  //     map<String, String> contentDistributionMap = new map<String, String>();

  //     for(ContentDocumentLink aContentDocumentLink : (test.IsRunningTest() && isTest) ? contentDocumentLinkListTest : contentDocumentLinkList){
  //       contentDocumentId.add(aContentDocumentLink.ContentDocumentId);    
  //     } 
      
  //     for(ContentDistribution aContentDistribution : [
  //       SELECT Id, ContentDocumentId, DistributionPublicUrl, CreatedDate
  //       FROM ContentDistribution
  //       WHERE ContentDocumentId IN :contentDocumentId
  //     ]){
  //       if(!String.isBlank(aContentDistribution.DistributionPublicUrl)){
  //         contentDistributionMap.put(aContentDistribution.ContentDocumentId, aContentDistribution.DistributionPublicUrl);
  //       }
  //     }
    
  //     for(ContentDocumentLink aContentDocumentLink : contentDocumentLinkList){
  //       if(contentDistributionMap.containsKey(aContentDocumentLink.ContentDocumentId) && !result.containsKey(aContentDocumentLink.LinkedEntityId)){
  //         result.put(aContentDocumentLink.LinkedEntityId, contentDistributionMap.get(aContentDocumentLink.ContentDocumentId));
  //       }
  //     }
  //   }
    
  //   System.debug('--- result : ' + result);
    
  //   System.debug('--- getPublicLinkMap End !');
    
  //   return result;
  // }

  // Retourne la liste des équipements de l'utilisateur connecté
  // public static List<Asset> getUserAsset(String theUserId){
  //   System.debug('--- getUserAsset Start !');
  //   System.debug('User Id:' + theUserId);

  //   List<Asset> result;

  //   if(!String.isBlank(theUserId)){    
  //     map<String, String> userMap = Utils.getAccountIdFromCommunityUser(new List<String>{theUserId});
  //     System.debug('user map:' + userMap);

  //     if(userMap.containsKey(theUserId)){
  //       result = [
  //         SELECT Id
  //         FROM Asset
  //         WHERE AccountId = :userMap.get(theUserId)
  //       ];
  //     }
  //   }

  //   System.debug('--- getUserAsset End !');
  //   System.debug('*** result:' + result);
  //   return result;
  // }
  
  // Retourne l'identifiant du record type d'un compte spécifié
  public static String getAccountRecordTypeId(String recordTypeLabel){
      return Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();
  }
  
  // Nettoie une valeur d'un formulaire
  public static String cleanValue(String theValue){
      String result;
      
      if(!String.isBlank(theValue)){
          result = String.escapeSingleQuotes(theValue.trim());
      }
      
      return result;
  }
  
  // Convertie une date UTC en GMT
  public static DateTime fromUTCToGMT(DateTime theDateTime){
    return Datetime.newInstanceGmt(theDateTime.year(), theDateTime.month(), theDateTime.day(), theDateTime.hour(), theDateTime.minute(), theDateTime.second());
  }
  
  // Envoie un email
  // public static void sendEmail(String targetRecordId, String templateId, String displayerName){
  //     System.debug('--- targetRecordId : ' + targetRecordId);
  //     System.debug('--- templateId : ' + templateId);
      
  //     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
  //     mail.setTargetObjectId(targetRecordId); 
  //     mail.setSenderDisplayName(displayerName); 
  //     mail.setUseSignature(false); 
  //     mail.setBccSender(false); 
  //     mail.setSaveAsActivity(false); 
  //     mail.setTemplateId(templateId); 
  //     Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       
  // }
}