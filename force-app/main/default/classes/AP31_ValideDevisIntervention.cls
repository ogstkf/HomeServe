/**
 * @File Name          : AP31_ValideDevisIntervention.cls
 * @Description        : 
 * @Author             : Spoon Consulting(KZE)
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 28-10-2021
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      Modification
 *==============================================================================
 * 1.0         24-10-2019               KZE/RRJ        Initial Version
 * 1.1         16-11-2020               BCH		       New work order creation types/reason depending on the associated Quote type
**/
public with sharing class AP31_ValideDevisIntervention {

    public static void createWorkORder(set<Quote> setQuo){
        System.debug('##starting method AP31_ValideDevisIntervention.createWorkORder: '+ setQuo);

        //CT-1852
        List<Quote> lstQuotes = [SELECT Id, Type_de_devis__c, Ordre_d_execution__c FROM Quote WHERE Id IN :setQuo];
        
        Map<Id, Id> mapQuoWoId = new Map<Id, Id>();
        Map<Id, Quote> mapWoQuote = new Map<Id, Quote>(); //AMO Map Work Order and Quote

        for(Quote quo: setQuo){
            if(quo.Ordre_d_execution__c != null){
                mapQuoWoId.put(quo.Id, quo.Ordre_d_execution__c);
                mapWoQuote.put(quo.Ordre_d_execution__c, quo);
            }
        }

        List<WorkOrder> lstWOToInsert = new  List<WorkOrder>();
        Map<Id, WorkOrder> mapOldNewWO = new Map<Id, WorkOrder>();

        for(WorkOrder wo: [ SELECT Id, 
                                    WorkTypeId, 
                                    Address, 
                                    City, 
                                    Country, 
                                    State, 
                                    Street, 
                                    PostalCode, 
                                    Status, 
                                    Quote__c, 
                                    ServiceTerritoryId, 
                                    AccountId, ContactId, ServiceContractId, LocationId, AssetId, CaseId, Priority, RootWorkOrderId, Subject, Type__c, Account.Campagne__c  
                            FROM WorkOrder 
                            WHERE Id IN :mapQuoWoId.values()]){
            WorkOrder newWO = new WorkOrder(
                WorkTypeId = wo.WorkTypeId,
                City = wo.City,
                Country = wo.Country,
                State = wo.State,
                Street = wo.Street,
                PostalCode = wo.PostalCode,
                ServiceTerritoryId = wo.ServiceTerritoryId,
                AccountId = wo.AccountId,
                ContactId = wo.ContactId,
                ServiceContractId = wo.ServiceContractId,
                // LocationId = wo.LocationId,
                AssetId = wo.AssetId,
                CaseId = wo.CaseId,
                Priority = 'Faible',
                Subject = wo.Subject,
                Type__c = wo.Type__c,
                Campagne__c = wo.Account.Campagne__c, //DMU 20200720 - TEC-64 L'assignation du code campagne
                Clientnoncontacte__c = true
            );
            
            if(mapWoQuote.get(wo.Id).Type_de_devis__c == 'Vente déquipement / Pose'){
                newWO.Type__c = 'Installation';
            }
            if(mapWoQuote.get(wo.Id).Type_de_devis__c == 'Mise en service'){
                newWO.Type__c = 'Commissioning';
            }
            if(mapWoQuote.get(wo.Id).Type_de_devis__c == 'Prestations' || mapWoQuote.get(wo.Id).Type_de_devis__c == 'Sous-traitance totale'
            || mapWoQuote.get(wo.Id).Type_de_devis__c == 'Vente de pièces/équipements au guichet'){
                newWO.Type__c = 'Troubleshooting';
            }
            if(mapWoQuote.get(wo.Id).Type_de_devis__c == 'Visite forfaitaire'){
                newWO.Type__c = 'Maintenance';
                newWO.Reason__c = 'Visite hors contrat';
            }
            newWO.Subject = LabelOfPicklistType(newWO.Type__c); 

            lstWOToInsert.add(newWO);
            mapOldNewWO.put(wo.Id, newWO);
        }

        if(lstWOToInsert.size()>0){
            insert lstWOToInsert;
        }
        System.debug('###### lstWOToInsert: '+lstWOToInsert);


        List<Quote> lstQuoToUpd = new List<Quote>();
        for(Id quoId: mapQuoWoId.keySet()){
            Id newWoId = mapOldNewWO.get(mapQuoWoId.get(quoId)).Id;
            lstQuoToUpd.add(new Quote(
                Id = quoId,
                Ordre_d_execution__c = newWoId
            ));
        }
        
        //Update Oppty as well
        List<Opportunity> lstOppToUpd = [SELECT Id, Ordre_d_execution__c FROM Opportunity WHERE Ordre_d_execution__c IN :mapOldNewWO.keySet() AND Ordre_d_execution__c!= null];
        for(Opportunity opp: lstOppToUpd){
            Id woId = mapOldNewWO.get(opp.Ordre_d_execution__c).Id;
            opp.Ordre_d_execution__c = woId;            
        }
        
        List<ProductRequest> lstPR = [SELECT Id, WorkOrderId FROM ProductRequest WHERE WorkOrderId IN :mapOldNewWO.keySet() AND WorkOrderId != null];
        for(ProductRequest pr : lstPR){
            Id woId = mapOldNewWO.get(pr.WorkOrderId).Id;
            pr.WorkOrderId = woId;
        }

        List<ProductRequestLineItem> lstPRLI = [SELECT Id, WorkOrderId FROM ProductRequestLineItem WHERE WorkOrderId IN :mapOldNewWO.keySet() AND WorkOrderId != null];
        for(ProductRequestLineItem prli : lstPRLI){
            Id woId = mapOldNewWO.get(prli.WorkOrderId).Id;
            prli.WorkOrderId = woId;
        }
        
        if(lstQuoToUpd.size()>0){
            update lstQuoToUpd;
            QuoteLineItemTriggerHandler.notRun = true;
            QuoteTriggerHandler.notRun = true;
        }
        
        //Update Oppty as well
        if (lstOppToUpd.size()>0){
            update lstOppToUpd;
            QuoteLineItemTriggerHandler.notRun = true;
            QuoteTriggerHandler.notRun = true;
        } 

        if(lstPRLI.size()>0){
            update lstPRLI;
        }
        
        if(lstPR.size()>0){
            update lstPR;
        }
        
        //DMU 20200619 - commented AP30_QuoteProductManagement due to homeserve project
         AP30_QuoteProductManagement.createRequiredProducts(lstQuoToUpd);

        System.debug('###### lstQuoToUpd: '+lstQuoToUpd);
    }
    
    public static String LabelOfPicklistType (string Test) {
      string type;
      Schema.DescribeFieldResult field = workorder.Type__c.getDescribe();
      for (Schema.PicklistEntry f : field.getPicklistValues()){
        if(f.getValue() == test){
           type = f.getLabel();
        }
      }
      if(type == null){
        type ='';
      }       
    return Type;          
    }
}