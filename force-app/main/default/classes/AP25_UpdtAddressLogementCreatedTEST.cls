/**
 * @File Name          : AP25_UpdtAddressLogementCreated.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 25/10/2019, 12:27:42
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/10/2019   AMO     Initial Version
**/
@isTest
public with sharing class AP25_UpdtAddressLogementCreatedTEST {
    
    static User mainUser;
    static list<Account> lstAcc;
    static List<Logement__c> lstTestLogement;
    static list<ServiceTerritory> lstServTerritory;
    static OperatingHours opHrs = new OperatingHours(); 
	static sofactoapp__Raison_Sociale__c raisonSocial;
    static{
        mainUser = TestFactory.createAdminUser('AP25_SetCaseStatus@test.COM', 
                                                TestFactory.getProfileAdminId());
        insert mainUser;

        System.runAs(mainUser){
                lstAcc = new list<Account>{
                new Account(RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
                ,Name='John ANA', BillingStreet = 'AA', BillingCity = 'AB', BillingPostalCode = 'AC', BillingCountry = 'France')
            };
			
            Insert lstAcc;
			sofactoapp__Compte_auxiliaire__c aux0 = DataTST.createCompteAuxilaire(lstAcc.get(0).Id);
            lstAcc.get(0).sofactoapp__Compte_auxiliaire__c = aux0.Id;
            update lstAcc;

            opHrs = TestFactory.createOperatingHour('testOpHrs');
            insert opHrs;
			raisonSocial = DataTST.createRaisonSocial();
            insert raisonSocial;
            lstServTerritory = new list<ServiceTerritory>{
                TestFactory.createServiceTerritory('SGS - test Territory', opHrs.Id, raisonSocial.Id)
            };
            insert lstServTerritory;

            lstTestLogement = new list<Logement__c>{
                new Logement__c(Account__c =lstAcc[0].Id, Inhabitant__c = lstAcc[0].Id)
            };

            lstTestLogement[0].Street__c='AA';
            lstTestLogement[0].City__c='AMO1';
            lstTestLogement[0].Postal_Code__c='333';
            lstTestLogement[0].Country__c='Belgique';
            lstTestLogement[0].Agency__c=lstServTerritory[0].Id;      

        }
    }

    @isTest
    public static void testUpdate(){
        system.runAs(mainUser){
            Test.startTest();
                Insert lstTestLogement;
            Test.stopTest();

            Account acc = [SELECT Id, BillingStreet, BillingCity, BillingPostalCode, BillingCountry FROM Account WHERE Id = :lstAcc[0].Id LIMIT 1];
            System.assertEquals(acc.BillingStreet, 'AA');
            System.assertEquals(acc.BillingCity, 'AMO1');
            System.assertEquals(acc.BillingPostalCode, '333');
            System.assertEquals(acc.BillingCountry, 'Belgique');
        }
    }

}