public class SSA_Remise_Summary {
   
    
    @InvocableMethod
    public static void RollupRemise (List<ID> recordId){
       
        System.debug('### entree invocabbele; nombre de remise en entrée '+ recordId.size());
        integer NbrPayment = 0;
    	decimal MontantTotal =0 ; 
        sofactoapp_remise__c Remise = new sofactoapp_remise__c();
        list<sofactoapp__R_glement__c> Reg2 = new list<sofactoapp__R_glement__c>();
        id remisenull = null;
        
        Remise.id = [select id 
                    from sofactoapp_remise__c 
                    where id in: recordId
                   limit 1].id;
        string remiseId = Remise.id;
        System.debug('### id de la remise  '+ remiseId);
 		
        list<sofactoapp__R_glement__c> Reglements = new list<sofactoapp__R_glement__c>();
       	Reglements = [select id,sofactoapp__Montant__c, sofactoapp_Remise__c from sofactoapp__R_glement__c 
				where sofactoapp_Remise__c in: [select id 
                    from sofactoapp_remise__c 
                    where id in: recordId
                   limit 1]];
        
        
      
       if(Reglements.size()<50) {
        System.debug('### npbre élément dans remise  '+ Reglements.size());
        for ( sofactoapp__R_glement__c reg :Reglements ){
             NbrPayment = NbrPayment +1;
             MontantTotal = MontantTotal + reg.sofactoapp__Montant__c ;
            
        }
       Remise.tech_montant_total__c = MontantTotal;
       Remise.tech_nombre_elemennt_remise__c= String.valueOf(NbrPayment);
            try{
            System.debug('### MAJ remise');
            update Remise;
            }catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
            }
       }
        
        
        else{
            System.debug('### npbre élément dans remise POUR SUPPRESSION '+ Reglements.size());
            
            for (sofactoapp__R_glement__c reg :Reglements ){
                reg.sofactoapp_Remise__c= remisenull;
               	reg2.add(reg);
                 }
        try{
            System.debug('### MAJ reglement');
            update reg2;
            }catch(Exception ex){
            System.debug('*** error message: '+ ex.getMessage());
            }
        
        }
        
     
    }

}