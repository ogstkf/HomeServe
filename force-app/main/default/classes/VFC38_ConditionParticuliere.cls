/**
 * @File Name          : VFC38_ConditionParticuliere.cls
 * @Description        : 
 * @Author             : AMO
 * @Group              : 
 * @Last Modified By   : AMO
 * @Last Modified On   : 10/12/2019, 12:24:57
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    19/11/2019   AMO     Initial Version 
 * 1.1    08/01/2019   DMG     Modif Champs Mapping
 * 1.1    13/01/2019   DMG     Modif Champs Mapping - Bulletin
 * 1.2    16/01/2019   DMG     Mapping - Bulletin - sofactoapp__R_glement__c
 * 1.3    23/01/2019   DMG     Address facturation not used anymore - use payeur contrat
 * 1.4    28/01/2019   DMG     Options contrat - Bundle Products
 * 1.5    30/03/2020   DMG     Payeur_du_contrat__r.Adress_complement__c
**/
public without sharing class VFC38_ConditionParticuliere {

    public ServiceContract currentServiceContract {get;set;}
    public List<ServiceContract> lstServiceContract {get;set;}
    public List<ServiceAppointment> lstServiceAppointment {get;set;}
    public List<ContractLineItem> lstContractLineItem {get;set;}
    public List<ContractLineItem> lstContractLineItemNotBundle {get;set;}
    public List<ContractLineItem> lstContractLineItemOptions {get;set;}
    public ContractLineItem cliMain {get;set;}
    // public ServiceAppointment sa {get;set;}
    // public String currentDate  {get;set;}
    // public String assetAccName {get;set;}
    // public String SRName {get;set;}
    // public Boolean BoolCheckClientPresent{get;set;}
    // public Boolean BoolCheckClientAbsent{get;set;}
    public decimal tax {get;set;}
    public string nomRemise {get;set;}
    public string valable {get;set;}
    public string agencyName {get;set;} 
    public wrapperSC w {get;set;}    
    public List<wrapperclI> cli {get;set;}  
    public Boolean showAddFacturation {get;set;}  
    public Boolean showAddAppareil {get;set;}  
    public Datetime dateVisite {get;set;}
    public Boolean isPersonAcc {get;set;}
    public Contact ConPro {get;set;}
    public decimal MaxPayment {get;set;}
    public decimal MinPayment {get;set;}
    public Boolean hasOptions {get;set;}
    public string idSignature {get;set;}

    public class wrapperSC{
        public ServiceTerritory agency {get;set;}  
        public Account account {get;set;}  
        public Account payeur {get;set;}
        public Contact contact {get;set;}  
        public Asset asset {get;set;}  
        public Logement__c logement {get;set;}  
       // public Promotional_Code__c promocode {get;set;}  

        public decimal discount {get;set;}
        public decimal grandTotal {get;set;}
        public String tax {get;set;}
        public String contractNumber {get;set;}
        public decimal dureeRemise {get;set;}
        public String BillingCity {get;set;}
        public String BillingCountry {get;set;}
        public String BillingPostalCode {get;set;}
        public String BillingStreet {get;set;}
        public Datetime dateEmission {get;set;}
        public Date StartDate {get;set;}
        public Date EndDate {get;set;}
        public String RUM {get;set;}
        public string EcheancierPaiement {get;set;}
        public string modeDePaiement {get;set;}

        public wrapperSC(ServiceContract sc){
            //main objects
            agency = sc.Agency__r;
            account = sc.Account;
            payeur = sc.Payeur_du_contrat__r;
            contact = sc.Contact;
            asset = sc.Asset__r;
            logement = sc.Logement__r;
          //  promocode = sc.Promotional_Code__r;
            //individual
            discount = sc.Discount;
            grandTotal = sc.GrandTotal;
            tax = sc.Tax__c;
            contractNumber = (sc.Numero_du_contrat__c != null) ? sc.Numero_du_contrat__c : sc.ContractNumber;
            dureeRemise = sc.Duree_d_application_de_la_remise__c;
            BillingCity = sc.BillingCity;
            BillingCountry = sc.BillingCountry;
            BillingPostalCode =sc.BillingPostalCode;
            BillingStreet = sc.BillingStreet;
            dateEmission = sc.CreatedDate;
            StartDate = sc.StartDate;
            EndDate = sc.EndDate;
            RUM = sc.sofactoapp_RUM__c;
            EcheancierPaiement = sc.Echeancier_Paiement__c ;
            modeDePaiement = sc.Sofactoapp_Mode_de_paiement__c;

        }
    }
    public class wrapperclI{
        public decimal TotalPrice {get;set;}
        public decimal ListPrice {get;set;}
        public String LineItemNumber {get;set;}
        public String technomproduit {get;set;}
        public decimal unitprice {get;set;}
        public decimal discount {get;set;}
       
        public wrapperclI(ContractLineItem ci){
            //individual
            TotalPrice = ci.TotalPrice;
            ListPrice = ci.ListPrice;
            LineItemNumber = ci.LineItemNumber;
            technomproduit = ci.tech_nom_produit__c;
            unitprice = ci.UnitPrice;
            discount = ci.Discount;
        }
    }
    public VFC38_ConditionParticuliere() {
        System.debug('**** In constructor VFP38_ConditionParticuliere ****');

        //try{
            string serviceContractId = ApexPages.currentPage().getParameters().get('id');

            // currentDate = string.valueOf(Datetime.now().format('dd/MM/yyyy'));

            if(String.isNotBlank(serviceContractId)){
                lstServiceContract = new List<ServiceContract>([SELECT Id
                                                , CreatedDate
                                                , StartDate
                                                , EndDate
                                                , sofactoapp_RUM__c
                                                , Agency__r.Agency_Code__c 
                                                , Agency__r.SEPA_IDnumber__c 
                                                , Agency__r.SEPA_ad1__c
                                                , Agency__r.SEPA_ad2__c
                                                , Agency__r.SEPA_ad3__c
                                                , Echeancier_Paiement__c
                                                , Sofactoapp_Mode_de_paiement__c
                                                , Agency__c
                                                , Numero_du_contrat__c
                                                , Agency__r.Name
                                                // , Agency__r.Address
                                                ,Agency__r.Street
                                                , Agency__r.Street2__c
                                                ,Agency__r.PostalCode
                                                ,Agency__r.City
                                                , Agency__r.Phone__c
                                                , Agency__r.Email__c
                                                , Agency__r.Siret__c
                                                , Agency__r.Intra_Community_VAT__c
                                                , Agency__r.CreatedDate
                                                , Contact.Name
                                                , Account.BillingStreet
                                                , Account.BillingPostalCode
                                                , Account.BillingCity
                                                , Account.BillingCountry
                                                , Account.PersonMobilePhone
                                                , Account.PersonEmail
                                                , Account.ClientNumber__c
                                                , ContractNumber
                                                , Asset__r.IDEquipment__c
                                                , Asset__r.IDEquipmenta__c  
                                                , Asset__r.Equipment_type_auto__c
                                                , Account.Name
                                                , Account.isPersonAccount
                                                , Discount
                                                , AccountId
                                                , Payeur_du_contrat__c
                                                , Payeur_du_contrat__r.Name
                                                , Payeur_du_contrat__r.isPersonAccount
                                                , Payeur_du_contrat__r.BillingStreet
                                                , Payeur_du_contrat__r.BillingPostalCode
                                                , Payeur_du_contrat__r.BillingCity
                                                , Payeur_du_contrat__r.BillingCountry
                                                , Payeur_du_contrat__r.PersonMobilePhone
                                                , Payeur_du_contrat__r.PersonEmail
                                                , Payeur_du_contrat__r.ClientNumber__c
                                                , Payeur_du_contrat__r.Adress_complement__c
                                                , BillingCity
                                                , BillingCountry
                                                , BillingPostalCode
                                                , BillingStreet
                                                , Logement__r.Street__c
                                                , Logement__r.Postal_Code__c
                                                , Logement__r.City__c
                                                , Logement__r.Inhabitant__r.Name
                                                , Asset__r.Equipment_type__c
                                                , Asset__r.Brand__c
                                                , Asset__r.Model__c
                                                , Asset__r.Power_in_kw__c
                                                , Asset__r.Puissance_thermique_chaud_PAC_CET__c
                                                , Asset__r.Puissance_thermique_froid_climatisation__c
                                                , Asset__r.Date_de_premi_re_allumage__c
                                                , Asset__r.Ducted_Mode__c
                                                , Asset__r.Type_of_gaz__c
                                                , Asset__r.Type_de_fluide_frigorigene__c
                                                , Asset__r.Kg_de_fluide__c
                                                , Asset__r.SerialNumber
                                                , Asset__r.BurnerBrand__c
                                                , Asset__r.BurnerModel__c
                                                , Asset__r.Burner_power_in_kw__c
                                                , Asset__r.Burner_Installation_date__c
                                                , Asset__r.Burner_Ducted_Mode__c
                                                , Asset__r.Burner_Gaz_type__c
                                                , Asset__r.Burner_Serial_number__c
                                                , Asset__r.Model_auto__c
                                                , Asset__r.Brand_auto__c
                                                , Asset__r.Power_kW_auto__c
                                                , Agency__r.Corporate_Name__c
                                                , Agency__r.Legal_Form__c
                                                , Agency__r.Capital_in_Eur__c
                                                , Agency__r.Tech_Capital_In_Eur__c
                                                , Agency__r.Corporate_Street__c
                                                , Agency__r.Corporate_Street2__c
                                                , Agency__r.Corporate_ZipCode__c
                                                , Agency__r.Corporate_City__c
                                                , Agency__r.Siren__c
                                                , Agency__r.RCS__c
                                                , Agency__r.Professional_Indemnity_Insurance__c
                                                , Agency__r.Decennal_Inurance__c
                                                , Agency__r.Insurance_Cover__c
                                                , Duree_d_application_de_la_remise__c
                                                , Tax__c
                                                , GrandTotal
                                                /*, Promotional_Code__r.Name
                                                , Promotional_Code__r.Offer__r.Name
                                                , Promotional_Code__r.Offer__r.The_offer_is_active__c
                                                , Promotional_Code__r.Offer__r.End_date_of_the_offer__c*/
                                           
                                                FROM ServiceContract 
                                                WHERE Id = :serviceContractId]);
                System.debug('**** List Service Contract ' + lstServiceContract);
                
                showAddFacturation = false;
                showAddAppareil = false;
                hasOptions = false;
                if(lstServiceContract.size() > 0){

                    lstContractLineItem = new List<ContractLineItem>([SELECT Id, LineItemNumber,Product2Id,tech_nom_produit__c, ServiceContractId, TotalPrice,ListPrice, IsBundle__c, UnitPrice, Discount FROM ContractLineItem WHERE (ServiceContractId IN :lstServiceContract AND IsBundle__c = True )]);
                    // AND IsBundle__c = True

                    Set<Id> setProductId = new Set<Id>();
                    
                    if(lstContractLineItem.size()>0){
                        cliMain=lstContractLineItem[0];
                        System.debug('**** cliMain ' + cliMain);
                        //DMG 28012020 - Options
                        List<Product_Bundle__c> lstpb = [Select Id,Product__c,Bundle__c from Product_Bundle__c where Product__c=:cliMain.Product2Id];
                        //has Bundle Product
                        if(lstpb.size()>0){
                            //look for Product_Bundle__c related
                            List<Product_Bundle__c> lstbps = [Select Id,Optional_Item__c,Product__c from Product_Bundle__c where Bundle__c=:lstpb[0].Bundle__c and Optional_Item__c =true];
                            System.debug('**** List Product_Bundle__c  ' + lstbps.size());
                            //build set of Products as options
                            for(Product_Bundle__c bp :lstbps){
                                setProductId.add(bp.Product__c);
                            }
                            //retrieve Contract Line Items Not IsBundle
                            lstContractLineItemNotBundle = new List<ContractLineItem>([SELECT Id, LineItemNumber,Product2Id,tech_nom_produit__c, ServiceContractId,Quantity, TotalPrice,ListPrice, IsBundle__c, UnitPrice, Discount FROM ContractLineItem WHERE (ServiceContractId IN :lstServiceContract AND IsBundle__c = False )]);

                            System.debug('**** List Contract Line Items Not IsBundle  ' + lstContractLineItemNotBundle.size());

                            //Loop to retrieve Options for contract
                            if(lstContractLineItemNotBundle.size()>0 && setProductId.size()>0 ){
                                lstContractLineItemOptions = new List<ContractLineItem>();
                                for(ContractLineItem c:lstContractLineItemNotBundle){
                                    if(setProductId.contains(c.Product2Id) && c.Quantity==1){
                                        lstContractLineItemOptions.add(c);
                                        hasOptions = true;
                                        System.debug('**** Is Option:  ' + c.LineItemNumber);
                                    }
                                }
                            }
                        }
                    }
                     cli = new List<wrapperclI>();
                     for(ContractLineItem con:lstContractLineItem){
                        wrapperclI c = new wrapperclI(con);
                        cli.add(c);
                     }
                    System.debug('**** cli ' + cli);

                    System.debug('List of contract line item where is bundle is true: ' + lstContractLineItem);
                    currentServiceContract = lstServiceContract[0];

                    //Check if address facturation same
                    if((currentServiceContract.BillingCity != currentServiceContract.Account.BillingCity) || 
                        (currentServiceContract.BillingPostalCode != currentServiceContract.Account.BillingPostalCode) || 
                        (currentServiceContract.BillingStreet != currentServiceContract.Account.BillingStreet) 
                        ){
                            showAddFacturation = true;
                    }
                    System.debug('**** showAddFacturation ' + showAddFacturation);
                    //Check if address appareil same
                    if((currentServiceContract.BillingCity != currentServiceContract.Logement__r.City__c) || 
                        (currentServiceContract.BillingPostalCode != currentServiceContract.Logement__r.Postal_Code__c) || 
                        (currentServiceContract.BillingStreet != currentServiceContract.Logement__r.Street__c) 
                        ){
                            showAddAppareil = true;
                    }
                    System.debug('**** showAddAppareil ' + showAddAppareil);
                    agencyName = currentServiceContract.Agency__r.Name;
                    if(currentServiceContract.Tax__c ==null){
                        tax = 0.0;
                    }else{
                        if(currentServiceContract.Tax__c == '5.5'){
                             tax = 0.055;
                        }else if(currentServiceContract.Tax__c == '10'){
                             tax = 0.1;
                        }else if(currentServiceContract.Tax__c == '20'){
                             tax = 0.2;
                        }
                    }
                    nomRemise = 'Remise ';
                    valable = '';
                    if(currentServiceContract.Duree_d_application_de_la_remise__c!=null){
                        nomRemise = 'Remise ';
                        valable = 'valable '+currentServiceContract.Duree_d_application_de_la_remise__c + ' ans';
                    }/*else{
                        if(currentServiceContract.Promotional_Code__r.Offer__r.Name !=null 
                            && currentServiceContract.Promotional_Code__r.Offer__r.The_offer_is_active__c)
                        {
                            nomRemise = 'Remise '+ currentServiceContract.Promotional_Code__r.Offer__r.Name;
                            valable =  ' valable jusqu\'au ' + currentServiceContract.Promotional_Code__r.Offer__r.End_date_of_the_offer__c.day() + '/' + currentServiceContract.Promotional_Code__r.Offer__r.End_date_of_the_offer__c.month() + '/' +currentServiceContract.Promotional_Code__r.Offer__r.End_date_of_the_offer__c.year() ;
                        }
                    }*/
                    w = new wrapperSC(currentServiceContract);
                    System.debug('**** w ' + w);
                    
                    //DMG 08/01/2020- Retrieve Service Appointment to get Date Visite 
                    lstServiceAppointment = [Select Id,EarliestStartTime from ServiceAppointment where Service_Contract__c =:currentServiceContract.Id order by createddate desc limit 1];
                    if(lstServiceAppointment.size()>0){
                        dateVisite = lstServiceAppointment[0].EarliestStartTime;
                    }

                    //DMG 08/01/2020- Check if Person Acc/ Business Acc
                    List<Contact> lstCon = new List<Contact> ();
                    isPersonAcc=true;
                    if(currentServiceContract.Account.isPersonAccount == false){
                        isPersonAcc = false;
                        System.debug('**** isPersonAcc ' + isPersonAcc);
                        lstCon = [Select Id,AccountId,Email,MobilePhone from Contact where AccountId =:currentServiceContract.AccountId limit 1];
                        System.debug('**** lstCon ' + lstCon);
                        if(lstCon.size()>0){
                            ConPro = lstCon[0];
                        }
                    }

                    // DMG 07/02/2020
                        List<Attachment> lstAttachmentClient = new List<Attachment>();
                        lstAttachmentClient = [select Id,Name from Attachment where ParentId=:currentServiceContract.Id  and  Name='SignatureClient.png' order by createdDate desc];
                        if(lstAttachmentClient.size() > 0){
                            idSignature = '/servlet/servlet.FileDownload?file='+lstAttachmentClient[0].id;
                        }else{
                            idSignature = 'null';
                        }
                    //DMG 16/01/2020- Retrieve Payment objects - sofactoapp__R_glement__c for Bulletin de Paiement
                    List<sofactoapp__R_glement__c> lstPaymentMax = new List<sofactoapp__R_glement__c> ();
                    List<sofactoapp__R_glement__c> lstPaymentMin = new List<sofactoapp__R_glement__c> ();
                    // if(currentServiceContract.Echeancier_Paiement__c == 'Mensuel'){
                        lstPaymentMax = [Select Id,sofactoapp__Montant__c,sofactoapp__Date__c from sofactoapp__R_glement__c where Sofacto_Service_Contract__c =:currentServiceContract.Id order by sofactoapp__Date__c desc limit 1];
                        lstPaymentMin = [Select Id,sofactoapp__Montant__c,sofactoapp__Date__c from sofactoapp__R_glement__c where Sofacto_Service_Contract__c =:currentServiceContract.Id order by sofactoapp__Date__c asc limit 1];
                        //Init Payment
                        MaxPayment = 0.0;
                        MinPayment = 0.0;
                        if(lstPaymentMax.size()>0){
                            MaxPayment = lstPaymentMax[0].sofactoapp__Montant__c;
                        }
                        if(lstPaymentMin.size()>0){
                            MinPayment = lstPaymentMin[0].sofactoapp__Montant__c;
                        }
                        System.debug('**** MaxPayment ' + MaxPayment);
                        System.debug('**** MinPayment ' + MinPayment);
                    // }


                    // System.debug('##### [hone: '+currentCase.Local_Agency__r.Phone__c);
                    // assetAccName = (lstCase[0].AssetId != null && Asset.AccountId != null) ? (lstCase[0].Asset.Account.IsPersonAccount == true ? lstCase[0].Asset.Account.FirstName + ' ' + lstCase[0].Asset.Account.LastName : lstCase[0].Asset.Account.Name) : ' ';
                    // list<ServiceAppointment> saLst = [select id, SchedStartTime,Status from ServiceAppointment where TECH_WorkOrderCaseId__c =: String.valueOf(lstCase[0].Id).substring(0, 15) order by createdDate desc limit 1 ];
                    // system.debug('*** saLst 1: '+ saLst);
                    // if(saLst.size()>0){
                    //     system.debug('*** saLst 2: '+ saLst);
                    //     sa = saLst[0];

                    //     System.debug('sa.Id : '+sa.Id);

                        //ANA START
                        // System.debug('## SA Status: ' + sa.Status); 
                        // if(sa.Status==AP_Constant.servAppStatusImpoToFinish || sa.Status==AP_Constant.servAppStatusEfectueConc || sa.Status==AP_Constant.servAppStatusTravauxAprevoir){
                        //     BoolCheckClientPresent=true;
                        // }
                        // else if(sa.Status==AP_Constant.servAppStatusClientAbsent){
                        //     BoolCheckClientAbsent=true;
                        // }
                        // else {
                        //     BoolCheckClientPresent=false;
                        //     BoolCheckClientAbsent=false;
                        // }

                        // SRName='';

                        // List<AssignedResource> lstASR = [SELECT ServiceResourceId FROM AssignedResource WHERE ServiceAppointmentId=:sa.Id ORDER BY CreatedDate DESC LIMIT 1];
                        // if(lstASR.size()>0){

                        //     List<ServiceResource> lstSR = [SELECT RelatedRecordId FROM ServiceResource WHERE Id =:lstASR[0].ServiceResourceId];

                        //     if(lstSR.size()>0){
                        //         User lstUser = [SELECT Name FROM User WHERE Id =:lstSR[0].RelatedRecordId];
                        //         SRName = lstUser.Name;
                        //     }
                        // }

                        //ANA END
                    // }

                }
        }

    }
}